<?php

namespace App;

use App\Models\Cycle;
use App\Models\Employee;
use App\Models\EmployeeHrRepresentative;
use App\Models\Role;
use App\Models\Status;
use App\Models\UserCycle;
use App\Models\UserRole;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','admin','super','active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    public static function createUser(Employee $employee){
        try {
            $cycle = Cycle::getCurrentCycle();
            $cycle_id = $cycle->id;
            $status_new = Status::find(6);
            $status_active = Status::find(1);
            $user = null;
            if ($employee !==null){
                if ($employee->status_id === 12 || $employee->status_id === $status_active->id  || $employee->status_id === 13){

                    $user_cycle = UserCycle::where('cycle_id', $cycle_id)->where('employee_id', $employee->id)->first();

                    if ($user_cycle === null){
                        $user_cycle = new UserCycle();
                        $user_cycle->cycle_id = $cycle_id;
                        $user_cycle->employee_id = $employee->id;
                        $user_cycle->status_id = 1; // active

                        //check if the user exists in the users table via the employee email
                        $user = User::where('email', $employee->email)->first();
                        if ($user === null)
                            $user = new User(); // if does not exist create a new one

                    }
                    else{
                        //check if the user exists via the USER_ID on the user_cycles table
                        $user = User::find($user_cycle->user_id);
                        //if user exists update details
                        if ($user === null)
                            $user = new User(); // if does not exist create a new one

                    }

                    //if user exists update details
                    $user->name = $employee->firstnames .' ' . $employee->lastname;
                    $user->email = $employee->email;
                    $user->username = $employee->email;
                    $user->password = Hash::make('Pa55w0rd*');
                    $user->save();

                    // map it to the user cycle
                    $user_cycle->status_id = $employee->status_id;
                    $user_cycle->user_id = $user->id;
                    $user_cycle->save();
                    logger(" user id: $user->id");
                    return $user;
                }
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public static function deactivateUser(Employee $employee){
        try {
            $cycle = Cycle::getCurrentCycle();
            $cycle_id = $cycle->id;
            $status_inactive = Status::where('name', 'Inactive')->first();

            $user_cycle = UserCycle::where('cycle_id', $cycle_id)->where('employee_id', $employee->id)->first();
            if ($user_cycle !== null){
                //update the user_cycle
                $user_cycle->status_id = $status_inactive->id;
                $user_cycle->save();

                $user = User::where('id', $user_cycle->user_id)->first();
                if ($user !== null){
                    $user->status_id = $status_inactive->id;
                    $user->save();
                    //update the user_roles
                    $user_roles = UserRole::where('user_id', $user->id)->get();
                    if ($user_roles !== null){
                        foreach ($user_roles as $user_role){
                            $user_role->delete();
                        }
                    }
                }
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public static function userRoleMapping(Employee $employee, User $user){

        try {
            $status_active = Status::find(1);
            $status_inactive = Status::find(2);
            $status_assessor = Status::find(12);
            $cycle = Cycle::getCurrentCycle();
            $cycle_id = $cycle->id;


            $role = Role::where('name', 'Scheme Contracting')->first();
            if ($role !== null){
                self::mapUserRole($user, $role);
            }

            $user_cycle = UserCycle::where('cycle_id', $cycle_id)->where('user_id', $user->id)->where('employee_id', $employee->id)->first();
            if ($user_cycle !== null){

                //---------- HR
                $hr_link = EmployeeHrRepresentative::where('hr_employee_id', $employee->id)->first();
                if ($hr_link !== null){
                    $user_cycle->is_hr = 1;
                }
                else{
                    $user_cycle->is_hr = 0;
                }

                //---------- Manager
                $subordinate = Employee::where('cycle_id', $cycle_id)->where('linemanager', $employee->personnelnumber)->first();
                if ($subordinate !== null){
                    $user_cycle->is_manager = 1;
                }
                else{
                    $user_cycle->is_manager = 0;
                }

                //---------- Participant
                if ($employee->status_id === $status_active->id)
                {
                    $user_cycle->is_participant = 1;
                }
                elseif ($employee->status_id === $status_assessor->id)
                {
                    $user_cycle->is_assessor = 1;
                    $user_cycle->is_participant = 0;
                }
                else{
                    $user_cycle->is_assessor = 0;
                    $user_cycle->is_participant = 0;
                }

                $user_cycle->save();
            }


        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public static function mapUserRole($user, $role){
        try {
            $user_role = UserRole::where('user_id',$user->id)->where('role_id',$role->id)->first();
            if ($user_role === null ){
                $user_role = new UserRole();
                $user_role->user_id = $user->id;
                $user_role->role_id = $role->id;
                $user_role->save();
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public function getEmployees(){
        $employees = Employee::where('email', $this->email)->where('cycle_id', Cycle::getCurrentCycle()->id)->get();
        return count($employees)>1 ? $employees : [];
    }
}

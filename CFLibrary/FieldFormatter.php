<?php

/* 
 * This inclusive preprocessed hypertext file holds methods for contoling a
 * FieldFormatter.
 * 
*/
namespace App\CFLibrary;

use Illuminate\Support\Facades\URL;
use App\Http\Controllers\ManagerMappingsController;
use App\Http\Controllers\LookupModalController;


/* Define formatting constants */
define('FIELD_FORMATTER_NONE', 0);

class FieldFormatter
{
  /*
  * FieldFormatter Constructor.
  *
  * @return void
  */
  function FieldFormatter()
  {
    
  }
  
  /**
   * @static
   * @param Mixed    $field The value to format
   * @param int      $type The field formatter key
   * @param null|int $id Record ID
   * @param null|string
   * @return array|bool|float|mixed|number|string
   */
  static function format($field, $type, $id = NULL, $field_name = NULL)
  {
    if ($type >= 256)
    {
      return formatCustom($field, $type, $id, $field_name);
    }
    else
    {
      switch ($type)
      {
        case 'FIELD_FORMATTER_NONE':
            return '';
        break;
        case 'FIELD_FORMATTER_EMAIL_PREVIEW':
            
            $button = '<button type="sumbit" class="btn btn-success" onclick="document.getElementById(\'mode\').value = \'button_click\';document.getElementById(\'button_id\').value = \''.$field.'\';">Preview</button>';
            return $button;
        break;    
        case 'FIELD_FORMATTER_NUMBER':
            if(is_numeric($field)){
                $value = number_format( $field , 2 ,"." , " " );
            }else{
                $value = $field;
            }
            return $value;
        break;    
        case 'ALTERNUMBER':
          return '';
        break;
        case 'DIVISIONFORMATTER':
            return $field;
            break;
        case 'VALIDATION_REPORT_LINK':
            $field  = base64_encode($field);
            $url    = URL::route('validation_report',['file'=>$field]);
            return  "<a href=\"$url\">Download Validation Report</a>";
            break;

        case 'IMPORT_FILE_LINK':
            $field  = base64_encode($field);
            $url    = URL::route('import_file',['file'=>$field]);
            return  "<a href=\"$url\">Download Import File</a>";
            break;

        case 'FIELD_FORMATTER_YES_NO':
          if ($field === 'Y' || $field === 'y') return 'Yes';
          if ($field === 'T' || $field === 't') return 'Yes';
          if (($field == '1') && ($field == 1))
          {
            return 'Yes';
          }
          else
          {
            return 'No';
          }
          break;
        case 'FIELD_FORMATTER_OPEN_CLOSE':
          if (($field == '1') && ($field == 1))
          {
            return 'Open';
          }
          else
          {
            return 'Closed';
          }
          break;
        case 'FIELD_FORMATTER_DECRYPT':
          /**IMPORTANT : NO NOT REMOVE THIS FORMATTER, IF YOU CANT USE IT THEN DONT CALL IT**
          **This formatter is there for a purpose and gets used to do testing, if you
          * are receiving bugs because of this formatter then remove it from all field output_rules,
          * you do not have to remove the code, if you do not call to this constant you will not 
          * process the mcrypt package which causes the problem for latest php version **/  
            /*Until newer module can be created disable the deprecate error check
            But need to find new method - hashing will work better but old data encrypted this way*/
            ini_set('error_reporting', E_ALL & ~E_DEPRECATED);
            $key = "{26300092-94D8-4e37-ABE0-E13050260803}";
            
            if (empty($field)) return $field;

            $td = mcrypt_module_open(MCRYPT_RIJNDAEL_256, '', MCRYPT_MODE_ECB, '');
            $key = substr($key, 0, mcrypt_enc_get_key_size($td));
            $iv_size = mcrypt_enc_get_iv_size($td);
            $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
            mcrypt_generic_init($td, $key, $iv);
            $decoded = base64_decode($field);
            if (strlen($decoded) > 0)
            {
              $p_t = mdecrypt_generic($td, base64_decode($field));
              $p_t = rtrim($p_t);
            }
            else
            {
              $p_t = null;
            }

            $field = $p_t;
            mcrypt_generic_deinit($td);
            mcrypt_module_close($td);
            return $field;
          break;
        case 'FIELD_FORMATTER_OPEN_RECORD':
          
            return $field;
            //return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm">open</button>';
          break; 
        case 'FIELD_FORMATTER_OPEN_PERSONNEL_RECORD':
          
            $selections['field_name'] = 'id';
            $selections['field_value'] = $field;
            $mode = 'read';
            
            $form_submit_data = array();
            $table_id = 'manager_lookup_'.$field;
            $panel = new TableViewerPanel('vw_employee_hcbp', '', $mode, $selections, $form_submit_data, 'employee_assessors','vertical',[],$table_id,true);
            $table = $panel->show();
            $card_template = $table['content'];
            
            return $card_template;
            //return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm">open</button>';
          break;
        case 'FIELD_FORMATTER_LOOKUP_HCBP':
          
            $selections['field_name'] = 'id';
            $selections['field_value'] = $field;
            $mode = 'read';
            
            $form_submit_data = array();
            $field_gen = self::generateRandomString(10);
            $table_id = 'manager_lookup_'.$field_gen.'_'.$field;
            $panel = new TableViewerPanel('vw_employee_hcbp', '', $mode, $selections, $form_submit_data, 'employee_assessors','vertical',[],$table_id,true);
            $table = $panel->show();
            $buffer = str_replace(array("\r", "\t"), '', $table['content']);
            $card_template = $buffer;
            
            return $card_template;
            //return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm">open</button>';
          break;
        case 'FIELD_FORMATTER_LOOKUP_ORGANISATION':
          
            $selections['field_name'] = 'id';
            $selections['field_value'] = $field;
            $mode = 'read';
            
            $form_submit_data = array();
            $field_gen = self::generateRandomString(10);
            $table_id = 'org_lookup_'.$field_gen.'_'.$field;
            $panel = new TableViewerPanel('vw_org_structure', '', $mode, $selections, $form_submit_data, 'org_structures','vertical',[],$table_id,true);
            $table = $panel->show();
            $buffer = str_replace(array("\r", "\t"), '', $table['content']);
            $card_template = $buffer;
            
            return $card_template;
            //return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm">open</button>';
          break;
        case 'FIELD_FORMATTER_LOOKUP_BENCHMARK_TABLE':
            $selections['field_name'] = 'id';
            $selections['field_value'] = $field;
            $mode = 'read';
            
            $form_submit_data = array();
            $field_gen = self::generateRandomString(10);
            $table_id = 'benchmark_'.$field_gen.'_'.$field;
            $panel = new TableViewerPanel('benchmarks', '', $mode, $selections, $form_submit_data, NULL,'vertical',[],$table_id,true);
            $table = $panel->show();
            $buffer = str_replace(array("\r", "\t", "\n"), '', $table['content']);
            /*$buffer = urlencode($buffer);
            $buffer = str_replace("%0D%0A", "<br />", $buffer);
            $buffer = urldecode($buffer);
            $buffer = addslashes(html_entity_decode($buffer));*/
            $card_template = $buffer;
            
            return $card_template;
          break;  
        case 'FIELD_FORMATTER_LOOKUP_VIEW':
          
            $selections['field_name'] = 'id';
            $selections['field_value'] = $field;
            $mode = 'read';
            
            $form_submit_data = array();
            $table_id = 'manager_lookup_'.$field;
            $panel = new TableViewerPanel('vw_employee_viewer', '', $mode, $selections, $form_submit_data, 'employee_assessors','vertical',[],$table_id,true);
            $table = $panel->show();
            $card_template = $table['content'];
            
            return $card_template;
            //return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm">open</button>';
          break;
        case 'FIELD_FORMATTER_LOOKUP_VALIDATION_MANAGER':
          
            $selections['field_name'] = 'id';
            $selections['field_value'] = $field;
            $mode = 'read';
            
            $form_submit_data = array();
            $table_id = 'manager_lookup_'.$field;
            $panel = new TableViewerPanel('vw_employee_validation_manager', '', $mode, $selections, $form_submit_data, 'employee_assessors','vertical',[],$table_id,true);
            $table = $panel->show();
            $card_template = $table['content'];
            
            return $card_template;
            //return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm">open</button>';
          break;
        case 'FIELD_FORMATTER_LOOKUP_ALLOCATION_MANAGER':
          
            $selections['field_name'] = 'id';
            $selections['field_value'] = $field;
            $mode = 'read';
            
            $form_submit_data = array();
            $table_id = 'manager_lookup_'.$field;
            $panel = new TableViewerPanel('vw_employee_allocation_manager', '', $mode, $selections, $form_submit_data, 'employee_assessors','vertical',[],$table_id,true);
            $table = $panel->show();
            $card_template = $table['content'];
            
            return $card_template;
            //return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm">open</button>';
          break;
        case 'FIELD_FORMATTER_LOOKUP_BUSINESS_MANAGER':
          
            $selections['field_name'] = 'id';
            $selections['field_value'] = $field;
            $mode = 'read';
            
            $form_submit_data = array();
            $table_id = 'manager_lookup_'.$field;
            $panel = new TableViewerPanel('vw_employee_business_manager', '', $mode, $selections, $form_submit_data, 'employee_assessors','vertical',[],$table_id,true);
            $table = $panel->show();
            $card_template = $table['content'];
            
            return $card_template;
            //return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm">open</button>';
          break;
       case 'FIELD_FORMATTER_LOOKUP_BUSINESS_AREA_MANAGER':
          
            $selections['field_name'] = 'id';
            $selections['field_value'] = $field;
            $mode = 'read';
            
            $form_submit_data = array();
            $table_id = 'manager_lookup_'.$field;
            $panel = new TableViewerPanel('vw_employee_business_area_manager', '', $mode, $selections, $form_submit_data, 'employee_assessors','vertical',[],$table_id,true);
            $table = $panel->show();
            $card_template = $table['content'];
            
            return $card_template;
            //return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm">open</button>';
          break;
        case 'FIELD_FORMATTER_MANAGER_LOOKUP':
            $selected = '';
            $fields = array();
            $table_id_name = 'lookup';
            if(!empty($field)){
                
                if(isset($field['value']) && !empty($field['value'])){
                    $selected = $field['value'];
                }
                
                if(isset($field['field']) && !empty($field['field'])){
                    $fields = $field['field'];
                    if($selected != ''){
                        $record_id = $selected;
                    }else{
                        $record_id = 0;
                    }
                    $table_id_name .= '_'.$fields['field_name'].'_'.$record_id;
                }
                
            }
            $field_name = $fields['field_name'];
            $selections['field_name'] = 'id';
            $selections['field_value'] = $selected;
            $mode = 'read';
            
            
            $form_submit_data = array();
            
            $table = ManagerMappingsController::getTablePanel($mode,$selections,$form_submit_data,$table_id_name,$field_name);
                       
            return $table;
          break;
        case 'FIELD_FORMATTER_DEFAULT_LOOKUP':
             $selected = '';
            $fields = array();
            $table_id_name = 'lookup';
            $table_name = '';
            if(!empty($field)){
                
                if(isset($field['value']) && !empty($field['value'])){
                    $selected = $field['value'];
                }
                
                if(isset($field['field']) && !empty($field['field'])){
                    $fields = $field['field'];
                    if($selected != ''){
                        $record_id = $selected;
                    }else{
                        $record_id = 0;
                    }
                    $table_id_name .= '_'.$fields['field_name'].'_'.$record_id;
                }
                if(isset($field['table_name']) && !empty($field['table_name'])){
                    $table_name = $field['table_name'];
                }
                
            }
            $field_name = $fields['field_name'];
            $selections['field_name'] = 'id';
            $selections['field_value'] = $selected;
            $mode = 'read';
            
            
            $form_submit_data = array();
            
            $table = LookupModalController::getTablePanel($mode,$selections,$form_submit_data,$table_id_name,$field_name,$table_name);
                       
            return $table;
            
            break;
        case 'FORMATTER_STATUS_REQUIRED_RULE':
            //print_r($field);
            
            
            break;
        case 'FORMATTER_MANAGER_LOOKUP_WINDOW':
            
            $table = LookupModalController::getLookupTableManager();
            return $table;   
            
            break;
        default:
          /* Didn't know what to do with the field */
          return $field;
      }
    }
    return false;
  }
  
  public static function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
  
  public static function getFieldFormatterKey($description)
  {

    $consts = get_defined_constants();
    if (array_key_exists($description, $consts))
    {
      return $consts[$description];

    }
    return FIELD_FORMATTER_NONE;
  }
    
}

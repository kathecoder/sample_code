<?php

/* 
 * Input Field Validation component. Formats fields for data security and integrity.
 * 
*/

namespace App\CFLibrary;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Shared\Date;

define('INPUT_VALIDATOR_VALID', 0);
define('INPUT_VALIDATOR_EMAIL', 1);
define('INPUT_VALIDATOR_UNIQUE', 2);
define('INPUT_VALIDATOR_TELEPHONE', 3);
define('INPUT_VALIDATOR_NUMBER_ROUND_DECI', 4);
define('INPUT_VALIDATOR_DATE_DD_MM_YYYY', 5);
define('INPUT_VALIDATOR_ROT13', 6);
define('INPUT_VALIDATOR_ENCRYPT', 7);
define('INPUT_VALIDATOR_NOT_SELECTED', 8);
define('INPUT_VALIDATOR_GENERIC_LOOKUP', 9);
define('INPUT_VALIDATOR_NOT_NUMERIC', 10);
define('INPUT_VALIDATOR_NO_NUMBERS', 11);
define('INPUT_VALIDATOR_NUMERIC', 12);
define('INPUT_VALIDATOR_NUMERIC_OPTIONAL', 13);
define('INPUT_VALIDATOR_DATE_YYYY_MM_DD', 14);
define('INPUT_VALIDATOR_IMPORT_ENCRYPT', 15);
define('INPUT_VALIDATOR_REQUIRED', 16);


class InputValidator
{

    public function validateType(&$field, $type, $tableName, $sqlName, $field_name, $id)
    {

        switch ($type)
        {
            case INPUT_VALIDATOR_DATE_YYYY_MM_DD:
                if (empty($field)) return true;
                try
                {
                    $date = new \DateTime($field);
                    $date = $date->format('Y-m-d');
                    if (strcmp($date, $field) != 0)
                    {
                        $this->set_validator_message($sqlName, $field_name . ' is in an incorrect format (YYYY-MM-DD).');
                        return false;
                    }
                }
                catch (\Exception $e)
                {
                    $this->set_validator_message($sqlName, $field_name . ' is in an incorrect format (YYYY-MM-DD).');
                    return false;
                }

                return true;
                break;

            case INPUT_VALIDATOR_NUMERIC_OPTIONAL:
                if (empty($field)) return true;
                if (!is_numeric($field))
                {
                    $this->set_validator_message($sqlName, 'Value must be numeric.');
                    return false;
                }
                return true;
                break;

            case INPUT_VALIDATOR_REQUIRED:
                if (empty($field) || $field === NULL || $field === '')
                {
                    $this->set_validator_message($sqlName, $field_name . ' is required.');
                    return false;
                }
                return true;
                break;

            case INPUT_VALIDATOR_NUMERIC:
                if (empty($field)) return true;
                if (!is_numeric($field))
                {
                    $this->set_validator_message($sqlName, 'Value must be numeric');
                    return false;
                }
                return true;
                break;
            case INPUT_VALIDATOR_NO_NUMBERS:
                if (strpbrk($field, '0123456789') !== false)
                {
                    $this->set_validator_message($sqlName, 'Value must not contain numbers');
                    return false;
                }
                return true;
                break;

            case INPUT_VALIDATOR_NOT_NUMERIC:
                if (is_numeric($field))
                {
                    $this->set_validator_message($sqlName, 'Value must not be numeric');
                    return false;
                }
                return true;
                break;

            case INPUT_VALIDATOR_GENERIC_LOOKUP:
                list($field, $table) = $field;
                $field = trim($field);
                if ($field === '' || $field === NULL) return true;

                /*$query = "SELECT COUNT(*) as cnt FROM $table WHERE name = '$field' OR id = '$field';";
                $result =  DB::select ($query);
                $cnt = $result[0]->cnt;

                if ($cnt === 0)
                {
                    $this->set_validator_message($sqlName, 'Invalid value for ' . $field_name);
                    return false;
                }*/

                $result = DB::table($table)->where(function($query) use($field){
                    $query->where('name', $field)->orWhere('id', $field);
                })->get();

                if (count($result) === 0)
                {
                    $this->set_validator_message($sqlName, 'Invalid value for ' . $field_name);
                    return false;
                }
                return true;
                break;


            case INPUT_VALIDATOR_NOT_SELECTED:
                if ($field == '1')
                {
                    $this->set_validator_message($sqlName, $field_name . ' must be selected');
                    return (false);
                }
                return true;
                break;
            case INPUT_VALIDATOR_VALID:
                return true;
                break;


            case INPUT_VALIDATOR_EMAIL:
                $field = trim($field);
                if (empty($field)) return true;
                if (!filter_var($field, FILTER_VALIDATE_EMAIL)) {
                    $this->set_validator_message($sqlName, $field_name . ' must be a valid email address.');
                    return false;
                }
                return true;
                break;

            case INPUT_VALIDATOR_UNIQUE:

                $field = trim($field);
                if (strlen($field) == 0) return true;

                $cnt = null;
                if ($id === '*')
                {
                    /*$query = "SELECT COUNT(*) as cnt FROM $tableName WHERE $sqlName = '$field';";
                    $result =  DB::select ($query);*/

                    $result = DB::table($tableName)->where($sqlName, $field)->get();
                    $cnt = count($result);
                }
                else
                {
                    //$query = "SELECT count(!id) FROM !table WHERE !field = ? AND !id <> ?";
                    //$cnt = @$_ENV['SITE_VARS']['SQL_CONNECTION']->getScalar($query, array($field, $id), array('!id' => 'id', '!table' => $tableName, '!field' => $sqlName));
                    /*$query = "SELECT COUNT($sqlName) as cnt FROM $tableName WHERE $sqlName = '$field' AND id <> $id;";
                    $result =  DB::select ($query);*/

                    $result = DB::table($tableName)->where($sqlName, $field)->where('id', '<>', $id)->get();
                    $cnt = count($result);
                }

                if (intval($cnt) > 0)
                {
                    $this->set_validator_message($sqlName, $field_name . ' already exists.');
                    return false;
                }
                return true;
                break;

            case INPUT_VALIDATOR_TELEPHONE:
                $field = trim($field);
                if (empty($field)) return true;
                if (strlen($field) == 10)
                {
                    //return is_numeric($field);
                    if (is_numeric($field) === false){
                        $this->set_validator_message($sqlName, $field_name . ' must be a valid phone number.');
                        return false;
                    }
                }
                elseif (strlen($field) == 12)
                {
                    if ($field[0] != '+') return false;

                    //return is_numeric(substr($field, 1, -1));
                    if (is_numeric(substr($field, 1, -1) === false)){
                        $this->set_validator_message($sqlName, $field_name . ' must be a valid phone number.');
                        return false;
                    }
                }
                return false;
                break;

            case INPUT_VALIDATOR_NUMBER_ROUND_DECI:
                if (empty($field)) return true;
                if (!is_numeric($field))
                {
                    $this->set_validator_message($sqlName, $field_name . ' must be a number.');
                    return false;
                }
                $points = explode('.', $field);

                if (count($points) < 2)
                {
                    if (empty($field)) $field = '0';
                }
                else
                {
                    $field = round($field, 1);
                }
                if (strpos($field, '.') === false) $field .= '.0';
                return true;
                break;

            case INPUT_VALIDATOR_DATE_DD_MM_YYYY:
                $field = trim($field);
                if (empty($field)) return true;
                // get date parts
                $dateParts = explode('-', $field);
                if(count($dateParts) < 3){
                    $check = self::validateExcelDate($field, "Y-m-d");
                    if ($check)
                        return $check;
                    else{
                        $this->set_validator_message($sqlName, $field_name . ' is in an incorrect format (DD-MM-YYYY).');
                        return false;
                    }
                }
                $ret = ((count($dateParts) == 3) && (is_numeric($dateParts[0])) && (is_numeric($dateParts[1])) && (is_numeric($dateParts[2])) && (checkdate($dateParts[1], $dateParts[0], $dateParts[2])));
                if (!$ret)
                {
                    $this->set_validator_message($sqlName, $field_name . ' is in an incorrect format (DD-MM-YYYY).');
                }
                return $ret;
                break;

            case INPUT_VALIDATOR_ROT13:
                $field = str_rot13($field);
                return true;
                break;

           /* case INPUT_VALIDATOR_ENCRYPT:
                $field = FieldFormatter::format($field, FIELD_FORMATTER_ENCRYPT);
                return true;
                break;

            case INPUT_VALIDATOR_IMPORT_ENCRYPT:
                if ($_ENV['SITE_VARS']['UNI_CURRENT_MODE'] === UNI_IMPORT)
                {
                    $field = FieldFormatter::format($field, FIELD_FORMATTER_ENCRYPT);
                }
                return true;
                break;*/
            default:
                return true;
        }
    }


    /*
      Sets the validator message
    */
    public function set_validator_message($field_name, $message)
    {
        if ($field_name == 'Field')
        {
            $this->validator_message[] = $message;
        }
        else
        {
            $this->validator_message[$field_name] = $message;
        }
    }

    /*
      Formats any field by removing all characters and sequnces that could
      cause security problems. i.e. All back slashes and HTML tags (to avoid
      any scripts from executing).

      Paramaters:
        $field    The field to format from

      Returns true on successfull validation
    */
    public function format_field(&$field)
    {
        //$field = strip_tags($field);
        //$field = str_replace('\\', '', $field);
        //$field = addslashes($field) ;
        $field = trim($field); //trim remaining whitespace
        return $field;
    }

    /*
      Formats a numeric field by removing any non-numeric elements.

      Paramaters:
        $field    The field to format from,
        $exceptions Set of special chacters to over look

      Note: By default the last '.' is considered as the decimal
          point for floating point values.

      Returns a string (for large number support)
    */
    public function formatNumericField(&$field, $exceptions = array('.', '-', ','))
    {
        $formated_field = null;
        $digits = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $exception_used = false;

        for ($i = strlen($field) - 1; $i >= 0; $i--)
        {
            /* Allow exceptions */
            if (in_array($field[$i], $exceptions) && !$exception_used)
            {
                /* format '-' and ',' to '.''s */
                if (in_array($field[$i], array('-', ',')))
                {
                    $field[$i] = '.';
                }

                $formated_field = $field[$i] . $formated_field;
                $exception_used = true;
            }

            /* Fill formated field */
            if (in_array($field[$i], $digits))
            {
                $formated_field = $field[$i] . $formated_field;
            }
        }

        /* Format floats */
        if ($exception_used && in_array('.', $exceptions))
        {
            $formated_field = number_format($formated_field, 2, '.', '');
        }

        $field = $formated_field;
        return $formated_field;
    }

    /*
      Validates an input field

      Parameters:
        $field              The field data to validate
        $field_name         Field name (default = 'Field')
        $blank              Allow blank fields
        $numeric            Only numeric
        $maximum_length     Maximum length

      Returns true on successful validation
    */
    public function validate(&$field, $field_name = 'Field', $blank = false, $numeric = false, $maximum_length = 255, $minimum_length = 0)
    {
       // if ()
        $field = $this->format_field($field);
        /* Check blank fields */
        if (!$blank && strlen($field) == 0 && !($numeric && $field == '0'))
        {
            $this->set_validator_message($field_name, $field_name . ' can\'t be left blank.');
            $this->validation_list[] = false;
            return false;
        }

        /* Check numeric field */
        if ($numeric && !is_numeric($field) && !(strlen($field) == 0 && $blank))
        {
            $this->set_validator_message($field_name, $field_name . ' must be a number.');
            $this->validation_list[] = false;
            return false;
        }

        /* Check maximum field length */
        if (strlen($field) > $maximum_length)
        {
            $this->set_validator_message($field_name, $field_name . ' is too long (only ' . $maximum_length . ' characters or less).');
            $this->validation_list[] = false;
            return false;
        }

        /* Check minimum field length */
        if (strlen($field) < $minimum_length)
        {
            $this->set_validator_message($field_name, $field_name . ' must be longer (at least ' . $minimum_length . ' characters or more).');
            $this->validation_list[] = false;
            return false;
        }

        /* Field is valid*/
        /*if (empty($field))
        {
            if (is_numeric($field))
            {
                $this->set_validator_message($field_name, 'Ok.');
            }
            else
            {
                $this->set_validator_message($field_name, 'Blank field, not recommended.');
            }
        }
        else
        {
            $this->set_validator_message($field_name, 'Ok.');
        }*/
      //  $this->set_validator_message($field_name, 'Ok.');
        $this->validation_list[] = true;
        return true;
    }

    /*
      Returns the validator message for the field
    */
    public function message($field)
    {
        if ($field == 'Field') $field = 0;
        return $this->validator_message[$field];
    }

    /*
      Returns true if all fields validated are valid.
    */
    public function allFieldsValid()
    {
        $result = true;

        for ($i = 0; $i < count($this->validation_list); $i++)
        {
            $result = $result & $this->validation_list[$i];
        }

        return $result;
    }

    public static function validateExcelDate(string $date, $format = 'Y-m-d')
    {
        try{
            $date = Carbon::instance(Date::excelToDateTimeObject($date))->format($format);
            $d = \DateTime::createFromFormat($format, $date);

            // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
            return $d && $d->format($format) === $date;
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }
    public  $validator_message;
    public $validation_list;
}


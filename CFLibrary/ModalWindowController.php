<?php

/*
 * The class will generate the raw html for a modal window according to the settings passed
 *
 */
namespace App\CFLibrary;

use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

Class ModalWindowController
{
    protected $id;
    protected $settings;
    protected $customSettings;
    protected $template_panel;
    protected $table_name;
    
    public function __construct($id, $label, $content, $title, $buttons = array(), $type = 'dialog', $role='document', $customSettings = array(), $table_name = '', $field = array(),$validationName,$validationNameTwo,$value,$children,$identifier)
    {
        $this->settings = array();
        $this->customSettings = array();
        
        $this->settings['id'] = $id;
        $this->settings['label'] = $label;
        $this->settings['content'] = $content;
        $this->settings['title'] = $title;
        $this->settings['value'] = $value;
        $this->settings['validationName'] = $validationName;
        $this->settings['validationNameTwo'] = $validationNameTwo;
        $this->settings['buttons'] = [];
        $this->settings['fields'] = $field;
       
        $this->settings['children'] = $children;
        $this->settings['identifier'] = $identifier;
        $this->table_name = $table_name;
         
        if(isset($buttons) && !empty($buttons)){
            $this->settings['buttons'] = $buttons;
            
            $this->settings['buttons']['controls_bottom_left'] = $buttons['controls_bottom_left'];
            if(empty($this->settings['buttons']['controls_bottom_left'])){
                $this->settings['buttons']['controls_bottom_left'] = array();               
            }
            $this->settings['buttons']['controls_bottom_right'] = $buttons['controls_bottom_right'];
            if(empty($this->settings['buttons']['controls_bottom_right'])){
                $this->settings['buttons']['controls_bottom_right'] = array();               
            }
            $this->settings['buttons']['controls_top_left'] = $buttons['controls_top_left'];
            if(empty($this->settings['buttons']['controls_top_left'])){
                $this->settings['buttons']['controls_top_left'] = array();               
            }
            $this->settings['buttons']['controls_top_right'] = $buttons['controls_top_right'];
            if(empty($this->settings['buttons']['controls_top_right'])){
                $this->settings['buttons']['controls_top_right'] = array();               
            }                      
        }else{
            //buttons on each corner of screen
            $this->settings['buttons']['controls_bottom_left'] = array();
            $this->settings['buttons']['controls_bottom_right'] = array();
            $this->settings['buttons']['controls_top_left'] = array();
            $this->settings['buttons']['controls_top_right'] = array();
            
        }
        $this->settings['type'] = $type;
        $this->settings['role'] = $role;
        
        //custom settings for 'class' and 'attr'
        foreach ($customSettings as $cKey => $cVal)
        {
            $this->settings[$cKey] = $cVal;
        }
        
    }
    
    public function getModal(): array
    {
      $this->generateContents(true);

      return
          [
              'header' => $this->settings['title'],
              'content' => $this->template_panel,
              'footer' => null,
              'id' => $this->settings['id'],
          ];
     }
    
    public function generateContents(){
        
        $model_calsses = $this->buildModalWindowClasses();
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/ModalWindow.twig',[
            'id' => $this->settings['id'],
            'type' => $this->settings['type'],
            'classes' => $model_calsses,
            'label' => $this->settings['label'],
            'title' =>  $this->settings['title'],
            'role' => $this->settings['role'],
            'content' => $this->settings['content'],
            'controls_bottom_left'=>$this->settings['buttons']['controls_bottom_left'],
            'controls_bottom_right'=>$this->settings['buttons']['controls_bottom_right'],
            'controls_top_left'=>$this->settings['buttons']['controls_top_left'],
            'controls_top_right'=>$this->settings['buttons']['controls_top_right'],  
            'table_name'=>$this->table_name,
            'validationName' => $this->settings['validationName'],
            'validationNameTwo' => $this->settings['validationNameTwo'],
            'value'=>$this->settings['value'], 
            'identifier' => $this->settings['identifier'],
            'children' => $this->settings['children'],
        ]);
        $this->template_panel = $template_panel->show($template);
        
        
    }
    
    public function buildModalWindowContent(){
        
        
    }
    
    public function buildModalWindowClasses(){
        
        $modal_classes = array();
        //eg. dialog will become modal-dialog
        $model_type = 'modal-'.$this->settings['type'];
       
        $modal_classes['parent_div'][] = 'modal';
        
        //eg.hide
        if(isset($this->settings['event']) && !empty($this->settings['event'])){
            $modal_classes['parent_div'][] = $this->settings['event'];
        }
        
        //eg.fade
        if(isset($this->settings['animation']) && !empty($this->settings['animation'])){
            $modal_classes['parent_div'][] = $this->settings['animation'];
        }
        
        $modal_classes['child_div'][] = $model_type;
        
        //eg.modal-dialog-centered
        if(isset($this->settings['position']) && !empty($this->settings['position'])){
            $modal_classes['child_div'][] = $this->settings['position'];
        }
        
        //eg.modal-lg
        if(isset($this->settings['size']) && !empty($this->settings['size'])){
            $modal_classes['child_div'][] = $this->settings['size'];
        }
        
        return $modal_classes;

    }
    
}
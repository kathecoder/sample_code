<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class UniFieldType extends Model
{
    protected $table = 'uni_fieldtypes';
}

<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class UniPermission extends Model
{
    protected $table = 'uni_permission';
}

<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class UniFieldWrite extends Model
{
    protected $table = 'uni_field_write';
}

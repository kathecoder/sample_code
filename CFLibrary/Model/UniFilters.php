<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class UniFilters extends Model
{
    protected $table = 'uni_filters';
}

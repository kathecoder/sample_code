<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class UniCalculations extends Model
{
    protected $table = 'uni_calculations';
}

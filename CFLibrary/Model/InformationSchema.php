<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class InformationSchema extends Model
{
    protected $table = 'INFORMATION_SCHEMA.COLUMNS';
}

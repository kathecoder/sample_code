<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class UniCalcTypes extends Model
{
    protected $table = 'uni_calc_types';
}

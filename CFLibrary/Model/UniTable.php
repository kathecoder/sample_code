<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class UniTable extends Model
{
    protected $table = 'uni_table';
}

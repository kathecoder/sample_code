<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class UniFieldCalculations extends Model
{
    protected $table = 'uni_field_calculations';
}

<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class UniFieldRead extends Model
{
    protected $table = 'uni_field_read';
}

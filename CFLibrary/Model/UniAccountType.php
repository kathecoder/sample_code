<?php

namespace App\CFLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class UniAccountType extends Model
{
    protected $table = 'uni_accounttype';
}

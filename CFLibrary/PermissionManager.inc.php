<?php

/*
 * The class validates the use of db fields against the current instance and user
 *
 */
namespace App\CFLibrary;

use App\CFLibrary\Model\UniPermission;
use Illuminate\Support\Facades\Auth;
use App\User;

Class PermissionManager
{
    private static $userType = NULL;
  
  
    function __construct(){
        /*Get user id from authentication data*/
        // Get the currently authenticated user...
        $user = Auth::user();

        // Get the currently authenticated user's ID...
        $id = Auth::id();
        $this->$userType = $id;
    }
    /**
    * Generates a usage code (eg. R: Read W:Write H:Hidden) for a particular table and field based on the current user and current mode
    *
    * @param int The table id from uni_tables
    * @param string The name of the field
    *
    * @return string The usage code for the field or NULL if the table is not under permission control (ie. Leave field permissions as is)
    */
   static function getUsageCode($table, $field)
   {
     /*if (self::$userType === NULL)
     {
       if (!isset($_SESSION['USER']['TYPE_ID'])) return 'H';
       self::$userType = $_SESSION['USER']['TYPE_ID'];
     }

     if (empty($table)) return 'W';

     if (isset($_SESSION['USER']['PermissionsManager'][$table][self::$userType][$field])) return $_SESSION['USER']['PermissionsManager'][$table][self::$userType][$field];


     $currentMode = PermissonManager::initMode();

     $usage = self::validateEnvironment($table, $currentMode);
     if (!$usage) return NULL;

     $userType = self::$userType;*/

     //Eloquent database collect





    /* if (empty($code)) $code = 'H';
     $_SESSION['USER']['PermissionsManager'][$table][self::$userType][$field] = $code;*/

     $code = 'W';  
     return $code;
   }
   
   
   static function getFormPermissions($table, $table_name)
   {
       $form_permission = array();
       
       $user = User::find(Auth::user()->id);
       
       $user_type = $user->accounttype;
       $user_instance = $user->instance_indicator_id;
       
       $permissions = UniPermission::where('uni_table_id', $table)
                ->where('user_type', $user_type)
                ->where('instance_id', $user_instance)
                ->get();
       
       
       foreach($permissions as $perms)
       {
           $form_permission[$perms->field_name]['field_name'] = $perms->field_name;
           $form_permission[$perms->field_name]['field_id'] = $perms->field_id;
           $form_permission[$perms->field_name]['uni_permissiontype_id'] = $perms->uni_permissiontype_id;
           $form_permission[$perms->field_name]['phase_id'] = $perms->phase_id;
           $form_permission[$perms->field_name]['new_field_label'] = $perms->new_field_label;
       }
       
       //print_r($form_permission);
       return $form_permission;
   }
}

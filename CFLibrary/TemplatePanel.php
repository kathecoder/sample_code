<?php
/**
 */
namespace App\CFLibrary;

class TemplatePanel
{
  protected $template;
  protected $params;

  public function __construct($template, $params = [])
  {
    $this->template = $template;
    $this->params = $params;
  }

  function show(TemplateRenderer $renderer)
  {
    //$renderer = new TemplateRenderer();
    $render_view = $renderer->render($this->template, $this->params);
    return $render_view;
  }


}

<?php

/* 
 * This specialization of the Panel class to render panel content on
 * xhtml complient browsers.
 * 
 */
namespace App\CFLibrary;

use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;
use App\CFLibrary\InputValidator;
use App\CFLibrary\FieldFormatter;
use App\CFLibrary\Model\UniTable;

class FormPanel
{
    /**
    * The properties of the data to display.
    *
    * @var array
    * @access public
    */
   var $fields;
   /**
    * The tables from which to get the data to display.
    *
    * @var string
    * @access public
    */
   var $where;
   protected $table;
   protected $customSettings;

   protected $key;

   protected $contents;
   protected $header;
   protected $model;
   protected $categories;
   protected $id_field;
   protected $entry_id;
   
   function __construct($table,$title, $fields, $where,$model,$categories, $customSettings,$id_field,$entry_id)
    {

      $this->table = $table;
      $this->contents = '';
      $this->header = $title;

      $this->key = $customSettings['key'] ?? '';
      $this->fields = $fields;
      $this->where = $where;

      $this->model = $model;
      $this->categories = $categories;
      $this->entry_id = $entry_id;
      $this->id_field = $id_field;
      if($this->id_field == 'field_value'){
          $this->id_field = 'id';
      }

      $this->customSettings = [
          'current_page' => '',
          'initialSortOrder' => 'ASC',
          'initialSortField' => null,
          'controls' => null,
          'controls_top_left' => null,
          'controls_top_right' => null,
          'controls_bottom_left' => null,
          'controls_bottom_right' => null,
      ];

      foreach ($customSettings as $key => $value)
      {
        $this->customSettings[$key] = $value;
      }

    }
    
    public function getPanel(): array
    {
      $this->generateContents(true);

      return
          [
              'header' => $this->header,
              'content' => $this->contents,
              'footer' => null,
              'id' => null,
          ];
     }
     
    private function generateContents($showForm)
    {
        $pre = '';
        $post = '';
         
        $field_data = array();
        $controls = $this->customSettings['controls'];
        //print_r($this->entry_id);
        $categories_tabs = $this->getCategorySetupTabs();
        $cnt_tabs = count($categories_tabs);
        //print_r($categories_tabs);exit;
        if($this->entry_id != '*' && $this->entry_id != ''){
            $field_data = $this->getFormData();
        }
        
        $form = $this->getForm($field_data);
        
        
        //print_r($categories_tabs);exit;      
        $create_record = false;
        if($categories_tabs[0]['name'] == 'Required Fields'){
            $create_record = true;
        }
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/EditForm.twig',[
            'custom_settings' => $this->customSettings,
            'form_id' => $this->table.'_table',
            'form_description' => $this->header,
            'fields' => $this->fields,
            'data' =>$field_data,
            'buttons'=>$controls,
            'tabs'=>$categories_tabs,
            'form'=>$form,
            'cnt_tabs'=>$cnt_tabs,
            'controls_bottom_left'=>$this->customSettings['controls_bottom_left'],
            'controls_bottom_right'=>$this->customSettings['controls_bottom_right'],
            'controls_top_left'=>$this->customSettings['controls_top_left'],
            'controls_top_right'=>$this->customSettings['controls_top_right'],
            'create_record'=>$create_record
        ]);
        $editForm = $template_panel->show($template);
        $this->contents = $editForm;
    }
    
    /**
    * @return array
    */
    private function getCategorySetupTabs()
    {
        $categories = array();
        $cats = $this->categories;
        $i=0;
        foreach($cats as $cat){
            $categories[$i]['name'] = $cat;
            $categories[$i]['tab_name'] = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $cat));        
            $i++;
        }
        
        return $categories;
    }
    
    /**
    * @return array
    */
    private function getForm($data)
    {
        $form = array();
        
            
        $cats = $this->categories;
        $i=0;
        //print_r($this->categories);exit;
        foreach($cats as $cat){
            $form[$cat][$i]['name'] = $cat;
            $form[$cat][$i]['tab_name'] = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $cat)); 
            $form_html = array();
           
            if(isset($this->fields[$cat])){
                $field_el = $this->fields[$cat];
                
                    foreach($field_el as $field){
                        //print_r($field);exit;
                        $element_html = $this->handleField($field,$data); 
                        //print_r('this : '. $element_html);exit;
                        $form_html[] = ''.$element_html.'';
                       
                    }
            }

            $form[$cat][$i]['tab_elements'] = $form_html;
            $i++;
        }
        
        //print_r($form);exit;
        
        
        return $form;
    }
       
    /**
    * Handles generation of editing content for a single field (label, control, description / validation)
    *
    * @param $field array
    * @return string
    */
    function handleField($el_field,$data)
    {
        $return = '';
        $editor = new EditorControls();
        $cats = $this->categories;
        $category = '';
        foreach($cats as $cat){
            $category = $cat;
        }
        if(isset($el_field['field_type']) !== 1){

            $field_type = $el_field['field_type'];
            switch($field_type){
                case "hidden" :
                    $result = $editor->handleTextField($el_field,$data);
                    //print_r('hidden');
                    $return = $result['content'];
                break;
                case "text" :
                            
                    if($el_field['field_name'] == 'cycle_id' || $el_field['field_name'] == 'user_id'){
                        $el_field['field_type'] = 'hidden';
                        $result = $editor->handleTextField($el_field,$data);
                    }else{
                        $result = $editor->handleTextField($el_field,$data);
                    }
                    //print_r('text');
                    $return = $result['content'];
                break;
                 case "readonly" :
                    $result = $editor->handleReadonlyField($el_field,$data);
                    //print_r('text');
                    $return = $result['content'];
                break;
                case "textarea" :
                    if($this->table == 'vw_signoff'){
                        $el_field['readonly'] = 'readonly';
                    }
                    $result = $editor->handleTextAreaField($el_field,$data);
                    //print_r('text');
                    $return = $result['content'];
                break;
                case "combo" :
                    if($el_field['field_name'] == 'cycle_id' || $el_field['field_name'] == 'user_id' || $el_field['field_name'] == 'phase_id' || $el_field['field_name'] == 'attended_by' ){
                        $el_field['field_type'] = 'hidden';
                        $result = $editor->handleTextField($el_field,$data);
                    }elseif($this->table = 'employee' && $el_field['field_name'] == 'status_id' ){
                            $result = $editor->handleComboField($el_field,$data,$this->model,$category);    
                    }elseif($this->table = 'cycle_phase_signoffs' && $el_field['field_name'] == 'status_id' ){
                            $el_field['field_type'] = 'hidden';
                        $result = $editor->handleTextField($el_field,$data);
                    }else{
                        $result = $editor->handleComboField($el_field,$data,$this->model);
                    }
                    
                    //print_r('text');
                    $return = $result['content'];
                break;
                case "date" :
                    $result = $editor->handleDateField($el_field,$data);
                    //print_r('text');
                    $return = $result['content'];
                break;
                case "formattednumber" :
                    $result = $editor->handleFormatterField($el_field,$data);
                    //print_r('text');
                    $return = $result['content'];
                break;   
                case "checkbox" :
                    $result = $editor->handleCheckboxField($el_field,$data);
                    //print_r('text');
                    $return = $result['content'];                    
                break; 
                case "number" :
                    $result = $editor->handleNumberField($el_field,$data);
                    //print_r('text');
                    $return = $result['content'];                    
                break;
                case "sectionbreak" :
                    $result = $editor->handleSectionBreakField($el_field,[]);
                    //print_r('text');
                    $return = $result['content'];                    
                break;
                case "lookup" :
                    $result = $editor->handleLookUpField($this->table,$el_field,$data,$category);
                    //print_r('text');
                    $return = $result['content'];                    
                break;
                case "calculation" :
                    $result = $editor->handleCalculationField($el_field,$data);
                    //print_r('text');
                    $return = $result['content'];                    
                break;
                case "ckeditor" :
                    $result = $editor->handleCKEditorField($el_field,$data);
                    //print_r('text');
                    $return = $result['content'];                    
                break;
                default:
                    $return = 's';
            }
        }
        
        return $return;
    }
    
    private function getFormData(){
        $data = array();
        $diff_table_data = array();
        $distinct_table = array();
        $cats = $this->categories;
        foreach($cats as $cat){
            foreach($this->fields[$cat] as $fields){            
                $get_data_table_id = $fields['uni_ref_table_id'];
                if(!in_array($fields['uni_ref_table_id'],$distinct_table)){
                    $distinct_table[] = $fields['uni_ref_table_id'];
                }
                $diff_table_data[$get_data_table_id][] = $fields['field_name'];
            }
        }
        //print_r($diff_table_data);exit;
        //print_r($distinct_table);exit;
        
        /*Get main unique id that will be used to link in tables**/
        $main_table_key =  UniTable::select('search','model','second_table')->where('name', $this->table)->first();   
        $table_keys = $main_table_key->search;
        $this_main_model = "App\\Models\\".$main_table_key->model;
        $second_table = $main_table_key->second_table;
        
        $primary_key = '';
        $secondary_key = '';
        $link_key = '';
        $primary_key_value = '';
        $secondary_key_value = '';
        $link_key_value = '';
        $key_list = explode('/',$table_keys);
        $cnt_key = 0;
        foreach($key_list as $key){
            if($cnt_key <= 0){
                $primary_key = $key;
            }else{
                if(strpos($key, '.') !== false){
                    $key_dat = explode('.',$key);
                    $link_key = $key_dat[0];
                    $secondary_key = $key_dat[1];
                }else{
                    $secondary_key = $key;
                    $link_key = $key;
                }
            }
            $cnt_key++;
        }
        
        foreach($distinct_table as $table){
            //data table
            //print_r($table);
            $field_list = array();
            $data_field_list = $diff_table_data[$table];
            
            $dataTable = UniTable::select('model','name','search')->where('id', $table)->first();
            //print_r($dataTable);
            $data_model = "App\\Models\\".$dataTable->model;
            $table_name = $dataTable->name;
            $this_key = $dataTable->search;
            
            //validate if field belongs to table
            $schema = array();
            $table_meta = TableViewerPanel::getTableStructure($table_name);
            foreach($table_meta as $meta){
                $schema[] = $meta['Field'];
            }
            //print_r($table_meta);exit;
            foreach($data_field_list as $validate_field){
                if(in_array($validate_field,$schema)){
                    $field_list[] = $validate_field;
                }
            }
            //print_r($field_list);exit;
            
            if(!empty($field_list)){
                if($this->table != 'vw_signoff'){
                    if($this->table == $table_name && $table_name != $second_table)
                    {
                        //first get value of the unique key from main table
                        //print_r($this_main_model);
                        //print_r($primary_key);
                        //print_r($this->id_field);
                        //print_r($this->entry_id);
                        $l_key_data = $this_main_model::select($primary_key)
                                ->where($this->id_field, $this->entry_id)    
                                ->first(); 
                        $primary_key_value = $l_key_data->$primary_key;

                        $l_raw_data = $data_model::select($field_list)
                                    ->where($primary_key, $primary_key_value)    
                                    ->first();

                    }else if ($this->table != $table_name && $table_name == $second_table)
                    {
                        //first get value of the unique key from main table
                        $l_key_data = $this_main_model::select($link_key)
                                ->where($this->id_field, $this->entry_id)    
                                ->first(); 
                        $link_key_value = $l_key_data->$link_key;
                        $secondary_key_value = $l_key_data->$link_key;

                        $l_raw_data = $data_model::select($field_list)
                                    ->where($secondary_key, $secondary_key_value)    
                                    ->first();

                    } else if($this->table != $table_name && $table_name != $second_table)
                    {
                        //first get value of the unique key from main table
                        $l_key_data = $this_main_model::select($primary_key)
                                ->where($this->id_field, $this->entry_id)    
                                ->first(); 
                        $primary_key_value = $l_key_data->$primary_key;

                        $l_raw_data = $data_model::select($field_list)
                                    ->where($primary_key, $primary_key_value)    
                                    ->first();
                    }
                }else{
                    $l_raw_data = $data_model::select($field_list)
                        ->where($this->id_field, $this->entry_id)    
                        ->first();
                }
                
                //print_r($l_raw_data);
                if(!empty($l_raw_data)){        
                    foreach($field_list as $column){                                    
                       $data[$column] = $l_raw_data->$column;
                    }
                } 
            }
        }
        //print_r($data);exit;
        return $data;
    }

}


<?php

/* 
 * This specialization of the Panel class to render panel content on
 * xhtml complient browsers.
 * 
 */
namespace App\CFLibrary;

use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;
use App\CFLibrary\InputValidator;
use App\CFLibrary\FieldFormatter;
use Illuminate\Support\Facades\Auth;

class TableCalculationPanel
{
    
    protected $contents;
    protected $header;
    
    function __construct()
    {
        $this->header= null;
        $this->contents= null;
    }
    
    public function getPanel(): array
    {
      $this->generateContents(true);

      return
          [
              'header' => $this->header,
              'content' => $this->contents,
              'footer' => null,
              'id' => null,
          ];
     }

     private function generateContents($showForm = true)
     {

        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/CalculationPanel.twig',[

        ]);
        $dataTable = $template_panel->show($template);
        $this->contents = $dataTable;
         
         

     }
    
}
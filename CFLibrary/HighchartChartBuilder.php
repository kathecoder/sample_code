<?php

/* 
 * This inclusive preprocessed hypertext file hold methods and attributes
 * for automatically viewing a chart in a viewerpanel
 * 
 */

namespace App\CFLibrary;

use App\CFLibrary\Model\UniTable;
use App\CFLibrary\Model\UniFieldRead;
use App\CFLibrary\Model\UniFieldWrite;
use App\CFLibrary\Model\UniFieldType;
use App\CFLibrary\Model\UniPermissionType;
use App\CFLibrary\Model\InformationSchema;

use \App\CFLibrary\PermissionManager;

use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartJsExpr;
use Ghunti\HighchartsPHP\HighchartOption;

class HighchartChartBuilder {
    
    public function __construct(){

    }
    
    
    public function getGenericChart($chart_desc, $chart_title, $sub_heading, $number_type, $data, $cats,$selected_element = NULL , $formatter = FIELD_FORMATTER_NONE,$export = false)
    {
        $chart = new Highchart();

        $chart_id = sprintf('piv_%s', $chart_desc);
        $chart->chart->renderTo = $chart_id;

        $chart->title->text = $chart_title;
        $chart->chart->marginTop = 50;
        $chart->legend->enabled = true;
        $chart->legend->itemStyle = ['fontSize' => '12px'];
        $chart->chart->plotBackgroundColor = null;
        $chart->chart->plotBorderWidth = null;
        $chart->chart->plotShadow = false;
        //$chart->plotOptions->series->lineWidth = 2;
        $chart->yAxis->title->enabled = false;
        $chart->yAxis->labels->enabled = true;
        //$chart->xAxis->labels->x = -25;
        $chart->yAxis->labels->style = ['fontSize' => '12px'];
        $chart->yAxis->min = 0;
        $chart->credits->enabled = false;
        $chart->legend->useHTML = true;
        $chart->colors = ['#004178','#812C35','#0D7153'];
        $chart->subtitle->text = $sub_heading . '<br/><b style="font-size: 14px; font-weight: 400;"></b>';
        
        $chart->xAxis->categories = $cats;
        //$chart->xAxis->categories = $catArray;
        //$chart->xAxis->labels->rotation = -90;

        $chart->plotOptions->series->stacking = 'normal';

        $chart->plotOptions->series->dataLabels->enabled = false;

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
            var text = '';
            text = this.point.name;
            return text;
        } ");
        
        if($graph_type != NULL) {
            $chart->chart->type = $graph_type;
        }else{
            $chart->chart->type = '';
        }
        
        $graph_data = array();
        $i=0;
        foreach ($data as $entity) {

            if($graph_type != NULL && !isset($entity['stack'])) {
                $graph_data[] = array(
                    'name' => $entity['name'],
                    'data' => $entity['data']
                );
            }else{

                if(!isset($entity['type']) && !isset($entity['stack'])){
                    $graph_data[] = array(
                        'name' => $entity['name'],
                        'data' => $entity['data'],
                        'lineWidth' => 0,
                        'marker' => ['enabled' => true,'radius' => 5],
                        'showInLegend' => $entity['showInLegend']
                    );
                }else if(isset($entity['stack'])) {
                    $graph_data[] = [
                        'name' => $entity['name'],
                        'showInLegend' => $entity['showInLegend'],
                        'data' => $entity['data'],
                        'stack' => $entity['stack'],

                    ];
                }else{
                    $graph_data[] = array(
                        'name' => $entity['name'],
                        'data' => $entity['data'],
                        'type' => $entity['type'],
                        'showInLegend' => $entity['showInLegend']
                    );
                }
            }

            $i++;
        }

        foreach ($graph_data as $data) {
            $chart->series[] = $data;
        }

        $chart->exporting->sourceWidth = 1000;
        $chart->exporting->sourceHeight = 500;
        $chart->exporting->allowHTML = false;
        $chart->exporting->scale = 1;

        self::applySimpleLabel($chart, $data);

        if($export){
            //send options back to be put in json file for exporting
            return $chart->renderOptions();
        }else {
            return $this->generateChart($chart, $chart_id, 900, 500, $chart_desc);
        }

    }
    
    private function generateChart(Highchart &$crt, $name, $width, $height, $chart_desc)
    {

        self::finalizeChart($crt, false);
        $js = $crt->render($name);
        //cerr($js);
        $html = <<<HTML
<script>
$(function() {
var {$js}
});
</script>
<div id="{$name}" style="min-width: {$width}px; height: {$height}px; min-height: 300px;margin: 0 auto"></div>
HTML;
        return $html;

    }

    public function configureHighCharts()
    {
        $options = new HighchartOption();
        $options->lang->blowupButtom = 'Blow Up';
        return ('<script>' . Highchart::setOptions($options) . '</script>');
    }
    
    public static function applySimpleLabel(Highchart &$chart, &$data, $numericFormat = '{point.percentage:.1f} %', $showPointName = true)
    {
      if (isset($data['type']) && $data['type'] == 'pie')
      {

        $point = $showPointName ? '<b>{point.name}</b>: ' : '';

        $chart->series[0]->dataLabels = [
            'enabled' => true,
            'format' => $point . $numericFormat,
            'style' => ['fontWeight' => 'bold', 'fontSize' => '8pt'],
            'borderWidth' => 1,
            'borderRadius' => 5,
            'borderColor' => '#AAA',
            'backgroundColor' => 'rgba(252, 255, 197, 0.7)'
        ];
      }
      else
      {
        $chart->series[0]->dataLabels = ['enabled' => true, 'style' => ['fontWeight' => 'bold', 'fontSize' => '8pt'], 'borderWidth' => 1, 'borderRadius' => 5, 'borderColor' => '#AAA', 'backgroundColor' => 'rgba(252, 255, 197, 0.7)'];
      }
    }
    
    public static function applyCombinedLabelFormatter(Highchart &$chart, &$data, $mode = 'percentage', $lineBreak = true, $leftAlign = false, $bottomAlign = true)
    {
      if (!isset($_SESSION['CONTROL_BAR']['SHOW_LABELS'])) return;
      if (!$_SESSION['CONTROL_BAR']['SHOW_LABELS']) return;
      $script = ($mode == 'percentage') ? 'series[i].points[this.point.x].percentage' : 'series[i].yData[this.point.x]';
      $append = ($mode == 'percentage') ? '%' : '';
      $lbAppend = ($lineBreak) ? '<br/>' : ';';
      $formatter = <<<JAVASCRIPT
function()
{
var series = this.point.series.chart.series,length = series.length;
var content = '';
var shortName = '';
var facilityCount = '';
for(var i = 0; i < length; i++)
{
  if(typeof series[i].points !== 'undefined')
  {
    if (typeof series[i].points[this.point.x].meta !== 'undefined')
    {
      facilityCount = series[i].points[this.point.x].meta.replace("Facility Count","F.Cnt")+'{$lbAppend}';
    }
    else
    {
      facilityCount = '';
    }
    shortName = series[i].name.replace("Not Submitted","Not Sub")
    shortName = shortName.replace("Carried Over","C. Over");
    content += shortName;
    content += ':'+Math.round({$script})+'{$append}{$lbAppend}'+facilityCount;
  }
}
return content;
}
JAVASCRIPT;

      $chart->plotOptions->series->dataLabels->formatter = new \Ghunti\HighchartsPHP\HighchartJsExpr($formatter);
      $chart->exporting->width = 1280;
      $chart->exporting->sourceWidth = 1280;

      $size = sizeof($data) - 1;
      $data[$size]['dataLabels'] = ['enabled' => true, 'style' => ['fontWeight' => 'bold', 'fontSize' => '8pt'], 'borderWidth' => 1, 'borderRadius' => 5, 'borderColor' => '#AAA', 'backgroundColor' => 'rgba(252, 255, 197, 0.4)'];
      if ($leftAlign) $data[$size]['dataLabels']['align'] = 'left';
      if ($bottomAlign) $data[$size]['dataLabels']['verticalAlign'] = 'bottom';
    }
    
    public static function finalizeChart(Highchart &$chart, $addPerc = true)
    {

      $chart->chart->style->fontFamily = 'NexaLight,"Open Sans",sans-serif';
      $chart->title->style->fontWeight = 'Bold';
      $chart->credits->enabled = false;
      $chart->exporting->buttons->customButton->onclick = new HighchartJsExpr('function () {$.popUpGraph(this);}');
      $chart->exporting->buttons->customButton->symbol = 'circle';
      $chart->exporting->buttons->customButton->_titleKey = 'blowupButtom';
      //$chart->exporting->url = PathRouter::rootPath() . 'chart_export/index.php';
      if ($addPerc)
      {
        $chart->tooltip->valueSuffix = '%';
        $chart->tooltip->valueDecimals = 2;
        $chart->yAxis->stackLabels->format = "{total:.2f}%";
        $chart->yAxis->labels->format = "{value}%";

        $chart->yAxis->max = 100;
        $chart->yAxis->min = 0;

      }

    } 
    
    public static function generateChart(Highchart &$crt, $name, $width, $height)
    {
      self::finalizeChart($crt, false);
      $js = $crt->render($name);

      $html = <<<HTML
<script>
$(function() {
var {$js}
});
</script>
<div id="{$name}" style="min-width: {$width}px; height: {$height}px; min-height: 300px;margin: 0 auto"></div>

HTML;
        return $html;
      }

    public static function getModalContainer()
    {
    return <<<HTML
<div class="modal fade" id="popup_chart_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:95%;">
    <div class="modal-content">
      <div class="modal-body">
        <div id="popup_chart" style="width:90%;"></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
HTML;

    }
}

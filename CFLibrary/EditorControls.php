<?php

/* 
 * This will create each input type control
 * 
 */
namespace App\CFLibrary;

use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;
use App\CFLibrary\FieldFormatter;
use \App\CFLibrary\CalculationFormatter;
use App\CFLibrary\Model\UniTable;
use App\CFLibrary\ModalWindowController;
use Auth;
use App\User;
use App\Models\InstanceIndicator;
use Illuminate\Support\Facades\Session;
use App\Models\Cycle;
use App\Models\CyclePhases;
use App\Models\Phase;
use App\Http\Controllers\PermissionController;

define('VERTICAL_EDITOR_WIDTH', 25);
define('HORIZONTAL_EDITOR_WIDTH', 26);

class EditorControls
{
    public $consts;
    protected $prepend;
    protected $append;
    protected $cycle_id;
    protected $phase_id;
    protected $instance_id;
    
    
    public function __construct()
    {
        $this->consts = get_defined_constants();
        $this->instance_id      = PermissionController::getUserInstance()['response']['instance'];
        $this->cycle_id         = Cycle::getLiveCycleID();
        $this->phase_id         = Phase::getLivePhaseID();
    }
    
    
    protected static function isFieldReadonly($field)
    {
      $readonlyFields = array('readonly', 'calculation', 'readonlynumber', 'sectionbreak', 'readonlycurrency');
      return (in_array($field['type'], $readonlyFields));
    }
    
    static function handleTextField($field, $data=array())
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        if(isset($data[$field['field_name']])){
            if($field['field_name'] == 'cycle_id' || $field['field_name'] == 'user_id' || $field['field_name'] == 'phase_id'){
                 if($field['field_name'] == 'cycle_id'){$data_value = $this->cycle_id;}
                 if($field['field_name'] == 'user_id'){$data_value = Auth::user()->id;}
                 if($field['field_name'] == 'phase_id'){$data_value = $this->phase_id;}
                // if($field['field_name'] == 'instance_indicator_id'){$data_value = $this->instance;}
            }else{
                $data_value = $data[$field['field_name']];
                $data_value = self::getReferenceFormatter($data_value,$field);
            }
        }
       
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/text.html.twig',[
           'element_field' => $field,
           'field_type' => $field['field_type'],
           'data_value' => $data_value,
           'readonly'   => $field['readonly']
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    static function handleReadonlyField($field, $data=array())
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        if(isset($data[$field['field_name']])){
            $data_value = $data[$field['field_name']];
            $data_value = self::getReferenceFormatter($data_value,$field);
        }
        $field['readonly'] = 'readonly';
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/text.html.twig',[
           'element_field' => $field,
           'field_type' => 'text',
           'data_value' => $data_value,
           'readonly'   => $field['readonly']
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    static function handleTextAreaField($field, $data=array())
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        if(isset($data[$field['field_name']])){
            $data_value = $data[$field['field_name']];
            $data_value = self::getReferenceFormatter($data_value,$field);
        }
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/textarea.html.twig',[
           'element_field' => $field,
           'field_type' => $field['field_type'],
           'data_value' => $data_value,
           'readonly'   => $field['readonly']
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    static function handleSectionBreakField($field, $data=array())
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        if(isset($data[$field['field_name']])){
            $data_value = $data[$field['field_name']];
            $data_value = self::getReferenceFormatter($data_value,$field);
        }
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/sectionbreak.html.twig',[
           'element_field' => $field,
           'field_type' => $field['field_type'],
           'data_value' => $data_value,
           'readonly'   => $field['readonly']
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    static function handleNumberField($field, $data=array())
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        if(isset($data[$field['field_name']])){
            $data_value = $data[$field['field_name']];
            $data_value = self::getReferenceFormatter($data_value,$field);
        }
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/text.html.twig',[
           'element_field' => $field,
           'field_type' => $field['field_type'],
           'data_value' => $data_value,
           'readonly'   => $field['readonly']
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    static function handleCalculationField($field, $data=array())
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        if(isset($data[$field['field_name']])){
            $data_value = $data[$field['field_name']];
            $data_value = self::getReferenceFormatter($data_value,$field);
        }
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/number.html.twig',[
           'element_field' => $field,
           'field_type' => $field['field_type'],
           'data_value' => $data_value,
           'readonly' => 'readonly'
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    static function handleDateField($field, $data=array())
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        if(isset($data[$field['field_name']])){
            $data = explode('-',$data[$field['field_name']]);
            $data_value = date($data[0].'-'.$data[1].'-'.$data[2]);
            $data_value = self::getReferenceFormatter($data_value,$field);
        }
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/date.html.twig',[
           'element_field' => $field,
           'field_type' => $field['field_type'],
           'data_value' => $data_value,
           'readonly'   => $field['readonly']
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    static function handleComboField($field, $data=array(), $model = '', $category = '')
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';
        //print_r('MODEL : '.$model);
        $data_value= '';
        if(isset($data[$field['field_name']])){
            if($field['field_name'] == 'cycle_id' || $field['field_name'] == 'user_id' || $field['field_name'] == 'phase_id'){
                if($field['field_name'] == 'cycle_id'){$data_value = $this->cycle_id;}
                 if($field['field_name'] == 'user_id'){$data_value = Auth::user()->id;}
                 if($field['field_name'] == 'phase_id'){$data_value = $this->phase_id;}
                // if($field['field_name'] == 'instance_indicator_id'){$data_value = $this->instance;}
                 
            }else{
                $data_value = $data[$field['field_name']];
            }
            
        }
        $data_list = [];
        $reference_rule = $field['reference_rule'];
        $js_func = '';
        //print_r($reference_rule);exit;
        if($reference_rule != 'None' && !empty($reference_rule) && $reference_rule != 'NULL'){
            $lookup_model = $field['reference_model'];
            $l_fieldname = $field['reference_field'];
                                
            $l_model = "App\\Models\\".$lookup_model;
            if($lookup_model == 'Status'){
                if($field['uni_table_id'] == 6 && $field['field_name'] == 'status_id'){
                    $user = User::find(Auth::user()->id);
                    if($category == 'Required Fields'){
                        $l_raw_data = $l_model::select($l_fieldname,'id')
                        ->where('status_type',2)
                        ->whereIn('id',[2,3])
                        ->get();
                        if($field['field_name'] == 'status_id'){
                            $js_func = 'onChange="changeRequired();"';
                        }
                    }else{
                        $l_raw_data = $l_model::select($l_fieldname,'id')
                            ->where('status_type',2)
                            ->get();
                        $field['readonly'] = '';
                    }
                }else{
                    $l_raw_data = $l_model::select($l_fieldname,'id')
                        ->where('status_type',1)
                        ->get();
                }
            }else{
                $l_raw_data = $l_model::select($l_fieldname,'id')
                        ->get();
            }
            
            $i=0;
            foreach($l_raw_data as $l_data){
                
                $data_list[$i]['name'] = $l_data->$l_fieldname;
                $data_list[$i]['value'] = $l_data->id;
                $i++;
            }
            //print_r($data_list);exit;
        }else if(!empty($model) && $model != 'NULL'){
            //populate data from table
            $get_table_id = $field['uni_table_id'];
            $get_data_table_id = $field['uni_ref_table_id'];
            if($get_table_id == $get_data_table_id){
                $l_model = "App\\Models\\".$model;
                $l_raw_data = $l_model::select($field['field_name'],'id')
                            ->get();
                 $i=0;
                foreach($l_raw_data as $l_data){

                    $data_list[$i]['name'] = $l_data->$field['field_name'];
                    $data_list[$i]['value'] = $l_data->id;
                    $i++;
                }
            }else{
                $dataTable = UniTable::select('model','name','search')->where('id', $get_data_table_id)->first();
                //print_r($dataTable);
                $data_model = $dataTable->model;
                $l_model = "App\\Models\\".$data_model;
                $l_raw_data = $l_model::select($field['field_name'],'id')
                            ->get();
                 $i=0;
                foreach($l_raw_data as $l_data){
                    print_r($field['field_name']);
                    $data_list[$i]['name'] = $l_data->$field['field_name'];
                    $data_list[$i]['value'] = $l_data->id;
                    $i++;
                }
            }
            
           
        }else{            
            $data_list[]['name'] = 'Select '.$field['field_name'];
            $data_list[]['value'] = '';
        }
        
        //print_r($data_list);//exit;

        /*if(!empty($model) && $model != 'NULL'){
            //populate data from table
            $l_model = "App\\Models\\".$model;
            $l_raw_data = $l_model::select($field['field_name'])
                        ->get();
            
            $i=0;
            foreach($l_raw_data as $l_data){
                
                $data_list[$i]['name'] = $l_data->$field['field_name'];
                $data_list[$i]['value'] = $l_data->$field['field_name'];
                $i++;
            }
        }else{            
            $data_list[]['name'] = 'Select '.$field['field_name'];
            $data_list[]['value'] = '';
        }*/
        
        
        
        //print_r($data_list);exit;
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/combo.html.twig',[
           'element_field' => $field,
           'field_type' => $field['field_type'],
           'data_value' => $data_value,
           'data_list' => $data_list,
           'readonly'   => $field['readonly'],
           'js_func'   => $js_func 
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
        
    }
    
    
    static function handleFormatterField($field, $data=array())
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        $field_format = $field['output_rule'];
        if(isset($data[$field['field_name']])){
            $data_value = $data[$field['field_name']];
            $data_value = self::getReferenceFormatter($data_value,$field); 
        }
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/text.html.twig',[
           'element_field' => $field,
           'field_type' => $field['field_type'],
           'data_value' => $data_value,
           'readonly' => 'readonly'
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    
    static function handleCheckboxField($field, $data=array())
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        if(isset($data[$field['field_name']])){
            $data_value = $data[$field['field_name']];
            $data_value = self::getReferenceFormatter($data_value,$field);
        }
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/checkbox.html.twig',[
           'element_field' => $field,
           'field_type' => $field['field_type'],
           'data_value' => $data_value,
           'readonly'   => $field['readonly']
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    static function handleLookUpField($table_name,$field, $data=array(),$category = '')
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        $data_table= '';
        $data_id = 0;
        $value = '';
        if(isset($data[$field['field_name']])){
            $data_value = $data[$field['field_name']];
            $value = $data[$field['field_name']];
            $data_value = self::getReferenceFormatter($data_value,$field,false);
        }
        
        $data_table = self::getLookupFormatter($table_name,$value,$field);
        if(isset($data['id']) && !empty($data['id'])){
            $data_id = $data['id'];
        }
        
        $validationName = 'return ValidationEvent'.$field['field_name'].'(this)';
        $validationNameTwo = 'ValidationEvent'.$field['field_name'].'(event)';
        $lines = '';
        $variables = '';
        list($children,$identifier) = self::getLookupCalculation($table_name,$value,$field);
        $calculation_script = '';
        //Modal window settings
        $id = 'modal_'.$field['field_name'].'_'.$data_id;
        $label = str_replace(" ","",$field['label']);
        $title = $field['label'];
        $content = $data_table;
        $buttons = array();
        $customsettings = array();
        $customsettings['animation'] = 'fade';
        $customsettings['position'] = 'modal-dialog-centered';
        $customsettings['size'] = 'modal-fluid';
        
        $modal = new ModalWindowController($id, $label, $content, $title, $buttons,'dialog','document', $customsettings, $field['field_name'],$field,$validationName,$validationNameTwo,$value,$children,$identifier);
        $modal_window = $modal->getModal();
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/lookup.html.twig',[
           'element_field' => $field,
            'field_type' => $field['field_type'],
            'data_value' => $data_value,
            'data_table' => $data_table,
            'readonly'   => $field['readonly'],
            'modal'      => $modal_window,
            'calculation_script' => $calculation_script,
            
        ]);
        $html = $template_panel->show($template);
        
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    static function handleCKEditorField($field, $data=array())
    {
        $ret = array(
            'label' => '',
            'description' => '',
            'content' => ''
        );
        $html = '';

        //print_r($field);
        $data_value= '';
        if(isset($data[$field['field_name']])){
            $data_value = $data[$field['field_name']];
            $data_value = self::getReferenceFormatter($data_value,$field);
        }
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/forms/ckeditor.html.twig',[
           'element_field' => $field,
           'field_type' => $field['field_type'],
           'data_value' => $data_value,
           'readonly'   => $field['readonly']
        ]);
        //print_r($template_panel->show($template));
        $html = $template_panel->show($template);
        //print_r($html);//exit;
        $ret ['content'] = $html;

        return $ret;
    }
    
    /**
    * @static
    * @param $field
    * @param $id
    * @param $inputMethod
    * @return string
    */
   static function doLabel($field, $id, $inputMethod)
   {
     $ret = '';
     switch ($inputMethod)
     {
       case INPUT_METHOD_STANDARD:
         $extraClass = 'labelEditable';
         if (self::isFieldReadonly($field))
         {
           $extraClass = 'labelReadonly';
         }
         $ret .= '<td class="label ' . $extraClass . '">';
         $ret .= $field['label'];
         $ret .= '</td>';
         break;
       case INPUT_METHOD_HORIZONTAL:
         $ret = '';
         break;
     }
     return $ret;
   }
    
   static public function getReferenceFormatter($value,$field,$doFormatter = true){
       
       $default_value = $field['default_value'];
       if($default_value != 'None' && !empty($default_value) && $default_value != 'NULL'){
           $value = FieldFormatter::format($value, $default_value);
       }
       
       
       $reference_rule = $field['reference_rule'];
       if($reference_rule != 'None' && !empty($reference_rule) && $reference_rule != 'NULL'){
                              
            $lookup_model = $field['reference_model'];
            //print_r($lookup_model);exit;
            $l_fieldname = $field['reference_field'];
            $l_fields = array();
            //print_r('field_name '.$field['reference_field']);exit;
            $l_fields = explode('/',$l_fieldname);
                                
            $l_model = "App\\Models\\".$lookup_model;
            if($field['reference_model_field'] == NULL){
                                    
                $l_raw_data = $l_model::select($l_fields)
                    ->where($field['field_name'], $value)
                    ->first(); 
                if(!empty($l_raw_data)){
                    $value = '';
                    foreach($l_fields as $l_field){
                        $value .= $l_raw_data->$l_field.' '; 
                    }
                }else{
                    $value = NULL;
                }

            }else{
                $l_raw_data = $l_model::select($l_fields)
                    ->where($field['reference_model_field'], $value)
                    ->first(); 
                if(!empty($l_raw_data)){
                    $value = '';
                    foreach($l_fields as $l_field){
                        $value .= $l_raw_data->$l_field.' '; 
                    }
                }else{
                    $value = NULL;
                } 
            }
            //print_r($value);//exit;
        }
        $field_format = $field['output_rule'];
        
        if($field_format != 'None' && !empty($field_format) && $field_format != 'NULL' && $doFormatter){
            //print_r($field_format);
            $value = FieldFormatter::format($value, $field_format);
        } 
       
       return $value;
   } 
    
   static public function getLookupFormatter($table_name,$value,$field){
       
        $field_format = $field['output_rule'];
        if($field_format != 'None' && !empty($field_format) && $field_format != 'NULL'){
            $vals['value'] = $value;
            $vals['field'] = $field;
            $vals['table_name'] = $table_name;
            $value = FieldFormatter::format($vals, $field_format);
        } 
       
        return $value;
        
   }
   
   static public function getLookupCalculation($table_name,$value,$field){
        $script = '';
        $lines = '';
        $variables = '';
        $field_calculation = $field['calculation_rule'];
        if($field_calculation != 'None' && !empty($field_calculation) && $field_calculation != 'NULL'){
            $vals['value'] = $value;
            $vals['field'] = $field;
            if($table_name == 1){
                $table_name = 'employees';
            }
            $vals['table_name'] = $table_name;
            /*print_r('value : ');
            print_r($value);
            print_r('field : ');
            print_r($field);
            print_r('table_name : ');
            print_r($table_name);*/
            list($children,$identifier) = CalculationFormatter::format($vals, $field_calculation,$table_name);
        } 
       
        return [$children,$identifier];
        
   }
   
}
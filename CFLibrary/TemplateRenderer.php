<?php
/**
 */
namespace App\CFLibrary;

use Twig_Environment;
use Twig_Extensions_Extension_Date;
use Twig_Function;
use Twig_Loader_Filesystem;
use App\CFLibrary\FieldFormatter;

class TemplateRenderer
{
  protected $location;
  /**
   * @var bool
   */
  protected $debugMode;

  /**
   * @var Twig_Environment
   */
  protected $twig;

  /**
   * @param null|string[] $location Path to search for the template file
   */
  public function __construct($location = null)
  {
    $this->debugMode = env('APP_DEBUG');
    if ($location === null)
    {
      $location = self::getDefaultTemplateLocations();
    }
    $this->location = $location;
  }

  public static function getDefaultTemplateLocations()
  {
    //return [ROOT_PATH . 'views' . DIRECTORY_SEPARATOR, realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR];
      $view_path = env('ASSET_URL'). 'views';
      return $view_path;
  }

  /**
   * Check if the supplied template can be found in one of the locations -
   *
   * @param $template
   * @return bool|string Location of template or false if not found
   */
  public function templateExists($template)
  {
    foreach ($this->location as $location)
    {
      if (is_readable($location . $template)) return $location;
    }

    return false;
  }

  protected function initializeTwig()
  {
    $loader = new Twig_Loader_Filesystem($this->location);
    $this->twig = new Twig_Environment($loader,
        [
            'cache' => 'twig',
            'auto_reload' => true,
            'debug' => $this->debugMode
        ]);
    if (class_exists('Twig_Extensions_Extension_Date'))
    {
      $this->twig->addExtension(new Twig_Extensions_Extension_Date());
    }


    $function = new \Twig_SimpleFunction('field_format', function (...$params) {

      return FieldFormatter::format(...$params);
    });
    $this->twig->addFunction($function);

    $function = new \Twig_SimpleFunction('url', function ($name, $params = null) {
      //return PathRouter::getPathForRoute($name, $params);
      //return redirect($name)->with($params);
      return redirect()->route($name, $params);
    });
    $this->twig->addFunction($function);

    if ($this->debugMode) $this->twig->addExtension(new \Twig_Extension_Debug());

  }

  public function render($template, $params)
  {
    if ($this->twig === null) $this->initializeTwig();

    /*if (!isset($params['image_path'])) $params['image_path'] = $_ENV['SITE_VARS']['IMAGE_PATH'];
    if (!isset($params['root_path'])) $params['root_path'] = $_ENV['SITE_VARS']['ROOT_PATH'];
    if (!isset($params['site_manager'])) $params['site_manager'] = $_ENV['SITE_VARS']['SITE_MANAGER'];
    if (!isset($params['currentPageBasePath'])) $params['currentPageBasePath'] = PathRouter::currentPageBasePath();
    if (!isset($params['currentPagePath'])) $params['currentPagePath'] = PathRouter::currentPagePath();*/

    return $this->twig->render($template, $params);
  }

  /**
   * @return bool
   */
  public function getDebugMode()
  {
    return $this->debugMode;
  }

  /**
   * @param bool $debugMode
   */
  public function setDebugMode($debugMode)
  {
    $this->debugMode = $debugMode;
  }

}

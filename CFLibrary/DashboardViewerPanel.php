<?php

/* 
 */

namespace App\CFLibrary\Model;




class DashboardViewerPanel {

    public function __construct()
    {
        
    }        
    
    public function show():array
    {
        $panel = $this->getViewer();
        if ($panel !== null)
        {
          return $panel->getPanel();
        }

        return null;
    }

    /**
    * This will return the panel to be added to template file
    */
    private function getViewer()
    {
        if (true){
            //Builds the editable form
            $customSettings = [];
            $panel = new ViewerEditPanel();
            //print_r($panel);exit;
            return $panel;
        }
        
        return null;
    }
    
}

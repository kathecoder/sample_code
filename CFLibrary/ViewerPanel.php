<?php

/* 
 * This specialization of the Panel class to render panel content on
 * xhtml complient browsers.
 * 
 */
namespace App\CFLibrary;

use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;
use App\CFLibrary\InputValidator;
use App\CFLibrary\FieldFormatter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ViewerPanel
{
  const TVP_FILTERS_PER_ROW = 3;
  const EMPTY_TABLE_ID = '53EA782C-0C3A-424C-8A54-007F9139EF6F';
  public $searchTitle;
  public $filtersHideable;
  public $filtersHidden;
  public $filtersPerRow;
  /**
   * The properties of the data to display.
   *
   * @var array
   * @access public
   */
  var $fields;
  /**
   * The tables from which to get the data to display.
   *
   * @var string
   * @access public
   */
  var $tables;
  var $where;
  protected $table;
  protected $customSettings;

  protected $key;
  /**
   * @var int
   */
  protected $count;
  protected $contents;
  protected $header;
  protected $model;
  protected $lookup;
  public $table_id_set;
  
  function __construct($table,$title, $fields, $where,$model, $customSettings,$table_id_set,$lookup)
  {
   
    $this->table = $table;
    $this->contents = '';
    $this->header = $title;
    $this->lookup = $lookup;
    $this->key = $customSettings['key'] ?? '';
    $this->fields = $fields;
    $this->tables = $table;
    $this->where = $where;
    
    $this->model = $model;
    $this->table_id_set = $table_id_set;

    $this->customSettings = [
        'controls' => null,
        'searchFields' => null,
        'size' => 12,
        'useStartEndButtons' => false,
        'getParameters' => '',
        'current_page' => '',
        'searchTitle' => '',
        'hideFieldHeaders' => false,
        'filtersHidden' => false,
        'filtersHideable' => false,
        'hideNavigation' => false,
        'filtersPerRow' => self::TVP_FILTERS_PER_ROW,
        'initialSortOrder' => 'ASC',
        'initialSortField' => null,
        'removeSelectAll' => false,
        'footer' => '',
        'preFooter' => '',
        'header' => '',
        'bottomNavigation' => true
    ];

    foreach ($customSettings as $key => $value)
    {
      $this->customSettings[$key] = $value;
    }

  }
  
  public function getPanel(): array
  {
    $this->generateContents(true);

    return
        [
            'header' => $this->header,
            'content' => $this->contents,
            'footer' => null,
            'id' => null,
        ];
   }
   
   private function generateContents($showForm)
   {
       
        $pre = '';
        $post = '';

        //no actions added yet to data except formatting
        $data = $this->getTableData();
        
        //assign special actions to td elements
        $i=0;
        $id_col_index = $this->getField("id");        
        foreach($data as $dataSubSet){
            
            $fieldIndex = 0;
            
            //Get id of element - this will be used as unique identifier
            $row_id = null;
            if ($id_col_index !== false)
            {
              $row_id = $this->arr_get_n($dataSubSet, $id_col_index);
            }
            
            $data[$i]['checkID'] = $row_id;
            
            //this to used to add aditional formatting and condistions to each data element
            /*******incomplete*********/
            foreach ($dataSubSet as $datum)
            {
                                              
            }
            $i++;
            //print_r($row_id);            
        }
        
        //print_r("index ; ".$id_col_index);exit;
        //print_r($this->customSettings);
        $controls = $this->customSettings['controls'];
        
        if(!isset($this->table_id_set) || empty($this->table_id_set) || $this->table_id_set == ''){
            $this->table_id_set = $this->table.'_table';
        }
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/DataTable.twig',[
            'current_route' => $this->customSettings['current_route'],
            'custom_settings' => $this->customSettings,
            'table_id' => $this->table_id_set,
            'table_description' => $this->header,
            'fields' => $this->fields,
            'data' =>$data,
            'buttons'=>$controls,
            'table_id_set' =>$this->table_id_set,
            'lookup' => $this->lookup,
        ]);
        $dataTable = $template_panel->show($template);
        $this->contents = $dataTable;
        
       
   }
   
    /**
    * @return array
    */
    private function getTableData()
    {
        /*Build the data table*/
          /* select the data to display from the database */
          $pre_data = array();
          $row_cnt=0;
          foreach ($this->fields as $field)
          {          
             
              if(!empty($field['fieldname']) && $field['show']){

                  $model = "App\\Models\\".$this->model;
                  //added filter wherein clause
                  //print_r(FilterHandler::getFilteredData($this->table, Auth::user()->id));
                  //$filters = Session::get('custom_filtered_ids');
                  //print_r($this->table);
                  
                  $query = $model::select($field['fieldname']);

                    if(count($this->where) > 0){
                        foreach($this->where as $where){
                            foreach($where as $column => $value)
                            {
                                if (is_array($value) == 1 ){
                                    if (count($value) > 0){
                                        $query =   $query->whereIn($column, $value);
                                    }
                                }
                                else{
                                    $query =   $query->where($column, '=', $value);
                                }

                            }
                        }
                    }
                  $query =   $query->whereIn('id', FilterHandler::getFilteredData($this->table, Auth::user()->id));
                  $raw_data = $query->get();
                  //$raw_data = $model::select($field['fieldname'])->whereIn('id', FilterHandler::getFilteredData($this->table, Auth::user()->id))->get();

                  $field_format = $field['format'];
                  $reference_rule = $field['reference'];
                  $i=0;
                  foreach($raw_data->toArray() as $key=>$val)
                  {                   
                      foreach($val as $fieldId=>$value){
 
                          if($reference_rule != 'None' && !empty($reference_rule) && $reference_rule != 'NULL'){
                              
                            $lookup_model = $field['reference_model'];
                            //print_r($lookup_model);exit;
                            $l_fieldname = $field['reference_field'];
                            $l_fields = array();
                            //print_r('field_name '.$field['reference_field']);exit;
                            $l_fields = explode('/',$l_fieldname);
                                
                            $l_model = "App\\Models\\".$lookup_model;
                            if($field['reference_model_field'] == NULL){
                                    
                                    $l_raw_data = $l_model::select($l_fields)
                                                    ->where($field['fieldname'], $value)
                                                    ->first(); 
                                    if(!empty($l_raw_data)){
                                        $value = '';
                                        foreach($l_fields as $l_field){
                                            $value .= $l_raw_data->$l_field.' '; 
                                        }
                                    }else{
                                        $value = NULL;
                                    }

                            }else{
                                $l_raw_data = $l_model::select($l_fields)
                                                    ->where($field['reference_model_field'], $value)
                                                    ->first(); 
                                    if(!empty($l_raw_data)){
                                        $value = '';
                                        foreach($l_fields as $l_field){
                                            $value .= $l_raw_data->$l_field.' '; 
                                        }
                                    }else{
                                        $value = NULL;
                                    } 
                            }
                          //print_r($value);exit;
                          }
                          if($field_format != 'None' && !empty($field_format) && $field_format != 'NULL'){
                            $value = FieldFormatter::format($value, $field_format);
                          }   
                             
                          $value = str_replace(":","-",$value);
                          $value = str_replace("&","and",$value);
                          $pre_data[$fieldId][$i] = $value;
                      }
                      $i++;
                  }
                  $row_cnt = $i;
              }      
             
          }

          $data = array();
          for($j=0;$j < $row_cnt;$j++){
              foreach ($this->fields as $field)
              { 
                  if(!empty($field['fieldname']) && $field['show']){
                      $data[$j][$field['fieldname']] = $pre_data[$field['fieldname']][$j];               
                  }
              }
          }
          
          return $data;
    }
   
  public static function queryAlias()
  {
    return ' as /*B9E9B815F68D*/ ';
  }
  
   /**
   * @param $fieldName
   * @return bool|int
   */
  function getField($fieldName)
  {
    $cnt = 0;
    foreach ($this->fields as $field)
    {
      if ($field['fieldname'] == $fieldName)
      {
        return $cnt;
      }
      $cnt++;
    }

    return false;
  }

  /**
   * @param $arr
   * @param $index
   * @return mixed
   */
  function arr_get_n($arr, $index)
  {
    $cnt = 0;
    foreach ($arr as $item)
    {
      if ($cnt == $index)
        return $item;
      $cnt++;
    }

    return null;
  }
  
}
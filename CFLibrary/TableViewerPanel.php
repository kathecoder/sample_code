<?php

/* 
 * This inclusive preprocessed hypertext file hold methods and attributes
 * for automatically viewing a table in a viewerpanel
 * 
 */
namespace App\CFLibrary;

use App\CFLibrary\Model\UniTable;
use App\CFLibrary\Model\UniFieldRead;
use App\CFLibrary\Model\UniFieldWrite;
use App\CFLibrary\Model\UniFieldType;
use App\CFLibrary\Model\UniPermissionType;
use App\CFLibrary\Model\InformationSchema;

use \App\CFLibrary\PermissionManager;

use App\Models\AppExport;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Cycle;
use Auth;
use App\User;
use App\Models\InstanceIndicator;
use App\Http\Controllers\PermissionController;

use App\Models\Audit;
use App\Models\Phase;


define('MAX_TEP_FIELD_COUNT', 30); //the number of records required before an extra navigation (cancel, save) is added to the top

define('INPUT_METHOD_STANDARD', 0); // Need to add a function to InputValidator
define('INPUT_METHOD_HORIZONTAL', 1); // Need to add a function to InputValidator


class TableViewerPanel
{
  /**
   * Panel Settings
   *
   * @var array
   */
  public $settings;
  public $form_data;

  /*::::NOTE:::: Need to move settings to its own method as construct as too many actions*/
  /**
   * TableViewerPanel Constructor
   *
   * @param string $table The name of the table to view
   * @param        $permissions string The permissions for the viewer (String containing one of the following characters CRUDS)
   * @param string $table/form states that will use different methods and functions to action form submit
   * @param array $selection(checkboxes)/form(inputs) any variables sent to form through request/post
   *                to be used as where unique keys/ later to be added to CRUD handling
   * @param string $where [Optional] An additional SQL clause to restrict the results from the table
   * @param array  $customSettings
   * @param const  used to format from elements from input validator
   */
  public function __construct($table, $permissions, $react_state = 'start', $selections = array(), $form_submit_data = array(),$edit_table = NULL, $view_type = 'vertical', $where = array(), $table_id_set = '', $lookup = false, $customSettings = array(), $inputMethod = INPUT_METHOD_STANDARD)
  {
    $this->settings = array();
    $this->form_data = $form_submit_data;
    $this->settings['table_id_set'] = $table_id_set;
    //First set all permissions to false
    $this->settings['permissions'] = array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'search' => false, 'export' => false, 'import' => false, 'expunge' => false);
    //parse permissions string
    $permissions = strtoupper($permissions);
    if (strpos($permissions, 'C') !== false) $this->settings['permissions']['create'] = true;
    if (strpos($permissions, 'R') !== false) $this->settings['permissions']['read'] = true;
    if (strpos($permissions, 'U') !== false) $this->settings['permissions']['update'] = true;
    if (strpos($permissions, 'D') !== false) $this->settings['permissions']['delete'] = true;
    if (strpos($permissions, 'S') !== false) $this->settings['permissions']['search'] = true;
    if (strpos($permissions, 'X') !== false) $this->settings['permissions']['export'] = true;
    if (strpos($permissions, 'I') !== false) $this->settings['permissions']['import'] = true;
    if (strpos($permissions, 'E') !== false) $this->settings['permissions']['expunge'] = true;

    //Get the table view, this will indicate to use datatable or ag-grid
    $this->settings['view_type'] = $view_type;
    
    $mode = $this->checkMode($react_state);
    if ($mode == 'read') $_SESSION['tvp']['mode'] = 'read';
    $this->settings['mode'] = $mode;
    
    $this->settings['lookup'] = $lookup;

    /* standard settings */
    $this->settings['template_base'] = '';
    $this->settings['current_page'] = '';
    $this->settings['current_route'] = '';
    $this->settings['image_path'] = '';
    $this->settings['table'] = $table;
    $this->settings['key'] = $table;
    $this->settings['getparameters'] = '3A517F4D=A7A6D898C36E';
    if($edit_table != NULL){
        $this->settings['edit_table'] = $edit_table;
    }else{
        $this->settings['edit_table'] = $table;
    }
    $this->settings['model'] = null;
    $this->settings['controller'] = null;
    $this->settings['imports'] = null;
    $this->settings['edit_model'] = null;
    $this->settings['edit_controller'] = null;
    $this->settings['edit_imports'] = null;
    $this->settings['edit_map'] = null;
    $this->settings['primarytable'] = $table;
    $this->settings['filterexport'] = true;
    $this->settings['controls'] = null;
    $this->settings['controls_top_left'] = null;
    $this->settings['controls_top_right'] = null;
    $this->settings['controls_bottom_left'] = null;
    $this->settings['controls_bottom_right'] = null;
    
    /*Response Settings*/
    $this->settings['status'] = null;
    $this->settings['message'] = null;
    
    
    
    /*Need to add this method in array format so you can loop through items and attach to eloquent object*/
    $this->settings['where'] = $where;
    $this->settings['initialwhere'] = $where;


    foreach ($customSettings as $cKey => $cVal)
    {
        $this->settings[$cKey] = $cVal;
    }



    /*On read show data table*/
    if($this->settings['mode'] == 'read' &&  $this->settings['view_type'] == 'vertical'){

        $this->settings['filtersperrow'] = null;
        $this->settings['filtershidden'] = null;
        $this->settings['filtershideable'] = null;
        $this->settings['usestartendbuttons'] = false;
        $this->settings['resultcount'] = 20;
        $this->settings['filterexport'] = false;


        $this->buildViewerArray();
    }

     /*On edit,save or create show edit form*/
    if(($this->settings['mode'] == 'edit' || $this->settings['mode'] == 'create' || $this->settings['mode'] == 'save')){
        //set id of record - column can be defined in array $selections passed
        if($this->settings['mode'] == 'create'){
            $this->settings['id_field'] = 'id';
            $this->settings['id'] = '*';
        }elseif($this->settings['mode'] == 'save'){
            //print_r($this->settings['mode']);
            //print_r($this->form_data);
            if(array_key_exists('id',$this->form_data)){
                $this->settings['id_field'] = 'id';
                $this->settings['id'] = $this->form_data['id'];
            }else{
                $this->settings['id_field'] = 'id';
                $this->settings['id'] = '*';
            }
   
        }else{
            //any field name specified which is the unique id for data
            foreach($selections as $key=>$value){
                $this->settings['id_field'] = $key;
                $this->settings['id'] = $value;
            }
        }
        $this->settings['scroller'] = false;
        $this->settings['categories'] = [];
                
        $this->settings['controls_top_right'][0]['name'] = "Close";
        $this->settings['controls_top_right'][0]['value'] = str_pad('Save and Close', 8, ' ', STR_PAD_BOTH);
        if($this->settings['view_type'] == 'vertical'){
            $this->settings['controls_top_right'][0]['onclick'] = "document.getElementById('mode').value = 'close';";
        }else{
            $this->settings['controls_top_right'][0]['onclick'] = "document.getElementById('mode').value = 'close_hor';";
        }
        
        $this->settings['controls_bottom_left'][0]['name'] = "Cancel";
        $this->settings['controls_bottom_left'][0]['value'] = str_pad('Cancel', 8, ' ', STR_PAD_BOTH);
        if($this->settings['view_type'] == 'vertical'){
            $this->settings['controls_bottom_left'][0]['onclick'] = "document.getElementById('mode').value = 'read';";
        }else{
            $this->settings['controls_bottom_left'][0]['onclick'] = "document.getElementById('mode').value = 'close_hor';";
        }
        if($this->settings['edit_table'] != 'vw_signoff'){
            if ($this->settings['permissions']['create'] || $this->settings['permissions']['update']) {
                $this->settings['controls_bottom_right'][0]['name'] = "Save";
                $this->settings['controls_bottom_right'][0]['value'] = str_pad('Save', 8, ' ', STR_PAD_BOTH);
                $this->settings['controls_bottom_right'][0]['onclick'] = "document.getElementById('mode').value = 'save';";
            }
        }
        
        
        $this->buildEditorArray($inputMethod);
        
        if($this->settings['mode'] == 'save'){
            //print_r($this->form_data);
            if(array_key_exists('id',$this->form_data) && $this->settings['id'] != ''){               
                list($status,$message) = $this->updateRecord();
                $this->settings['status'] = $status;
                $this->settings['message'] = $message;
            }else{               
                list($status,$message) = $this->createRecord();
                $this->settings['status'] = $status;
                $this->settings['message'] = $message;
            }
   
        }
    }
    
    if(($this->settings['mode'] == 'read') &&  $this->settings['view_type'] == 'horizontal')
    {
        $this->settings['categories'] = [];
        
        $this->settings['controls'][0]['name'] = "Cancel";
        $this->settings['controls'][0]['value'] = str_pad('Cancel', 8, ' ', STR_PAD_BOTH);
        $this->settings['controls'][0]['onclick'] = "document.getElementById('mode').value = 'read';";
 
        if ($this->settings['permissions']['create'] || $this->settings['permissions']['update']) {
            $this->settings['controls'][1]['name'] = "Save";
            $this->settings['controls'][1]['value'] = str_pad('Save', 8, ' ', STR_PAD_BOTH);
            $this->settings['controls'][1]['onclick'] = "document.getElementById('mode').value = 'save';";
        }
        
        $this->buildEditorArray($inputMethod);
        
        
    }
      
    
    
    /*On export data table*/
    /* had to rework the where clause, sorry if this was important
      if($this->settings['mode'] == 'export'){
        logger($this->settings);
        if ($this->settings['filterexport'] === true){

              $exportWhere =  $this->settings['where'];
              logger($exportWhere);
              $pos = strpos($exportWhere, ' WHERE ');
              $exportWhere = substr($exportWhere, $pos + 7);
          }
        else
          {
              $exportWhere = $this->settings['initialwhere'];
          }
      }
    */


  }

  /**
   * Build the editor array along with other settings - This settings array can be changed in the client code after the call to the constructor
   *
   * @param $editorMode
   */
  function buildEditorArray($editorMode)
  {
    //Table settings - check if table different than edit table - use edit table as main for forms
    if ($this->settings['table'] != $this->settings['edit_table'])
    {
          $this->settings['table'] = $this->settings['edit_table'];
    }

    $table_name = $this->settings['table'];
    $table_name = ltrim($table_name, '#');
    //print_r($tables->name);
    $tables = UniTable::where('name', $table_name)->first();
    //print_r($tables->name);

    //when table data is loaded
    //TITLE OF FORM
    
    $this->settings['model'] = $tables->model;
    
    
    if($this->settings['view_type'] != 'horizontal'){
        $this->settings['description'] = $tables->description;   
        $description = $this->settings['description'];
        if($this->settings['edit_table'] != 'vw_signoff'){
            $header = 'Edit';
        }else{
             $header = 'View';
        }
        if ($this->settings['id'] == '*' || $this->settings['id'] == 0) $header = 'Create New ';
        $this->settings['title'] =  $description.' - '.$header;

        if ($this->settings['title'][strlen($this->settings['title']) - 1] == 's')
        {
          $this->settings['title'] = substr($this->settings['title'], 0, -1);
        }
    }else if($this->settings['mode'] == 'edit' && $this->settings['view_type'] == 'horizontal'){
        $this->settings['description'] = $tables->description;   
        $description = $this->settings['description'];
        if($this->settings['edit_table'] != 'vw_signoff'){
            $header = 'Edit';
        }else{
             $header = 'View';
        }

        if ($this->settings['id'] == '*' || $this->settings['id'] == 0) $header = 'Create New ';
        $this->settings['title'] =  $description.' - '.$header;

        if ($this->settings['title'][strlen($this->settings['title']) - 1] == 's')
        {
          $this->settings['title'] = substr($this->settings['title'], 0, -1);
        }
    }else{
        $this->settings['description'] = $tables->description;      
    }
    
    if($this->settings['view_type'] == 'horizontal'){
        //Permission control settings
        $this->settings['controls'] = array();

        if ($this->settings['permissions']['update']) {
            $this->settings['controls'][0]['name'] = $this->settings['key'] . "update";
            $this->settings['controls'][0]['value'] = str_pad('Update', 8, ' ', STR_PAD_BOTH);
            $this->settings['controls'][0]['onclick'] = "document.getElementById('mode').value = 'update';";
        }
        
        if ($this->settings['permissions']['export']){
            $this->settings['controls'][1]['name'] = $this->settings['key'] . "export";
            $this->settings['controls'][1]['value'] = str_pad('Export Data ...', 8, ' ', STR_PAD_BOTH);
            $this->settings['controls'][1]['onclick'] = "document.getElementById('mode').value = 'export';";
        }

        
    }
    
    

    $this->settings['tableid'] = $tables->id;
    $table = $this->settings['table'];
    $tableid = $this->settings['tableid'];
    $metas = $this->getTableStructure($table);

    $hasCats = false;
    $cats = array();

    //Get specific permission array
    $permArr = PermissionController::getFormPermissions($this->settings['tableid'], $this->settings['table']);
    //$permArr = PermissionManager::getFormPermissions($this->settings['tableid'], $this->settings['table']);
    $allowed_cats = array();
    $user       = User::find(Auth::user()->id);
    $account_type = $user->accounttype;
    if(!empty($permArr)){
        foreach($permArr as $permission_column){
            
            if($account_type == 4){
                if($permission_column['uni_permissiontype_id'] == 2 || $permission_column['uni_permissiontype_id'] == 3){
                    $allowed_cats[] = $permission_column['field_name'];
                }   
            }else{
                $allowed_cats[] = $permission_column['field_name'];
            }
        }
    }
    
    //print_r($allowed_cats);
    if($this->settings['view_type'] == 'horizontal'){
        $categories = UniFieldWrite::select('category')
                ->where('uni_table_id', $this->settings['tableid'])
                ->where('omithorizontal', 0)
                ->whereIn('field_name', $allowed_cats)
                ->distinct()
                ->get();

        $i = 0;
        foreach($categories as $cat){
            $cats[$i] = $cat->category;
            //print_r($cat->category);
            $i++;
        }
        if (!empty($cats)) $hasCats = true;
    }else
    {
        $categories = UniFieldWrite::select('category')
                ->where('uni_table_id', $this->settings['tableid'])
                ->distinct()
                ->get();

        $i = 0;
        foreach($categories as $cat){
            $cats[$i] = $cat->category;
            //print_r($cat->category);
            $i++;
        }
        if (!empty($cats)) $hasCats = true;
    }

    
    //print_r($permArr);
    //if ($hasCats)
    //{
        //validates categories against the permissions setup
        $cats = $this->validateCategories($tableid, $metas, $cats, $permArr);

        if($this->settings['view_type'] != 'horizontal'){
            if ($this->settings['id'] == '*' || $this->settings['id'] == 0) // create new record
            {
              $reqs = UniFieldWrite::all()
                ->where('uni_table_id', $this->settings['tableid'])
                ->whereIn('required', [1,21])
                ->count();
               //print_r('count : '.$reqs);
              if (intval($reqs) > 0) //we have required fields in categories.Do not show all cats(reset cats)
              {
                $cats = array('Required Fields');
              }
            }
        }else if($this->settings['view_type'] == 'horizontal' && $this->settings['mode'] == 'edit'){
            if ($this->settings['id'] == '*' || $this->settings['id'] == 0) // create new record
            {
              $reqs = UniFieldWrite::all()
                ->where('uni_table_id', $this->settings['tableid'])
                ->whereIn('required', [1,21])
                ->count();
               //print_r('count : '.$reqs);
              if (intval($reqs) > 0) //we have required fields in categories.Do not show all cats(reset cats)
              {
                $cats = array('Required Fields');
              }
            }
        }

    //}
    $cats = array_unique($cats);
    $this->settings['categories'] = $cats;
    $this->settings['metas'] = $metas;
    //Get field specifications
    foreach($cats as $cat){
        //print_r($cat);
        $fields = array();
        if($cat == 'Required Fields'){
            $fields = UniFieldWrite::where('uni_table_id', $this->settings['tableid'])
            ->whereIn('required', [1,21])
            ->orderBy('order', 'asc')
            ->get();
        }else{
            $fields = UniFieldWrite::where('uni_table_id', $this->settings['tableid'])
            ->where('category', $cat)
            ->orderBy('order', 'asc')
            ->get();
        }

        $perm_types = array();
        $permission_types = UniPermissionType::all();
        foreach($permission_types as $p_types){
            $perm_types[$p_types->id] = $p_types->code;
        }
        
        if(!empty($fields)){
            $i=0;
            
            foreach($fields as $field){
                $field_write = array();
                $field_write['label'] = $field->label;
                if($cat == 'Required Fields'){
                    $usageCode = 'W';
                    if(isset($permArr[$field->field_name]) && !empty($permArr[$field->field_name]))
                    {                        
                        $new_label = $permArr[$field->field_name]['new_field_label'];
                        if(!empty($new_label)){
                             $field_write['label'] = $permArr[$field->field_name]['new_field_label'];
                        }
                    }
                }else{
                    $usageCode = 'W';
                    $user       = User::find(Auth::user()->id);
                    $account_type = $user->accounttype;
                    if(isset($permArr[$field->field_name]) && !empty($permArr[$field->field_name]))
                    {
                        
                        $this_field_perm = $permArr[$field->field_name];
                        //print_r('name '.$permArr[$field->field_name]['new_field_label']);
                        if($this->settings['edit_table'] == 'vw_signoff'){
                            
                            $usageCode = 'R';
                        }else{
                            if($account_type == 4){
                               $permissiontype_id = $this_field_perm['uni_permissiontype_id']; 
                            }else{
                               $permissiontype_id = $this_field_perm['uni_permissiontype_id']; 
                               if($permissiontype_id == 1){
                                    $usageCode = $perm_types[$permissiontype_id];
                                }else{
                                    $usageCode = 'W';
                                }
                            }
                        }
                        
                        
                        $new_label = $permArr[$field->field_name]['new_field_label'];
                        if(!empty($new_label)){
                             $field_write['label'] = $permArr[$field->field_name]['new_field_label'];
                        }
                        $usageCode = $perm_types[$permissiontype_id];
                    }
                }
                
                $field_write['label'] = str_replace("%(ZAR)%","",$field_write['label']);
                if($field->field_name == 'id'){
                    $usageCode = 'R';
                }
                //print_r($usageCode);
                if($usageCode != 'H'){
                
                
                $field_write['id'] = $field->id;
                $field_write['uni_table_id'] = $field->uni_table_id;
                $field_write['uni_ref_table_id'] = $field->uni_ref_table_id;
                $field_write['field_name'] = $field->field_name;
                $field_write['field_id'] = $field->field_id;
                
                $field_write['description'] = $field->description;
                $field_write['uni_fieldtype_id'] = $field->uni_fieldtype_id;
                /*Get fieldtype from dbo*/
                $field_type = UniFieldType::select('name')
                    ->where('id', $field->uni_fieldtype_id)
                    ->first();
                
                $field_write['field_type'] = $field_type->name;
                
                if($cat == 'Required Fields'){
                    $field_write['readonly'] = '';
                }else{
                    if($usageCode == 'R'){
                        $field_write['readonly'] = 'readonly';
                    }else{
                         $field_write['readonly'] = '';
                    }
                }
                
                
                if($cat == 'Required Fields'){
                    $field_write['required'] = $field->required;
                }else{
                    if($field->required == 21){
                        $field_write['required'] = 1;
                    }else{
                        $field_write['required'] = $field->required;
                    }
                }
                $field_write['default_value'] = $field->default_value;
                $field_write['category'] = $field->category;
                $field_write['sub_category'] = $field->sub_category;
                $field_write['order'] = $field->order;
                $field_write['default_value'] = $field->default_value;
                $field_write['output_rule'] = $field->output_rule;
                $field_write['validation_rule'] = $field->validation_rule;
                $field_write['reference_rule'] = $field->reference_rule;
                $field_write['required_rule'] = $field->reference_rule;
                $field_write['calculation_rule'] = $field->calculation_rule;
                $field_write['calc_parent_id'] = $field->calc_parent_id;
                $field_write['freeze_col'] = $field->freeze_col;
                $field_write['omitstandard'] = $field->omitstandard;
                $field_write['omithorizontal'] = $field->omithorizontal;
                $field_write['htep_sort'] = $field->htep_sort;
                $field_write['htep_width'] = $field->htep_width;
                $field_write['htep_filter'] = $field->htep_filter;
                $field_write['htep_resize'] = $field->htep_resize;
                $field_write['htep_group_show'] = $field->htep_group_show;
                $field_write['htep_rendering'] = $field->htep_rendering;
                                            
                //print_r($field->freeze_col);
                //when reference is looking for data in another table eg.lookup
                if (!empty($field->reference_rule) && $field->reference_rule != 'NULL')
                {

                    //print_r($field->reference_rule);exit;

                    $reference = $field->reference_rule;
                    $reference_data = explode('.', $reference);
                    $cnt_ref = count($reference_data);
                    if($cnt_ref == 3){
                        $l_tablename = $reference_data[0];
                        $l_tablename_fieldname = $reference_data[1];
                        $fieldname = $reference_data[2];

                        //reference_table
                        $lookupTable = UniTable::select('model')->where('name', ltrim($l_tablename, '#'))->first();
                        $lookup_model = $lookupTable->model;

                        $field_write['reference_model'] = $lookup_model;
                        $field_write['reference_model_field'] = $l_tablename_fieldname;
                        $field_write['reference_field'] = $fieldname;
                        //print_r($fieldname);exit;
                    }else{

                        $l_tablename = current(explode('.', $field->reference_rule));
                        $fieldname = explode('.', $field->reference_rule);
                        $fieldname = end($fieldname);

                        //reference_table
                        $lookupTable = UniTable::select('model')->where('name', ltrim($l_tablename, '#'))->first();
                        $lookup_model = $lookupTable->model;

                        $field_write['reference_model'] = $lookup_model;
                        $field_write['reference_model_field'] = NULL;
                        $field_write['reference_field'] = $fieldname;
                    }



                }


                $field_write['min'] = $field->min;
                $field_write['max'] = $field->max;
                $field_write['tooltip'] = $field->tooltip;
                $field_write['tooltip_text'] = $field->tooltip_text;
                $field_write['prepend'] = true;
                $field_write['append'] = false;
                $this->settings['fields'][$cat][$i] = $field_write;
                $i++;
                }
            }

        }
    }
    //print_r($this->settings['fields']);exit;
  }

  function validateCategories($tableid, $metas, $cats, $permissions)
  {
      $categories = array();
      
      foreach ($cats as $cat)
        {
            $catValue = $cat;
            $fields = UniFieldWrite::select('field_name')
            ->where('uni_table_id', $this->settings['tableid'])
            ->where('category', $catValue)
            ->orderBy('order', 'asc')
            ->get();

            $perm_types = array();
            $permission_types = UniPermissionType::all();
            
            foreach($permission_types as $p_types){
                $perm_types[$p_types->id] = $p_types->code;
            }
            
            $cnt=0;
            foreach($fields as $field){
                //print_r($field->field_name);
                /*Go through permission tabel to get specific permission per user type and field name (H|W|R)*/
                $this_field = $field->field_name;
                
                $user       = User::find(Auth::user()->id);
                $account_type = $user->accounttype;
                if($account_type == 4){
                    $usageCode = 'R';
                    if(isset($permissions[$this_field]) && !empty($permissions[$this_field]))
                    {
                        $this_field_perm = $permissions[$this_field];
                        $permissiontype_id = $this_field_perm['uni_permissiontype_id'];
                        $usageCode = $perm_types[$permissiontype_id];

                    }
                }else{
                    $usageCode = 'W';
                    if(isset($permissions[$this_field]) && !empty($permissions[$this_field]))
                    {
                        $this_field_perm = $permissions[$this_field];
                        $permissiontype_id = $this_field_perm['uni_permissiontype_id'];
                        if($permissiontype_id == 1){
                            $usageCode = $perm_types[$permissiontype_id];
                        }

                    }
                }
                
                
                
                if($usageCode != 'H'){
                  $categories[] = $cat;// only assign distinct
                }
                $cnt++;
            }
        }
        //print_r($categories);
        return $categories;
  }


   /**
   * buildViewerArray
   *
   * Build the viewerpanel array along with other settings, this array can then be changed later on
   *
   * @return void
   */
  function buildViewerArray()
  {
    //Table settings
    $table_name = $this->settings['table'];
    $table_name = ltrim($table_name, '#');
    $tables = UniTable::where('name', $table_name)->first();
    //print_r($tables->name);

    /*Setting the table settings pulled from database*/
    $this->settings['search'] = $tables->search;
    $this->settings['description'] = $tables->description;
    $this->settings['tableid'] = $tables->id;
    
    if(empty($this->settings['table_id_set']) || $this->settings['table_id_set'] == '')
    {
        $this->settings['table_id_set'] = $table_name.'_table';
    }
    //$this->settings['searchTitle'] = $this->getSearchTitle();

    $this->settings['model'] = $tables->model;
    $this->settings['controller'] = $tables->controller;
    $this->settings['imports'] = $tables->imports;
    $this->settings['edit_model'] = $tables->model;
    $this->settings['edit_controller'] = $tables->controller;
    $this->settings['edit_imports'] = $tables->imports;

    //check the edit table as well if viewing from one table and editing this table
    $this->settings['edit_tableid'] = $this->settings['tableid'];
    if ($this->settings['table'] != $this->settings['edit_table'])
    {
      $editTable = UniTable::where('name', ltrim($this->settings['edit_table'], '#'))->first();

      $this->settings['edit_tableid'] = $editTable->id;
      $this->settings['edit_model'] = $editTable->model;
      $this->settings['edit_controller'] = $editTable->controller;
      $this->settings['edit_imports'] = $editTable->imports;
    }

    //Permission control settings
    $this->settings['controls'] = array();
    if ($this->settings['permissions']['create']) {
        $this->settings['controls'][0]['name'] = $this->settings['key'] . "new";
        $this->settings['controls'][0]['value'] = str_pad('New', 8, ' ', STR_PAD_BOTH);
        $this->settings['controls'][0]['onclick'] = "document.getElementById('mode').value = 'new';";
    }
    if($this->settings['edit_table'] == 'vw_signoff'){
        if ($this->settings['permissions']['update'] && ($this->settings['buttons']['edit'] ?? true) ) {
            $this->settings['controls'][1]['name'] = $this->settings['key'] . "update";
            $this->settings['controls'][1]['value'] = str_pad('View', 8, ' ', STR_PAD_BOTH);
            $this->settings['controls'][1]['onclick'] = "document.getElementById('mode').value = 'edit';";
        }
    }else{
        if ($this->settings['permissions']['update'] && ($this->settings['buttons']['edit'] ?? true) ) {
            $this->settings['controls'][1]['name'] = $this->settings['key'] . "update";
            $this->settings['controls'][1]['value'] = str_pad('Edit', 8, ' ', STR_PAD_BOTH);
            $this->settings['controls'][1]['onclick'] = "document.getElementById('mode').value = 'edit';";
        }
    }
    if ($this->settings['permissions']['read'] && !($this->settings['permissions']['update'] && ($this->settings['buttons']['edit'] ?? true)) ) {
        $this->settings['controls'][1]['name'] = $this->settings['key'] . "edit";
        $this->settings['controls'][1]['value'] = str_pad('View', 8, ' ', STR_PAD_BOTH);
        $this->settings['controls'][1]['onclick'] = "document.getElementById('mode').value = 'edit';";
    }
    if ($this->settings['permissions']['delete']) {
        $this->settings['controls'][2]['name'] = $this->settings['key'] . "delete";
        $this->settings['controls'][2]['value'] = str_pad('Delete', 8, ' ', STR_PAD_BOTH);
        $this->settings['controls'][2]['onclick'] = "document.getElementById('mode').value = 'delete';";
    }
    if ($this->settings['permissions']['expunge']) {
        $this->settings['controls'][3]['name'] = $this->settings['key'] . "expunge";
        $this->settings['controls'][3]['value'] = str_pad('Delete', 8, ' ', STR_PAD_BOTH);
        $this->settings['controls'][3]['onclick'] = "document.getElementById('mode').value = 'expunge';";
    }
    if ($this->settings['permissions']['export']){
        $this->settings['controls'][4]['name'] = $this->settings['key'] . "export";
        $this->settings['controls'][4]['value'] = str_pad('Export Data ...', 8, ' ', STR_PAD_BOTH);
        $this->settings['controls'][4]['onclick'] = "document.getElementById('mode').value = 'export';";
    }
    if ($this->settings['permissions']['import']){
        $this->settings['controls'][5]['name'] = $this->settings['key'] . "import";
        $this->settings['controls'][5]['value'] = str_pad('Import Data ...', 8, ' ', STR_PAD_BOTH);
        $this->settings['controls'][5]['onclick'] = "document.getElementById('mode').value = 'import';";
    }

    //Field settings
    $fields = UniFieldRead::where('uni_table_id', $this->settings['tableid'])->get();

    $this->settings['fields'] = array();
    $consts = get_defined_constants();
    $editString = '';
    $refedTableFields = array();

    $joinedTables = array();

    foreach($fields as $field){
        //print_r('id : '.$field->id);

        $read_field = array();
        $read_field['id'] = $field->id;
        $read_field['sql'] = $this->settings['primarytable'] . '.' . $field->field_id . $this->queryAlias() . $field->field_id . '_' . $field->order;
        $read_field['table_field'] = $this->settings['primarytable'] . '.' . $field->field_id;
        $read_field['fieldname'] = $field->field_id;
        if ($field->field_id == 'id' && strlen($editString) == 0) $editString = $read_field['sql'];
        $read_field['name'] = $field->label;
        if (empty($read_field['name'])) $read_field['name'] = ucwords($field->field_id);
        $read_field['filter'] = $field->uni_filtertype_id;
        if (empty($read_field['filter'])) $read_field['filter'] = 0;
        $read_field['width'] = $field->width . '%';
        //$read_field['alignment'] = $field->align;
        if (empty($read_field['alignment'])) $read_field['alignment'] = 'left';
        $read_field['sort'] = $field->sort === 1 || $field->sort === 't';
        $read_field['reference'] = $field->reference_rule;

        $read_field['reference_model'] = NULL;
        $read_field['reference_model_field'] = NULL;
        $read_field['reference_field'] = NULL;

        //when reference is looking for data in another table eg.lookup
        if (!empty($field->reference_rule) && $field->reference_rule != 'NULL')
        {

            //print_r($field->reference_rule);exit;

            $reference = $field->reference_rule;
            $reference_data = explode('.', $reference);
            $cnt_ref = count($reference_data);
            if($cnt_ref == 3){
                $l_tablename = $reference_data[0];
                $l_tablename_fieldname = $reference_data[1];
                $fieldname = $reference_data[2];

                //reference_table
                $lookupTable = UniTable::select('model')->where('name', ltrim($l_tablename, '#'))->first();
                $lookup_model = $lookupTable->model;

                $read_field['reference_model'] = $lookup_model;
                $read_field['reference_model_field'] = $l_tablename_fieldname;
                $read_field['reference_field'] = $fieldname;
            }else{

                $l_tablename = current(explode('.', $field->reference_rule));
                $fieldname = explode('.', $field->reference_rule);
                $fieldname = end($fieldname);

                //reference_table
                $lookupTable = UniTable::select('model')->where('name', ltrim($l_tablename, '#'))->first();
                $lookup_model = $lookupTable->model;

                $read_field['reference_model'] = $lookup_model;
                $read_field['reference_model_field'] = NULL;
                $read_field['reference_field'] = $fieldname;
            }



        }

        //This assigns the field formatter
        if (!empty($field->output_rule))
        {
          $read_field['format'] = $field->output_rule;
        }
        else
        {
          $read_field['format'] = 'None';
        }

        $read_field['field'] = '%';
        $read_field['show'] = false;

        if (!empty($field->tag) && substr($read_field['name'],0,1) != '#')
        {
          $read_field['show'] = true;
          $read_field['tag'] = $field->tag;
          if ($read_field['tag'] == '*')
          {
            if (!$this->settings['permissions']['update'])
            {
              $read_field['tag'] = null;
            }
            else $read_field['tag'] = '<a class="navigation" href="' . $this->settings['current_page'] . '?edit=' . urlencode($this->settings['key']) . 'edit&id=|' . $this->settings['tableid'] . '_%' . $editString . '%|&' . $this->settings['getparameters'] . '">%%%</a>';
          }
        }
        $this->settings['fields'][] = $read_field;
    }
    //print_r($this->settings);
    //exit;
  }

  public function show():array
  {
    $panel = $this->getViewer();
    if ($panel !== null)
    {
      return $panel->getPanel();
    }

    return null;
  }

  /**
   * This will return the panel to be added to template file
   */
  private function getViewer()
  {
    if ($this->settings['mode'] == 'read' &&  $this->settings['view_type'] == 'vertical')
    {
      if (!$this->settings['permissions']['search'])
      {
        $this->settings['search'] = '';
      }
      $pre = '';
      $post = '';

      $customSettings = [
          'controls' => $this->settings['controls'],
          'searchFields' => $this->settings['search'],
          'size' => $this->settings['resultcount'],
          'useStartEndButtons' => $this->settings['usestartendbuttons'],
          'getParamters' => $this->settings['getparameters'],
          'current_route' => $this->settings['current_route'],
          'status' => $this->settings['status'],
          'message' => $this->settings['message']
      ];

      foreach ($this->settings as $key => $val)
      {
        $customSettings[$key] = $val;
      }

      if ($this->settings['filtersperrow'] !== null) $customSettings['filtersPerRow'] = $this->settings['filtersperrow'];
      if ($this->settings['filtershidden'] !== null) $customSettings['filtersHidden'] = $this->settings['filtershidden'];
      if ($this->settings['filtershideable'] !== null) $customSettings['filtersHideable'] = $this->settings['filtershideable'];
      if (isset($this->settings['remove_select_all'])) $customSettings['removeSelectAll'] = $this->settings['remove_select_all'];
      if (isset($this->settings['initial_sort_field'])) $customSettings['initialSortField'] = $this->settings['initial_sort_field'];
      if (isset($this->settings['initial_sort_order'])) $customSettings['initialSortOrder'] = $this->settings['initial_sort_order'];

      //Builds the datatable
      $panel = new ViewerPanel($this->settings['table'],$this->settings['description'], $this->settings['fields'],
          $this->settings['where'],$this->settings['model'],
          $customSettings,$this->settings['table_id_set'],$this->settings['lookup']
      );

      if (isset($this->footer)) $panel->footer = $this->footer;
      if (isset($this->header)) $panel->header = $this->header;

      return $panel;
    }else if (($this->settings['mode'] == 'edit' || $this->settings['mode'] == 'create' || $this->settings['mode'] == 'save') ){
        //Builds the editable form  
        $customSettings = [
          'controls' => $this->settings['controls'],
          'controls_top_left' => $this->settings['controls_top_left'], 
          'controls_top_right' => $this->settings['controls_top_right'],
          'controls_bottom_left' => $this->settings['controls_bottom_left'],
          'controls_bottom_right' => $this->settings['controls_bottom_right'],  
          'status' => $this->settings['status'],
          'message' => $this->settings['message']];
        $panel = new FormPanel($this->settings['table'],$this->settings['title'], $this->settings['fields'],
            $this->settings['where'],$this->settings['model'],$this->settings['categories'],
            $customSettings,
            $this->settings['id_field'],
            $this->settings['id']
        );
        //print_r($panel);exit;
        return $panel;
    }else if (( $this->settings['mode'] == 'read') &&  $this->settings['view_type'] == 'horizontal'){
        //Builds the editable form
        //print_r($this->settings['fields']);
        $customSettings = ['controls' => $this->settings['controls']];
        $panel = new ViewerEditPanel($this->settings['table'],$this->settings['description'], $this->settings['fields'],
            $this->settings['where'],$this->settings['model'],$this->settings['categories'],
            $customSettings,$this->settings['table_id_set']
        );
        //print_r($panel);exit;
        return $panel;
    }
    return null;
  }


  /**
   * checkMode
   *
   * Checks to see what should be done (view, edit, create, delete)
   *
   * @return string view mode
   */
   function checkMode($react_state)
   {

       switch($react_state){
           case 'start':
               $currentPath= Route::getFacadeRoot()->current()->uri();
               $this->settings['current_route'] = $currentPath;
               return 'read';
           break;
           case 'new':
               if ($this->settings['permissions']['create'])
                {
                    return 'create';
                }else{
                    return 'read';
                }

           break;
           case 'edit':
               if ($this->settings['permissions']['update'])
                {
                    return 'edit';
                }else{
                    return 'read';
                }
           break;
            case 'save':
               if ($this->settings['permissions']['update'] || $this->settings['permissions']['create'])
                {
                    return 'save';
                }else{
                    return 'read';
                }
           break;
           case 'delete':
               if ($this->settings['permissions']['delete'])
                {
                    return 'edit';
                }else{
                    return 'read';
                }
               return 'delete';
           break;
           case 'export':
               if ($this->settings['permissions']['export'])
                {

                    return 'export';
                }else{

                    return 'read';
                }
               return 'export';
           break;
           default:
               return 'read';
       }
   }

  public static function queryAlias()
  {
    return ' as /*B9E9B815F68D*/ ';
  }

    /**
    * @param $fieldName
    * @return bool|int
    */
   function getField($fieldName)
   {
     $cnt = 0;
     //print_r($this->settings['fields']);
     if(isset($this->settings['categories']) && !empty($this->settings['categories'])){
        foreach($this->settings['categories'] as $cat){
            $cnt_fields = count($this->settings['fields'][$cat]);
            for($k=0;$k<$cnt_fields;$k++){
                if($this->settings['fields'][$cat][$k]['field_name'] == $fieldName){
                    return $cnt;
                }
                $cnt++; 
            }             
        }        
     }else{   
        foreach ($this->settings['fields'] as $field)
        {
          if ($field['field_name'] == $fieldName)
          {
            return $cnt;
          }
          $cnt++;
        }
     }

     return false;
   }
   
   
   /**
    * @param $fieldName
    * @return array
    */
   function getFieldArray($fieldName)
   {
     $field_array = array();
     //print_r($this->settings['fields']);
     if(isset($this->settings['categories']) && !empty($this->settings['categories'])){
        foreach($this->settings['categories'] as $cat){
            $cnt_fields = count($this->settings['fields'][$cat]);
            for($k=0;$k<$cnt_fields;$k++){
                if($this->settings['fields'][$cat][$k]['field_name'] == $fieldName){
                    $field_array = $this->settings['fields'][$cat][$k];
                }
            }             
        }        
     }else{   
        foreach ($this->settings['fields'] as $field)
        {
          if ($field['field_name'] == $fieldName)
          {
            $field_array = $field;
          }
        }
     }

     return $field_array;
   }

   /**
    * @param $table
    */
   public static function getTableStructure($table){
       $schema = array();

       $information_schema = InformationSchema::select(
                'CHARACTER_MAXIMUM_LENGTH as CharLength',
                'NUMERIC_PRECISION as Base',
                'NUMERIC_SCALE as Decimals',
                'COLUMN_NAME as Field',
                'DATA_TYPE as Type',
                'IS_NULLABLE as Nullable')
            ->where('table_name', $table)
            ->get();

       $cnt = 0;
       foreach($information_schema as $info){
           $schema[$cnt]['CharLength'] = $info->CharLength;
           $schema[$cnt]['Base'] = $info->Base;
           $schema[$cnt]['Decimals'] = $info->Decimals;
           $schema[$cnt]['Field'] = $info->Field;
           $schema[$cnt]['Type'] = $info->Type;
           $schema[$cnt]['Nullable'] = $info->Nullable;
           $cnt++;
       }

       return $schema;
   }
   
   
   
   /**
   *
   * updateRecord: Updates a record with the supplied validated input from the user
   *
   * @return list [status,message] True if successful and false if failed along with message
   */
function updateRecord()
{
    $status = true;
    $message = 'Success Update';
    $create_list = $this->validateWriteInputs(); 
    //print_r($update_list);
    list(
        $table_main,
        $table_unique_key,
        $table_key_value,
        $table_main_data,
        $table_second,
        $table_second_unique_key,
        $table_second_key_value,
        $table_second_data,
        $table_other,
        $table_other_unique_key,
        $table_other_key_value,
        $table_other_data
    ) = $this->getTableDependencies($create_list);
      
    /*if(isset($this->settings['edit_table'])){
         print_r($this->settings['edit_table']);
    }*/
    $user       = User::find(Auth::user()->id);
    $user_personnel_number = $user->personnelnumber;
    $main_id = 0;
    $emp_personnelnumber = '';
    foreach($table_main_data as $main_t=>$data_arr){
        //($main_t);
        if($main_t == 'cycle_phases'){
            
            $instance = PermissionController::getUserInstance();
            $instance_id = $instance['response']['instance'];
            $cycle_id = 1;
            
            $main_t_model_phase = "App\\Models\\CyclePhases";
            $phase_table = $main_t_model_phase::find($phase_id);
            $phase_table::where('instance_id', $instance_id)      
                    ->where('cycle_id', $cycle_id) 
                    ->chunkById(200, function ($flights) {
                $phase_table->each->update(['live' => 0]);
                $phase_table->each->update(['open' => 0]);
            });
            
            $phase_id = 3;
            $main_t_model_phase = "App\\Models\\CyclePhases";
            $phase_table = $main_t_model_phase::find($phase_id)
                    ->where('instance_id', $instance_id)      
                    ->where('cycle_id', $cycle_id) ;
            $phase_table->open = 1;
            $phase_table->live = 1;
            $phase_table->save();
            
        }else{
        
            $t_tables = UniTable::select('model')->where('name', $main_t)->first();
            $main_t_model = "App\\Models\\".$t_tables->model;
            $i=0;
            foreach($table_key_value as $key_name=>$unique_main)
            {
                //main id will always be first
                if($i == 0){
                    $main_id_check = $main_t_model::select('id')->where($key_name,$unique_main)->first();
                    $get_main_id = $main_id_check->id;

                    if($get_main_id != 0){
                        $main_id = $get_main_id;
                        $data_table = $main_t_model::find($get_main_id);
                        if($main_t == 'employees'){
                            $emp_personnelnumber = $data_table->personnelnumber;
                        }
                        
                        foreach($data_arr as $key_val=>$dat_val){
                            $data_table->$key_val = $dat_val;
                            $value_before = $data_table->$key_val;
                            $value_after = $dat_val;
                            $phase_id = Phase::getLivePhaseID();
                            if($main_t == 'employees'){
                                $audit = new Audit;
                                $audit_entry = $audit::captureAudit($emp_personnelnumber, $key_val, $user_personnel_number, $value_before, $value_after, $phase_id);
                            }
                        }
                        $data_table->save();
                    }
                }
                $i++;
            }   
        }
    }
    
    //secondary table uses second unique key
    foreach($table_second_data as $main_t=>$data_arr){
        $t_tables = UniTable::select('model')->where('name', $main_t)->first();
        $main_t_model = "App\\Models\\".$t_tables->model;
        $i=0;
        foreach($table_key_value as $key_name=>$unique_main)
        {
            //main id will always be first
            if($i == 1){
                //get key value set by main table
                if(strpos($table_unique_key[$i],'.') > 0){
                    $key_name_data = explode('.',$table_unique_key[$i]);
                    $this_key_name = $key_name_data[1];
                }   
                
                $main_id_check = $main_t_model::select('id')->where($this_key_name,$unique_main)->first();
                $get_main_id = $main_id_check->id;
                
                if($get_main_id != 0){
                    $data_table = $main_t_model::find($get_main_id);
                    
                    foreach($data_arr as $key_val=>$dat_val){
                        if($key_val == 'instance_id' && $dat_val != ''){
                            $lookup_table = "App\\Models\\Instance";
                            $lookup_tbl = $lookup_table::select('id')->where('name',$dat_val)->first();
                            //print_r($dat_val);
                            //dd($lookup_tbl);
                            $dat_val = $lookup_tbl->id;
                        }
                        if($key_val == 'region_id' && $dat_val != ''){
                            $lookup_table = "App\\Models\\Region";
                            $lookup_tbl = $lookup_table::select('id')->where('name',$dat_val)->first();
                            $dat_val = $lookup_tbl->id;
                        }
                        if($key_val == 'country_id' && $dat_val != ''){
                            //print_r($dat_val);
                            $lookup_table = "App\\Models\\Country";
                            $lookup_tbl = $lookup_table::select('id')->where('name',$dat_val)->first();
                            $dat_val = $lookup_tbl->id;
                        }
                        if($key_val == 'company_id' && $dat_val != ''){
                            $lookup_table = "App\\Models\\Company";
                            $lookup_tbl = $lookup_table::select('id')->where('name',$dat_val)->first();
                            $dat_val = $lookup_tbl->id;
                        }
                        if($key_val == 'business_area_id' && $dat_val != ''){
                            $lookup_table = "App\\Models\\BusinessArea";
                            $lookup_tbl = $lookup_table::select('id')->where('name',$dat_val)->first();
                            $dat_val = $lookup_tbl->id;
                        }
                        if($key_val == 'business_unit_id' && $dat_val != ''){
                            $lookup_table = "App\\Models\\BusinessUnit";
                            $lookup_tbl = $lookup_table::select('id')->where('name',$dat_val)->first();
                            $dat_val = $lookup_tbl->id;
                        }
                        if($key_val == 'division_id' && $dat_val != ''){
                            //print_r($dat_val);
                            $lookup_table = "App\\Models\\Division";
                            $lookup_tbl = $lookup_table::select('id')->where('name',$dat_val)->first();
                            $dat_val = $lookup_tbl->id;
                        }
                        if($key_val == 'department_id' && $dat_val != ''){
                            $lookup_table = "App\\Models\\Department";
                            $lookup_tbl = $lookup_table::select('id')->where('name',$dat_val)->first();
                            $dat_val = $lookup_tbl->id;
                        }
                        
                        $value_before = $data_table->$key_val;
                        $value_after = $dat_val;
                        $phase_id = Phase::getLivePhaseID();
                        if($main_t == 'employees'){
                            $audit = new Audit;
                            $audit_entry = $audit::captureAudit($emp_personnelnumber, $key_val, $user_personnel_number, $value_before, $value_after, $phase_id);
                        }
                        
                        $data_table->$key_val = $dat_val;
                    }
                    $data_table->save();
                }
            }
            $i++;
        }        
    }
       
    foreach($table_other_data as $main_t=>$data_arr){
        $t_tables = UniTable::select('model')->where('name', $main_t)->first();
        $main_t_model = "App\\Models\\".$t_tables->model;
        $i=0;
        foreach($table_key_value as $key_name=>$unique_main)
        {
            //main id will always be first
            if($i == 0){
                $main_id_check = $main_t_model::select('id')->where($key_name,$unique_main)->first();
                $get_main_id = $main_id_check->id;
                
                if($get_main_id != 0){
                    $data_table = $main_t_model::find($get_main_id);
                    if($main_t == 'employees'){
                        $emp_personnelnumber = $data_table->personnelnumber;
                    }
                    foreach($data_arr as $key_val=>$dat_val){
                        
                        if($key_val == 'participation_category_id' && $dat_val != ''){
                            $lookup_table = "App\\Models\\ParticipationCategory";
                            $lookup_tbl = $lookup_table::select('id')->where('name',$dat_val)->first();
                            $dat_val = $lookup_tbl->id;
                        }
                        if($key_val == 'incentive_scheme_id' && $dat_val != ''){
                            $lookup_table = "App\\Models\\IncentiveScheme";
                            $lookup_tbl = $lookup_table::select('id')->where('name',$dat_val)->first();
                            $dat_val = $lookup_tbl->id;
                        }
                        
                        $value_before = $data_table->$key_val;
                        $value_after = $dat_val;
                        $phase_id = Phase::getLivePhaseID();
                        if($main_t == 'employees'){
                            $audit = new Audit;
                            $audit_entry = $audit::captureAudit($emp_personnelnumber, $key_val, $user_personnel_number, $value_before, $value_after, $phase_id);
                        }
                        
                        $data_table->$key_val = $dat_val;
                    }
                    $data_table->save();
                }
            }
            $i++;
        }        
    }
    
    if($main_id != 0){
        $this->settings['id_field'] = 'id';
        $this->settings['id'] = $main_id;
            
        $this->buildEditorArray(INPUT_METHOD_STANDARD);
    }
  
 
    return [$status,$message];
}
  
    /**
   *
   * createRecord: Inserts the supplied validated input from the user into the database
   *
   * @return list [status,message] True if successful and false if failed along with message
   */
    function createRecord()
    {
        $status = true;
        $message = 'Success Create';
        $create_list = $this->validateWriteInputs();  
        //print_r($create_list);
        
        list(
            $table_main,
            $table_unique_key,
            $table_key_value,
            $table_main_data,
            $table_second,
            $table_second_unique_key,
            $table_second_key_value,
            $table_second_data,
            $table_other,
            $table_other_unique_key,
            $table_other_key_value,
            $table_other_data
        ) = $this->getTableDependencies($create_list);
       
        //Firts create entry in the main table
        $main_id = 0;
        $personnel_number = '';
        foreach($table_main_data as $main_t=>$data_arr){
            $t_tables = UniTable::select('model')->where('name', $main_t)->first();
            $main_t_model = "App\\Models\\".$t_tables->model;
            
            $data_table = new $main_t_model;
            foreach($data_arr as $key_val=>$dat_val){
                if($key_val == 'user_id' && $dat_val == ''){
                    $dat_val = Auth::user()->id;
                }
                if($key_val == 'personnelnumber'){
                    $personnel_number = $dat_val;
                }
                
                $data_table->$key_val = $dat_val;
            }
            if($main_t == 'employees'){
                $data_table->org_structure_id = 1;
                $data_table->cycle_id = 1;
                $instance = PermissionController::getUserInstance();
                $instance_id = $instance['response']['instance'];
                $data_table->instance_indicator_id = $instance_id;           
            }
            
            
            $data_table->save();
            $main_id = $data_table->id;
            
            if($main_t == 'employees'){
                 //insert default into performance
                $t_tables_perf = UniTable::select('model')->where('name', 'employee_performances')->first();
                $main_t_model_perf = "App\\Models\\".$t_tables_perf->model;

                $data_table_perf = new $main_t_model_perf;
                $data_table_perf->personnelnumber = $personnel_number;
                
                $data_table_perf->save();
                //$secondary_id = $data_table_perf->id;
                
            }
        }
        
        //Validate that the entry has been inserted
        if($main_id != 0){
        
            if(count($table_second) >= 1){ //Run only when there is a secondary table
                //Second create entry in the secondary table
                $secondary_id = '';
                foreach($table_second_data as $main_t=>$data_arr){
                    $t_tables = UniTable::select('model')->where('name', $main_t)->first();
                    $main_t_model = "App\\Models\\".$t_tables->model;

                    $data_table = new $main_t_model;
                    foreach($data_arr as $key_val=>$dat_val){
                        $data_table->$key_val = $dat_val;
                    }
                    $data_table->save();
                    $secondary_id = $data_table->id;
                }
                
                if(isset($secondary_id) && $secondary_id != ''){
                    if(isset($table_unique_key[1]) && !empty($table_unique_key[1])){
                        foreach($table_main_data as $main_t=>$data_arr){
                            $t_tables = UniTable::select('model')->where('name', $main_t)->first();
                            $main_t_model = "App\\Models\\".$t_tables->model;

                            
                            $update_column = '';
                            if(strpos($table_unique_key[1],'.')){
                                $str_dat = explode('.',$table_unique_key[1]);
                                $update_column = $str_dat[0];                               
                            }else{
                                $update_column = $table_unique_key[1];
                            }
                            $data_table = $main_t_model::find($main_id);
                            $data_table->$update_column = $secondary_id;
                            $data_table->save();
                        }
                    }
                }
                
            }

            //Last create entry in the other tablea
            foreach($table_other_data as $main_t=>$data_arr){
                $t_tables = UniTable::select('model')->where('name', $main_t)->first();
                $main_t_model = "App\\Models\\".$t_tables->model;

                $data_table = new $main_t_model;

                //Get value of unique key set above
                if(in_array($main_t,$table_other)){
                    foreach($table_other_key_value as $key_val=>$dat_val){
                        $data_table->$key_val = $dat_val;
                    }
                }

                foreach($data_arr as $key_val=>$dat_val){
                    $data_table->$key_val = $dat_val;
                }
                $data_table->save();
            }   
        }
        
        if($main_id != 0){
            $this->settings['id_field'] = 'id';
            $this->settings['id'] = $main_id;
             //$this->settings['mode'] = 'read';
            //$this->buildEditorArray(INPUT_METHOD_STANDARD);
        }
        
        return [$status,$message];
      
    }
    
    
     /**
    * @return list table data to be update/inserted
    */
    private function getTableDependencies($create_list){
        $table_main = array();
        $table_unique_key = array();     
        $table_key_value = array();
               
        $table_second = array();
        $table_second_unique_key = array();      
        $table_second_key_value = array();   
               
        $table_other = array();
        $table_other_unique_key = array();      
        $table_other_key_value = array();    
        
        foreach($create_list as $list){
            foreach($list as $create){
                if($create['table_main'] == 1 && !in_array($create['table_name'],$table_main)){
                    $table_main[] = $create['table_name'];
                    
                    $table_key = $create['table_key'];
                    //get_first for main id key
                    $dat_table_key = explode('/',$table_key);
                    foreach($dat_table_key as $dat_key){
                        $table_unique_key[] = $dat_key;
                    }
                }
                
                if($create['second_table'] != null && $create['second_table'] != 'NULL' && !in_array($create['second_table'],$table_second) && !in_array($create['second_table'],$table_main)){
                    $table_second[] = $create['second_table'];
                }
                
                if(isset($table_second) && !empty($table_second)){
                    foreach($table_second as $second){
                        if(isset($create['table_name']) && $create['table_name'] == $second){
                            if(isset($create['table_key']) && !in_array($create['table_key'],$table_second_unique_key)){
                                $table_second_unique_key[] = $create['table_key'];
                            }
                        }
                    }                  
                }
                
                if($create['table_main'] == 0 && !in_array($create['table_name'],$table_main) && !in_array($create['table_name'],$table_second) && !in_array($create['table_name'],$table_other)){
                    $table_other[] = $create['table_name'];
                }
                
                if(isset($table_other) && !empty($table_other)){
                    foreach($table_other as $other){
                        if(isset($create['table_name']) && $create['table_name'] == $other){
                            if(isset($create['table_key']) && !in_array($create['table_key'],$table_other_unique_key)){
                                $table_other_unique_key[] = $create['table_key'];
                            }
                        }
                    }                  
                }
                
                foreach($table_unique_key as $unique_key){
                     $this_unique_key = $unique_key;
                    if(strpos($unique_key,'.') > 0){
                        $data = explode('.',$unique_key);
                        $this_unique_key = $data[0];
                    }
                   
                    if(isset($create['table_fields']['field_name']) && $create['table_fields']['field_name'] == $this_unique_key){
                        $table_key_value[$this_unique_key] = $create['table_fields']['submit'];
                    }
                }
                
                foreach($table_second_unique_key as $unique_key){
                     $this_unique_key = $unique_key;
                    if(strpos($unique_key,'.') > 0){
                        $data = explode('.',$unique_key);
                        $this_unique_key = $data[0];
                    }
                   
                    $i = 1;
                    if(count($table_unique_key) >= 2){
                        foreach($table_unique_key as $key_name){
                            if($i >= 2){
                                if(strpos($key_name,'.') > 0){
                                    $key_name_data = explode('.',$key_name);
                                    $this_key_name = $data[1];
                                    if($this_key_name == $unique_key){
                                        $table_second_key_value[$this_key_name] = $table_key_value[$data[0]];
                                    }
                                    
                                }
                                
                            }
                            $i++;
                        }
                    }
                    
                    if(isset($create['table_fields']['field_name']) && $create['table_fields']['field_name'] == $this_unique_key){
                        $table_second_key_value[$this_unique_key] = $create['table_fields']['submit'];
                    }
                }
                
                foreach($table_other_unique_key as $unique_key){
                    $this_unique_key = $unique_key;
                    if(isset($table_key_value[$this_unique_key])){
                        $table_other_key_value[$this_unique_key] = $table_key_value[$this_unique_key];
                    }
                }
                
            }
        } 
        
        $table_main_data = array();
        foreach($table_main as $main_t){
            foreach($create_list as $list){
                foreach($list as $create){
                    if(isset($create['table_name']) && $create['table_name'] ==  $main_t){
                        if(isset($create['table_fields']['field_name']) && $create['table_fields']['field_name'] != 'id'){
                        $table_main_data[$main_t][$create['table_fields']['field_name']] = $create['table_fields']['submit'];    
                        }
                    }
                }
            }    
        }
        
        $table_second_data = array();
        foreach($table_second as $main_t){
            foreach($create_list as $list){
                foreach($list as $create){
                    if(isset($create['table_name']) && $create['table_name'] ==  $main_t){
                        if(isset($create['table_fields']['field_name']) && $create['table_fields']['field_name'] != 'id'){
                        $table_second_data[$main_t][$create['table_fields']['field_name']] = $create['table_fields']['submit'];    
                        }
                    }
                }
            }    
        }
        
        $table_other_data = array();
        foreach($table_other as $main_t){
            foreach($create_list as $list){
                foreach($list as $create){
                    if(isset($create['table_name']) && $create['table_name'] ==  $main_t){
                        if(isset($create['table_fields']['field_name']) && $create['table_fields']['field_name'] != 'id'){
                        $table_other_data[$main_t][$create['table_fields']['field_name']] = $create['table_fields']['submit'];    
                        }
                    }
                }
            }    
        }
        
        return [
            $table_main,
            $table_unique_key,
            $table_key_value,
            $table_main_data,
            $table_second,
            $table_second_unique_key,
            $table_second_key_value,
            $table_second_data,
            $table_other,
            $table_other_unique_key,
            $table_other_key_value,
            $table_other_data
        ];
    }
    
   
    private function validateWriteInputs()
    {
      
        $update_fields = array();
        $full_update_fields = array();
        $diff_table_data = array();
        $distinct_table = array();

        //$data = User::create ( [ 'name' => 'first' ]); dd ($data->id); 
        
        if(isset($this->form_data) && !empty($this->form_data)){
            //print_r($this->form_data);
            foreach($this->form_data as $key=>$value){
                if( !empty($this->getFieldArray($key))){
                    $update_fields[] = $this->getFieldArray($key);
                }                
            }
        }
      
        foreach($update_fields as $data_fields){            
            $get_data_table_id = $data_fields['uni_ref_table_id'];
            if(!in_array($data_fields['uni_ref_table_id'],$distinct_table)){
                $distinct_table[] = $data_fields['uni_ref_table_id'];
            }
            $diff_table_data[$get_data_table_id][] = $data_fields['field_name'];
        }
        
        foreach($distinct_table as $table){
            //data table
            //print_r($table);
            $field_list = array();
            $create_list = array();
            $data_field_list = $diff_table_data[$table];
            
            $dataTable = UniTable::select('model','name','search','second_table')->where('id', $table)->first();
            //print_r($dataTable);
            $data_model = $dataTable->model;
            $table_name = $dataTable->name;
            $this_key = $dataTable->search;
            $second_table = $dataTable->second_table; 
            
            //validate if field belongs to table
            $schema = array();
            $table_schemas = array();
            $table_meta = $this->getTableStructure($table_name);
            foreach($table_meta as $meta){
                $schema[] = $meta['Field'];
                $table_schemas[$meta['Field']] = $meta;
            }
            //print_r($table_meta);
            $cnt = 0;
            foreach($data_field_list as $validate_field){
                if(in_array($validate_field,$schema)){
                    $create_list['field_name'] = $validate_field;
                    $field_submit = $this->form_data[$validate_field];
                    $create_list['submit'] = $field_submit;
                    $field_list[$cnt]['model'] = $data_model;
                    $field_list[$cnt]['table_name'] = $table_name;
                    $field_list[$cnt]['table_key'] = $this_key;
                    $field_list[$cnt]['second_table'] = $second_table;
                    $field_list[$cnt]['table_fields'] = $create_list;
                    if(isset($this->settings['table']) && $this->settings['table'] == $table_name){
                        $field_list[$cnt]['table_main'] = 1;
                    }else{
                         $field_list[$cnt]['table_main'] = 0;
                    }
                    $field_list[$cnt]['table_schema'] = $table_schemas[$validate_field];
                    $cnt++;
                }
            }
            $full_update_fields[] = $field_list;
            
             //print_r($field_list);
            
        }
      
      return $full_update_fields;
      
    }
  
  
}


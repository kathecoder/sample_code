<?php

/* 
 * This will create each input type control
 * 
 */
namespace App\CFLibrary;

use App\CFLibrary\Model\UniFieldWrite;
use App\CFLibrary\Model\UniTable;
use App\Http\Controllers\ManagerMappingsController;
use App\Http\Controllers\LookupModalController;

/* Define formatting constants */
define('CALCULATION_FORMATTER_NONE', 1000);

class CalculationFormatter
{
    /*
  * CalculationFormatter Constructor.
  *
  * @return void
  */
  function CalculationFormatter()
  {
    
  }
  
  /**
   * @static
   * @param Mixed    $field The value to format
   * @param int      $type The calculation formatter key
   * @param null|int $id Record ID
   * @param null|string
   * @return array|bool|float|mixed|number|string
   */
  static function format($field, $type,$table_parent, $id = NULL, $field_name = NULL)
  {
    if ($type >= 1050)
    {
      return formatCustom($field, $type, $id, $field_name);
    }
    else
    {
      switch ($type)
      {
        case 'CALCULATION_FORMATTER_NONE':
            return 'N/A';
        break;  
        case 'CALCULATION_DEFAULT_LOOKUP':
            $selected = '';
            $fields = array();
            $table_id_name = 'lookup';
           
            $table_name = '';
            if(!empty($field)){
                
                if(isset($field['value']) && !empty($field['value'])){
                    $selected = $field['value'];
                }
                
                if(isset($field['field']) && !empty($field['field'])){
                    $fields = $field['field'];
                    if($selected != ''){
                        $record_id = $selected;
                    }else{
                        $record_id = 0;
                    }
                    $table_id_name .= '_'.$fields['field_name'].'_'.$record_id;
                }
                if(isset($field['table_name']) && !empty($field['table_name'])){
                    $table_name = $field['table_name'];
                   
                }
                
            }
            $field_name = $fields['field_name'];
            $selections['field_name'] = 'id';
            $selections['field_value'] = $selected;           
            
            $form_submit_data = array();
            $table = LookupModalController::getLookupTableName($field_name);
            list($children,$identifier) = self::buildCalculationScript($field, $type, $table_parent, $selected, $field_name,$table_name);
                       
            return [$children,$identifier];
            
            break;
        
        default:
          /* Didn't know what to do with the calculation */
          return $field;
      }
    }
    return false;
  }
  
  public static function getCalculationFormatterKey($description)
  {

    $consts = get_defined_constants();
    if (array_key_exists($description, $consts))
    {
      return $consts[$description];

    }
    return CALCULATION_FORMATTER_NONE;
  }
  
  /**
   * @static
   * @param Mixed    $field The value to format
   * @param int      $type The calculation formatter key
   * @param Mixed    $table_parent 
   * @param null|int $id Record ID
   * @param null|string
   * @return array|bool|float|mixed|number|string
   */
  public static function buildCalculationScript($field, $type, $table_parent, $field_name, $id = NULL, $table_name = NULL)
  {
      $script = '';
      $selected = '';
      if(!empty($field)){
                
            if(isset($field['value']) && !empty($field['value'])){
                $selected = $field['value'];
            }

            if(isset($field['field']) && !empty($field['field'])){
                $fields = $field['field'];
                $field_name = $fields['field_name'];
            }
            if(isset($field['table_name']) && !empty($field['table_name'])){
                $table_name = $field['table_name'];

            }

        }
        
        $lines = '';
        $variables = '';
        $children = array();
        $identifiers = array();
        if(!is_numeric($field_name)){
            $child_id = self::getChildFields($table_parent,$field_name);
            $table = 'lookup_'.$field_name;
            $value = $selected;
            //print_r($child_id);
            foreach($child_id as $child){
                $children[] = $child;
                if($child == $field_name){
                    $identifier = $child;
                }else{
                    $identifier = str_replace("_id", "", $child);
                }
                
                $identifiers[] = $identifier;
                //$variables .= "var value_{$child} = '{$table}'+'_'+ this_value +'_{$identifier}_' + data[1];\n";
                //$lines .= "if(value_{$child} == elements[i].name){if (self.parent.document.getElementById('{$child}')){ self.parent.document.getElementById('{$child}').value = elements[i].value';)}\n";
                
            }
        }
      //Get the value selected
      
      return [$children,$identifiers];
      
  }
  

  public static function getChildFields($table_parent,$field_name)
  {
      $child_fields = array();
      //print_r('parent');
      //print_r($table_parent);
      if(is_numeric($table_parent)){
        $tables = UniTable::where('id', $table_parent)->first();
      }else{
        $tables = UniTable::where('name', $table_parent)->first();  
      }
      /*foreach($tables as $tbl){
          print_r($tbl);
      }*/
      $tableid = $tables->id;
      $fields = UniFieldWrite::select('field_id')
                ->where('uni_table_id', $tableid)
                ->where('calc_parent_id', $field_name)
                ->distinct()
                ->get();
      
      foreach($fields as $field){
         $child_fields[] = $field->field_id;
      }
      
      return $child_fields;
  }
    
    
}    
    


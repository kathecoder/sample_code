<?php

/*
 * This inclusive preprocessed hypertext file hold methods and attributes
 * for automatically viewing a table in a viewerpanel
 *
 */
namespace App\CFLibrary;


use App\CFLibrary\Model\UniFieldRead;
use App\CFLibrary\Model\UniFieldWrite;
use App\CFLibrary\Model\UniTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class TableEditHandler
{
    public static function getTableHeader($request)
    {
        try {
            $table   =   UniTable::where('name', $request->table)->first();
            $array   =   array();
            $uniRead    =   UniFieldWrite::where('uni_table_id', $table->id)->get();
            foreach ($uniRead as $data)
            {
                if($data->uni_fieldtype_id===6)//date
                {
                    $array[]       =   array(
                        'id'=>$data->id,
                        'headerName'=>$data->label,
                        'field'=>$data->field_name,
                        'width'=>200,
                        'freeze_col'=>$data->freeze_col,
                        'input_type'=>$data->uni_fieldtype_id

                    );
                }
                else if($data->uni_fieldtype_id===8)//combo box
                {
                    $array[]       =   array(
                        'id'=>$data->id,
                        'headerName'=>$data->label,
                        'field'=>$data->field_name,
                        'width'=>100,
                        'freeze_col'=>$data->freeze_col,
                        'input_type'=>8,
                        'values'=>self::getOptions($request->table,$data->field_name)

                    );
                }
                else//1.Hidden, 2.Text , 3.TextArea, 4.Number, 5.Password and Others
                {
                    $array[]       =   array(
                        'id'=>$data->id,
                        'headerName'=>$data->label,
                        'field'=>$data->field_name,
                        'width'=>100,
                        'freeze_col'=>$data->freeze_col,
                        'input_type'=>$data->uni_fieldtype_id

                    );
                }


            }

            return array ('error'=>false,'response'=>$array,'message'=>'Success');

        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
    public static function getTableEditor($request, $user_id)
    {
        logger(5);
        try {
            ini_set('memory_limit', '2048M');
//            $columns = Schema::getColumnListing($request->table);
            logger(FilterHandler::getFilteredData($request->table, $user_id));
            $table  =   json_decode(DB::table($request->table)->whereIn('id', FilterHandler::getFilteredData($request->table, $user_id))->get(), true);
            $data_array = array();
            foreach($table as $data)
            {
                $field_array='{'; $count=0;
                foreach($data as $item=>$value2)
                {
                    if($count>0)
                    {
                        $field_array=$field_array.',';  
                    }
                    
                    //check if the value is numeric, if not then return the actual data or else do some check for foreign key and Formmater
                    $my_value=is_numeric($value2) ? self::getReferencedData($request->table,$item,$value2):$value2;

                    
                    $field_array=$field_array.'"'.$item.'":"'.$my_value.'"';
                    
                    $count++;
                }
                $field_array=$field_array.'}';
                $decoded_field_array=json_decode($field_array, true);
                $data_array[]=$decoded_field_array;
                
                
            }
           
        
            return array ('error'=>false,'response'=>$data_array,'message'=>'Success');

        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
    public static function getReferencedData($table, $field_name, $id)
    {
        //check if the field belongs to anothe table, then reurn its text which is name
        $table  =   FilterHandler::getPrimaryTable($table,$field_name);
        if($table!==false)
        {
            
            try{
                $res  =   json_decode(DB::table($table)->where('id', $id)->get(['name as name']), true);
                return count($res)>0?$res[0]['name']:$id;
            }
            catch(\Exception $e)
            {
                //im returning id for now becuase the referenced table does ot have 'name' column in it, therefore you must consider using formmater
                //TO: check if the field has a formmater, then get the formmated value
                $unitable = UniTable::where('name', $table)->get();
               
                if(count($unitable)>0)
                {
                    $uni_field_read =   UniFieldWrite::where('uni_table_id', $unitable[0]->id)->get();
                    
                    if(count($uni_field_read)>0)
                    {
                        // logger($uni_field_read);
                        //check reference rule
                        if($uni_field_read[0]->reference_rule!==NULL && $uni_field_read[0]->reference_rule!=='NULL' && $uni_field_read[0]->reference_rule!=='' && $uni_field_read[0]->reference_rule!==' ')
                        {
                            $ref    =   explode('.',$uni_field_read[0]->reference_rule);  

                            if(count($ref)===3)//if the reference rule have three words separed by dot
                            {
                                $_table   =     json_decode(DB::table($ref[0])->where($ref[1], $id)->get(), true);
                                if(count($_table)>0)
                                {
                                    return $_table[0][$ref[2]];
                                    
                                
                                }
                                
                            }
                            //TODO: have to do when words are two or one in the reference rule
                    
                        }
                        else{
                            //check for the formmater
                            if($uni_field_read[0]->output_rule!==NULL && $uni_field_read[0]->output_rule!=='NULL' && $uni_field_read[0]->output_rule!=='' && $uni_field_read[0]->output_rule!==' ')
                            {
                            return FieldFormatter::format($id,$uni_field_read[0]->output_rule);
                            }
                        }
                    }

                }
            return $id;
            }
        }
        else{
            REFRENCE_CHECK:
            
            //check if the reference rule is not null then get the data
            $unitable = UniTable::where('name', $table)->get();
            if(count($unitable)>0)
            {
                $uni_field_read =   UniFieldWrite::where('uni_table_id', $unitable[0]->id)->get();
                if(count($uni_field_read)>0)
                {
                    // logger($uni_field_read);
                     //check reference rule
                    if($uni_field_read[0]->reference_rule!==NULL && $uni_field_read[0]->reference_rule!=='NULL' && $uni_field_read[0]->reference_rule!=='' && $uni_field_read[0]->reference_rule!==' ')
                    {
                        $ref    =   explode('.',$uni_field_read[0]->reference_rule);  

                        if(count($ref)===3)//if the reference rule have three words separed by dot
                        {
                            $_table   =     json_decode(DB::table($ref[0])->where($ref[1], $id)->get(), true);
                            if(count($_table)>0)
                            {
                                
                                return $_table[0][$ref[2]];
                                
                            
                            }
                            
                        }
                        //TODO: have to do when words are two or one in the reference rule
                        
                    }
                    else{
                        
                        //check for the formmater
                        if($uni_field_read[0]->output_rule!==NULL && $uni_field_read[0]->output_rule!=='NULL' && $uni_field_read[0]->output_rule!=='' && $uni_field_read[0]->output_rule!==' ')
                        {
                           return FieldFormatter::format($id,$uni_field_read[0]->output_rule);
                        }
                    }
                }

            }
            return $id;
        }
        return $id;
    }
    public static function getOptions($table,$field)
    {
        $table  =   FilterHandler::getPrimaryTable($table,$field);
        if($table!==false)
        {
            $res  =   json_decode(DB::table($table)->get(['name as name']), true);
            $array=[];
            foreach($res as $data)
            {
                $array[]=$data['name'];

            }
            return $array;
        }
        else{
            return array();
        }
        
    }

}

?>


<?php

/* 
 * This specialization of the Panel class to render panel content on
 * xhtml complient browsers.
 * 
 */
namespace App\CFLibrary;

use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;
use App\CFLibrary\InputValidator;
use App\CFLibrary\FieldFormatter;
use Illuminate\Support\Facades\Auth;

class ViewerEditPanel
{
  const TVP_FILTERS_PER_ROW = 3;
  const EMPTY_TABLE_ID = '53EA782C-0C3A-424C-8A54-007F9139EF6F';
  public $searchTitle;
  public $filtersHideable;
  public $filtersHidden;
  public $filtersPerRow;
  /**
   * The properties of the data to display.
   *
   * @var array
   * @access public
   */
  var $fields;
  /**
   * The tables from which to get the data to display.
   *
   * @var string
   * @access public
   */
  var $tables;
  var $where;
  protected $table;
  protected $customSettings;

  protected $key;
  /**
   * @var int
   */
  protected $count;
  protected $contents;
  protected $header;
  protected $model;
  protected $categories;
  protected $table_id_set;
  
  function __construct($tables,$title, $fields, $where,$model,$categories, $customSettings, $table_id_set)
  {
    
    $this->table = $tables;
    $this->contents = '';
    $this->header = $title;
    $this->where = $where;
      
    $this->key = $customSettings['key'] ?? '';
    $this->fields = $fields;
    $this->tables = $tables;
    
    $this->model = $model;
    $this->categories = $categories;
    $this->table_id_set = $table_id_set;

    $this->customSettings = [
        'controls' => null,
        'searchFields' => null,
        'size' => 12,
        'useStartEndButtons' => false,
        'getParameters' => '',
        'current_page' => '',
        'searchTitle' => '',
        'hideFieldHeaders' => false,
        'filtersHidden' => false,
        'filtersHideable' => false,
        'hideNavigation' => false,
        'filtersPerRow' => self::TVP_FILTERS_PER_ROW,
        'initialSortOrder' => 'ASC',
        'initialSortField' => null,
        'removeSelectAll' => false,
        'footer' => '',
        'preFooter' => '',
        'header' => '',
        'bottomNavigation' => true
    ];

    foreach ($customSettings as $key => $value)
    {
      $this->customSettings[$key] = $value;
    }

  }
  
  public function getPanel(): array
  {
    $this->generateContents(true);

    return
        [
            'header' => $this->header,
            'content' => $this->contents,
            'footer' => null,
            'id' => null,
        ];
   }
   
   private function generateContents($showForm)
   {
       
        $pre = '';
        $post = '';
        $cats = $this->categories;
        $select_fields = array();
        foreach($cats as $cat){
            foreach ($this->fields[$cat] as $field)
            { 
                if(!empty($field['field_name']) ){
                    $select_fields[]['fieldname'] = $field['field_name'];
                    $select_fields[]['fieldtype'] = $field['field_type'];
                }
            }  
        }
        
        
        //no actions added yet to data except formatting
        $data = $this->getTableData($select_fields);
        $categories_tabs = $this->getCategorySetupTabs();
        //print_r("index ; ".$id_col_index);exit;
        //print_r($this->customSettings);
        $controls = $this->customSettings['controls'];
        
        //$managerLookupTable = FieldFormatter::format(1, "FIELD_FORMATTER_LOOKUP_HCBP");
        $managerLookupTable = '';
        $benchmarkLookupTable = FieldFormatter::format(1, "FIELD_FORMATTER_LOOKUP_BENCHMARK_TABLE");
        //$benchmarkLookupTable = htmlentities($benchmarkLookupTable, ENT_COMPAT,'ISO-8859-1', true);
        //$benchmarkLookupTable = json_encode($benchmarkLookupTable);
        //print_r($managerLookupTable);exit;
        
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/EditDataTable.twig',[
            'table_id' => $this->table.'_table',
            'table_description' => $this->header,
            'fields' => $this->fields,
            'data' =>$data,
            'buttons'=>$controls,
            'header_group'=>$categories_tabs,
            'select_fields' => $select_fields,
            'table_id_set' =>$this->table_id_set,
            'managerLookupTable' => $managerLookupTable,
            'benchmarkLookupTable' => $benchmarkLookupTable
        ]);
        $dataTable = $template_panel->show($template);
        //$dataTable = str_replace(array("\r", "\t", "\n"), '', $dataTable);
        $this->contents = $dataTable;
        
       
   }
   
    /**
    * @return array
    */
    private function getCategorySetupTabs()
    {
        $categories = array();
        $cats = $this->categories;
        $i=0;
        foreach($cats as $cat){
            $categories[$i]['name'] = $cat;
            $categories[$i]['tab_name'] = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $cat));        
            $i++;
        }
        
        return $categories;
    }
   
   
    /**
    * @return array
    */
    private function getTableData($select_fields)
    {
        /*Build the data table*/
        /* select the data to display from the database */
        $data = array();
        $row_cnt=0;
        $i=1;       
        
        $cats = $this->categories;
        $select_fields = array();
        $output_rules = array();
        foreach($cats as $cat){
            foreach ($this->fields[$cat] as $field)
            { 
                if(!empty($field['field_name'] && $field['omithorizontal'] ===0) ){
                    $select_fields[] = $field['field_name'];
                }
                
                $field_format = $field['output_rule'];
                if($field_format != 'None' && !empty($field_format) && $field_format != 'NULL' && $field['uni_fieldtype_id'] != 15 && $field['uni_fieldtype_id'] != 4){
                   $output_rules[$field['field_name']][] = $field_format;
                } 
            }  
        }
        
        $model = "App\\Models\\".$this->model;
        //print_r(FilterHandler::getFilteredData($this->table, Auth::user()->id));
        $query = $model::select($select_fields);

        if(count($this->where) > 0){
            //print_r($this->where);
            foreach($this->where as $key=>$where){
                //print_r($key);
                //print_r($where);
                
                if($key == 'or'){ 
                    foreach($where as $column => $value)
                    {
                        //print_r($column);
                        //print_r($value);
                        $this_where_or = $value;
                        //print_r($this_where_or);
                        if(is_array($this_where_or)){
                            foreach($this_where_or as $column => $value){
                                $query->orWhere($column, '=', $value);                           
                            }  
                        }
                        
                        
                    }

                }else{
                    foreach($where as $column => $value)
                    {
                        //print_r($column);
                        //print_r($value);
                        $query->where($column, '=', $value);
                    }


                }
            }
            
            
            
            /*foreach($this->where as $key=>$where){
                print_r($key);
                 print_r($where);
                if($key != 'or'){               
                    foreach($where as $column => $value)
                    {
                        $query->where($column, '=', $value);
                    }
                }else{
                    $this_where_or = $where;
                    foreach($this_where_or as $where){
                    
                        foreach($where as $column => $value)
                        {
                            $query->orWhere($column, '=', $value);
                        }
                    }  
                    
                }
            }*/
            
        }
        $query =   $query->whereIn('id', FilterHandler::getFilteredData($this->table, Auth::user()->id));
        $raw_data = $query->get();
        
        //print_r($raw_data);  
        foreach($raw_data->toArray() as $key=>$val)
        {
            foreach($val as $fieldId=>$value){
                
                if(isset($output_rules[$fieldId]) && !empty($output_rules[$fieldId])){
                    $field_format = $output_rules[$fieldId];
                    $field_format = $field_format[0];
                    if($field_format != 'None' && !empty($field_format) && $field_format != 'NULL'){
                        //print_r($field_format[0]);exit;
                        $value = FieldFormatter::format($value, $field_format);
                    } 
                }
                
                $data[$i][$fieldId] = $value;
                
            }
            $i++;
        }
        
        return $data;
    }
   
  public static function queryAlias()
  {
    return ' as /*B9E9B815F68D*/ ';
  }
  
   /**
   * @param $fieldName
   * @return bool|int
   */
  function getField($fieldName)
  {
    $cnt = 0;
    foreach ($this->fields as $field)
    {
      if ($field['field_name'] == $fieldName)
      {
        return $cnt;
      }
      $cnt++;
    }

    return false;
  }

  /**
   * @param $arr
   * @param $index
   * @return mixed
   */
  function arr_get_n($arr, $index)
  {
    $cnt = 0;
    foreach ($arr as $item)
    {
      if ($cnt == $index)
        return $item;
      $cnt++;
    }

    return null;
  }
  
}
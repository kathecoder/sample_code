<?php


namespace App\CFLibrary;

use App\CFLibrary\Model\UniFieldRead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\CFLibrary\Model\UniFieldWrite;
use App\CFLibrary\Model\UniFilters;
use App\CFLibrary\Model\UniTable;
use App\CFLibrary\Model\UniUserFiltration;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class FilterHandler
{
    public static function getFilterList($request)
    {
        //**TODO: this should be permision driven */
        
        try {
            $table  =   UniTable::where('name', $request->table)->get();
            if(count($table)===1)
            {
                $fields     =   UniFieldRead::where('uni_table_id', $table[0]->id)->where('uni_filtertype_id', '>',1)->get();

                $array  =   array();
                foreach($fields as $item)
                {
                    $array[]=array(
                        "id"=> $item->id,
                        "filter_title"=> ltrim($item->label, "#"),
                        "field_title"=> ltrim($item->label, "#"),
                        "uni_filter_type_id"=> $item->uni_filtertype_id,
                        "uni_table_id"=> $table[0]->id,
                        "field_name"=> $item->field_name
                    );
                }
                return array ('error'=>false,'response'=>$array,'message'=>'Success');
            }
            else
            {
                return array ('error'=>false,'response'=>[],'message'=>'No Table name called: '.$request->table);
            }

            // $read_fields    =   DB::select('SELECT uni_field_read.id, uni_field_read.label as filter_title,uni_field_read.label as field_title, uni_field_read.uni_filtertype_id as uni_filter_type_id, uni_field_read.uni_table_id as uni_table_id, uni_field_read.field_name as field_name FROM uni_field_read, uni_table WHERE uni_field_read.uni_filtertype_id!=1 AND uni_field_read.uni_table_id=uni_table.id AND uni_table.name="'.$request->table.'"');
            // return array ('error'=>false,'response'=>$read_fields,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
    public static function getFilters($request, $user_id){
        
        try {

            $data = [];
            $dataTotal = [];
            $date='';

            switch ($request->filter_type)
            {
                case 1: //No filter
                    break;
                case 2: //Combo multi select
                    $data       =   self::getFiltersForSelection($request->id, $user_id);
                    break;
                case 3: //Date Range
                    //$res         =  UniUserFiltration::where('user_id', Auth::user()->id)->where('uni_filter_id', $request->filter_id);
                    //$data        =  count($res)>0   ?   $res[0]->value  : '';
                    break;
                case 4: //Radio Button
                    $data        =   self::getFiltersForSelection($request->id, $user_id);
                    $dataTotal   =   self::getFiltersForSelection($request->id, $user_id);
                    break;
                case 5: //Check List
                    $data        =   self::getFiltersForSelection($request->id, $user_id);
                    $dataTotal   =   self::getFiltersForSelection( $request->id, $user_id);
                    break;
                case 6: //Combo No Option
                    $data       =   self::getFiltersForSelection($request->id, $user_id);
                    break;
                case 7: //Excell
                    $data        =   self::getFiltersForSelection($request->id, $user_id);
                    $dataTotal   =   self::getFiltersForSelection($request->id, $user_id);
                    break;
                default:
                    $data = [];
                    $dataTotal = [];
                    break;

            }

            $filterCount =   self::getFilterCount($request->id, $user_id);
            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$data,
                    'pageNate'=>self::paginate(count($dataTotal),10),
                    'totalTableData'=>count($dataTotal),
                    'filterCount'=>$filterCount,
                    'date'=>$date
                ),
                'message'=>'Filters successfully Loaded'
            );

        } catch (\Exception $e) {

            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
    public static function createFilters($request, $user_id){
        try {
            
            
            
            
            //removing filter from db
            $filter     =   UniUserFiltration::where('user_id', $user_id)
                            ->where('uni_field_read_id', $request->id);
            if($request->filter_type===5)
            {
                $filter ->where('value', $request->value);
            }
            

            $filter=$filter->get();
            if(count($filter)>0)
            {
                if($request->filter_type===2)
                {
                    foreach($filter as $data)
                    {
                        $remove =   UniUserFiltration::find($data->id);
                        $remove->delete(); 
                    }
                }
                else
                {
                    $remove =   UniUserFiltration::find($filter[0]->id);
                    $remove->delete();
                }
                
            }

            //*********inserting filter***************
            if($request->filter_type===7 || $request->filter_type===5 || $request->filter_type===4 || $request->filter_type===3 || $request->filter_type===6)
            {//insert all other filters thats are not multiselct
                $res                    =   new UniUserFiltration();
                $res->user_id           =   $user_id;
                $res->uni_field_read_id =   $request->id;
                $res->value             =   $request->value;
                $res->save();
            }
            if($request->filter_type===2)
            {//insert multi select
            
                foreach($request->value as $data)
                {
                    $res                =   new UniUserFiltration();
                    $res->user_id       =   $user_id;
                    $res->uni_field_read_id =   $request->id;
                    $res->value         =   $data['value'];
                    $res->save();
                }
            }

            return array ('error'=>false,'response'=>[],'message'=>'Success');

        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
    public static function clearFilters($user_id)
    {
        try {
            $res = UniUserFiltration::where('user_id', $user_id)->delete();
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
    public static function getFilteredData($table_name, $user_id)
    {
        $uniTable   =   UniTable::where('name', $table_name)->get();
        if(count($uniTable)===1)
        {
           $table   =   DB::table($table_name);
           $filter  =   UniUserFiltration::where('user_id', $user_id)->get();
           $array   =   array();
           $array2   =   array();
           foreach($filter as $fl)
           {
               $uniFieldRead    =   UniFieldRead::find($fl->uni_field_read_id);
               if($uniFieldRead->uni_filtertype_id!==3)//combo
               {
                    
                    if(self::sortArray($array, $uniFieldRead->field_name)===true)
                    {//logger('orWhere '.$uniFieldRead->field_name.'-->'. $fl->value);
                        $table->orWhere($uniFieldRead->field_name, $fl->value);
                    }
                    else if(self::sortArray($array, $uniFieldRead->field_name)===false)
                    {//logger('Where '.$uniFieldRead->field_name.'-->'. $fl->value);
                        $table->where($uniFieldRead->field_name, $fl->value);
                    } 
                    $array[]=$uniFieldRead->field_name;
                    $array2[]=$fl->value;
                    
               }
               else if($uniFieldRead->uni_filtertype_id===3)//date range
               {

                    $date = explode(' / ',$fl->value);
                    $table->whereBetween('created_at', [$date[0], $date[1]]);
                   
               }
               
              
           }

            self::interceptFiltration($table);

            return self::convertJsonIntoArray($table->get(['id']));

        }
        logger('TODO: Handle response when there is no table found');
        
       
    }
    public static function getFiltersForSelection($id, $user_id)
    {
        $uniFieldRead   =   UniFieldRead::find($id);
        $table          =   UniTable::find($uniFieldRead->uni_table_id);

        $field          =   DB::table($table->name)->get(['id',$uniFieldRead->field_name.' as name']);

        $array          =   array();
        $sort_array     =   array();
        
        foreach($field as $item)
        {
            $name=0;
            $_id=0;
            if(is_numeric($item->name))
            {
                $refTable=self::getPrimaryTable($table->name,$uniFieldRead->field_name);
                if($refTable!==false)
                {
                    $res=DB::table($refTable)->where('id', $item->name)->get();
                    if(count($res)>0)
                    {
                        $name=$res[0]->name; 
                        $_id=$item->name; 
                    }
                    else{
                        continue;
                    }
                
                }
                else
                { 
                     //check reference rule
                    if($uniFieldRead->reference_rule!==NULL && $uniFieldRead->reference_rule!=='NULL' && $uniFieldRead->reference_rule!=='' && $uniFieldRead->reference_rule!==' ')
                    {
                        $ref    =   explode('.',$uniFieldRead->reference_rule);
                       
                        if(count($ref)===3)
                        {
                            $_table   =     json_decode(DB::table($ref[0])->where($ref[1], $item->name)->get(), true);
                            if(count($_table)>0)
                            {
                                $name=$_table[0][$ref[2]];
                                $_id=$item->name; 
                               
                            }
                            else
                            {
                                $name=$item->name;
                                $_id=$item->id; 
                            }
                        }
                        else if(count($ref)===2)
                        {
                            $_table   =     json_decode(DB::table($ref[0])->where('id', $item->name)->get(), true);
                            if(count($_table)>0)
                            {
                                $name=$_table[0][$ref[1]];
                                $_id=$item->name; 
                               
                            }
                            else
                            {
                                $name=$item->name;
                                $_id=$item->id; 
                            } 
                        }
                        else if(count($ref)===1)
                        {
                            $_table   =     json_decode(DB::table($ref[0])->where('id', $item->name)->get(), true);
                            if(count($_table)>0)
                            {
                                $name=$_table[0]['name'];
                                $_id=$item->name; 
                               
                            }
                            else
                            {
                                $name=$item->name;
                                $_id=$item->id; 
                            } 
                        }
                        else
                        {
                            $name=$item->name;
                            $_id=$item->id; 
                        }
                        
                       
                    }
                    else
                    {
                       if($uniFieldRead->output_rule!==NULL)
                        {
                            $value = FieldFormatter::format($item->name,$uniFieldRead->output_rule);//$field, $type, $id = NULL, $field_name = NULL
                            $name=$value;
                            $_id=$item->name; 
                           
                        }
                        //check formmater
                        
                        else 
                        {
                            $name=$item->name;
                            $_id=$item->id; 
                        } 
                    }
                    
                   
                   
                    
                }
            }
            else
            {
                $name=$item->name;
                $_id=$item->id; 
            }

            //sort array
            if(self::sortArray($sort_array, $name)===false)
            {
                $sort_array[]=$name;
                $array[]            =   array(
                    "id"            =>  $_id,
                    "value"         => $_id,
                    "name"          =>  $name, 
                    "label"         =>  $name, 
                    "defaultChecked"=>  self::defaultChecked($id,$_id, $user_id)
                );
            }
               
            
        }



        return $array;
        
    }
    public static function sortArray($array, $item)
    {
        foreach($array as $data)
        {
            if($data===$item)
            {
                return true;
            }
        }
        return false;
    }
    public static function getUniqueArrayOfID($array)
    {

        $id = [];
        foreach($array as $item)
        {
            $id[]=$item->name;
        }
        return array_unique($id);
    }
    public static function getPrimaryTable($table, $col)
    {
        $res    =   DB::select("select distinct COLUMN_NAME as ref, referenced_table_name as primary_table from information_schema.key_column_usage where referenced_table_name is not null and table_name = '".$table."' AND COLUMN_NAME='".$col."' order by primary_table");
        return count($res)>0 ? $res[0]->primary_table : false;
    }








    public static function getFiltersForCombo($id, $user_id)
    {
        $uniFieldRead   =   UniFieldRead::find($id);
        $table          =   UniTable::find($uniFieldRead->uni_table_id);

        $field          =   DB::table($table->name)->get(['id',$uniFieldRead->field_name.' as name']);

        $array          =   array();
        $sort_array     =   array();
        
        foreach($field as $item)
        {
            $name=0;
            if(is_numeric($item->name))
            {
                $refTable=self::getPrimaryTable($table->name,$uniFieldRead->field_name);
                if($refTable!==false)
                {
                    $res=DB::table($refTable)->where('id', $item->name)->get();
                    if(count($res)>0)
                    {
                        $name=$res[0]->name; 
                    }
                    else{
                        continue;
                    }
                
                }
                else
                {
                    $name=$item->name;
                }
            }
            else
            {
                $name=$item->name;
            }

            //sort array
            if(self::sortArray($sort_array, $name)===false)
            {
                $sort_array[]=$name;
                $array[]            =   array(
                    "id"            =>  $item->id,
                    "name"          =>  $name, //get the field name if the value is numeric and when the colmn is a foreign key from another table
                    "defaultChecked"=>  self::defaultChecked($id,$item->id, $user_id)
                );
            }
            
        }



        return $array;
    }
    public static function defaultCheckedForCombo($uni_filter_id, $value, $user_id)
    {
        $filter     =   UniUserFiltration::where('user_id', $user_id)
                        ->where('uni_filter_id', $uni_filter_id)
                        ->where('value', $value)
                        ->get();
      
        return count($filter)>0 ? $filter[0]->value  :   null;
    }
    public static function defaultChecked($uni_filter_id, $value, $user_id)
    {
        $filter     =   UniUserFiltration::where('user_id',$user_id)
            ->where('uni_field_read_id', $uni_filter_id)
            ->where('value', $value)
            ->get();
        return count($filter)>0 ? true  :   false;
    }
    public static function getFilterCount($filter_id, $user_id)
    {
        $filter     =   UniUserFiltration::
        where('user_id', $user_id)
            ->where('uni_field_read_id', $filter_id)
            ->get();
        return count($filter);
    }
    public static function paginate($total,$limitPerPage)
    {
        $results = array();

        $count=0;
        for($i=0; $i<$total;){
            $results[$count] = $i;
            $count= $count+1;
            $i=$i+$limitPerPage;
        }

        return $results;
    }
    public static function convertJsonIntoArray($json){
        $array = [];
        foreach ($json as $data){
            $array[] = $data->id;
        }
        return $array;
    }
    public static function convertJsonIntoArray2($json){

        $array = '';
        $count = 0;
        foreach ($json as $data){
            if($count===0){
                $array = $array. ''.$data->id;
            }else{
                $array = $array. ','.$data->id;
            }
            $count++;

        }
        return $array;
    }
    public static function convertJsonIntoArrayIndex2($json){
        $array = '';
        $count = 0;
        foreach ($json as $data){
            if($count===0){
                $array = $array. ''.$data['id'];
            }else{
                $array = $array. ','.$data['id'];
            }
            $count++;

        }
        return $array;
    }
    public static function convertJsonIntoArrayIndex($json){
        if(count($json)>0){
            $array = [];
            foreach ($json as $data){
                $array[] = $data['id'];
            }
            return $array;
        }else{
            return [];
        }


    }
    public static function searchArray($array, $value){
        foreach ($array as $item){
            if($item===$value){
                return true;
            }
        }
        return false;
    }

    public static function interceptFiltration($table){
        $custom_filtered_ids = Session::get('custom_filtered_ids');
        if ($custom_filtered_ids !== null && $custom_filtered_ids === 0){ // accounttype = [1,2,3]
            return self::convertJsonIntoArray($table->get(['id']));
        }
        elseif ($custom_filtered_ids !== null && count($custom_filtered_ids) > 0){
            $filtered_data = self::convertJsonIntoArray($table->get(['id']));
            $result = array_intersect($filtered_data, $custom_filtered_ids);
            return $result;
        }
        elseif ($custom_filtered_ids !== null && count($custom_filtered_ids) === 0){
            return [];
        }
    }

}
?>
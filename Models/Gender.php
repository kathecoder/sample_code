<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $table = 'genders';

    public static function getAllRecords($searchInput,$skip,$take, $is_paginate, $filterFilters=false)
    {
        $ids = Filters::getFilterFilters('gender_id');

        $res    =   Gender::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', $ids);}

        $res->orderBy('name');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }
        return $res->get();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstanceIndicator extends Model
{
    protected $table = 'instance_indicators';
}

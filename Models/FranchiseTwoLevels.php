<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FranchiseTwoLevels extends Model
{
    protected $table = "franchise_two_levels";
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProcessOverview extends Model
{
    protected $table = 'process_overviews';
}

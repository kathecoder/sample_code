<?php


namespace App\Models;


use App\Exports\CustomEarningDistributionExport;
use App\Exports\CustomExport;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class EarningsDistribution
{

    public static function getIncentiveByBandRatingTotal(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }
        $records->select(
            'band_id',
            DB::raw('
            count(*) as total_band_head_count,
            ROUND(SUM(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) THEN calc_proposedsalary_zar END), 1) as total_proposedsalary_zar,
            ROUND(SUM(CASE WHEN (do_not_process_bonus IS NOT NULL OR do_not_process_bonus !=1) THEN revised_pro_rated_incentive_value END), 1) as total_revised_pro_rated_incentive_value,
            ROUND(AVG(proposedsalary_percentage), 1) as total_avg_proposedsalary_percentage
            ')
        )

            ->whereNotNull('band_id')
            ->groupBy('band_id');


        $records = $records->get();
        return $records;

    }
    public static function getIncentiveByBandRating(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn         = $manager_pn->personnelnumber;

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
        $records->select(
            'band_id',
            'current_rating_id',
            DB::raw('
            count(*) as total_band_head_count,
            ROUND(SUM(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) THEN calc_proposedsalary_zar END), 1) as total_proposedsalary_zar,
            ROUND(SUM(CASE WHEN (do_not_process_bonus IS NOT NULL OR do_not_process_bonus !=1) THEN revised_pro_rated_incentive_value END), 1) as total_revised_pro_rated_incentive_value,
            ROUND(AVG(proposedincentive_percentage), 1) as total_avg_proposedsalary_percentage
            ')
        )

            ->whereNotNull('band_id')
            ->whereNotNull( 'current_rating_id')
            ->orderBy('band_id', 'ASC')
            ->orderBy('current_rating_id', 'ASC')
            ->groupBy('band_id')
            ->groupBy('current_rating_id');

        $records = $records->get();
        return $records;

    }
    public static function getIncentiveByBandRatingGrndTotal(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn         = $manager_pn->personnelnumber;

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
        $records->select(
            DB::raw('
            count(*) as total_band_head_count,
            ROUND(SUM(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) THEN calc_proposedsalary_zar END), 1) as total_proposedsalary_zar,
            ROUND(SUM(CASE WHEN (do_not_process_bonus IS NOT NULL OR do_not_process_bonus !=1) THEN revised_pro_rated_incentive_value END), 1) as total_revised_pro_rated_incentive_value,
            ROUND(AVG(proposedsalary_percentage), 1) as total_avg_proposedsalary_percentage
            ')
        );


        $records = $records->get();
        return $records;

    }


    public static function getIncentiveByBusinessArea(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }
        $records->select(
            'orglevel5',
            DB::raw('
            count(CASE WHEN scheme_id IN (20,23, 43) THEN 1 ELSE 0 END) as total_band_head_count,
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) as calc_kpi_incentive_value,
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN financial_incentive_value END)  as financial_incentive_value,
            
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN financial_incentive_value END) + 
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) as total_incentive_value,
            
            ROUND((sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) / 
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_salary_zar * (calc_non_financial / 100) END)) * 100, 1) as allocated_percentage  
            
            ')
        )

            ->whereNotNull('orglevel5')
            ->groupBy('orglevel5');


        $records = $records->whereIn('scheme_id', [20,23, 43])->get();
        return $records;

    }
    public static function getIncentiveByBusinessAreaGrandTotal(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }
        $records->select(
            DB::raw('
            count(CASE WHEN scheme_id IN (20,23, 43) THEN 1 ELSE 0 END) as total_band_head_count,
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) as calc_kpi_incentive_value,
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN financial_incentive_value END)  as financial_incentive_value,
            
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN financial_incentive_value END) + 
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) as total_incentive_value,
            
            ROUND((sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) / 
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_salary_zar * (calc_non_financial / 100) END)) * 100, 1) as allocated_percentage 
            ')
        );


        $records = $records->whereIn('scheme_id', [20,23, 43])->get();
        return $records;

    }
    public static function getAllStaffIncentiveByBA(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }
        $records->select(
            'orglevel5',
            DB::raw('
            count(*) as total_head_count,
            sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN calc_salary_zar END) as total_salary_value_zar,
            sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_proposedsalary_zar,
            
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_proposedincentive_zar,
            
            AVG( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN proposedsalary_percentage END) as total_avg_proposedsalary_percentage, 
            AVG( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar - calc_salary_zar END)  as total_avg_proposedsalary_zar

            
            ')
        )

            ->whereNotNull('orglevel5')
            ->groupBy('orglevel5');


        $records = $records->get();
        return $records;
    }


    public static function getAllStaffIncentiveByBABandCurrentRating(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }
        $records->select(
            'orglevel5',
            'band_id',
            'current_rating_id',
            DB::raw('
            count(*) as total_head_count,
            sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN calc_salary_zar END) as total_salary_value_zar,
            sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_proposedsalary_zar,
            
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_proposedincentive_zar,
            
            AVG( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_avg_proposedsalary_zar,

            AVG( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_avg_proposedincentive_zar
            
            ')
        )

            ->whereNotNull('orglevel5')
            ->whereNotNull('band_id')
            ->whereNotNull( 'current_rating_id')
            ->orderBy('band_id', 'ASC')
            ->orderBy('current_rating_id', 'ASC')
            ->groupBy('orglevel5')
            ->groupBy('band_id')
            ->groupBy('current_rating_id');

        $records = $records->get();
        return $records;
    }

    public static function getAllStaffIncentiveByBABand(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }
        $records->select(
            'orglevel5',


            DB::raw('
            SUM( CASE WHEN (band_id = 1 OR band_id = 2) THEN 1 ELSE 0 END) as total_head_count_b1b2,
            SUM( CASE WHEN (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN calc_salary_zar END) as total_salary_value_zar_b1b2,
            SUM( CASE WHEN (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_proposedsalary_zar_b1b2,
            SUM( CASE WHEN (band_id = 1 OR band_id = 2) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_proposedincentive_zar_b1b2,
            AVG( CASE WHEN (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_avg_proposedsalary_zar_b1b2,
            AVG( CASE WHEN (band_id = 1 OR band_id = 2) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_avg_proposedincentive_zar_b1b2,
            
            
            
            
            
            SUM( CASE WHEN (band_id = 3) THEN 1 ELSE 0 END) as total_head_count_b3,
            SUM( CASE WHEN (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN calc_salary_zar END) as total_salary_value_zar_b1b2,
            SUM( CASE WHEN (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_proposedsalary_zar_b1b2,
            SUM( CASE WHEN (band_id = 3) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_proposedincentive_zar_b1b2,
            AVG( CASE WHEN (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_avg_proposedsalary_zar_b1b2,
            AVG( CASE WHEN (band_id = 3) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_avg_proposedincentive_zar_b1b2,
            
            
            
            
            
            SUM( CASE WHEN (band_id = 4 OR band_id = 5) THEN 1 ELSE 0 END) as total_head_count_b4b5,
            SUM( CASE WHEN (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN calc_salary_zar END) as total_salary_value_zar_b1b2,
            SUM( CASE WHEN (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_proposedsalary_zar_b1b2,
            SUM( CASE WHEN (band_id = 4 OR band_id = 5) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_proposedincentive_zar_b1b2,
            AVG( CASE WHEN (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_avg_proposedsalary_zar_b1b2,
            AVG( CASE WHEN (band_id = 4 OR band_id = 5) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_avg_proposedincentive_zar_b1b2
            
            ')
        )

            ->whereNotNull('orglevel5')
            ->groupBy('orglevel5');

        $records = $records->get();
        return $records;
    }

    public static function getAllStaffIncentiveByBABands($bands){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }
        $records->select(
            'orglevel5',
            DB::raw('
            count(*) as total_head_count,
            sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN calc_salary_zar END) as total_salary_value_zar,
            sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_proposedsalary_zar,
            
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_proposedincentive_zar,
            
            AVG( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN proposedsalary_percentage END) as total_avg_proposedsalary_percentage,
            
            AVG( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_avg_proposedsalary_zar
            ')
        )

            ->whereIn('band_id', $bands)
            ->groupBy('orglevel5')
        ;

        $records = $records->get();
        return $records;
    }

    public static function getAllStaffIncentiveByBABandsRatings($bands){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }
        $records->select(
            'orglevel5',
            'current_rating_id',
            DB::raw('
            
            count(*) as total_head_count,
            sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN calc_salary_zar END) as total_salary_value_zar,
            sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_proposedsalary_zar,
            
            sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar END) as total_proposedincentive_zar,
            
            AVG( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN proposedsalary_percentage END) as total_avg_proposedsalary_percentage,
            
            AVG( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END)  as total_avg_proposedsalary_zar

            ')
        )

            ->whereIn('band_id', $bands)
            ->groupBy('orglevel5')
            ->groupBy('current_rating_id');

        $records = $records->get();
        return $records;
    }


    public static function buildExportAllStaffIncentives(){
        $headers = ['Row Labels','Headcount FY2020','Sum of Current Annual Salary (ZAR)',
            'Sum of New Annual Salary (ZAR)','Sum of Total Bonus Value (ZAR)','Average of Proposed Salary Increase %','Average increase'];
        $calc_orglevel5             = EarningsDistribution::getAllStaffIncentiveByBA();
        $current_ratings            = CurrentRating::all()->pluck('name', 'id')->toArray();

        $business_area_B1B2s        = EarningsDistribution::getAllStaffIncentiveByBABands( [1, 2]);
        $business_area_rating_B1B2  = EarningsDistribution::getAllStaffIncentiveByBABandsRatings([1, 2]);

        $business_area_B3           = EarningsDistribution::getAllStaffIncentiveByBABands([3]);
        $business_area_rating_B3    = EarningsDistribution::getAllStaffIncentiveByBABandsRatings([3]);

        $business_area_B4B5s        = EarningsDistribution::getAllStaffIncentiveByBABands([4,5]);
        $business_area_rating_B4B5  = EarningsDistribution::getAllStaffIncentiveByBABandsRatings([4, 5]);

        $rows = [];
        foreach ($calc_orglevel5 as $item){

            $item->orglevel5                                = $item->orglevel5;
            $item->total_head_count                         = $item->total_head_count;
            $item->total_salary_value_zar                   = number_format($item->total_salary_value_zar, 0, '.', ' ') ;
            $item->total_proposedsalary_zar                 = number_format($item->total_proposedsalary_zar, 0, '.', ' ');
            $item->total_proposedincentive_zar              = number_format($item->total_proposedincentive_zar, 0, '.', ' ');
            $item->total_avg_proposedsalary_percentage      = number_format($item->total_avg_proposedsalary_percentage, 1, '.', ' ');
            $item->total_avg_proposedsalary_zar             = number_format($item->total_avg_proposedsalary_zar, 0, '.', ' ');

            $rows[] = (array)$item;

            if ($business_area_B1B2s){
                foreach ($business_area_B1B2s as $business_area_B1B2){

                    if ($business_area_B1B2->orglevel5 == $item->orglevel5){
                        $business_area_B1B2_ = $business_area_B1B2;
                        $business_area_B1B2_->orglevel5                             = "   B1 & B2";
                        $business_area_B1B2_->total_head_count                      = $business_area_B1B2_->total_head_count;
                        $business_area_B1B2_->total_salary_value_zar                = number_format($business_area_B1B2_->total_salary_value_zar, 0, '.', ' ');
                        $business_area_B1B2_->total_proposedsalary_zar              = number_format($business_area_B1B2_->total_proposedsalary_zar, 0, '.', ' ');
                        $business_area_B1B2_->total_proposedincentive_zar           = number_format($business_area_B1B2_->total_proposedincentive_zar, 0, '.', ' ');
                        $business_area_B1B2_->total_avg_proposedsalary_percentage   = number_format($business_area_B1B2_->total_avg_proposedsalary_percentage, 1, '.', ' ');
                        $business_area_B1B2_->total_avg_proposedsalary_zar          =  number_format($business_area_B1B2_->total_avg_proposedsalary_zar, 0, '.', ' ');

                        $rows[] =  (array)$business_area_B1B2_;
                    }
                }
                /***  Third Level  B1 & B2 ***/
                foreach ($business_area_rating_B1B2 as $rating_B1B2){
                    if ($rating_B1B2->orglevel5 == $item->orglevel5){
                        $current_rating_id =  $current_ratings[$rating_B1B2->current_rating_id];

                        $rating_B1B2->orglevel5                             = "      $current_rating_id";
                        $rating_B1B2->total_head_count                      = $rating_B1B2->total_head_count;
                        $rating_B1B2->total_salary_value_zar                = number_format($rating_B1B2->total_salary_value_zar, 0, '.', ' ');
                        $rating_B1B2->total_proposedsalary_zar              = number_format($rating_B1B2->total_proposedsalary_zar, 0, '.', ' ');
                        $rating_B1B2->total_proposedincentive_zar           = number_format($rating_B1B2->total_proposedincentive_zar, 0, '.', ' ');
                        $rating_B1B2->total_avg_proposedsalary_percentage   = number_format($rating_B1B2->total_avg_proposedsalary_percentage, 1, '.', ' ');
                        $rating_B1B2->total_avg_proposedsalary_zar          = number_format($rating_B1B2->total_avg_proposedsalary_zar, 0, '.', ' ');

                        unset($rating_B1B2->current_rating_id); // not needed in the export
                        $rows[] =  (array)$rating_B1B2;
                    }
                }
            }

            //////////////////////////////////////////////////////////   B3   ///////////////////////////////////////////////////////////////////////////
            /***  Second Level  B3 ***/

            if ($business_area_B3){
                foreach ($business_area_B3 as $business_area_B3z){
                    if ($business_area_B3z->orglevel5 == $item->orglevel5){
                        $business_area_B3z_ = $business_area_B3z;

                        $business_area_B3z_->orglevel5                             = "   B3";
                        $business_area_B3z_->total_head_count                      = $business_area_B3z_->total_head_count;
                        $business_area_B3z_->total_salary_value_zar                = number_format($business_area_B3z_->total_salary_value_zar, 0, '.', ' ');
                        $business_area_B3z_->total_proposedsalary_zar              = number_format($business_area_B3z_->total_proposedsalary_zar, 0, '.', ' ');
                        $business_area_B3z_->total_proposedincentive_zar           = number_format($business_area_B3z_->total_proposedincentive_zar, 0, '.', ' ');
                        $business_area_B3z_->total_avg_proposedsalary_percentage   = number_format($business_area_B3z_->total_avg_proposedsalary_percentage, 1, '.', ' ');
                        $business_area_B3z_->total_avg_proposedsalary_zar          =  number_format($business_area_B3z_->total_avg_proposedsalary_zar, 0, '.', ' ');

                        $rows[] = (array)$business_area_B3z_;

                    }
                }
                /***  Third Level  B3 ***/
                foreach ($business_area_rating_B3 as $rating_B3){
                    if ($rating_B3->orglevel5 == $item->orglevel5){
                        $current_rating_id =  $current_ratings[$rating_B3->current_rating_id];

                        $rating_B3->orglevel5                             = "      $current_rating_id";
                        $rating_B3->total_head_count                      = $rating_B3->total_head_count;
                        $rating_B3->total_salary_value_zar                = number_format($rating_B3->total_salary_value_zar, 0, '.', ' ');
                        $rating_B3->total_proposedsalary_zar              = number_format($rating_B3->total_proposedsalary_zar, 0, '.', ' ');
                        $rating_B3->total_proposedincentive_zar           = number_format($rating_B3->total_proposedincentive_zar, 0, '.', ' ');
                        $rating_B3->total_avg_proposedsalary_percentage   = number_format($rating_B3->total_avg_proposedsalary_percentage, 1, '.', ' ');
                        $rating_B3->total_avg_proposedsalary_zar          = number_format($rating_B3->total_avg_proposedsalary_zar, 0, '.', ' ');

                        unset($rating_B3->current_rating_id); // not needed in the export
                        $rows[] =  (array)$rating_B3;
                    }
                }
            }

            /////////////////////////////////////////////////////// B4 & B5 //////////////////////////////////////////////////////////////////////////////

            /***  2nd Level  B4 & B5 ***/

            if ($business_area_B4B5s){
                foreach ($business_area_B4B5s as $business_area_B4B5){
                    if ($business_area_B4B5->orglevel5 == $item->orglevel5){
                        $business_area_B4B5_ = $business_area_B4B5;

                        $business_area_B4B5_->orglevel5                             = "   B4 & B5";
                        $business_area_B4B5_->total_head_count                      = $business_area_B4B5_->total_head_count;
                        $business_area_B4B5_->total_salary_value_zar                = number_format($business_area_B4B5_->total_salary_value_zar, 0, '.', ' ');
                        $business_area_B4B5_->total_proposedsalary_zar              = number_format($business_area_B4B5_->total_proposedsalary_zar, 0, '.', ' ');
                        $business_area_B4B5_->total_proposedincentive_zar           = number_format($business_area_B4B5_->total_proposedincentive_zar, 0, '.', ' ');
                        $business_area_B4B5_->total_avg_proposedsalary_percentage   = number_format($business_area_B4B5_->total_avg_proposedsalary_percentage, 1, '.', ' ');
                        $business_area_B4B5_->total_avg_proposedsalary_zar          =  number_format($business_area_B4B5_->total_avg_proposedsalary_zar, 0, '.', ' ');

                        $rows[] =  (array)$business_area_B4B5_;
                    }
                }

                /***  Third Level  B4 & B5 ***/
                foreach ($business_area_rating_B4B5 as $rating_B4B5){
                    if ($rating_B4B5->orglevel5 == $item->orglevel5){
                        $current_rating_id =  $current_ratings[$rating_B4B5->current_rating_id];

                        $rating_B4B5->orglevel5                             = "      $current_rating_id";
                        $rating_B4B5->total_head_count                      = $rating_B4B5->total_head_count;
                        $rating_B4B5->total_salary_value_zar                = number_format($rating_B4B5->total_salary_value_zar, 0, '.', ' ');
                        $rating_B4B5->total_proposedsalary_zar              = number_format($rating_B4B5->total_proposedsalary_zar, 0, '.', ' ');
                        $rating_B4B5->total_proposedincentive_zar           = number_format($rating_B4B5->total_proposedincentive_zar, 0, '.', ' ');
                        $rating_B4B5->total_avg_proposedsalary_percentage   = number_format($rating_B4B5->total_avg_proposedsalary_percentage, 1, '.', ' ');
                        $rating_B4B5->total_avg_proposedsalary_zar          = number_format($rating_B4B5->total_avg_proposedsalary_zar, 0, '.', ' ');

                        unset($rating_B4B5->current_rating_id); // not needed in the export
                        $rows[] =  (array)$rating_B4B5;
                    }
                }
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        }

        $export =  new CustomEarningDistributionExport($rows, $headers);

        return $export;


    }

    public static function buildExportSMRiskSpecialistIncentive(){
        $headers = ['Business Area','Headcount FY2020','Sum Non-Financial (Discretionary/KPI) Allocation value',
            'Sum of Financial Value','Sum of Total Bonus Value FY2020','% of Discretionary pool allocated'];

        $incentive_orglevel5    = EarningsDistribution::getIncentiveByBusinessArea();
        //$grandTotals            = EarningsDistribution::getIncentiveByBusinessAreaGrandTotal();

        $rows = [];


        foreach($incentive_orglevel5 as $item){
            $calc_kpi_incentive_value   = $item->calc_kpi_incentive_value !== null ? $item->calc_kpi_incentive_value : 0;
            $financial_incentive_value  = $item->financial_incentive_value !== null ? $item->financial_incentive_value : 0;
            $total_incentive_value      = $calc_kpi_incentive_value + $financial_incentive_value;

            $array_temp = array(
                'orglevel5'                 => $item->orglevel5,
                'total_band_head_count'     => $item->total_band_head_count,
                'calc_kpi_incentive_value'  => number_format($item->calc_kpi_incentive_value, 0, '.', ' '),
                'financial_incentive_value' => number_format($item->financial_incentive_value, 0, '.', ' '),
                'total_incentive_value'     => number_format($total_incentive_value, 0, '.', ' '),
                'allocated_percentage'      => $item->allocated_percentage,
            );

            $rows [] = $array_temp;

        }

        $export =  new CustomEarningDistributionExport($rows, $headers);

        return $export;

    }

    public static function buildExportBandRatings(){
        $headers = ['Salary band & performance rating','Headcount FY2020','Sum of New Annual Salary (ZAR)',
            'Sum of Revised Pro-rated Incentive Value','Average Bonus as % of salary bill'];

        $incentive_band_rating_totals   = EarningsDistribution::getIncentiveByBandRatingTotal();
        $incentive_band_rating          = EarningsDistribution::getIncentiveByBandRating();
        $bands                          = Band::all()->pluck('name', 'id')->toArray();
        $current_ratings                 = CurrentRating::all()->pluck('name', 'id')->toArray();

        $rows = [];


        foreach ($incentive_band_rating_totals as $top_record){

            $array_temp = array(
                'orlglevel5'                                => $bands[$top_record->band_id],
                'total_band_head_count'                     => $top_record->total_band_head_count,
                'total_proposedsalary_zar'                  => number_format($top_record->total_proposedsalary_zar, 0, '.', ' ') ,
                'total_revised_pro_rated_incentive_value'   => number_format($top_record->total_revised_pro_rated_incentive_value, 0, '.', ' '),
                'total_avg_proposedsalary_percentage'       => $top_record->total_avg_proposedsalary_percentage,
            );

            $rows[] = $array_temp;


            foreach ($incentive_band_rating as $item){
                if ($top_record->band_id == $item->band_id){

                    $array_temp2 = array(
                        'orlglevel5'                                => "    " . $current_ratings[$item->current_rating_id],
                        'total_band_head_count'                     => $item->total_band_head_count,
                        'total_proposedsalary_zar'                  => number_format($item->total_proposedsalary_zar, 0, '.', ' ') ,
                        'total_revised_pro_rated_incentive_value'   => number_format($item->total_revised_pro_rated_incentive_value, 0, '.', ' '),
                        'total_avg_proposedsalary_percentage'       => $item->total_avg_proposedsalary_percentage,
                    );

                    $rows[] = $array_temp2;
                }
            }
        }




        $export =  new CustomEarningDistributionExport($rows, $headers);

        return $export;


    }

    public static function buildExportTop20Earners(){
        $headers = ['Rank','Name','Performance Rating','FY2019 Annual Salary',
            '2020 Annual Salary','Incr %','FY2019 Bonus','FY2020 Bonus','% Bonus Variance' ];

        $dbTopTwenty    = PerformanceRating::getTopTwentyEarners();

        $rows = [];

        $count = 0;
        foreach ($dbTopTwenty as $record){
            $count = $count + 1;
            $array_temp = array(
                'rank'                      =>    $count,
                'name'                      =>    $record->firstnames . " " . $record->lastname,
                'current_rating'            =>    $record->current_rating,
                'h_1_salary_zar'            =>    number_format($record->h_1_salary_zar, 0,'.', ' '),
                'calc_salary_zar'           =>    number_format($record->calc_salary_zar, 0,'.', ' '),
                'proposedsalary_percentage' =>    number_format($record->proposedsalary_percentage, 0,'.', ' '),
                'h_1_incentive_zar'         =>    number_format($record->h_1_incentive_zar, 0,'.', ' '),
                'calc_proposedincentive_zar'=>    number_format($record->calc_proposedincentive_zar, 0,'.', ' '),
                'bonus_variance'            =>    round($record->bonus_variance,1),
            );

            $rows[] = $array_temp;

        }
        $export =  new CustomEarningDistributionExport($rows, $headers);

        return $export;


    }

}
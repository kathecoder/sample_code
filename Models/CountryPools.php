<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryPools extends Model
{
    protected $table = 'country_pools';
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Country extends Model
{
    protected $table = 'countries';


    public static function getALlCountries($searchInput,$skip,$take, $is_paginate, $filterFilters=false)
    {
        $ids = Filters::getFilterFilters('country_id');

        $res    =   Country::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', $ids);}

        $res->orderBy('name');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }
        return $res->get();
    }

}

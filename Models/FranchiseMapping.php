<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FranchiseMapping extends Model
{
    protected $table = "franchise_mappings";



    public static function getFranchiseMappings($searchInput,$skip,$take, $is_paginate, $status =null, $clause = ""){

        $records = DB::table('vw_franchise_mapping');
        $records =   $records->where(function($query) use ($searchInput){
            $query
                ->where('incentive_scheme', 'like', '%'.$searchInput.'%')
                ->orWhere('franchise_one_level', 'like', '%'.$searchInput.'%')
                ->orWhere('franchise_two_level', 'like', '%'.$searchInput.'%')
                ->orWhere('franchise_three_level', 'like', '%'.$searchInput.'%');
        });

        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }

        return $records->get();

    }
}

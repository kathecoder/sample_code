<?php

namespace App\Models;

use App\CFLibrary\FilterHandler;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Benchmark extends Model
{
    protected $table = 'benchmarks';



    public static function getRecords($searchInput,$skip,$take, $is_paginate){ // /* $status = null,$sort_by,$is_ascending, $clause = ""*/
        $records = DB::table('benchmarks');
        $records = $records
            ->where(function($query) use ($searchInput){
                $query
                    ->where('job_code', 'like', '%'.$searchInput.'%')
                    ->orWhere('title', 'like', '%'.$searchInput.'%')
                    ->orWhere('grade', 'like', '%'.$searchInput.'%')
                    ->orWhere('p2', 'like', '%'.$searchInput.'%')
                    ->orWhere('mm', 'like', '%'.$searchInput.'%')
                    ->orWhere('indicator', 'like', '%'.$searchInput.'%');


            })
            ->select(['id','job_code', 'title', 'grade', 'mm', 'indicator' ]);


        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }

        return $records->get();

    }
}

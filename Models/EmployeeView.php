<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeView extends Model
{
    protected $table = 'vw_employees_combine';
}

<?php


namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

define('INPUT_VALIDATOR_REM_GENERIC_LOOKUP', 267);
define('INPUT_VALIDATOR_REM_INCENTIVE_SCHEME', 297);
define('INPUT_VALIDATOR_REM_DO_NOT_PROCESS_SALARY', 298);
define('INPUT_VALIDATOR_REM_CURRENCY_CODE', 299);
define('INPUT_VALIDATOR_REM_JOB_SCALE_CODE', 300);
define('INPUT_VALIDATOR_REM_ASSESSOR', 301);

class InputValidator extends \App\CFLibrary\InputValidator
{


    public function validateType(&$field, $type, $tableName, $sqlName, $field_name, $id)
    {
        if ($type < 256)
        {
            return parent::validateType($field, $type, $tableName, $sqlName, $field_name, $id);
        }
        else
        {
            switch ($type)
            {
                case INPUT_VALIDATOR_REM_GENERIC_LOOKUP:
                    list($field, $table) = $field;
                    $result = DB::table($table)->where('name', $field)->orWhere('id',$field)->get();
                    if (count($result) === 0)
                    {
                        $this->set_validator_message($sqlName, 'Invalid value for ' . $field_name);
                        return false;
                    }
                    return true;
                    break;

                case INPUT_VALIDATOR_REM_INCENTIVE_SCHEME:
                    $tmp = array($field, 'incentive_schemes');
                    return $this->validateType($tmp, INPUT_VALIDATOR_REM_GENERIC_LOOKUP, $tableName, $sqlName, $field_name, $id);
                    break;

                case INPUT_VALIDATOR_REM_CURRENCY_CODE:
                    if (empty($field) || $field === NULL || $field === '')  return true;
                    $table = "currencies";
                    $result = DB::table($table)->where(function($query) use($field){
                        $query->where('code', $field)->orWhere('id', $field);
                    })->get();

                    if (count($result) === 0)
                    {
                        $this->set_validator_message($sqlName, 'Invalid value for ' . $field_name);
                        return false;
                    }
                    return true;
                    break;

                case INPUT_VALIDATOR_REM_JOB_SCALE_CODE:
                    if (empty($field) || $field === NULL || $field === '')  return true;
                    $table = "benchmarks";
                    $result = DB::table($table)->where('job_code', $field)->get();

                    if (count($result) === 0)
                    {
                        $this->set_validator_message($sqlName, 'Invalid value for ' . $field_name);
                        return false;
                    }
                    return true;
                    break;

                case INPUT_VALIDATOR_REM_ASSESSOR:
                    if (empty($field) || $field === NULL || $field === '')  return true;
                    $import_personnel_numbers = Session::get('import_personnel_numbers');
                    if (in_array($field, $import_personnel_numbers))
                        return true;
                    else {
                        $assessor = Employee::where('personnelnumber', $field)->first();
                        if ($assessor !== null)
                            return true;
                        else
                            $this->set_validator_message($sqlName,  "The personnel number, $field, does not belong to any employee in our records or in the upload file. " );
                        return false;
                    }

                    break;

                default:
                    return true;
            }
        }
    }
}
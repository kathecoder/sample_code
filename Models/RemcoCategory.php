<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemcoCategory extends Model
{
    protected $table = 'remco_categories';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Division extends Model
{
    protected $table = 'divisions';


    public static function getAllDivisions($searchInput,$skip,$take, $is_paginate, $filterFilters=false)
    {
        $ids = Filters::getFilterFilters('division_id');

        $res    =   Division::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', $ids);}

        $res->orderBy('name');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }
        ;
        return $res->get();





        $emp_ids = Filters::getFilterFilters('division_id');
        Employee::getEmployeeIDsAsHRAndManager($emp_ids);

        $employees    =   DB::table("employees")
            ->join('org_structures', 'org_structures.id', 'employees.org_structure_id')
            ->where("employees.cycle_id", Cycle::getCurrentCycleId());
        if($filterFilters===true){
            $employees->whereIn('employees.id',$emp_ids);
        }
        $employees->select("org_structures.division_id");
        $array_ids = $employees->pluck('division_id')->toArray();


        $res = Division::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', $array_ids);}

        if(Filters::getFilteredOrgStructure()!==null){
            //$res->where('id', Filters::getFilteredOrgStructure()->division_id);
        }

        $res->orderByDesc('updated_at');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }


        return $res->get();
    }
}

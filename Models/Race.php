<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    protected $table = 'races';
    //

    public static function getAllRaces($searchInput,$skip,$take, $is_paginate, $filterFilters=false)
    {
        $ids = Filters::getFilterFilters('race_id');

        $res    =   Race::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', $ids);}

        $res->orderBy('name');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }
        return $res->get();
    }
}

<?php

namespace App\Models;

use App\CFLibrary\FieldFormatter;
use Illuminate\Database\Eloquent\Model;

class EmployeeIncentive extends Model
{
    protected $table = 'employee_incentives';


    public static function saveEmployeeIncentives(Employee $employee, $row){
        $incentive_schemes          = IncentiveScheme::all()->pluck('name', 'id')->toArray();
        $participation_categories   = ParticipationCategory::all()->pluck('name', 'id')->toArray();
        $currencies                 = Currency::all()->pluck('code', 'id')->toArray();
        $cycle_id = 1; //todo: update cycle id
        $employee_salary = EmployeeSalary::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
        $employee_incentive = EmployeeIncentive::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
        $incentive_scheme_id = null;

        if ($employee_incentive === null){
            $employee_incentive                 = new EmployeeIncentive();
            $employee_incentive->cycle_id       = $cycle_id;
            $employee_incentive->personnelnumber    = $employee->personnelnumber;
        }

        if (isset($row['h_1_incentive_scheme']))
            $employee_incentive->h_1_incentive_scheme = $row['h_1_incentive_scheme'];

        if (isset($row['incentive_scheme_id'])){
            $column_value = $row['incentive_scheme_id'];

            $incentive_scheme_id = IncentiveScheme::where('name', $column_value)->first();


            $check = in_array($column_value, $incentive_schemes);
            if ($check){
                $key = array_search($column_value, $incentive_schemes);
                $incentive_scheme_id = $key;
                $employee_incentive->incentive_scheme_id = $key;
            }
            else {
                $employee_incentive->incentive_scheme_id = null;
            }
        }

        if (isset($row['participation_category_id'])){
            $column_value = $row['participation_category_id'];

            $incentive_scheme = IncentiveScheme::where('name', $row['incentive_scheme_id'])->first();
            $pc = ParticipationCategory::where('name', $column_value)->where('incentive_scheme_id', $incentive_scheme->id)->first();

            //$check = in_array($column_value, $participation_categories);
            if ($pc){
                //$key = array_search($column_value, $participation_categories);
                $employee_incentive->participation_category_id = $pc->id;
            }
            else {
                $employee_incentive->participation_category_id = null;
            }
        }

        if (isset($row['allocation']))
            $employee_incentive->allocation = $row['allocation'];

        if (isset($row['assessment_score']))
            $employee_incentive->assessment_score = $row['assessment_score'];

        if (isset($row['h_1_incentive_zar']))
            $employee_incentive->h_1_incentive_zar = $row['h_1_incentive_zar'];

        if (isset($row['suggested_incentive']))
            $employee_incentive->suggested_incentive = $row['suggested_incentive'];

        if (isset($row['deferral_top_up']))
            $employee_incentive->deferral_top_up = $row['deferral_top_up'];

        if (isset($row['financial_incentive_value']))
            $employee_incentive->financial_incentive_value = $row['financial_incentive_value'];

        if (isset($row['h_1_allocation']))
            $employee_incentive->h_1_allocation = $row['h_1_allocation'];

        if (isset($row['h_1_assessment_score']))
            $employee_incentive->h_1_assessment_score = $row['h_1_assessment_score'];

        if (isset($row['h_1_calc_cash']))
            $employee_incentive->h_1_calc_cash = $row['h_1_calc_cash'];

        if (isset($row['h_1_cash']))
            $employee_incentive->h_1_cash = $row['h_1_cash'];

        if (isset($row['h_1_deferral']))
            $employee_incentive->h_1_deferral = $row['h_1_deferral'];

        if (isset($row['h_1_discount_achievement']))
            $employee_incentive->h_1_discount_achievement = $row['h_1_discount_achievement'];

        if (isset($row['h_1_financial']))
            $employee_incentive->h_1_financial = $row['h_1_financial'];

        if (isset($row['h_1_financial_incentive_value']))
            $employee_incentive->h_1_financial_incentive_value = $row['h_1_financial_incentive_value'];

        if (isset($row['h_1_incentive_currency']))
            $employee_incentive->h_1_incentive_currency = $row['h_1_incentive_currency'];

        if (isset($row['h_1_incentive_percentage']))
            $employee_incentive->h_1_incentive_percentage = $row['h_1_incentive_percentage'];

        if (isset($row['h_1_incentive_top_up']))
            $employee_incentive->h_1_incentive_top_up = $row['h_1_incentive_top_up'];

        if (isset($row['h_1_incentive_value']))
            $employee_incentive->h_1_incentive_value = $row['h_1_incentive_value'];

        if (isset($row['h_1_kpi_incentive_value']))
            $employee_incentive->h_1_kpi_incentive_value = $row['h_1_kpi_incentive_value'];

        if (isset($row['h_1_non_financial']))
            $employee_incentive->h_1_non_financial = $row['h_1_non_financial'];

        if (isset($row['h_1_on_target_achievement']))
            $employee_incentive->h_1_on_target_achievement = $row['h_1_on_target_achievement'];

        if (isset($row['h_1_ref_contribution']))
            $employee_incentive->h_1_ref_contribution = $row['h_1_ref_contribution'];

        if (isset($row['incentive_cap']))
            $employee_incentive->incentive_cap = $row['incentive_cap'];

        if (isset($row['incentive_top_up']))
            $employee_incentive->incentive_top_up = $row['incentive_top_up'];

        if (isset($row['longterm_incentive_value']))
            $employee_incentive->longterm_incentive_value = $row['longterm_incentive_value'];

        if (isset($row['longterm_incentive_value']))
            $employee_incentive->longterm_incentive_value = $row['longterm_incentive_value'];

        if (isset($row['pension_percentage']))
            $employee_incentive->pension_percentage = $row['pension_percentage'];

        if (isset($row['pension_value']))
            $employee_incentive->pension_value = $row['pension_value'];

        if (isset($row['proposed_deferred_shares']))
            $employee_incentive->proposed_deferred_shares = $row['proposed_deferred_shares'];

        if (isset($row['proposed_deferred_value']))
            $employee_incentive->proposed_deferred_value = $row['proposed_deferred_value'];

        if (isset($row['proposedincentive_currency_id'])){
            $column_value = $row['proposedincentive_currency_id'];
            $key = array_search(strtoupper($column_value), $currencies);
            if ($key>0){
                $employee_incentive->proposedincentive_currency_id = $key;
            }
            elseif(array_key_exists($column_value, $currencies)){
                $employee_incentive->proposedincentive_currency_id = $column_value;
            }
            else {
                $employee_incentive->proposedincentive_currency_id = null;
            }
        }

        if (isset($row['proposedincentive_value']) ){
            $proposedincentive_value = $row['proposedincentive_value'] == '' ? 0 : $row['proposedincentive_value'];

            $salary_value   = null;
            $salary_currency= null;
            if (isset($row['salary_currency_id'])) {
                $match = strtoupper($row['salary_currency_id']);
                $salary_currency = Currency::where('code', $match)->first();
            }
            elseif($employee_salary){
                $salary_currency = Currency::findOrFail($employee_salary->salary_currency_id);
            }

            if (isset($row['salary_value']) ){
                $salary_value = $row['salary_value'];
            }
            elseif($employee_salary){
                $salary_value = $employee_salary->salary_value;
            }

            $employee_incentive->proposedincentive_value      = round($proposedincentive_value, 2);
            $employee_incentive->proposedincentive_percentage = round($salary_value != 0 ? $proposedincentive_value * 100 / $salary_value : 0, 1);

            if($salary_currency){
                $employee_incentive->calc_proposedincentive_zar   = round($proposedincentive_value / $salary_currency->conversion_rate, 2) ;
                $my_calc_proposedincentive_zar              = round($proposedincentive_value / $salary_currency->conversion_rate, 2) ;

            }
            else{
                //$employee_incentive->calc_proposedincentive_zar = 0;
            }

        }
        else if (isset($row['proposedincentive_value']) && empty($row['proposedincentive_value'])){

            $employee_incentive->proposedincentive_value      = 0;
            $employee_incentive->proposedincentive_percentage = 0.0;
            $employee_incentive->calc_proposedincentive_zar = 0;
        }



        if ((isset($row['proposedincentive_percentage'])) &&  (isset($row['proposedincentive_value']) ))
        {
            //don't do any calculation - both values are calculated above through the proposedincentive_value field
        }
        else{

            if (isset($row['proposedincentive_percentage']) && !empty($row['proposedincentive_percentage'])){
                $proposedincentive_percentage               = $row['proposedincentive_percentage'];
                $employee_incentive->proposedincentive_percentage = $row['proposedincentive_percentage'];

                $salary_value   = null;
                $salary_currency = null;
                if (isset($row['salary_currency_id'])) {
                    $match = strtoupper($row['salary_currency_id']);
                    $salary_currency = Currency::where('code', $match)->first();
                }
                elseif($employee_salary){
                    $salary_currency = Currency::findOrFail($employee_salary->salary_currency_id);
                }

                if (isset($row['salary_value']) ){
                    $salary_value = $row['salary_value'];
                }
                elseif($employee_salary){
                    $salary_value = $employee_salary->salary_value;
                }

                $proposedincentive_value = $proposedincentive_percentage * $salary_value / 100;
                $employee_incentive->proposedincentive_value = round($proposedincentive_value, 0);
                //$employee_incentive->proposedincentive_percentage = round($salary_value != 0 ? $proposedincentive_value * 100 / $salary_value : 0, 2);

                if($salary_currency){
                    $employee_incentive->calc_proposedincentive_zar   = round($proposedincentive_value / $salary_currency->conversion_rate, 0);
                    $my_calc_proposedincentive_zar              = round($proposedincentive_value / $salary_currency->conversion_rate, 0);
                }

            }

            else if (isset($row['proposedincentive_percentage']) && empty($row['proposedincentive_percentage'])){
                $employee_incentive->proposedincentive_percentage = 0.0;
                $employee_incentive->proposedincentive_value = 0;
                $employee_incentive->calc_proposedincentive_zar   = 0;

            }
        }

        if (isset($row['provident_percentage']))
            $employee_incentive->provident_percentage = $row['provident_percentage'];

        if (isset($row['provident_value']))
            $employee_incentive->provident_value = $row['provident_value'];

        if (isset($row['ref_contribution'])){
            $salary_value   = null;
            $salary_currency = null;
            if (isset($row['salary_currency_id'])) {
                $match = strtoupper($row['salary_currency_id']);
                $salary_currency = Currency::where('code', $match)->first();
            }
            elseif($employee_salary){
                $salary_currency = Currency::findOrFail($employee_salary->salary_currency_id);
            }

            $ref_contribution                               = round($row['ref_contribution'], 0);
            $employee_incentive->ref_contribution           = $ref_contribution;
            $employee_incentive->ref_contribution_percent   = $employee_incentive->proposedincentive_value > 0 ? round($ref_contribution / $employee_incentive->proposedincentive_value * 100, 1) : 0;
            $employee_incentive->ref_contribution_zar       = $salary_currency->conversion_rate > 0 ? round($ref_contribution  / $salary_currency->conversion_rate, 0) : 0;

        }

        if (isset($row['ref_contribution_percent'])){
            $salary_value   = null;
            $salary_currency = null;
            if (isset($row['salary_currency_id'])) {
                $match = strtoupper($row['salary_currency_id']);
                $salary_currency = Currency::where('code', $match)->first();
            }
            elseif($employee_salary){
                $salary_currency = Currency::findOrFail($employee_salary->salary_currency_id);
            }

            $ref_contribution_percent                       = round($row['ref_contribution_percent'], 1);
            $employee_incentive->ref_contribution_percent   = $ref_contribution_percent;
            $employee_incentive->ref_contribution           = $employee_incentive->proposedincentive_value * $ref_contribution_percent / 100;
            $employee_incentive->ref_contribution_zar       = $ref_contribution  / $salary_currency->conversion_rate;

        }

        if (isset($row['retail_scheme_august']))
            $employee_incentive->retail_scheme_august = $row['retail_scheme_august'];

        if (isset($row['retail_scheme_january']))
            $employee_incentive->retail_scheme_january = $row['retail_scheme_january'];

        if (isset($row['split_value']))
            $employee_incentive->split_value = $row['split_value'];

        if (isset($row['stanlib_suggest_incentive_cash_value']))
            $employee_incentive->stanlib_suggest_incentive_cash_value = $row['stanlib_suggest_incentive_cash_value'];

        if (isset($row['stanlib_suggest_incentive_shares_value']))
            $employee_incentive->stanlib_suggest_incentive_shares_value = $row['stanlib_suggest_incentive_shares_value'];

        if (isset($row['stanlib_suggest_incentive_total_value']))
            $employee_incentive->stanlib_suggest_incentive_total_value = $row['stanlib_suggest_incentive_total_value'];

        if (isset($row['previous_allocation']))
            $employee_incentive->previous_allocation = $row['previous_allocation'];

        if (isset($row['previous_allocation']))
            $employee_incentive->previous_allocation = $row['previous_allocation'];


        if (isset($row['performance_reward_plan']))
            $employee_incentive->performance_reward_plan = $row['performance_reward_plan'];


        if (isset($row['do_not_process_bonus'])){
            $column_value = strtolower( $row['do_not_process_bonus']);
            $employee_incentive->do_not_process_bonus = FieldFormatter::format($column_value,'FIELD_FORMATTER_YES_NO') =='Yes' ? 1:0;
        }

        Audit::AuditTrailPerModel('employee_incentives', $employee_incentive, $employee->personnelnumber);
        $employee_incentive->save();

        return $employee_incentive;

    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompaRatioGuidelines extends Model
{
    protected $table = 'all_compa_ratio_guides';
}
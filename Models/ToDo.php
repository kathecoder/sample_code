<?php

namespace App\Models;

use App\CFLibrary\FilterHandler;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ToDo extends Model
{
    protected $table = 'todos';
    public $timestamps = false;

    public static function getRecords($searchInput,$skip,$take, $is_paginate, $status =null, $clause){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $record_ids      = FilterHandler::getFilteredData('todos', Auth::user()->id);
     

        $records = DB::table('todos')
            ->join('cycles', 'cycles.id', 'todos.cycle_id')
            ->join('users', 'users.id', 'todos.user_id')
             ->where('todos.user_id','=',Auth::user()->id)

            //->where('todos.cycle_id', '=', Cycle::getLiveCycleID())
            //->where('todos.user_id', '=', $manager_pn->id)
            ->where(function($query) use ($searchInput){
                $query
                    ->where('todos.subject', 'like', '%'.$searchInput.'%')
                    ->orWhere('todos.updated_at', 'like', '%'.$searchInput.'%');
            })
            ->select(
                DB::raw("todos.id as id, todos.subject as subject, todos.updated_at as updated_at, todos.created_at as created_at, todos.status as status"),
                DB::raw("users.name")
            );
             if ($clause=="all"){$clause = null;}
 
            if ($clause!=null){ 
                $records = $records->where('todos.status','=',$clause);
            }
 
            
 
          
            $records = $records->orderBy('todos.created_at','asc');
            $record_names = [];
  
           // $records->whereIn('status_id',$record_ids);  // Global filter
        //$user_cycles->orderByDesc('user_cycles.updated_at');

        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }
        return $records->get();






















        $records = DB::table('vw_employees_export');
        if (count($record_ids) > 0){
            $records = $records->whereIn('id',  $record_ids);
        }

        if ($active_instance == 1){
            $headers = Employee::$group_headers_sv;
            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ($active_instance == 2){
            $headers = Employee::$africa_headers_sv;
            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ($active_instance == 3){
            $headers = Employee::$stanlib_headers_sv;
            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn);

            });
        }

        $headers[]='id';
        $headers[]='calc_phaseclustermanager2name';

        if (!in_array('calc_businessmanager2name', $headers))
            $headers[]='calc_businessmanager2name';

        if (!in_array('calc_prorata', $headers))
            $headers[]='calc_prorata';

        $records = $records

            ->where(function($query) use ($searchInput){
                $query
                    ->where('personnelnumber', 'like', '%'.$searchInput.'%')
                    ->orWhere('jobtitle', 'like', '%'.$searchInput.'%')
                    ->orWhere('lastname', 'like', '%'.$searchInput.'%')
                    ->orWhere('knownas', 'like', '%'.$searchInput.'%')
                    ->orWhere('firstnames', 'like', '%'.$searchInput.'%');
            })
            ->select($headers);

        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }

        return $records->get();

    }


     public static function getFilteredIssueLogs($searchInput,$skip,$take, $is_paginate, $status, $clause = "")
    {
        //Log::notice($clause);
        $sql = "SELECT issue_logs.*,users.name,statuses.name as status_name, issue_sub_categories.name as issue_sub_categories
                FROM issue_logs,users,statuses,issue_sub_categories
                WHERE issue_logs.status_id=statuses.id 
                AND issue_logs.cycle_id=".Cycle::getCurrentCycleId()."
                AND issue_logs.user_id=users.id
                AND issue_logs.issue_sub_category_id=issue_sub_categories.id
                ".$clause;

        if($status>0){
            $sql=$sql." AND statuses.id=$status";
        }


        $filter1 = Filters::where('user_id', Auth::user()->id)->where('reference_table','issue_category')->get(['reference_table_id as id']);
        if(count($filter1)>0){
            //logger($filter1);
            $sql=$sql." AND issue_logs.issue_sub_category_id IN (".Filters::convertJsonIntoArray2($filter1).")";
        }

        $filter = Filters::where('user_id', Auth::user()->id)->where('reference_table','logged_by')->get(['reference_table_id as id']);
        if(count($filter)>0){
            $sql=$sql." AND issue_logs.user_id IN (".Filters::convertJsonIntoArray2($filter).")";
        }

        $filter = Filters::where('user_id', Auth::user()->id)->where('reference_table','date')->get();
        if(count($filter)>0){
            $sql=$sql." AND issue_logs.date_opened BETWEEN '".$filter[0]->created_at."' AND '".$filter[0]->updated_at."'";
        }

        $sql=$sql." AND users.name LIKE '%$searchInput%' AND issue_logs.subject LIKE '%$searchInput%' AND statuses.name LIKE '%$searchInput%' AND issue_logs.issue LIKE '%$searchInput%'";
        if($is_paginate===true){
            $sql=$sql."LIMIT $skip,$take";
        }


        //Log::info($sql);
        $res=DB::SELECT($sql);
        if(is_array($res)){
            return $res;
        }else{
            return [];
        }

    }
}

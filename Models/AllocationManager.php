<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AllocationManager extends Model
{
    protected $table = 'vw_employee_allocation_manager';
}
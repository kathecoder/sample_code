<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InactiveEmployee extends Model
{
    protected $table = 'vw_employees_inactive';
}

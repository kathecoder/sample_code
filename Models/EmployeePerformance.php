<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class EmployeePerformance extends Model
{
    protected $table = 'employee_performances';

    public static function saveEmployeePerformances(Employee $employee, $row){

        $performance_ratings = PerformanceRating::all()->pluck('name', 'id')->toArray();
        $cycle_id = 1; //todo: update cycle id
        $employee_performance = EmployeePerformance::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
        if ($employee_performance === null){
            $employee_performance = new EmployeePerformance();
            $employee_performance->cycle_id = $cycle_id;
            $employee_performance->personnelnumber = $employee->personnelnumber;
        }


        $employee_old = $employee_performance;

        if (isset($row['current_rating_id'])){
            $column_value = $row['current_rating_id'];
            $check = PerformanceRating::where('name', $column_value)->first();
            $old_value =  $employee_performance->current_rating_id;
            if ($check){
                $new_value = $check->id;
                $employee_performance->current_rating_id = $check->id;
            }



        }

        if (isset($row['h_1_performance1']))
            $employee_performance->h_1_performance1 = $row['h_1_performance1'];

        if (isset($row['h_2_performance1']))
            $employee_performance->h_2_performance1 = $row['h_2_performance1'];

        if (isset($row['critical_assessment']))
            $employee_performance->critical_assessment = $row['critical_assessment'];

        if (isset($row['critical_assessment']))
            $employee_performance->critical_assessment = $row['critical_assessment'];

        if (isset($row['talent_indicator']))
            $employee_performance->talent_indicator = $row['talent_indicator'];

        Audit::AuditTrailPerModel('employee_performances', $employee_performance, $employee->personnelnumber);
        $employee_performance->save();

    }



}

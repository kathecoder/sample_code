<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\CFLibrary\FilterHandler;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Models\CyclePhases;
use App\Models\Phase;
use Illuminate\Support\Facades\Session;

class PerformanceRating extends Model
{
    protected $table = 'performance_ratings';


    public static function getAllRecords($searchInput,$skip,$take, $is_paginate, $filterFilters=false)
    {
        $ids = Filters::getFilterFilters('current_rating_id');
        $res    =   PerformanceRating::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', $ids);}
        $res->orderBy('name');
        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }
        return $res->get();
    }


    public static function getAllDemographicsIncentivesRecords(){
        $manager_pn         = Auth::user();
        $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $phase_id           = Phase::getLivePhaseID();

        $dbIncentives       = DB::table('vw_employees_export');
        $dbIncentives       = Filters::applyFilters($dbIncentives, Auth::user());
        $dbIncentives       = $dbIncentives->where('instance_indicator_id','=',$active_instance);
        $manager_pn         = $manager_pn->personnelnumber;

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbIncentives = $dbIncentives->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }


            $dbIncentives->select( DB::raw('count(*) as total'),

            DB::raw('ROUND(AVG(CASE WHEN (gender = \'Female\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as female'),
            DB::raw('ROUND(AVG(CASE WHEN (gender = \'Male\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as male'),
            DB::raw('ROUND(AVG(CASE WHEN (race = \'African\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as african'),
            DB::raw('ROUND(AVG(CASE WHEN (race = \'Coloured\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as coloured'),
            DB::raw('ROUND(AVG(CASE WHEN (race = \'Indian\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as indian'),
            DB::raw('ROUND(AVG(CASE WHEN (race = \'White\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as white'),

            DB::raw('ROUND(AVG(CASE WHEN (race = \'African\' OR race = \'Coloured\' OR race = \'Indian\') AND (proposedincentive_percentage>0)
                THEN proposedincentive_percentage END),2) as minAIC'),
                DB::raw('ROUND(AVG(CASE WHEN (gender = \'Female\') AND (proposedincentive_value>0) THEN proposedincentive_value END),2) as female1'),
                DB::raw('ROUND(AVG(CASE WHEN (gender = \'Male\') AND (proposedincentive_value>0) THEN proposedincentive_value END),2) as male1'),
                DB::raw('ROUND(AVG(CASE WHEN (race = \'African\') AND (proposedincentive_value>0) THEN proposedincentive_value END),2) as african1'),
                DB::raw('ROUND(AVG(CASE WHEN (race = \'Coloured\') AND (proposedincentive_value>0) THEN proposedincentive_value END),2) as coloured1'),
                DB::raw('ROUND(AVG(CASE WHEN (race = \'Indian\') AND (proposedincentive_value>0) THEN proposedincentive_value END),2) as indian1'),
                DB::raw('ROUND(AVG(CASE WHEN (race = \'White\') AND (proposedincentive_value>0) THEN proposedincentive_value END),2) as white1'),
            DB::raw('ROUND(AVG(CASE WHEN (race = \'African\' OR race = \'Coloured\' OR race = \'Indian\') AND (proposedincentive_value>0) 
                THEN proposedincentive_value END),2) as minAIC1'),

                DB::raw('ROUND(MAX(CASE WHEN (gender = \'Female\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as maxfemale'), // Highest Incentive - Female
                DB::raw('ROUND(MAX(CASE WHEN (gender = \'Male\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as maxmale'), // Highest Incentive - Male
                DB::raw('ROUND(MIN(CASE WHEN (gender = \'Female\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as minfemale'), // Lowest Incentive - Female (excl zeros)
                DB::raw('ROUND(MIN(CASE WHEN (gender = \'Male\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as minmale'), // Lowest Incentive - Male (excl zeros)

                DB::raw('ROUND(MAX(CASE WHEN (race = \'White\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as maxwhite'),
                DB::raw('ROUND(MIN(CASE WHEN (race = \'White\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),2) as minwhite'),

                DB::raw('ROUND(MIN(CASE WHEN (race = \'African\' OR race = \'Coloured\' OR race = \'Indian\') AND (proposedincentive_percentage>0) 
                THEN proposedincentive_percentage END),2) as minAIC'),

            DB::raw('ROUND(AVG(CASE WHEN (race = \'African\' OR race = \'Coloured\' OR race = \'Indian\') AND (proposedincentive_percentage>0) 
            THEN proposedincentive_percentage END),2) as avgAIC'),
        /*  DB::raw('ROUND(MIN(CASE WHEN race = \'African\' OR race = \'Coloured\' OR race = \'Indian\'
            THEN proposedincentive_percentage END),2) as minAIC'),*/

            DB::raw('ROUND(MAX(CASE WHEN (race = \'African\' OR race = \'Coloured\' OR race = \'Indian\') AND (proposedincentive_percentage>0) 
            THEN proposedincentive_percentage END),2) as maxAIC')
        /*  DB::raw('ROUND(MAX(CASE WHEN race = \'African\' OR race = \'Coloured\' OR race = \'Indian\'
            THEN proposedincentive_percentage END),2) as maxAIC')*/
                );
                $dbIncentives = $dbIncentives->get();

            $arr_genderamt = [0,0];
            $arr_genderperc = [0,0,0,0,0];
            $arr_raceperc = [0,0,0,0,0];
            $arr_raceamt = [0,0,0,0,0];

            foreach ($dbIncentives as $value){
                $arr_genderamt = [(float)$value->male1 , (float)$value->female1];
                $arr_raceamt = [(float)$value->white1,(float)$value->african1, (float)$value->indian1 , (float)$value->coloured1,
                    (float)$value-> minAIC1];

                $arr_genderperc = [$value->male , $value->female];
                $arr_raceperc = [$value->white,$value->african, $value->indian, $value->coloured, $value->avgAIC];
            }


            return [
                'all_records'=> $dbIncentives,
                'gender'=> $arr_genderamt,
                'gender_percentage'=> $arr_genderperc,
                'race'=>$arr_raceamt,
                'race_percentage'=>$arr_raceperc
            ];


    }

    public static function getAllDemographicsIncreaseRecords(){
        $manager_pn         = Auth::user();
        $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $record_ids         = FilterHandler::getFilteredData('vw_employees_export', Auth::user()->id);
        $phase_id           = Phase::getLivePhaseID();
        $manager_pn         = $manager_pn->personnelnumber;

        $demographics_increase_db_search    = DB::table('vw_employees_export');
        $demographics_increase_db_search    = Filters::applyFilters($demographics_increase_db_search, Auth::user());
        $demographics_increase_db_search    = $demographics_increase_db_search->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $demographics_increase_db_search = $demographics_increase_db_search->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }
        $demographics_increase_db_search->select( DB::raw('count(*) as total'),
        DB::raw('ROUND(AVG(CASE WHEN (gender = \'Female\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),1) as female'),
        DB::raw('ROUND(AVG(CASE WHEN (gender = \'Male\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),1) as male'),
        DB::raw('ROUND(AVG(CASE WHEN (race = \'African\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),1) as african'),
        DB::raw('ROUND(AVG(CASE WHEN (race = \'Coloured\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),1) as coloured'),
        DB::raw('ROUND(AVG(CASE WHEN (race = \'Indian\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),1) as indian'),
        DB::raw('ROUND(AVG(CASE WHEN (race = \'White\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),1) as white'),
        DB::raw('ROUND(AVG(CASE WHEN (race = \'African\' OR race = \'Coloured\' OR race = \'Indian\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),1) as avg_salary_increase_perc_aic'),
        //
        DB::raw('ROUND(AVG(CASE WHEN (race = \'African\' OR race = \'Coloured\' OR race = \'Indian\') AND (calc_proposedsalary_zar - calc_salary_zar)>0 
            THEN (calc_proposedsalary_zar - calc_salary_zar) END),1) as minAIC'),
        DB::raw('ROUND(AVG(CASE WHEN (gender = \'Female\') AND (calc_proposedsalary_zar - calc_salary_zar)>0 THEN (calc_proposedsalary_zar - calc_salary_zar) END),2) as female1'),
        DB::raw('ROUND(AVG(CASE WHEN (gender = \'Male\') AND (calc_proposedsalary_zar - calc_salary_zar)>0 THEN (calc_proposedsalary_zar - calc_salary_zar) END),2) as male1'),
        // race
        DB::raw('ROUND(AVG(CASE WHEN (race = \'African\') AND (calc_proposedsalary_zar - calc_salary_zar)>0 THEN (calc_proposedsalary_zar - calc_salary_zar) END),2) as african1'),
        DB::raw('ROUND(AVG(CASE WHEN (race = \'Coloured\') AND (calc_proposedsalary_zar - calc_salary_zar)>0 THEN (calc_proposedsalary_zar - calc_salary_zar) END),2) as coloured1'),
        DB::raw('ROUND(AVG(CASE WHEN (race = \'Indian\') AND (calc_proposedsalary_zar - calc_salary_zar)>0 THEN (calc_proposedsalary_zar - calc_salary_zar) END),2) as indian1'),
        DB::raw('ROUND(AVG(CASE WHEN (race = \'White\') AND (calc_proposedsalary_zar - calc_salary_zar)>0 THEN (calc_proposedsalary_zar - calc_salary_zar) END),2) as white1'),
        DB::raw('ROUND(AVG(CASE WHEN (race = \'African\' OR race = \'Coloured\' OR race = \'Indian\') AND (calc_proposedsalary_zar - calc_salary_zar)>0 
                                                                THEN (calc_proposedsalary_zar - calc_salary_zar) END),2) as minAIC1'),
        //
        DB::raw('ROUND(MAX(CASE WHEN (gender = \'Female\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),2) as maxfemale'), // Highest Increase - Female
        DB::raw('ROUND(MAX(CASE WHEN (gender = \'Male\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),2) as maxmale'), // Highest Increase - Male
        DB::raw('ROUND(MIN(CASE WHEN (gender = \'Female\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),2) as minfemale'), // Lowest Increase - Female (excl ze)
        DB::raw('ROUND(MIN(CASE WHEN (gender = \'Male\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),2) as minmale'),// Lowest Increase - Male (excl zeros)

        DB::raw('ROUND(MAX(CASE WHEN (race = \'White\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),2) as maxwhite'), // Highest Increase - White
        DB::raw('ROUND(MIN(CASE WHEN (race = \'White\') AND (proposedsalary_percentage > 0) THEN proposedsalary_percentage END),2) as minwhite'), // Lowest Increase - White (excl zeros)

        //Lowest Increase - AIC (excl zeros)
        DB::raw('ROUND(MIN(CASE WHEN (race = \'African\' OR race = \'Coloured\' OR race = \'Indian\') AND (proposedsalary_percentage > 0)
         THEN proposedsalary_percentage END),2) as minAIC'),
        /*  DB::raw('ROUND(MIN(CASE WHEN race = \'African\' OR race = \'Coloured\' OR race = \'Indian\'
         THEN proposedincentive_percentage END),2) as minAIC'),*/

        //Highest Increase - AIC
        DB::raw('ROUND(MAX(CASE WHEN (race = \'African\' OR race = \'Coloured\' OR race = \'Indian\') AND (proposedsalary_percentage > 0)
            THEN proposedsalary_percentage END),2) as maxAIC')
        /*  DB::raw('ROUND(MAX(CASE WHEN race = \'African\' OR race = \'Coloured\' OR race = \'Indian\'
        THEN proposedincentive_percentage END),2) as maxAIC')*/
        );

        $demographics_db_results = $demographics_increase_db_search->get();

            $arr_genderamt  = [0,0];
            $arr_genderperc = [0,0];
            $arr_raceperc   = [0,0,0,0,0];
            $arr_raceamt    = [0,0,0,0,0];

            foreach ($demographics_db_results as $value){
                $arr_genderamt = [(float)$value->male1 , (float)$value->female1];
                $arr_raceamt = [(float)$value->white1,(float)$value->african1, (float)$value->indian1 , (float)$value->coloured1,
                    (float)$value-> minAIC1];

                $arr_genderperc = [$value->male , $value->female];
                $arr_raceperc = [$value->white,$value->african, $value->indian, $value->coloured, $value-> avg_salary_increase_perc_aic];
            }

            return [
                'all_records'=> $demographics_db_results,
                'gender'=> $arr_genderamt,
                'gender_percentage'=> $arr_genderperc,
                'race'=>$arr_raceamt,
                'race_percentage'=>$arr_raceperc
            ];
        }


    public static function getSalaryincreaseRecords()
    {
        $manager_pn         = Auth::user();
        $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn         = $manager_pn->personnelnumber;

        $dbIncrease = DB::table('vw_employees_export');
        $dbIncrease = Filters::applyFilters($dbIncrease, Auth::user());

        $dbIncrease = $dbIncrease->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbIncrease = $dbIncrease->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }

        $dbIncrease->select('band_id','jobgrade',
        //DB::raw('ROUND(AVG(CASE WHEN current_rating = \'TEA - Too early to assess\' THEN proposedsalary_percentage END),1) as tot1'),

       DB::raw('ROUND((((( (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'TEA - Too early to assess\') THEN calc_proposedsalary_zar END) )  -
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'TEA - Too early to assess\') THEN calc_salary_zar END) ) ) /
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'TEA - Too early to assess\') THEN calc_salary_zar END) ) )* 100)  ) , 2) as tot1'),

        DB::raw('ROUND((((( (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'1 - Not achieved\') THEN calc_proposedsalary_zar END) )  -
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'1 - Not achieved\') THEN calc_salary_zar END) ) ) /
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'1 - Not achieved\') THEN calc_salary_zar END) ) )* 100)  ) , 2) as tot2'),

        DB::raw('ROUND((((( (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'2 - Partially achieved\') THEN calc_proposedsalary_zar END) )  -
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'2 - Partially achieved\') THEN calc_salary_zar END) ) ) /
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'2 - Partially achieved\') THEN calc_salary_zar END) ) )* 100)  ) , 2) as tot3'),

        DB::raw('ROUND((((( (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'3 - Fully achieved\') THEN calc_proposedsalary_zar END) )  -
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'3 - Fully achieved\') THEN calc_salary_zar END) ) ) /
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'3 - Fully achieved\') THEN calc_salary_zar END) ) )* 100)  ) , 2) as tot4'),

        DB::raw('ROUND((((( (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'4 - Exceptionally achieved\') THEN calc_proposedsalary_zar END) )  -
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'4 - Exceptionally achieved\') THEN calc_salary_zar END) ) ) /
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'4 - Exceptionally achieved\') THEN calc_salary_zar END) ) )* 100)  ) , 2) as tot5'),


        DB::raw('ROUND((((( (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'TEA - Too early to assess\') THEN h_1_proposedsalary_zar END) )  -
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'TEA - Too early to assess\') THEN h_1_salary_zar END) ) ) /
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'TEA - Too early to assess\') THEN h_1_salary_zar END) ) )* 100)  ) , 2) as h_1_tot1'),

        DB::raw('ROUND((((( (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'1 - Not achieved\') THEN h_1_proposedsalary_zar END) )  -
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'1 - Not achieved\') THEN h_1_salary_zar END) ) ) /
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'1 - Not achieved\') THEN h_1_salary_zar END) ) )* 100)  ) , 2) as h_1_tot2'),

        DB::raw('ROUND((((( (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'2 - Partially achieved\') THEN h_1_proposedsalary_zar END) )  -
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'2 - Partially achieved\') THEN h_1_salary_zar END) ) ) /
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'2 - Partially achieved\') THEN h_1_salary_zar END) ) )* 100)  ) , 2) as h_1_tot3'),

        DB::raw('ROUND((((( (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'3 - Fully achieved\') THEN h_1_proposedsalary_zar END) )  -
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'3 - Fully achieved\') THEN h_1_salary_zar END) ) ) /
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'3 - Fully achieved\') THEN h_1_salary_zar END) ) )* 100)  ) , 2) as h_1_tot4'),

        DB::raw('ROUND((((( (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'4 - Exceptionally achieved\') THEN h_1_proposedsalary_zar END) )  -
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'4 - Exceptionally achieved\') THEN h_1_salary_zar END) ) ) /
         (sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (h_1_performance1 = \'4 - Exceptionally achieved\') THEN h_1_salary_zar END) ) )* 100)  ) , 2) as h_1_tot5')

        )
            ->whereNotNull('band_id')
            ->groupBy('band_id');
        $dbIncrease = $dbIncrease->get();

        $arr_b1 = [0,0,0,0,0];
        $arr_b2 = [0,0,0,0,0];
        $arr_b3 = [0,0,0,0,0];
        $arr_b4 = [0,0,0,0,0];
        $arr_b5 = [0,0,0,0,0];
        $arr_h_1_b1 = [0,0,0,0,0];
        $arr_h_1_b2 = [0,0,0,0,0];
        $arr_h_1_b3 = [0,0,0,0,0];
        $arr_h_1_b4 = [0,0,0,0,0];
        $arr_h_1_b5 = [0,0,0,0,0];
        //

        // logger($dbIncrease);


        foreach ($dbIncrease as $value){
            switch ($value->jobgrade){
                case "B1":
                    $arr_b1 = [(float)$value->tot2,(float)$value->tot3,(float)$value->tot4,(float)$value->tot5,(float)$value->tot1];

                    $arr_h_1_b1 = [ (float)$value->h_1_tot2, (float)$value->h_1_tot3, (float)$value->h_1_tot4, (float)$value->h_1_tot5, (float)$value->h_1_tot1];
                    break;
                case "B2":
                    $arr_b2 = [(float)$value->tot2,(float)$value->tot3,(float)$value->tot4,(float)$value->tot5,(float)$value->tot1];
                    $arr_h_1_b2 = [ (float)$value->h_1_tot2, (float)$value->h_1_tot3, (float)$value->h_1_tot4, (float)$value->h_1_tot5, (float)$value->h_1_tot1];
                    break;
                case "B3":
                    $arr_b3 = [(float)$value->tot2,(float)$value->tot3,(float)$value->tot4,(float)$value->tot5,(float)$value->tot1];
                    $arr_h_1_b3 = [ (float)$value->h_1_tot2, (float)$value->h_1_tot3, (float)$value->h_1_tot4, (float)$value->h_1_tot5, (float)$value->h_1_tot1];
                    break;
                case "B4":
                    $arr_b4 = [(float)$value->tot2,(float)$value->tot3,(float)$value->tot4,(float)$value->tot5,(float)$value->tot1];
                    $arr_h_1_b4 = [ (float)$value->h_1_tot2, (float)$value->h_1_tot3, (float)$value->h_1_tot4, (float)$value->h_1_tot5, (float)$value->h_1_tot1];
                    break;
                case "B5":
                    $arr_b5 = [(float)$value->tot2,(float)$value->tot3,(float)$value->tot4,(float)$value->tot5,(float)$value->tot1];
                    $arr_h_1_b5 = [ (float)$value->h_1_tot2, (float)$value->h_1_tot3, (float)$value->h_1_tot4, (float)$value->h_1_tot5, (float)$value->h_1_tot1];
                    break;
                default:
                    break;
            }
        }

        return [
            'arr_b1'=> $arr_b1,
                'arr_h_1_b1'=>$arr_h_1_b1,
                'arr_b2'=>$arr_b2,
                'arr_h_1_b2'=>$arr_h_1_b2,
                'arr_b3'=>$arr_b3,
                'arr_h_1_b3'=>$arr_h_1_b3,
                'arr_b4'=>$arr_b4,
                'arr_h_1_b4'=>$arr_h_1_b4,
                'arr_b5'=>$arr_b5,
                'arr_h_1_b5'=>$arr_h_1_b5
        ];
    }


    public static function getArr_package(){
        try {
            $manager_pn         = Auth::user();
            $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $active_instance    = PermissionController::getUserInstance()['response']['instance'];
            $record_ids         = FilterHandler::getFilteredData('vw_employees_export', Auth::user()->id);
            $phase_id           = Phase::getLivePhaseID();
            $manager_pn         = $manager_pn->personnelnumber;

            $arr_package = DB::table('vw_employees_export');
            $arr_package = Filters::applyFilters($arr_package, Auth::user());
            $arr_package = $arr_package->where('instance_indicator_id','=',$active_instance);

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $arr_package = $arr_package->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                        ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);
                });
            }
            $arr_package->select('band_id','jobgrade',
                DB::raw(' SUM( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) THEN calc_salary_zar END) as currentTotPackage')
                // DB::raw('sum( calc_salary_zar ) as currentTotPackage')
                 , DB::raw(' SUM( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) THEN calc_proposedsalary_zar END) as proposedTotPackage')
                //  , DB::raw('sum(  calc_proposedsalary_zar ) as proposedTotPackage')
                )
                ->whereNotNull('band_id')
                ->groupBy('band_id'/*,'calc_salary_zar'*/);

                // DB::enableQueryLog();

                 $arr_package = $arr_package->get();
                //  $queries = DB::getQueryLog();

                // logger( $queries);


                  return $arr_package;

        }catch (\Exception $exception){
            logger($exception);
            return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
        }



    }

    public static function getPerformanceb1b2()
    {
            $manager_pn         = Auth::user();
            $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $active_instance    = PermissionController::getUserInstance()['response']['instance'];
            $record_ids         = FilterHandler::getFilteredData('vw_employees_export', Auth::user()->id);
            $phase_id           = Phase::getLivePhaseID();
            $manager_pn         = $manager_pn->personnelnumber;

            $dbperformances = DB::table('vw_employees_export');
            $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
            $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

        try{

             if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $dbperformances = $dbperformances->where(function ($q) use($manager_pn,$active_instance) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                        ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn)
                        ;
                });
            }
            $dbperformances->select('band_id', DB::raw('count(*) as total'),
                DB::raw('ROUND(((COUNT(CASE WHEN (current_rating_id =1) THEN 1 END)/COUNT(*))*100),1) as tot1'),
                DB::raw('ROUND(((COUNT(CASE WHEN (current_rating_id =2) THEN 1 END)/COUNT(*))*100),1) as tot2'),
                DB::raw('ROUND(((COUNT(CASE WHEN (current_rating_id =3) THEN 1 END)/COUNT(*))*100),1) as tot3'),
                DB::raw('ROUND(((COUNT(CASE WHEN (current_rating_id =4) THEN 1 END)/COUNT(*))*100),1) as tot4'),
                DB::raw('ROUND(((COUNT(CASE WHEN (current_rating_id =5) THEN 1 END)/COUNT(*))*100),1) as tot5')
                // DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'Unknown\' THEN 1 END)/COUNT(*))*100),2) as tot6')

                )


                ->whereNotNull('band_id')
                ->whereIn('band_id',[1,2])
                ->groupBy('band_id');
             $dbperformances = $dbperformances->get();

            return $dbperformances;


        }catch (\Exception $exception){
            logger($exception);
            return ['error'=> 'Something went wrong. Please contact administrator.'];
        }
    }


    public static function getTotalPackage(){
        try {// proposedincentive_value
            $manager_pn      = Auth::user();
            $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $active_instance = PermissionController::getUserInstance()['response']['instance'];
            $manager_pn      = $manager_pn->personnelnumber;

            $TotPackage = DB::table('vw_employees_export');
            $TotPackage = Filters::applyFilters($TotPackage, Auth::user());
            $TotPackage = $TotPackage->where('instance_indicator_id','=', $active_instance );
            // $TotPackage = $TotPackage->where('instance_indicator_id','=',$active_instance);

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $TotPackage = $TotPackage->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                        ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                });
            }

            if ($active_instance == 1 || $active_instance == 3){//for group and stanlib
                ////////////////////////////////////////////**********************************General Staff = 13 ************************
                $TotPackage->select(

                    DB::raw('ROUND((sum( CASE WHEN (band_id=1 OR band_id=2) AND (scheme_id=13) THEN (calc_proposedincentive_zar) END) ) , 2) as proposedTotPackageb1b2'), //  Total Bonus Value
                    DB::raw('ROUND((sum( CASE WHEN (band_id=1 OR band_id=2) AND (scheme_id=13) THEN ( calc_salary_zar) END) ) , 2) as currentTotPackageb1b2'), //  Current Total Package

                    DB::raw('ROUND((sum( CASE WHEN (band_id=3) AND (scheme_id=13) THEN ( calc_proposedincentive_zar) END) ) , 2) as proposedTotPackageb3'), //  Total Bonus Value
                    DB::raw('ROUND((sum( CASE WHEN (band_id=3) AND (scheme_id=13) THEN ( calc_salary_zar) END) ) , 2) as currentTotPackageb3'), //  Current Total Package

                    DB::raw('ROUND((sum( CASE WHEN (band_id=4 OR band_id=5) AND (scheme_id=13) THEN ( calc_proposedincentive_zar) END) ) , 2) as proposedTotPackageb4b5'), //  Total Bonus Value
                    DB::raw('ROUND((sum( CASE WHEN (band_id=4 OR band_id=5) AND (scheme_id=13) THEN ( calc_salary_zar) END) ) , 2) as currentTotPackageb4b5') //  Current Total Package
                );
                // logger($active_instance);

            }
            elseif($active_instance == 2){
                $TotPackage->select(

                    DB::raw('ROUND( (sum( CASE WHEN  (band_id = 1 OR band_id = 2) AND (scheme_id = 12) THEN ( calc_proposedincentive_zar) END) ) , 2) as proposedTotPackageb1b2'), //  Total Bonus Value
                    DB::raw('ROUND(  (sum( CASE WHEN  (band_id = 1 OR band_id = 2) AND (scheme_id = 12) THEN ( calc_salary_zar) END) ) , 2) as currentTotPackageb1b2'), //  Current Total Package

                    DB::raw('ROUND( (sum( CASE WHEN  (band_id = 3) AND (scheme_id = 12) THEN ( calc_proposedincentive_zar) END) ) , 2) as proposedTotPackageb3'), //  Total Bonus Value
                    DB::raw('ROUND( (sum( CASE WHEN  (band_id = 3) AND (scheme_id = 12) THEN ( calc_salary_zar) END) ) , 2) as currentTotPackageb3'), //  Current Total Package

                    DB::raw('ROUND( (sum( CASE WHEN  (band_id = 4 OR band_id = 5) AND (scheme_id = 12) THEN ( calc_proposedincentive_zar) END) ) , 2) as proposedTotPackageb4b5'), //  Total Bonus Value
                    DB::raw('ROUND( (sum( CASE WHEN  (band_id = 4 OR band_id = 5) AND (scheme_id = 12) THEN ( calc_salary_zar) END) ) , 2) as currentTotPackageb4b5') //  Current Total Package
                );
            }

            $TotPackage = $TotPackage->get();

        } catch (\Exception $exception){
            logger($exception);
            return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
        }

        return $TotPackage;
    }

    public static function getBonus(){
        try {// proposedincentive_value
            $manager_pn         = Auth::user();
            $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $active_instance    = PermissionController::getUserInstance()['response']['instance'];
            $record_ids         = FilterHandler::getFilteredData('vw_employees_export', Auth::user()->id);
            $phase_id           = Phase::getLivePhaseID();
            $manager_pn         = $manager_pn->personnelnumber;

            $arr_bonus = DB::table('vw_employees_export');
            $arr_bonus = Filters::applyFilters($arr_bonus, Auth::user());
            $arr_bonus = $arr_bonus->where('instance_indicator_id','=',$active_instance);
            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $arr_bonus = $arr_bonus->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                        ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                });
            }

            $arr_bonus->select('band_id',
                     DB::raw('sum(calc_salary_zar) as currentTotPackage'),
                     DB::raw('sum(calc_proposedsalary_zar) as proposedTotPackage')
                     )
                ->whereNotNull('band_id')
                ->groupBy('band_id');
        } catch (\Exception $exception){
            logger($exception);
            return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
        }

        $arr_bonus = $arr_bonus->get();
        return $arr_bonus;
    }
    public static function getScores(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];

        $manager_pn         = $manager_pn->personnelnumber;

        $arr_score = DB::table('vw_employees_export');
        $arr_score = Filters::applyFilters($arr_score, Auth::user());
        $arr_score = $arr_score->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $arr_score = $arr_score->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }

        $arr_score->select('band_id',

        DB::raw('ROUND(((SUM(CASE WHEN current_rating = \'1 - Not achieved\' THEN calc_proposedsalary_zar ELSE 0 END)/
        SUM(CASE WHEN current_rating = \'1 - Not achieved\' THEN calc_proposedsalary_zar ELSE 0 END))*100),2) as tot1'),
        DB::raw('ROUND(((SUM(CASE WHEN current_rating = \'2 - Partially achieved\' THEN calc_proposedsalary_zar ELSE 0 END)/COUNT(*))*100),2) as tot2'),
        DB::raw('ROUND(((SUM(CASE WHEN current_rating = \'3 - Fully achieved\' THEN calc_proposedsalary_zar ELSE 0 END)/COUNT(*))*100),2) as tot3'),
        DB::raw('ROUND(((SUM(CASE WHEN current_rating = \'4 - Exceptionally achieved\' THEN calc_proposedsalary_zar ELSE 0 END)/COUNT(*))*100),2) as tot4'),
        DB::raw('ROUND(((SUM(CASE WHEN current_rating = \'TEA - Too early to assess\' THEN calc_proposedsalary_zar ELSE 0 END)/COUNT(*))*100),2) as tot5')
        )
            ->whereNotNull('band_id')
        ->groupBy('band_id');

        $arr_score = $arr_score->get();

        return $arr_score;

    }


    public static function getPerformanceb4b5()
    {
            $manager_pn      = Auth::user();
            $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $active_instance = PermissionController::getUserInstance()['response']['instance'];

            $manager_pn         = $manager_pn->personnelnumber;

            $dbperformances = DB::table('vw_employees_export');
            $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
            $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

        try{
            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                        ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                });
            }

            $dbperformances->select('band_id', DB::raw('count(*) as total'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'1 - Not achieved\' THEN 1 END)/COUNT(*))*100),1) as tot1'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'2 - Partially achieved\' THEN 1 END)/COUNT(*))*100),1) as tot2'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'3 - Fully achieved\' THEN 1 END)/COUNT(*))*100),1) as tot3'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'4 - Exceptionally achieved\' THEN 1 END)/COUNT(*))*100),1) as tot4'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'TEA - Too early to assess\' THEN 1 END)/COUNT(*))*100),1) as tot5'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'Unknown\' THEN 1 END)/COUNT(*))*100),1) as tot6'))
                ->whereIn('band_id',[4,5])
                ->whereNotNull('band_id')
                ->groupBy('band_id');

            $dbperformances = $dbperformances->get();
            return $dbperformances;
        }catch (\Exception $exception){
            logger($exception);
            return ['error'=> 'Something went wrong. Please contact administrator.'];
        }
    }



    public static function getPerformanceGeneral()
    {
            $manager_pn      = Auth::user();
            $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $active_instance = PermissionController::getUserInstance()['response']['instance'];
            $manager_pn         = $manager_pn->personnelnumber;

            $dbperformances = DB::table('vw_employees_export');
            $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
            $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

        try{
            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                        ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                });
            }

            if ($active_instance == 1 || $active_instance == 3){
                $dbperformances->select('orglevel1', DB::raw('count(*) as total'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'1 - Not achieved\' THEN 1 END)/COUNT(*))*100),1) as tot1'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'2 - Partially achieved\' THEN 1 END)/COUNT(*))*100),1) as tot2'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'3 - Fully achieved\' THEN 1 END)/COUNT(*))*100),1) as tot3'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'4 - Exceptionally achieved\' THEN 1 END)/COUNT(*))*100),1) as tot4'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'TEA - Too early to assess\' THEN 1 END)/COUNT(*))*100),1) as tot5'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'Unknown\' THEN 1 END)/COUNT(*))*100),1) as tot6'))
                    ->where('scheme_id', 13)
                    ->whereNotNull('orglevel1')
                    ->groupBy('orglevel1');
            } elseif($active_instance == 2){
                $dbperformances->select('orglevel1', DB::raw('count(*) as total'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'1 - Not achieved\' THEN 1 END)/COUNT(*))*100),2) as tot1'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'2 - Partially achieved\' THEN 1 END)/COUNT(*))*100),2) as tot2'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'3 - Fully achieved\' THEN 1 END)/COUNT(*))*100),2) as tot3'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'4 - Exceptionally achieved\' THEN 1 END)/COUNT(*))*100),2) as tot4'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'TEA - Too early to assess\' THEN 1 END)/COUNT(*))*100),2) as tot5'),
                    DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'Unknown\' THEN 1 END)/COUNT(*))*100),2) as tot6'))
                    ->where('scheme_id', 12)
                    ->whereNotNull('orglevel1')
                    ->groupBy('orglevel1');
            }


            $dbperformances = $dbperformances->get();
            return $dbperformances;
        }catch (\Exception $exception){
            logger($exception);
            return ['error'=> 'Something went wrong. Please contact administrator.'];
        }
    }

    public static function getPerformanceSMI()
    {
            $manager_pn      = Auth::user();
            $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $active_instance = PermissionController::getUserInstance()['response']['instance'];

            $manager_pn         = $manager_pn->personnelnumber;

            $dbperformances = DB::table('vw_employees_export');
            $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
            $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

        try{
            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                        ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                });
            }

            $dbperformances->select('orglevel1', DB::raw('count(*) as total'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'1 - Not achieved\' THEN 1 END)/COUNT(*))*100),2) as tot1'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'2 - Partially achieved\' THEN 1 END)/COUNT(*))*100),2) as tot2'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'3 - Fully achieved\' THEN 1 END)/COUNT(*))*100),2) as tot3'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'4 - Exceptionally achieved\' THEN 1 END)/COUNT(*))*100),2) as tot4'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'TEA - Too early to assess\' THEN 1 END)/COUNT(*))*100),2) as tot5'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'Unknown\' THEN 1 END)/COUNT(*))*100),2) as tot6'))
                ->whereIn('scheme_id',[20,23])
                ->whereNotNull('orglevel1')
                ->groupBy('orglevel1');

            $dbperformances = $dbperformances->get();

            return $dbperformances;
        }catch (\Exception $exception){
            logger($exception);
            return ['error'=> 'Something went wrong. Please contact administrator.'];
        }
    }



        //avg clusters
    public static function getAllAvgClusters()
    {
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];

        $manager_pn         = $manager_pn->personnelnumber;

        $avgclusters = DB::table('vw_employees_export');
        $avgclusters = Filters::applyFilters($avgclusters, Auth::user());
        $avgclusters = $avgclusters->where('instance_indicator_id','=',$active_instance);

        try{
            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $avgclusters = $avgclusters->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                        ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                });
            }
                $avgclusters->select('orglevel5', DB::raw('count(*) as total'),
                    DB::raw('ROUND(COALESCE(AVG(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) AND (calc_proposedsalary_zar - calc_salary_zar) !=0 THEN (calc_proposedsalary_zar - calc_salary_zar) END),0),2) as clusterAvg'))
                    ->whereNotNull('orglevel5')
                    ->groupBy('orglevel5');

                $avgclusters =  $avgclusters->get();
                return $avgclusters;
            }catch (\Exception $exception){
                logger($exception);
                return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
            }
    }


            /**********************************Average Salary Per Bands*********************************/
        //avg salary value
    public static function getSalValueAvg()
    {
        try{
            $manager_pn      = Auth::user();
            $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $active_instance = PermissionController::getUserInstance()['response']['instance'];

            $manager_pn         = $manager_pn->personnelnumber;

            $salvalue = DB::table('vw_employees_export');
            $salvalue = Filters::applyFilters($salvalue, Auth::user());
            $salvalue = $salvalue->where('instance_indicator_id','=',$active_instance);

            $salvalue->select('band_id', DB::raw('count(*) as total'), //proposedsalary_percentage
                //DB::raw('ROUND((AVG(calc_salary_zar)/ SUM(calc_salary_zar))*100,2)as bandsSalAvg'))
                //proposed increase percentage per band
                //DB::raw('ROUND(AVG(CASE WHEN (jobgrade = 1) AND (proposedsalary_percentage>0) THEN proposedsalary_percentage END),2) as b1tot'),
                DB::raw('ROUND(AVG(
                CASE WHEN (proposedsalary_percentage>0) THEN proposedsalary_percentage END
                ),2) as bandsSalAvg'))
                ->whereNotNull('band_id')
                //*//
                //->where(function ($q) {$q->orwhere('do_not_process_salary', NULL)->orWhere('do_not_process_salary', 0);})
                ->groupBy('band_id');
            $salvalue =  $salvalue->get();
            return $salvalue;
        }catch (\Exception $exception){
            logger($exception);
            return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
        }
    }
        //avg proposed salary value
    public static function getProposedSalValueAvg()
    {

        try{
                    $manager_pn      = Auth::user();
                    $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
                    $active_instance = PermissionController::getUserInstance()['response']['instance'];


                    $proposedsalvalue = DB::table('vw_employees_export');
                    $proposedsalvalue = Filters::applyFilters($proposedsalvalue, Auth::user());
                    $manager_pn         = $manager_pn->personnelnumber;

                    if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                        $proposedsalvalue = $proposedsalvalue->where(function ($q) use($manager_pn) {
                            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                        });
                    }

                    $proposedsalvalue->select(
                        'band_id',
                            DB::raw('count(*) as total'),
                            DB::raw(
                                'ROUND( AVG( CASE WHEN (calc_proposedsalary_zar - calc_salary_zar) > 0 THEN calc_proposedsalary_zar - calc_salary_zar END
                        ),2) as bandsSalAvg'))
                        ->whereNotNull('band_id')

                        //   ->where(function ($q) {
                        //       $q->orwhere('do_not_process_salary', NULL)
                        //       ->orWhere('do_not_process_salary', 0);
                        //     })
                        ->groupBy('band_id');
                    $proposedsalvalue =  $proposedsalvalue->get();
                    return $proposedsalvalue;
                }catch (\Exception $exception){
                    logger($exception);
                    return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
                }
    }

        //avg Bands percentages
    public static function getAllAvgBands()
    {
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $avgBands = DB::table('vw_employees_export');
        $avgBands = Filters::applyFilters($avgBands, Auth::user());
        $avgBands = $avgBands->where('instance_indicator_id','=',$active_instance);

        try{
        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
        $avgBands = $avgBands->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
        }

        $avgBands->select('current_rating_id', DB::raw('count(*) as total'),
            DB::raw('ROUND(AVG(proposedsalary_percentage),2) as tot1'))

            /*
            DB::raw('ROUND(COALESCE( AVG(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 )  THEN (calc_proposedsalary_zar) END),0)/
            SUM( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 )  THEN (calc_proposedsalary_zar) END)*100,2) as tot1'))
            */
            ->whereNotNull('current_rating_id')
            ->groupBy('current_rating_id');


        $avgBands =  $avgBands->get();

        return $avgBands;
        }catch (\Exception $exception){
        logger($exception);
        return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
        }
    }

        //Populate the table
    public static function getPopulateTable()
    {
                $manager_pn      = Auth::user();
                $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
                $active_instance = PermissionController::getUserInstance()['response']['instance'];

                $manager_pn         = $manager_pn->personnelnumber;

                $packageSummary = DB::table('vw_employees_export');
                $packageSummary = Filters::applyFilters($packageSummary, Auth::user());
                $packageSummary = $packageSummary->where('instance_indicator_id','=',$active_instance);
            try{
                if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                    $packageSummary = $packageSummary->where(function ($q) use($manager_pn,$active_instance) {
                        $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                            ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                            ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                            ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                            ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                            ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                            ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                            ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                            ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                            ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                            ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                            ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn)
                            ->orWhere('instance_indicator_id', $active_instance);


                    });
                }
                $packageSummary->select(

                //DB::raw('ROUND(COALESCE(AVG(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 )  THEN (calc_proposedsalary_zar - calc_salary_zar) END),0),2) as clusterAvg')

                    DB::raw('SUM(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) THEN (calc_salary_zar) END) as totPackage'),
                    DB::raw('ROUND(SUM(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) THEN calc_proposedsalary_zar - calc_salary_zar END),0) as increaseAllocation'),
                    DB::raw('SUM(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) THEN (calc_proposedsalary_zar) END) as packAfterIncrease'),

                    DB::raw('count(*) as headcount'),
                    DB::raw('MAX(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) AND (calc_proposedsalary_zar - calc_salary_zar) > 0   THEN (calc_proposedsalary_zar - calc_salary_zar) END) as highIncrease'),
                    DB::raw('MIN(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) AND (calc_proposedsalary_zar - calc_salary_zar) > 0  THEN (calc_proposedsalary_zar - calc_salary_zar) END) as lowIncrease'),
                    DB::raw('AVG(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) AND (calc_proposedsalary_zar - calc_salary_zar) > 0  THEN (calc_proposedsalary_zar - calc_salary_zar) END) as avgIncrease'),
                    DB::raw('ROUND(MAX(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 )  AND proposedsalary_percentage > 0  THEN proposedsalary_percentage END),1) as highIncreasePerc'),
                    DB::raw('ROUND(MIN(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 ) AND proposedsalary_percentage > 0  THEN proposedsalary_percentage END),1) as lowIncreasePerc'),
                    DB::raw('ROUND(AVG(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1 )  AND proposedsalary_percentage > 0  THEN proposedsalary_percentage END),1) as avgIncreasePerc')
                );
            $packageSummary =  $packageSummary->get();
            return $packageSummary;
        }catch (\Exception $exception){
            logger($exception);
            return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
        }
    }

        //populate the Salary uplift
    public static function getPopulateSalUplift()
    {
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $UpliftSummary = DB::table('vw_employees_export');
        $UpliftSummary = Filters::applyFilters($UpliftSummary, Auth::user());
        $UpliftSummary = $UpliftSummary->where('instance_indicator_id','=',$active_instance);

    try{
        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $UpliftSummary = $UpliftSummary->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }

        
            
            $UpliftSummary->select(

                //Salary Uplift B4B5
                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'1 - Not achieved\') AND  (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN  (current_rating = \'1 - Not achieved\') AND   (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_not_achievedb4b5'),

                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'2 - Partially achieved\') AND  (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN  (current_rating = \'2 - Partially achieved\') AND   (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_partially_achievedb4b5
                '),

                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'3 - Fully achieved\') AND  (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN  (current_rating = \'3 - Fully achieved\') AND   (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_fully_achievedb4b5
                '),

                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'4 - Exceptionally achieved\') AND  (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN  (current_rating = \'4 - Exceptionally achieved\') AND   (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_exceptionally_achievedb4b5
                '),

                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'TEA - Too early to assess\') AND  (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN  (current_rating = \'TEA - Too early to assess\') AND   (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_too_early_assessb4b5
                '),



                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'1 - Not achieved\')  AND (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN  (current_rating = \'1 - Not achieved\') AND (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_not_achievedb3
                '),



                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'2 - Partially achieved\') AND (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN (current_rating = \'2 - Partially achieved\')  AND (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_partially_achievedb3
                '),



                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'3 - Fully achieved\') AND (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN (current_rating = \'3 - Fully achieved\')  AND (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_fully_achievedb3
                '),



                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'4 - Exceptionally achieved\') AND (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN (current_rating = \'4 - Exceptionally achieved\') AND (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_exceptionally_achievedb3
                '),



                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'TEA - Too early to assess\') AND (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN (current_rating = \'TEA - Too early to assess\') AND (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_too_early_assessb3
                '),



                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'1 - Not achieved\') AND (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN (current_rating = \'1 - Not achieved\') AND (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_not_achievedb1b2
                '),



                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'2 - Partially achieved\') AND (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN (current_rating = \'2 - Partially achieved\') AND (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_partially_achievedb1b2
                '),


                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'3 - Fully achieved\') AND (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN (current_rating = \'3 - Fully achieved\') AND (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_fully_achievedb1b2
                '),



                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'4 - Exceptionally achieved\') AND (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN (current_rating = \'4 - Exceptionally achieved\') AND (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_exceptionally_achievedb1b2
                '),


                DB::raw('ROUND(((   (sum( CASE WHEN (current_rating = \'TEA - Too early to assess\') AND (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                                    (sum( CASE WHEN (current_rating = \'TEA - Too early to assess\') AND (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as uplift_too_early_assessb1b2
                ')

            );

            $UpliftSummary =  $UpliftSummary->get(); //dd($UpliftSummary);
            return $UpliftSummary;
        }catch (\Exception $exception){
            logger($exception);
            return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
        }
    }
    public static function getPackageByBands()
    {
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;

        $UpliftSummary = DB::table('vw_employees_export');
        $UpliftSummary = Filters::applyFilters($UpliftSummary, Auth::user());
        $UpliftSummary = $UpliftSummary->where('instance_indicator_id','=',$active_instance);

    try{
        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $UpliftSummary = $UpliftSummary->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }


            $UpliftSummary->select(
                DB::raw(
                '
                instance_indicator_id,

                ROUND(((SUM(CASE WHEN (band_id IN (1, 2)) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN calc_salary_zar END) )),2) as current_tot_package_b1b2,
                ROUND(((SUM(CASE WHEN (band_id IN (3)) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN calc_salary_zar END) )),2) as current_tot_package_b3,
                ROUND(((SUM(CASE WHEN (band_id IN (4, 5)) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN calc_salary_zar END) )),2) as current_tot_package_b4b5,
                
                ROUND(((SUM(CASE WHEN (band_id IN (1, 2)) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN calc_proposedsalary_zar END) )),2) as proposed_tot_package_b1b2,
                ROUND(((SUM(CASE WHEN (band_id IN (3)) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN calc_proposedsalary_zar END) )),2) as proposed_tot_package_b3,
                ROUND(((SUM(CASE WHEN (band_id IN (4, 5)) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN calc_proposedsalary_zar END) )),2) as proposed_tot_package_b4b5,
            
                ROUND((SUM(CASE WHEN band_id IN (1, 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_proposedsalary_zar END) - SUM(CASE WHEN band_id IN (1, 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) / 
                (SUM(CASE WHEN band_id IN (1, 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN calc_salary_zar END))* 100, 1) as uplift_b1b2,
                
                
                ROUND((SUM(CASE WHEN band_id IN (3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN calc_proposedsalary_zar END) - SUM(CASE WHEN band_id IN (3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN calc_salary_zar END) ) / 
                (SUM(CASE WHEN band_id IN (3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN calc_salary_zar END))* 100, 1) as uplift_b3,
                
                
                ROUND((SUM(CASE WHEN band_id IN (4,5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN calc_proposedsalary_zar END) - SUM(CASE WHEN band_id IN (4,5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) / 
                (SUM(CASE WHEN band_id IN (4,5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN calc_salary_zar END))* 100, 1) as uplift_b4b5
                '
                )

            );

            $UpliftSummary =  $UpliftSummary->groupBy('instance_indicator_id')->get();
            return $UpliftSummary;
        }catch (\Exception $exception){
            logger($exception);
            return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
        }
    }


    public static function getAlldbPerformanceReview()
    {
        try{
            $manager_pn      = Auth::user();
            $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $active_instance = PermissionController::getUserInstance()['response']['instance'];

            $manager_pn         = $manager_pn->personnelnumber;

            $dbperformances = DB::table('vw_employees_export');
            $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
            $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                });
            }
        $dbperformances->select('orglevel1', DB::raw('count(*) as total'),
        DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'1 - Not Achieved\' THEN 1 END)/COUNT(*))*100),2) as tot1'),
        DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'2 - Partially achieved\' THEN 1 END)/COUNT(*))*100),2) as tot2'),
        DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'3 - Fully achieved\' THEN 1 END)/COUNT(*))*100),2) as tot3'),
        DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'4 - Exceptionally achieved\' THEN 1 END)/COUNT(*))*100),2) as tot4'),
        DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'TEA - Too Early to Assess\' THEN 1 END)/COUNT(*))*100),2) as tot5'),
        DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'Unknown\' THEN 1 END)/COUNT(*))*100),2) as tot6'))
        ->whereNotNull('orglevel1')
        ->groupBy('orglevel1');

            $dbperformances = $dbperformances->get();
            return $dbperformances;
                    }catch (\Exception $exception){
                        logger($exception);
                        return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
                    }
    }


    public static function SalaryIncreaseAnalysis(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];

        $manager_pn         = $manager_pn->personnelnumber;

        $salaryIncreaseData = DB::table('vw_employees_export');
        $salaryIncreaseData = Filters::applyFilters($salaryIncreaseData, Auth::user());
        $salaryIncreaseData = $salaryIncreaseData->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $salaryIncreaseData = $salaryIncreaseData->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                        ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                });
                }

    }

    public static function headCount(){
                 //heatcount recieving increase
             $manager_pn      = Auth::user();
                $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
                $active_instance = PermissionController::getUserInstance()['response']['instance'];

                $manager_pn         = $manager_pn->personnelnumber;

                $contIncrease = DB::table('vw_employees_export');
                $contIncrease = Filters::applyFilters($contIncrease, Auth::user());
                $contIncrease = $contIncrease->where('instance_indicator_id','=',$active_instance);

            try{
                if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                    $contIncrease = $contIncrease->where(function ($q) use($manager_pn) {
                        $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                            ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                            ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                            ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                            ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                            ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                            ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                            ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                            ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                            ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                            ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                            ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                    });
                }

            $contIncrease->select(
                DB::raw('count(*) as headcount')
            )
            ->where('proposedsalary_percentage', '>',0);
            $contIncrease =  $contIncrease->get();
            return $contIncrease;
        }catch (\Exception $exception){
            logger($exception);
            return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
        }
    }

    public static function StatDate(){
        //startdate
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];


        $countLessY = DB::table('vw_employees_export');
        $countLessY = Filters::applyFilters($countLessY, Auth::user());
        $countLessY = $countLessY->where('instance_indicator_id','=',$active_instance);
        try{
            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $countLessY = $countLessY->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                        ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


                });
            }
                $countLessY->select(
                    DB::raw('count(*) as headcount')
                )
                ->whereColumn('startdate', '>', 'startdate');
                $countLessY =  $countLessY->get();

                return $countLessY;
            }catch (\Exception $exception){
                logger($exception);
                return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
            }
    }

    //Increace Spent Per Rating
    public static function getProposeSpent(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];

        $manager_pn         = $manager_pn->personnelnumber;

        $proposedspent = DB::table('vw_employees_export');
        $proposedspent = Filters::applyFilters($proposedspent, Auth::user());
        $proposedspent = $proposedspent->where('instance_indicator_id','=',$active_instance);

        try{
        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
        $proposedspent = $proposedspent->where(function ($q) use($manager_pn) {
        $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
            ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
            ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
            ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
            ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
            ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
            ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
            ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
            ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
            ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
            ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
            ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
        }
            $proposedspent->select('current_rating_id', DB::raw('count(*) as total'),
            //DB::raw('ROUND(AVG( calc_proposedsalary_zar ),2) as bandsSalAvg'))
            DB::raw('ROUND(SUM( calc_proposedsalary_zar - calc_salary_zar ),2) as bandsSalAvg'))
            ->whereNotNull('current_rating_id')
            //*//
            ->where(function ($q) {$q->orwhere('do_not_process_salary', NULL)->orWhere('do_not_process_salary', 0);})
            ->groupBy('current_rating_id');
            $proposedspent =  $proposedspent->get();

            return $proposedspent;
            }catch (\Exception $exception){
            logger($exception);
            return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
            }
    }


         //incentive analysis dashboard

    public static function getIncentiveAnalData(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];

        $manager_pn         = $manager_pn->personnelnumber;

        $dbIncentives = DB::table('vw_employees_export');
        $dbIncentives = Filters::applyFilters($dbIncentives, Auth::user());
        $dbIncentives = $dbIncentives->where('instance_indicator_id','=',$active_instance);

    try{
        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbIncentives = $dbIncentives->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
                    $dbIncentives->select( DB::raw('count(*) as total'),
                    DB::raw('FORMAT(SUM(calc_proposedincentive_zar),0) as totsum'),
                    DB::raw('FORMAT((SUM(calc_proposedincentive_zar)/ SUM(calc_salary_zar)) * 100,1) as ctcsum'),
                    DB::raw('COUNT(*) as numemp'),
                    DB::raw('ROUND(COUNT(CASE WHEN proposedincentive_percentage > 0 THEN 1 END),0) as empIncr'), //Headcount receiving an Incentive

                    DB::raw('FORMAT(MAX(calc_proposedincentive_zar),0) as highproposedzar'), //highest incentive value
                    DB::raw('ROUND(MAX(proposedincentive_percentage),1) as highperce'),//highest incentive percentage

                    //DB::raw('ROUND(MIN(calc_proposedincentive_zar),2) as lowincentive'),
                    DB::raw('FORMAT(MIN(CASE WHEN calc_proposedincentive_zar > 0  THEN calc_proposedincentive_zar END),0) as lowincentive'), //lowest incentive value

                    //DB::raw('ROUND(MIN(proposedincentive_percentage),2) as lowhighrace'),
                    DB::raw('ROUND(MIN(CASE WHEN proposedincentive_percentage > 0  THEN proposedincentive_percentage END),1) as lowhighrace'), //lowest incentive percentage

                    DB::raw('ROUND(AVG(IFNULL(calc_proposedincentive_zar, 0)),0) as avg_incentive_percentage'),
                    //DB::raw('FORMAT(AVG(CASE WHEN calc_proposedincentive_zar > 0  THEN calc_proposedincentive_zar END),0) as avg_incentive_percentage'), //Average Incentive value

                    DB::raw('ROUND(AVG(IFNULL(proposedincentive_percentage, 0)),1) as lowhighpere'),
                    //DB::raw('ROUND(AVG(CASE WHEN proposedincentive_percentage > 0  THEN proposedincentive_percentage END),1) as lowhighpere'), // average incentive percentage

                    //proposed increase percentage per band
                    DB::raw('ROUND(AVG(CASE WHEN (band_id = 1) AND (proposedsalary_percentage>0) THEN proposedsalary_percentage END),1) as b1tot'),
                    DB::raw('ROUND(AVG(CASE WHEN (band_id = 2) AND (proposedsalary_percentage>0) THEN proposedsalary_percentage END),1) as b2tot'),
                    DB::raw('ROUND(AVG(CASE WHEN (band_id = 3) AND (proposedsalary_percentage>0) THEN proposedsalary_percentage END),1) as b3tot'),
                    DB::raw('ROUND(AVG(CASE WHEN (band_id = 4) AND (proposedsalary_percentage>0) THEN proposedsalary_percentage END),1) as b4tot'),
                    DB::raw('ROUND(AVG(CASE WHEN (band_id = 5) AND (proposedsalary_percentage>0) THEN proposedsalary_percentage END),1) as b5tot'),

                    //proposed incentive percentage per band
                    DB::raw('ROUND(AVG(CASE WHEN (band_id = 1) AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),1) as b1perc'),
                    DB::raw('ROUND(AVG(CASE WHEN (band_id = 2) AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),1) as b2perc'),
                    DB::raw('ROUND(AVG(CASE WHEN (band_id = 3) AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),1) as b3perc'),
                    DB::raw('ROUND(AVG(CASE WHEN (band_id = 4) AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),1) as b4perc'),
                    DB::raw('ROUND(AVG(CASE WHEN (band_id = 5) AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),1) as b5perc')
                );
                 $dbIncentives = $dbIncentives->get();
                 return $dbIncentives;

         }catch (\Exception $exception){
             logger($exception);
             return \redirect()->back()->with('error', 'Something went wrong. Please contact administrator.');
         }
    }

    public static function getdbPerformScheme(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];

        $manager_pn         = $manager_pn->personnelnumber;

        $dbperfperscheme = DB::table('vw_employees_export');
        $dbperfperscheme = Filters::applyFilters($dbperfperscheme, Auth::user());
        $dbperfperscheme = $dbperfperscheme->where('instance_indicator_id','=',$active_instance);
        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbperfperscheme = $dbperfperscheme->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
         $dbperfperscheme->select( DB::raw('count(*) as total'),
            DB::raw('ROUND((AVG(CASE WHEN (current_rating = \'1 - Not achieved\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END)),0) as tot1'),
            DB::raw('ROUND((AVG(CASE WHEN (current_rating = \'2 - Partially achieved\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END)),0) as tot2'),
            DB::raw('ROUND((AVG(CASE WHEN (current_rating = \'3 - Fully achieved\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END)),0) as tot3'),
            DB::raw('ROUND((AVG(CASE WHEN (current_rating = \'4 - Exceptionally achieved\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END)),0) as tot4'),
            DB::raw('ROUND(AVG(CASE WHEN (current_rating = \'TEA - Too early to assess\') AND (proposedincentive_percentage>0) THEN proposedincentive_percentage END),0) as tot5'),

             DB::raw('ROUND((SUM(CASE WHEN (current_rating = \'1 - Not achieved\') THEN proposedincentive_value END)),0) as overalltot1'),
            DB::raw('ROUND((SUM(CASE WHEN (current_rating = \'2 - Partially achieved\') THEN proposedincentive_value END)),0) as overalltot2'),
            DB::raw('ROUND((SUM(CASE WHEN (current_rating = \'3 - Fully achieved\') THEN proposedincentive_value END)),0) as overalltot3'),
            DB::raw('ROUND((SUM(CASE WHEN (current_rating = \'4 - Exceptionally achieved\') THEN proposedincentive_value END)),0) as overalltot4'),
            DB::raw('ROUND((SUM(CASE WHEN (current_rating = \'TEA - Too early to assess\') THEN proposedincentive_value END)),0) as overalltot5')
        );


         $dbperfperscheme = $dbperfperscheme->get();
         return $dbperfperscheme;




    }
    public static function getdbAvgCluster(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];

        $manager_pn         = $manager_pn->personnelnumber;

        $avgclusters = DB::table('vw_employees_export');
        $avgclusters = Filters::applyFilters($avgclusters, Auth::user());
        $avgclusters = $avgclusters->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $avgclusters = $avgclusters->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }


         $avgclusters->select('orglevel5', DB::raw('count(*) as total'),
            DB::raw('ROUND(SUM(CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus !=1 ) THEN calc_proposedincentive_zar  END),0) as clusterAvg'))
            ->whereNotNull('orglevel5')
            ->groupBy('orglevel5');

         $avgclusters =  $avgclusters->get();
         return $avgclusters ;
    }
    public static function getdbSalaryAnalysisTopFifty(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];

        $manager_pn         = $manager_pn->personnelnumber;

        $dbsalaryanalysis = DB::table('vw_employees_export');
        $dbsalaryanalysis = Filters::applyFilters($dbsalaryanalysis, Auth::user());
        $dbsalaryanalysis = $dbsalaryanalysis->where('instance_indicator_id','=',$active_instance);
        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbsalaryanalysis = $dbsalaryanalysis->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }


         $dbsalaryanalysis->select ('knownas', 'lastname', 'proposedsalary_value')
            ->where(function ($q) {$q->where('do_not_process_salary', NULL)->orWhere('do_not_process_salary', 0);})
            ->orderBy ('calc_proposedsalary_zar',  'DESC')
            ->take(50);

         $dbsalaryanalysis = $dbsalaryanalysis->get();
        return $dbsalaryanalysis;



    }

    public static function getdbSalaryAnalysisTopFiftyIncentive(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];

        $manager_pn         = $manager_pn->personnelnumber;

        $dbinsentiveanalysis = DB::table('vw_employees_export');
        $dbinsentiveanalysis = Filters::applyFilters($dbinsentiveanalysis, Auth::user());
        $dbinsentiveanalysis = $dbinsentiveanalysis->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbinsentiveanalysis = $dbinsentiveanalysis->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }


            $dbinsentiveanalysis->select ('knownas', 'lastname', 'proposedincentive_value')
            ->where(function ($q) {$q->orwhere('do_not_process_bonus', NULL)->orWhere('do_not_process_bonus', 0);})
            ->orderBy ('proposedincentive_value', 'DESC')
            ->take(50);
                $dbinsentiveanalysis = $dbinsentiveanalysis->get();

                return $dbinsentiveanalysis;



    }
    public static function getTopTwentyEarners(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn         = $manager_pn->personnelnumber;

        $dbsalaryanalysis = DB::table('vw_employees_export');
        $dbsalaryanalysis = Filters::applyFilters($dbsalaryanalysis, Auth::user());
        $dbsalaryanalysis = $dbsalaryanalysis->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbsalaryanalysis = $dbsalaryanalysis->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }



    // logger($active_instance);
        // $dbsalaryanalysis = $dbsalaryanalysis->select (
        //             'lastname',
        //             'firstnames',
        //             'current_rating',
        //             'salary_value',
        //             'h_1_salary_value',
        //             'proposedsalary_percentage',
        //             'h_1_incentive_value',
        //             'proposedincentive_value',
        //             DB::raw('FORMAT(( proposedincentive_percentage - h_1_incentive_percentage),2) as bonus_variance ')

        //         )
        //         ->where('salary_value','>','0')
        //         ->orderBy ('salary_value',  'DESC')
        //         ->take(20);

        $dbsalaryanalysis->select (
            'lastname',
            'firstnames',
            'current_rating',
            DB::raw('ROUND(calc_salary_zar, 0) as calc_salary_zar'),
            DB::raw('ROUND(h_1_salary_zar, 0) as h_1_salary_zar'),
            DB::raw('ROUND(proposedsalary_percentage, 1) as proposedsalary_percentage'),
            DB::raw('ROUND(h_1_incentive_zar,0) as h_1_incentive_zar'),
            DB::raw('ROUND(calc_proposedincentive_zar, 0) as calc_proposedincentive_zar'),
            DB::raw('FORMAT(( (calc_proposedincentive_zar - h_1_incentive_zar)/h_1_incentive_zar * 100), 1) as bonus_variance '),
            DB::raw('ROUND((h_1_incentive_zar + calc_salary_zar), 0) as top_earner ')
              )
        ->where(function ($q) {
            $q->where('do_not_process_salary', NULL)
            ->orWhere('do_not_process_salary', 0);})
        ->orderBy ('top_earner',  'DESC')
        ->take(20);


        return  $dbsalaryanalysis->get() ;
    }
    public static function getBusinessReviewtwoModelData(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn         = $manager_pn->personnelnumber;

        $dbperformances = DB::table('vw_employees_export');
        $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
        $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

           if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
        $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
    }
        $dbperformances->select('orglevel1', DB::raw('count(*) as overall_total_head_count'),
            DB::raw(' COUNT(CASE WHEN current_rating = \'1 - Not Achieved\' THEN 1 END) as overall_total_head_count_not_achieved'),
            DB::raw(' COUNT(CASE WHEN current_rating = \'2 - Partially achieved\' THEN 1 END) as overall_total_head_count_partially_achieved'),
            DB::raw(' COUNT(CASE WHEN current_rating = \'3 - Fully achieved\' THEN 1 END) as overall_total_head_count_fully_achieved'),
            DB::raw(' COUNT(CASE WHEN current_rating = \'4 - Exceptionally achieved\' THEN 1 END) as overall_total_head_count_exceptionally_achieved'),
            DB::raw(' COUNT(CASE WHEN current_rating = \'TEA - Too Early to Assess\' THEN 1 END) as overall_total_head_count_tea'),
            DB::raw(' COUNT(CASE WHEN current_rating = \'Unknown\' THEN 1 END) as overall_total_head_count_unknown'),


            DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'1 - Not achieved\' THEN 1 END)/COUNT(*))*100),2) as overall_total_head_count_not_achieved_percentage'),
            DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'2 - Partially achieved\' THEN 1 END)/COUNT(*))*100),2) as overall_total_head_count_partially_achieved_percentage'),
            DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'3 - Fully achieved\' THEN 1 END)/COUNT(*))*100),2) as overall_total_head_count_fully_achieved_percentage'),
            DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'4 - Exceptionally achieved\' THEN 1 END)/COUNT(*))*100),2) as overall_total_head_count_exceptionally_achieved_percentage'),
            DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'TEA - Too early to assess\' THEN 1 END)/COUNT(*))*100),2) as overall_total_head_count_tea_percentage'),
            DB::raw('ROUND(((COUNT(CASE WHEN current_rating = \'Unknown\' THEN 1 END)/COUNT(*))*100),2) as overall_total_head_count_unknown_percentage'))

            ->whereNotNull('orglevel1')
            ->groupBy('orglevel1');

        $dbperformances = $dbperformances->get();
        return $dbperformances;
    }
    public static function getBusinessReviewtwoModelData_BandPercentage(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn         = $manager_pn->personnelnumber;

        $dbperformances = DB::table('vw_employees_export');
        $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
        $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

           if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
            }
            $dbperformances->select(
                'band_id',
                DB::raw('count(*) as total_band_head_count'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 1 THEN 1 END)/COUNT(*))*100),2) as band_not_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 2 THEN 1 END)/COUNT(*))*100),2) as band_partially_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 3 THEN 1 END)/COUNT(*))*100),2) as band_fully_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 4 THEN 1 END)/COUNT(*))*100),2) as band_exceptionally_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 5 THEN 1 END)/COUNT(*))*100),2) as band_tea'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id IS NULL THEN 1 END)/COUNT(*))*100),2) as band_unknown'))
                ->whereNotNull('band_id')
                ->groupBy('band_id');

                $dbperformances = $dbperformances->get();
                return $dbperformances;

    }

    public static function getBusinessReviewtwoModelData_BandPercentageB3B4(){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn         = $manager_pn->personnelnumber;

        $dbperformances = DB::table('vw_employees_export');
        $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
        $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

           if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
            }
            $dbperformances->select(
                'instance_indicator_id',
                DB::raw('count(*) as total_band_head_count'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 1 THEN 1 END)/COUNT(*))*100),1) as band_not_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 2 THEN 1 END)/COUNT(*))*100),1) as band_partially_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 3 THEN 1 END)/COUNT(*))*100),1) as band_fully_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 4 THEN 1 END)/COUNT(*))*100),1) as band_exceptionally_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 5 THEN 1 END)/COUNT(*))*100),1) as band_tea'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id IS NULL THEN 1 END)/COUNT(*))*100),1) as band_unknown'))
                ->whereIN('band_id',[3,4])
                ->whereNotNull('band_id')
                ->groupBy('instance_indicator_id');

                $dbperformances = $dbperformances->get();
                return $dbperformances;

    }


    public static function getBusinessReviewtwoModelData_BandCount($band_id){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn         = $manager_pn->personnelnumber;

        $dbperformances = DB::table('vw_employees_export');
        $dbperformances = Filters::applyFilters($dbperformances, Auth::user());

           if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
            }
            $dbperformances->select(
                $band_id,
                DB::raw('count(*) as total_band_head_count'),
                DB::raw('COUNT(CASE WHEN current_rating_id = 1 THEN 1 END) as total_band1_not_achieved'),
                DB::raw('COUNT(CASE WHEN current_rating_id = 2 THEN 1 END) as total_band2_partially_achieved'),
                DB::raw('COUNT(CASE WHEN current_rating_id = 3 THEN 1 END) as total_band3_fully_achieved'),
                DB::raw('COUNT(CASE WHEN current_rating_id = 4 THEN 1 END) as total_band4_exceptionally_achieved'),
                DB::raw('COUNT(CASE WHEN current_rating_id = 5 THEN 1 END) as total_band5_tea'),
                DB::raw('COUNT(CASE WHEN current_rating_id IS NULL THEN 1 END) as total_band_unknown'))
                ->whereNotNull($band_id)
                ->groupBy($band_id);


                $dbperformances = $dbperformances->get();
                return $dbperformances;

    }
    public static function getBusinessReviewtwoModelData_BandAverageIncrease($band_id){
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn         = $manager_pn->personnelnumber;

        $dbperformances = DB::table('vw_employees_export');
        $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
        $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

           if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
            }
            $dbperformances->select(
                $band_id,
                DB::raw('count(*) as total_band_head_count'),

                // DB::raw('COUNT(CASE WHEN current_rating_id = 1 THEN 1 END) as count_band1_not_achieved'),
                // DB::raw('COUNT(CASE WHEN current_rating_id = 2 THEN 1 END) as count_band2_partially_achieved'),
                // DB::raw('COUNT(CASE WHEN current_rating_id = 3 THEN 1 END) as count_band3_fully_achieved'),
                // DB::raw('COUNT(CASE WHEN current_rating_id = 4 THEN 1 END) as count_band4_exceptionally_achieved'),
                // DB::raw('COUNT(CASE WHEN current_rating_id = 5 THEN 1 END) as count_band5_tea'),

                //     DB::raw('ROUND(AVG(CASE WHEN current_rating_id = 1 THEN (((calc_proposedsalary_zar-calc_salary_zar)/calc_salary_zar)*100) END),2) as band1_percentage_average'),
                //     DB::raw('ROUND(AVG(CASE WHEN current_rating_id = 2 THEN (((calc_proposedsalary_zar-calc_salary_zar)/calc_salary_zar)*100) END),2) as band2_percentage_average'),
                //     DB::raw('ROUND(AVG(CASE WHEN current_rating_id = 3 THEN (((calc_proposedsalary_zar-calc_salary_zar)/calc_salary_zar)*100) END),2) as band3_percentage_average'),
                //     DB::raw('ROUND(AVG(CASE WHEN current_rating_id = 4 THEN (((calc_proposedsalary_zar-calc_salary_zar)/calc_salary_zar)*100) END),2) as band4_percentage_average'),
                //     DB::raw('ROUND(AVG(CASE WHEN current_rating_id = 5 THEN (((calc_proposedsalary_zar-calc_salary_zar)/calc_salary_zar)*100) END),2) as band5_percentage_average'),

                    DB::raw('ROUND(AVG(CASE WHEN (current_rating_id = 1 ) THEN (((calc_proposedsalary_zar-calc_salary_zar)/calc_salary_zar)*100) END),2) as percentage_average_NA'),
                    DB::raw('ROUND(AVG(CASE WHEN  (current_rating_id = 2 ) THEN (((calc_proposedsalary_zar-calc_salary_zar)/calc_salary_zar)*100) END),2) as percentage_average_PA'),
                    DB::raw('ROUND(AVG(CASE WHEN  (current_rating_id = 3 )  THEN (((calc_proposedsalary_zar-calc_salary_zar)/calc_salary_zar)*100) END),2) as percentage_average_FA'),
                    DB::raw('ROUND(AVG(CASE WHEN  (current_rating_id = 4 ) THEN (((calc_proposedsalary_zar-calc_salary_zar)/calc_salary_zar)*100) END),2) as percentage_average_EA'),
                    DB::raw('ROUND(AVG(CASE WHEN  (current_rating_id = 5 ) THEN (((calc_proposedsalary_zar-calc_salary_zar)/calc_salary_zar)*100) END),2) as percentage_average_TEA')
                    )
                ->whereNotNull($band_id)
                ->groupBy($band_id);


                $dbperformances = $dbperformances->get();
                return $dbperformances;

    }
    public static function getSeniorManagementandSpecialistTableData(){
          $manager_pn      = Auth::user();
          $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
          $active_instance = PermissionController::getUserInstance()['response']['instance'];
          $manager_pn      = $manager_pn->personnelnumber;

          $dbperformances = DB::table('vw_employees_export');
          $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
          $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

             if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
              $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
                  $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                      ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                      ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                      ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                      ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                      ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                      ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                      ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                      ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                      ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                      ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                      ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


              });
              }



                    $dbperformances->select(
                        'scheme_id',
                        DB::raw('count( CASE WHEN (scheme_id = 43) THEN 1 END ) as head_count_sp'),
                        DB::raw('count( CASE WHEN (scheme_id = 23 OR scheme_id = 20) THEN 1 END ) as head_count_seniorman'),
                        // DB::raw('sum( CASE WHEN (scheme_id = 43) THEN calc_kpi_incentive_value END) as calc_kpi_incentive_value_sp'),
                        DB::raw('sum( CASE WHEN (scheme_id = 43) THEN proposedincentive_value END) as proposed_bonus_zar_sp'),
                        DB::raw('sum( CASE WHEN (scheme_id = 23 OR scheme_id = 20) THEN proposedincentive_value END) as proposed_bonus_zar_seniorman'),

                        DB::raw('sum( CASE WHEN (scheme_id = 43) THEN calc_salary_zar *(calc_non_financial / 100) * 0.85 END) as total_actual_pool_sp'),
                        DB::raw('sum( CASE WHEN (scheme_id = 23 OR scheme_id = 20) THEN calc_salary_zar *(calc_non_financial / 100) * 0.85 END) as total_actual_pool_seniorman')



                        )
                        ->whereNotNull('scheme_id')
                        ->groupBy('scheme_id');


                        $dbperformances = $dbperformances->get();
                        return $dbperformances;


    }
    public static function getGeneralStaffTableData(){
          $manager_pn      = Auth::user();
          $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
          $active_instance = PermissionController::getUserInstance()['response']['instance'];

          $manager_pn         = $manager_pn->personnelnumber;

          $dbperformances = DB::table('vw_employees_export');
          $dbperformances = Filters::applyFilters($dbperformances, Auth::user());
          $dbperformances = $dbperformances->where('instance_indicator_id','=',$active_instance);

             if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
              $dbperformances = $dbperformances->where(function ($q) use($manager_pn) {
                  $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                      ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                      ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                      ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                      ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                      ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                      ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                      ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                      ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                      ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                      ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                      ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


              });
              }

                    $dbperformances->select(
                        'band_id',
                        DB::raw('count( CASE WHEN (scheme_id = 13 AND band_id = 2) THEN 1 END ) as head_count_b1'),
                        DB::raw('count( CASE WHEN (scheme_id = 13 AND band_id = 3) THEN 1 END ) as head_count_b2'),
                        DB::raw('count( CASE WHEN (scheme_id = 13 AND band_id = 4) THEN 1 END ) as head_count_b4'),
                        DB::raw('count( CASE WHEN (scheme_id = 13 AND band_id = 5) THEN 1 END ) as head_count_b5'),

                        DB::raw('sum( CASE WHEN (scheme_id = 13 AND band_id = 2) THEN proposedincentive_value END) as proposed_bonus_zar_b2'),
                        DB::raw('sum( CASE WHEN (scheme_id = 13 AND band_id = 3) THEN proposedincentive_value END) as proposed_bonus_zar_b3'),
                        DB::raw('sum( CASE WHEN (scheme_id = 13 AND band_id = 4) THEN proposedincentive_value END) as proposed_bonus_zar_b4'),
                        DB::raw('sum( CASE WHEN (scheme_id = 13 AND band_id = 5) THEN proposedincentive_value END) as proposed_bonus_zar_b5')
                        )
                        ->whereNotNull('band_id')
                        ->groupBy('band_id');


                        $dbperformances = $dbperformances->get();
                        return $dbperformances;


    }

    public static function getAfricaBusinessReviewGeneralSalaries(){
        $manager_pn      = Auth::user();
          $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
          $active_instance = PermissionController::getUserInstance()['response']['instance'];

          $manager_pn         = $manager_pn->personnelnumber;

          $dbGensalaries = DB::table('vw_employees_export');
          $dbGensalaries = Filters::applyFilters($dbGensalaries, Auth::user());
          $dbGensalaries = $dbGensalaries->where('instance_indicator_id','=',$active_instance);
          if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbGensalaries = $dbGensalaries->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
          $dbGensalaries->select('orglevel7',
                                    DB::raw('FORMAT(SUM(CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary <> 1 THEN calc_salary_zar * conversion_rate END) ,0) as total'),
                                    DB::raw('FORMAT(SUM( calc_proposedincentive_zar * conversion_rate),0) as bonus'),

                                    DB::raw('COUNT(*) as headcount'),
                                    //DB::raw('FORMAT(AVG(proposedincentive_percentage),2) as actual')
                                    DB::raw('FORMAT((( (sum(calc_proposedincentive_zar * conversion_rate) ) / 
                                                (sum(calc_salary_zar * conversion_rate ) ) )* 100  ) , 1) as actual')

                                )

                    ->whereNotNull('orglevel7')
                     ->where('scheme_id',12)
                    ->groupBy('orglevel7');

                 $dbGensalaries =  $dbGensalaries->get();
                 return $dbGensalaries;







    }

    public static function getGeneralStaffBandsBonus(){
        $manager_pn      = Auth::user();
          $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
          $active_instance = PermissionController::getUserInstance()['response']['instance'];

          $manager_pn         = $manager_pn->personnelnumber;

          $dbGeneralBonus = DB::table('vw_employees_export');
          $dbGeneralBonus = Filters::applyFilters($dbGeneralBonus, Auth::user());
          $dbGeneralBonus = $dbGeneralBonus->where('instance_indicator_id','=',$active_instance);

          if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbGeneralBonus = $dbGeneralBonus->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
          $dbGeneralBonus->select('band_id',
                                    DB::raw('SUM(CASE WHEN (do_not_process_salary IS NULL) OR (do_not_process_salary <> 1) THEN proposedincentive_value END) as sum_total_bonus'),
                                    // DB::raw('sum(CASE WHEN (do_not_process_salary IS NULL) OR (do_not_process_salary <> 1) THEN h_1_incentive_value END) as avg_h1_bonus_total'),
                                    // DB::raw('FORMAT(SUM( calc_proposedincentive_zar * conversion_rate),2) as bonus'),
                                    DB::raw('COUNT(*) as headcount'),
                                    //DB::raw('FORMAT(AVG(proposedincentive_percentage),2) as actual')
                                    DB::raw('FORMAT((( (SUM(proposedincentive_value) ) / 
                                    SUM(salary_value) )* 100  ) , 2) as bonus_salary_percentage')

                                )

                    ->whereNotNull('band_id')
                     ->where('scheme_id',13)
                    ->groupBy('band_id');

                 $dbGeneralBonus =  $dbGeneralBonus->get();
                 return $dbGeneralBonus;







    }


    public static function getAfricaBusinessReviewSeniorSalaries(){
        $manager_pn      = Auth::user();
          $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
          $active_instance = PermissionController::getUserInstance()['response']['instance'];

          $manager_pn         = $manager_pn->personnelnumber;

          $dbsalaries = DB::table('vw_employees_export');
          $dbsalaries = Filters::applyFilters($dbsalaries, Auth::user());
          $dbsalaries = $dbsalaries->where('instance_indicator_id','=',$active_instance);
          if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbsalaries = $dbsalaries->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
          $dbsalaries->select('orglevel7',
                    DB::raw('FORMAT(COALESCE(SUM(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN calc_salary_zar * conversion_rate END),0)  ,0) as total'),
                    DB::raw('COUNT(*) as headcount'),
                    DB::raw('ROUND((((( (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar * conversion_rate END) )  - (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar * conversion_rate END) ) ) /
                     (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar * conversion_rate END) ) )* 100)  ) , 1) as actual'),

                    DB::raw('FORMAT(COALESCE( (((
                    ((sum(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN
                    calc_proposedsalary_zar
                    END) - sum(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN 
                    calc_salary_zar
                     END)) / sum( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN 
                     calc_salary_zar * conversion_rate
                   END) ))*100) ),0), 1) as uplift')
                )

                    ->whereNotNull('orglevel7')
                     ->whereIn('scheme_id', [12, 24])
                    ->groupBy('orglevel7');

                 $dbsalaries =  $dbsalaries->get();
                 return $dbsalaries;







    }
    public static function getAfricaBusinessReviewSeniorBonus(){
        $manager_pn      = Auth::user();
          $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
          $active_instance = PermissionController::getUserInstance()['response']['instance'];

          $manager_pn       = $manager_pn->personnelnumber;
          $dbbonus = DB::table('vw_employees_export');
          $dbbonus = Filters::applyFilters($dbbonus, Auth::user());
          $dbbonus = $dbbonus->where('instance_indicator_id','=',$active_instance);


          if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $dbbonus = $dbbonus->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
          $dbbonus->select('orglevel7',
          DB::raw('FORMAT(SUM(CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary <> 1 THEN calc_salary_zar * conversion_rate END) ,0) as total'),
          DB::raw('FORMAT(SUM( calc_proposedincentive_zar * conversion_rate),0) as bonus'),
          DB::raw('COUNT(*) as headcount'),
          // DB::raw('FORMAT(AVG(proposedincentive_percentage),2) as actual')

          DB::raw('FORMAT((( (sum(calc_proposedincentive_zar * conversion_rate) ) / 
          (sum(calc_salary_zar * conversion_rate ) ) )* 100  ) , 1) as actual')
                )

                    ->whereNotNull('orglevel7')
                    ->where('scheme_id',24)
                    ->groupBy('orglevel7');

                 $dbbonus =  $dbbonus->get();
                 return $dbbonus;







    }

    public static function getoverallPerformanceDistribution_BusinessReview(){

        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $overallPerformanceHeadcount = DB::table('vw_employees_export');
        $overallPerformanceHeadcount = Filters::applyFilters($overallPerformanceHeadcount, Auth::user());
        $overallPerformanceHeadcount = $overallPerformanceHeadcount->where('instance_indicator_id','=',$active_instance);

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $overallPerformanceHeadcount = $overallPerformanceHeadcount->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
        $overallPerformanceHeadcount = $overallPerformanceHeadcount->whereIn('status_id',  [1,7,19]);

        $overallPerformanceHeadcount->select('current_rating_id',
            DB::raw('COUNT(*) AS total_count '),
            /*DB::raw('(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'2 - Partially achieved\') THEN COUNT(*) END) AS partially_achieved_count '),
            DB::raw('(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'3 - Fully achieved\') THEN COUNT(*) END) AS fully_achieved_count '),
            DB::raw('(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'4 - Exceptionally achieved\') THEN COUNT(*) END) AS exceptionally_achieved_count '),
            DB::raw('(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (current_rating = \'TEA - Too early to assess\') THEN COUNT(*) END) AS tstr ')*/

            DB::raw('(CASE WHEN (current_rating = \'2 - Partially achieved\') THEN COUNT(*) END) AS partially_achieved_count '),
            DB::raw('(CASE WHEN (current_rating = \'3 - Fully achieved\') THEN COUNT(*) END) AS fully_achieved_count '),
            DB::raw('(CASE WHEN (current_rating = \'4 - Exceptionally achieved\') THEN COUNT(*) END) AS exceptionally_achieved_count '),
            DB::raw('(CASE WHEN (current_rating = \'TEA - Too early to assess\') THEN COUNT(*) END) AS tstr ')
            )
            // ->whereNotNull('current_rating')
            ->groupBy('current_rating_id');

            $overallPerformanceHeadcount =  $overallPerformanceHeadcount->get();
            return $overallPerformanceHeadcount ;



    }


    public static function getAverageBandIncrease_businessReview(){


        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $BandIncreaseMendate = DB::table('vw_employees_export');
        $BandIncreaseMendate = Filters::applyFilters($BandIncreaseMendate, Auth::user());
        $BandIncreaseMendate = $BandIncreaseMendate->where('instance_indicator_id','=',$active_instance);


            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $BandIncreaseMendate = $BandIncreaseMendate->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }

        $BandIncreaseMendate= $BandIncreaseMendate->whereIn('status_id',  [1,7,19]);

        /*$BandIncreaseMendate->select(
            'band_id',
            DB::raw('ROUND((CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (band_id=1) THEN AVG(proposedsalary_percentage) END ),2) AS b1_avg_percentage_increase'),
            DB::raw('ROUND((CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (band_id=2) THEN AVG(proposedsalary_percentage) END ),2) AS b2_avg_percentage_increase'),
            DB::raw('ROUND((CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (band_id=3) THEN AVG(proposedsalary_percentage) END ),2) AS b3_avg_percentage_increase'),
            DB::raw('ROUND((CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (band_id=4) THEN AVG(proposedsalary_percentage) END ),2) AS b4_avg_percentage_increase'),
            DB::raw('ROUND((CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (band_id=5) THEN AVG(proposedsalary_percentage) END ),2) AS b5_avg_percentage_increase'),


            // DB::raw('ROUND((CASE WHEN (band_id=1 OR band_id=2) THEN AVG(proposedsalary_percentage) END ),2) AS b5_avg_percentage_increase_MAX'),

            DB::raw('COUNT(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (band_id=1) THEN 1 END ) AS b1_count_increase'),
            DB::raw('COUNT(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (band_id=2) THEN 1 END ) AS b2_count_increase'),
            DB::raw('COUNT(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (band_id=3) THEN 1 END ) AS b3_count_increase'),
            DB::raw('COUNT(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (band_id=4) THEN 1 END ) AS b4_count_increase'),
            DB::raw('COUNT(CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) AND (band_id=5) THEN 1 END ) AS b5_count_increase')

        )*/
        $BandIncreaseMendate->select(
            'band_id',
            DB::raw('ROUND((CASE WHEN (band_id=1) THEN AVG(proposedsalary_percentage) END ),2) AS b1_avg_percentage_increase'),
            DB::raw('ROUND((CASE WHEN (band_id=2) THEN AVG(proposedsalary_percentage) END ),2) AS b2_avg_percentage_increase'),
            DB::raw('ROUND((CASE WHEN (band_id=3) THEN AVG(proposedsalary_percentage) END ),2) AS b3_avg_percentage_increase'),
            DB::raw('ROUND((CASE WHEN (band_id=4) THEN AVG(proposedsalary_percentage) END ),2) AS b4_avg_percentage_increase'),
            DB::raw('ROUND((CASE WHEN (band_id=5) THEN AVG(proposedsalary_percentage) END ),2) AS b5_avg_percentage_increase'),


            // DB::raw('ROUND((CASE WHEN (band_id=1 OR band_id=2) THEN AVG(proposedsalary_percentage) END ),2) AS b5_avg_percentage_increase_MAX'),

            DB::raw('COUNT(CASE WHEN (band_id=1) THEN 1 END ) AS b1_count_increase'),
            DB::raw('COUNT(CASE WHEN (band_id=2) THEN 1 END ) AS b2_count_increase'),
            DB::raw('COUNT(CASE WHEN (band_id=3) THEN 1 END ) AS b3_count_increase'),
            DB::raw('COUNT(CASE WHEN (band_id=4) THEN 1 END ) AS b4_count_increase'),
            DB::raw('COUNT(CASE WHEN (band_id=5) THEN 1 END ) AS b5_count_increase')

        )


        ->whereNotNull('current_rating_id')
        ->groupBy('band_id');
        $BandIncreaseMendate =  $BandIncreaseMendate->get();
        return  $BandIncreaseMendate;

        // logger( "");
        // logger( $BandIncreaseMendate);
        // logger("method : ".__METHOD__.' Line: '.__LINE__ );



    }

    public static function getAverageBandIncrease_businessReview2($bands){


        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $BandIncreaseMendate = DB::table('vw_employees_export');
        $BandIncreaseMendate = Filters::applyFilters($BandIncreaseMendate, Auth::user());
        $BandIncreaseMendate = $BandIncreaseMendate->where('instance_indicator_id','=',$active_instance);

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $BandIncreaseMendate = $BandIncreaseMendate->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }

        $BandIncreaseMendate= $BandIncreaseMendate->whereIn('status_id',  [1, 7,19]);
        $BandIncreaseMendate->select(
            'instance_indicator_id',
            DB::raw('ROUND(AVG(  CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN proposedsalary_percentage END) ,1) AS avg_percentage_increase'),
            DB::raw('COUNT( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN 1 ELSE 0 END) AS count_increase')
        )
        ->whereIn('band_id', $bands)
        ->groupBy('instance_indicator_id');
        $BandIncreaseMendate =  $BandIncreaseMendate->get();
        return  $BandIncreaseMendate;

    }

    public static function getAverageBandIncrease_businessReviewTotalAverage(){


        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $BandIncreaseMendate = DB::table('vw_employees_export');
        $BandIncreaseMendate = Filters::applyFilters($BandIncreaseMendate, Auth::user());
        $BandIncreaseMendate = $BandIncreaseMendate->where('instance_indicator_id','=',$active_instance);


            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $BandIncreaseMendate = $BandIncreaseMendate->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }

        $BandIncreaseMendate= $BandIncreaseMendate->whereIn('status_id',  [1, 7,19]);
        $BandIncreaseMendate->select(
            'instance_indicator_id',
            DB::raw('ROUND(AVG(  CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN proposedsalary_percentage END) ,1) AS avg_percentage_increase'),
            DB::raw('COUNT( CASE WHEN (do_not_process_salary IS NULL OR do_not_process_salary != 1) THEN 1 ELSE 0 END) AS count_increase')

        )
            ->whereIn('band_id', [1,2,3,4,5])
        ->groupBy('instance_indicator_id');
        $BandIncreaseMendate =  $BandIncreaseMendate->get();
        return  $BandIncreaseMendate;

    }

    public static function getAveragePercentSalaryBandVsPerformanceB5B4(){

        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $BandSalaryVsPerformanceDb = DB::table('vw_employees_export');
        $BandSalaryVsPerformanceDb = Filters::applyFilters($BandSalaryVsPerformanceDb, Auth::user());
        $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
    }
        $BandSalaryVsPerformanceDb->select(
            'current_rating_id',
            DB::raw('ROUND(AVG(proposedsalary_percentage),1) as averageBandSalary')
        )
        ->whereIn('band_id', [4,5])
        ->groupBy('current_rating_id');
        return $BandSalaryVsPerformanceDb->get();
    }



    public static function getAverageSalaryBandVsPerformanceB5B4(){


        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $BandSalaryVsPerformanceDb = DB::table('vw_employees_export');
        $BandSalaryVsPerformanceDb = Filters::applyFilters($BandSalaryVsPerformanceDb, Auth::user());
        $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where('instance_indicator_id','=',$active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
    }

        $BandSalaryVsPerformanceDb->select(
            'current_rating_id',
            DB::raw('ROUND(AVG(proposedincentive_percentage),1) AS averageBandSalary')
        )
        ->whereIn('band_id', [4,5])
        ->groupBy('current_rating_id');
        $BandSalaryVsPerformanceDb=$BandSalaryVsPerformanceDb->get();
        return $BandSalaryVsPerformanceDb;
    }
    public static function getAveragePercentSalaryBandVsPerformanceB2(){

        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $BandSalaryVsPerformanceDb = DB::table('vw_employees_export');
        $BandSalaryVsPerformanceDb = Filters::applyFilters($BandSalaryVsPerformanceDb, Auth::user());
        $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
    }
        $BandSalaryVsPerformanceDb->select(
            'current_rating_id',
            DB::raw('ROUND(AVG(proposedsalary_percentage),1) AS averageBandSalary')
        )
        ->whereIn('band_id', [1,2])
        ->groupBy('current_rating_id');
        $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->get();
        return $BandSalaryVsPerformanceDb;
    }
    public static function getAverageSalaryBandVsPerformanceB2(){




        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];

        $manager_pn      = $manager_pn->personnelnumber;
        $BandSalaryVsPerformanceDb = DB::table('vw_employees_export');
        $BandSalaryVsPerformanceDb = Filters::applyFilters($BandSalaryVsPerformanceDb, Auth::user());
        $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
    }

        $BandSalaryVsPerformanceDb->select(
            'current_rating_id',
            DB::raw('ROUND(AVG(proposedincentive_percentage),1) AS averageBandSalary')
        )
        ->whereIn('band_id', [1,2])
        ->groupBy('current_rating_id');
        return $BandSalaryVsPerformanceDb->get();
    }
    public static function getAveragePercentSalaryBandVsPerformanceB3(){


        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $BandSalaryVsPerformanceDb = DB::table('vw_employees_export');
        $BandSalaryVsPerformanceDb = Filters::applyFilters($BandSalaryVsPerformanceDb, Auth::user());
        $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
    }

        $BandSalaryVsPerformanceDb->select(
            'current_rating_id',
            DB::raw('ROUND(AVG(proposedsalary_percentage),1) AS averageBandSalary')
        )
        ->whereIn('band_id', [3])
        ->groupBy('current_rating_id');
        return $BandSalaryVsPerformanceDb->get();
    }
    public static function getAverageSalaryBandVsPerformanceB3(){

        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $BandSalaryVsPerformanceDb = DB::table('vw_employees_export');
        $BandSalaryVsPerformanceDb = Filters::applyFilters($BandSalaryVsPerformanceDb, Auth::user());
        $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $BandSalaryVsPerformanceDb = $BandSalaryVsPerformanceDb->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
    }


        $BandSalaryVsPerformanceDb->select(
            'current_rating_id',
            DB::raw('ROUND(AVG(proposedincentive_percentage),1) AS averageBandSalary')
        )
        ->whereIn('band_id', [3])
        ->groupBy('current_rating_id');
        $BandSalaryVsPerformanceDb =  $BandSalaryVsPerformanceDb->get();
        return $BandSalaryVsPerformanceDb;
    }

    public static function SeniorManBonus_review(){



        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $SeniorManAndSP_Db = DB::table('vw_employees_export');
        $SeniorManAndSP_Db = Filters::applyFilters($SeniorManAndSP_Db, Auth::user());
        $SeniorManAndSP_Db = $SeniorManAndSP_Db->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $SeniorManAndSP_Db = $SeniorManAndSP_Db->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
        }
        $SeniorManAndSP_Db= $SeniorManAndSP_Db->whereIn('status_id',  [1,7,19]);


         $SeniorManAndSP_Db->select(
             'instance_indicator_id',
            DB::raw('count(*) as total_head_count'),
            DB::raw('ROUND(SUM( CASE WHEN (scheme_id = 23 OR scheme_id = 20) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN  calc_proposedincentive_zar END),2) AS bonus_sm'),
            DB::raw('
            sum( CASE WHEN (scheme_id = 23 OR scheme_id = 20) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) / 
             sum( CASE WHEN (scheme_id = 23 OR scheme_id = 20) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_actual_pool   END) * 100 as total_actual_pool_percentage_sm')
             /*,

             DB::raw(' ( CASE WHEN (scheme_id = 23)  THEN ((SUM(calc_actual_pool)/SUM(salary_value))*100 )  END)  as total_actual_pool_percentage_sm ')*/

            // DB::raw(' sum( CASE WHEN (scheme_id = 43)  THEN calc_salary_zar *(calc_non_financial / 100) * 0.85    END)  as total_actual_pool_sp ')
        )
        ->whereIn('scheme_id',[20,23])
        ->groupBy('instance_indicator_id');
       $getResults =  $SeniorManAndSP_Db->get();



        return $getResults;

     }
    public static function SPBonus_review(){

        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $SP_Db = DB::table('vw_employees_export');
        $SP_Db = Filters::applyFilters($SP_Db, Auth::user());
        $SP_Db = $SP_Db->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $SP_Db = $SP_Db->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
        }
        $SP_Db= $SP_Db->whereIn('status_id',  [1,7,19]);

        $SP_Db->select(
            DB::raw('scheme_id'),
            DB::raw('count(*) as total_head_count'),
            DB::raw('ROUND(SUM(CASE WHEN scheme_id = 43 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar END),2) AS bonus_sp'),
            DB::raw(' sum( CASE WHEN scheme_id = 43 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) / 
             sum( CASE WHEN (scheme_id = 43) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_actual_pool   END) * 100 as total_actual_pool_percentage_sp ')
            /*,
             DB::raw(' sum( CASE WHEN (scheme_id = 43)  THEN calc_salary_zar * (calc_non_financial / 100) * 0.85    END)  as total_actual_pool_sp ')*/
        )
        ->where('scheme_id',43)
        ->groupBy('scheme_id');
        $SP_Db= $SP_Db->get();


        // logger($SP_Db);
        return $SP_Db;

     }


    public static function generalStaffBandsvsBonus(){

        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $Sbando = DB::table('vw_employees_export');
        $Sbando = Filters::applyFilters($Sbando, Auth::user());
        $Sbando = $Sbando->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $Sbando = $Sbando->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
     $Sbando->select(
            DB::raw('band_id'),
            DB::raw('count(*) as total_head_count'),
             DB::raw('ROUND(SUM(proposedincentive_value),2) AS bonus_value'),
            // DB::raw('(CASE WHEN (band_id IS NOT NULL) THEN (ROUND((((SUM(proposedincentive_value)-SUM(salary_value))/SUM(salary_value))*100),2))  END)  as bonus_percentage_salary ')
            DB::raw('(CASE WHEN (band_id IS NOT NULL) THEN (ROUND((((SUM(CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN proposedincentive_value END))/SUM( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN salary_value END))*100),2))  END)  as bonus_percentage_salary ')

        )
        ->where('scheme_id',13)
        ->groupBy('band_id');
        $Sbando= $Sbando->get();
            return $Sbando;

        // return $Sbando;
     }



    public static function generalStaffBandsvsBonus2($band){

        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $Sbando = DB::table('vw_employees_export');
        $Sbando = Filters::applyFilters($Sbando, Auth::user());
        $Sbando = $Sbando->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $Sbando = $Sbando->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
     $Sbando->select(
            'instance_indicator_id',
            DB::raw('count(*) as total_head_count'),
            DB::raw('ROUND(SUM(CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN proposedincentive_value END),0) AS bonus_value'),
            DB::raw('ROUND((
                    sum( CASE WHEN scheme_id = 13 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar END) / 
                    sum( CASE WHEN (scheme_id = 13) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar END) * 100), 1) as  bonus_percentage'
            )

        )
        ->where('scheme_id',13)
        ->whereIn('band_id',$band)
        ->groupBy('instance_indicator_id');
        $Sbando= $Sbando->get();
            return $Sbando;

        // return $Sbando;
     }

    public static function generalStaffBandsvsBonus2GranTotal(){

        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $Sbando = DB::table('vw_employees_export');
        $Sbando = Filters::applyFilters($Sbando, Auth::user());
        $Sbando = $Sbando->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $Sbando = $Sbando->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
     $Sbando->select(
            'instance_indicator_id',
            DB::raw('count(*) as total_head_count'),
            DB::raw('ROUND(SUM(CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN proposedincentive_value END),0) AS bonus_value'),
            DB::raw('ROUND((
                    sum( CASE WHEN scheme_id = 13 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar END) / 
                    sum( CASE WHEN (scheme_id = 13) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar END) * 100), 1) as  bonus_percentage'
            )

        )
        ->where('scheme_id',13)
        ->groupBy('instance_indicator_id');
        $Sbando= $Sbando->get();
            return $Sbando;

        // return $Sbando;
     }


    public static function getAvgIncreaseBandVsRating($band_id){

        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $manager_pn->personnelnumber;
        $IncreaseBand_Db = DB::table('vw_employees_export');
        $IncreaseBand_Db = Filters::applyFilters($IncreaseBand_Db, Auth::user());
        $IncreaseBand_Db = $IncreaseBand_Db->where('instance_indicator_id','=',$active_instance);


        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $IncreaseBand_Db = $IncreaseBand_Db->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


            });
        }
        $IncreaseBand_Db= $IncreaseBand_Db->whereIn('status_id',  [1,7,19]);
        $IncreaseBand_Db->select(
            'current_rating_id',
            DB::raw('AVG(proposedsalary_percentage) AS avg_percentage_increase')

            )
            ->where('band_id',$band_id)
            ->groupBy('current_rating_id');
        return $IncreaseBand_Db->get();

    }

    public static function calcSMSpecialist(){
        $user            = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $user->personnelnumber;
        $data            = DB::table('cycle2019_records');
        $data            = $data->where('instance_indicator_id', $active_instance);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $data = $data->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
        }

        $data            = $data->select(
                DB::raw('incentive_scheme_id as incentive_scheme_id, COUNT(*) as head_count, 
                sum( CASE WHEN incentive_scheme_id = \'Specialist Scheme\' THEN calc_kpi_incentive_value END) as calc_kpi_incentive_value,
                sum( CASE WHEN incentive_scheme_id = \'Specialist Scheme\' THEN calc_salary_zar *(calc_non_financial / 100) END)  as total_actual_pool_sp,
                
                ROUND((sum( CASE WHEN incentive_scheme_id = \'Specialist Scheme\' THEN calc_kpi_incentive_value END) /
                    sum( CASE WHEN incentive_scheme_id = \'Specialist Scheme\' THEN calc_salary_zar *(calc_non_financial / 100) END)) * 100, 1 ) as percentage_sp,
            
                sum( CASE WHEN incentive_scheme_id = \'Senior Management Scheme\' THEN allocation END) as allocation,
                sum( CASE WHEN incentive_scheme_id = \'Senior Management Scheme\' THEN calc_on_target_achievement END) as calc_on_target_achievement,
                sum( CASE WHEN incentive_scheme_id = \'Senior Management Scheme\' THEN calc_proposedincentive_zar END) as pis_sm_risk,
                ROUND((sum( CASE WHEN incentive_scheme_id = \'Senior Management Scheme\' THEN allocation END) / 
                       sum( CASE WHEN incentive_scheme_id = \'Senior Management Scheme\' THEN calc_on_target_achievement END) * 100), 1) as percentage_sm
            ')
        );
        $data = $data->groupBy('incentive_scheme_id');
        return $data->get();
    }

    public static function calcGeneralStaff(){
        $user            = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $manager_pn      = $user->personnelnumber;
        $data            = DB::table('cycle2019_records');
        $data            = $data->where('instance_indicator_id', $active_instance);
        $data            = $data->where('incentive_scheme_id', '=', 'General Staff Scheme');
        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $data = $data->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);


        });
        }

        $data            = $data->select(
                DB::raw(' instance_indicator_id,
                                COUNT(CASE WHEN band_id = 1 THEN band_id END) as b1_head_count,
                                COUNT(CASE WHEN band_id = 2 THEN band_id END) as b2_head_count,
                                COUNT(CASE WHEN band_id = 3 THEN band_id END) as b3_head_count,
                                COUNT(CASE WHEN band_id = 4 OR band_id = 5 THEN band_id END) as b4b5_head_count,
                                COUNT(band_id) as total_bands,
                                
                                ROUND(SUM(CASE WHEN band_id = 1 THEN calc_proposedincentive_zar END),2) as bonus_value_b1,
                                ROUND(SUM(CASE WHEN band_id = 2 THEN calc_proposedincentive_zar END) ,2)as bonus_value_b2,
                                ROUND(SUM(CASE WHEN band_id = 3 THEN calc_proposedincentive_zar END),2) as bonus_value_b3,
                                ROUND(SUM(CASE WHEN band_id = 4 OR band_id = 5 THEN calc_proposedincentive_zar END),2) as bonus_value_b4b5,
                                SUM(calc_proposedincentive_zar) as total_bonus_value,
                                
                                
                                ROUND(((SUM(CASE WHEN band_id = 1 THEN calc_proposedincentive_zar END) / SUM(CASE WHEN band_id = 1 THEN calc_salary_zar END) ) * 100),2) as percent_b1,
                                ROUND(((SUM(CASE WHEN band_id = 2 THEN calc_proposedincentive_zar END) / SUM(CASE WHEN band_id = 2 THEN calc_salary_zar END) ) * 100),2) as percent_b2,
                                ROUND(((SUM(CASE WHEN band_id = 3 THEN calc_proposedincentive_zar END) / SUM(CASE WHEN band_id = 3 THEN calc_salary_zar END) ) * 100),2) as percent_b3,
                                ROUND(((SUM(CASE WHEN band_id = 4 OR band_id =5 THEN calc_proposedincentive_zar END) / SUM(CASE WHEN band_id = 4 OR band_id =5 THEN calc_salary_zar END) ) * 100),2) as percent_b4b5,
                                ROUND(((SUM(CASE WHEN band_id = 2 OR  band_id = 3 OR band_id = 4 OR band_id = 5 THEN calc_proposedincentive_zar END) / SUM(CASE WHEN band_id = 2 OR  band_id = 3 OR band_id = 4 OR band_id = 5 THEN calc_salary_zar END) ) * 100),2) as percent_total
            ')
        );
        $data = $data->groupBy('instance_indicator_id');
        return $data->get();
    }




}




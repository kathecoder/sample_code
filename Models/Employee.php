<?php

namespace App\Models;

use App\CFLibrary\FilterHandler;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class Employee extends Model
{
    protected $table = 'employees';
    public static $employee_fields = ['personnelnumber', 'instance_indicator_id','title_id', 'firstnames', 'lastname', 'knownas', 'startdate',  'enddate',
        'actuary_indicator','emailaddress', 'gender_id', 'race_id', 'fte', 'date_of_birth', 'secondee', 'comments', 'status_id',
        'band_id','milestone_id', 'jobgrade2', 'jobtitle', 'charterband_id', 'occupational_level_id', 'occupational_category_id', 'remco_category_id',
        'linemanager', 'linemanager_position', 'maternity',
        'linemanager_signitory_manager', 'linemanager_signitory_manager_position'];

    public static function saveEmployees($row){
        try{
            $titles                 = Title::all()->pluck('name', 'id')->toArray();
            $genders                = Gender::all()->pluck('name', 'id')->toArray();
            $bands                  = Band::all()->pluck('name', 'id')->toArray();
            $races                  = Race::all()->pluck('name', 'id')->toArray();
            $milestones             = Milestone::all()->pluck('name', 'id')->toArray();
            $charterbands           = Charterband::all()->pluck('name', 'id')->toArray();
            $occupationalCategories = OccupationalCategory::all()->pluck('name', 'id')->toArray();
            $occupationalLevels     = OccupationalLevel::all()->pluck('name', 'id')->toArray();
            $remcoCategories        = RemcoCategory::all()->pluck('name', 'id')->toArray();
            $statuses               = Status::all()->pluck('name', 'id')->toArray();
            $cycle_id               = 1; //todo: update cycle id

            if (isset($row['personnelnumber'])){
                $employee = null;
                $employee = Employee::where('personnelnumber', $row['personnelnumber'])->where('cycle_id', $cycle_id)->first();
                if ($employee === null){
                    $employee = new Employee();
                    $employee->personnelnumber = $row['personnelnumber'];
                    $employee->cycle_id = $cycle_id;
                }

                if (isset($row['firstnames']))
                    $employee->firstnames =  $row['firstnames'];

                if (isset($row['cost_centre'])){
                    $employee->cost_centre  = $row['cost_centre'];
                }

                if (isset($row['lastname']))
                    $employee->lastname = $row['lastname'];

                if (isset($row['knownas']))
                    $employee->knownas = $row['knownas'];

                if (isset($row['emailaddress']))
                    $employee->emailaddress = $row['emailaddress'];

                if (isset($row['fte']))
                    $employee->fte = $row['fte'];

                if (isset($row['jobgrade2']))
                    $employee->jobgrade2 = $row['jobgrade2'];

                if (isset($row['jobtitle']))
                    $employee->jobtitle = $row['jobtitle'];

                if (isset($row['comments']))
                    $employee->comments = $row['comments'];

                if (isset($row['instance_indicator_id']))
                    $employee->instance_indicator_id = $row['instance_indicator_id'];

                if (isset($row['actuary_indicator'])){
                    $column_value = strtolower( $row['actuary_indicator']);
                    if (in_array($column_value, ['y', 'yes'])){
                        $employee->actuary_indicator = 'Y';
                    }
                    elseif (in_array($column_value, ['n', 'no'])){
                        $employee->actuary_indicator = 'N';
                    }
                    else{
                        $employee->actuary_indicator = null;
                    }
                }

                if (isset($row['secondee'])){
                    $column_value = strtolower( $row['secondee']);
                    if (in_array($column_value, ['y', 'yes'])){
                        $employee->secondee = 'Y';
                    }
                    elseif (in_array($column_value, ['n', 'no'])){
                        $employee->secondee = 'N';
                    }
                    else{
                        $employee->secondee = null;
                    }
                }

                if (isset($row['calc_prorata'])){
                    $column_value = strtolower( $row['calc_prorata']);
                    if (in_array($column_value, ['y', 'yes'])){
                        $employee->calc_prorata = 'Y';
                    }
                    elseif (in_array($column_value, ['n', 'no'])){
                        $employee->calc_prorata = 'N';
                    }
                    else{
                        $employee->calc_prorata = null;
                    }
                }

                if (isset($row['maternity'])){
                    $column_value = strtolower( $row['maternity']);
                    if (in_array($column_value, ['y', 'yes'])){
                        $employee->maternity = 'Y';
                    }
                    elseif (in_array($column_value, ['n', 'no'])){
                        $employee->maternity = 'N';
                    }
                    else{
                        $employee->maternity = null;
                    }
                }

                if (isset($row['title_id'])){
                    $column_value = ucfirst( $row['title_id']);
                    $check = in_array(ucfirst($column_value), $titles);
                    if ($check){
                        $key = array_search($column_value, $titles);
                        $employee->title_id = $key;
                    }
                    else {
                        $employee->title_id = null;
                    }
                }

                if (isset($row['startdate'])){
                    $column_value = $row['startdate'];
                    if ($column_value !== null){
                        $check = AppAssist::validateDate($column_value);
                        if ($check){
                            if ($column_value){
                                $result = AppAssist::transformDateTime($column_value); //dd($result);
                                $employee->startdate = $result;
                            }

                        }
                        else {
                            $employee->startdate = null;
                        }
                    }
                    else {
                        $employee->startdate = null;
                    }

                }

                if (isset($row['date_of_birth'])){
                    $column_value = $row['date_of_birth'];
                    if ($column_value !== null){
                        $check = AppAssist::validateDate($column_value);
                        if ($check){
                            if ($column_value){
                                $result = AppAssist::transformDateTime($column_value); //dd($result);
                                $employee->date_of_birth = $result;
                            }

                        }
                        else {
                            $employee->date_of_birth = null;
                        }
                    }
                    else {
                        $employee->date_of_birth = null;
                    }

                }

                if (isset($row['enddate'])){
                    $column_value = $row['enddate'];
                    if ($column_value !== null){
                        $check = AppAssist::validateDate($column_value);
                        if ($check){
                            if ($column_value){
                                $result = AppAssist::transformDateTime($column_value); //dd($result);
                                $employee->enddate = $result;
                            }

                        }
                        else {
                            $employee->enddate = null;
                        }
                    }
                    else {
                        $employee->enddate = null;
                    }

                }

                if (isset($row['gender_id'])){
                    $column_value = $row['gender_id'];
                    $key = array_search($column_value, $genders);
                    if ($key>0){
                        $employee->gender_id= $key;
                    }
                    elseif(array_key_exists($column_value, $genders)){
                        $employee->gender_id = $column_value;
                    }
                    else {
                        $employee->gender_id = null;
                    }
                }

                if (isset($row['band_id'])){
                    $column_value = $row['band_id'];
                    $key = array_search($column_value, $bands); //dd($key);
                 // if (in_array($column_value, $bands)){
                    if ($key>0){
                        $employee->band_id= $key;
                    }
                    elseif(array_key_exists($column_value, $bands)){
                        $employee->band_id = $column_value;
                    }
                    else {
                        $employee->band_id = null;
                    }
                }

                if (isset($row['milestone_id'])){
                    $column_value = $row['milestone_id'];
                    $key = array_search($column_value, $milestones);
                    if ($key>0){
                        $employee->milestone_id= $key;
                    }
                    elseif(array_key_exists($column_value, $milestones)){
                        $employee->milestone_id = $column_value;
                    }
                    else {
                        $employee->milestone_id = null;
                    }
                }

                if (isset($row['race_id'])){
                    $column_value = ucfirst( $row['race_id']);
                    $check = in_array(ucfirst($column_value), $races);
                    if ($check){
                        $key = array_search($column_value, $races);
                        $employee->race_id = $key;
                    }
                    else {
                        $employee->race_id = null;
                    }
                }

                if (isset($row['status_id'])){
                    $column_value = $row['status_id'];
                    $key = array_search($column_value, $statuses);
                    if ($key>0){
                        $employee->status_id= $key;
                    }
                    elseif(array_key_exists($column_value, $statuses)){
                        $employee->status_id = $column_value;
                    }
                    else {
                        $employee->status_id = null;
                    }
                }

                if (isset($row['charterband_id'])){
                    $column_value = ucfirst( $row['charterband_id']);
                    $check = in_array(ucfirst($column_value), $charterbands);
                    if ($check){
                        $key = array_search($column_value, $charterbands);
                        $employee->charterband_id = $key;
                    }
                    else {
                        $employee->charterband_id = null;
                    }
                }

                if (isset($row['occupational_level_id'])){
                    $column_value = ucfirst( $row['occupational_level_id']);
                    $check = in_array(ucfirst($column_value), $occupationalLevels);
                    if ($check){
                        $key = array_search($column_value, $occupationalLevels);
                        $employee->occupational_level_id = $key;
                    }
                    else {
                        $employee->occupational_level_id = null;
                    }
                }

                if (isset($row['occupational_level_id'])){
                    $column_value = ucfirst( $row['occupational_level_id']);
                    $check = in_array(ucfirst($column_value), $occupationalLevels);
                    if ($check){
                        $key = array_search($column_value, $occupationalLevels);
                        $employee->occupational_level_id = $key;
                    }
                    else {
                        $employee->occupational_level_id = null;
                    }
                }

                if (isset($row['occupational_category_id'])){
                    $column_value = ucfirst( $row['occupational_category_id']);
                    $check = in_array(ucfirst($column_value), $occupationalCategories);
                    if ($check){
                        $key = array_search($column_value, $occupationalCategories);
                        $employee->occupational_category_id = $key;
                    }
                    else {
                        $employee->occupational_category_id = null;
                    }
                }

                if (isset($row['remco_category_id'])){
//                    $column_value = ucfirst( $row['remco_category_id']);
                    //$check = in_array($column_value, $remcoCategories);
                    $column_value =  $row['remco_category_id'];
                    $object = RemcoCategory::where('name', $column_value)->first();
                    if ($object){
                        //$key = array_search($column_value, $remcoCategories);
                        $employee->remco_category_id = $object->id;
                    }
                    else {
                        $employee->remco_category_id = null;
                    }
                }

                if (isset($row['linemanager']))
                    $employee->linemanager = $row['linemanager'];

                if (isset($row['linemanager_position']))
                    $employee->linemanager_position = $row['linemanager_position'];

                if (isset($row['linemanager_signitory_manager']))
                    $employee->linemanager_signitory_manager = $row['linemanager_signitory_manager'];

                if (isset($row['linemanager_signitory_manager_position']))
                    $employee->linemanager_signitory_manager_position = $row['linemanager_signitory_manager_position'];

                Audit::AuditTrailPerModel('employees', $employee, $employee->personnelnumber);
                $employee->save();

                return array('error' => false, 'response' => $employee);
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public static function removeExistingData()
    {
        $cycle_id = Cycle::getLiveCycleID(); //TODO: update cycle id
        $employees = Employee::where('cycle_id', $cycle_id)->get();
        $org_structures = OrgStructure::all();
        foreach ($employees as $employee){
            $emp_assessor = EmployeeAssessor::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
            if ($emp_assessor)
                $emp_assessor->delete();

            $emp_benchmark = EmployeeBenchmarks::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
            if ($emp_benchmark)
                $emp_benchmark->delete();

            $emp_performance = EmployeePerformance::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
            if ($emp_performance)
                $emp_performance->delete();

            $emp_incentives = EmployeeIncentive::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
            if ($emp_incentives)
                $emp_incentives->delete();

            $emp_salary = EmployeeSalary::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
            if ($emp_salary)
                $emp_salary->delete();

            $emp_user = User::where('personnelnumber', $employee->personnelnumber)->first();
            if ($emp_user)
                $emp_user->delete();

            $employee->delete();

        }
        foreach ($org_structures as $org_structure){
            $employee = Employee::where('org_structure_id',$org_structure->id)->first();
            if ($employee === null)
                $org_structure->delete();
        }
        $manager_type_users = ManagerTypeUser::all();
        foreach ($manager_type_users as $manager_type_user){
            $manager_type_user->delete();
        }
    }

    public static $admin_headers_sv = array(
        'personnelnumber',
        'instance_indicator_description',
        'lastname',
        'knownas',
        'startdate',
        'jobgrade',
        'milestone',
        'race',
        'gender',
        'calc_prorata',
        'maternity',
        'jobtitle',
        'current_rating',
        'h_1_performance1',
        'empltype',
        'emplcategory',
        'salary_value',
        'salary_currency',
        'incentive_scheme',
        'incentive_cap',
        'participation_category',
        'orglevel5',
        'orglevel6',
        'orglevel7',
        'orglevel8',
        'linemanager',
        'costcentre',
        'allocationmanager1',
        'calc_allocationmanager1name',
        'allocationmanager2',
        'calc_allocationmanager2name',
        'allocationmanager3',
        'calc_allocationmanager3name',
        'businessmanager1',
        'calc_businessmanager1name',
        'businessmanager2',
        'calc_businessmanager2name',
        'clustermanager1',
        'calc_phaseclustermanager1name',
        'hrbp1',
        'calc_hrbp1name',
        'hrbp2',
        'calc_hrbp2name',
        'hrbp3',
        'calc_hrbp3name',
        'hrbp4',
        'calc_hrbp4name',
        'hrbp5',
        'calc_hrbp5name',
        'emailaddress',
        'internal_jobscale_code',
        'calc_internal_jobscale_name',
        'calc_internal_jobscale_indicator'
    );
    public static $group_headers_sv = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'jobgrade',
        'milestone',
        'calc_prorata',
        'maternity',
        'jobtitle',
        'incentive_scheme',
        'incentive_cap',
        'participation_category',
        'current_rating',
        'h_1_performance1',
        'orglevel5',
        'orglevel6',
        'orglevel7',
        'orglevel8',
        'linemanager',
        'allocationmanager1',
        'calc_allocationmanager1name',
        'allocationmanager2',
        'calc_allocationmanager2name',
        'allocationmanager3',
        'calc_allocationmanager3name',
        'businessmanager1',
        'calc_businessmanager1name',
        'businessmanager2',
        'calc_businessmanager2name',
        'clustermanager1',
        'calc_phaseclustermanager1name',
        'hrbp1',
        'calc_hrbp1name',
        'hrbp2',
        'calc_hrbp2name',
        'hrbp3',
        'calc_hrbp3name',
        'hrbp4',
        'calc_hrbp4name',
        'hrbp5',
        'calc_hrbp5name',
        'emailaddress',
    );
    public static $stanlib_headers_sv = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'startdate',
        'jobgrade',
        'milestone',
        'calc_prorata',
        'maternity',
        'jobtitle',
        'orglevel3',
        'orglevel4',
        'orglevel5',
        'orglevel6',
        'orglevel7',
        'orglevel8',
        'linemanager',
        'current_rating',
        'h_1_performance1',
        'incentive_scheme',
        'incentive_cap',
        'internal_jobscale_code',
        'calc_internal_jobscale_name',
        'calc_internal_jobscale_indicator',
        'allocationmanager1',
        'calc_allocationmanager1name',
        'allocationmanager2',
        'calc_allocationmanager2name',
        'allocationmanager3',
        'calc_allocationmanager3name',
        'businessmanager1',
        'calc_businessmanager1name',
        'businessmanager2',
        'calc_businessmanager2name',
        'businessmanager3',
        'calc_businessmanager3name',
        'clustermanager1',
        'calc_phaseclustermanager1name',
        'clustermanager2',
        'calc_phaseclustermanager2name',
        'hrbp1',
        'calc_hrbp1name',
        'hrbp2',
        'calc_hrbp2name',
        'hrbp3',
        'calc_hrbp3name',
        'hrbp4',
        'calc_hrbp4name',
        'hrbp5',
        'calc_hrbp5name',
        'emailaddress'
    );
    public static $africa_headers_sv = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'startdate',
        'gender',
        'jobtitle',
        'orglevel3',
        'orglevel4',
        'orglevel5',
        'orglevel6',
        'orglevel7',
        'orglevel8',
        'linemanager',
        'current_rating',
        'h_1_performance1',
        'incentive_scheme',
        'calc_allocationmanager1name',
        'calc_allocationmanager2name',
        'calc_businessmanager1name',
        'calc_businessmanager2name',
        'calc_phaseclustermanager1name',
        'hrbp1',
        'calc_hrbp1name',
        'hrbp2',
        'calc_hrbp2name',
        'hrbp3',
        'calc_hrbp3name',
        'hrbp4',
        'calc_hrbp4name',
        'hrbp5',
        'calc_hrbp5name',
        'emailaddress'
    );

    public static function dynamicRecordUpdate(&$object, $table, $fieldsValueArray, $exclude_field){
        $columns    = DB::select("describe $table");
        foreach ($columns as $column) {
            if (!in_array($column, $exclude_field)){ //
                $value_before = '';
                $value_after = '';
                if (isset($fieldsValueArray[$column->Field])){
                    $value_before                = $object->{$column->Field};
                    $value_after                 = $object[$column->Field];
                    $object->{$column->Field}  = $object[$column->Field];
                }
                if($value_before !== $value_after){
                    Audit::captureAudit(
                        $object->personnelnumber,
                        $column->Field,
                        Auth::user()->personnelnumber,
                        $value_before,
                        $value_after
                    );
                    logger("VALUE BEFORE: $value_before");
                    logger("VALUE  AFTER: $value_after");
                    logger("FIELD  NAME: $column->Field");
                    logger("#############################");
                }

            }
        }
        $object->save();
    }




    public static function getRecords($searchInput,$skip,$take, $is_paginate, $status =null, $clause = ""){
        $manager_pn      = Auth::user()->personnelnumber;
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $record_ids      = FilterHandler::getFilteredData('vw_employees_combine', Auth::user()->id);

        $records = DB::table('vw_employees_export');
        if (count($record_ids) > 0){
            $records = $records->whereIn('id',  $record_ids);
        }

        if ($active_instance == 1){
            $headers = Employee::$group_headers_sv;
            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ($active_instance == 2){
            $headers = Employee::$africa_headers_sv;
            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ($active_instance == 3){
            $headers = Employee::$stanlib_headers_sv;
            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn);

            });
        }

        $headers[]='id';
        $headers[]='calc_phaseclustermanager2name';

        if (!in_array('calc_businessmanager2name', $headers))
            $headers[]='calc_businessmanager2name';

        if (!in_array('calc_prorata', $headers))
            $headers[]='calc_prorata';

        $records = $records

            ->where(function($query) use ($searchInput){
                $query
                    ->where('personnelnumber', 'like', '%'.$searchInput.'%')
                    ->orWhere('jobtitle', 'like', '%'.$searchInput.'%')
                    ->orWhere('lastname', 'like', '%'.$searchInput.'%')
                    ->orWhere('knownas', 'like', '%'.$searchInput.'%')
                    ->orWhere('firstnames', 'like', '%'.$searchInput.'%');
            })
            ->select($headers);

        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }

        return $records->get();

    }

    public static function getEmployeeRecords($searchInput,$skip,$take, $is_paginate, $status = null,$sort_by = null,$is_ascending = null, $clause = ""){
        $user            = Auth::user();
        $manager_pn      = $user->personnelnumber;
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        //$record_ids      = FilterHandler::getFilteredData('vw_employees_combine', Auth::user()->id);

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, $user);

        if (is_array($status)){
            $records = $records->whereIn('status_id',  $status);
        }
        else{
            $records = $records->where('status_id',  $status);
        }

        if ($active_instance == 1){
            $headers = Employee::$group_headers_sv;
//            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ($active_instance == 2){
            $headers = Employee::$africa_headers_sv;
//            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ($active_instance == 3){
            $headers = Employee::$stanlib_headers_sv;
//            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){

            $records = $records->where('instance_indicator_id',  $active_instance);

            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                    ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);
            });
        }

        $headers[]='id';
        $headers[]='status_id';
        $headers[]='calc_phaseclustermanager2name';

        if (!in_array('calc_businessmanager2name', $headers))
            $headers[]='calc_businessmanager2name';

        if (!in_array('calc_prorata', $headers))
            $headers[]='calc_prorata';

        $records = $records

            ->where(function($query) use ($searchInput){
                $query
                    ->where('personnelnumber', 'like', '%'.$searchInput.'%')
                    ->orWhere('jobtitle', 'like', '%'.$searchInput.'%')
                    ->orWhere('lastname', 'like', '%'.$searchInput.'%')
                    ->orWhere('knownas', 'like', '%'.$searchInput.'%')
                    ->orWhere('firstnames', 'like', '%'.$searchInput.'%');

            })
            ->select($headers);
        if ($sort_by !== '' || $sort_by = null){
            $is_ascending = $is_ascending === true ? 'ASC' : 'DESC';
            $records =  $records->orderBy($sort_by, $is_ascending);
        }

        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }

        return $records->get();

    }








    public function refreshCalculations(){
        // salary
        $employee_salary    = EmployeeSalary::where('personnelnumber', $this->personnelnumber)->first();
        $employee_incentive = EmployeeIncentive::where('personnelnumber', $this->personnelnumber)->first();
        $employee_benchmark = EmployeeBenchmarks::where('personnelnumber', $this->personnelnumber)->first();
        $org_structure      = OrgStructure::find($this->org_structure_id);

        $currency           = Currency::find($employee_salary->salary_currency_id);

        $calc_non_financial = 0;
        $participation_category                     = ParticipationCategory::find($employee_incentive->participation_category_id);
        if ($participation_category){
            $calc_non_financial                         = $participation_category->split_non_financial;
            $employee_incentive->calc_non_financial     = $calc_non_financial;
            $employee_incentive->calc_financial         = $participation_category->split_financial;
        }

        $proposedincentive_value                    = round($employee_incentive->proposedincentive_percentage / 100 * round($employee_salary->salary_value,0), 0) ;

        $proposedincentive_value                    = $employee_incentive->proposedincentive_value; //= $proposedincentive_value;
        //$employee_incentive->proposedincentive_value= $proposedincentive_value;
        $deferral_top_up                            = ($employee_incentive->deferral_top_up != null ? $employee_incentive->deferral_top_up : 0);
        $ref_contribution                           = ($employee_incentive->ref_contribution != null ? $employee_incentive->ref_contribution : 0);
        $incentive_top_up                           = ($employee_incentive->incentive_top_up != null ? $employee_incentive->incentive_top_up : 0);
        $stanlib_suggest_incentive_units_value      = ($employee_incentive->stanlib_suggest_incentive_units_value != null ? $employee_incentive->stanlib_suggest_incentive_units_value : 0);
        $proposed_deferred_value                    = ($employee_incentive->proposed_deferred_value != null ? $employee_incentive->proposed_deferred_value : 0);
        $employee_incentive->calc_cash              = round($proposedincentive_value + $incentive_top_up - $proposed_deferred_value - $stanlib_suggest_incentive_units_value - $ref_contribution, 0);
        $h_1_incentive_value                        = $employee_incentive->h_1_incentive_value;

        $employee_incentive->calc_incentivechange   = round($proposedincentive_value - $h_1_incentive_value, 0);

        $employee_incentive->calc_proposed_cash_value         = round($proposedincentive_value + $incentive_top_up - $proposed_deferred_value - $stanlib_suggest_incentive_units_value - $ref_contribution, 0);
        $employee_incentive->calc_proposed_cash_value_zar     = round(($proposedincentive_value + $incentive_top_up - $proposed_deferred_value - $stanlib_suggest_incentive_units_value - $ref_contribution)  / $currency->conversion_rate, 0);
        $employee_incentive->calc_proposed_deferred_value_zar = round($proposed_deferred_value / $currency->conversion_rate, 0);
        $employee_incentive->incentive_top_up_zar             = round($incentive_top_up / $currency->conversion_rate, 0);
        $employee_incentive->performance_reward_plan_zar      = $employee_incentive->performance_reward_plan > 0 ? round($employee_incentive->performance_reward_plan / $currency->conversion_rate, 0) : 0;

        $proposed_deferred_shares                   = ($employee_incentive->proposed_deferred_shares != null ? $employee_incentive->proposed_deferred_shares : 0);
        $longterm_incentive_value                   = ($employee_incentive->longterm_incentive_value != null ? $employee_incentive->longterm_incentive_value : 0);
        $stanlib_suggest_incentive_shares_value     = ($employee_incentive->stanlib_suggest_incentive_shares_value != null ? $employee_incentive->stanlib_suggest_incentive_shares_value : 0);
        //$employee_incentive->calc_total_deferral_topup    = round($deferral_top_up + $proposedincentive_value + $incentive_top_up + $stanlib_suggest_incentive_units_value + $stanlib_suggest_incentive_shares_value + $proposed_deferred_shares + $longterm_incentive_value , 2);//round($proposedincentive_value + $deferral_top_up, 2);


        $employee_incentive->calc_total_deferral_topup    = round($proposedincentive_value + $incentive_top_up + $stanlib_suggest_incentive_shares_value + $deferral_top_up, 0);

        $my_calc_proposedincentive_zar                    = $proposedincentive_value / $currency->conversion_rate; //$employee_incentive->calc_proposedincentive_zar;
        $employee_incentive->calc_proposedincentive_zar   = round($my_calc_proposedincentive_zar, 0);

        $salary_value                                       =  round($employee_salary->salary_value, 0);
        $calc_salary_zar                                    = round(($salary_value / $currency->conversion_rate ), 0);
        $proposedsalary_value                               =  ($employee_salary->salary_value * round($employee_salary->proposedsalary_percentage, 1) / 100) + $employee_salary->salary_value;
        $employee_salary->proposedsalary_value              = $proposedsalary_value;
        $my_calc_proposedsalary_zar                         = round(($proposedsalary_value / $currency->conversion_rate ), 0);
        $employee_salary->calc_change_from_current          = round($proposedsalary_value - $salary_value, 0);
        $calc_new_monthly_salary_value                      = $proposedsalary_value  / 12;
        $employee_salary->calc_monthly_salary_value         = round($salary_value / 12, 0);
        $employee_salary->calc_salary_zar                   = $calc_salary_zar;
        $employee_salary->calc_proposedsalary_zar           = $my_calc_proposedsalary_zar;
        $employee_salary->calc_total_rem                    = round($my_calc_proposedsalary_zar + $my_calc_proposedincentive_zar, 0);
        $employee_salary->calc_new_monthly_salary_value     = round($calc_new_monthly_salary_value, 0);
        $my_calc_total_rem                                  = round($my_calc_proposedsalary_zar + $my_calc_proposedincentive_zar, 0);
        $calc_prior_totalrem                                = $employee_salary->calc_prior_totalrem ;
        $employee_salary->calc_totalpercent                 = $my_calc_total_rem!= 0 ? round((100 - ($calc_prior_totalrem / $my_calc_total_rem) * 100), 1) : 0;
        $employee_salary->calc_monthly_salary_ZAR           = round(($proposedsalary_value / $currency->conversion_rate  ) / 12, 0) ;

        //round(CASE employee_salaries.calc_salary_zar *(employee_incentives.calc_non_financial / 85, 2) as calc_actual_pool,
        $employee_incentive->calc_actual_pool   = round($calc_salary_zar * ($calc_non_financial/100), 0);

        //Audit::AuditTrailPerModel('employee_incentives', $employee_incentive, $this->personnelnumber);
        $employee_incentive->save();

        //Audit::AuditTrailPerModel('employee_salaries', $employee_incentive, $this->personnelnumber);
        $employee_salary->save();



        if (in_array($employee_incentive->incentive_scheme_id, [20, 23, 43])){

            $participation_category_id  = $employee_incentive->participation_category_id;
            $participation_category     = ParticipationCategory::find($participation_category_id);
            $assessment_score           = $employee_incentive->assessment_score > 0 ? $employee_incentive->assessment_score : 0;

            if ($participation_category){

                $calc_non_financial =  $participation_category->split_non_financial;
                $calc_kpi_incentive_percentage                      = $salary_value >  0 ? ((($salary_value * $assessment_score / 100 * ($calc_non_financial / 100))/$salary_value) * 100) : 0;
                //$employee_incentive->assessment_score               = round($assessment_score, 0);
                $employee_incentive->calc_kpi_incentive_percentage  = round($calc_kpi_incentive_percentage, 1);
                $calc_kpi_incentive_value                           = $salary_value * $assessment_score / 100 * ($calc_non_financial / 100);
                $employee_incentive->calc_kpi_incentive_value       = round($calc_kpi_incentive_value, 0);
                $financial_incentive_value                          = round($employee_incentive->financial_incentive_value, 0);

                $proposedincentive_value                            = $calc_kpi_incentive_value + ($financial_incentive_value === null ? 0 : $financial_incentive_value);

                $proposedincentive_percentage                       = $salary_value > 0 ? $proposedincentive_value / $salary_value * 100 : 0;
                $employee_incentive->proposedincentive_value        = round($proposedincentive_value, 1);
                $employee_incentive->proposedincentive_percentage   = round($proposedincentive_percentage, 1);

                //do ZAR calculations
                $calc_proposedincentive_zar                         = round($proposedincentive_value / $currency->conversion_rate, 0);
                $employee_incentive->calc_proposedincentive_zar     = $calc_proposedincentive_zar;

                $employee_incentive->save();
            }

        }

        $benchmark          = Benchmark::find($employee_benchmark->benchmark_id);
        if ($benchmark){

            $employee_benchmark->calc_intbench_salary_median    = $benchmark->p2;
            $employee_benchmark->calc_intbench_bonus_median     = $benchmark->mm;

            $employee_benchmark->calc_compa_ratio_salary_pre    = round($benchmark->p2 > 0 ? ($calc_salary_zar / $benchmark->p2) * 100 : 0, 0);
            $employee_benchmark->calc_compa_ratio_salary_post   = round($benchmark->p2 != 0 ? ($employee_salary->calc_proposedsalary_zar / $benchmark->p2)  * 100 : 0, 0);
            $employee_benchmark->calc_compa_ratio_incentive     = round($benchmark->mm > 0 ? ($my_calc_proposedincentive_zar / $benchmark->mm) * 100 : 0, 0);

            $employee_benchmark->save();
        }
    }
}

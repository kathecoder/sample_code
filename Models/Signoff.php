<?php

namespace App\Models;

use App\CFLibrary\FilterHandler;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Models\CyclePhases;
use App\Models\Phase;
class Signoff extends Model
{
    protected $table = 'vw_signoff';



    public static function getRecords($searchInput,$skip,$take,$is_paginate,$ascending=false,$startDate,$endDate){
        
        $manager_pn      = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $record_ids      = FilterHandler::getFilteredData('vw_signoff', Auth::user()->id);
        $phase_id         = Phase::getLivePhaseID();

        if($user_roles){

      
        $records = DB::table('vw_signoff')
      
        // ->where('vw_signoff.user_id','=',Auth::user()->id)
        ->where(function($query) use ($searchInput){
            $query
                ->where('vw_signoff.user_name', 'like', '%'.$searchInput.'%');
                // ->where('vw_signoff.comment', 'like', '%'.$searchInput.'%');
         });

         $records->where('vw_signoff.phase_id','=',$phase_id);
         if(Auth::user()->admin==0){
            $records->where('vw_signoff.user_id','=', Auth::user()->id);
        }


         $records->select(                
            DB::raw("
                    vw_signoff.id as id, 
                    vw_signoff.comment as comment, 
                    vw_signoff.accept as status, 
                    vw_signoff.user_name as user_name, 
                    vw_signoff.business_area as business_area, 
                    vw_signoff.business_unit as business_unit, 
                    vw_signoff.signoff_date as created_at"
            )
        );

        if ($startDate !== null && $endDate !== null){ 
        //    $records = $records->whereBetween('vw_signoff.signoff_date', [ $startDate,$endDate ]);/** not inclusive of selected dates*/
         
            $records = $records->whereDate('vw_signoff.signoff_date', '>=', $startDate);
            $records = $records->whereDate('vw_signoff.signoff_date', '<=', $endDate);
       

         }
         $records = $records->orderByDesc('vw_signoff.signoff_date');

        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }
        return $records->get();

    }else
    {
        return [];

    }
}


}

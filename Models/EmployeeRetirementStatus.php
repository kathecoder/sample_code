<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeRetirementStatus extends Model
{
    protected $table = 'vw_employees_retirement';
}

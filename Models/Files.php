<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use function Matrix\trace;

class Files extends Model
{
    protected $table = 'files';

    public static function createOrUpdateFile($id, $reference_table_id, $reference_table_name, $file_path){

        if($file_path!==false){
            if($id>0){
                $files = Files::find($id);
            }else{
                $files = new Files();
            }

            $files -> reference_table_id = $reference_table_id;
            $files -> reference_table_name = $reference_table_name;
            $files -> file_path = $file_path;
            $files ->save();

            Log::notice('New File: '.$file_path.', File Id: '.$files->id);

            if($files->id===true){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }
    public static function getFile($reference_table_id, $reference_table_name){
        $res = Files::where('reference_table_id', $reference_table_id)->where('reference_table_name',$reference_table_name)->get();

        if(count($res)){
            return $res[0]->file_path;
        }
        return false;
    }
    public static function handleFileUpload($path, $file){

        if($file!==null){
            //Log::notice('File Base64: '.$file);
            $extension = explode('/', mime_content_type($file))[1];
            $file_extension = '';
            if($extension==='vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
                $file_extension = '.xlsx';
            }else if($extension==='jpeg') {
                $file_extension = '.jpeg';
            }
            else if($extension==='msword')
             {
                $file_extension = '.doc';
            }
            else if($extension==='msoutlook')
             {
                $file_extension = '.ost';
            }
            else if($extension==='pdf')
             {
                $file_extension = '.pdf';
            }
            else if($extension==='PDF')
             {
                $file_extension = '.PDF';
            }
            else if($extension==='png') {
                $file_extension = '.png';
            }
            else if($extension==='PNG') {
                $file_extension = '.PNG';
            }
            else if($extension==='ms-powerpoint') {
                $file_extension = '.pptx';
            }
            else if($extension==='vnd.openxmlformats-officedocument.presentationml.presentation') {
                $file_extension = '.pptx';
            } 
            else if($extension==='vnd.ms-word.template.macroenabled.12') {
                $file_extension = '.docx';
            }
            else if($extension==='vnd.openxmlformats-officedocument.wordprocessingml.document') {
                $file_extension = '.docx';
            }
            else if($extension==='csv') {
                $file_extension = '.csv';
            }
            
            else if($extension==='plain') {
                $file_extension = '.csv';
            }
            
            else if($extension==='vnd.ms-outlook') {
                $file_extension = '.msg';
            }
            else{
                $file_extension = $extension;
            }

            $filename = $path. date('YmdHis')."_". rand(102388585858580,100) . "".$file_extension;
            $file_upload = explode(";base64,", $file);
            $file_upload_base64 = base64_decode($file_upload[1]);
            $destinationPath = 'public'.$filename;
            Storage::put($destinationPath, $file_upload_base64);


            return $filename;
        }else{
            return false;
        }



    }

    /**
     * @param $id
     * @param $reference_table_id
     * @param $reference_table_name
     * @param $file_path
     * @return array|bool
     */
    public static function storeSchemeContract($id, $reference_table_id, $reference_table_name, $file_path){
        try{
            if($file_path!==false){
                $files_array = [];
                $old_file = Files::where('reference_table_id', $reference_table_id)->where('reference_table_name', $reference_table_name)->where('status_id', 6)->first();
                if ($old_file){
                    $old_file->status_id = 7;
                    $old_file->save();
                    $files_array['old_id'] = $old_file->id;
                }

                $files = new Files();
                $files -> reference_table_id = $reference_table_id;
                $files -> reference_table_name = $reference_table_name;
                $files -> file_path = $file_path;
                $files -> status_id = 6;
                $files ->save();
                $files_array['new_id'] = $files->id;
                Log::notice('New File: '.$file_path.', File Id: '.$files->id);

                return $files_array;
                if($files->id===true){
                    return $files_array;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }



    public static function downloadFileByStoragePath($path){
        try {
                logger($path);
            return response()->file(storage_path('app/'. $path), [
                'Content-Type' => 'application/octet-stream',
                'Content-Disposition' => 'inline; filename=file.pdf',
                'Content-Length' => filesize(storage_path('app/'. $path)),
                'Content-Transfer-Encoding' => 'binary',
                'Accept-Ranges' => 'bytes'
            ]);
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public static function renameContract($user_cycle){

        $file = Files::where('reference_table_id', $user_cycle->id)->where('reference_table_name', 'user_cycles')->where('status_id', 6)->first();

        if ($file !== null){

            $path = $file->file_path;
            $actual_name = pathinfo($path,PATHINFO_FILENAME);
            $extension = pathinfo($path, PATHINFO_EXTENSION);
            $timestamp = "_" . time();
            $new_path = "public/pdfs/old/$actual_name$timestamp.$extension";
            rename(storage_path('app/'. $file->file_path), storage_path("app/public/pdfs/old/$actual_name$timestamp.$extension") );
            $file->file_path = $new_path;
            $file->save();

        }
    }

    public static function getDownloadLink($use_cycle_id, $status_id){
        try{
            $file = Files::orderBy('created_at', 'desc')->where('reference_table_name', 'user_cycles')->where('reference_table_id', $use_cycle_id)->where('status_id', $status_id)->first();
            $download_link =  URL::to('/') .'/'. $file->file_path;
            return  str_replace('public', 'storage', $download_link);
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }
}

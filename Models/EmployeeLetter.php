<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeLetter extends Model
{
    protected $table = "employee_letter";




    public static function script(){
        # 1. Iterate the folder get the letter/file
        # 2. convert the file
        # 3. store file in a different directory

        //$file = base_path(). '/database/data/permissions.csv';
        $path = base_path(). '/storage/app/Letters/FINANCE';
        $path2 = base_path(). '/storage/app/Letters/FINANCE_C/';
        $rdi = new \RecursiveDirectoryIterator($path);
        $rii = new \RecursiveIteratorIterator($rdi);
        $count = 0;
        foreach ($rii as $file) {

            if ($file->isDir()) {
                continue;
            }
            if (pathinfo($file)['extension'] == 'DS_Store') {
                continue;
            }
            $count = 1 + $count;
            logger($file);
            ## create temp folder needed by apache user in order to be able to run LibreOffice from cmd
            ## the temp folder must a directory that apache has write permissions on
            $temp_folder = public_path() . '/temp';
            if (!file_exists($temp_folder)){
                \File::makeDirectory($temp_folder, $mode = 0777, true, true);
            }
            $file_to_convert        = str_replace(' ', '\ ', $file); ## escape spaces
            $path                   = str_replace(' ', '\ ', $path); ## escape spaces
            $outputfile = $path2 . pathinfo($file)['filename'];
            $outputfile                   = str_replace(' ', '\ ', $outputfile); ## escape spaces
            //logger("output " . $outputfile);

            //$result = shell_exec("export HOME=$temp_folder && libreoffice --headless --convert-to pdf $file_to_convert --outdir  $path");
            //$result = shell_exec("export HOME=$temp_folder && /Applications/LibreOffice.app/Contents/MacOS/soffice --headless  --convert-to pdf $file_to_convert --outdir  $path");
            //$result = shell_exec("unoconv -f pdf -o $outputfile  -e RestrictPermissions=true -e PermissionPassword=test -e Printing=0 -e Changes=0 -e EnableCopyingOfContent=false $file_to_convert");
            //sleep(3);
        }
        //logger("COUNT: " . $count);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessorEmployee extends Model
{
    protected $table = 'vw_employees_assessor';
}

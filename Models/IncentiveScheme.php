<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncentiveScheme extends Model
{
    protected $table = 'incentive_schemes';

    public static function getAllRecords($searchInput,$skip,$take, $is_paginate, $filterFilters=false)
    {
        $ids = Filters::getFilterFilters('scheme_id');

        $res    =   IncentiveScheme::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', $ids);}

        $res->orderBy('name');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }
        return $res->get();
    }
}

<?php

namespace App\Models;

use App\Http\Controllers\Admin\ImportController;
use App\Http\Controllers\PermissionController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use phpDocumentor\Reflection\Types\Self_;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class DataField extends Model
{
    protected $table = 'data_fields';

    public static function columnDefinitionBuilder($type = ''){
        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $phase_id           = Phase::getLivePhaseID();
        $user               = Auth::user();
        $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 : 4;
        $isHCBP             = ManagerTypeUser::isHCBP($user->personnelnumber, $active_instance_id );

        if ($active_instance_id === 3 && $isHCBP === false){ //stanlib manager permissions
            $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 :7; // any manager type other than HCBP
            //logger(" Not HCBP: " . $user->personnelnumber );
        }

        $column_definitions = array();
        $tableOptions       = array(
            "table_title"               => "",
            "className"                 => "table table-striped table-responsive table-bordered  tb-sv",
            "tableContainerClassName"   => "auto-scrollX",
            "hasOnBlur"                 => true
        );
        $column_definitions["tableOptions"] = $tableOptions;

        $records = DB::table('permissions')
            ->leftJoin('data_fields', 'data_fields.id', 'permissions.data_field_id')
            ->leftJoin('data_field_labels', 'data_field_labels.data_field_id', 'data_fields.id');

        $records =   $records
            ->select(
                DB::raw("data_fields.name, data_fields.label, data_fields.control_type, data_fields.control_type"),
                DB::raw("data_field_labels.label as instance_label"),
                DB::raw("permissions.access, permissions.freeze_col ")
            );

        $records = $records->where('data_field_labels.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.manager_type_id', $manager_type);
        $records = $records->where('permissions.cycle_id', 1);
        $records = $records->where('permissions.phase_id', $phase_id);
        $records = $records->whereIn('permissions.access', ['R', 'W']);

        $records = self::filterAllocationTableColumns($records, $type);

        $records = $records->get();

        foreach ($records as $record){
            $col = [];

            $col["column_properties"] = array(
                "name"      => $record->name,
                "title"     => $record->name === 'id' ? 'Action' : $record->instance_label,
                "width"     => "",
                "allowSort" => true,
                "freeze"    => $record->freeze_col === 1? true : false
            );

            //------------- ID
            if ($record->name == 'id'){
                //column text
                $col['button'][] = array(
                    "actionType"    => "edit_pa",
                    "show"          => true,
                    "passValue"     => "id",
                    "icon_class"    =>  "fas fa-eye",
                    "className"     => "btn-wide btn-shadow btn btn-outline-primary btn-sm"
                );
            }
            //------------- Text + Write
            else if ($record->control_type == 'text' && $record->access == 'W'){
                //column text
                $col['input'][] = array(
                    "name"          => $record->name,
                    "show"          => true,
                    "className"     => $record->name,
                    "input_type"    => "text"
                );
            }
            //------------- Number + Write
            else if ($record->control_type == 'number' && $record->access == 'W'){
                //column text

                if ($record->name == 'assessment_score'){
                    $col['input'][] = array(
                        "name"          => $record->name,
                        "show"          => true,
                        "className"     => "",
                        "input_type"    => "text",
                        "extra"    => array(
                            "depend_from_this_field" => 'special_scheme',
                            "conditional"            => 'boolean',
                        )
                    );
                    $col['fa_icon'][] = array(
                        "show"          => true,
                        "className"     => "fa fa-info-circle pull-right assessment_score_info",
                        "actionType"     => "assessment_score_info",
                        "passValue"     => "id",
                        "extra"         => array(
                            "depend_from_this_field" => 'special_scheme',
                            "conditional"            => 'boolean',
                        )
                    );
                }
                elseif ($record->name == 'proposedincentive_value'){
                    $col['input'][] = array(
                        "name"          => $record->name,
                        "show"          => true,
                        "className"     => "pull-right",
                        "input_type"    => "text",
                        "extra"    => array(
                            "depend_from_this_field" => 'not_special_scheme',
                            "conditional"            => 'boolean',
                        )
                    );
                }
                elseif ($record->name == 'proposedincentive_percentage'){
                    $col['input'][] = array(
                        "name"          => $record->name,
                        "show"          => true,
                        "className"     => "pull-right",
                        "input_type"    => "text",
                        "extra"    => array(
                            "depend_from_this_field" => 'not_special_scheme',
                            "conditional"            => 'boolean',
                        )
                    );
                }
                else{
                    $col['input'][] = array(
                        "name"          => $record->name,
                        "show"          => true,
                        "className"     => "pull-right",
                        "input_type"     => "text"
                    );
                }

            }
            //------------- Boolean + Write
            else if ($record->control_type == 'boolean' && $record->access == 'W'){
                //column text
                $col['input'][] = array(
                    "name"          => $record->name,
                    "show"          => true,
                    "className"     => "",
                    "input_type"     => "checkbox"
                );
            }
            //------------- calculation + Read
            else if ((  $record->control_type == 'calculation' || $record->control_type == 'formula' ||
                        $record->control_type == 'currency' || $record->control_type == 'number' ||
                        $record->control_type == 'currency') &&
                        $record->access == 'R' && $record->name != 'personnelnumber'){
                //column text
                $col['text'][] = array(
                    "name"          => $record->name,
                    "show"          => true,
                    "className"     => "cf_calculation pull-right",
                );
            }
            else{
                //column text
                $col['text'][] = array(
                    "name"          => $record->name,
                    "show"          => true,
                    "className"     => ""
                );
            }

            $column_definitions['colums'][] = $col;
        }

        return $column_definitions;

    }

    public static function getFields($type = ''){
        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $phase_id           = Phase::getLivePhaseID();
        $user               = Auth::user();
        $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 : 4;

        if ($active_instance_id === 3 && ManagerTypeUser::isHCBP($user->personnelnumber, $active_instance_id ) == false){
            $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 :7;
        }

        $records = DB::table('permissions')
            ->leftJoin('data_fields', 'data_fields.id', 'permissions.data_field_id')
            ->leftJoin('data_field_labels', 'data_field_labels.data_field_id', 'data_fields.id');

        $records =   $records
            ->select(
                DB::raw("data_fields.name, data_fields.label, data_fields.control_type, data_fields.control_type"),
                DB::raw("data_field_labels.label as instance_label"),
                DB::raw("permissions.access, permissions.freeze_col ")
            );

        $records = $records->where('data_field_labels.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.manager_type_id', $manager_type);
        $records = $records->where('permissions.cycle_id', 1);
        $records = $records->where('permissions.phase_id', $phase_id);
        $records = $records->whereIn('permissions.access', ['R', 'W']);

        return $records->pluck('data_fields.name')->toArray();
    }

    /**Get Export Headers
     * @return array
     */
    public static function getExportFields(){
        ini_set('memory_limit',-1);
        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $phase_id           = Phase::getLivePhaseID();
        $user               = Auth::user();
        $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 : 4;

        if ($active_instance_id === 3 && ManagerTypeUser::isHCBP($user->personnelnumber, $active_instance_id ) == false){
            $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 :7;
        }

        $records = DB::table('permissions')
            ->leftJoin('data_fields', 'data_fields.id', 'permissions.data_field_id')
            ->leftJoin('data_field_labels', 'data_field_labels.data_field_id', 'data_fields.id');

        $records =   $records
            ->select(
                DB::raw("data_fields.name, data_fields.label, data_fields.control_type, data_fields.control_type"),
                DB::raw("data_field_labels.label as instance_label"),
                DB::raw("permissions.access, permissions.freeze_col, permissions.exportable ")
            );

        $records = $records->where('data_field_labels.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.manager_type_id', $manager_type);
        $records = $records->where('permissions.cycle_id', 1);
        $records = $records->where('permissions.phase_id', $phase_id);
        $records = $records->where('permissions.exportable', 1);

        return $records->pluck('data_fields.name')->toArray();
    }

    public static function filterAllocationTableColumns($records, $type){

        if ($type == 'salary'){
            return $records->where('permissions.on_salary_table', 'LIKE', '1');
        }
        else if ($type == 'general'){
            return $records->where('permissions.on_general_staff_incentive_table', 'LIKE', '1');
        }
        else if ($type == 'smi_risk'){
            return $records->where('permissions.on_sm_and_risk_incentive_table', 'LIKE', '1');
        }
        else if ($type == 'specialist'){
            return $records->where('permissions.on_specialist_incentive_table', 'LIKE', '1');
        }
        else if ($type == 'other'){
            return $records->where('permissions.more_info_screen_access', 'LIKE', '1');
        }
        else{
            return $records->where('permissions.more_info_screen_access', 'LIKE', '1');
        }
    }

    public static function filterFilteredColumns($records, $type){
        if ($type == 'general'){
            $records = $records->where('scheme_id', 13);
        }
        else if ($type == 'smi_risk'){
            $records = $records->whereIn('scheme_id', [20, 23]);
        }
        else if ($type == 'specialist'){
            $records = $records->where('scheme_id', 43);
        }
        else if ($type == 'other'){
            $records = $records->whereNotIn('scheme_id', [13, 43, 20, 23]);
        }
        else{
            //$records = $records->where('permissions.more_info_screen_access', 'LIKE', '1');
        }
        return $records;
    }


    public static function getFormFields($request = null){
        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $phase_id           = Phase::getLivePhaseID();
        $user               = Auth::user();
        $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 : 4;

        if ($active_instance_id === 3 && ManagerTypeUser::isHCBP($user->personnelnumber, $active_instance_id ) === false){
            $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 :7;
        }

        $category = $request['category'];
        $records = DB::table('permissions')
            ->leftJoin('data_fields', 'data_fields.id', 'permissions.data_field_id')
            ->leftJoin('data_field_labels', 'data_field_labels.data_field_id', 'data_fields.id');

        // exclude proposedincentive_value/percentage fields if employee has these schemes
        if ($manager_type === 4 && isset( $request['incentive_scheme']) && in_array( $request['incentive_scheme'], ['Specialist Scheme', 'Senior Management Scheme', 'Risk Senior Management Scheme'])){
            $records =   $records->whereNotIn('data_field_labels.data_field_id', [205, 206]);
        }
        $records =   $records
            ->select(
                DB::raw("data_fields.name, data_fields.label, data_fields.control_type, data_fields.control_type, data_fields.data_category_id"),
                DB::raw("data_field_labels.label as instance_label"),
                DB::raw("permissions.access")
            );

        $records = $records->where('data_field_labels.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.manager_type_id', $manager_type);
        $records = $records->where('permissions.cycle_id', 1);
        $records = $records->whereIn('permissions.access', ['R', 'W']);
        $records = $records->where('permissions.phase_id', $phase_id);
        if ($category !== null){
            $records = $records->where('data_fields.data_category_id', $category);
        }

        $records =  $records->get();
        return $records;
    }





































    // used for import
    // if any headings change, it will only need to be changed here, as import uses these to get values
    public static $employee_table_fields = [
        //the values (column names) can change, but the order (index) within this array must stay the same
        //the field value will be fetched by the array value here, so the order in the file doesn't matter
        0 => 'personnelnumber',
        1 => 'instance_indicator_id',
//        1 => 'lastname',
        2 => 'firstnames',
        3 => 'gender',
        4 => 'race',
        //for employee_cycle
        5 => 'emailaddress',
        6 => 'salary_value',
        7 => 'startdate',
        8 => 'jobgrade', //band
        9 => 'milestone',
        10 => 'incentive_scheme',
        11 => 'salary_currency',
        12 => 'record_status'
    ];

    // these fields are not also stored in the employee_fields table, directly in employee_cycle
    public static $ignore_import = [
        'personnelnumber',
        'lastname',
        'firstnames',
        'gender',
        'race',
        'emailaddress',
        'startdate'
    ];

    public static $org_prefix = 'orglevel';

    public static $org_levels = 8;

    public static $pre_validateFields = [
        0 => 'personnelnumber',     //required
        1 => 'instance_indicator_id',     //required

    ];
    public static $assessors_ignore = [
        "allocationmanager1",
        "allocationmanager2",
        "allocationmanager3",
        "allocationmanager4",
        "allocationmanager5",
        "allocationmanager6",
        "validationmanager1",
        "validationmanager2",
        "validationmanager3",
        "validationmanager4",
        "validationmanager5",
        "validationmanager6",
        "clustermanager1",
        "clustermanager2",
        "clustermanager3",
        "clustermanager4",
        "clustermanager5",
        "clustermanager6",
        "moderator1",
        "moderator2",
        "moderator3",
        "moderator4",
        "moderator5",
        "moderator6",
        "businessmanager1",
        "businessmanager2",
        "businessmanager3",
        "businessmanager4",
        "businessmanager5",
        "businessmanager6",
        "emp_contingent_awarded1",
        "emp_contingent_awarded2",
        "emp_contingent_awarded3",
        "emp_contingent_awarded4",
        "emp_contingent_awarded5",
        "emp_contingent_awarded6",

        "view1",
        "view2",
        "view3",
        "view4",
        "view5",
        "view6",
        "view7",
        "view8",
        "view9",
        "view10",
        "view11",
        "view12",
        "view13",
        "view14",
        "view15",

        "hrbp1",
        "hrbp2",
        "hrbp3",
        "hrbp4",
        "hrbp5",
        "hrbp6",

    ];

    public static $pre_validateNotBlanks = [0,1,2,3,6,7,10,14,15,16,17];

    //map data fields ids to form values
    public static function formFieldsToArray($fields_array, $strip_off)
    {
        $my_fields_array = [];
        foreach ($fields_array as $field => $value){

            if (strpos($field, $strip_off) !== false) {
                $missing[] = $field;
                $field = str_replace($strip_off, '', $field);
                $arr = (explode("&",$field));
                $field = $arr[1];
                $my_fields_array[$field] = $value;
            }

        }
        return $my_fields_array;
    }

    public static function validateImport($row, $data_fields_by_names, $lineno){
        ini_set('max_execution_time', 2000);
        ini_set('memory_limit', '-1');
        $rows_failed = []; // [ ['lineno'=>0, 'personnelno'=>1, 'field'=>'', 'status'=>'error', 'msg'=>['msg'], 'value'=>'' ], [], []... ]
        $currency_code  = Currency::all()->pluck('code')->toArray();
        $scheme_name    = Scheme::all()->pluck('name')->toArray();
        $record_status = [1, 2, 3, 4, 5 , 6, 7, 8];

        foreach ($row as $field => $col){
            $value = trim($col);
            $line = false;
            if (array_key_exists($field, $data_fields_by_names) === true){
                $sys_data_field = $data_fields_by_names[$field];
                //required
                if (ucwords($sys_data_field->required) != null){

                    if ((empty($value) && $value == '')){
                        $line = [
                            'lineno' => $lineno,
                            'personnelno' => $row['personnelnumber'],
                            'field' => $field,
                            'status' => 'error',
                            'msg' => 'Blank not allowed',
                            'value' => $value
                        ];
                    }
                }
                elseif ((!empty($value) && $value != '')){
                    switch ($sys_data_field->control_type){

                        case 'text':

                            /* if (($field == 'jobgrade') && (!empty($value))) { //band/jobgrade
                                 $first = strtoupper(substr($value,0,1));
                                 $second = (int)substr($value, 1,1);
                                 if (($first != 'B') || (($second < 1) || ($second > 5))){
                                     $rows_failed = [
                                         'lineno' => $lineno,
                                         'personnelno' => $row['personnelnumber'],
                                         'field' => $field,
                                         'status' => 'error',
                                         'msg' => 'Invalid Band/Jobgrade value, B1-5',
                                         'value' => $value
                                     ];
                                 }

                             }*/

                            break;
                        case 'date':
                            //$test = preg_match('/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/',$value);
                            $test = false;
                            try{
                                $dateValue = Date::excelToDateTimeObject((int)$value);
                                $result = $dateValue->format('Y-m-d');
                                $test = preg_match('/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/',$result);

                            }catch (\Exception $exception){
                                \Log::error($exception->getMessage());
                                \Log::error('Class Name: '.  __CLASS__ .' || Line No.: ' . __LINE__ . ' Field: '. $field . ' Data: '. $value);
                            }

                            if (!empty($value) && !$test){
                                $rows_failed = [
                                    'lineno' => $lineno,
                                    'personnelno' => $row['personnelnumber'],
                                    'field' => $field,
                                    'status' => 'error',
                                    'msg' => 'Invalid Date',
                                    'value' => $value
                                ];
                            }

                            break;
                        case 'number':
                            if (!empty($value) && !is_numeric($value)){
                                $rows_failed = [
                                    'lineno' => $lineno,
                                    'personnelno' => $row['personnelnumber'],
                                    'field' => $field,
                                    'status' => 'error',
                                    'msg' => 'Invalid Number',
                                    'value' => $value
                                ];
                            }
                            break;

                        case 'lookup':
                            if($field == 'incentive_scheme'){ // incentive scheme
                                // $search = Scheme::where('name',ucfirst($value))->first();
                                if (!in_array($value, $scheme_name)){
                                    $rows_failed = [
                                        'lineno' => $lineno,
                                        'personnelno' => $row['personnelnumber'],
                                        'field' => $field,
                                        'status' => 'error',
                                        'msg' => 'Invalid Incentive Scheme, not existing in the system',
                                        'value' => $value
                                    ];
                                }

                            }
                            else if ($field == 'salary_currency'){
                                // $search = Currency::where('code',$value)->first();
                                if (!in_array($value, $currency_code)){
                                    $rows_failed = [
                                        'lineno' => $lineno,
                                        'personnelno' => $row[DataField::$employee_table_fields[0]],
                                        'field' => $field,
                                        'status' => 'error',
                                        'msg' => 'Invalid currency code, not existing in the system',
                                        'value' => $value
                                    ];
                                }

                            }
                            else if ($field == 'record_status'){
                                if (!in_array((integer)$value, $record_status) /*|| !in_array($value, $record_status_reason)*/){
                                    $rows_failed = [
                                        'lineno' => $lineno,
                                        'personnelno' => $row[DataField::$employee_table_fields[0]],
                                        'field' => $field,
                                        'status' => 'error',
                                        'msg' => 'Incorrect record status, 1-5 allowed',
                                        'value' => $value
                                    ];
                                }


                            }
                            elseif (($field == 'jobgrade') && (!empty($value))) { //band/jobgrade
                                /* $first = strtoupper(substr($value,0,1));
                                 $second = (int)substr($value, 1,1);
                                 if (($first != 'B') || (($second < 1) || ($second > 5))){
                                     $rows_failed = [
                                         'lineno' => $lineno,
                                         'personnelno' => $row[DataField::$employee_table_fields[0]],
                                         'field' => $field,
                                         'status' => 'error',
                                         'msg' => 'Invalid Band/Jobgrade value, B1-5',
                                         'value' => $value
                                     ];
                                 }*/










                                if (($value < 1) || ($value > 5)){
                                    $rows_failed = [
                                        'lineno' => $lineno,
                                        'personnelno' => $row[DataField::$employee_table_fields[0]],
                                        'field' => $field,
                                        'status' => 'error',
                                        'msg' => 'Invalid Band/Jobgrade value, 1-5',
                                        'value' => $value
                                    ];
                                }

                            }

                            elseif (($field == 'milestone') && (!empty($value))) { //milestone


                                if ( ($value < 1) || ($value > 3) ){
                                    $rows_failed = [
                                        'lineno' => $lineno,
                                        'personnelno' => $row[DataField::$employee_table_fields[0]],
                                        'field' => $field,
                                        'status' => 'error',
                                        'msg' => 'Invalid Milestone value, M1-4',
                                        'value' => $value
                                    ];
                                }

                            }

                            elseif (($field == 'internal_jobscale_code') && (!empty($value) || $value != '')) { //benchmark


                            }

                            break;

                        default:
                            //
                    }
                }
            }
        }

        //ImportController::$importIssues[] = $rows_failed;
        if ( !empty($rows_failed)){
            $message['error'] = $rows_failed;
            return $message;
        }else{
            $message['success'] = $row['personnelnumber'];
            return $message;
        }
    }

    public static function isDate($string) {
        $matches = array();
        $pattern = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
        if (!preg_match($pattern, $string, $matches)) return false;
        if (!checkdate($matches[2], $matches[1], $matches[3])) return false;
        return true;
    }

    public static $ec_assessors = [
        'validationmanager1',
        'validationmanager2',
        'validationmanager3',
        'validationmanager4',
        'validationmanager5',
        'validationmanager6',

        'allocationmanager1',
        'allocationmanager2',
        'allocationmanager3',
        'allocationmanager4',
        'allocationmanager5',
        'allocationmanager6',

        'businessmanager1',
        'businessmanager2',
        'businessmanager3',
        'businessmanager4',
        'businessmanager5',
        'businessmanager6',

        'clustermanager1',
        'clustermanager2',
        'clustermanager3',
        'clustermanager4',
        'clustermanager5',
        'clustermanager6',

        'view1',
        'view2',
        'view3',
        'view4',
        'view5',
        'view6',

        'hrbp1',
        'hrbp2',
        'hrbp3',
        'hrbp4',
        'hrbp5',
        'hrbp6',
    ];
    public static $employeeCycleFields = [
        "altpersonnelnumber",
        "business_unit",
        "calc_prorata_factor",
        "charterband",
        "country",
        "currency_id",
        "cycle_id",
        "date_of_birth",
        "department",
        "division",
        "emailaddress",
        "emplcategory",
        "employee_id",
        "emplsubgrp",
        "empltype",
        "enddate",
        "fte",
        "internal_jobscale_code",
        "jobgrade",
        "jobgrade2",
        "jobtitle",
        "knownas",
        "linemanager",
        "linemanager_position",
        "linemanager_signitory_manager",
        "linemanager_signitory_manager_position",
        "maternity",
        "milestone",
        "org_structure_id",
        "record_status",
        "record_status_reason",
        "scheme_id",
        "startdate",
        "sub_owner_id",
    ];
    public static $ec_incentive_fields = [
        "allocation"
        ,"assessment_score"
        , "cycle_id"
        , "deferral_top_up"
        , "employee_cycle_id"
        , "financial_incentive_value"
        , "h_1_allocation"
        , "h_1_assessment_score"
        , "h_1_calc_cash"
        , "h_1_cash"
        , "h_1_deferral"
        , "h_1_discount_achievement"
        , "h_1_financial"
        , "h_1_financial_incentive_value"
        , "h_1_incentive_currency"
        , "h_1_incentive_scheme_id"
        , "h_1_incentive_top_up"
        , "h_1_incentive_value"
        , "h_1_incentive_zar"
        , "h_1_kpi_incentive_value"
        , "h_1_non_financial"
        , "h_1_on_target_achievement"
        , "h_1_ref_contribution"
        , "id"
        , "incentive_cap"
        , "incentive_top_up"
        , "longterm_incentive_value"
        , "pension_percentage"
        , "pension_value"
        , "previous_allocation"
        , "proposed_deferred_shares"
        , "proposed_deferred_value"
        , "proposedincentive_currency"
        , "proposedincentive_percentage"
        , "proposedincentive_value"
        , "provident_percentage"
        , "provident_value"
        , "ref_contribution"
        , "retail_scheme_august"
        , "retail_scheme_january"
        , "split_value"
        , "stanlib_suggest_incentive_cash_value"
        , "stanlib_suggest_incentive_shares_value"
        , "stanlib_suggest_incentive_total_value"
        , "stanlib_suggest_incentive_units_value"
        , "suggested_incentive"
    ];
    public static $ec_performance_fields = [
        "calc_last_performance_comment"
        , "critical_assessment"
        , "current_rating"
        , "cycle_id"
        , "employee_cycle_id"
        , "h_1_performance1"
        , "h_2_performance1"
        , "id"
        , "talent_indicator"
    ];
    public static $ec_benchmark_fields = [
        'employee_cycle_id', 'cycle_id', 'calc_internal_jobscale_name', 'calc_internal_jobscale_indicator', 'internal_jobscale_code',
        'calc_intbench_bonus_median', 'calc_compa_ratio_incentive', 'calc_compa_ratio_salary_pre', 'calc_compa_ratio_salary_post', 'calc_intbench_salary_median',
        'calc_internal_jobscale_grade', 'benchmark_id',
    ];

    public static $personal_admin_incentive_fields = [
        "employee_cycle_id" ,
        "cycle_id",
        "h_1_incentive_scheme_id",
        "incentive_scheme_id",
        "allocation" ,
        "assessment_score",
        "calc_cash",
        "calc_discount_achievement",
        "calc_discount_achievement_value" ,
        "calc_financial",
        "h_1_incentive_zar" ,
        "calc_incentivechange",
        "calc_kpi_incentive_value" ,
        "calc_last_incentive_comments" ,
        "calc_non_financial" ,
        "calc_on_target_achievement" ,
        "calc_proposed_cash_value" ,
        "calc_proposed_cash_value_zar",
        "calc_proposed_deferred_value_zar" ,
        "calc_proposedincentive_zar" ,
        "calc_retail_scheme",
        "suggested_incentive",
        "calc_total_deferral_topup" ,
        "calc_total_retail_scheme",
        "deferral_top_up",
        "financial_incentive_value",
        "h_1_allocation",
        "h_1_assessment_score",
        "h_1_calc_cash" ,
        "h_1_cash",
        "h_1_deferral",
        "h_1_discount_achievement" ,
        "h_1_financial",
        "h_1_financial_incentive_value",
        "h_1_incentive_currency",
        "h_1_incentive_percentage" ,
        "h_1_incentive_top_up" ,
        "h_1_incentive_value",
        "h_1_kpi_incentive_value",
        "h_1_non_financial" ,
        "h_1_on_target_achievement",
        "h_1_ref_contribution" ,
        "incentive_cap",
        "incentive_top_up" ,
        "longterm_incentive_value",
        "participation_category",
        "pension_percentage" ,
        "pension_value",
        "proposed_deferred_shares" ,
        "proposed_deferred_value",
        "proposedincentive_currency" ,
        "proposedincentive_percentage" ,
        "proposedincentive_value",
        "provident_percentage" ,
        "provident_value" ,
        "ref_contribution" ,
        "retail_scheme_august" ,
        "retail_scheme_january",
        "split_value" ,
    "stanlib_suggest_incentive_cash_value" ,
    "stanlib_suggest_incentive_shares_value" ,
    "stanlib_suggest_incentive_total_value" ,
    "stanlib_suggest_incentive_units_value",
    "previous_allocation" ,
    "calc_suggested_incentive" ,
    ];

    public static $personal_admin_salary_fields =   [
            "calc_interim_increase_value" ,
            "salary_continuation_plan",
            "salary_currency",
            "proposedsalary_currency",
            "salary_value",
            "calc_salary_zar",
            "calc_monthly_salary_value",
            "do_not_process_salary",
            "stanlib_suggest_salary_percentage" ,
            "proposedsalary_value",
            "calc_last_salary_comment",
            "calc_change_from_current",
            "calc_new_monthly_salary_value",
            "calc_monthly_salary_ZAR",
            "calc_percentage_upscale" ,
            "calc_percentage_upscale_previous",
            "prior_totalrem",
            "calc_total_rem",
            "calc_totalpercent",
            "contributionsrc1",
            "contributionsrc1_zar",
            "h_1_salary_currency",
            "h_1_salary_value",
            "h_1_salary_zar",
            "h_1_proposedsalary_percentage",
            "h_1_proposedsalary_value",
            "h_1_proposedsalary_zar",
            "proposedsalary_percentage",
            "calc_suggested_salary",
            "calc_proposedsalary_zar",
        ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeErrorStatus extends Model
{
    protected $table = 'vw_employees_error';
}

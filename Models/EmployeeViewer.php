<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeViewer extends Model
{
    protected $table = 'vw_employee_viewer';
}
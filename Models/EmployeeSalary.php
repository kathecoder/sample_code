<?php

namespace App\Models;

use App\CFLibrary\FieldFormatter;
use Illuminate\Database\Eloquent\Model;

class EmployeeSalary extends Model
{
    protected $table = 'employee_salaries';

    public static function saveEmployeeSalaries(Employee $employee, $row){
        $currencies = Currency::all()->pluck('code', 'id')->toArray();
        $cycle_id = 1; //todo: update cycle id
        $employee_salary = EmployeeSalary::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
        if ($employee_salary === null){
            $employee_salary = new EmployeeSalary();
            $employee_salary->cycle_id = $cycle_id;
            $employee_salary->personnelnumber = $employee->personnelnumber;
        }

        if (isset($row['salary_continuation_plan'])){
            $column_value = $row['salary_continuation_plan'];
            $employee_salary->salary_continuation_plan = FieldFormatter::format($column_value,'FIELD_FORMATTER_YES_NO') =='Yes' ? 1:0;
        }

        if (isset($row['salary_currency_id'])){
            $column_value = $row['salary_currency_id'];
            $key = array_search(strtoupper($column_value), $currencies);
            if ($key>0){
                $employee_salary->salary_currency_id= $key;
            }
            elseif(array_key_exists($column_value, $currencies)){
                $employee_salary->salary_currency_id = $column_value;
            }
            else {
                $employee_salary->salary_currency_id = null;
            }
        }

        if (isset($row['salary_currency_id_id'])){
            $column_value = $row['salary_currency_id_id'];
            $key = array_search(strtoupper($column_value), $currencies);
            if ($key>0){
                $employee_salary->salary_currency_id_id= $key;
            }
            elseif(array_key_exists($column_value, $currencies)){
                $employee_salary->salary_currency_id_id = $column_value;
            }
            else {
                $employee_salary->salary_currency_id_id = null;
            }
        }

        if (isset($row['salary_value']))
            $employee_salary->salary_value = $row['salary_value'];

        

        if (isset($row['stanlib_suggest_salary_percentage']))
            $employee_salary->stanlib_suggest_salary_percentage = $row['stanlib_suggest_salary_percentage'];

        if (isset($row['do_not_process_salary'])){
            $column_value = strtolower( $row['do_not_process_salary']);
            $employee_salary->do_not_process_salary = FieldFormatter::format($column_value,'FIELD_FORMATTER_YES_NO') =='Yes' ? 1:0;
            /*
            if (in_array($column_value, ['y', 'yes'])){
                $employee_salary->do_not_process_salary = 1;
            }
            elseif (in_array($column_value, ['n', 'no'])){
                $employee_salary->do_not_process_salary = 0;
            }
            else{
                $employee_salary->do_not_process_salary = null;
            }*/
        }

        if (isset($row['prior_totalrem']))
            $employee_salary->prior_totalrem = $row['prior_totalrem'];

        if (isset($row['prior_totalrem']))
            $employee_salary->prior_totalrem = $row['prior_totalrem'];

        if (isset($row['contributionsrc1']))
            $employee_salary->contributionsrc1 = $row['contributionsrc1'];

        if (isset($row['contributionsrc1_zar']))
            $employee_salary->contributionsrc1_zar = $row['contributionsrc1_zar'];

        if (isset($row['h_1_salary_currency']))
            $employee_salary->h_1_salary_currency = $row['h_1_salary_currency'];

        if (isset($row['h_1_salary_value']))
            $employee_salary->h_1_salary_value = $row['h_1_salary_value'];

        if (isset($row['h_1_salary_zar']))
            $employee_salary->h_1_salary_zar = $row['h_1_salary_zar'];

        if (isset($row['h_1_proposedsalary_percentage']))
            $employee_salary->h_1_proposedsalary_percentage = $row['h_1_proposedsalary_percentage'];

        if (isset($row['h_1_proposedsalary_value']))
            $employee_salary->h_1_proposedsalary_value = $row['h_1_proposedsalary_value'];


        if (isset($row['calc_suggested_salary']))
            $employee_salary->calc_suggested_salary = $row['calc_suggested_salary'];

        if (isset($row['calc_percentage_upscale_previous']))
            $employee_salary->calc_percentage_upscale_previous = $row['calc_percentage_upscale_previous'];

        if (isset($row['calc_percentage_upscale']))
            $employee_salary->calc_percentage_upscale = $row['calc_percentage_upscale'];

        if (isset($row['h_1_proposedsalary_zar']))
            $employee_salary->h_1_proposedsalary_zar = $row['h_1_proposedsalary_zar'];


            /** proposed salary  value section  start*/
        if (isset($row['proposedsalary_value'])){
            $salary_currency    = null;
            $salary_value       = null;
            if (isset($row['salary_currency_id'])) {
                $match = strtoupper($row['salary_currency_id']);
                $salary_currency = Currency::where('code', $match)->first();
            }
            elseif($employee_salary){
                $salary_currency = Currency::findOrFail($employee_salary->salary_currency_id);
            }

            if (isset($row['salary_value']) ){
                $salary_value = $row['salary_value'];
            }
            elseif($employee_salary){
                $salary_value = $employee_salary->salary_value;
            }

            $proposedsalary_value                       = $row['proposedsalary_value'] == '' ? 0 : $row['proposedsalary_value'];
            $employee_salary->proposedsalary_value            = $proposedsalary_value;
            $employee_salary->calc_new_monthly_salary_value   = round($proposedsalary_value / 12, 2);
            $my_calc_proposedsalary_zar                 = round($proposedsalary_value / $salary_currency->conversion_rate , 2);
            $employee_salary->calc_proposedsalary_zar         = $my_calc_proposedsalary_zar ;
            $employee_salary->calc_monthly_salary_ZAR         = round($my_calc_proposedsalary_zar / 12, 2) ;

            $employee_salary->calc_change_from_current    = round($proposedsalary_value - $salary_value, 2);
            $employee_salary->proposedsalary_percentage   = round($salary_value > 0 ? ( $proposedsalary_value - $salary_value) / $salary_value * 100 : 0, 1);
        }
            //$employee_salary->proposedsalary_value = $row['proposedsalary_value'];
        /** proposed salary value section  end*/




        /** proposed salary percentage section  start*/
//       if (property_exists("proposedsalary_percentage",$row)){
           if (isset($row['proposedsalary_percentage']) && !empty($row['proposedsalary_percentage']) ){
               $salary_currency    = null;
               $salary_value       = null;
               if (isset($row['salary_currency_id'])) {
                   $match = strtoupper($row['salary_currency_id']);
                   $salary_currency = Currency::where('code', $match)->first();
                   if ($salary_currency)
                       $employee_salary->salary_currency_id = $salary_currency->id;
               }
               elseif($employee_salary){
                   $salary_currency = Currency::findOrFail($employee_salary->salary_currency_id);
               }

               if (isset($row['salary_value']) ){
                   $salary_value = $row['salary_value'];
               }
               elseif($employee_salary){
                   $salary_value = $employee_salary->salary_value;
               }

               /**  (proposedsalary_percentage / 100 * salary_value) +  salary_value */
//            $employee_salary->proposedsalary_percentage       = $row['proposedsalary_percentage'];
               $proposedsalary_percentage                        = $row['proposedsalary_percentage'] == '' ? 0 : $row['proposedsalary_percentage'];
               $proposedsalary_value                             = round(($proposedsalary_percentage  / 100 * $salary_value) + $salary_value, 0);
               $employee_salary->calc_new_monthly_salary_value   = round($proposedsalary_value / 12, 0);
               $employee_salary->proposedsalary_value            = round($proposedsalary_value, 0);
               $employee_salary->proposedsalary_percentage       = round($proposedsalary_percentage, 1);
               $employee_salary->calc_change_from_current        = round($proposedsalary_value - $salary_value, 0);

               $my_calc_proposedsalary_zar                       = round($proposedsalary_value / $salary_currency->conversion_rate , 0);
               $employee_salary->calc_proposedsalary_zar         = $my_calc_proposedsalary_zar;
               $employee_salary->calc_monthly_salary_ZAR         = round($my_calc_proposedsalary_zar / 12, 0);

           }

           else if (isset($row['proposedsalary_percentage']) && empty($row['proposedsalary_percentage'])){
               $salary_currency    = null;
               $salary_value       = null;
               if (isset($row['salary_currency_id'])) {
                   $match = strtoupper($row['salary_currency_id']);
                   $salary_currency = Currency::where('code', $match)->first();
                   if ($salary_currency)
                       $employee_salary->salary_currency_id = $salary_currency->id;
               }
               elseif($employee_salary){
                   $salary_currency = Currency::findOrFail($employee_salary->salary_currency_id);
               }

               if (isset($row['salary_value']) ){
                   $salary_value = $row['salary_value'];
               }
               elseif($employee_salary){
                   $salary_value = $employee_salary->salary_value;
               }

               $employee_salary->proposedsalary_percentage       = 0.0;
               $proposedsalary_value                             = round($salary_value, 0);
               $employee_salary->calc_new_monthly_salary_value   = round($proposedsalary_value / 12, 0);
               $employee_salary->proposedsalary_value            = round($proposedsalary_value, 0);
               $employee_salary->calc_change_from_current        = round($proposedsalary_value - $salary_value, 0);

               $my_calc_proposedsalary_zar                       = round($proposedsalary_value / $salary_currency->conversion_rate , 0);
               $employee_salary->calc_proposedsalary_zar         = $my_calc_proposedsalary_zar;
               $employee_salary->calc_monthly_salary_ZAR         = round($my_calc_proposedsalary_zar / 12, 0);

               //logger("PN: " . $employee_salary->personnelnumber);


           }
//       }

        Audit::AuditTrailPerModel('employee_salaries', $employee_salary, $employee->personnelnumber);
        $employee_salary->save();

    }

}

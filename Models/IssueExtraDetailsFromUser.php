<?php

namespace App\Models;

use Dompdf\Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class IssueExtraDetailsFromUser extends Model
{
    protected $table = 'issue_extra_details_from_user';

    public static function createOrUpdate($issue_log_id,$state){

        //--------{ Update Employee Information } ----------
        if($state['ui_pi']!==null){//Personal Information | 1
            $count = 1;
            foreach ($state['ui_pi'] as $item){
                if(is_array($item)===false){
                    $extra = new IssueExtraDetailsFromUser();
                    $extra -> issue_log_id = $issue_log_id;
                    $extra -> issue_extra_detail_id = $count;
                    $extra -> text = $item;
                    $extra -> save();

                    $count ++;
                }

            }

        }
        if($state['ui_si']!==null){//Performance - previously Incentive Scheme
            $count = 8;
            foreach ($state['ui_si'] as $item){
                if(is_array($item)===false){
                    try {
                        $extra = new IssueExtraDetailsFromUser();
                        $extra -> issue_log_id = $issue_log_id;
                        $extra -> issue_extra_detail_id = $count;
                        $extra -> text = $item;
                        $extra -> save();
                        Log::notice($count.' Performance: '. $item);
                        $count ++;
                    }catch (\Exception $e){
                        Log::notice($e->getMessage());
                    }
                }

            }
        }
        if($state['ui_tb']!==null){//Target Business Unit | 3
            $count = 11;
            foreach ($state['ui_tb'] as $item){
                if(is_array($item)===false){
                    $extra = new IssueExtraDetailsFromUser();
                    $extra -> issue_log_id = $issue_log_id;
                    $extra -> issue_extra_detail_id = $count;
                    $extra -> text = $item;
                    $extra -> save();

                    $count ++;
                }

            }
        }
        if($state['ui_p']!==null){//Position | 4
            $count = 16;
            foreach ($state['ui_p'] as $item){
                if(is_array($item)===false){
                    $extra = new IssueExtraDetailsFromUser();
                    $extra -> issue_log_id = $issue_log_id;
                    $extra -> issue_extra_detail_id = $count;
                    $extra -> text = $item;
                    $extra -> save();

                    $count ++;
                }

            }
        }
        if($state['ui_rl']!==null){//Reporting line | 5
            $count = 20;
            foreach ($state['ui_rl'] as $item){
                if(is_array($item)===false){
                    $extra = new IssueExtraDetailsFromUser();
                    $extra -> issue_log_id = $issue_log_id;
                    $extra -> issue_extra_detail_id = $count;
                    $extra -> text = $item;
                    $extra -> save();

                    $count ++;
                }

            }
        }
        if($state['ui_org']!==null){//Organisational Structure | 6
            $count = 24;
            foreach ($state['ui_org'] as $item){

                if(is_array($item)===false){
                    Log::notice('Org structure: '.$item);
                    $extra = new IssueExtraDetailsFromUser();
                    $extra -> issue_log_id = $issue_log_id;
                    $extra -> issue_extra_detail_id = $count;
                    $extra -> text = $item;
                    $extra -> save();

                    $count ++;
                }

            }
        }
        //--------{ Add New User } ----------
        if($state['ui_sp']!==null){//Scheme Participant | 7
            $count = 299;
            foreach ($state['ui_sp'] as $item){
                if(is_array($item)===false){
                    $extra = new IssueExtraDetailsFromUser();
                    $extra -> issue_log_id = $issue_log_id;
                    $extra -> issue_extra_detail_id = $count;
                    $extra -> text = $item;
                    $extra -> save();

                    $count ++;
                }

            }
        }
        if($state['ui_a']!==null){//Assessor | 8
            $count = 49;
            foreach ($state['ui_a'] as $item){
                if(is_array($item)===false){
                    $extra = new IssueExtraDetailsFromUser();
                    $extra -> issue_log_id = $issue_log_id;
                    $extra -> issue_extra_detail_id = $count;
                    $extra -> text = $item;
                    $extra -> save();

                    $count ++;
                }

            }
        }
        //--------{ Termination } ----------
        if($state['ui_te']!==null){//Termination | 9
            $count = 29;
            foreach ($state['ui_te'] as $item){
                if(is_array($item)===false){
                    $extra = new IssueExtraDetailsFromUser();
                    $extra -> issue_log_id = $issue_log_id;
                    $extra -> issue_extra_detail_id = $count;
                    $extra -> text = $item;
                    $extra -> save();

                    $count ++;
                }

            }
        }
        //--------{ Others } ----------
        if($state['ui_others']!==null){//Others | 10
            $count = 32;
            foreach ($state['ui_others'] as $item){
                if(is_array($item)===false){
                    $extra = new IssueExtraDetailsFromUser();
                    $extra -> issue_log_id = $issue_log_id;
                    $extra -> issue_extra_detail_id = $count;
                    $extra -> text = $item;
                    $extra -> save();

                    $count ++;
                }

            }
        }

    }
    public static function getIssueLogExtraDetail($issue_id, $sub_issue_log_id)
    {
        //Log::notice('Sub Issue Id: '.print_r($sub_issue_log_id, true));
        $res = IssueExtraDetailsFromUser::where('issue_log_id', $issue_id)
            ->whereIn('issue_extra_detail_id', $sub_issue_log_id)
            ->get();
        return $res;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrgStructure extends Model
{
    protected  $table = 'org_structures';

    public static function orgStructureMapping($employee,  $instance_input, $region_input, $country_input, $company_input, $business_area_input, $business_unit_input, $division_input, $department_input ){
        $instance_id = null;
        $region_id = null;
        $country_id = null;
        $company_id = null;
        $business_area_id = null;
        $business_unit_id = null;
        $division_id = null;
        $department_id = null;
        try{
            $instance = $instance_input !== null ? Instance::where('name', $instance_input)->first() : false;

            if ($instance === null){
                $instance = new Instance();
                $instance->name = $instance_input;
                $instance->save();

            }
            $instance_id = $instance === false ? null : $instance->id;

            $region = $region_input !== null ? Region::where('name', $region_input)->first() : false;

            if ($region === null){
                $region = new Region();
                $region->name = $region_input;
                $region->save();

            }
            $region_id = $region === false ? null : $region->id;

            $country = $country_input !== null ? Country::where('name', $country_input)->first() : false;

            if ($country === null){
                $country = new Country();
                $country->name = $country_input;
                $country->save();

            }
            $country_id = $country === false ? null : $country->id;

            $company = $company_input !== null ? Company::where('name', $company_input)->first() : false;

            if ($company === null){
                $company = new Company();
                $company->name = $company_input;
                $company->save();

            }
            $company_id = $company === false ? null : $company->id;

            $business_area = $business_area_input !== null ? BusinessArea::where('name', $business_area_input)->first() : false;

            if ($business_area === null){
                $business_area = new BusinessArea();
                $business_area->name = $business_area_input;
                $business_area->save();

            }
            $business_area_id = $business_area === false ? null : $business_area->id;

            $business_unit = $business_unit_input !== null ? BusinessUnit::where('name', $business_unit_input)->first() : false;
            if ($business_unit === null){
                $business_unit = new BusinessUnit();
                $business_unit->name = $business_unit_input;
                $business_unit->save();
            }

            $business_unit_id = $business_unit === false ? null : $business_unit->id;

            $division = $division_input !== null ? Division::where('name', $division_input)->first() : false;
            if ($division === null){
                $division = new Division();
                $division->name = $division_input;
                $division->save();
            }

            $division_id = $division === false ? null : $division->id;

            $department = $department_input !== null ? Department::where('name', $department_input)->first() : false;

            if ($department === null){
                $department = new Department();
                $department->name = $department_input;
                $department->save();
            }
            $department_id = $department === false ? null : $department->id;

            $org_structure = OrgStructure::
            where('instance_id', $instance_id)
                ->where('region_id', $region_id)
                ->where('country_id', $country_id)
                ->where('company_id', $company_id)
                ->where('business_area_id', $business_area_id)
                ->where('business_unit_id', $business_unit_id)
                ->where('division_id', $division_id)
                ->where('department_id', $department_id)->first();

            if ($org_structure === null){
                $org_structure = new OrgStructure();
                $org_structure->instance_id = $instance_id;
                $org_structure->region_id = $region_id;
                $org_structure->country_id = $country_id;
                $org_structure->company_id = $company_id;
                $org_structure->business_area_id = $business_area_id;
                $org_structure->business_unit_id = $business_unit_id;
                $org_structure->division_id = $division_id;
                $org_structure->department_id = $department_id;

                $org_structure->save();
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

        return $org_structure;
    }




    public function getAllOrgStructures(Request $request){

        try{
            $array = array();
            $searchValue = $request->state['searchInput'];
            $take = $request->state['take'];
            $skip = $request->state['skip'];

            /*$employee_ids_to_exclude = $request->except;

            $whereEmpIdNotIn = "";
            if (isset($employee_ids_to_exclude)){
                if (count($employee_ids_to_exclude) > 0){
                    $employee_ids_to_exclude = $str =  implode(",", $employee_ids_to_exclude);
                    $whereEmpIdNotIn .= " AND  employees.id NOT IN($employee_ids_to_exclude)";
                }
            }


            $records = DB::select("
                    select org_structures.id, business_areas.name as business_area, business_units.name as business_unit, divisions.name as division, departments.name as department  
                           FROM org_structures  
                           LEFT JOIN business_areas on business_areas.id = org_structures.business_area_id 
                           LEFT JOIN business_units on business_units.id = org_structures.business_unit_id 
                           LEFT JOIN divisions on divisions.id = org_structures.division_id 
                           LEFT JOIN departments on departments.id = org_structures.department_id 
                           
                           WHERE 1 $whereEmpIdNotIn AND ( business_areas.name LIKE '%$searchValue%' or business_units.name LIKE '%$searchValue%' or divisions.name LIKE '%$searchValue%' or departments.name LIKE '%$searchValue%') 
                        ORDER BY org_structures.updated_at DESC limit $take offset $skip
            
            ");




            $total = DB::SELECT("select count(*) as total FROM org_structures 
                           LEFT JOIN business_areas on business_areas.id = org_structures.business_area_id 
                           LEFT JOIN business_units on business_units.id = org_structures.business_unit_id 
                           LEFT JOIN divisions on divisions.id = org_structures.division_id 
                           LEFT JOIN departments on departments.id = org_structures.department_id 
                           
                                        WHERE 1 $whereEmpIdNotIn AND ( business_areas.name LIKE '%$searchValue%' or business_units.name LIKE '%$searchValue%' or divisions.name LIKE '%$searchValue%' or departments.name LIKE '%$searchValue%')
                                       ");
            $total = $total[0]->total;*/


            $orgs = DB::table('vw_organisational_structures');
            $orgs =   $orgs->where(function($query) use ($searchValue){
                $query
                    ->where('region', 'like', '%'.$searchValue.'%')
                    ->orWhere('instance', 'like', '%'.$searchValue.'%')
                    ->orWhere('country', 'like', '%'.$searchValue.'%')
                    ->orWhere('company', 'like', '%'.$searchValue.'%')
                    ->orWhere('business', 'like', '%'.$searchValue.'%')
                    ->orWhere('division', 'like', '%'.$searchValue.'%')
                    ->orWhere('department', 'like', '%'.$searchValue.'%')
                    ->orWhere('business_unit', 'like', '%'.$searchValue.'%');
            });

            $total = DB::table('vw_organisational_structures');
            $total =   $total->where(function($query) use ($searchValue){
                $query
                    ->where('region', 'like', '%'.$searchValue.'%')
                    ->orWhere('instance', 'like', '%'.$searchValue.'%')
                    ->orWhere('country', 'like', '%'.$searchValue.'%')
                    ->orWhere('company', 'like', '%'.$searchValue.'%')
                    ->orWhere('business', 'like', '%'.$searchValue.'%')
                    ->orWhere('division', 'like', '%'.$searchValue.'%')
                    ->orWhere('department', 'like', '%'.$searchValue.'%')
                    ->orWhere('business_unit', 'like', '%'.$searchValue.'%');
            });



            foreach ($orgs as $item){
                $array[] = array(
                    'id'        =>$item->id,
                    'instance'  =>$item->business_area,
                    'region'    =>$item->region,
                    'country'   =>$item->country,
                    'company'            =>$item->company,
                    'business'            =>$item->business,
                    'business_unit'            =>$item->business_unit,
                    'division'            =>$item->division,
                    'department'            =>$item->department,
                );
            }

            return array (
                'error'             =>false,
                'response'          =>array(
                    'data'          =>$array,
                    'pageNate'      =>$this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>count($total),
                    'currentCycle'  =>Cycle::getCurrentCycle()
                ),
                'message'=>'Organisational Structure successfully Loaded'
            );
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }




}

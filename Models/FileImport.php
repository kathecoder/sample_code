<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FileImport extends Model
{
    protected $table = 'imports';


    public static function downloadFileByStoragePath($path){
        try {
            logger($path);
            $file_name = explode('/', $path);
            $file_name = end($file_name);
            return response()->file(storage_path('app/'. $path), [
                'Content-Type' => 'application/octet-stream',
                'Content-Disposition' => 'inline; filename='.$file_name,
                'Content-Length' => filesize(storage_path('app/'. $path)),
                'Content-Transfer-Encoding' => 'binary',
                'Accept-Ranges' => 'bytes'
            ]);
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }


    public static function getRecords($searchInput,$skip,$take, $is_paginate)
    {
        //$audited_files = Filters::convertJsonIntoArray(Files::where('reference_table_name', 'user_cycles')->where('status_id', 7)->get(['reference_table_id as id']));

        $records = DB::table('imports')
            ->join('cycles', 'cycles.id', 'imports.cycle_id')
            ->join('users', 'imports.user_id', 'users.id')
            ->where('imports.cycle_id', '=', Cycle::getLiveCycleID())
            ->where(function($query) use ($searchInput){
                $query
                    ->where('users.name', 'like', '%'.$searchInput.'%')
                    ->orWhere('imports.comment', 'like', '%'.$searchInput.'%')
                    ->orWhere('imports.status', 'like', '%'.$searchInput.'%');
            })
            ->select(
                DB::raw("users.name as name"),
                DB::raw("imports.id as id, imports.status, imports.comment, imports.path_source, imports.validation_report, imports.path_report, imports.created_at")
            );

        //$records->orderByDesc('imports.updated_at');

        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }
        return $records->get();
    }
}

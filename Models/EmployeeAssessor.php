<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EmployeeAssessor extends Model
{
    protected $table = 'employee_assessors';

    public static function mapEmployeeAssessors(Employee $employee, $row){
        try{
            $cycle_id = Cycle::getLiveCycleID();
            $employee_assessor = EmployeeAssessor::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
            if ($employee_assessor === null){
                $employee_assessor = new EmployeeAssessor();
                $employee_assessor->cycle_id = $cycle_id;
                $employee_assessor->personnelnumber = $employee->personnelnumber;
            }

            $assessor_fields = AppAssist::getWriteFields('employee_assessors');

            foreach ($assessor_fields as $assessor_field){
                if (isset($row[$assessor_field]))
                    $employee_assessor->{$assessor_field} = $row[$assessor_field];
            }

            Audit::AuditTrailPerModel('employee_assessors', $employee_assessor, $employee->personnelnumber);
            $employee_assessor->save();
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public static function getAllRecords($searchInput,$skip,$take, $is_paginate, $filterFilters=false, $type)
    {
        //$collection = Filters::getFilterFilters('allocation_manager_id');
        $collection = Filters::getFilterFilters($type);
        $tem_col = substr($type, 0, -2);

        $emp_pns = [];
        /*foreach ($collection as $personnel){
            for ($x=1; $x<=6; $x++){
                $col = $tem_col . $x;
                if ($personnel->{$col} != null || $personnel->{$col} != '')
                    $emp_pns[] = $personnel->{$col};
            }
        }
        $emp_pns = array_unique($emp_pns);*/

        if ($type ==='allocation_manager_id') {

            foreach ($collection as $personnel){

                if ($personnel->allocation_manager_1 !== null || $personnel->allocation_manager_1 !== '')
                    $emp_pns[] = $personnel->allocation_manager_1;

                if ($personnel->allocation_manager_2 !== null || $personnel->allocation_manager_2 !== '')
                    $emp_pns[] = $personnel->allocation_manager_2;

                if ($personnel->allocation_manager_3 !== null || $personnel->allocation_manager_3 !== '')
                    $emp_pns[] = $personnel->allocation_manager_3;

                if ($personnel->allocation_manager_4 !== null || $personnel->allocation_manager_4 !== '')
                    $emp_pns[] = $personnel->allocation_manager_4;

                if ($personnel->allocation_manager_5 !== null || $personnel->allocation_manager_5 !== '')
                    $emp_pns[] = $personnel->allocation_manager_5;

                if ($personnel->allocation_manager_6 !== null || $personnel->allocation_manager_6 !== '')
                    $emp_pns[] = $personnel->allocation_manager_6;

            }

        }
        else if($type === 'business_manager_id'){
            foreach ($collection as $personnel){

                if ($personnel->business_manager_1 !== null || $personnel->business_manager_1 !== '')
                    $emp_pns[] = $personnel->business_manager_1;

                if ($personnel->business_manager_2 !== null || $personnel->business_manager_2 !== '')
                    $emp_pns[] = $personnel->business_manager_2;

                if ($personnel->business_manager_3 !== null || $personnel->business_manager_3 !== '')
                    $emp_pns[] = $personnel->business_manager_3;

                if ($personnel->business_manager_4 !== null || $personnel->business_manager_4 !== '')
                    $emp_pns[] = $personnel->business_manager_4;

                if ($personnel->business_manager_5 !== null || $personnel->business_manager_5 !== '')
                    $emp_pns[] = $personnel->business_manager_5;

                if ($personnel->business_manager_6 !== null || $personnel->business_manager_6 !== '')
                    $emp_pns[] = $personnel->business_manager_6;

            }

        }
        else if($type === 'business_area_manager_id'){
            foreach ($collection as $personnel){

                if ($personnel->business_area_manager_1 !== null || $personnel->business_area_manager_1 !== '')
                    $emp_pns[] = $personnel->business_area_manager_1;

                if ($personnel->business_area_manager_2 !== null || $personnel->business_area_manager_2 !== '')
                    $emp_pns[] = $personnel->business_area_manager_2;

                if ($personnel->business_area_manager_3 !== null || $personnel->business_area_manager_3 !== '')
                    $emp_pns[] = $personnel->business_area_manager_3;

                if ($personnel->business_area_manager_4 !== null || $personnel->business_area_manager_4 !== '')
                    $emp_pns[] = $personnel->business_area_manager_4;

                if ($personnel->business_area_manager_5 !== null || $personnel->business_area_manager_5 !== '')
                    $emp_pns[] = $personnel->business_area_manager_5;

                if ($personnel->business_area_manager_6 !== null || $personnel->business_area_manager_6 !== '')
                    $emp_pns[] = $personnel->business_area_manager_6;

            }
        }
        $collection = DB::table('vw_employees_export')
                        ->where(function($query) use ($searchInput){
                            $query
                                ->where('personnelnumber', 'like', '%'.$searchInput.'%')
                                ->orWhere('jobtitle', 'like', '%'.$searchInput.'%')
                                ->orWhere('lastname', 'like', '%'.$searchInput.'%')
                                ->orWhere('knownas', 'like', '%'.$searchInput.'%')
                                ->orWhere('firstnames', 'like', '%'.$searchInput.'%');
                        })
                        ->select(self::$assessors_headers);

        if($filterFilters===true){$collection ->  whereIn('personnelnumber', $emp_pns);}

        $collection->orderBy('knownas');

        if($is_paginate===true){
            $collection->skip($skip)
                ->take($take);
        }
        return $collection->get();
    }

    public static $assessors_headers = [
        'id',
        'personnelnumber',
        'knownas',
        'lastname',
        'orglevel5',
        'orglevel6',
        'orglevel7',
        'orglevel8'
        ,
    ];
}

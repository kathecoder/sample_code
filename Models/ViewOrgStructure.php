<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewOrgStructure extends Model
{
    protected $table = 'vw_org_structure';
}
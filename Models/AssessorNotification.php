<?php

namespace App\Models;

use App\Http\Controllers\PermissionController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AssessorNotification extends Model
{
    protected $table = 'assessor_phase_notifications';
    
    public static function getData($searchInput,$skip,$take, $is_paginate){
        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $cycle_id           = Cycle::getLiveCycleID();
        $phase_id           = Phase::getLivePhaseID();
        $records = DB::table('assessor_phase_notifications')
            ->join('cycles', 'cycles.id', 'assessor_phase_notifications.cycle_id')
            ->join('instance_indicators', 'instance_indicators.id', 'assessor_phase_notifications.instance_indicator_id')
            ->join('phases', 'phases.id', 'assessor_phase_notifications.phase_id')
            ->join('employees', 'employees.personnelnumber', 'assessor_phase_notifications.personnelnumber')

            ->where('assessor_phase_notifications.instance_indicator_id', $active_instance_id)
            ->where('assessor_phase_notifications.phase_id', $phase_id)
            ->where('assessor_phase_notifications.cycle_id', $cycle_id)
            ->where(function($query) use ($searchInput){
                $query
                    ->where('instance_indicators.name', 'like', '%'.$searchInput.'%')
                    ->orWhere('employees.personnelnumber', 'like', '%'.$searchInput.'%')
                    ->orWhere('employees.knownas', 'like', '%'.$searchInput.'%')
                    ->orWhere('employees.emailaddress', 'like', '%'.$searchInput.'%')
                    ->orWhere('employees.lastname', 'like', '%'.$searchInput.'%');
            })
            ->select(
                DB::raw("assessor_phase_notifications.id as id, assessor_phase_notifications.updated_at as notified_on, assessor_phase_notifications.notified as notified, assessor_phase_notifications.resent  as resent, assessor_phase_notifications.instance_indicator_id  as instance_indicator  "),
                DB::raw("employees.knownas, employees.lastname, employees.personnelnumber, employees.emailaddress"),
                DB::raw("instance_indicators.name as instance"),
                DB::raw("phases.name as phase"),
                DB::raw("cycles.name as cycle_name")
            );

        $records->orderByDesc('assessor_phase_notifications.updated_at');

        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }
        return $records->get();
    }
}
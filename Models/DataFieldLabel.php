<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataFieldLabel extends Model
{
    protected $fillable = ['sub_owner_id', 'data_field_id'];

}

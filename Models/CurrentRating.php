<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrentRating extends Model
{
    protected $table = 'current_ratings';
}

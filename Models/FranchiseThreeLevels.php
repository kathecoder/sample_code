<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FranchiseThreeLevels extends Model
{
    protected $table = "franchise_three_levels";
}

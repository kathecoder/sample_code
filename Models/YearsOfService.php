<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YearsOfService extends Model
{
    protected $table = 'years_of_services';
}

<?php

namespace App\Models;

use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class Filters extends Model
{
    protected $table = 'filters';

    public static function createFilter($reference_table, $reference_table_id)
    {
        $check_filters = Filters::where('user_id', Auth::user()->id)
            ->where('reference_table', $reference_table)
            ->where('reference_table_id', $reference_table_id)
            ->get();

        if(count($check_filters)>0){
            $filter = Filters::find($check_filters[0]->id);
            $filter->delete();
        }
        else{
            $filter = new Filters();
            $filter -> user_id = Auth::user()->id;
            $filter -> reference_table = $reference_table;
            $filter -> reference_table_id = $reference_table_id;
            $filter -> save();
        }

    }
    public static function createFilter2($reference_table, $created_at, $updated_at)
    {

        $check_filters = Filters::where('user_id', Auth::user()->id)
            ->where('reference_table', $reference_table)
            ->where('reference_table_id', null)
            ->get();

        if(count($check_filters)>0){
            $filter = Filters::find($check_filters[0]->id);
            $filter->delete();
        }
        $filter = new Filters();
        $filter -> user_id = Auth::user()->id;
        $filter -> reference_table = $reference_table;
        $filter -> reference_table_id = null;
        $filter -> created_at = $created_at;
        $filter -> updated_at = $updated_at;
        $filter -> save();

    }
    public static function clearFilters()
    {
        $res = Filters::where('user_id', Auth::user()->id)->delete();
        return $res;
    }
    public static function searchFilters($reference_table, $reference_table_id)
    {
        $res = Filters::where('user_id', Auth::user()->id)
            ->where('reference_table', $reference_table)
            ->where('reference_table_id', $reference_table_id)
            ->get();
        if(count($res)>0){
            return true;
        }else{
            return false;
        }
    }
    public static function getFilterFiltersOld($type)
    {
        $res    =   DB::table('user_cycles')
                    ->join('employees', 'employees.id', 'user_cycles.employee_id')
                    ->join('org_structures', 'org_structures.id', 'employees.org_structure_id');
        if($type==='target_mapping_id'){
            $res    ->join('target_mappings', 'target_mappings.id', 'employees.target_mapping_id');
        }
        $res        ->where('user_cycles.cycle_id', Cycle::getLiveCycleID())
                    ->where('user_cycles.is_live', 1);

        if($type==='business_area_id'){$res->select("org_structures.business_area_id as id");}
        if($type==='business_unit_id'){$res->select("org_structures.business_unit_id as id");}
        if($type==='division_id'){$res->select("org_structures.division_id as id");}
        if($type==='department_id'){$res->select("org_structures.department_id as id");}
        if($type==='scheme_id'){$res->select("employees.scheme_id as id");}
        if($type==='participation_category_id'){$res->select("employees.participation_category_id as id");}
        if($type==='employee_manager_mappings'){$res->select("employees.id as id");}
        if($type==='target_mapping_id'){
                $res->select(DB::raw("((SELECT CONCAT_WS(',',target_mappings.unit_target_business_unit_id1,target_mappings.unit_target_business_unit_id2,target_mappings.unit_target_business_unit_id3) from target_mappings where target_mappings.id=employees.target_mapping_id)) as id"));
                return [$res->get()[0]->id];
        }

        return Filters::convertJsonIntoArray($res->get());

    }

    public static function getFilterFilters($type)
    {

        if($type==='country_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='business_area_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='business_unit_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='division_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='race_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='department_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);;
        if($type==='current_rating_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='gender_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='band_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='scheme_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='allocation_manager_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='business_manager_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);
        if($type==='business_area_manager_id') return self::filterEmployeeColumnIDsPerManager(null, $type, $use_active_instance_id = true);













    }
    public static function getFilteredCycleId(){
        $filters = Filters::where('user_id', 2)->get();

        $business_areas = array();
        $business_units = array();
        $divisions = array();
        $departments = array();
        $manager_name = array();
        $target_business_units = array();
        $schemes = array();
        $participation_categories = array();

        foreach ($filters as $data){
            if($data->reference_table==='business_areas'){
                $business_areas[] = $data->reference_table_id;
            }else if($data->reference_table==='business_units'){
                $business_units[] = $data->reference_table_id;
            }else if($data->reference_table==='divisions'){
                $divisions[] = $data->reference_table_id;
            }else if($data->reference_table==='departments'){
                $departments[] = $data->reference_table_id;
            }else if($data->reference_table==='manager_name'){
                $manager_name[] = $data->reference_table_id;
            }else if($data->reference_table==='target_business_units'){
                $target_business_units[] = $data->reference_table_id;
            }else if($data->reference_table==='schemes'){
                $schemes[] = $data->reference_table_id;
            }else if($data->reference_table==='participation_categories'){
                $participation_categories[] = $data->reference_table_id;
            }

        }

        //sort org structure
        $org = OrgStructure::where('id','>', 0);
            if(count($business_areas)>0){$org->whereIn('business_area_id', $business_areas);}
            if(count($business_units)>0){$org->whereIn('business_unit_id', $business_units);}
            if(count($divisions)>0){$org->whereIn('division_id', $divisions);}
            if(count($departments)>0){$org->whereIn('department_id', $departments);}

        return $org->get();
    }
    public static function getFilteredOrgStructure()
    {
        $user_id = Auth::user()->id;
        $cycle_id = Cycle::getLiveCycleID();
        $user_cycle = UserCycle::where('user_id', $user_id)
            ->where('cycle_id', $cycle_id)
            ->get();

        if(count($user_cycle)===0){//Admin
            return null;
        }else{//Participant
            $employee = Employee::find($user_cycle[0]->employee_id);
            $org = OrgStructure::find($employee->org_structure_id);
            return $org;
        }
    }
    public static function filterOrgStructure($searchInput){
        $user_id = Auth::user()->id;


        //--Get Filters
        $filters = Filters::where('user_id', $user_id)->get(['id']);

        $org = OrgStructure::where('id', '>', 0);


        $whereCondition ="org_structures.id>0";
        $tables = "org_structures";
        if(count($filters)>0){

            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'business_areas')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.business_area_id=business_areas.id AND business_areas.id IN (".Filters::convertJsonIntoArray2($filtered).") AND business_areas.name LIKE '%".$searchInput."%')";
                $tables = $tables .",business_areas";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.business_area_id IS NULL OR org_structures.business_area_id IS NOT NULL)";
            }

            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'business_units')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.business_unit_id=business_units.id AND business_units.id IN (".Filters::convertJsonIntoArray2($filtered).") AND business_units.name LIKE '%".$searchInput."%')";
                $tables = $tables .",business_units";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.business_unit_id IS NULL OR org_structures.business_unit_id IS NOT NULL)";
            }

            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'divisions')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.division_id=divisions.id AND divisions.id IN (".Filters::convertJsonIntoArray2($filtered).") AND divisions.name LIKE '%".$searchInput."%')";
                $tables = $tables .",divisions";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.division_id IS NULL OR org_structures.division_id IS NOT NULL)";
            }
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'departments')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.department_id=departments.id AND departments.id IN (".Filters::convertJsonIntoArray2($filtered).") AND departments.name LIKE '%".$searchInput."%')";
                $tables = $tables .",departments";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.department_id IS NULL OR org_structures.department_id IS NOT NULL)";
            }


        }

        $query = "SELECT org_structures.*  FROM ".$tables." WHERE ".$whereCondition;
//        Log::info("Sql Satatement: ".$query);
        $res=DB::SELECT($query);
        return Filters::convertJsonIntoArray($res);

    }
    public static function myOrgWithCycleFiltration($searchInput,$skip,$take, $is_paginate, $is_live=1, $condition = ''){
        $user_id = Auth::user()->id;
        $cycle_id=Cycle::getLiveCycleID();
        //Log::emergency('Cycle Id: '. $cycle_id);
        //--Get Filters
        $filters = Filters::where('user_id', $user_id)->get(['id']);

        $org = OrgStructure::where('id', '>', 0);

        $tables = "user_cycles,employees,org_structures,participation_categories,schemes,target_mappings,target_business_units, bands, milestones, races, genders, statuses ";
        $whereCondition ="org_structures.id>0";
        $whereCondition = $whereCondition. " AND (employees.org_structure_id=org_structures.id)";
        $whereCondition = $whereCondition. " AND (employees.id=user_cycles.employee_id)";
        $whereCondition = $whereCondition. " AND (employees.band_id=bands.id)";
        $whereCondition = $whereCondition. " AND (employees.milestone_id=milestones.id)";
        $whereCondition = $is_live === -1 ? $whereCondition. "" : $whereCondition. " AND (user_cycles.is_live=".$is_live.")";
        $whereCondition = $whereCondition. " AND (user_cycles.cycle_id=".$cycle_id.")";
        $whereCondition = $whereCondition. " AND (employees.participation_category_id=participation_categories.id)";
        $whereCondition = $whereCondition. " AND (employees.scheme_id=schemes.id)";
        $whereCondition = $whereCondition. " AND (employees.target_mapping_id=target_mappings.id)";
        $whereCondition = $whereCondition. " AND (target_mappings.unit_target_business_unit_id1=target_business_units.id)";
        $whereCondition = $whereCondition. " AND (employees.race_id=races.id)";
        $whereCondition = $whereCondition. " AND (employees.band_id=bands.id)";
        $whereCondition = $whereCondition. " AND (employees.milestone_id=milestones.id)";
        $whereCondition = $whereCondition. " AND (employees.gender_id=genders.id)";
        $whereCondition = $whereCondition. " AND (employees.status_id=statuses.id)";
        $whereCondition = $whereCondition. $condition; // AND employees.status_id IN (1)
        $orWhereCondition = " AND (employees.firstnames LIKE '%".$searchInput."%' OR employees.lastname LIKE '%".$searchInput."%' OR employees.personnelnumber LIKE '%".$searchInput."%'";
        $orWhereCondition = $orWhereCondition." OR participation_categories.name LIKE '%".$searchInput."%' OR schemes.name LIKE '%".$searchInput."%' OR target_business_units.name LIKE '%".$searchInput."%'";
        if(count($filters)>0){

            //---Business area
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'business_areas')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.business_area_id=business_areas.id AND business_areas.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR business_areas.name LIKE '%".$searchInput."%'";
                $tables = $tables .",business_areas";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.business_area_id IS NULL OR org_structures.business_area_id IS NOT NULL)";
            }

            //---Business Units
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'business_units')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.business_unit_id=business_units.id AND business_units.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR business_units.name LIKE '%".$searchInput."%'";
                $tables = $tables .",business_units";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.business_unit_id IS NULL OR org_structures.business_unit_id IS NOT NULL)";
            }

            //---Divisions
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'divisions')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.division_id=divisions.id AND divisions.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR divisions.name LIKE '%".$searchInput."%'";
                $tables = $tables .",divisions";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.division_id IS NULL OR org_structures.division_id IS NOT NULL)";
            }

            //---Departments
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'departments')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.department_id=departments.id AND departments.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR departments.name LIKE '%".$searchInput."%'";
                $tables = $tables .",departments";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.department_id IS NULL OR org_structures.department_id IS NOT NULL)";
            }

            //---Manager
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'manager_name')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (employee_manager_mappings.employee_id=employees.id AND employee_manager_mappings.manager_employee_id IN (".Filters::convertJsonIntoArray2($filtered)."))";
//                $orWhereCondition = $orWhereCondition. " OR departments.name LIKE '%".$searchInput."%'";
                $tables = $tables .",employee_manager_mappings";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.department_id IS NULL OR org_structures.department_id IS NOT NULL)";
            }

            //---Participation Category
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'participation_categories')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (participation_categories.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
            }

            //---Scheme
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'schemes')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (schemes.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
            }

            //---Targert Business Unit
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'target_business_units')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (target_business_units.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
            }

        }

        $orWhereCondition = $orWhereCondition .")";
        $select = "employees.id,employees.org_structure_id, employees.firstnames, employees.lastname, employees.personnelnumber, employees.position, employees.email, employees.linemanager, employees.startdate";
        $select = $select.", participation_categories.name as participation_category_id, participation_categories.split_financial as financial, participation_categories.split_non_financial as non_financial";
        $select = $select.", schemes.name as scheme_name";
        $select = $select.", (SELECT business_areas.name FROM business_areas, org_structures WHERE org_structures.business_area_id=business_areas.id AND org_structures.business_area_id=(SELECT org_structures.business_area_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as business_areas";
        $select = $select.", (SELECT business_units.name FROM business_units, org_structures WHERE org_structures.business_unit_id=business_units.id AND org_structures.business_unit_id=(SELECT org_structures.business_unit_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as business_units";
        $select = $select.", (SELECT divisions.name FROM divisions, org_structures WHERE org_structures.division_id=divisions.id AND org_structures.division_id=(SELECT org_structures.division_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as divisions";
        $select = $select.", (SELECT departments.name FROM departments, org_structures WHERE org_structures.department_id=departments.id AND org_structures.department_id=(SELECT org_structures.department_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as department";
        $select = $select.", (SELECT employees.firstnames FROM employees, employee_manager_mappings WHERE employee_manager_mappings.employee_id=user_cycles.employee_id AND employee_manager_mappings.manager_employee_id=employees.id) as manager_firstname";
        $select = $select.", (SELECT employees.lastname FROM employees, employee_manager_mappings WHERE employee_manager_mappings.employee_id=user_cycles.employee_id AND employee_manager_mappings.manager_employee_id=employees.id) as manager_lastname";
        $select = $select.", user_cycles.notified_manager";
        $select = $select.", user_cycles.date_generated";
        $select = $select.", user_cycles.downloaded";
        $select = $select.", user_cycles.has_accepted";
        $select = $select.", user_cycles.date_accepted";
        $select = $select.", user_cycles.notified_participant";
        $select = $select.", bands.name as bands";
        $select = $select.", milestones.name as milestones";
        $select = $select.", bands.name as band";
        $select = $select.", races.name as race";
        $select = $select.", genders.name as gender";
        $select = $select.", statuses.name as status";

        $limit = "";
        if($is_paginate===true){
           $limit = $limit."LIMIT ".$skip.",".$take;
        }
        $query = "SELECT ".$select." FROM ".$tables." WHERE ".$whereCondition." ".$orWhereCondition." ".$limit;

        $res=DB::SELECT($query);

        $array = array();

        foreach ($res as $item) {

            $array[] = array(
                "id" => $item->id,
                "personnelnumber" => $item->personnelnumber,
                "firstnames" => $item->firstnames,
                "lastname" => $item->lastname,
                "linemanager" => $item->linemanager,
                "manager_name" => $item->manager_firstname . ' ' . $item->manager_lastname,
                "position" => $item->position,
                "email" => $item->email,
                "gender" => $item->gender,
                "race" => $item->race,
                "band" => $item->band,
                "milestones" => $item->milestones,
                "startdate" => $item->startdate,
                "business_areas" => $item->business_areas,
                "business_unit" => $item->business_units,
                "divisions" => $item->divisions,
                "department" => $item->department,
                "scheme_name" => $item->scheme_name,
                "participation_category_id" => $item->participation_category_id,
                "status" => $item->status,
                "targetb1" => TargetBusinessUnit::getTargertBunitOfEmployee($item->id, 0),
                "targetb2" => TargetBusinessUnit::getTargertBunitOfEmployee($item->id, 1),
                "targetb3" => TargetBusinessUnit::getTargertBunitOfEmployee($item->id, 2),
                "financial" => $item->financial,
                "non_financial" => $item->non_financial,
                "notified_manager" => $item->notified_manager === 1 ? "Yes" : "No",
                "date_generated" => $item->date_generated !== null ? "Yes" : "No",
                "my_date_generated" => $item->date_generated,
                "downloaded" => $item->downloaded === 1 ? "Yes" : "No",
                "has_accepted" => $item->has_accepted === 1 ? "Yes" : "No",
                "date_accepted" => $item->date_accepted,
                "notified_participant" => $item->notified_participant === 1 ? "Yes" : "No",
                "bands"=>$item->bands,
            );
        }

        return $array;

    }
    public static function myOrgWithCycleForReportsAndDashBoardFiltration($searchInput,$skip,$take, $is_paginate, $is_live=1, $condition = ''){
        Log::notice("Filter Inititian");

        $user_id = Auth::user()->id;
        $cycle_id=Cycle::getLiveCycleID();



        $org = OrgStructure::where('id', '>', 0);

        $tables = "user_cycles,employees,org_structures, bands, milestones";
        $whereCondition ="org_structures.id>0";
        $whereCondition = $whereCondition. " AND (employees.org_structure_id=org_structures.id)";
        $whereCondition = $whereCondition. " AND (employees.id=user_cycles.employee_id)";
        $whereCondition = $whereCondition. " AND (employees.status_id=1)";
        $whereCondition = $whereCondition. " AND (employees.band_id=bands.id)";
        $whereCondition = $whereCondition. " AND (employees.milestone_id=milestones.id)";
        $whereCondition = $is_live === -1 ? $whereCondition. "" : $whereCondition. " AND (user_cycles.is_live=".$is_live.")";
        $whereCondition = $whereCondition. " AND (user_cycles.cycle_id=".$cycle_id.")";
        $whereCondition = $whereCondition. $condition; // AND employees.status_id IN (1)
        $orWhereCondition = " AND (employees.firstnames LIKE '%".$searchInput."%' OR employees.lastname LIKE '%".$searchInput."%' OR employees.personnelnumber LIKE '%".$searchInput."%'";

        $filters = Filters::where('user_id', $user_id)->get(['id']);
        if(count($filters)>0){

            //---Business area
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'business_areas')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.business_area_id=business_areas.id AND business_areas.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR business_areas.name LIKE '%".$searchInput."%'";
                $tables = $tables .",business_areas";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.business_area_id IS NULL OR org_structures.business_area_id IS NOT NULL)";
            }

            //---Business Units
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'business_units')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.business_unit_id=business_units.id AND business_units.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR business_units.name LIKE '%".$searchInput."%'";
                $tables = $tables .",business_units";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.business_unit_id IS NULL OR org_structures.business_unit_id IS NOT NULL)";
            }

            //---Divisions
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'divisions')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.division_id=divisions.id AND divisions.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR divisions.name LIKE '%".$searchInput."%'";
                $tables = $tables .",divisions";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.division_id IS NULL OR org_structures.division_id IS NOT NULL)";
            }

            //---Departments
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'departments')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.department_id=departments.id AND departments.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR departments.name LIKE '%".$searchInput."%'";
                $tables = $tables .",departments";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.department_id IS NULL OR org_structures.department_id IS NOT NULL)";
            }

            //---Manager
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'manager_name')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (employee_manager_mappings.employee_id=employees.id AND employee_manager_mappings.manager_employee_id IN (".Filters::convertJsonIntoArray2($filtered)."))";
//                $orWhereCondition = $orWhereCondition. " OR departments.name LIKE '%".$searchInput."%'";
                $tables = $tables .",employee_manager_mappings";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.department_id IS NULL OR org_structures.department_id IS NOT NULL)";
            }

            //---Participation Category
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'participation_categories')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (employees.participation_category_id=participation_categories.id AND participation_categories.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR participation_categories.name LIKE '%".$searchInput."%'";
                $tables = $tables .",participation_categories";
                Log::notice("Participation Category");
            }else{
                $whereCondition = $whereCondition. " AND (employees.participation_category_id IS NULL OR employees.participation_category_id IS NOT NULL)";

            }

            //---Scheme
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'schemes')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (employees.scheme_id=schemes.id AND schemes.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR schemes.name LIKE '%".$searchInput."%'";
                $tables = $tables .",schemes";
            }else{
                $whereCondition = $whereCondition. " AND (employees.scheme_id IS NULL OR employees.scheme_id IS NOT NULL)";
            }


        }

        $orWhereCondition = $orWhereCondition .")";
        $select = "employees.id,employees.org_structure_id, employees.firstnames, employees.lastname, employees.personnelnumber, employees.position";
        $select = $select.", (SELECT participation_categories.name FROM participation_categories,employees WHERE employees.participation_category_id=participation_categories.id AND employees.id=user_cycles.employee_id LIMIT 0,1) as participation_category_id";
        $select = $select.", (SELECT participation_categories.split_financial FROM participation_categories,employees WHERE employees.participation_category_id=participation_categories.id AND employees.id=user_cycles.employee_id LIMIT 0,1) as financial";
        $select = $select.", (SELECT participation_categories.split_non_financial FROM participation_categories,employees WHERE employees.participation_category_id=participation_categories.id AND employees.id=user_cycles.employee_id LIMIT 0,1) as non_financial";
        $select = $select.", (SELECT schemes.name FROM schemes,employees WHERE employees.scheme_id=schemes.id AND employees.id=user_cycles.employee_id LIMIT 0,1) as scheme_name";
        $select = $select.", (SELECT business_areas.name FROM business_areas, org_structures WHERE org_structures.business_area_id=business_areas.id AND org_structures.business_area_id=(SELECT org_structures.business_area_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as business_areas";
        $select = $select.", (SELECT business_units.name FROM business_units, org_structures WHERE org_structures.business_unit_id=business_units.id AND org_structures.business_unit_id=(SELECT org_structures.business_unit_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as business_units";
        $select = $select.", (SELECT divisions.name FROM divisions, org_structures WHERE org_structures.division_id=divisions.id AND org_structures.division_id=(SELECT org_structures.division_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as divisions";
        $select = $select.", (SELECT departments.name FROM departments, org_structures WHERE org_structures.department_id=departments.id AND org_structures.department_id=(SELECT org_structures.department_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as department";
        $select = $select.", (SELECT employees.firstnames FROM employees, employee_manager_mappings WHERE employee_manager_mappings.employee_id=user_cycles.employee_id AND employee_manager_mappings.manager_employee_id=employees.id) as manager_firstname";
        $select = $select.", (SELECT employees.lastname FROM employees, employee_manager_mappings WHERE employee_manager_mappings.employee_id=user_cycles.employee_id AND employee_manager_mappings.manager_employee_id=employees.id) as manager_lastname";
        $select = $select.", user_cycles.notified_manager";
        $select = $select.", user_cycles.date_generated";
        $select = $select.", user_cycles.downloaded";
        $select = $select.", user_cycles.has_accepted";
        $select = $select.", user_cycles.date_accepted";
        $select = $select.", user_cycles.notified_participant";
        $select = $select.", bands.name as bands";
        $select = $select.", milestones.name as milestones";

        $limit = "";
        if($is_paginate===true){
           $limit = $limit."LIMIT ".$skip.",".$take;
        }
        $query = "SELECT ".$select." FROM ".$tables." WHERE ".$whereCondition." ".$orWhereCondition." ".$limit;

        $res=DB::SELECT($query);

        $array = array();

        foreach ($res as $item) {

            $array[] = array(
                "id" => $item->id,
                "personnelnumber" => $item->personnelnumber,
                "firstnames" => $item->firstnames,
                "lastname" => $item->lastname,
                "position" => $item->position,
                "manager_name" => $item->manager_firstname . ' ' . $item->manager_lastname,
                "scheme_name" => $item->scheme_name,
                "participation_category_id" => $item->participation_category_id,
                "financial" => $item->financial,
                "non_financial" => $item->non_financial,
                "business_areas" => $item->business_areas,
                "business_unit" => $item->business_areas,
                "divisions" => $item->divisions,
                "department" => $item->department,
                "targetb1" => TargetBusinessUnit::getTargertBunitOfEmployee($item->id, 0),
                "targetb2" => TargetBusinessUnit::getTargertBunitOfEmployee($item->id, 1),
                "targetb3" => TargetBusinessUnit::getTargertBunitOfEmployee($item->id, 2),
                "notified_manager" => $item->notified_manager === 1 ? "Yes" : "No",
                "date_generated" => $item->date_generated !== null ? "Yes" : "No",
                "my_date_generated" => $item->date_generated,
                "downloaded" => $item->downloaded === 1 ? "Yes" : "No",
                "has_accepted" => $item->has_accepted === 1 ? "Yes" : "No",
                "date_accepted" => $item->date_accepted,
                "notified_participant" => $item->notified_participant === 1 ? "Yes" : "No",
                "bands"=>$item->bands,
                "milestones"=>$item->milestones,
            );
        }

        return $array;

    }
    public static function myOrgWithCycleForDashBoardFiltration($searchInput,$skip,$take, $is_paginate, $is_live=1, $employees){
        $user_id = Auth::user()->id;
        $cycle_id=Cycle::getLiveCycleID();

        //--Get Filters
        $filters = Filters::where('user_id', $user_id)->get(['id']);

        $org = OrgStructure::where('id', '>', 0);

        $tables = "user_cycles,employees,org_structures,participation_categories,schemes,target_mappings,target_business_units, bands, milestones";
        $whereCondition ="org_structures.id>0";
        $whereCondition = $whereCondition. " AND (employees.org_structure_id=org_structures.id)";
        $whereCondition = $whereCondition. " AND (employees.id=user_cycles.employee_id)";
        $whereCondition = $whereCondition. " AND (employees.band_id=bands.id)";
        $whereCondition = $whereCondition. " AND (employees.milestone_id=milestones.id)";
        $whereCondition = $whereCondition. " AND (user_cycles.is_live=".$is_live.")";
        $whereCondition = $whereCondition. " AND (user_cycles.cycle_id=".$cycle_id.")";
        $whereCondition = $whereCondition. " AND (employees.participation_category_id=participation_categories.id)";
        $whereCondition = $whereCondition. " AND (employees.scheme_id=schemes.id)";
        $whereCondition = $whereCondition. " AND (employees.target_mapping_id=target_mappings.id)";
        $whereCondition = $whereCondition. " AND (target_mappings.unit_target_business_unit_id1=target_business_units.id)";
        $orWhereCondition = " AND (employees.firstnames LIKE '%".$searchInput."%' OR employees.lastname LIKE '%".$searchInput."%' OR employees.personnelnumber LIKE '%".$searchInput."%'";
        $orWhereCondition = $orWhereCondition." OR participation_categories.name LIKE '%".$searchInput."%' OR schemes.name LIKE '%".$searchInput."%' OR target_business_units.name LIKE '%".$searchInput."%'";
        if(count($filters)>0){

            //---Business area
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'business_areas')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.business_area_id=business_areas.id AND business_areas.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR business_areas.name LIKE '%".$searchInput."%'";
                $tables = $tables .",business_areas";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.business_area_id IS NULL OR org_structures.business_area_id IS NOT NULL)";
            }

            //---Business Units
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'business_units')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.business_unit_id=business_units.id AND business_units.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR business_units.name LIKE '%".$searchInput."%'";
                $tables = $tables .",business_units";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.business_unit_id IS NULL OR org_structures.business_unit_id IS NOT NULL)";
            }

            //---Divisions
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'divisions')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.division_id=divisions.id AND divisions.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR divisions.name LIKE '%".$searchInput."%'";
                $tables = $tables .",divisions";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.division_id IS NULL OR org_structures.division_id IS NOT NULL)";
            }

            //---Departments
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'departments')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (org_structures.department_id=departments.id AND departments.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
                $orWhereCondition = $orWhereCondition. " OR departments.name LIKE '%".$searchInput."%'";
                $tables = $tables .",departments";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.department_id IS NULL OR org_structures.department_id IS NOT NULL)";
            }

            //---Managers
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'manager_name')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (employee_manager_mappings.employee_id=employees.id AND employee_manager_mappings.manager_employee_id IN (".Filters::convertJsonIntoArray2($filtered)."))";
//                $orWhereCondition = $orWhereCondition. " OR departments.name LIKE '%".$searchInput."%'";
                $tables = $tables .",employee_manager_mappings";
            }else{
                $whereCondition = $whereCondition. " AND (org_structures.department_id IS NULL OR org_structures.department_id IS NOT NULL)";
            }

            //---Participation Category
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'participation_categories')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (participation_categories.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
            }

            //---Scheme
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'schemes')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (schemes.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
            }

            //---Targert Business Unit
            $filtered = Filters::where('user_id', $user_id)->where('reference_table', 'target_business_units')->get(['reference_table_id as id']);
            if(count($filtered)>0){
                $whereCondition = $whereCondition. " AND (target_business_units.id IN (".Filters::convertJsonIntoArray2($filtered)."))";
            }


        }

        $orWhereCondition = $orWhereCondition .")";
        $select = "employees.id,employees.org_structure_id, employees.firstnames, employees.lastname, employees.personnelnumber, employees.position";
        $select = $select.", participation_categories.name as participation_category_id";
        $select = $select.", schemes.name as scheme_name";
        $select = $select.", (SELECT business_areas.name FROM business_areas, org_structures WHERE org_structures.business_area_id=business_areas.id AND org_structures.business_area_id=(SELECT org_structures.business_area_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as business_areas";
        $select = $select.", (SELECT business_units.name FROM business_units, org_structures WHERE org_structures.business_unit_id=business_units.id AND org_structures.business_unit_id=(SELECT org_structures.business_unit_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as business_units";
        $select = $select.", (SELECT divisions.name FROM divisions, org_structures WHERE org_structures.division_id=divisions.id AND org_structures.division_id=(SELECT org_structures.division_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as divisions";
        $select = $select.", (SELECT departments.name FROM departments, org_structures WHERE org_structures.department_id=departments.id AND org_structures.department_id=(SELECT org_structures.department_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as department";
        $select = $select.", (SELECT employees.firstnames FROM employees, employee_manager_mappings WHERE employee_manager_mappings.employee_id=user_cycles.employee_id AND employee_manager_mappings.manager_employee_id=employees.id) as manager_firstname";
        $select = $select.", (SELECT employees.lastname FROM employees, employee_manager_mappings WHERE employee_manager_mappings.employee_id=user_cycles.employee_id AND employee_manager_mappings.manager_employee_id=employees.id) as manager_lastname";
        $select = $select.", user_cycles.notified_manager";
        $select = $select.", user_cycles.date_generated";
        $select = $select.", user_cycles.downloaded";
        $select = $select.", user_cycles.has_accepted";
        $select = $select.", user_cycles.notified_participant";
        $select = $select.", bands.name as bands";
        $select = $select.", milestones.name as milestones";

        $whereIn = "AND employees.id IN (".$employees.")";
        $limit = "";
        if($is_paginate===true){
           $limit = $limit."LIMIT ".$skip.",".$take;
        }
        $query = "SELECT ".$select." FROM ".$tables." WHERE ".$whereCondition." ".$whereIn." ".$orWhereCondition." ".$limit;

        $res=DB::SELECT($query);

        $array = array();

        foreach ($res as $item) {

            $array[] = array(
                "id" => $item->id,
                "personnelnumber" => $item->personnelnumber,
                "firstnames" => $item->firstnames,
                "lastname" => $item->lastname,
                "position" => $item->position,
                "manager_name" => $item->manager_firstname . ' ' . $item->manager_lastname,
                "scheme_name" => $item->scheme_name,
                "participation_category_id" => $item->participation_category_id,
                "business_areas" => $item->business_areas,
                "business_unit" => $item->business_areas,
                "divisions" => $item->divisions,
                "department" => $item->department,
                "targetb1" => TargetBusinessUnit::getTargertBunitOfEmployee($item->id, 0),
                "targetb2" => TargetBusinessUnit::getTargertBunitOfEmployee($item->id, 1),
                "notified_manager" => $item->notified_manager === 1 ? "yes" : "No",
                "date_generated" => $item->notified_manager !== null ? "yes" : "No",
                "downloaded" => $item->downloaded === 1 ? "yes" : "No",
                "has_accepted" => $item->has_accepted === 1 ? "yes" : "No",
                "notified_participant" => $item->notified_participant === 1 ? "yes" : "No",
                "bands"=>$item->bands,
                "milestones"=>$item->milestones,
            );
        }



        return $array;

    }
    public static function getAllEmployeesFilteredByOrgStructure($searchInput,$skip,$take, $is_paginate)
    {
        $emp = DB::table("employees")
            ->join("schemes", 'schemes.id', 'employees.scheme_id')
            ->join("participation_categories", 'participation_categories.id', 'employees.participation_category_id')
            ->join("org_structures", 'org_structures.id', 'employees.org_structure_id')
            ->join("business_areas", 'business_areas.id', 'org_structures.business_area_id')
            ->join("business_units", 'business_units.id', 'org_structures.business_unit_id')
            ->join("divisions", 'divisions.id', 'org_structures.division_id');

        //---work with Participation Category
        $participation =
        $emp ->where(function ($query) use($searchInput) {
                $query
                    ->Where('employees.lastname', 'like', '%' . $searchInput . '%')
                    ->orWhere('business_areas.name', 'like', '%' . $searchInput . '%')
                    ->orWhere('business_units.name', 'like', '%' . $searchInput . '%')
                    ->orWhere('divisions.name', 'like', '%' . $searchInput . '%');
            })

            ->select(
                DB::raw("employees.id,employees.org_structure_id, employees.firstnames, employees.lastname"),
                DB::raw("participation_categories.name as participation_category_id"),
                DB::raw("schemes.name as scheme_name"),
                DB::raw("business_areas.name as business_areas"),
                DB::raw("business_units.name as business_units"),
                DB::raw("divisions.name as divisions")
            );

        if($is_paginate===true){
            $emp->skip($skip)
                ->take($take);
        }

        $filtered_org = Filters::filterOrgStructure($searchInput);

        $array = array();
        foreach ($emp->get() as $item){
            if(Filters::searchArray($filtered_org, $item->org_structure_id)===true){
                $array[] = array(
                    "id"=>$item->id,
                    "firstnames"=>$item->firstnames,
                    "lastname"=>$item->lastname,
                    "scheme_name"=>$item->scheme_name,
                    "participation_category_id"=>$item->participation_category_id,
                    "business_areas"=>$item->business_areas,
                    "divisions"=>$item->divisions,
                );
            }

        }
        return $array;


    }
    public static function getAllEmployeesFilteredByOrgStructureAndCycleOptions($searchInput,$skip,$take, $is_paginate, $is_live=1){
        $cycle_id = Cycle::getLiveCycleID();
        $filtered_org = Filters::myOrgWithCycleFiltration($searchInput,$skip,$take, $is_paginate, $is_live=1);
//        $filtered_org = Filters::filterOrgStructure($searchInput);
        $emp = DB::table("employees")
            ->join("user_cycles", 'user_cycles.employee_id', 'employees.id')
            ->join("schemes", 'schemes.id', 'employees.scheme_id')
            ->join("participation_categories", 'participation_categories.id', 'employees.participation_category_id')
            ->join("target_mappings", 'target_mappings.id', 'employees.target_mapping_id')
            ->join("target_business_units", 'target_business_units.id', 'target_mappings.unit_target_business_unit_id1')
            ->join("org_structures", 'org_structures.id', 'employees.org_structure_id');

            //--participation category
                $participation = Filters::where('user_id', Auth::user()->id)->where('reference_table', 'participation_categories')->get(['reference_table_id as id']);
                if(count($participation)>0){
                    $emp->whereIn('participation_categories.id', Filters::convertJsonIntoArray($participation));
                }

            //--participation category
            $scheme = Filters::where('user_id', Auth::user()->id)->where('reference_table', 'schemes')->get(['reference_table_id as id']);
            if(count($scheme)>0){
                $emp->whereIn('schemes.id', Filters::convertJsonIntoArray($scheme));
            }
            //--target_business_units category
            $scheme = Filters::where('user_id', Auth::user()->id)->where('reference_table', 'target_business_units')->get(['reference_table_id as id']);
            if(count($scheme)>0){
                $emp->whereIn('target_business_units.id', Filters::convertJsonIntoArray($scheme));
            }

        $emp->whereIn('org_structures.id', $filtered_org)
            ->where('user_cycles.is_live',$is_live )
            ->where('user_cycles.cycle_id', $cycle_id)
            ->where(function ($query) use($searchInput) {
                $query
                    ->where('employees.lastname', 'like', '%' . $searchInput . '%')
                    ->orWhere('employees.firstnames', 'like', '%' . $searchInput . '%')
                    ->orWhere('schemes.name', 'like', '%' . $searchInput . '%')
                    ->orWhere('participation_categories.name', 'like', '%' . $searchInput . '%');


                    $query->orWhere('employees.personnelnumber', 'like', '%' . $searchInput . '%');

            })

            ->select(
                DB::raw("employees.id,employees.org_structure_id, employees.firstnames, employees.lastname, employees.personnelnumber, employees.position"),
                DB::raw("participation_categories.name as participation_category_id"),
                DB::raw("schemes.name as scheme_name"),

                DB::raw("(SELECT business_areas.name FROM business_areas, org_structures WHERE org_structures.business_area_id=business_areas.id AND org_structures.business_area_id=(SELECT org_structures.business_area_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as business_areas"),
                DB::raw("(SELECT business_units.name FROM business_units, org_structures WHERE org_structures.business_unit_id=business_units.id AND org_structures.business_unit_id=(SELECT org_structures.business_unit_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as business_units"),
                DB::raw("(SELECT divisions.name FROM divisions, org_structures WHERE org_structures.division_id=divisions.id AND org_structures.division_id=(SELECT org_structures.division_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as divisions"),
                DB::raw("(SELECT departments.name FROM departments, org_structures WHERE org_structures.department_id=departments.id AND org_structures.department_id=(SELECT org_structures.department_id FROM employees, org_structures WHERE employees.org_structure_id=org_structures.id AND employees.id=user_cycles.employee_id) LIMIT 0,1) as department"),

                DB::raw("(SELECT employees.firstnames FROM employees, employee_manager_mappings WHERE employee_manager_mappings.employee_id=user_cycles.employee_id AND employee_manager_mappings.manager_employee_id=employees.id) as manager_firstname"),
                DB::raw("(SELECT employees.lastname FROM employees, employee_manager_mappings WHERE employee_manager_mappings.employee_id=user_cycles.employee_id AND employee_manager_mappings.manager_employee_id=employees.id) as manager_lastname"),
                DB::raw("user_cycles.notified_manager"),
                DB::raw("user_cycles.date_generated"),
                DB::raw("user_cycles.downloaded"),
                DB::raw("user_cycles.has_accepted"),
                DB::raw("user_cycles.notified_participant")
            );

        if($is_paginate===true){
            $emp->skip($skip)
                ->take($take);
        }


        $emp_data=$emp->get();
        $array = array();

        foreach ($emp_data as $item) {

            $array[] = array(
                "id" => $item->id,
                "personnelnumber" => $item->personnelnumber,
                "firstnames" => $item->firstnames,
                "lastname" => $item->lastname,
                "position" => $item->position,
                "manager_name" => $item->manager_firstname . ' ' . $item->manager_lastname,
                "scheme_name" => $item->scheme_name,
                "participation_category_id" => $item->participation_category_id,
                "business_areas" => $item->business_areas,
                "business_unit" => $item->business_areas,
                "divisions" => $item->divisions,
                "department" => $item->department,
                "targetb1" => TargetBusinessUnit::getTargertBunitOfEmployee($item->id, 0),
                "targetb2" => TargetBusinessUnit::getTargertBunitOfEmployee($item->id, 1),
                "notified_manager" => $item->notified_manager === 1 ? "yes" : "No",
                "date_generated" => $item->notified_manager !== null ? "yes" : "No",
                "downloaded" => $item->downloaded === 1 ? "yes" : "No",
                "has_accepted" => $item->has_accepted === 1 ? "yes" : "No",
                "notified_participant" => $item->notified_participant === 1 ? "yes" : "No",
            );
        }



        return $array;

    }
    public static function convertJsonIntoArray($json){
        $array = [];
        foreach ($json as $data){
            $array[] = $data->id;
        }
        return $array;
    }
    public static function convertJsonIntoArray2($json){

        $array = '';
        $count = 0;
        foreach ($json as $data){
            if($count===0){
                $array = $array. ''.$data->id;
            }else{
                $array = $array. ','.$data->id;
            }
            $count++;

        }
        return $array;
    }
    public static function convertJsonIntoArrayIndex2($json){
        $array = '';
        $count = 0;
        foreach ($json as $data){
            if($count===0){
                $array = $array. ''.$data['id'];
            }else{
                $array = $array. ','.$data['id'];
            }
            $count++;

        }
        return $array;
    }
    public static function convertJsonIntoArrayIndex($json){
        if(count($json)>0){
            $array = [];
            foreach ($json as $data){
                $array[] = $data['id'];
            }
            return $array;
        }else{
            return [];
        }


    }
    public static function searchArray($array, $value){
        foreach ($array as $item){
            if($item===$value){
                return true;
            }
        }
        return false;
    }





    public static function filterEmployeeColumnIDsPerManager($table_name = null, $column = null, $use_active_instance_id = true){

        try{
            $cycle_id           = Cycle::getLiveCycleID(); //todo: update cycle id
            $user               = Auth::user();
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $collection         = null;
            $manager_pn         = $user->personnelnumber;
            $table_name         = $table_name === null ? 'vw_employees_export' : $table_name;

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])) {

                $collection = DB::table('vw_employees_export')
                    ->where('instance_indicator_id',     $active_instance_id)
                    ->where('cycle_id',     $cycle_id);
                $collection->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)

                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                        ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

                });
            }
            else{
                $collection = DB::table('vw_employees_export')
                    ->where('cycle_id',     $cycle_id);
                /*if ($use_active_instance_id)
                    $collection =  $collection->where('instance_indicator_id',     $active_instance_id);*/
            }
            $collection = Filters::applyFilters($collection, $user);
            /*if (in_array($column , ['allocation_manager_id', 'business_manager_id', 'business_area_manager_id'])){
                $tem_col = substr($column, 0, -2);
//                $collection =  $collection->select('allocation_manager_1', 'allocation_manager_2', 'allocation_manager_3', 'allocation_manager_4', 'allocation_manager_5', 'allocation_manager_6')->get();
                $collection =  $collection->select($tem_col.'1', $tem_col.'2', $tem_col.'3', $tem_col.'4', $tem_col.'5', $tem_col.'6')->get();
            }*/
            if ($column ==='allocation_manager_id') {
                $collection =  $collection->select('allocation_manager_1', 'allocation_manager_2', 'allocation_manager_3', 'allocation_manager_4', 'allocation_manager_5', 'allocation_manager_6')->get();
            }
            else if($column === 'business_manager_id'){
                $collection =  $collection->select('business_manager_1', 'business_manager_2', 'business_manager_3', 'business_manager_4', 'business_manager_5', 'business_manager_6')->get();
            }
           else if($column === 'business_area_manager_id'){
               $collection =  $collection->select('business_area_manager_1', 'business_area_manager_2', 'business_area_manager_3', 'business_area_manager_4', 'business_area_manager_5', 'business_area_manager_6')->get();
           }

           else
                $collection =  $collection->pluck($column)->toArray();

            return $collection;
        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }

    public static function applyFilters( $collection, $user){

        $countries_ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'countries')->pluck('reference_table_id as id')->toArray();
        if(count($countries_ids)>0){
            $collection = $collection->whereIn('country_id', $countries_ids);
        }

        $business_area_ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'business_areas')->pluck('reference_table_id as id')->toArray();
        if(count($business_area_ids)>0){
            $collection = $collection->whereIn('business_area_id', $business_area_ids);
        }

        $business_unit_ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'business_units')->pluck('reference_table_id as id')->toArray();
        if(count($business_unit_ids)>0){
            $collection = $collection->whereIn('business_unit_id', $business_unit_ids);
        }

        $division_ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'divisions')->pluck('reference_table_id as id')->toArray();
        if(count($division_ids)>0){
            $collection = $collection->whereIn('division_id', $division_ids);
        }

        $race_ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'races')->pluck('reference_table_id as id')->toArray();
        if(count($race_ids)>0){
            $collection = $collection->whereIn('race_id', $race_ids);
        }

        $current_rating_ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'performance_ratings')->pluck('reference_table_id as id')->toArray();
        if(count($current_rating_ids)>0){
            $collection = $collection->whereIn('current_rating_id', $current_rating_ids);
        }
        $ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'genders')->pluck('reference_table_id as id')->toArray();
        if(count($ids)>0){
            $collection = $collection->whereIn('gender_id', $ids);
        }
        $ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'bands')->pluck('reference_table_id as id')->toArray();
        if(count($ids)>0){
            $collection = $collection->whereIn('band_id', $ids);
        }
        $ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'schemes')->pluck('reference_table_id as id')->toArray();
        if(count($ids)>0){
            $collection = $collection->whereIn('scheme_id', $ids);
        }

        $ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'statuses')->pluck('reference_table_id as id')->toArray();
        if(count($ids)>0){
            $collection = $collection->whereIn('status_id', $ids);
        }
        else{
            $collection = $collection->whereIn('status_id', [1,19]); // by default get active employees and active-scp employees
        }

        $ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'allocation_managers')->pluck('reference_table_id as id')->toArray();
        if(count($ids)>0){
            $collection = $collection
                ->where(function($query) use ($ids){
                    $query
                        ->whereIn('allocation_manager_1',  $ids)->orWhereIn('allocation_manager_2', $ids)->orWhereIn('allocation_manager_3', $ids)
                        ->orWhereIn('allocation_manager_4', $ids)->orWhereIn('allocation_manager_5', $ids)->orWhereIn('allocation_manager_6', $ids);
                });
        }

        $ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'business_managers')->pluck('reference_table_id as id')->toArray();
        if(count($ids)>0){
            $collection = $collection
                ->where(function($query) use ($ids){
                    $query
                        ->whereIn('business_manager_1',  $ids)->orWhereIn('business_manager_2', $ids)->orWhereIn('business_manager_3', $ids)
                        ->orWhereIn('business_manager_4', $ids)->orWhereIn('business_manager_5', $ids)->orWhereIn('business_manager_6', $ids);
                });
        }

        $ids = DB::table('filters')->where('user_id', $user->id)->where('reference_table', 'business_area_managers')->pluck('reference_table_id as id')->toArray();
        if(count($ids)>0){
            $collection = $collection
                ->where(function($query) use ($ids){
                    $query
                        ->whereIn('business_area_manager_1',  $ids)->orWhereIn('business_area_manager_2', $ids)->orWhereIn('business_area_manager_3', $ids)
                        ->orWhereIn('business_area_manager_4', $ids)->orWhereIn('business_area_manager_5', $ids)->orWhereIn('business_area_manager_6', $ids);
                });
        }

        return $collection;
    }

    public static function createStatusFilters($request){
        //delete all status filters and store new ones
        $filtration_statuses = Filters::where('reference_table', 'statuses')->where('user_id', Auth::user()->id)->get();
        foreach ($filtration_statuses as $filtration_status){
            $filtration_status->delete();
        }

        foreach ($request->filter_ids as $filter_id){
            $res = Filters::createFilter($request->type, $filter_id);
        }
    }

    /**
     *  we need to identify the active status_id state based on the filters to facilitate toggling of active tab(s)
     * @return int[]
     */
    public static function getStatusFilters(){
        // we need to identify the active status_id state based on the filters to facilitate toggling of active tab(s)
        $status_filters = Filters::where('reference_table', 'statuses')->where('user_id', Auth::user()->id)->pluck('reference_table_id')->toArray();
        if (count($status_filters) === 0){
            $status_filters = [1,19];
        }

        return $status_filters;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OccupationalLevel extends Model
{
    protected $table = 'occupational_levels';
}

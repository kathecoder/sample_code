<?php


namespace App\Models;


use App\CFLibrary\FilterHandler;
use App\Exports\CustomExport;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;

class AppExport
{
    public static function buildExport(string $table_name, array $exclude_fields, $where = null, $read_table = null){
        ini_set('memory_limit',-1);
        try{
            $active_instance = PermissionController::getUserInstance()['response']['instance'];
            //$record_ids      = FilterHandler::getFilteredData($table_name, Auth::user()->id);
            $manager_pn      = Auth::user()->personnelnumber;
            $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];

            //Todo: check if the tables exists before processing
            $view_table_name = "vw_". $table_name."_export";

            $data = [];

            //$status = $where['status_id'];
            $records = DB::table('vw_employees_export')/*->where('status_id', $status)*/;
            $records = Filters::applyFilters($records, Auth::user());

            $page = ($where !== null && isset($where['my_page'])) ? $where['my_page'] : '';

            if ( in_array($user_roles['accounttype'], [1, 2, 3]) && ($page == 'personnel_admin')){
                if(!in_array(Phase::getLivePhaseID(), [2,7,12])){
                    $headers = DataField::getExportFields();
                    //logger($headers);
                }
                else{
                    $headers = Employee::$admin_headers_sv;
                }
            }

            elseif ( in_array($user_roles['accounttype'], [1, 2, 3]) && ($page != 'personnel_admin')){

                $records = $records->where('instance_indicator_id',  $active_instance);
                //check if we are not in structural validation then override the hearders/fields
                if(!in_array(Phase::getLivePhaseID(), [2,7,12])){
                    $headers = DataField::getExportFields();
                }
                else{
                    $headers = Employee::$admin_headers_sv;
                }
            }

            elseif ($active_instance == 1){
                $records = $records->where('instance_indicator_id',  $active_instance);

                if(!in_array(Phase::getLivePhaseID(), [2,7,12])){
                    $headers = DataField::getExportFields();
                }
                else{
                    $headers = Employee::$group_headers_sv;
                }
            }

            elseif ($active_instance == 2){
                $records = $records->where('instance_indicator_id',  $active_instance);
                if(!in_array(Phase::getLivePhaseID(), [2,7,12])){
                    $headers = DataField::getExportFields();
                }
                else{
                    $headers = Employee::$africa_headers_sv;
                }
            }

            elseif ($active_instance == 3){
                $records = $records->where('instance_indicator_id',  $active_instance);
                if(!in_array(Phase::getLivePhaseID(), [2,7,12])){
                    $headers = DataField::getExportFields();
                }
                else{
                    $headers = Employee::$stanlib_headers_sv;
                }
            }

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){

                if(in_array(Phase::getLivePhaseID(), [2,7,12])){ // On structural validation
                    $records = $records->where(function ($q) use($manager_pn) {
                        $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                            ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn);
                    });
                }
                else{
                    $records = $records->where(function ($q) use($manager_pn) {
                        $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                            ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)

                            ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                            ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                            ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                            ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                            ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                            ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                            ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                            ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                            ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                            ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

                    });
                }
            }

            $imploded_strings = implode("','", $headers);
            $data_field_ids = DataField::whereIn('name', $headers)
                ->orderByRaw(DB::raw("FIELD(name, '$imploded_strings')"))
                ->pluck('id')->toArray();

            $imploded_strings = implode("','", $data_field_ids);
            $headings = DataFieldLabel::whereIn('data_field_id', $data_field_ids)
                ->where('instance_indicator_id', $active_instance)
                ->orderByRaw(DB::raw("FIELD(data_field_id, '$imploded_strings')"))
                ->pluck('label')->toArray();

            //$records = $records->select($headers)->get();

           // DB::enableQueryLog();

            $records = $records->select($headers)->get();
            //logger(DB::getQueryLog());

            foreach ($records as $record){

                foreach ($headers as $header){
                    if(in_array($header, ['h_1_on_target_achievement', 'h_1_on_target_achievement'])){
                        $record->{$header}  = round($record->{$header}, 0);
                    }
                }

                $data [] = (array) $record;
            }

            $export =  new CustomExport($data, $headings);
            //logger($export);
            return array('error' => false, 'response' => $export, 'message' => "Successfully built the export.");
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }


    public static function buildExportOld(string $table_name, array $exclude_fields, $where = null, $read_table = null){
        ini_set('memory_limit',-1);
        try{
            $headings        = [];
            $fields          = [];
            $active_instance = PermissionController::getUserInstance()['response']['instance'];
            //$read_fields     = $read_table === null ? AppAssist::getUniFieldWrites($table_name, $exclude_fields, true) : AppAssist::getUniFieldWrites($read_table, $exclude_fields, true);
            $record_ids      = FilterHandler::getFilteredData($table_name, Auth::user()->id);
            $manager_pn      = Auth::user()->personnelnumber;
            $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
/*
            foreach ($read_fields as $key => $read_field){
                $headings[] = $read_field->label;
                $fields[]   = $key;
            }*/

            //Todo: check if the tables exists before processing
            $view_table_name = "vw_". $table_name."_export";

            $data = [];

            //logger('Record IDS: ' .  count($record_ids));
            $status = 1;
            $records = DB::table($view_table_name)->where('status_id', $status);
            $records = Filters::applyFilters($records, Auth::user());

            $page = ($where !== null && isset($where['my_page'])) ? $where['my_page'] : '';
            if (count($record_ids) > 0){
                $records = $records->whereIn('id',  $record_ids);
            }
            if ( in_array($user_roles['accounttype'], [1, 2, 3]) && ($page == 'personnel_admin')){
                $headers = Employee::$admin_headers_sv;
            }
            elseif ( in_array($user_roles['accounttype'], [1, 2, 3]) && ($page != 'personnel_admin')){
                $headers = Employee::$admin_headers_sv;
                $records = $records->where('instance_indicator_id',  $active_instance);
            }
            elseif ($active_instance == 1){
                $headers = Employee::$group_headers_sv;
                $records = $records->where('instance_indicator_id',  $active_instance);
            }

            elseif ($active_instance == 2){
                $headers = Employee::$africa_headers_sv;
                $records = $records->where('instance_indicator_id',  $active_instance);
            }

            elseif ($active_instance == 3){
                $headers = Employee::$stanlib_headers_sv;
                $records = $records->where('instance_indicator_id',  $active_instance);
            }


            //todo: adapt such that it takes into consideration other phases
            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $records = $records->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn);

                });
            }


            $imploded_strings = implode("','", $headers);
            $data_field_ids = DataField::whereIn('name', $headers)
                ->orderByRaw(DB::raw("FIELD(name, '$imploded_strings')"))
                ->pluck('id')->toArray();

            $imploded_strings = implode("','", $data_field_ids);
            $headings = DataFieldLabel::whereIn('data_field_id', $data_field_ids)
                ->where('instance_indicator_id', $active_instance)
                ->orderByRaw(DB::raw("FIELD(data_field_id, '$imploded_strings')"))
                ->pluck('label')->toArray();

            $records = $records->select($headers)->get();
            foreach ($records as $record){

                $data [] = (array) $record;
            }

            $export =  new CustomExport($data, $headings);
            return array('error' => false, 'response' => $export, 'message' => "Successfully built the export.");
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    /**
     * Todo: update the function to work Export Management Page (To be added later)
     * The function should receive post request from the "Export Management Page - or whatever we decide to call it"
     * The "Export Management Page" should post  $table_name and $exclude_fields, amongst other fields
     * @param $table_name
     * @param $where_clause
     * @return array
     */
    public static function storeExport($table_name, $where = null, $read_table=null){
        ini_set('memory_limit',-1);
        try{
            //$table_name = $request->input('table_name');
            //$exclude_fields = $request->input('exclude_fields'); show tables like "test1"
            $exclude_fields = ['id','cycle_id', 'calc_change_structure', 'benchmark_id', 'calc_years_of_service', 'calc_actual_pool_specialist', 'calc_total_incentive_budget'];
            $res            = AppExport::buildExport( $table_name,  $exclude_fields, $where, null);
            //logger($res);
            if ($res['error'] === false){
                $export = $res['response'];
                $file_name      = "/public/spreadsheet/Exports/$table_name/$table_name" . '_export_'.date('YmdHis').'.xlsx';
                Excel::store($export, $file_name);
                $response = array('export' => $export, 'file_path' => base64_encode($file_name));
                return array('error' => false, 'response' => $response, 'message' => "Successfully stored export file");

            }
            else{
                return $res;
            }

        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
}
<?php

namespace App\Models;

use App\Http\Controllers\MailController;
use App\Http\Controllers\PermissionController;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use PhpParser\Node\Stmt\Foreach_;


class IssueLog extends Model
{
    protected $table = 'issue_logs';

    public static function resolveIssueLog($id, $comment, $status){
        $issue = IssueLog::find($id);
        $issue->status_id= $status;
        $issue->attended_by= Auth::user()->id;
        $issue->comment= $comment;
        if($status===14){
            $issue->date_closed= Carbon::now()->toDateTimeString();
        }
        $issue->save();

        if($status===14){
            //Send Emails to user whomlogged issue
            $issue = IssueLog::find($id);
            $logger=User::find($issue->user_id);
            $body = "Dear ".$logger->name.",<br /><br />";
            $body = $body . "Issue Logged: ".issueLogsSubCategory::find($issue->issue_sub_category_id)->name." (".$issue->ref.")<br /><br />";
            $body = $body . "Status: Closed.<br /><br />";
            $body = $body . "Comment: ".$comment.".<br /><br />";
            $body = $body . "Thank you for your commitment in this process.<br /><br />";
            $body = $body . "Kind Regards,<br />Group Reward";

            //MailController::sendIssueLogEmail($logger->email, $body, "Your issue has been closed on the Scheme Contracting Issue Log");
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $email = Email::where('instance_id', $active_instance_id)->where('name', 'Issue Log Email')->first();
            $sendTo = $logger->email;
            $tags = array(
                '%NAME%'        => $logger->name,
                //'%IssueLogged%' => issueLogsSubCategory::find($issue->issue_sub_category_id)->name." " .($issue->ref),
                '%IssueLogged%' => $issue->subject,
                '%feedback%'    => $comment,
            );
            MailController::sendNotification( $email, $sendTo, $tags);
        }
        return true;

    }
    public static function createUpdateIssueLog($id, $subject, $issue_category_id, $issue_sub_category_id, $date_opened, $issue_text)
    {
        if($id>0){
            $issue = IssueLog::find($id);
        }else{
            $issue = new IssueLog();
            $issue->cycle_id=Cycle::getLiveCycleID();
        }

        $issue->user_id= Auth::user()->id;
        $issue->subject= $subject;
        $issue->issue_category_id= $issue_category_id;
        $issue->issue_sub_category_id= $issue_sub_category_id;
        $issue->date_opened= $date_opened;
        $issue->issue = $issue_text;
        $issue->status_id= 12;
        $issue->save();

        return $issue->id;
    }
    public static function createUpdateIssueLogWithExtraDetails($state)
    {
        if($state['id']>0){
            $issue = IssueLog::find($state['id']);
        }else{
            $issue = new IssueLog();
            $issue->cycle_id=Cycle::getLiveCycleID();
            loop:
            $ref = IssueLog::getToken(5);
            if(IssueLog::checkToken($ref)===false){
                goto loop;
            }else{
                $issue->ref= $ref;
                //Log::notice('Issue Log ref: '.$ref);
            }


        }


        $issue->user_id= Auth::user()->id;
        $issue->subject= $state['subject'];
        $issue->issue_category_id= $state['issue_category_id'];
        $issue->issue_sub_category_id= $state['issue_sub_category_id'];
        $issue->date_opened= Carbon::now()->toDateTimeString();
        $issue->issue = $state['issue'];
        $issue->status_id= 12;
        $issue->save();

        if($issue->id>0){
            //handle file upload
           // Log::notice('Issue Log created, id='.$issue->id);
            $file_path = Files::handleFileUpload('/issueLogs/', $state['file']);
            Files::createOrUpdateFile(null,$issue->id, 'issue_logs', $file_path);

            //Send Emails to all Admin
            $logger=User::find(Auth::user()->id);

            foreach (User::where('admin', 1)->get() as $item){
                //Log::notice('Issue Log Email sent to this admin: '.$item->name);
                $body = "Dear <b>".$item->name."</b>,<br /><br />".$logger->name." added a new issue on the Issue Log.<br /><br />";
                $body = $body . "To access the issue, click on the <a href='".url('/')."'>Scheme Contracting System</a> and log in using your windows/network credentials.<br /><br />";
                $body = $body . "Thank you for your commitment in this process.<br /><br />";
                $body = $body . "Kind Regards,<br />Group Reward";
                //MailController::sendIssueLogEmail($item->email, $body, "New issue logged on the Scheme Contracting Issue Log");

                $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
                $email = Email::where('instance_id', $active_instance_id)->where('name', 'New IssueLog')->first();
                $sendTo = $item->email;
                $tags = array(
                    '%NAME%'        => $item->name,
                    '%HRBP%'        => $logger->name,

                );
                MailController::sendNotification( $email, $sendTo, $tags);
            }

//            "@ckeditor/ckeditor5-build-decoupled-document": "^20.0.0",
//        "@ckeditor/ckeditor5-editor-decoupled": "^20.0.0",

            // Log::notice("Done Creating Issue Log");
            return IssueExtraDetailsFromUser::createOrUpdate($issue->id,$state);
        }else{
            return true;
        }

    }
    public static function getToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $token;
    }
    public static function checkToken($ref)
    {
        $res = IssueLog::where('ref', $ref)->get();

        if(count($res)>0){
            return false;
        }else{
            return true;
        }
    }
    public static function getIssueLogs($searchInput,$skip,$take, $is_paginate, $status)
    {
        //Log::notice('User Role, '.Auth::user()->is_admin);

        $res=DB::table('issue_logs')
            ->join('users', 'users.id', 'issue_logs.user_id')
            ->join('statuses', 'statuses.id', 'issue_logs.status_id')
            ->join('issue_sub_categories', 'issue_sub_categories.id', 'issue_logs.issue_sub_category_id')
            ->where('cycle_id', Cycle::getLiveCycleID());

        if(Auth::user()->admin==0){
            $res->where('users.id', Auth::user()->id);
        }

        if($status>0){
            $res->where('statuses.id', $status);
        }


           $res ->where(function($query) use($searchInput){
                $query->where('users.name', 'like', '%'.$searchInput.'%')
                    ->orWhere('issue_logs.subject', 'like', '%'.$searchInput.'%')
                    ->orWhere('statuses.name', 'like', '%'.$searchInput.'%')
                    ->orWhere('issue_logs.issue', 'like', '%'.$searchInput.'%');
            });
        $res->select(
            DB::raw("issue_logs.*"),
            DB::raw("users.name"),
            DB::raw("statuses.name as status_name"),
            DB::raw("issue_sub_categories.name as issue_sub_categories")
        );
        $res->orderByDesc('created_at');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }

        return $res->get();
    }
    public static function getFilteredIssueLogs($searchInput,$skip,$take, $is_paginate, $status, $clause = "",$startDate,$endDate,$selected)
    {
        $sql = "SELECT 
                        issue_logs.*,
                        users.name,
                        statuses.name as status_name, 
                        issue_sub_categories.name as issue_sub_categories
                FROM 
                    issue_logs,
                    users,
                    statuses,
                    issue_sub_categories
                WHERE 
                    issue_logs.status_id=statuses.id 
                AND 
                    issue_logs.cycle_id=".Cycle::getLiveCycleID()."
                AND issue_logs.user_id=users.id
                AND issue_logs.issue_sub_category_id=issue_sub_categories.id
                and statuses.id in (12,13,14)
                ".$clause;

        if($status>0){
            $sql=$sql." AND statuses.id=$status";
        }


        $filter1 = Filters::where('user_id', Auth::user()->id)->where('reference_table','issue_category')->get(['reference_table_id as id']);
        if(count($filter1)>0){

            $sql=$sql." AND issue_logs.issue_sub_category_id IN (".Filters::convertJsonIntoArray2($filter1).")";
        }

        $filter = Filters::where('user_id', Auth::user()->id)->where('reference_table','logged_by')->get(['reference_table_id as id']);
        if(count($filter)>0){
            $sql=$sql." AND issue_logs.user_id IN (".Filters::convertJsonIntoArray2($filter).")";
        }

        $filter = Filters::where('user_id', Auth::user()->id)->where('reference_table','date')->get();
        if(count($filter)>0){
            $sql=$sql." AND issue_logs.date_opened BETWEEN '".$filter[0]->created_at."' AND '".$filter[0]->updated_at."'";
        }

        if ($startDate!==null){ 
            $sql=$sql." AND( issue_logs.date_opened >= Date('".$startDate."') AND issue_logs.date_opened <= Date('".$endDate."')) ";
 
      }
      
        if ($selected !==null && $selected !=="all"){ 
            $sql=$sql." AND( issue_logs.issue_category_id = '".$selected."' ) ";
 
      }

        $sql=$sql." AND (users.name LIKE '%$searchInput%' OR issue_sub_categories.name LIKE '%$searchInput%' OR issue_logs.subject LIKE '%$searchInput%' OR statuses.name LIKE '%$searchInput%' OR issue_logs.issue LIKE '%$searchInput%')";
        $sql=$sql." ORDER BY date_opened DESC ";

        if($is_paginate===true){
            $sql=$sql."LIMIT $skip,$take";
        }
        //Log::info($sql);
        $res=DB::SELECT($sql);






        
        // $rutu = $records->get(); 
        // logger(DB::getQueryLog());
        // return $rutu;

        if(is_array($res)){
            return $res;
        }else{
            return [];
        }

    }
    public static function getIssueLogsDash()
    {
        $sql = "SELECT 
                        issue_logs.*,
                        users.name,
                        statuses.name as status_name, 
                        issue_sub_categories.name as issue_sub_categories
                FROM 
                    issue_logs,
                    users,
                    statuses,
                    issue_sub_categories
                WHERE 
                    issue_logs.status_id=statuses.id 
                AND 
                    issue_logs.cycle_id=".Cycle::getLiveCycleID()."
                AND issue_logs.user_id=users.id
                AND issue_logs.issue_sub_category_id=issue_sub_categories.id
                and statuses.id in (12,13,14)
                ";

        // if($status>0){
        //     $sql=$sql." AND statuses.id=$status";
        // }


        $filter1 = Filters::where('user_id', Auth::user()->id)->where('reference_table','issue_category')->get(['reference_table_id as id']);
        if(count($filter1)>0){

            $sql=$sql." AND issue_logs.issue_sub_category_id IN (".Filters::convertJsonIntoArray2($filter1).")";
        }

        $filter = Filters::where('user_id', Auth::user()->id)->where('reference_table','logged_by')->get(['reference_table_id as id']);
        if(count($filter)>0){
            $sql=$sql." AND issue_logs.user_id IN (".Filters::convertJsonIntoArray2($filter).")";
        }

        $filter = Filters::where('user_id', Auth::user()->id)->where('reference_table','date')->get();
        if(count($filter)>0){
            $sql=$sql." AND issue_logs.date_opened BETWEEN '".$filter[0]->created_at."' AND '".$filter[0]->updated_at."'";
        }

    //     if ($startDate!==null){ 
    //         $sql=$sql." AND( issue_logs.date_opened >= Date('".$startDate."') AND issue_logs.date_opened <= Date('".$endDate."')) ";
 
    //   }
      
    //     if ($selected !==null && $selected !=="all"){ 
    //         $sql=$sql." AND( issue_logs.issue_category_id = '".$selected."' ) ";
 
    //   }

        // $sql=$sql." AND (users.name LIKE '%$searchInput%' OR issue_sub_categories.name LIKE '%$searchInput%' OR issue_logs.subject LIKE '%$searchInput%' OR statuses.name LIKE '%$searchInput%' OR issue_logs.issue LIKE '%$searchInput%')";
        $sql=$sql." ORDER BY date_opened DESC ";

        // if($is_paginate===true){
        //     $sql=$sql."LIMIT $skip,$take";
        // }
        //Log::info($sql);
        $res=DB::SELECT($sql);






        
        // $rutu = $records->get(); 
        // logger(DB::getQueryLog());
        // return $rutu;

        if(is_array($res)){
            return $res;
        }else{
            return [];
        }

    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FranchiseOneLevels extends Model
{
    protected $table = "franchise_one_levels";
}

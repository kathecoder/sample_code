<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManagerType extends Model
{
    protected $table = 'manager_types';
}

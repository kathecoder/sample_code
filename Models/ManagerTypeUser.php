<?php

namespace App\Models;

use App\Http\Controllers\UserRolesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ManagerTypeUser extends Model
{
    protected $table = 'manager_type_users';

    public static function filterDatasetManagerType($records, $user_roles){
        $manager_pn         = Auth::user()->personnelnumber;

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                    ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }
        return $records;
    }

    public static function isHCBP($manager_pn, $active_instance){

        $records            = DB::table('vw_employees_export');
        $records            = $records->whereIn('status_id',  [1,7,19]);
        $records            = $records->where('instance_indicator_id',  $active_instance);

        $records = $records->where(function ($q) use($manager_pn) {
            $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn);

        });

        if (count($records->get()) > 0){
            return true;
        }
        else{
            return false;
        }

    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeDuplicateStatus extends Model
{
    protected $table = 'vw_employees_duplicate';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BusinessArea extends Model
{
    protected $table = 'business_areas';


    public static function getALlBusinessAreas($searchInput,$skip,$take, $is_paginate, $filterFilters=false)
    {

        logger(__METHOD__);
        $res    =   BusinessArea::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', Filters::getFilterFilters('business_area_id'));}


        if(Filters::getFilteredOrgStructure()!==null){
            //$res->where('id', Filters::getFilteredOrgStructure()->business_area_id);
        }

        $res->orderByDesc('updated_at');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }


        return $res->get();
    }


    public static function getALlBusinessArea($searchInput,$skip,$take, $is_paginate, $filterFilters=false)
    {
        $ids = Filters::getFilterFilters('business_area_id');

        $res    =   BusinessArea::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', $ids);}

        $res->orderBy('name');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }
        ;
        return $res->get();





        $emp_ids = Filters::getFilterFilters('business_area_id');
        Employee::getEmployeeIDsAsHRAndManager($emp_ids);

        $employees    =   DB::table("employees")
            ->join('org_structures', 'org_structures.id', 'employees.org_structure_id')
            ->where("employees.cycle_id", Cycle::getCurrentCycleId());
        if($filterFilters===true){
            $employees->whereIn('employees.id',$emp_ids);
        }
        $employees->select("org_structures.business_area_id");
        $array_ids = $employees->pluck('business_area_id')->toArray();


        $res    =   BusinessArea::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', $array_ids);}

        if(Filters::getFilteredOrgStructure()!==null){
            //$res->where('id', Filters::getFilteredOrgStructure()->business_area_id);
        }

        $res->orderByDesc('updated_at');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }


        return $res->get();
    }

}

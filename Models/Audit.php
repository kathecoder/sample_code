<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\UserRolesController;

class Audit extends Model
{
    protected $table = 'audits';

    public static function captureAudit(string $employee_personnel_number, string $field_name, string $user_personnel_number, $value_before, $value_after, int $phase_id = null,  int $cycle_id = null ){
        try{
            $audit = new Audit();

            $audit->employee_personnel_number   = $employee_personnel_number;
            $audit->field_name                  = $field_name;
            $audit->user_personnel_number       = $user_personnel_number;
            $audit->value_before                = $value_before;
            $audit->value_after                 = $value_after;
            $audit->cycle_id                    = $cycle_id;
            $audit->phase_id                    = $phase_id;
            $audit->created_at                  = now();
            $audit->save();
            return array('error' => false, 'response' => [], 'message' => 'Audit trail logged successfully');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
    
    public static function getAudits($searchInput,$skip,$take, $is_paginate, $startDate = null, $endDate = null)
    {
        ini_set('max_execution_time', 21100);
        ini_set('memory_limit', '-1');
        $user            = Auth::user();
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $manager_pn      = $user->personnelnumber;
        $audit_trail = DB::table('audits')
            ->join('users', 'users.personnelnumber', 'audits.user_personnel_number')
            ->join('employees', 'employees.personnelnumber', 'audits.employee_personnel_number')
            ->join('employee_incentives', 'employees.personnelnumber', 'employee_incentives.personnelnumber')
            ->join('employee_assessors', 'employees.personnelnumber', 'employee_assessors.personnelnumber');

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){ // admin roles [1, 2, 3] - if not admin
                //$audit_trail =  $audit_trail->where('audits.user_personnel_number', $user->personnelnumber);

                $audit_trail = $audit_trail->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                        ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);
                });

                // ------> exclude these fields when the employee in question falls under the following schemes [smi, specialist, & risk]
                if (in_array('field_name', ['proposedsalary_value',  'proposedsalary_percentage',   'calc_proposedsalary_zar',  'calc_proposedincentive_zar'])){
                    $audit_trail =  $audit_trail->whereNotIn('employee_incentives.incentive_scheme_id', [20,23, 43]);
                }

        }
        else{
            //$audit_trail =  $audit_trail->where('audits.user_personnel_number', $user->personnelnumber);
            //$audit_trail =  $audit_trail->whereIn('employee_incentives.incentive_scheme_id', [20,23, 43]);
        }

        $audit_trail =   $audit_trail->where(function($query) use ($searchInput){
                $query
                    ->where('audits.employee_personnel_number', 'like', '%'.$searchInput.'%')
                    ->orWhere('employees.personnelnumber', 'like', '%'.$searchInput.'%')
                    ->orWhere('employees.knownas', 'like', '%'.$searchInput.'%')
                    ->orWhere('employees.lastname', 'like', '%'.$searchInput.'%')
                    ->orWhere('audits.user_personnel_number', 'like', '%'.$searchInput.'%')
                    ->orWhere('audits.field_name', 'like', '%'.$searchInput.'%')
                    ->orWhere('audits.value_before', 'like', '%'.$searchInput.'%')
                    ->orWhere('audits.value_after', 'like', '%'.$searchInput.'%')
                    ->orWhere('audits.created_at', 'like', '%'.$searchInput.'%')
                    ->orWhere('users.personnelnumber', 'like', '%'.$searchInput.'%')
                    ->orWhere('users.name', 'like', '%'.$searchInput.'%');
                })
            ->select(
                DB::raw("CONCAT(employees.knownas,' ',employees.lastname) as employee_personnel_name"),
                DB::raw("audits.employee_personnel_number as employee_personnel_number"),    
                DB::raw("users.name as user_personnel_number"),
                DB::raw("audits.field_name as field_name"),
                DB::raw("audits.value_before as value_before"),
                DB::raw("audits.value_after as value_after"),
                DB::raw("audits.created_at as created_at")
            );

        logger($startDate);
        logger($endDate);
        if ($startDate !== null && $endDate !== null){
            $sql = "audits.created_at < $endDate + INTERVAL 1 DAY ";
            $audit_trail = $audit_trail->whereDate('audits.created_at', '>=', $startDate);
            $audit_trail = $audit_trail->whereDate('audits.created_at', '<=', $endDate);

            //$audit_trail = $audit_trail->whereRaw( $sql   );
            // $audit_trail = $audit_trail->whereBetween('audits.created_at', [$startDate, $endDate]);
        }
        $audit_trail = $audit_trail->orderByDesc('audits.created_at');

        if($is_paginate===true){
            $audit_trail->skip($skip)
                ->take($take);
        }
        DB::enableQueryLog();
        $audit_trail = $audit_trail->get();
       // logger(DB::getQueryLog());

        return $audit_trail;
    }

    public static function AuditTrailPerModel($table, $object, $personnelnumber){
        try{
            //logger(__LINE__);
            $cycle_id = Cycle::getLiveCycleID();
            $phase_id = Phase::getLivePhaseID();
            $columns  = DB::select("describe $table");
            foreach($columns as $key => $column){
                if (!in_array($column->Field, ['id', 'created_at', 'updated_at' ]))
                {
                    /*logger("####################################################" );
                    logger("PN:          " . $personnelnumber);
                    logger("VB:          " .  $object->getOriginal($column->Field));
                    logger("VA:          " .  $object->{$column->Field});
                    logger("Filed Name:  " . $column->Field);
                    logger("Filed Name:  " . $column->Field);
                    logger("####################################################" );*/


                    if ($object->getOriginal($column->Field) != $object->{$column->Field}){

                        Audit::captureAudit(
                            $personnelnumber, //
                            $column->Field,
                            Auth::user()->personnelnumber,
                            $object->getOriginal($column->Field),
                            $object->{$column->Field},
                            $phase_id,
                            $cycle_id
                        );
                    }
                }
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }
}

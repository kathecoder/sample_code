<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusReason extends Model
{
    protected $table = 'status_reason';
}
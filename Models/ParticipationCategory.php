<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParticipationCategory extends Model
{
    protected $table = 'participation_categories';
}

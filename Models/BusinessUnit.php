<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BusinessUnit extends Model
{
    protected $table = 'business_units';

    public static function getALlBusinessUnits($searchInput,$skip,$take, $is_paginate, $filterFilters=false)
    {
        //if this is the current primary filter store the array in the session
        // such that when you pull it up all the options for for filtering are still there

        $ids = Filters::getFilterFilters('business_unit_id');

        $res    =   BusinessUnit::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id', $ids);}

        $res->orderBy('name');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }
        ;
        return $res->get();

















        $emp_ids = Filters::getFilterFilters('business_unit_id');
        Employee::getEmployeeIDsAsHRAndManager($emp_ids);

        $employees    =   DB::table("employees")
            ->join('org_structures', 'org_structures.id', 'employees.org_structure_id')
            ->where("employees.cycle_id", Cycle::getCurrentCycleId());
        if($filterFilters===true){
            $employees->whereIn('employees.id',$emp_ids);
        }
        $employees->select("org_structures.business_unit_id");
        $array_ids = $employees->pluck('business_unit_id')->toArray();

        $res = BusinessUnit::where('name','like', '%'.$searchInput.'%');
        if($filterFilters===true){$res ->  whereIn('id',$array_ids);}

        if(Filters::getFilteredOrgStructure()!==null){
            //$res->where('id', Filters::getFilteredOrgStructure()->business_unit_id);
        }

        $res->orderByDesc('updated_at');

        if($is_paginate===true){
            $res->skip($skip)
                ->take($take);
        }


        return $res->get();
    }
}

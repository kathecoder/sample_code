<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryIncreaseMatrices extends Model
{
    protected $table = 'salary_increase_matrices';
}
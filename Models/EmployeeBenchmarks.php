<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeBenchmarks extends Model
{
    protected $table ='employee_benchmarks';

    public static function saveEmployeeBenchmarks($employee, $row){
        $cycle_id = 1; //todo: update cycle id
        $salary_currency = null;
        $employee_benchmark         = EmployeeBenchmarks::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
        $employee_salary            = EmployeeSalary::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
        $employee_incentive         = EmployeeIncentive::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();

        $calc_salary_zar            = $employee_salary->calc_salary_zar;
        $calc_proposedsalary_zar    = $employee_salary->calc_proposedsalary_zar;
        $calc_proposedincentive_zar = $employee_incentive->calc_proposedincentive_zar;


        if ($employee_benchmark === null){
            $employee_benchmark                     = new EmployeeBenchmarks();
            $employee_benchmark->cycle_id           = $cycle_id;
            $employee_benchmark->personnelnumber    = $employee->personnelnumber;
        }

        if (isset($row['internal_jobscale_code'])){
            $col = $row['internal_jobscale_code'];
            $benchmark = Benchmark::where('job_code',$col)->where('title',$row['calc_internal_jobscale_name'])->where('indicator',$row['calc_internal_jobscale_indicator'])->first();
            if ($benchmark !== null){
                $employee_benchmark->benchmark_id                       = $benchmark->id;
                $employee_benchmark->internal_jobscale_code             = $benchmark->job_code;
                $employee_benchmark->calc_internal_jobscale_name        = $benchmark->title;
                $employee_benchmark->calc_internal_jobscale_indicator   = $benchmark->indicator;
                $employee_benchmark->calc_internal_jobscale_grade       = $benchmark->grade;

                $employee_benchmark->calc_intbench_salary_median        = $benchmark->p2;
                $employee_benchmark->calc_intbench_bonus_median         = $benchmark->mm;

                $employee_benchmark->calc_compa_ratio_salary_pre    = round($benchmark->p2 > 0 ? ($calc_salary_zar / $benchmark->p2) * 100 : 0, 0);
                $employee_benchmark->calc_compa_ratio_salary_post   = round($benchmark->p2 != 0 ? ($calc_proposedsalary_zar / $benchmark->p2)  * 100 : 0, 0);
                $employee_benchmark->calc_compa_ratio_incentive     = round($benchmark->mm > 0 ? ($calc_proposedincentive_zar / $benchmark->mm) * 100 : 0, 0);

            }
            else {
                $employee_benchmark->benchmark_id           = null;
                $employee_benchmark->internal_jobscale_code = null;
            }

        }

        Audit::AuditTrailPerModel('employee_benchmarks', $employee_benchmark, $employee->personnelnumber);
        $employee_benchmark->save();

    }
}

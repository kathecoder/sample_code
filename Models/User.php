<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class User extends Model
{
    protected $table    =   'users';

    public static function CreateUserByEmployee(Employee $employee){
        try{
            $is_new = false;
            $user   = User::where('personnelnumber', $employee->personnelnumber)->first();

            $name   = $employee->knownas !== null ? $employee->knownas : $employee->firstnames;
            if ($user === null){
                $user   = new User();
                $is_new = true;
                $user->password                 =  Hash::make('Pa55w0rd*');
                $user->active                   =  1;
                $user->admin                    =  0;
                $user->super                    =  0;
                $user->accounttype              =  4;//Default Personnel User
            }

            $user->name                     = $name   . ' ' . $employee->lastname;
            $user->email                    =  $employee->emailaddress;
            $user->username                 =  $employee->emailaddress;
            $user->personnelnumber          =  $employee->personnelnumber;
            $user->instance_indicator_id    =  $employee->instance_indicator_id;
            $user->save();

            return array('error' => false, 'response' => ['user' => $user, 'is_new'=>$is_new], 'message' => 'Successfully created user');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    /**
     * Essentially, this is user and role mapping function
     * $manager_type_id is role_id,
     * @param $manager_type_id
     * @param $user_id
     */
    public static function CreateManagerTypeUser($manager_type_id, $user_id){
        try {
            $object = ManagerTypeUser::where('manager_type_id', $manager_type_id)->where('user_id', $user_id)->first();

            if ($object === null){
                $object = new ManagerTypeUser();
                $object->user_id            = $user_id;
                $object->manager_type_id    = $manager_type_id;
                $object->save();
            }

            return $object;
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
    
    public static function allUsers($searchInput,$skip,$take, $is_paginate)
    {
        $users = User::where('name', 'LIKE', '%'.$searchInput.'%')
            ->orWhere('email', 'LIKE', '%'.$searchInput.'%');

        if($is_paginate===true){
            $users->skip($skip)
                ->take($take);
        }
//        $users->orderBy('updated_at', 'DESC');

        return $users->get();

    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class issueLogsCategory extends Model
{
    protected $table = 'issue_categories';

    public static function getAllCategories()
    {
        $categories = issueLogsCategory::all();

        return $categories;
    }
}

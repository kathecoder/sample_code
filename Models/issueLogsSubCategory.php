<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class issueLogsSubCategory extends Model
{
    protected $table = 'issue_sub_categories';

    public static function getIssueSubCategories($id){
        $res = issueLogsSubCategory::where('issue_category_id', $id)->get();

        return $res;
    }
    public static function getALlSubCategory($search, $skip, $take, $is_paginate){
        $data = issueLogsSubCategory::where('id','>',0);

        if($is_paginate===true){
            $data-> skip($skip)
                ->take($take);
        }

        return $data->get();
    }
    public static function getFilterdSubCategory($searchInput, $skip, $take, $is_paginate){
        $data   =   DB::table("issue_sub_categories")
                    ->join("issue_logs", "issue_logs.issue_sub_category_id", "issue_sub_categories.id")
                    ->where("issue_logs.cycle_id", Cycle::getLiveCycleID())
                    ->where('issue_sub_categories.name', 'LIKE', '%'.$searchInput.'%')
                    ->select("issue_sub_categories.*");

        if($is_paginate===true){
            $data-> skip($skip)
                ->take($take);
        }

        return $data->distinct()->get();
    }

}

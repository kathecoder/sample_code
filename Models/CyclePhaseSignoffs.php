<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CyclePhaseSignoffs extends Model
{
    protected $table = 'cycle_phase_signoffs';
}
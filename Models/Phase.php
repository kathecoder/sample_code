<?php

namespace App\Models;

use App\Http\Controllers\PermissionController;
use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    protected $table = 'phases';

    public static function getLivePhaseID(){

        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $cycle_id        = Cycle::getLiveCycleID();
        $live_phase_id   = 0;

        $live_phase = CyclePhases::where('live', 1)->where('cycle_id', $cycle_id)->where('instance_id', $active_instance_id)->first();
        if ($live_phase){
            $live_phase_id = $live_phase->phase_id;
        }
        return $live_phase_id;
    }

    public static function getLivePhase(){
        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $cycle_id           = Cycle::getLiveCycleID();
        $live_phase         = null;

        $cycle_phase = CyclePhases::where('live', 1)->where('cycle_id', $cycle_id)->where('instance_id', $active_instance_id)->first();
        if ($cycle_phase){
            $live_phase = Phase::find($cycle_phase->phase_id);
        }
        return $live_phase;
    }

    public static function getPhasesPerCurrentInstance(){
        $instance_id    = PermissionController::getUserInstance()['response']['instance'];
        $cycle_id       = Cycle::getLiveCycleID();
        $phases         = null;

        $phases = Phase::where('sub_owner_id', $instance_id)->pluck('name', 'id')->toArray();;
        if ($phases)
            return $phases;

        return $phases;
    }

    public static function getPhasesPerInstanceID(int $instance_id){
        //$instance_id    = PermissionController::getUserInstance()['response']['instance'];
        $cycle_id       = Cycle::getLiveCycleID();
        $phases         = null;

        $phases = Phase::where('sub_owner_id', $instance_id)->get();
        if ($phases)
            return $phases;

        return $phases;
    }
}

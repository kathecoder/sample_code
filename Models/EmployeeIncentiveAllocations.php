<?php

namespace App\Models;

use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EmployeeIncentiveAllocations extends Model
{
    protected $table = 'vw_employee_allocation_incentive';


    public static function getRecords($searchInput,$skip,$take, $is_paginate, $status = null,$sort_by = null,$is_ascending = false, $type = ""){
        $manager_pn         = Auth::user()->personnelnumber;
        $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $records            = DB::table('vw_employees_export');
        $records            = Filters::applyFilters($records, Auth::user());
        $records            = $records->whereIn('status_id',  [1,7,19])->where('instance_indicator_id',  $active_instance);
        $records            = DataField::filterFilteredColumns($records, $type);
        $headers            = DataField::getFields($type);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                    ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

            });
        }

        $headers[]='id';
        $headers[]='status_id';

        $records = $records
                    ->where(function($query) use ($searchInput){
                        $query
                            ->where('personnelnumber', 'like', '%'.$searchInput.'%')
                            ->orWhere('jobtitle', 'like', '%'.$searchInput.'%')
                            ->orWhere('lastname', 'like', '%'.$searchInput.'%')
                            ->orWhere('knownas', 'like', '%'.$searchInput.'%')
                            ->orWhere('firstnames', 'like', '%'.$searchInput.'%');

                    })
                    ->select($headers);
        if ($sort_by !== '' || $sort_by = null){
            $is_ascending = $is_ascending === true ? 'ASC' : 'DESC';
            $records =  $records->orderBy($sort_by, $is_ascending);
        }

        if($is_paginate===true /* && $searchInput ==''*/){
            $records->skip($skip)
                ->take($take);
        }

        return $records->get();

    }

    public static function salaryCalculations($id, $field, $value){

        try{

            $employee           = Employee::find($id);
            $employee_salary    = EmployeeSalary::where('personnelnumber', $employee->personnelnumber)->first();
            $salary_value       = $employee_salary->salary_value;
            $currency           = Currency::find($employee_salary->salary_currency_id);
            $current_rating_id      = EmployeePerformance::where('personnelnumber', $employee->personnelnumber)->first()->current_rating_id;


            if ($field == 'proposedsalary_percentage'){

                if ($current_rating_id == 1){
                    return array (
                        'error'=>false,
                        'response'=> ['proposedsalary_percentage'   => number_format(0, 1, '.', ' ')],
                        'message'=>'The employee has a rating of Not Achieved and does not qualify for an incentive');
                }

                $proposedsalary_percentage                   = round($value, 1);
                $proposedsalary_value                        = ($salary_value * ($value / 100)) + $salary_value;
                $employee_salary->proposedsalary_percentage  = round($value, 1);
                $employee_salary->proposedsalary_value       = round($proposedsalary_value, 0);

                //do ZAR calculations
                $employee_salary->calc_proposedsalary_zar = round($proposedsalary_value / $currency->conversion_rate, 0);

                Audit::AuditTrailPerModel('employee_salaries', $employee_salary, $employee->personnelnumber);
                $employee_salary->save();
                $employee->refreshCalculations();
                $res =  [
                    'proposedsalary_percentage'      => number_format($employee_salary->proposedsalary_percentage, 1, '.', ' '),
                    'proposedsalary_value'           => number_format($employee_salary->proposedsalary_value, 0, '.', ' ') ,
                    'calc_proposedsalary_zar'        => number_format($employee_salary->calc_proposedsalary_zar, 0, '.', ' '),

                ];
                $employee_salary->save();
            }
            elseif ( $field == 'proposedsalary_value'){

                if ($current_rating_id == 1){
                    return array (
                        'error'=>false,
                        'response'=> ['proposedsalary_value'   => number_format($employee_salary->salary_value, 0, '.', ' ') ],
                        'message'=>'The employee has a rating of Not Achieved and does not qualify for an incentive');
                }
                //(proposedsalary_value - salary_value) / salary_value * 100

                $proposedsalary_percentage                  = $salary_value > 0 ?  ($value - $salary_value) / $salary_value * 100 : 0.0 ;
                $proposedsalary_value                       = $value;
                $employee_salary->proposedsalary_value      = $value;
                $employee_salary->proposedsalary_percentage = $proposedsalary_percentage;

                //do ZAR calculations
                $employee_salary->calc_proposedsalary_zar = round($proposedsalary_value / $currency->conversion_rate, 0);

                Audit::AuditTrailPerModel('employee_salaries', $employee_salary, $employee->personnelnumber);
                $employee_salary->save();
                $employee->refreshCalculations();
                $res = [
                    'proposedsalary_percentage'     => number_format($proposedsalary_percentage, 1, '.', ' '),
                    'proposedsalary_value'          => number_format($proposedsalary_value, 0, '.', ' '),
                    'calc_proposedsalary_zar'       => number_format($employee_salary->calc_proposedsalary_zar, 0, '.', ' ') ,
                ];
            }
            elseif ($field == 'do_not_process_salary'){

                $employee_salary->do_not_process_salary = $value === 'on' ? 1:0;

                Audit::AuditTrailPerModel('employee_salaries', $employee_salary, $employee->personnelnumber);
                $employee_salary->save();
                $employee->refreshCalculations();
                $res = ['do_not_process_salary'      =>  $value == 'on' ? 1:0];

            }

            return array (
                'error'=>false,
                'response'=> $res,
                'message'=>'Salary Allocation Calculations Complete');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical("ErrorCode: EIA-SC-".$e->getLine() ." :".$msg);
            return array('error' => true, 'response' => [], 'message' => 'Salary Allocation Calculations Complete Failed: ' . "\nErrorCode: EIA-SC-".$e->getLne() );
        }
    }

    public static function incentiveCalculations($id, $field, $value){

        $employee               = Employee::find($id);
        $employee_salary        = EmployeeSalary::where('personnelnumber', $employee->personnelnumber)->first();
        $employee_incentive     = EmployeeIncentive::where('personnelnumber', $employee->personnelnumber)->first();
        $current_rating_id      = EmployeePerformance::where('personnelnumber', $employee->personnelnumber)->first()->current_rating_id;
        $current_rating         = CurrentRating::find($current_rating_id);
        $salary_value           = round($employee_salary->salary_value,0);
        $currency               = Currency::find($employee_salary->salary_currency_id);
        $validation_message     = $employee_incentive->incentive_scheme_id === 43 ? self::$message_sp : self::$message_smi;
        $specific_message       = $employee->knownas . ' ' . $employee->lastname . ' ' . 'Performance Rating is ' . $current_rating_id;
        $validation_message     = $validation_message . $specific_message;

        // -------------------- Update Through Proposed Incentive Percentage
        if ($field == 'proposedincentive_percentage'){
            $proposedincentive_value                            = $value * $salary_value / 100;
            $proposedincentive_percentage                       = round($value, 1);;
            $employee_incentive->proposedincentive_percentage   = round($value, 1);
            $employee_incentive->proposedincentive_value        = round($proposedincentive_value, 0) ;

            //do ZAR calculations
            $employee_incentive->calc_proposedincentive_zar = round($proposedincentive_value / $currency->conversion_rate, 0);

            Audit::AuditTrailPerModel('employee_incentives', $employee_incentive, $employee->personnelnumber);
            $employee_incentive->save();
            $employee->refreshCalculations();
            $res =  [
                'proposedincentive_percentage'  => number_format($proposedincentive_percentage, 1, '.', ' '),
                'proposedincentive_value'       => number_format($proposedincentive_value, 0, '.', ' '),
                'calc_proposedincentive_zar'    => number_format($employee_incentive->calc_proposedincentive_zar, 0, '.', ' '),

            ];
        }

        // -------------------- Update Through Proposed Incentive Value
        elseif ( $field == 'proposedincentive_value'){
            $proposedincentive_value                            = round($value, 0);
            $proposedincentive_percentage                       = $value / $salary_value * 100;
            $employee_incentive->proposedincentive_percentage   = round($proposedincentive_percentage, 1);
            $employee_incentive->proposedincentive_value        = round($proposedincentive_value, 1) ;

            //do ZAR calculations
            $employee_incentive->calc_proposedincentive_zar = round($proposedincentive_value / $currency->conversion_rate, 0);

            Audit::AuditTrailPerModel('employee_incentives', $employee_incentive, $employee->personnelnumber);
            $employee_incentive->save();
            $employee->refreshCalculations();
            $res =  [
                'proposedincentive_percentage' => number_format($proposedincentive_percentage, 1, '.', ' '),
                'proposedincentive_value'       => number_format($proposedincentive_value, 0, '.', ' '),
                'calc_proposedincentive_zar'    => number_format($employee_incentive->calc_proposedincentive_zar, 0, '.', ' '),

            ];
        }
        // -------------------- Update Through Assessment Score
        elseif ( $field == 'assessment_score'){

            $assessment_score = round($value, 0);
            /*if ($current_rating_id == 1){
                return array (
                    'error'=>true,
                    'response'=> ['assessment_score-'. $id   => 0],
                    'message'=>'The employee has a rating of Not Achieved and does not qualify for an incentive');
            }*/

            // ---------- first validation
            if ($assessment_score < 0 || $assessment_score > 125){
                return array (
                    'error'=>true,
                    'response'=> ['assessment_score'   => $employee_incentive->assessment_score],
                    'message'=>$validation_message);
            }

            // get scheme id 43, 20, 20
            if ($employee_incentive->incentive_scheme_id === 43){ // ---------------- specialist



                if ( $assessment_score > 125){
                    return array (
                        'error'=>true,
                        'response'=> ['assessment_score'   => $employee_incentive->assessment_score],
                        'message'=>$validation_message);
                }


            }
            else if ($employee_incentive->incentive_scheme_id === 20 || $employee_incentive->incentive_scheme_id === 23){ // Risk Senior Management Scheme


                if ( $assessment_score > 100){
                    return array (
                        'error'=>true,
                        'response'=> ['assessment_score'   => $employee_incentive->assessment_score],
                        'message'=>$validation_message);
                }
            }

            $participation_category_id  = $employee_incentive->participation_category_id;
            $participation_category     = ParticipationCategory::find($participation_category_id);

            if ($participation_category === null){
                return array (
                    'error'     =>true,
                    'response'  => ['assessment_score'   => 0],
                    'message'   =>'The employee does not have a participation category mapped to them. Please contact administrator.');
            }


            $calc_non_financial =  $participation_category->split_non_financial;
            $calc_kpi_incentive_percentage                      = $salary_value >  0 ? ((($salary_value * $assessment_score / 100 * ($calc_non_financial / 100))/$salary_value) * 100) : 0;
            $employee_incentive->assessment_score               = round($value, 1);
            $employee_incentive->calc_kpi_incentive_percentage  = round($calc_kpi_incentive_percentage, 1);
            $calc_kpi_incentive_value                           = $salary_value * $assessment_score / 100 * ($calc_non_financial / 100);
            $employee_incentive->calc_kpi_incentive_value       = round($calc_kpi_incentive_value, 0);
            $financial_incentive_value                          = round($employee_incentive->financial_incentive_value, 0);

            $proposedincentive_value                            = $calc_kpi_incentive_value + ($financial_incentive_value === null ? 0 : $financial_incentive_value);

            $proposedincentive_percentage                       = $salary_value > 0 ? $proposedincentive_value / $salary_value * 100 : 0;
            $employee_incentive->proposedincentive_value        = round($proposedincentive_value, 1);
            $employee_incentive->proposedincentive_percentage   = round($proposedincentive_percentage, 1);

            //do ZAR calculations
            $calc_proposedincentive_zar                         = round($proposedincentive_value / $currency->conversion_rate, 0);
            $employee_incentive->calc_proposedincentive_zar     = $calc_proposedincentive_zar;

            Audit::AuditTrailPerModel('employee_incentives', $employee_incentive, $employee->personnelnumber);
            $employee_incentive->save();

            $employee->refreshCalculations();
            /*$res =  [
                'assessment_score-'.                $id   => $assessment_score,
                //'calc_non_financial-'.              $id   => $calc_non_financial,
                'calc_kpi_incentive_percentage-'.   $id   => number_format($calc_kpi_incentive_percentage, 1, '.', ' '),
                'proposedincentive_value-'.         $id   => number_format($proposedincentive_value, 0, '.', ' '),
                'calc_proposedincentive_zar-'.      $id   => number_format($calc_proposedincentive_zar, 0, '.', ' '),
            ];*/
            $res =  [
                'assessment_score'              => $assessment_score,
                'calc_kpi_incentive_percentage' => number_format($calc_kpi_incentive_percentage, 1, '.', ' '),
                'proposedincentive_value'       => number_format($proposedincentive_value, 0, '.', ' '),
                'calc_proposedincentive_zar'    => number_format($calc_proposedincentive_zar, 0, '.', ' '),
            ];
        }

        return array (
            'error'=>false,
            'response'=> $res,
            'message'=>'Allocation Calculations Complete');

    }

    public static $message_sp = 'Assessment score for Specialist - Input field of a number (%) between 0 and 125. 
Validation of number allowed based on Performance Score field as follows:
0 – 60 % for 2 - Partially Achieved
60 – 100 % for 3 - Fully Achieved
100 – 125 % for 4 - Exceptionally Achieved
0 - 125% Discretionary % for Too Early to Assess
';
    public static $message_smi = 'Assessment score for SMI & Risk- Input field of a number (%) between 0 and 100. 
Validation of number allowed based on Performance Score field as follows:
0 – 40 % for Partially Achieved
40 – 80 % for Fully Achieved
80 – 100 % for Exceptionally Achieved
0 - 100% Discretionary % for Too Early to Assess
';

}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OccupationalCategory extends Model
{
    protected $table = 'occupational_categories';
}

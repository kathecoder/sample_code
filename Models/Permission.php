<?php

namespace App\Models;

use App\Http\Controllers\PermissionController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Permission extends Model
{
    protected $table = 'permissions';

    public static function columnDefinitionBuilder(){
        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $phase_id           = Phase::getLivePhaseID();
        $user               = Auth::user();
        $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 : 4;

        $column_definitions = array();
        $tableOptions       = array(
            "table_title"               => "",
            "className"                 => "table table-striped table-responsive table-bordered  tb-sv",
            "tableContainerClassName"   => "auto-scrollX"
        );
        $column_definitions["tableOptions"] = $tableOptions;

        $records = DB::table('permissions')
            ->leftJoin('data_fields', 'data_fields.id', 'permissions.data_field_id')
            ->leftJoin('data_field_labels', 'data_field_labels.data_field_id', 'data_fields.id');

        $records =   $records
            ->select(
                DB::raw("data_fields.name, data_fields.label, data_fields.control_type, data_fields.control_type"),
                DB::raw("data_field_labels.label as instance_label"),
                DB::raw("permissions.access, permissions.freeze_col ")
            );

        $records = $records->where('data_field_labels.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.more_info_screen_access', 'LIKE', '1');
        $records = $records->where('permissions.manager_type_id', $manager_type);
        $records = $records->where('permissions.cycle_id', 1);
        $records = $records->where('permissions.phase_id', $phase_id);
        $records = $records->whereIn('permissions.access', ['R', 'W']);

        $records = $records->get();

        foreach ($records as $record){
            $col = [];

            $col["column_properties"] = array(
                "name"      => $record->name,
                "title"     => $record->name === 'id' ? 'Action' : $record->instance_label,
                "width"     => "",
                "allowSort" => true,
                "freeze"    => $record->freeze_col === 1? true : false
            );

            //------------- ID
            if ($record->name == 'id'){
                //column text
                $col['button'][] = array(
                    "actionType"    => "",
                    "show"          => true,
                    "passValue"     => "id",
                    "icon_class"    =>  "fas fa-eye",
                    "className"     => "btn-wide btn-shadow btn btn-outline-primary btn-sm"
                );
            }
            //------------- Text + Write
            else if ($record->control_type == 'text' && $record->access == 'W'){
                //column text
                $col['input'][] = array(
                    "name"          => $record->name,
                    "show"          => true,
                    "className"     => "",
                    "input_type"     => "text"
                );
            }
            //------------- Number + Write
           else if ($record->control_type == 'number' && $record->access == 'W'){
                //column text
                $col['input'][] = array(
                    "name"          => $record->name,
                    "show"          => true,
                    "className"     => "",
                    "input_type"     => "text"
                );
            }
           //------------- Boolean + Write
           else if ($record->control_type == 'boolean' && $record->access == 'W'){
                //column text
                $col['input'][] = array(
                    "name"          => $record->name,
                    "show"          => true,
                    "className"     => "",
                    "input_type"     => "checkbox"
                );
            }
            else{
                //column text
                $col['text'][] = array(
                    "name"          => $record->name,
                    "show"          => true,
                    "className"     => ""
                );
            }

            $column_definitions['colums'][] = $col;
        }

        return $column_definitions;


































        $column_definitions = array();
        $tableOptions = array(
            "table_title"               => "",
            "className"                 => "table table-hover table-striped"
            //"className"                 => "table table-striped table-responsive table-bordered  tb-sv",
            //"tableContainerClassName"   => "auto-scrollX"
            );
        $column_definitions["tableOptions"] = $tableOptions;

        $col1 = [];

        /*// column properties
        $col1["column_properties"] = array(
            "name"      => "id",
            "title"     => "Action",
            "width"     => "",
            "allowSort" => true,
            "freeze"    => false
        );

        //column button
        $col1['button'][] = array(
            "actionType"    => "edit",
            "show"          => true,
            "passValue"     => "id",
            "icon_class"    =>  "fas fa-eye",
            "className"     => "btn-wide btn-shadow btn btn-outline-primary btn-sm"
        );


        "input": [{
                "name": "proposedsalary_percentage",
                "show": true,
                "className": "",
                "input_type": "text"
            }]

        $column_definitions['columns'][] = $col1;*/

        $col2 = [];

        // column properties
        $col2["column_properties"] = array(
            "name"      => "subject",
            "title"     => "Subject",
            "width"     => "",
            "allowSort" => true,
            "freeze"    => false
        );

        //column text
        $col2['text'][] = array(
            "actionType"    => "subject",
            "show"          => true,
            "className"     => ""
        );


        $column_definitions['colums'][] = $col2;

        $col3 = [];

        // column properties
        $col3["column_properties"] = array(
            "name"      => "updated_at",
            "title"     => "updated_at",
            "width"     => "",
            "allowSort" => true,
            "freeze"    => false
        );

        //column text
        $col3['text'][] = array(
            "actionType"    => "updated_at",
            "show"          => true,
            "className"     => ""
        );

        $column_definitions['colums'][] = $col3;


        $col4 = [];

        // column properties
        $col4["column_properties"] = array(
            "name"      => "created_at",
            "title"     => "created_at",
            "width"     => "",
            "allowSort" => true,
            "freeze"    => false
        );

        //column text
        $col4['text'][] = array(
            "name"    => "created_at",
            "show"          => true,
            "className"     => ""
        );

        $column_definitions['colums'][] = $col4;


        $col5 = [];

        // column properties
        $col5["column_properties"] = array(
            "name"      => "status",
            "title"     => "status",
            "width"     => "",
            "allowSort" => true,
            "freeze"    => false
        );

        //column text
        $col5['text'][] = array(
            "name"    => "status",
            "show"          => true,
            "className"     => ""
        );

        $column_definitions['colums'][] = $col5;

        $col1 = [];

        // column properties
        $col1["column_properties"] = array(
            "name"      => "id",
            "title"     => "Action",
            "width"     => "",
            "allowSort" => true,
            "freeze"    => false
        );

        //column button
        $col1['button'][] = array(
            "actionType"    => "edit",
            "show"          => true,
            "passValue"     => "id",
            "icon_class"    =>  "fas fa-eye",
            "className"     => "btn-wide btn-shadow btn btn-outline-primary btn-sm"
        );

        $column_definitions['columns'][] = $col1;

        //$column_definitions = json_encode($column_definitions);
        return $column_definitions;
    }

    public static function getFields($category){
        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $phase_id           = Phase::getLivePhaseID();
        $user               = Auth::user();
        $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 : 4;


        $records = DB::table('permissions')
            ->leftJoin('data_fields', 'data_fields.id', 'permissions.data_field_id')
            ->leftJoin('data_field_labels', 'data_field_labels.data_field_id', 'data_fields.id');

        $records =   $records
            ->select(
                DB::raw("data_fields.name, data_fields.label, data_fields.control_type, data_fields.control_type"),
                DB::raw("data_field_labels.label as instance_label"),
                DB::raw("permissions.access, permissions.freeze_col ")
            );

        $records = $records->where('data_field_labels.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.instance_indicator_id', $active_instance_id);
        $records = $records->where('permissions.more_info_screen_access', 'LIKE', '1');
        $records = $records->where('permissions.manager_type_id', $manager_type);
        $records = $records->where('permissions.cycle_id', 1);
        $records = $records->where('permissions.phase_id', $phase_id);
        $records = $records->whereIn('permissions.access', ['R', 'W']);

        return $records->pluck('data_fields.name')->toArray();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CyclePhases extends Model
{
    protected $table = 'cycle_phases';
}
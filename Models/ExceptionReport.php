<?php

namespace App\Models;

use App\CFLibrary\FilterHandler;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ExceptionReport extends Model
{
    protected $table = 'vw_exception_report_generic';



    public static function getRecords( $searchInput,$skip,$take, $is_paginate,$link ){

        $user            = Auth::user();
        $manager_pn      = $user->personnelnumber;
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        // $record_ids      = FilterHandler::getFilteredData('vw_employees_combine', Auth::user()->id);

        // $records = Filters::applyFilters($records, $user);
    

        //$records = DB::table('vw_exception_report_generic');
        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        $records = $records->where('instance_indicator_id','=',$active_instance);

        // if (count($record_ids) > 0){
        //     $records = $records->whereIn('id',  $record_ids);
        // }
        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)
                    ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                    ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)
                    ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                    ->orWhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)
                    ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                    ->orWhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)
                    ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                    ->orWhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)
                    ->orWhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                    ->orWhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

 
            });
        }
        if($link == 'low_performance'){
               $records =  $records->where(function ($query) {
                $query->where('current_rating_id', '=', 1)
                      ->orWhere('current_rating_id', '=', 2);
            });
            $records = $records->where(function ($query) {
                //$query->where('proposedsalary_value','>', 'salary_value')
                $query->whereRaw('proposedsalary_value > salary_value')
                      ->orWhere('proposedsalary_percentage', '>', 0)
                      ->orWhere('proposedincentive_percentage', '>', 0)
                      ->orWhere('proposedincentive_value', '>', 0);
            });
        }
        if($link == 'unchanged_performance'){
//                 $records = $records->where('proposedsalary_value', '=', 'calc_suggested_salary');
                 $records = $records->where('proposedsalary_percentage', '=', 'suggest_salary_percentage');
       }
        if($link == 'lessthan_performance'){
                 $records = $records->where('proposedsalary_value', '<', 'salary_value');
       }
        if($link == 'zero_performance'){
            $records = $records->where('status_id', '=', 1);
            $records = $records->Where(function ($query) {
                $query->where('salary_value', '=',0 )
                     ->orWhere('salary_value', '=', null);
                   
             });
        }
       
        if($link == 'good_performance'){
               $records =  $records->Where(function ($query) {
                $query->where('current_rating_id', '=', 3)
                      ->orWhere('current_rating_id', '=', 4);
            });
            $records = $records->Where(function ($query) {
               // $query->where('proposedsalary_value', '=', 'salary_value')
                $query->whereRaw('proposedsalary_value = salary_value')
                      ->orWhere('proposedsalary_percentage', '=', 0)
                      ->orWhere('proposedsalary_percentage', '=', null)
                      ->orWhere('proposedincentive_percentage', '=', 0)
                      ->orWhere('proposedincentive_percentage', '=', null)
                      ->orWhere('proposedincentive_value', '=', 0);
             });
        }
        /**below average */
        if($link == 'below_performance'){
            $res_avg = self::getProposedSalaryAverage();
           /* logger($res_avg);
            logger($res_avg[0]->avg_proposedsalary_percentage);*/
            $records = $records->where('proposedsalary_percentage','<', $res_avg[0]->avg_proposedsalary_percentage);
            /* $records = $records->Where(function ($query) {
                $query->where(
                    'proposedsalary_percentage',
                    '<',
                    DB::raw('(
                        select AVG(proposedsalary_percentage) as avg_proposedsalary_percentage
                        from `employee_salaries`
                        )' )
                     );
             });*/
        }


        
        $records =   $records->select(
                'id as id',
                'scheme_id as scheme_id',
                'personnelnumber as personnelnumber',
                'lastname as lastname',
                'knownas as knownas',
                'orglevel3 as orglevel3',
                'orglevel4 as orglevel4',
                'orglevel5 as orglevel5',
                'orglevel6 as orglevel6',
                'orglevel7 as orglevel7',
                'orglevel8 as orglevel8',
                'current_rating as current_rating',
                'proposedsalary_percentage as proposedsalary_percentage',
                'proposedincentive_percentage as proposedincentive_percentage',
                 'salary_value as salary_value'
        );
        $records =   $records->where(function($query) use ($searchInput){
            $query
                ->where('lastname', 'like', '%'.$searchInput.'%')
                ->orWhere('knownas', 'like', '%'.$searchInput.'%')
                ->orWhere('personnelnumber', 'like', '%'.$searchInput.'%');
              
        });

        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }


        $results = $records->get();

        //logger(DB::getQueryLog());

        return $results;




   
   



}

    public static function getProposedSalaryAverage(){
        $manager_pn         = Auth::user()->personnelnumber;
        $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $records            = DB::table('vw_employees_export');
        $records            = $records->whereIn('status_id',  [1,7,19]);
        $records            = $records->where('instance_indicator_id',  $active_instance);
        $records            = Filters::applyFilters($records, Auth::user());
        $records            = ManagerTypeUser::filterDatasetManagerType($records, $user_roles);
        $records            = $records->select(DB::raw(
            '
            ROUND(AVG(IFNULL(proposedsalary_percentage,0)), 2) as avg_proposedsalary_percentage
            '
        ));

       // DB::enableQueryLog();
        $records = $records->get();
        //logger(DB::getQueryLog());
        return $records;

    }
}

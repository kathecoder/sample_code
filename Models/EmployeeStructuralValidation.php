<?php

namespace App\Models;

use App\CFLibrary\FilterHandler;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UserRolesController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmployeeStructuralValidation extends Model
{
    protected $table = 'vw_employee_structure_validation';

    public static function getRecords($searchInput,$skip,$take, $is_paginate, $status =null,$sort_by,$is_ascending, $clause = ""){
        $manager_pn      = Auth::user()->personnelnumber;
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        //$record_ids      = FilterHandler::getFilteredData('vw_employees_combine', Auth::user()->id);

        $records = DB::table('vw_employees_export');
        $records = Filters::applyFilters($records, Auth::user());
        //$records = $records->where('status_id',  $status);

        if ($active_instance == 1){
            $headers = Employee::$group_headers_sv;
            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ($active_instance == 2){
            $headers = Employee::$africa_headers_sv;
            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ($active_instance == 3){
            $headers = Employee::$stanlib_headers_sv;
            $records = $records->where('instance_indicator_id',  $active_instance);
        }

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->where(function ($q) use($manager_pn) {
                $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                    ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn);

            });
        }

        $headers[]='id';
        $headers[]='status_id';
        $headers[]='calc_phaseclustermanager2name';

        if (!in_array('calc_businessmanager2name', $headers))
            $headers[]='calc_businessmanager2name';

        if (!in_array('calc_prorata', $headers))
            $headers[]='calc_prorata';

        $records = $records

            ->where(function($query) use ($searchInput){
                $query
                    ->where('personnelnumber', 'like', '%'.$searchInput.'%')
                    ->orWhere('jobtitle', 'like', '%'.$searchInput.'%')
                    ->orWhere('lastname', 'like', '%'.$searchInput.'%')
                    ->orWhere('knownas', 'like', '%'.$searchInput.'%')
                    ->orWhere('firstnames', 'like', '%'.$searchInput.'%');

            })
            ->select($headers);
            if ($sort_by !== '' || $sort_by = null){
                $is_ascending = $is_ascending === true ? 'ASC' : 'DESC';
                $records =  $records->orderBy($sort_by, $is_ascending);
            }
            // else{
            //     $records =  $records->orderBy("personnelnumber", 'DESC');
            // }  
        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }

        return $records->get();

    }

    public static function getAssessorRecords($searchInput,$skip,$take, $is_paginate, $status =null, $clause = ""){
        $manager_pn      = Auth::user()->personnelnumber;
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        //$record_ids      = FilterHandler::getFilteredData('vw_employee_combine', Auth::user()->id);

        $user_ids = ManagerTypeUser::all()->pluck('user_id')->toArray();
        $personnelnumber = \App\User::all()->pluck('personnelnumber')->toArray();

        $records = DB::table('vw_employees_export') ->whereIn('personnelnumber', array_unique($personnelnumber));

        $headers[]='id';
        $records = $records
            ->where(function($query) use ($searchInput){
                $query
                    ->where('personnelnumber', 'like', '%'.$searchInput.'%')
                    ->orWhere('jobtitle', 'like', '%'.$searchInput.'%')
                    ->orWhere('lastname', 'like', '%'.$searchInput.'%')
                    ->orWhere('knownas', 'like', '%'.$searchInput.'%')
                    ->orWhere('firstnames', 'like', '%'.$searchInput.'%');
            })
            ->select(self::$assessors_headers);

        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }

        return $records->get();

    }

    public static $assessors_headers = [
        'id',
        'personnelnumber',
        'knownas',
        'lastname',
        'orglevel5',
        'orglevel6',
        'orglevel7',
        'orglevel8'
        ,
    ];









    public static function getOrg($searchInput,$skip,$take, $is_paginate, $status =null, $clause = ""){
        /*$manager_pn      = Auth::user()->personnelnumber;
        $user_roles      = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $record_ids      = FilterHandler::getFilteredData('vw_employees_combine', Auth::user()->id);*/

        $records = DB::table('vw_organisational_structures');
        $records =   $records->where(function($query) use ($searchInput){
            $query
                ->where('region', 'like', '%'.$searchInput.'%')
                ->orWhere('instance', 'like', '%'.$searchInput.'%')
                ->orWhere('country', 'like', '%'.$searchInput.'%')
                ->orWhere('company', 'like', '%'.$searchInput.'%')
                ->orWhere('business', 'like', '%'.$searchInput.'%')
                ->orWhere('division', 'like', '%'.$searchInput.'%')
                ->orWhere('department', 'like', '%'.$searchInput.'%')
                ->orWhere('business_unit', 'like', '%'.$searchInput.'%');
        });


        if($is_paginate===true){
            $records->skip($skip)
                ->take($take);
        }

        return $records->get();

    }
}
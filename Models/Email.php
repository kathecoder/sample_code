<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class Email extends Model
{
    protected $table = 'emails';

    public function getHeader()
    {
        try{
            switch ($this->instance_id)
            {
                case  1 :
                    $header_img = '<div style="width:98%; background-color:#00154d; color:white; padding: 10px; height: 70px;">

    <table style="width:100%; border-collapse: collapse; height: 60px;">
        <tr style="background-color:#00154d;">
            <td style="width: 10%;">
                 <img src="#" alt="#">
            </td>
            <td style="width: 60%;">
                <p style="font-size:2.5vw; display:inline; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>system_name</b></p>
            </td>
            <td style="width: 30%; text-align: right;">
                <div class="image-container">
                     <img class="responsive" height="60px" width="220" src="base_url/img/rcg.jpg" alt="#">
                </div>

            </td>
        </tr>
    </table>
    
</div>';
                    break;

                case  2 :
                    $header_img = '<div style="width:98%; background-color:#00154d; color:white; padding: 10px; height: 70px;">

    <table style="width:100%; border-collapse: collapse; height: 60px;">
        <tr style="background-color:#00154d;">
            <td style="width: 10%;">
                <img src="#" alt="#">
            </td>
            <td style="width: 60%;">
                <p style="font-size:2.5vw; display:inline; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>system_name</b></p>
            </td>
            <td style="width: 30%; text-align: right;">
                <div class="image-container">
                   <img class="responsive" height="60px" width="220" src="base_url/img/rcg.jpg" alt="#">
                </div>

            </td>
        </tr>
    </table>
    
</div>';
                    break;

                case  3 :
                    $header_img = '<div style="width:98%; background-color:#000000; color:white; padding: 10px; height: 70px;">

    <table style="width:100%; border-collapse: collapse; height: 60px;">
        <tr style="background-color:#000000;">
            <td style="width: 10%;">  
                <img class="responsive" width="220" src="#" alt="#">
            </td>
            <td style="width: 60%;">
                <p style="font-size:2.5vw; display:inline; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>system_name</b></p>
            </td>
            <td style="width: 30%; text-align: right;">
                <div class="image-container">
                     <!--<img class="responsive" height="60px" width="220" src="base_url/img/rcg.jpg" alt="#">-->
                </div>

            </td>
        </tr>
    </table>
    
</div>';
                    break;

                default:
                    $header_img = '<table style="width:100%; border-collapse: collapse; height: 60px;">
        <tr style="background-color:#000000;">
            <td style="width: 10%;">
                <img src="{{\Illuminate\Support\Facades\URL::asset(\'/#\')}}" alt="#">
            </td>
            <td style="width: 60%;">
                <p style="font-size:2.5vw; display:inline; color:white;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>system_name</b></p>
            </td>
            <td style="width: 30%; text-align: right;">
                <div class="image-container">
                    <img class="responsive" height="60px" width="220" src="base_url/img/rcg.jpg" alt="#">
                    
                </div>

            </td>
        </tr>
    </table>';
                    break;
            }

            //$header_name
            $header    = '<img src="base_url/img/email_headers/header_img">';
            $header_img    = str_replace('base_url', URL::to('/'), $header_img);
            $header_img    = str_replace('system_name', 'REMSYS', $header_img);
            $header    = str_replace('header_img', $header_img, $header);
        return $header_img;
//            return array('error' => true, 'response' => $header, 'message' => 'Email header mapped successfully');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }


    public static function getRecords ($searchInput,$skip,$take, $is_paginate, $status = null, $clause = ""){

        $user_cycles = DB::table('emails')
            ->join('instance_indicators', 'instance_indicators.id', 'emails.instance_id')
            ->where(function($query) use ($searchInput){
                $query
                    ->where('emails.name', 'like', '%'.$searchInput.'%')
                    ->orWhere('emails.subject', 'like', '%'.$searchInput.'%')
                    ->orWhere('instance_indicators.name', 'like', '%'.$searchInput.'%');
            })
            ->select(
                DB::raw("emails.id as id"),
                DB::raw("emails.name, emails.subject, emails.created_at as created_at, emails.updated_at as updated_at"),
                DB::raw("instance_indicators.name as instance")
            );

        //$user_cycles->orderByDesc('user_cycles.updated_at');

        if($is_paginate===true){
            $user_cycles->skip($skip)
                ->take($take);
        }
        return $user_cycles->get();

    }
}
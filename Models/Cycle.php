<?php

namespace App\Models;

//use App\Http\Controllers\MailController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use DB;

class Cycle extends Model
{
    protected $table = 'cycles';

    /**
     * @return array|null
     */
    public static function getCurrentCycle(){

        try{
            $user_id= Auth::id();
            $filters = Filters::where('reference_table', 'cycles')->where('user_id', $user_id)->first();
            if ($filters !== null){
                return Cycle::find($filters->reference_table_id);
            }

            $cycle =  Cycle::where('status_id', '!=', 7)->get();

            if(count($cycle)>0){
                return $cycle[0];
            }else{
                return self::getLatestClosedCycle();
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    /**
     * @return array
     */
    public static function getLatestClosedCycle(){

        try{
            $status_closed = Status::where('name', 'Closed/Archived')->first();
            $cycle =  Cycle::where('status_id', $status_closed->id)->orderBy('created_at', 'desc')->first();

            return $cycle;
        }
        catch (\Exception $e) {
                $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
                Log::critical($msg);
                return array('error' => true, 'response' => [], 'message' => $msg);
            }
    }

    public static function getCurrentCycleId(){
        $cycle_id = 0;
        if(Cycle::getCurrentCycle()!==null){
            $cycle_id = Cycle::getCurrentCycle()->id;
            //Log::info('Current Cycle Id: '.$cycle_id);
        }

        return $cycle_id;
    }

    public static function getCycleById($id){
        $cycle = Cycle::find($id);
        return $cycle;
    }


    public static function getLiveCycleID(){
        //Todo: update function accordingly
        return 1;
    }

}

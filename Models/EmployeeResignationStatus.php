<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeResignationStatus extends Model
{
    protected $table = 'vw_employees_resign';
}

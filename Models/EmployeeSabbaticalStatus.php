<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeSabbaticalStatus extends Model
{
    protected $table = 'vw_employees_sabbatical';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessAreaManager extends Model
{
    protected $table = 'vw_employee_business_area_manager';
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Charterband extends Model
{
    protected $table = 'charterbands';
}

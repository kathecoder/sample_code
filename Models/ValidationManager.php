<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ValidationManager extends Model
{
    protected $table = 'vw_employee_validation_manager';
}
<?php

namespace App\Models;

use App\CFLibrary\Model\UniFieldWrite;
use App\Http\Controllers\MailController;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Pluralizer;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class AppAssist extends Model
{
    public static function transformDateTime(string $value, string $format = 'Y-m-d')
    {
        try {
            return Carbon::instance(Date::excelToDateTimeObject($value))->format($format);
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public static function validateDate(string $date, $format = 'Y-m-d')
    {
        try{
            $date = Carbon::instance(Date::excelToDateTimeObject($date))->format($format);
            $d = \DateTime::createFromFormat($format, $date);

            // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
            return $d && $d->format($format) === $date;
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }


    }

    public static function notify($employee, $type, $reminder = false) {
        try {
            $cycle = Cycle::getCurrentCycle();
            $body = '';
            if ($employee !== null){

                if ($type === 'hr')
                    $body = $cycle->html_email_template_hr;
                else if ($type === 'manager')
                    $body = $cycle->html_email_template_manager;
                else if ($type === 'manager_2')
                    $body = $cycle->html_email_template_manager_2;
                else if ($type === 'participant')
                    $body = $cycle->html_email_template_participant;

                $user_cycle = UserCycle::where('employee_id', $employee->id)->where('cycle_id', $cycle->id)->first();
                $user = User::find($user_cycle->user_id);
                return MailController::sendNotifyEmail($body, $user, $employee, $type, $reminder);
            }

        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public static function strictEmpty($var) {

        // Delete this line if you want space(s) to count as not empty
        $var = trim($var);

        if(isset($var) === true && $var === '') {

            // It's empty
            return true;

        }
        else {

            // It's not empty
            return false;

        }

    }

    public static function generateRandomString($length = 10) {
        $characters         = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength   = strlen($characters);
        $randomString       = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function mapUniWriteTable(string $table_main, string $table_secondary, string $category = null, array $field_excluded = null, $field_validation_rule = null, $field_reference_rules = null){
        try{
            $uni_table  = DB::table('uni_table')->select('*')->where('name', strtolower($table_main))->first();
            $columns    = DB::select("describe $table_secondary");
            $count      = 1;
            foreach ($columns as $column){
                if (in_array($column->Field, $field_excluded)) continue;
                $label                  = substr($column->Field, -3) === '_id' ? substr($column->Field, 0, - 3) : $column->Field;
                $label                  = substr($label, 0, 5) === 'calc_' ? substr($label,  5) : $label;
                $label                  = ucwords(str_replace('_', ' ', $label));
                $reference_rule         = AppAssist::generateCustomFieldReferenceRule($column->Field, $field_reference_rules);
                $default_reference_rule = substr($column->Field, -3) === '_id' ? Pluralizer::plural(substr($column->Field, 0, -3), 2)  . '.name' : NULL;

                $validation_rule        = AppAssist::generateCustomFieldValidationRule($column->Field, $field_validation_rule);

                //check whether record for the field in question exists before inserting
                $uni_field_write = DB::table('uni_field_write')->select('*')->where('uni_table_id', $uni_table->id)->where('field_name', $column->Field)->first();

                if($uni_field_write === null){
                    DB::table('uni_field_write')->insert(
                        array(
                            'uni_table_id'      => $uni_table->id,
                            'uni_ref_table_id'  => $uni_table->id,
                            'field_name'        => $column->Field,
                            'field_id'          => $column->Field,
                            'label'             => $label,
                            'description'       => $label,
                            'icon'              => NULL,
                            'uni_fieldtype_id'  => self::getFieldTypeId($column),
                            'required'          => $column->Null === 'NO' ? 1:0,
                            'default_value'     => $column->Default,
                            'category'          => $category === null ? ucwords(str_replace('_', ' ', $uni_table->name)) : $category,
                            'sub_category'      => null,
                            'order'             => $count,
                            'reference_rule'    => $reference_rule  !== false ? $reference_rule  : $default_reference_rule,
                            'validation_rule'   => $validation_rule !== false ? $validation_rule : self::generateDefaultInputValidator($column),
                            /*'output_rule'       => $column->Field,
                            'omitexport'        => $column->Field,
                            'min'               => $column->Field,
                            'max'               => $column->Field,
                            'tooltip'           => $column->Field,
                            'tooltip_text'      => $column->Field,*/
                        )
                    );
                }

                $count++;
            }
            return array('error' => false, 'response' => [], 'message' => "Successfully updated $uni_table->name");
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public static function getFieldTypeId($column){
        $type       = $column->Type;
        $is_lookup  = substr($column->Field, -3);
        $is_calc    = substr($column->Field, 0, 5);

        if ($is_lookup  === '_id')                             return $type   = 15; //lookup
        if ($is_calc    === 'calc_')                           return $type   = 17; //calculation
        if (strpos($type, 'bigint')     !== false)      return $type   = 4;
        if (strpos($type, 'int')        !== false)      return $type   = 4;
        if (strpos($type, 'tinyint')    !== false)      return $type   = 4;
        if (strpos($type, 'varchar')    !== false)      return $type   = 2;
        if (strpos($type, 'text')       !== false)      return $type   = 2;
        if (strpos($type, 'date')       !== false)      return $type   = 6;
        if (strpos($type, 'datetime')   !== false)      return $type   = 7;
        if (strpos($type, 'decimal')    !== false)      return $type   = 4;
        // TODO: add calc_* field type
        return 2;

    }

    public static function generateDefaultInputValidator($column, $field_validation_rule = null){

        $field_type_id = self::getFieldTypeId($column);
        if (in_array($field_type_id, [4]))  return 'INPUT_VALIDATOR_NUMERIC';
        if (in_array($field_type_id, [6]))  return 'INPUT_VALIDATOR_DATE_DD_MM_YYYY';
        if (in_array($field_type_id, [15])) return 'INPUT_VALIDATOR_GENERIC_LOOKUP';

        return null;
    }

    public static function generateCustomFieldReferenceRule($field_name, $field_reference_rules = null){
        if ($field_reference_rules !== null){
            if (array_key_exists($field_name, $field_reference_rules)){
                return $field_reference_rules[$field_name];
            }
        }
        return false;
    }

    public static function generateCustomFieldValidationRule($field_name, $field_validation_rules = null){
        if ($field_validation_rules !== null){
            if (array_key_exists($field_name, $field_validation_rules)){
                return $field_validation_rules[$field_name];
            }
        }
        return false;
    }

    public static function getWriteFields($table_name, $exclude_fields = null){
        try{
            $write_fields = [];
            $columns = DB::select("describe $table_name");
            foreach ($columns as $column){
                $field = $column->Field;
                $is_calc = substr($field, 0, 5);
                if ($is_calc === 'calc_') continue; //calculated field

                if ($exclude_fields !== null){
                    if (in_array($field, $exclude_fields))  continue;
                }

                $write_fields[] =  $column->Field;
            }

            return $write_fields;
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public static function getUniFieldWrites($tableName, $exclude_fields = [], $omit_export = true){
        try{
            if (is_numeric($tableName)){
                $records =  DB::table('uni_field_write')
                    ->select('*')
                    ->where('uni_table_id', $tableName)
                    ->whereNotIn('field_name', $exclude_fields);

                if ($omit_export)
                    $records = $records->where(function($query) use ($omit_export){
                                      $query->where('omitexport', 1)->orWhere('omitexport', null);
                                });


                $records = $records->get();
                $temp_array = [];

                foreach ($records as $record){
                    $temp_array[$record->field_name] = $record;
                }
                return $temp_array;
            }
            else{

                $uni_table = DB::table('uni_table')->select('id')
                    ->where('name', $tableName)
                    ->first();
                $records =  DB::table('uni_field_write')
                    ->select('*')
                    ->where('uni_table_id', $uni_table->id)
                    ->whereNotIn('field_name', $exclude_fields);

                if ($omit_export)
                    $records = $records->where(function($query) use ($omit_export){
                        $query->where('omitexport', 0)->orWhere('omitexport', 'NULL')->orWhere('omitexport', null);
                    });

                $records = $records->get();

                $temp_array = [];
                if ($records){
                    foreach ($records as $record){
                        $temp_array[$record->field_name] = $record;
                    }
                }
                return $temp_array;
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public static function getUniFieldReads($tableName, $exclude_fields = [], $omit_export = true){
        try{
            if (is_numeric($tableName)){
                $records =  DB::table('uni_field_read')
                    ->select('*')
                    ->where('uni_table_id', $tableName)
                    ->whereNotIn('field_name', $exclude_fields);

                /*if ($omit_export)
                    $records = $records->where(function($query) use ($omit_export){
                                      $query->where('omitexport', 1)->orWhere('omitexport', null);
                                });*/

                $records = $records->get();
                $temp_array = [];

                foreach ($records as $record){
                    $temp_array[$record->field_name] = $record;
                }
                return $records;
            }
            else{
                $uni_table = DB::table('uni_table')->select('id')
                    ->where('name', $tableName)
                    ->first();
                $records =  DB::table('uni_field_read')
                    ->select('*')
                    ->where('uni_table_id', $uni_table->id)
                    ->whereNotIn('field_name', $exclude_fields);

                /*if ($omit_export)
                    $records = $records->where(function($query) use ($omit_export){
                        $query->where('omitexport', 1)->orWhere('omitexport', null);
                    });*/

                $records = $records->get();

                $temp_array = [];
                if ($records){
                    foreach ($records as $record){
                        $temp_array[$record->field_name] = $record;
                    }
                }
                return $temp_array;
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public static function getDataFields($exclude_fields = [], $active_instance, $omit_export = true){
        try{
            $records = DB::table('data_fields')
                    ->join('data_field_labels', 'data_field_labels.data_field_id', 'data_fields.id')
                    ->whereNotIn('name', $exclude_fields)
                    ->where('instance_indicator_id', $active_instance);

            if ($omit_export)
                $records = $records->where(function($query) use ($omit_export){
                    $query->where('omitexport', 1)->orWhere('omitexport', null);
                });
            $records = $records->get();
            $temp_array = [];

            foreach ($records as $record){
                $temp_array[$record->name] = $record;
            }
            return $temp_array;

        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeIPPStatus extends Model
{
    protected $table = 'vw_employees_active_ipp';
}

<?php

namespace App\Imports;

use App\Models\Employee;
use App\Models\EmployeePerformance;
use App\Models\EmployeeSalary;
use App\Models\InputValidator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ValidateEmployeeImport implements ToCollection, WithHeadingRow
{
    protected $table;
    protected $fields;
    public $rows_failed = [];
    protected $upload_mode;

    public function __construct($upload_mode)
    {
        $this->upload_mode = $upload_mode;
    }

    /**
     * @param Collection $rows
     * @return array
     */
    public function collection(Collection $rows)
    {
        ini_set('max_execution_time', 21100);
        ini_set('memory_limit', '-1');
        $field_properties   = $this->getTableWriteFields($this->table);
        $validator          = new InputValidator();
        $lineno             = 1;

        try{
            $import_personnel_numbers = [];
            foreach ($rows as $row)
            {
               $import_personnel_numbers[] = $row['personnelnumber'];
            }
            Session::put('import_personnel_numbers',$import_personnel_numbers);

            if ($this->upload_mode == 2){
                logger("Upload mode is $this->upload_mode - New records only");
            }

            foreach ($rows as $row)
            {


                $lineno++;
                foreach ($field_properties as $field_name => $field_property){
                    $line = false;
                    if(isset($row[$field_name])){

                       if ($field_name == 'salary_currency_id' &&  (isset($row['status_id']) && $row['status_id'] == 2))
                            continue;
                       if ($field_name == 'hrbp_1' &&  (isset($row['status_id']) && $row['status_id'] == 2))
                            continue;

                        $field_value        = $row[$field_name];
                        $validation_rule    = defined($field_property->validation_rule) ? constant($field_property->validation_rule) : null;
                        $reference_table    = '';
                        if ($validation_rule === INPUT_VALIDATOR_GENERIC_LOOKUP){

                            if ($field_property->reference_rule !== null){
                                $arr = explode('.', $field_property->reference_rule);
                                $reference_table = $arr[0]; // get a table name from the reference rule
                               if (!Schema::hasTable($reference_table)){
                                   $message  = "The reference table for '$field_property->label' does not exist. Please contact administrator.";
                                    $validator->set_validator_message($field_name, $message);
                                   continue;
                                }
                            }

                            $field_value = array($field_value, $reference_table);
                        }

                        if ($this->upload_mode == 2 && $field_name === "personnelnumber"){
                            $is_type_validated =  $validator->validateType($field_value, INPUT_VALIDATOR_UNIQUE, $this->table, $field_property->field_name, $field_property->label, 1);
                        }
                        else {
                            $is_type_validated =  $validator->validateType($field_value, $validation_rule, $this->table, $field_property->field_name, $field_property->label, 1);
                        }

                       if ($is_type_validated === false){

                           $line = [
                               'lineno'         => $lineno,
                               'personnelno'    => $row['personnelnumber'],
                               'field'          => $field_name,
                               'label'          => $field_property->label,
                               'status'         => 'error',
                               'msg'            => $validator-> message($field_name),
                               'value'          => $field_value
                           ];
                       }
                       else{

                           //validate(&$field, $field_name = 'Field', $blank = false, $numeric = false, $maximum_length = 255, $minimum_length = 0)
                           $res = $validator->validate($field_value, $field_name, $field_property->required === 0 ? true:false, $field_property->uni_fieldtype_id === 4, $field_property->max === null ? 65535:$field_property->max,$field_property->min === null ? 0:$field_property->min);
                           if ($res === false){
                               $line = [
                                   'lineno'         => $lineno,
                                   'personnelno'    => $row['personnelnumber'],
                                   'field'          => $field_name,
                                   'label'          => $field_property->label,
                                   'status'         => 'error',
                                   'msg'            => $validator-> message($field_name),
                                   'value'          => $field_value
                               ];
                           }
                       }

                        if ($line)
                            $this->rows_failed[] = $line;
                    }
                }
            }

        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public function getTableWriteFields($tableName){
        try{
            if (is_numeric($tableName)){
                $records =  DB::table('uni_field_write')->select('*')
                    ->where('uni_table_id', $tableName)
                    ->get();
                $temp_array = [];

                foreach ($records as $record){
                    $temp_array[$record->field_name] = $record;
                }
                return $records;
            }
            else{
                $uni_table_id = DB::table('uni_table')->select('id')
                    ->where('name', $tableName)
                    ->first();
                $records =  DB::table('uni_field_write')->select('*')
                    ->where('uni_table_id', $uni_table_id->id)
                    ->get();

                $temp_array = [];
                if ($records){
                    foreach ($records as $record){
                        $temp_array[$record->field_name] = $record;
                    }
                }
                return $temp_array;
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public function setTable($table){
        $this->table = $table;
    }
}

<?php

namespace App\Imports;

use App\Models\Audit;
use App\Models\Employee;
use App\Models\EmployeeAssessor;
use App\Models\EmployeeBenchmarks;
use App\Models\EmployeeIncentive;
use App\Models\EmployeePerformance;
use App\Models\EmployeeSalary;
use App\Models\FranchiseMapping;
use App\Models\FranchiseOneLevels;
use App\Models\FranchiseThreeLevels;
use App\Models\FranchiseTwoLevels;
use App\Models\OrgStructure;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EmployeeImport implements ToCollection, WithHeadingRow
{
    public $import_personnel_numbers = [];
    /**
     * @param Collection $rows
     * @return array
     */
    public function collection(Collection $rows)
    {
        ini_set('max_execution_time', 21100);
        ini_set('memory_limit', '-1');
        try{
            foreach ($rows as $row)
            {
                $this->import_personnel_numbers[] = $row['personnelnumber'];
                // 1. create/update employee
                $res = Employee::saveEmployees($row);
                if ($res['error'] === false)
                    $employee = $res['response'];
                else
                   return $res;

                // Performances
                 $employee_performance  = EmployeePerformance::saveEmployeePerformances($employee, $row);
                // Salaries
                 $employee_salary       = EmployeeSalary::saveEmployeeSalaries($employee, $row);
                // Incentives
                $employee_incentive     = EmployeeIncentive::saveEmployeeIncentives($employee, $row);
                // Org-Structure
                $instance = null;
                if (isset($row['instance_id'])){
                    $instance = $row['instance_id'];
                }

                $region = null;
                if (isset($row['region_id'])){
                    $region = $row['region_id'];
                }

                $country = null;
                if (isset($row['country_id'])){
                    $country = $row['country_id'];
                }

                $company = null;
                if (isset($row['company_id'])){
                    $company = $row['company_id'];
                }

                $business_area = null;
                if (isset($row['business_area_id'])){
                    $business_area = $row['business_area_id'];
                }

                $business_unit = null;
                if (isset($row['business_unit_id'])){
                    $business_unit = $row['business_unit_id'];
                }

                $division = null;
                if (isset($row['division_id'])){
                    $division = $row['division_id'];
                }

                $department = null;
                if (isset($row['department_id'])){
                    $department = $row['department_id'];
                }

                //if all org structure fields are not up uploaded then do not update the org structure
                if($department === null && $business_unit === null && $business_area === null && $company === null && $country === null && $region === null && $instance === null ){
                    // do nothing
                }
                else{
                    $org_structure = OrgStructure::orgStructureMapping($employee, $instance, $region, $country, $company, $business_area, $business_unit, $division, $department);
                    $employee->org_structure_id = $org_structure->id;
                }

                // Benchmarks
                $employee_benchmarks = EmployeeBenchmarks::saveEmployeeBenchmarks($employee, $row);
                // Assessors
                $employee_assessor  = EmployeeAssessor::mapEmployeeAssessors($employee, $row);
                Audit::AuditTrailPerModel('employees', $employee, $employee->personnelnumber);

                $franchise_one_level_id = null; $franchise_two_level_id = null; $franchise_three_level_id = null;

                if (isset($row['franchise_one_level_id']) || isset($row['franchise_two_level_id']) || isset($row['franchise_three_level_id'])){

                    if (isset($row['franchise_one_level_id'])){
                        $franchise_one_level = FranchiseOneLevels::where('name', $row['franchise_one_level_id'])->first();
                        if ($franchise_one_level){
                            $franchise_one_level_id = $franchise_one_level->id;
                        }
                    }

                    if (isset($row['franchise_two_level_id'])){
                        $franchise_two_level = FranchiseTwoLevels::where('name', $row['franchise_two_level_id'])->first();
                        if ($franchise_two_level){
                            $franchise_two_level_id = $franchise_two_level->id;
                        }
                    }

                    if (isset($row['franchise_three_level_id'])){
                        $franchise_three_level = FranchiseThreeLevels::where('name', $row['franchise_three_level_id'])->first();
                        if ($franchise_three_level){
                            $franchise_three_level_id = $franchise_three_level->id;
                        }
                    }

                    $franchise_mapping = FranchiseMapping::where('incentive_scheme_id', $employee_incentive->incentive_scheme_id)
                        ->where('franchise_one_level_id', $franchise_one_level_id)
                        ->where('franchise_two_level_id', $franchise_two_level_id)
                        ->where('franchise_three_level_id', $franchise_three_level_id)->first();

                    if ($franchise_mapping === null){
                        $franchise_mapping = new FranchiseMapping();
                        $franchise_mapping->incentive_scheme_id         = $employee_incentive->incentive_scheme_id;
                        $franchise_mapping->franchise_one_level_id      = $franchise_one_level_id;
                        $franchise_mapping->franchise_two_level_id      = $franchise_two_level_id;
                        $franchise_mapping->franchise_three_level_id    = $franchise_three_level_id;
                        $franchise_mapping->save();

                    }

                    $employee->franchise_mapping_id = $franchise_mapping->id;
                }

                $employee->save();
                $employee->refreshCalculations();
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }
}

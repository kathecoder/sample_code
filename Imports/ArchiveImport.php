<?php

namespace App\Imports;

use App\Models\Cycle2019Record;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ArchiveImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $columns = DB::select("describe cycle2019_records");
        ini_set('max_execution_time', 21100);
        ini_set('memory_limit', '-1');
        try {
            foreach ($rows as $row) {
                $employee = Cycle2019Record::where('personnelnumber', $row['personnelnumber'])->first();
                if ($employee === null){
                    $employee = new Cycle2019Record();
                    $employee->personnelnumber = $row['personnelnumber'];

                    foreach ($columns as $column){
                        $col = $column->Field;
                        if (isset($row[$col])){
                            $employee->{$col} = $row[$col];
                        }
                    }

                    $employee->save();
                }
            }
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
}

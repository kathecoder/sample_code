<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class OTPMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $opt;
    public $name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->opt  = $user->otp;
        $this->name = $user->name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.otp')->with([
            'otp' => $this->opt,
            'name' => $this->opt,
        ])->subject('#: OTP');
    }
}

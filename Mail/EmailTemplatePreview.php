<?php

namespace App\Mail;

use App\Models\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\URL;

class EmailTemplatePreview extends Mailable
{
    use Queueable, SerializesModels;
    private $email = null;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $body = str_replace('base_path', URL::to('/'), $this->email->html_template);
        return $this->subject('Email Template Preview')->view('_pages.mails.mailbody')
            ->with([
                'header'    => $this->email->getheader(),
                'body'      => $body,
            ]);
    }
}

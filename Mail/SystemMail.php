<?php

namespace App\Mail;

use App\Models\Email;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\URL;

class SystemMail extends Mailable
{
    use Queueable, SerializesModels;
    private $email;
    private $tags;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Email $email, $tags)
    {
        $this->email = $email;
        $this->tags = $tags;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $body = $this->email->html_template;
        foreach ($this->tags as $tag => $value){
            $body = str_replace($tag, $value, $body);
        }

        return $this->subject($this->email->subject)->view('mail.mailbody')
            ->with([
                'header'    => $this->email->getheader(),
                'body'      => $body,
            ]);
    }
}

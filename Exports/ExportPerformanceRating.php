<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;

class ExportPerformanceRating implements FromArray, ShouldAutoSize, WithHeadings, WithEvents
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        $headings = [
            'Area',
            'Headcount',
            'Not Achieved',
            'Partially Achieved',
            'Fully Achieved',
            'Exceptionally Achieved',
            'TEA',
            'Unknown Rating',
        ];
        return $headings;
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {

                $user = \Auth::user();
                $active_instance = \Session::has('active_instance') ? \Session::get('active_instance') : $user->sub_owner_id;

                $sub_owner_id = $active_instance;
                if ($sub_owner_id != 1) {
                    //
                    // style heading
                    $styleArray = [
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['rgb' => '000000'],
                            ],
                        ],
                        'font' => [
                            'bold' => true,
                            'color' => ['rgb' => 'FFFFFF'],
                            'name' => 'Arial',
                        ],
                        'fill' => [
                            'fillType'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['rgb' => '00164E'],
                        ],
                    ];
                } else {
                    // style heading
                    $styleArray = [
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['rgb' => '000000'],
                            ],
                        ],
                        'font' => [
                            'bold' => true,
                            'color' => ['rgb' => 'FFFFFF'],
                            'name' => 'Arial',
                        ],
                        'fill' => [
                            'fillType'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['rgb' => '00164E'],
                        ],
                    ];
                }


                $event->sheet->getStyle('A1:X1')->applyFromArray($styleArray);

                $sheet = $event->getSheet();
                $sheet->freezePane('E2');
                $cellColl = $sheet->getCellCollection()->getHighestRowAndColumn();
                $lastCell = $cellColl['column'].$cellColl['row'];
                $cells = 'A1:'.$lastCell;

                $styleArray2 = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                    'font' => [
                        'name' => 'Arial',
                    ],
                ];

                /* *
                    'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                        ],
                 * */
                $event->sheet->getStyle($cells)->applyFromArray($styleArray2);

                $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(20);
            },
        ];
    }

 /*   public function drawings()
    {
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();*/

        /*$drawing->setName('Logo');
        $drawing->setDescription('Logo');
        $user = \Auth::user();
        $active_instance = \Session::has('active_instance') ? \Session::get('active_instance') : $user->sub_owner_id;

        $sub_owner_id = $active_instance;

        if ($sub_owner_id == 1)
            $drawing->setPath(public_path('/img/export-logo3.png'));
        else
            $drawing->setPath(public_path('/img/export_stanlib.png'));*/

/*        $drawing->setHeight(55);
        $drawing->setCoordinates('A1');

        return $drawing;
    }*/

    // sheet name
/*    public function title(): string
    {
        return "REMSYS: Performance Rating Report";
    }*/
}

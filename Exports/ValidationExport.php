<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class ValidationExport implements FromArray, ShouldAutoSize, WithHeadings, WithEvents
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array //same order as import
    {
        $headings = [
            "Line Number",
            "Personnel Number",
            "Field",
            "Label",
            "Status",
            "Message",
            "Value"
        ];
        return $headings;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // make headings bold
                $event->sheet->getStyle('A1:G1')->applyFromArray([
                    'font' => [
                        'bold' => true,
                        'name' => 'Arial',
                    ],
                ]);
                $sheet = $event->getSheet();
                //$sheet->freezePane('D2');

                $cellColl = $sheet->getCellCollection()->getHighestRowAndColumn();
                $lastCell = $cellColl['column'].$cellColl['row'];
                $cells = 'A1:'.$lastCell;
                $styleArray2 = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                    'font' => [
                        'name' => 'Arial',
                    ],
                ];
                $event->sheet->getStyle($cells)->applyFromArray($styleArray2);
            },
        ];
    }
}

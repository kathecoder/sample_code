<?php

namespace App\Exports;

use App\Http\Controllers\PermissionController;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class CustomEarningDistributionExport implements FromArray, ShouldAutoSize, WithHeadings, WithEvents
{
    protected $data;
    protected $headers;

    public function __construct(array $data, $headers)
    {
        $this->data = $data;
        $this->headers = $headers;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array //same order as import
    {
        $headings = $this->headers;
        return $headings;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {

                $active_instance =  PermissionController::getUserInstance()['response']['instance'];

                $sub_owner_id = $active_instance;
                if ($sub_owner_id == 3) {
                    // style heading
                    $styleArray = [
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['rgb' => '000000'],
                            ],
                        ],
                        'font' => [
                            'bold' => true,
                            'color' => ['rgb' => 'FFFFFF'],
                            'name' => 'Arial',
                        ],
                        'fill' => [
                            'fillType'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['rgb' => 'rgba(13, 13, 13, 0.99)'],
                        ],
                    ];

                    $event->sheet->getStyle('A1:CD1')->applyFromArray($styleArray);
                }
                elseif ($sub_owner_id == 2) {
                    // style heading
                    $styleArray = [
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['rgb' => '000000'],
                            ],
                        ],
                        'font' => [
                            'bold' => true,
                            'color' => ['rgb' => 'FFFFFF'],
                            'name' => 'Arial',
                        ],
                        'fill' => [
                            'fillType'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['rgb' => '00164E'],
                        ],
                    ];

                    $event->sheet->getStyle('A1:CD1')->applyFromArray($styleArray);
                }
                elseif ($sub_owner_id == 1) {
                    // style heading
                    $styleArray = [
                        'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['rgb' => '000000'],
                            ],
                        ],
                        'font' => [
                            'bold' => true,
                            'color' => ['rgb' => 'FFFFFF'],
                            'name' => 'Arial',
                        ],
                        'fill' => [
                            'fillType'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['rgb' => '00164E'],
                        ],
                    ];

                    $event->sheet->getStyle('A1:CD1')->applyFromArray($styleArray);
                }




                $sheet = $event->getSheet();
                //$sheet->freezePane('E2');
                $cellColl = $sheet->getCellCollection()->getHighestRowAndColumn();
                $lastCell = $cellColl['column'].$cellColl['row'];
                $cells = 'A1:'.$lastCell;

                $styleArray2 = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                    'font' => [
                        'name' => 'Arial',
                    ],
                ];
                $event->sheet->getStyle($cells)->applyFromArray($styleArray2);

                $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(18);
            },
        ];
    }
}

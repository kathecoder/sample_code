<?php

namespace App\Http\Controllers;
use App\CFLibrary\TableEditHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TableEditorController extends Controller
{
    public function getEditTableHeaders(Request $request)
    {
        return TableEditHandler::getTableHeader($request);
    }
    public function getTableEditor(Request $request)
    {
        return TableEditHandler::getTableEditor($request, Auth::user()->id);
    }
}

<?php

namespace App\Http\Controllers;

use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use App\Models\Employee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Audit;
use Illuminate\Support\Facades\DB;

class AuditTrailController extends Controller
{

   protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }
   
   public function create(Request $request)
   {
             
        return true;
   }

   public function getCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        if(isset($request['data'])){
            $data = $request['data'];
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];
                
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }else{
                        $id = '';
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        $panel = new TableViewerPanel('audits', 'R', $mode, $selections);
        $table = $panel->show();
        $card_template = $table['content'];
        if($mode == "new"){
            $id = 0;
        }
        
        Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }
    
    public function audits(Request $request){
        ini_set('max_execution_time', 21100);
        ini_set('memory_limit', '-1');
        try {
            $startDate  = isset($request['startDate']) ? $request['startDate'] : null;
            $endDate    = isset($request['endDate']) ? $request['endDate'] : null;


            $res        = Audit::getAudits($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $startDate, $endDate);// Log::debug($res);
            $dataTotal  = Audit::getAudits($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $startDate, $endDate);

            $array = [];
            foreach ($res as $item){
                if(!empty($item->value_before) && $item->value_before!= ''){
                    $value_before_checked = self::convertFieldValue($item->field_name,$item->value_before);
                }
                else{
                    $value_before_checked = $item->value_before;
                }
                if(!empty($item->value_after) && $item->value_after != ''){
                    $value_after_checked = self::convertFieldValue($item->field_name,$item->value_after);
                }
                else{
                    $value_after_checked = $item->value_after;
                }
                
                $array[] = array(
                    'employee_personnel_number'=> $item->employee_personnel_name,
                    'user_personnel_number'=>$item->user_personnel_number,
                    'field_name'=>$item->field_name,
                    'value_before'=>$value_before_checked,
                    'value_after'=>$value_after_checked,
                    'created_at'=>$item->created_at,
                );
            }
            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>$this->pageNationController->index(count($dataTotal),$request->state['take']),
                    'totalTableData'=>count($dataTotal)
                ),
                'message'=>'Audits successfully Loaded'
            );
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get Audits');
        }
    }
    
    public function convertFieldValue($fieldname,$value){
        
        $convert_value = '';
        switch($fieldname){
            case 'cycle_id' :
                $cycle_id_sql = DB::table('cycles')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'instance_indicator_id' :
                $cycle_id_sql = DB::table('instance_indicators')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'title_id' :
                $cycle_id_sql = DB::table('titles')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'gender_id' :
                $cycle_id_sql = DB::table('genders')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'race_id' :
                $cycle_id_sql = DB::table('races')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'status_id' :
                $cycle_id_sql = DB::table('statuses')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'status_reason_id' :
                $cycle_id_sql = DB::table('status_reason')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'years_of_service_id' :
                $cycle_id_sql = DB::table('years_of_services')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'band_id' :
                $cycle_id_sql = DB::table('bands')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'milestone_id' :
                $cycle_id_sql = DB::table('milestones')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'remco_category_id' :
                $cycle_id_sql = DB::table('remco_categories')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'occupational_category_id' :
                $cycle_id_sql = DB::table('occupational_categories')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'occupational_level_id' :
                $cycle_id_sql = DB::table('occupational_levels')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'charterband_id' :
                $cycle_id_sql = DB::table('charterbands')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'current_rating_id' :
                $cycle_id_sql = DB::table('current_ratings')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'h_1_performance1' :
                $cycle_id_sql = DB::table('current_ratings')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'h_2_performance1' :
                $cycle_id_sql = DB::table('current_ratings')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'critical_assessment' :
                $cycle_id_sql = DB::table('current_ratings')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'talent_indicator' :
                $cycle_id_sql = DB::table('current_ratings')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'salary_currency_id' :
                $cycle_id_sql = DB::table('currencies')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'h_1_salary_currency' :
                $cycle_id_sql = DB::table('currencies')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                
                break;
            case 'proposedsalary_currency_id' :
                $cycle_id_sql = DB::table('currencies')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                break;
            case 'incentive_scheme_id' :
                $cycle_id_sql = DB::table('incentive_schemes')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                
                break;
            case 'incentive_scheme' :
                $cycle_id_sql = DB::table('incentive_schemes')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;

                break;
            case 'participation_category_id' :
                $cycle_id_sql = DB::table('participation_categories')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                
                break;
            case 'participation_category_id' :
                $cycle_id_sql = DB::table('participation_categories')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                
                break;
            case 'proposedincentive_currency_id' :
                $cycle_id_sql = DB::table('currencies')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;
                break;
            
            case 'validation_manager_1' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'validation_manager_2' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'validation_manager_3' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;      
                break;
            case 'validation_manager_4' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'validation_manager_5' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'allocation_manager_1' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'allocation_manager_2' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'allocation_manager_3' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'allocation_manager_4' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'allocation_manager_5' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
                
            case 'business_manager_1' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'business_manager_2' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'business_manager_3' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'business_manager_4' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'business_manager_5' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
                
            case 'business_area_manager_1' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'business_area_manager_2' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'business_area_manager_3' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'business_area_manager_4' :
               $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'business_area_manager_5' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
                
            case 'hrbp_1' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'hrbp_2' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'hrbp_3' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'hrbp_4' :
               $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'hrbp_5' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
             case 'hrbp_6' :
                $cycle_id_sql = DB::table('employees')
                    ->where('personnelnumber', $value)
                    ->select('knownas','lastname')->first();
                $convert_value = $cycle_id_sql->knownas.' '.$cycle_id_sql->lastname;    
                break;
            case 'benchmark_id' :
                $cycle_id_sql = DB::table('benchmarks')
                    ->where('id', $value)
                    ->select('title')->first();
                $convert_value = $cycle_id_sql->title;
                break;
            case 'instance_id' :
                $cycle_id_sql = DB::table('instances')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                break;
            case 'region_id' :
                $cycle_id_sql = DB::table('regions')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                break;
            case 'country_id' :
                $cycle_id_sql = DB::table('countries')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                break;
            case 'company_id' :
                $cycle_id_sql = DB::table('companies')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                break;
            case 'business_area_id' :
                $cycle_id_sql = DB::table('business_areas')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                break;
            case 'business_unit_id' :
                $cycle_id_sql = DB::table('business_units')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                break;
            case 'division_id' :
                $cycle_id_sql = DB::table('divisions')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                break;
            case 'department_id' :
                $cycle_id_sql = DB::table('departments')
                    ->where('id', $value)
                    ->select('name')->first();
                $convert_value = $cycle_id_sql->name;    
                break;
            default:
                $convert_value = $value;
            
        }
        
        return $convert_value;
    }

}

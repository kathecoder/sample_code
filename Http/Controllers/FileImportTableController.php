<?php

namespace App\Http\Controllers;

use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use App\Models\FileImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class FileImportTableController extends Controller
{
    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }
   
   public function create(Request $request)
   {
             
        return true;
   }

   public function getCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        if(isset($request['data'])){
            $data = $request['data'];
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];
                
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }else{
                        $id = '';
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        $panel = new TableViewerPanel('imports', 'S', $mode, $selections);
        $table = $panel->show();
        $card_template = $table['content'];
        if($mode == "new"){
            $id = 0;
        }
        
        //Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }

    public function getValidationFile($file) {
        ini_set('max_execution_time', 1100);
        ini_set('memory_limit', '-1');
       try{
           $file    = base64_decode($file);
           $import  = FileImport::where('validation_report', $file)->first();
           return response()->download(storage_path('app/' . $import->validation_report));
       }
       catch (\Exception $e) {
           $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
           Log::critical($msg);
           return array('error' => true, 'response' => [], 'message' => $msg);
       }

    }


    public function getImportFile($file) {
        ini_set('max_execution_time', 1100);
        ini_set('memory_limit', '-1');
       try{
           $file    = base64_decode($file);
           $import  = FileImport::where('path_source', $file)->first();
           return response()->download(storage_path('app/' . $import->path_source));
       }
       catch (\Exception $e) {
           $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
           Log::critical($msg);
           return array('error' => true, 'response' => [], 'message' => $msg);
       }

    }
    public function getFileImports(Request $request){

        try {

            $res = FileImport::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true);
            $total = FileImport::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'],false);

            $array = [];

            foreach ($res as $data){
                $array []= array(
                    'id'                        => $data->id,
                    'name'                      => $data->name,
                    'status'                    => $data->status,
                    'comment'                   => $data->comment,
                    'created_at'                => $data->created_at,
                    'path_source'               => URL::route('import_file',['file'=>base64_encode($data->path_source)]),//URL::to().base64_encode($data->path_source),
                    'validation_report'         => URL::route('validation_report',['file'=>base64_encode($data->validation_report)]),//$data->validation_report,
                    'path_report'               => $data->path_report,
                    'is_download_original'      =>$data->path_source !== null ? true : false,
                    'has_validation_report'     =>$data->validation_report != '0' ? true:false,
                );
            }
//            Log::warning('Check this out');
            return array (
                'error'=>false,
                'response'=>array(
                    'data'          =>$array,
                    'pageNate'      =>$this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>count($total)
                ),
                'message'=>'Data Import Files Loaded successfully');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get data import records');
        }
    }
}

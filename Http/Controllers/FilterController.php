<?php

namespace App\Http\Controllers;

use App\CFLibrary\FilterHandler;
use App\Models\Band;
use App\Models\BusinessArea;
use App\Models\BusinessUnit;
use App\Models\Country;
use App\Models\Department;
use App\Models\Division;
use App\Models\Employee;
use App\Models\EmployeeAssessor;
use App\Models\EmployeePerformance;
use App\Models\Gender;
use App\Models\IncentiveScheme;
use App\Models\issueLogsCategory;
use App\Models\ParticipationCategory;
use App\Models\PerformanceRating;
use App\Models\Race;
use App\Models\Scheme;
use App\Models\Status;
use App\Models\TargetBusinessUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\issueLogsSubCategory;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use App\Models\Filters;
use App\Models\IssueLog;

class FilterController extends Controller
{
    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }
    public function getFilterList(Request $request)
    {

        return  FilterHandler::getFilterList($request);
    }
    public function getFilters(Request $request)
    {

        try {
            $data = [];
            $dataTotal = [];

            switch ($request->type)
            {
                case "countries":
                    $data = Country::getALlCountries($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                    $dataTotal = Country::getALlCountries($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                    break;
                case "business_areas":
                    $data = BusinessArea::getALlBusinessArea($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                    $dataTotal = BusinessArea::getALlBusinessArea($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                break;
                case "business_units":
                    $data = BusinessUnit::getALlBusinessUnits($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                    $dataTotal = BusinessUnit::getALlBusinessUnits($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                    break;
                case "divisions":
                    $data = Division::getAllDivisions($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                    $dataTotal = Division::getAllDivisions($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                    break;
                case "races":
                    $data = Race::getAllRaces($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                    $dataTotal = Race::getAllRaces($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                    break;
                case "performance_ratings":
                    $data = PerformanceRating::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                    $dataTotal = PerformanceRating::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                    break;
                case "genders":
                    $data = Gender::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                    $dataTotal = Gender::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                    break;
                case "bands":
                    $data = Band::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                    $dataTotal = Band::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                    break;
                case "schemes":
                    $data = IncentiveScheme::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                    $dataTotal = IncentiveScheme::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                    break;
                case "allocation_managers":
                    $data = EmployeeAssessor::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters, 'allocation_manager_id');
                    $dataTotal = EmployeeAssessor::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters, 'allocation_manager_id');
                    break;
                case "business_area_managers":
                    $data = EmployeeAssessor::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters, 'business_area_manager_id');
                    $dataTotal = EmployeeAssessor::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters, 'business_area_manager_id');
                    break;
                case "business_managers":
                    $data = EmployeeAssessor::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters, 'business_manager_id');
                    $dataTotal = EmployeeAssessor::getAllRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters, 'business_manager_id');
                    break;
                /* case "departments":
                     $data = Department::getALlDepartments($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                     $dataTotal = Department::getALlDepartments($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                     break;
                 case "manager_name":
                     $data = Employee::managerName($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                     $dataTotal = Employee::managerName($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                     break;
                 case "target_business_units":
                     $data = TargetBusinessUnit::getTargetBusinessUnits($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                     $dataTotal = TargetBusinessUnit::getTargetBusinessUnits($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                     break;
                 case "schemes":
                     $data = Scheme::getALlSchemes($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                     $dataTotal = Scheme::getALlSchemes($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                     break;
                 case "participation_categories":
                     $data = ParticipationCategory::getAllParticipationCategories($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->filterFilters);
                     $dataTotal = ParticipationCategory::getAllParticipationCategories($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request->filterFilters);
                     break;
                 case "issue_category":
                     $data = issueLogsCategory::getFilterdIssueCategory($request->state['searchInput'], $request->state['skip'], $request->state['take'], true);
                     $dataTotal = issueLogsCategory::getFilterdIssueCategory($request->state['searchInput'], $request->state['skip'], $request->state['take'], false);
                     break;
                 case "issue_sub_category":
                     $data = issueLogsSubCategory::getFilterdSubCategory($request->state['searchInput'], $request->state['skip'], $request->state['take'], true);
                     $dataTotal = issueLogsSubCategory::getFilterdSubCategory($request->state['searchInput'], $request->state['skip'], $request->state['take'], false);
                     break;
                 case "status":
                     $data = Status::getFilterdIssueStatus($request->state['searchInput'], $request->state['skip'], $request->state['take'], true);
                     $dataTotal = Status::getFilterdIssueStatus($request->state['searchInput'], $request->state['skip'], $request->state['take'], false);
                     break;
                 case "logged_by":
                     $data = User::getLoggedBy($request->state['searchInput'], $request->state['skip'], $request->state['take'], true);
                     $dataTotal = User::getLoggedBy($request->state['searchInput'], $request->state['skip'], $request->state['take'], false);
                     break;*/
                default:
                    $data = [];
                    $dataTotal = [];
                    break;

            }

            $array = array();
            $total_selected = 0;
            foreach ($data as $item){

                if (in_array($request->type, ['allocation_managers', 'business_managers', 'business_area_managers'])){
                    $checked = Filters::searchFilters($request->type, $item->personnelnumber);

                    if($checked===true){
                        $total_selected++;
                    }
                    $array[] = array(
                        'id'=>$item->personnelnumber,
                        'name'=>$item->knownas . ' ' . $item->lastname,
                        'defaultChecked'=> $checked
                    );
                }
                else{
                    $checked = Filters::searchFilters($request->type, $item->id);

                    if($checked===true){
                        $total_selected++;
                    }
                    $array[] = array(
                        'id'=>$item->id,
                        'name'=>$item->name,
                        'defaultChecked'=> $checked
                    );
                }

            }

            $return_total_checked = 'All';
            if($total_selected>0){
                $return_total_checked = $total_selected."+";
            }
            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>$this->pageNationController->index(count($dataTotal),10),
                    'totalTableData'=>count($dataTotal),
                    'totalChecked'=>$return_total_checked
                ),
                'message'=>'Filters successfully Loaded'
            );

        }
        catch (\Exception $e) {

            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }


        //return FilterHandler::getFilters($request, Auth::user()->id);
    }
    public function createFilters(Request $request)
    {
        try {
            if (isset($request['page']) && $request['page'] == 'personnel_admin'){
                $res = Filters::createStatusFilters($request); // this filters are specific record status
            }
            else if(isset($request['id']) && $request['id']===null){
                $res = Filters::createFilter2($request->type, $request->created_at, $request->updated_at);
            }
            else{
                foreach ($request->filter_ids as $filter_id){
                    $res = Filters::createFilter($request->type, $filter_id);
                }
            }

            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
        //return FilterHandler::createFilters($request, Auth::user()->id);
    }

    public function clearFilters(Request $request)
    {
        try {
            $res = Filters::clearFilters();
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
        //return FilterHandler::clearFilters(Auth::user()->id);
    }
    public static function convertJsonIntoArray($json){
        $array = [];
        foreach ($json as $data){
            $array[] = $data->id;
        }
        return $array;
    }
    public static function convertJsonIntoArray2($json){

        $array = '';
        $count = 0;
        foreach ($json as $data){
            if($count===0){
                $array = $array. ''.$data->id;
            }else{
                $array = $array. ','.$data->id;
            }
            $count++;

        }
        return $array;
    }
    public static function convertJsonIntoArrayIndex2($json){
        $array = '';
        $count = 0;
        foreach ($json as $data){
            if($count===0){
                $array = $array. ''.$data['id'];
            }else{
                $array = $array. ','.$data['id'];
            }
            $count++;

        }
        return $array;
    }
    public static function convertJsonIntoArrayIndex($json){
        if(count($json)>0){
            $array = [];
            foreach ($json as $data){
                $array[] = $data['id'];
            }
            return $array;
        }else{
            return [];
        }


    }
    public static function searchArray($array, $value){
        foreach ($array as $item){
            if($item===$value){
                return true;
            }
        }
        return false;
    }

    //custom filtration
    public static function getFilteredUsers()
    {
        $res    =   DB::table('uni_user_filtration')
            ->join('uni_filters', 'uni_filters.id', 'uni_user_filtration.uni_filter_id')
            ->join('uni_table', 'uni_table.id', 'uni_filters.uni_table_id')
            ->where('uni_user_filtration.user_id', Auth::user()->id)
            ->where('uni_table.name', 'roles')
            ->select('uni_user_filtration.value as id')
            ->get();
        $users  =   DB::table('users')
            ->get();
        return self::convertJsonIntoArray($users);
    }

    /**
     * @param Request $request
     * @return array
     * Building an array of filters per table name
     */
    public function getFiltersIssueLog(Request $request){

        try {
            $data = [];
            $dataTotal = [];

            switch ($request->type)
            {
                case "issue_category":
                    $data = issueLogsSubCategory::getFilterdSubCategory($request->state['searchInput'], $request->state['skip'], $request->state['take'], true);
                    $dataTotal = issueLogsSubCategory::getFilterdSubCategory($request->state['searchInput'], $request->state['skip'], $request->state['take'], false);
                    break;
                case "logged_by":
                    $data = User::getLoggedBy($request->state['searchInput'], $request->state['skip'], $request->state['take'], true);
                    $dataTotal = User::getLoggedBy($request->state['searchInput'], $request->state['skip'], $request->state['take'], false);
                    break;
                default:
                    $data = [];
                    $dataTotal = [];
                    break;

            }

            $array = array();
            $total_selected = 0;
            foreach ($data as $item){
                $checked = Filters::searchFilters($request->type, $item->id);

                if($checked===true){
                    $total_selected++;
                }
                $array[] = array(
                    'id'=>$item->id,
                    'name'=>$item->name,
                    'defaultChecked'=> $checked
                );
            }

            $return_total_checked = 'All';
            if($total_selected>0){
                $return_total_checked = $total_selected."+";
            }
            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>$this->pageNationController->index(count($dataTotal),10),
                    'totalTableData'=>count($dataTotal),
                    'totalChecked'=>$return_total_checked
                ),
                'message'=>'Filters successfully Loaded',
                'data'=> $array
            );

        } catch (\Exception $e) {

            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

}

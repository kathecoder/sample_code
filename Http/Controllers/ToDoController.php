<?php

namespace App\Http\Controllers;

use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;
use App\Models\Cycle;
use App\Models\Status;
use App\Models\ToDo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Exceptions\Handler;
use Illuminate\Log\Logger;
use App\Models\UserCycle;


class ToDoController extends Controller
{
    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }


    public function create(Request $request)
   {
             
        return true;
   }

   public function getCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        if(isset($request['data'])){
            $data = $request['data'];
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];
                
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }else{
                        $id = '';
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        $form_submit_data = array();
        if($mode == 'save'){
            if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                        //print_r($request['data']);
                        $item_field = '';
                        $dat_split = explode('_',$key);
                        $count_split = count($dat_split);
                        for($j=0;$j<($count_split - 1);$j++){
                            $item_field .= $dat_split[$j].'_';
                        }
                        $item_field = substr($item_field,0,(strlen($item_field) - 1));
                        $item_value = $dat;
                        $form_submit_data[$item_field] = $item_value;
                        if($item_field == 'id'){
                            $selections['field_name'] = $item_field;
                            $selections['field_value'] = $item_value;
                        }
                        $i++;
                }
            }
        }
        
        // $user_roles = PermissionController::getRolesMapping();
        $where[]['user_id'] = Auth::user()->id;
        
        $panel = new TableViewerPanel('todos', 'CRU', $mode, $selections, $form_submit_data, 'todos','vertical', $where);
        $table = $panel->show();
        $card_template = $table['content'];
        
        //Log::info("Portal Route");
        if($mode == "new"){
            $id = 0;
        }
        
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }

    public function getTodoRecords(Request $request){

        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        if($request->state['take']===null){
         }
        // Log::warning("The value of selected: ".$request['selected']);
        // $selectedValue  = isset($request['selected']) ? $request['selected'] : null;
 

        $res    = ToDo::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, null, $request['selected']);
        $total  = Todo::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, null, $request['selected']);

        $array = [];

        foreach ($res as $data){
            $array []= array(
                'id'            => $data->id,
                'name'          => $data->name,
                'status'        => $data->status,
                'status_ids'     => $data->status,
                'created_at'   => $data->created_at,
                'updated_at'   => $data->updated_at,
                'subject'       => $data->subject,
            );
        }
        return array (
            'error'=>false,
            'response'=>array(
                'data'=>$array,
                'pageNate'=>$this->pageNationController->index(count($total),$request->state['take']),
                'totalTableData'=>count($total),
                'currentCycle'=>Cycle::getLiveCycleID()
            ),
            'message'=>'Issue Log Loaded'
        );
    }

    public function saveUpdateTodo(Request $request){

            $status     = Status::find((int)$request->status);
            $subject    = $request->subject;
            $body       = $request->body;
            $record_id  = $request->record_id;
            $user_id =  Auth::user()->id;

            $object = Todo::find($record_id);
 


        if ($record_id === null || $record_id == 0 ){
            $object = new ToDo();
        }
        
        try {
        $object->status     = $status->name;
        $object->subject    = $subject;
        $object->body       = $body;
        $object->user_id    = $user_id;
        $object->created_at = date('Y-m-d H:i:s');
        $object->cycle_id   = Cycle::getLiveCycleID();
        $object->save();
        } catch (\Throwable $th) {
            return [
                'message'=> 'error! fields cannot be empty.',
                'message-log'=> $th,
                'error'=>true
            ];
        }

       
    }

    public function UpdateTodo(Request $request){
        
        if((int)$request->status){
            $statusCode = (int)$request->status;
        }else{
             switch ($request->status) {
            case 'Active':
                $statusCode = 10;
                break;
            case 'Inactive':
                $statusCode = 11;
                break;
            
            default:
                null;
                break;
        }
        }

       

        $status     = Status::find($statusCode);
        $record_id  = $request->record_id;
        $object = Todo::find($record_id);

        try {
            $update_record = DB::table('todos')
            ->where('id',$object->id)
            ->update(['subject' => $request->subject,'body' => $request->body,'status' => $status->name,'updated_at' =>date("Y-m-d H:i:s")]);
    
                return [
                    'message'=>"success"
                ];
        } catch (\Throwable $th) {
            return [
                'message'=> 'error! fields cannot be empty.',
                'message-log'=> $th,
                'error'=>true
            ];
        }
 
       


    }


    public function getSingleRecord(Request $request){

        $record     = $request->record_id;
        $data = DB::table('todos')
                    ->where('todos.id', '=', $record)
                    ->select('todos.*')
                    ->get();

        switch ($data[0]->status) {
            case 'Active':
                $statusCode = 10;
                break;
            case 'Inactive':
                $statusCode = 11;
                break;
            
            default:
                null;
                break;
        }
                     
        return [
            'data'=>$data,
            'status_code'=>$statusCode
        ];
    
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\DataField;
use App\Models\Filters;
use App\Models\ManagerType;
use App\Models\Country;

use App\CFLibrary\FilterHandler;
use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use App\Models\EmployeeSalary;
use App\Models\EmployeeIncentive;
use App\Models\Employee;

use App\Models\ManagerTypeUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class IncentiveAllocationCalcController extends Controller
{

    public function __construct()
    {

    }

    public function create(Request $request)
    {

        return true;
    }

    public function getCardPanel(Request $request)
    {

        $data = [];
        $mode = 'read';
        $id = '';


        $card_template = '';

        if ($mode == "new") {
            $id = 0;
        }


        /*SET PERMISSION PER INSTANCE,PHASE(IF ANY),USER(IF ANY) etc.*/
        $instance_id = NULL;
        $instance_name = '';
        $user_pers_no = PermissionController::getUserPersonnelNumber();
        $user_instance = PermissionController::getUserInstance();
        if ($user_instance['message'] == 'Success') {
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }
        $where[]['instance_indicator_id'] = $instance_id;

        //print_r($res);
        $user_roles = PermissionController::getRolesMapping();
        if (isset($request['tab']) && !empty($request['tab']) && $request['tab'] === 'start_tab') {
            $current_tab = '';
            //get first table tab according to perms
            if (isset($user_roles['response']['type']['is_allocator']) && $user_roles['response']['type']['is_allocator'] === 1) {
                $current_tab = 'is_allocator';
                $content_header = 'Allocation Manager';
            } else if (isset($user_roles['response']['type']['is_area_mgt']) && $user_roles['response']['type']['is_area_mgt'] === 1) {
                $current_tab = 'is_area_mgt';
                $content_header = 'Business Area Manager';
            } else if (isset($user_roles['response']['type']['is_hcbp']) && $user_roles['response']['type']['is_hcbp'] === 1) {
                $current_tab = 'is_hcbp';
                $content_header = 'Human Capital Business Partner (HCBP)';
            } else if (isset($user_roles['response']['type']['is_viewer']) && $user_roles['response']['type']['is_viewer'] === 1) {
                $current_tab = 'is_viewer';
                $content_header = 'Reward Administrators and/or Viewer';
            } else {
                $current_tab = 'start_tab';
                $content_header = 'Allocation';
            }
        } else {
            $current_tab = $request['tab'];
            if (isset($request['tab_name']) && !empty($request['tab_name']) && $request['tab_name'] !== '') {
                $content_header = $request['tab_name'];
            }
        }
        $custom_html = $this->getCustomPanel();

        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/CalculationPanel.twig', [
            'instance' => $instance_id,
            'custom_table' => $custom_html,
        ]);
        $dataTable = $template_panel->show($template);
        $card_template = $dataTable;

        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data' => $data,
            'mode' => $mode,
            'id' => $id,
        );
    }

    private function getCustomPanel()
    {
        $html = '';

        //get instance
        $user_instance = PermissionController::getUserInstance();
        if ($user_instance['message'] == 'Success') {
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }

        $categories_tabs = $this->getCategoryTabs();
        $cnt_tabs = count($categories_tabs);

        $get_salary_pool_table = $this->getSalaryPoolCategoryTable();
        $get_calc_tabs_table = $this->getCalculationTabsTable($categories_tabs, $get_salary_pool_table);

        if ($instance_id == 3) {
            $get_salary_pool_table = null;
        }

        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('custom/allocation_group.twig', [
            'pool_table' => $get_salary_pool_table,
            'tabs_table' => $get_calc_tabs_table,
        ]);
        $html = $template_panel->show($template);

        return $html;
    }

    private function getCalculationTabsTable($categories_tabs, $get_salary_pool_table = null)
    {
        $html = '';
        $tables = array();
        $div_class = 'col-md-8';
        //get instance
        $user_instance = PermissionController::getUserInstance();
        if ($user_instance['message'] == 'Success') {
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }

        $cnt_tabs = count($categories_tabs);

        if (isset($instance_id) && $instance_id != NULL) {
            switch ($instance_id) {
                case '1':
                    $div_class = 'col-md-8';
                    $table_html = '';
                    foreach ($categories_tabs as $cat_tab) {

                        $table_fields = $this->getTableFields($cat_tab['name']);
                        //print_r($table_fields);
                        $table_html = '<div class="row">
                            <div class="col-md-6">
                                
                                <table class="table-striped table-bordered calculation-table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Entire Dataset</th>
                                    </tr>
                                    </thead>
                                    <tbody>';

                        foreach ($table_fields['entire'] as $fields) {
                            $table_html .= '<tr>
                                        <td> <b>' . $fields['label'] . '</b></td>
                                        <td width="30%"> <span class="pull-right" id="">' . $fields['value'] . '</span></td>
                                    </tr>';
                        }

                        $table_html .= '</tbody>
                                </table>
                            </div>
                            <div class="col-md-6">

                                <table class="table-striped table-bordered calculation-table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Filtered Dataset</th>
                                    </tr>
                                    </thead>
                                    <tbody>';

                        foreach ($table_fields['filter'] as $fields) {
                            $table_html .= '<tr>
                                        <td> <b>' . $fields['label'] . '</b></td>
                                        <td width="30%"> <span class="pull-right" id="">' . $fields['value'] . '</span></td>
                                    </tr>';
                        }

                        $table_html .= '</tbody>
                                </table>
                            </div>
                        </div>';

                        $tables[$cat_tab['tab_name']] = $table_html;
                    }
                    break;
                case '2':
                    $div_class = 'col-md-9';
                    $table_html = '';

                    //print_r($country_tabs);
                    foreach ($categories_tabs as $cat_tab) {
                        $country_tabs = $this->getCountryTabs($cat_tab['tab_name']);
                        $cnt_country_tabs = count($country_tabs);
                        foreach ($country_tabs as $country) {

                            $country_fields = $this->getTableFields($cat_tab['tab_name']);
                            //print_r($table_fields);
                            $country_table = '<div class="row">
                                <div class="col-md-6">

                                    <table class="table-striped table-bordered calculation-table">
                                        <thead>
                                        <tr>
                                            <th colspan="2">Entire Dataset</th>
                                        </tr>
                                        </thead>
                                        <tbody>';

                            foreach ($country_fields['entire'] as $fields) {
                                $country_table .= '<tr>
                                            <td> <b>' . $fields['label'] . '</b></td>
                                            <td width="30%"> <span class="pull-right" id="">' . $fields['value'] . '</span></td>
                                        </tr>';
                            }

                            $country_table .= '</tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">

                                    <table class="table-striped table-bordered calculation-table">
                                        <thead>
                                        <tr>
                                            <th colspan="2">Filtered Dataset</th>
                                        </tr>
                                        </thead>
                                        <tbody>';

                            foreach ($country_fields['filter'] as $fields) {
                                $country_table .= '<tr>
                                            <td> <b>' . $fields['label'] . '</b></td>
                                            <td width="30%"> <span class="pull-right" id="">' . $fields['value'] . '</span></td>
                                        </tr>';
                            }

                            $country_table .= '</tbody>
                                    </table>
                                </div>
                            </div>';


                            $tables_country[$country['tab_name']] = $country_table;
                        }
                        $template = new TemplateRenderer();
                        $template_panel = new TemplatePanel('custom/allocation_tabs.twig', [
                            'custom_settings' => [],
                            'tabs' => $country_tabs,
                            'tables' => $tables_country,
                            'cnt_tabs' => $cnt_country_tabs,
                            'div_class' => 'col-md-12',
                            'sub_tabs' => true
                        ]);
                        $table_html = $template_panel->show($template);

                        $tables[$cat_tab['tab_name']] = $table_html;
                    }
                    break;
                case '3':
                    $div_class = 'col-sd-12';
                    $table_html = '';


                    foreach ($categories_tabs as $cat_tab) {

                        $table_html = '<div class="row">';

                        if ($get_salary_pool_table != null) {
                            $get_salary_pool_table = $this->getSalaryPoolCategoryTable($cat_tab['tab_name']);
                            $table_html .= $get_salary_pool_table;
                        }

                        $table_fields = $this->getTableFields($cat_tab['tab_name']);
                        //print_r($table_fields);
                        $table_html .= '<div class="col-md-6">
                                
                                <table class="table-striped table-bordered calculation-table">
                                    <tbody>';

                        foreach ($table_fields as $fields) {
                            $table_html .= '<tr>
                                        <td> <b>' . $fields['label'] . '</b></td>';

                            $values = $fields['value'];
                            foreach ($values as $val) {
                                $table_html .= '<td width="20%"> <span class="pull-right" id="">' . $val . '</span></td>';
                            }
                            $table_html .= '</tr>';
                        }

                        $table_html .= '</tbody>
                                </table>
                            </div>
                            </div>';

                        $tables[$cat_tab['tab_name']] = $table_html;
                    }
                    break;
            }
        }

        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('custom/allocation_tabs.twig', [
            'custom_settings' => [],
            'tabs' => $categories_tabs,
            'tables' => $tables,
            'cnt_tabs' => $cnt_tabs,
            'div_class' => $div_class
        ]);
        $html = $template_panel->show($template);

        return $html;
    }

    private function getTableFields($category)
    {
        $table_fields = array();

        //get instance
        $user_instance = PermissionController::getUserInstance();
        if ($user_instance['message'] == 'Success') {
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }
        if (isset($instance_id) && $instance_id != NULL) {
            switch ($instance_id) {
                case '1':

                    switch ($category) {
                        case 'Salary':

                            /*NOTE: NEED TO ADD CURRENY TP THE LABELS: READ CURRENCY LOOKUPA ND VALUE FORM DATABASE*/
                            $total_salary = 0;
                            $total_new_salary = 0;
                            $propose_salary = 0;
                            $uplift_perc = 0.0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('personnelnumber')
                                ->where('cycle_id', 1)
                                ->where('instance_indicator_id', 1)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('salary_value', 'proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                                $propose_salary += floatval($salaries->proposedsalary_value);
                            }
                            $total_new_salary = $propose_salary - $total_salary;
                            if ($total_new_salary > 0) {
                                $uplift_perc = ($total_new_salary / $total_salary) * 100;
                            } else {
                                $uplift_perc = 0;
                            }
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $total_new_salary = number_format($total_new_salary, 2, ".", " ");
                            $uplift_perc = number_format($uplift_perc, 2, ".", " ");

                            $table_fields['entire'][0]['label'] = 'Current Total Salary (ZAR)';
                            $table_fields['entire'][0]['value'] = $total_salary . '';
                            $table_fields['entire'][1]['label'] = 'Proposed Increase allocation amount (ZAR)';
                            $table_fields['entire'][1]['value'] = $total_new_salary . '';
                            $table_fields['entire'][2]['label'] = 'New total salary amount (ZAR)';
                            $table_fields['entire'][2]['value'] = $propose_salary . '';
                            $table_fields['entire'][3]['label'] = 'Total percentage uplift salary';
                            $table_fields['entire'][3]['value'] = $uplift_perc . '%';

                            //get employee salary data per filters
                            //NOTE:STILL NEED TO ADD FILTERS
                            $total_salary = 0;
                            $total_new_salary = 0;
                            $propose_salary = 0;
                            $uplift_perc = 0.0;

                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('personnelnumber')
                                ->where('cycle_id', 1)
                                ->where('instance_indicator_id', 1)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }

                            $emp_salary = EmployeeSalary::select('salary_value', 'proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            foreach ($emp_salary as $salaries) {
                                $total_salary += $salaries->salary_value;
                                $propose_salary += $salaries->proposedsalary_value;
                            }
                            $total_new_salary = $propose_salary - $total_salary;
                            if ($total_new_salary > 0) {
                                $uplift_perc = ($total_new_salary / $total_salary) * 100;
                            } else {
                                $uplift_perc = 0;
                            }
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $total_new_salary = number_format($total_new_salary, 2, ".", " ");
                            $uplift_perc = number_format($uplift_perc, 2, ".", " ");

                            $table_fields['filter'][0]['label'] = 'Current selected salary (ZAR)';
                            $table_fields['filter'][0]['value'] = $total_salary . '';
                            $table_fields['filter'][1]['label'] = 'Proposed selected increase allocation amount (ZAR)';
                            $table_fields['filter'][1]['value'] = $total_new_salary . '';
                            $table_fields['filter'][2]['label'] = 'New selected salary amount (ZAR)';
                            $table_fields['filter'][2]['value'] = $propose_salary . '';
                            $table_fields['filter'][3]['label'] = 'Selected Percentage Uplift In Salary';
                            $table_fields['filter'][3]['value'] = $uplift_perc . '%';
                            break;

                        case 'General Staff Incentive':

                            /*NOTE: NEED TO ADD CURRENY TP THE LABELS: READ CURRENCY LOOKUPA ND VALUE FORM DATABASE*/
                            $total_proposedincentive = 0;
                            $total_new_salary = 0;
                            $suggested_incentive = 0;
                            $uplift_perc = 0.0;
                            $total_salary = 0;
                            $total_incentive_pool = 0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->where('incentive_scheme_id', 1)
                                ->where('instance_indicator_id', 1)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);
                            //get employee salary data 
                            $emp_salary = EmployeeIncentive::select('proposedincentive_value', 'suggested_incentive')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_proposedincentive += floatval($salaries->proposedincentive_value);
                                $suggested_incentive += floatval($salaries->suggested_incentive);
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('salary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                            }
                            if ($suggested_incentive > 0) {
                                $suggested_incentive = ($suggested_incentive / $cnt_emplyee);
                            } else {
                                $suggested_incentive = 0;
                            }
                            $total_incentive_pool = $total_salary * 0.68;
                            $incentive_pool_available = $total_proposedincentive - $total_incentive_pool;
                            $total_proposedincentive = number_format($total_proposedincentive, 2, ".", " ");
                            $suggested_incentive = number_format($suggested_incentive, 2, ".", " ");

                            $total_incentive_pool = number_format($total_incentive_pool, 2, ".", " ");
                            $incentive_pool_available = number_format($incentive_pool_available, 2, ".", " ");

                            $table_fields['entire'][0]['label'] = 'Total proposed incentive (ZAR)';
                            $table_fields['entire'][0]['value'] = $total_proposedincentive . '';
                            $table_fields['entire'][1]['label'] = 'Proposed incentive %';
                            $table_fields['entire'][1]['value'] = $suggested_incentive . '%';
                            $table_fields['entire'][2]['label'] = 'Total incentive pool (6.80%)';
                            $table_fields['entire'][2]['value'] = $total_incentive_pool;
                            $table_fields['entire'][3]['label'] = 'Total incentive pool available';
                            $table_fields['entire'][3]['value'] = $incentive_pool_available;

                            $total_proposedincentive = 0;
                            $total_new_salary = 0;
                            $suggested_incentive = 0;
                            $uplift_perc = 0.0;
                            $total_salary = 0;
                            $total_incentive_pool = 0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->where('incentive_scheme_id', 1)
                                ->where('instance_indicator_id', 1)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);
                            //get employee salary data 
                            $emp_salary = EmployeeIncentive::select('proposedincentive_value', 'suggested_incentive')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_proposedincentive += floatval($salaries->proposedincentive_value);
                                $suggested_incentive += floatval($salaries->suggested_incentive);
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('salary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                            }
                            if ($suggested_incentive > 0) {
                                $suggested_incentive = ($suggested_incentive / $cnt_emplyee);
                            } else {
                                $suggested_incentive = 0;
                            }
                            $total_incentive_pool = $total_salary * 0.68;
                            $incentive_pool_available = $total_proposedincentive - $total_incentive_pool;
                            $total_proposedincentive = number_format($total_proposedincentive, 2, ".", " ");
                            $suggested_incentive = number_format($suggested_incentive, 2, ".", " ");

                            $total_incentive_pool = number_format($total_incentive_pool, 2, ".", " ");
                            $incentive_pool_available = number_format($incentive_pool_available, 2, ".", " ");


                            $table_fields['filter'][0]['label'] = 'Selected proposed incentive (ZAR)';
                            $table_fields['filter'][0]['value'] = $total_proposedincentive . '';
                            $table_fields['filter'][1]['label'] = 'Selected Proposed incentive %';
                            $table_fields['filter'][1]['value'] = $suggested_incentive . '%';
                            $table_fields['filter'][2]['label'] = 'Selected incentive pool (6.80%)';
                            $table_fields['filter'][2]['value'] = $total_incentive_pool;
                            $table_fields['filter'][3]['label'] = 'Selected incentive pool available';
                            $table_fields['filter'][3]['value'] = $incentive_pool_available;
                            break;
                        case 'SM & Risk Incentive':

                            /*NOTE: NEED TO ADD CURRENY TP THE LABELS: READ CURRENCY LOOKUPA ND VALUE FORM DATABASE*/
                            $total_proposedincentive = 0;
                            $total_new_salary = 0;
                            $suggested_incentive = 0;
                            $uplift_perc = 0.0;
                            $total_salary = 0;
                            $total_incentive_pool = 0;
                            $calc_non_financial = 0;
                            $total_actual_pool = 0;
                            $total_allocation = 0;
                            $total_variance = 0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->whereIn('incentive_scheme_id', [3, 14])
                                ->where('instance_indicator_id', 1)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);
                            //get employee salary data 
                            $emp_salary = EmployeeIncentive::select('proposedincentive_value', 'suggested_incentive', 'calc_non_financial', 'allocation')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_proposedincentive += floatval($salaries->proposedincentive_value);
                                $suggested_incentive += floatval($salaries->suggested_incentive);
                                if ($salaries->calc_non_financial != NULL) {
                                    $calc_non_financial += floatval($salaries->calc_non_financial);
                                }
                                $total_allocation += floatval($salaries->allocation);
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('salary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                            }
                            if ($total_salary > 0) {
                                $total_actual_pool = ($total_salary * $calc_non_financial) / 100;
                            } else {
                                $total_actual_pool = 0;
                            }
                            if ($suggested_incentive > 0) {
                                $suggested_incentive = ($suggested_incentive / $cnt_emplyee);
                            } else {
                                $suggested_incentive = 0;
                            }
                            $total_incentive_pool = $total_salary * 0.68;
                            $incentive_pool_available = $total_proposedincentive - $total_incentive_pool;
                            $total_variance = $total_actual_pool - $total_allocation;

                            $total_proposedincentive = number_format($total_proposedincentive, 2, ".", " ");
                            $suggested_incentive = number_format($suggested_incentive, 2, ".", " ");

                            $total_incentive_pool = number_format($total_incentive_pool, 2, ".", " ");
                            $incentive_pool_available = number_format($incentive_pool_available, 2, ".", " ");
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $total_allocation = number_format($total_allocation, 2, ".", " ");
                            $total_variance = number_format($total_variance, 2, ".", " ");

                            $table_fields['entire'][0]['label'] = 'Current total salary (ZAR)';
                            $table_fields['entire'][0]['value'] = $total_salary;
                            $table_fields['entire'][1]['label'] = 'Total Actual Pool';
                            $table_fields['entire'][1]['value'] = $total_actual_pool;
                            $table_fields['entire'][2]['label'] = 'Total SMI Non-Financial Allocation Value';
                            $table_fields['entire'][2]['value'] = $total_allocation;
                            $table_fields['entire'][3]['label'] = 'Total Variance';
                            $table_fields['entire'][3]['value'] = $total_variance;

                            /*NOTE: NEED TO ADD CURRENY TP THE LABELS: READ CURRENCY LOOKUPA ND VALUE FORM DATABASE*/
                            $total_proposedincentive = 0;
                            $total_new_salary = 0;
                            $suggested_incentive = 0;
                            $uplift_perc = 0.0;
                            $total_salary = 0;
                            $total_incentive_pool = 0;
                            $calc_non_financial = 0;
                            $total_actual_pool = 0;
                            $total_allocation = 0;
                            $total_variance = 0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->whereIn('incentive_scheme_id', [3, 14])
                                ->where('instance_indicator_id', 1)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);
                            //get employee salary data 
                            $emp_salary = EmployeeIncentive::select('proposedincentive_value', 'suggested_incentive', 'calc_non_financial', 'allocation')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_proposedincentive += floatval($salaries->proposedincentive_value);
                                $suggested_incentive += floatval($salaries->suggested_incentive);
                                if ($salaries->calc_non_financial != NULL) {
                                    $calc_non_financial += floatval($salaries->calc_non_financial);
                                }
                                $total_allocation += floatval($salaries->allocation);
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('salary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                            }
                            if ($total_salary > 0) {
                                $total_actual_pool = ($total_salary * $calc_non_financial) / 100;
                            } else {
                                $total_actual_pool = 0;
                            }
                            if ($suggested_incentive > 0) {
                                $suggested_incentive = ($suggested_incentive / $cnt_emplyee);
                            } else {
                                $suggested_incentive = 0;
                            }
                            $total_incentive_pool = $total_salary * 0.68;
                            $incentive_pool_available = $total_proposedincentive - $total_incentive_pool;
                            $total_variance = $total_actual_pool - $total_allocation;

                            $total_proposedincentive = number_format($total_proposedincentive, 2, ".", " ");
                            $suggested_incentive = number_format($suggested_incentive, 2, ".", " ");

                            $total_incentive_pool = number_format($total_incentive_pool, 2, ".", " ");
                            $incentive_pool_available = number_format($incentive_pool_available, 2, ".", " ");
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $total_allocation = number_format($total_allocation, 2, ".", " ");
                            $total_variance = number_format($total_variance, 2, ".", " ");


                            $table_fields['filter'][0]['label'] = 'Currency selected salary (ZAR)';
                            $table_fields['filter'][0]['value'] = $total_salary;
                            $table_fields['filter'][1]['label'] = 'Selected Actual Pool';
                            $table_fields['filter'][1]['value'] = $total_actual_pool;
                            $table_fields['filter'][2]['label'] = 'Selected SMI Non-Financial Allocation Value';
                            $table_fields['filter'][2]['value'] = $total_allocation;
                            $table_fields['filter'][3]['label'] = 'Selected Variance';
                            $table_fields['filter'][3]['value'] = $total_variance;
                            break;
                        case 'Specialist':

                            /*NOTE: NEED TO ADD CURRENY TP THE LABELS: READ CURRENCY LOOKUPA ND VALUE FORM DATABASE*/
                            $total_proposedincentive = 0;
                            $total_new_salary = 0;
                            $suggested_incentive = 0;
                            $uplift_perc = 0.0;
                            $total_salary = 0;
                            $total_incentive_pool = 0;
                            $calc_non_financial = 0;
                            $total_actual_pool = 0;
                            $total_allocation = 0;
                            $total_variance = 0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->whereIn('incentive_scheme_id', [2])
                                ->where('instance_indicator_id', 1)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);
                            //get employee salary data 
                            $emp_salary = EmployeeIncentive::select('proposedincentive_value', 'suggested_incentive', 'calc_non_financial', 'allocation')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_proposedincentive += floatval($salaries->proposedincentive_value);
                                $suggested_incentive += floatval($salaries->suggested_incentive);
                                if ($salaries->calc_non_financial != NULL) {
                                    $calc_non_financial += floatval($salaries->calc_non_financial);
                                }
                                $total_allocation += floatval($salaries->allocation);
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('salary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                            }
                            if ($total_salary > 0) {
                                $total_actual_pool = ($total_salary * $calc_non_financial) / 100;
                            } else {
                                $total_actual_pool = 0;
                            }
                            if ($suggested_incentive > 0) {
                                $suggested_incentive = ($suggested_incentive / $cnt_emplyee);
                            } else {
                                $suggested_incentive = 0;
                            }
                            $total_incentive_pool = $total_salary * 0.68;
                            $incentive_pool_available = $total_proposedincentive - $total_incentive_pool;
                            $total_variance = $total_actual_pool - $total_allocation;

                            $total_proposedincentive = number_format($total_proposedincentive, 2, ".", " ");
                            $suggested_incentive = number_format($suggested_incentive, 2, ".", " ");

                            $total_incentive_pool = number_format($total_incentive_pool, 2, ".", " ");
                            $incentive_pool_available = number_format($incentive_pool_available, 2, ".", " ");
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $total_allocation = number_format($total_allocation, 2, ".", " ");
                            $total_variance = number_format($total_variance, 2, ".", " ");

                            $table_fields['entire'][0]['label'] = 'Current total salary (ZAR)';
                            $table_fields['entire'][0]['value'] = $total_salary;
                            $table_fields['entire'][1]['label'] = 'Total Actual Pool';
                            $table_fields['entire'][1]['value'] = $total_actual_pool;
                            $table_fields['entire'][2]['label'] = 'Total KPI Non-Financial Allocation Value';
                            $table_fields['entire'][2]['value'] = $total_allocation;
                            $table_fields['entire'][3]['label'] = 'Total Variance';
                            $table_fields['entire'][3]['value'] = $total_variance;

                            /*NOTE: NEED TO ADD CURRENY TP THE LABELS: READ CURRENCY LOOKUPA ND VALUE FORM DATABASE*/
                            $total_proposedincentive = 0;
                            $total_new_salary = 0;
                            $suggested_incentive = 0;
                            $uplift_perc = 0.0;
                            $total_salary = 0;
                            $total_incentive_pool = 0;
                            $calc_non_financial = 0;
                            $total_actual_pool = 0;
                            $total_allocation = 0;
                            $total_variance = 0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->whereIn('incentive_scheme_id', [2])
                                ->where('instance_indicator_id', 1)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);
                            //get employee salary data 
                            $emp_salary = EmployeeIncentive::select('proposedincentive_value', 'suggested_incentive', 'calc_non_financial', 'allocation')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_proposedincentive += floatval($salaries->proposedincentive_value);
                                $suggested_incentive += floatval($salaries->suggested_incentive);
                                if ($salaries->calc_non_financial != NULL) {
                                    $calc_non_financial += floatval($salaries->calc_non_financial);
                                }
                                $total_allocation += floatval($salaries->allocation);
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('salary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                            }
                            if ($total_salary > 0) {
                                $total_actual_pool = ($total_salary * $calc_non_financial) / 100;
                            } else {
                                $total_actual_pool = 0;
                            }
                            if ($suggested_incentive > 0) {
                                $suggested_incentive = ($suggested_incentive / $cnt_emplyee);
                            } else {
                                $suggested_incentive = 0;
                            }
                            $total_incentive_pool = $total_salary * 0.68;
                            $incentive_pool_available = $total_proposedincentive - $total_incentive_pool;
                            $total_variance = $total_actual_pool - $total_allocation;

                            $total_proposedincentive = number_format($total_proposedincentive, 2, ".", " ");
                            $suggested_incentive = number_format($suggested_incentive, 2, ".", " ");

                            $total_incentive_pool = number_format($total_incentive_pool, 2, ".", " ");
                            $incentive_pool_available = number_format($incentive_pool_available, 2, ".", " ");
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $total_allocation = number_format($total_allocation, 2, ".", " ");
                            $total_variance = number_format($total_variance, 2, ".", " ");


                            $table_fields['filter'][0]['label'] = 'Currency selected salary (ZAR)';
                            $table_fields['filter'][0]['value'] = $total_salary;
                            $table_fields['filter'][1]['label'] = 'Selected Actual Pool';
                            $table_fields['filter'][1]['value'] = $total_actual_pool;
                            $table_fields['filter'][2]['label'] = 'Selected KPI Non-Financial Allocation Value';
                            $table_fields['filter'][2]['value'] = $total_allocation;
                            $table_fields['filter'][3]['label'] = 'Selected Variance';
                            $table_fields['filter'][3]['value'] = $total_variance;
                            break;
                        case 'Other Incentives':

                            /*NOTE: NEED TO ADD CURRENY TP THE LABELS: READ CURRENCY LOOKUPA ND VALUE FORM DATABASE*/
                            $propose_salary = 0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('personnelnumber')
                                ->where('cycle_id', 1)
                                ->where('instance_indicator_id', 1)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $propose_salary += floatval($salaries->proposedsalary_value);
                            }

                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $table_fields['entire'][0]['label'] = 'Total proposed incentive (ZAR)';
                            $table_fields['entire'][0]['value'] = $propose_salary . '';

                            //get employee salary data per filters
                            //NOTE:STILL NEED TO ADD FILTERS

                            $propose_salary = 0;


                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('personnelnumber')
                                ->where('cycle_id', 1)
                                ->where('instance_indicator_id', 1)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }

                            $emp_salary = EmployeeSalary::select('proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            foreach ($emp_salary as $salaries) {

                                $propose_salary += $salaries->proposedsalary_value;
                            }

                            $propose_salary = number_format($propose_salary, 2, ".", " ");


                            $table_fields['filter'][0]['label'] = 'Total proposed incentive (ZAR)';
                            $table_fields['filter'][0]['value'] = $propose_salary . '';
                            break;
                    }

                    break;
                case '2':
                    //print_r($category);


                    /*NOTE: NEED TO ADD CURRENY TP THE LABELS: READ CURRENCY LOOKUPA ND VALUE FORM DATABASE*/
                    switch ($category) {
                        case 'salary':

                            /*NOTE: NEED TO ADD CURRENY TP THE LABELS: READ CURRENCY LOOKUPA ND VALUE FORM DATABASE*/
                            $total_salary = 0;
                            $total_new_salary = 0;
                            $propose_salary = 0;
                            $uplift_perc = 0.0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('personnelnumber')
                                ->where('cycle_id', 1)
                                ->where('instance_indicator_id', 2)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('salary_value', 'proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                                $propose_salary += floatval($salaries->proposedsalary_value);
                            }
                            $total_new_salary = $propose_salary - $total_salary;
                            if ($total_new_salary > 0) {
                                $uplift_perc = ($total_new_salary / $total_salary) * 100;
                            } else {
                                $uplift_perc = 0;
                            }
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $total_new_salary = number_format($total_new_salary, 2, ".", " ");
                            $uplift_perc = number_format($uplift_perc, 2, ".", " ");

                            $table_fields['entire'][0]['label'] = 'Current Total Salary (KES)';
                            $table_fields['entire'][0]['value'] = $total_salary . '';
                            $table_fields['entire'][1]['label'] = 'Proposed Increase allocation amount (KES)';
                            $table_fields['entire'][1]['value'] = $total_new_salary . '';
                            $table_fields['entire'][2]['label'] = 'New total salary amount (KES)';
                            $table_fields['entire'][2]['value'] = $propose_salary . '';
                            $table_fields['entire'][3]['label'] = 'Total percentage uplift salary';
                            $table_fields['entire'][3]['value'] = $uplift_perc . '%';

                            //get employee salary data per filters
                            //NOTE:STILL NEED TO ADD FILTERS
                            $total_salary = 0;
                            $total_new_salary = 0;
                            $propose_salary = 0;
                            $uplift_perc = 0.0;

                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('personnelnumber')
                                ->where('cycle_id', 1)
                                ->where('instance_indicator_id', 2)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }

                            $emp_salary = EmployeeSalary::select('salary_value', 'proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            foreach ($emp_salary as $salaries) {
                                $total_salary += $salaries->salary_value;
                                $propose_salary += $salaries->proposedsalary_value;
                            }
                            $total_new_salary = $propose_salary - $total_salary;
                            if ($total_new_salary > 0) {
                                $uplift_perc = ($total_new_salary / $total_salary) * 100;
                            } else {
                                $uplift_perc = 0;
                            }
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $total_new_salary = number_format($total_new_salary, 2, ".", " ");
                            $uplift_perc = number_format($uplift_perc, 2, ".", " ");

                            $table_fields['filter'][0]['label'] = 'Current selected salary (KES)';
                            $table_fields['filter'][0]['value'] = $total_salary . '';
                            $table_fields['filter'][1]['label'] = 'Proposed selected increase allocation amount (KES)';
                            $table_fields['filter'][1]['value'] = $total_new_salary . '';
                            $table_fields['filter'][2]['label'] = 'New selected salary amount (KES)';
                            $table_fields['filter'][2]['value'] = $propose_salary . '';
                            $table_fields['filter'][3]['label'] = 'Selected Percentage Uplift In Salary';
                            $table_fields['filter'][3]['value'] = $uplift_perc . '%';
                            break;

                        case 'general_incentive':

                            /*NOTE: NEED TO ADD CURRENY TP THE LABELS: READ CURRENCY LOOKUPA ND VALUE FORM DATABASE*/
                            $total_proposedincentive = 0;
                            $total_new_salary = 0;
                            $suggested_incentive = 0;
                            $uplift_perc = 0.0;
                            $total_salary = 0;
                            $total_incentive_pool = 0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->where('instance_indicator_id', 2)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);
                            //get employee salary data 
                            $emp_salary = EmployeeIncentive::select('proposedincentive_value', 'suggested_incentive')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_proposedincentive += floatval($salaries->proposedincentive_value);
                                $suggested_incentive += floatval($salaries->suggested_incentive);
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('salary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                            }
                            if ($suggested_incentive > 0) {
                                $suggested_incentive = ($suggested_incentive / $cnt_emplyee);
                            } else {
                                $suggested_incentive = 0;
                            }
                            $total_incentive_pool = $total_salary * 0.68;
                            $incentive_pool_available = $total_proposedincentive - $total_incentive_pool;
                            $total_proposedincentive = number_format($total_proposedincentive, 2, ".", " ");
                            $suggested_incentive = number_format($suggested_incentive, 2, ".", " ");

                            $total_incentive_pool = number_format($total_incentive_pool, 2, ".", " ");
                            $incentive_pool_available = number_format($incentive_pool_available, 2, ".", " ");

                            $table_fields['entire'][0]['label'] = 'Total proposed incentive (KES)';
                            $table_fields['entire'][0]['value'] = $total_proposedincentive . '';
                            $table_fields['entire'][1]['label'] = 'Proposed incentive %';
                            $table_fields['entire'][1]['value'] = $suggested_incentive . '%';
                            $table_fields['entire'][2]['label'] = 'Total incentive pool (6.80%)';
                            $table_fields['entire'][2]['value'] = $total_incentive_pool;
                            $table_fields['entire'][3]['label'] = 'Total incentive pool available';
                            $table_fields['entire'][3]['value'] = $incentive_pool_available;

                            $total_proposedincentive = 0;
                            $total_new_salary = 0;
                            $suggested_incentive = 0;
                            $uplift_perc = 0.0;
                            $total_salary = 0;
                            $total_incentive_pool = 0;
                            //get all employees for instance data 
                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->where('instance_indicator_id', 2)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);
                            //get employee salary data 
                            $emp_salary = EmployeeIncentive::select('proposedincentive_value', 'suggested_incentive')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_proposedincentive += floatval($salaries->proposedincentive_value);
                                $suggested_incentive += floatval($salaries->suggested_incentive);
                            }

                            //get employee salary data 
                            $emp_salary = EmployeeSalary::select('salary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                            }
                            if ($suggested_incentive > 0) {
                                $suggested_incentive = ($suggested_incentive / $cnt_emplyee);
                            } else {
                                $suggested_incentive = 0;
                            }
                            $total_incentive_pool = $total_salary * 0.68;
                            $incentive_pool_available = $total_proposedincentive - $total_incentive_pool;
                            $total_proposedincentive = number_format($total_proposedincentive, 2, ".", " ");
                            $suggested_incentive = number_format($suggested_incentive, 2, ".", " ");

                            $total_incentive_pool = number_format($total_incentive_pool, 2, ".", " ");
                            $incentive_pool_available = number_format($incentive_pool_available, 2, ".", " ");


                            $table_fields['filter'][0]['label'] = 'Selected proposed incentive (KES)';
                            $table_fields['filter'][0]['value'] = $total_proposedincentive . '';
                            $table_fields['filter'][1]['label'] = 'Selected Proposed incentive %';
                            $table_fields['filter'][1]['value'] = $suggested_incentive . '%';
                            $table_fields['filter'][2]['label'] = 'Selected incentive pool (6.80%)';
                            $table_fields['filter'][2]['value'] = $total_incentive_pool;
                            $table_fields['filter'][3]['label'] = 'Selected incentive pool available';
                            $table_fields['filter'][3]['value'] = $incentive_pool_available;
                            break;
                    }


                    break;
                case '3':
                    switch ($category) {
                        case 'all_increase':

                            $table_fields[0]['label'] = 'Compa-Ratio Band';
                            $table_fields[0]['value'][] = '0-85%';
                            $table_fields[0]['value'][] = '85%-95%';
                            $table_fields[0]['value'][] = '95%-105%';
                            $table_fields[0]['value'][] = '105%-115%';
                            $table_fields[0]['value'][] = '115%+';
                            $table_fields[0]['value'][] = '115%+';

                            $compa_85 = 0;
                            $compa_95 = 0;
                            $compa_105 = 0;
                            $compa_105_plus = 0;
                            $compa_average = 0;
                            $total_salary = 0;
                            $propose_salary = 0;

                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->whereIn('incentive_scheme_id', [17])
                                ->where('instance_indicator_id', 3)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);

                            //get employee salary data
                            $emp_salary = EmployeeSalary::select('salary_value', 'proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                                $propose_salary += floatval($salaries->proposedsalary_value);
                            }

                            $total_new_salary = $propose_salary - $total_salary;
                            if ($total_new_salary != 0) {
                                $uplift_perc = ($total_new_salary / $total_salary) * 100;
                            } else {
                                $uplift_perc = 0;
                            }
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $total_new_salary = number_format($total_new_salary, 2, ".", " ");
                            $uplift_perc = number_format($uplift_perc, 2, ".", " ");


                            $table_fields[1]['label'] = 'Strategy Drivers';
                            $table_fields[1]['value'][] = $uplift_perc . '%';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';

                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->whereIn('incentive_scheme_id', [10])
                                ->where('instance_indicator_id', 3)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);

                            //get employee salary data
                            $emp_salary = EmployeeSalary::select('salary_value', 'proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                                $propose_salary += floatval($salaries->proposedsalary_value);
                            }

                            $total_new_salary = $propose_salary - $total_salary;
                            if ($total_new_salary != 0) {
                                $uplift_perc = ($total_new_salary / $total_salary) * 100;
                            } else {
                                $uplift_perc = 0;
                            }
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $total_new_salary = number_format($total_new_salary, 2, ".", " ");
                            $uplift_perc = number_format($uplift_perc, 2, ".", " ");

                            $table_fields[2]['label'] = 'Strategy Influencers';
                            $table_fields[2]['value'][] = $uplift_perc . '%';
                            $table_fields[2]['value'][] = '';
                            $table_fields[2]['value'][] = '';
                            $table_fields[2]['value'][] = '';
                            $table_fields[2]['value'][] = '';
                            $table_fields[2]['value'][] = '';


                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->whereIn('incentive_scheme_id', [9])
                                ->where('instance_indicator_id', 3)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);

                            //get employee salary data
                            $emp_salary = EmployeeSalary::select('salary_value', 'proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                                $propose_salary += floatval($salaries->proposedsalary_value);
                            }

                            $total_new_salary = $propose_salary - $total_salary;
                            if ($total_new_salary != 0) {
                                $uplift_perc = ($total_new_salary / $total_salary) * 100;
                            } else {
                                $uplift_perc = 0;
                            }
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $total_new_salary = number_format($total_new_salary, 2, ".", " ");
                            $uplift_perc = number_format($uplift_perc, 2, ".", " ");

                            $table_fields[3]['label'] = 'Investment Professionals';
                            $table_fields[3]['value'][] = $uplift_perc . '%';
                            $table_fields[3]['value'][] = '';
                            $table_fields[3]['value'][] = '';
                            $table_fields[3]['value'][] = '';
                            $table_fields[3]['value'][] = '';
                            $table_fields[3]['value'][] = '';

                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->whereIn('incentive_scheme_id', [7])
                                ->where('instance_indicator_id', 3)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);

                            //get employee salary data
                            $emp_salary = EmployeeSalary::select('salary_value', 'proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                                $propose_salary += floatval($salaries->proposedsalary_value);
                            }

                            $total_new_salary = $propose_salary - $total_salary;
                            if ($total_new_salary != 0) {
                                $uplift_perc = ($total_new_salary / $total_salary) * 100;
                            } else {
                                $uplift_perc = 0;
                            }
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $total_new_salary = number_format($total_new_salary, 2, ".", " ");
                            $uplift_perc = number_format($uplift_perc, 2, ".", " ");

                            $table_fields[4]['label'] = 'Distribution';
                            $table_fields[4]['value'][] = $uplift_perc . '%';
                            $table_fields[4]['value'][] = '';
                            $table_fields[4]['value'][] = '';
                            $table_fields[4]['value'][] = '';
                            $table_fields[4]['value'][] = '';
                            $table_fields[4]['value'][] = '';

                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->whereIn('incentive_scheme_id', [8])
                                ->where('instance_indicator_id', 3)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);

                            //get employee salary data
                            $emp_salary = EmployeeSalary::select('salary_value', 'proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                                $propose_salary += floatval($salaries->proposedsalary_value);
                            }

                            $total_new_salary = $propose_salary - $total_salary;
                            if ($total_new_salary != 0) {
                                $uplift_perc = ($total_new_salary / $total_salary) * 100;
                            } else {
                                $uplift_perc = 0;
                            }
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $total_new_salary = number_format($total_new_salary, 2, ".", " ");
                            $uplift_perc = number_format($uplift_perc, 2, ".", " ");

                            $table_fields[5]['label'] = 'Executive';
                            $table_fields[5]['value'][] = $uplift_perc . '%';
                            $table_fields[5]['value'][] = '';
                            $table_fields[5]['value'][] = '';
                            $table_fields[5]['value'][] = '';
                            $table_fields[5]['value'][] = '';
                            $table_fields[5]['value'][] = '';

                            $employees = array();
                            $emp = Employee::select('employees.personnelnumber')
                                ->join('employee_incentives', 'employee_incentives.personnelnumber', '=', 'employees.personnelnumber')
                                ->join('incentive_schemes', 'incentive_schemes.id', '=', 'employee_incentives.incentive_scheme_id')
                                ->where('employees.cycle_id', 1)
                                ->whereIn('incentive_scheme_id', [21])
                                ->where('instance_indicator_id', 3)
                                ->get();

                            foreach ($emp as $employee) {
                                $employees[] = $employee->personnelnumber;
                            }
                            $cnt_emplyee = count($employees);

                            //get employee salary data
                            $emp_salary = EmployeeSalary::select('salary_value', 'proposedsalary_value')
                                ->where('cycle_id', 1)
                                ->whereIn('personnelnumber', $employees)
                                ->get();
                            //dd($emp_salary);

                            foreach ($emp_salary as $salaries) {
                                $total_salary += floatval($salaries->salary_value);
                                $propose_salary += floatval($salaries->proposedsalary_value);
                            }

                            $total_new_salary = $propose_salary - $total_salary;
                            if ($total_new_salary != 0) {
                                $uplift_perc = ($total_new_salary / $total_salary) * 100;
                            } else {
                                $uplift_perc = 0;
                            }
                            $total_salary = number_format($total_salary, 2, ".", " ");
                            $propose_salary = number_format($propose_salary, 2, ".", " ");
                            $total_new_salary = number_format($total_new_salary, 2, ".", " ");
                            $uplift_perc = number_format($uplift_perc, 2, ".", " ");

                            $table_fields[6]['label'] = 'Retail Incentive Scheme';
                            $table_fields[6]['value'][] = $uplift_perc . '%';
                            $table_fields[6]['value'][] = '';
                            $table_fields[6]['value'][] = '';
                            $table_fields[6]['value'][] = '';
                            $table_fields[6]['value'][] = '';
                            $table_fields[6]['value'][] = '';
                            break;
                        case 'all_incentive':
                            $table_fields[0]['label'] = '';
                            $table_fields[0]['value'][] = 'Total Pool';
                            $table_fields[0]['value'][] = 'Allocated';
                            $table_fields[0]['value'][] = 'Remaining';
                            $table_fields[1]['label'] = 'Absolute Return';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[2]['label'] = 'Balanced';
                            $table_fields[2]['value'][] = '';
                            $table_fields[2]['value'][] = '';
                            $table_fields[2]['value'][] = '';
                            $table_fields[3]['label'] = 'Multi-Manager';
                            $table_fields[3]['value'][] = '';
                            $table_fields[3]['value'][] = '';
                            $table_fields[3]['value'][] = '';
                            $table_fields[4]['label'] = 'Property';
                            $table_fields[4]['value'][] = '';
                            $table_fields[4]['value'][] = '';
                            $table_fields[4]['value'][] = '';
                            $table_fields[5]['label'] = 'Research';
                            $table_fields[5]['value'][] = '';
                            $table_fields[5]['value'][] = '';
                            $table_fields[5]['value'][] = '';
                            $table_fields[6]['label'] = 'Direct Property';
                            $table_fields[6]['value'][] = '';
                            $table_fields[6]['value'][] = '';
                            $table_fields[6]['value'][] = '';

                            $table_fields[7]['label'] = 'Unconstrained Equity';
                            $table_fields[7]['value'][] = '';
                            $table_fields[7]['value'][] = '';
                            $table_fields[7]['value'][] = '';
                            $table_fields[8]['label'] = 'Value';
                            $table_fields[8]['value'][] = '';
                            $table_fields[8]['value'][] = '';
                            $table_fields[8]['value'][] = '';
                            $table_fields[9]['label'] = 'Infrastructure';
                            $table_fields[9]['value'][] = '';
                            $table_fields[9]['value'][] = '';
                            $table_fields[9]['value'][] = '';
                            $table_fields[10]['label'] = 'SADPDF';
                            $table_fields[10]['value'][] = '';
                            $table_fields[10]['value'][] = '';
                            $table_fields[10]['value'][] = '';
                            $table_fields[11]['label'] = 'DevCO';
                            $table_fields[11]['value'][] = '';
                            $table_fields[11]['value'][] = '';
                            $table_fields[11]['value'][] = '';
                            $table_fields[12]['label'] = 'Total Investment Professionals';
                            $table_fields[12]['value'][] = '';
                            $table_fields[12]['value'][] = '';
                            $table_fields[12]['value'][] = '';

                            break;
                        case 'general_staff':
                            $table_fields[0]['label'] = 'Rating';
                            $table_fields[0]['value'][] = 'TEA';
                            $table_fields[0]['value'][] = '1';
                            $table_fields[0]['value'][] = '2';
                            $table_fields[0]['value'][] = '3';
                            $table_fields[0]['value'][] = '4';
                            $table_fields[0]['value'][] = 'Average';
                            $table_fields[1]['label'] = 'General Staff';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[2]['label'] = 'Guideline';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '8-10%';
                            $table_fields[2]['value'][] = '10-12%';
                            $table_fields[2]['value'][] = '';

                            break;
                        case 'professional_staff':
                            $table_fields[0]['label'] = 'Rating';
                            $table_fields[0]['value'][] = 'TEA';
                            $table_fields[0]['value'][] = '1';
                            $table_fields[0]['value'][] = '2';
                            $table_fields[0]['value'][] = '3';
                            $table_fields[0]['value'][] = '4';
                            $table_fields[0]['value'][] = 'Average';
                            $table_fields[1]['label'] = 'Professional Staff';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[2]['label'] = 'Guideline';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '8-10%';
                            $table_fields[2]['value'][] = '10-12%';
                            $table_fields[2]['value'][] = '';

                            break;
                        case 'strategy':
                            $table_fields[0]['label'] = 'Rating';
                            $table_fields[0]['value'][] = 'TEA';
                            $table_fields[0]['value'][] = '1';
                            $table_fields[0]['value'][] = '2';
                            $table_fields[0]['value'][] = '3';
                            $table_fields[0]['value'][] = '4';
                            $table_fields[0]['value'][] = 'Average';
                            $table_fields[1]['label'] = 'Strategy Influencers';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[2]['label'] = 'Guideline';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '8-10%';
                            $table_fields[2]['value'][] = '10-12%';
                            $table_fields[2]['value'][] = '';

                            break;
                        case 'strategy_drivers':
                            $table_fields[0]['label'] = 'Rating';
                            $table_fields[0]['value'][] = 'TEA';
                            $table_fields[0]['value'][] = '1';
                            $table_fields[0]['value'][] = '2';
                            $table_fields[0]['value'][] = '3';
                            $table_fields[0]['value'][] = '4';
                            $table_fields[0]['value'][] = 'Average';
                            $table_fields[1]['label'] = 'Strategy Drivers';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[2]['label'] = 'Guideline';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '8-10%';
                            $table_fields[2]['value'][] = '10-12%';
                            $table_fields[2]['value'][] = '';

                            break;
                        case 'executive':
                            $table_fields[0]['label'] = 'Rating';
                            $table_fields[0]['value'][] = 'TEA';
                            $table_fields[0]['value'][] = '1';
                            $table_fields[0]['value'][] = '2';
                            $table_fields[0]['value'][] = '3';
                            $table_fields[0]['value'][] = '4';
                            $table_fields[0]['value'][] = 'Average';
                            $table_fields[1]['label'] = 'Executive';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[2]['label'] = 'Guideline';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '8-10%';
                            $table_fields[2]['value'][] = '10-12%';
                            $table_fields[2]['value'][] = '';

                            break;
                        case 'investment':
                            $table_fields[0]['label'] = '';
                            $table_fields[0]['value'][] = 'Pool Available';
                            $table_fields[0]['value'][] = 'Pool Allocated';
                            $table_fields[0]['value'][] = 'Pool Remaining';
                            $table_fields[1]['label'] = 'Investment Professionals';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';

                            break;
                        case 'distribution':
                            $table_fields[0]['label'] = 'Rating';
                            $table_fields[0]['value'][] = 'TEA';
                            $table_fields[0]['value'][] = '1';
                            $table_fields[0]['value'][] = '2';
                            $table_fields[0]['value'][] = '3';
                            $table_fields[0]['value'][] = '4';
                            $table_fields[0]['value'][] = 'Average';
                            $table_fields[1]['label'] = 'Distribution';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[1]['value'][] = '';
                            $table_fields[2]['label'] = 'Guideline';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '0%';
                            $table_fields[2]['value'][] = '8-10%';
                            $table_fields[2]['value'][] = '10-12%';
                            $table_fields[2]['value'][] = '';

                            break;
                    }


                    break;
            }
        }

        return $table_fields;
    }

    private function getSalaryPoolCategoryTable($category = null)
    {
        $html = '';

        //get instance
        $user_instance = PermissionController::getUserInstance();
        if ($user_instance['message'] == 'Success') {
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }

        $table_columns = [];
        $table_row_labels = [];
        $table_label = '';


        if (isset($instance_id) && $instance_id != NULL) {
            switch ($instance_id) {
                case '1':
                    $table_label = 'Total salary increase per band';
                    $table_columns[] = [
                        '',
                        'Band 1 & 2',
                        'Band 3',
                        'Band 4 & 5',
                    ];

                    $table_row_labels[] = 'Target';
                    $table_row_labels[] = 'Actual';
                    $table_row_target[] = '';
                    $table_row_target[] = '6%';
                    $table_row_target[] = '6%';
                    $table_row_target[] = '6.5%';
                    $table_row_actual[] = '';
                    $table_row_actual[] = '<b style="color:green">5.37%</b>';//need calc
                    $table_row_actual[] = '<b style="color:blue">0.62%</b>';//need calc
                    $table_row_actual[] = '<b style="color:green">4.66%</b>';//need calc

                    $html .= '<div class="col-md-4">';
                    $html .= '<div class="card">';
                    $html .= '<div class="card-header">' . $table_label . '</div>';
                    $html .= '<div class="card-body">';
                    $html .= '<table class="table-striped table-bordered calculation-table">';

                    //build table header
                    $table_columns_count = 1;
                    $count_col_row = count($table_columns);
                    for ($cols_row = 0; $cols_row < $count_col_row; $cols_row++) {
                        $html .= '<tr>';
                        $cnt_cols = count($table_columns[$cols_row]);
                        $table_columns_count = $cnt_cols;
                        for ($cols = 0; $cols < $cnt_cols; $cols++) {
                            $html .= '<th>';
                            $html .= $table_columns[$cols_row][$cols];
                            $html .= '</th>';
                        }
                        $html .= '</tr>';
                    }

                    //build table body
                    $count_rows = count($table_row_labels);
                    for ($cols_row = 0; $cols_row < $count_rows; $cols_row++) {
                        $html .= '<tr>';
                        for ($cols = 0; $cols < $table_columns_count; $cols++) {
                            $html .= '<td>';
                            if ($cols == 0) {
                                $html .= '<b>' . $table_row_labels[$cols_row] . '</b>';
                            } else {
                                if ($cols_row == 0) {
                                    $html .= '<b>' . $table_row_target[$cols] . '</b>';
                                } else {
                                    $html .= '<b>' . $table_row_actual[$cols] . '</b>';
                                }
                            }
                            $html .= '</td>';
                        }
                        $html .= '</tr>';
                    }


                    $html .= '</table>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';

                    break;
                case '2':
                    $table_label = 'Salary Pool';
                    $table_columns[] = [
                        '',
                        'Percentage',
                    ];

                    $table_row_labels[] = 'Target';
                    $table_row_labels[] = 'Actual';

                    $html .= '<div class="col-md-3">';
                    $html .= '<div class="card">';
                    $html .= '<div class="card-header">' . $table_label . '</div>';
                    $html .= '<div class="card-body">';
                    $html .= '<table class="table-striped table-bordered calculation-table">';

                    //build table header
                    $table_columns_count = 1;
                    $count_col_row = count($table_columns);
                    for ($cols_row = 0; $cols_row < $count_col_row; $cols_row++) {
                        $html .= '<tr>';
                        $cnt_cols = count($table_columns[$cols_row]);
                        $table_columns_count = $cnt_cols;
                        for ($cols = 0; $cols < $cnt_cols; $cols++) {
                            $html .= '<th>';
                            $html .= $table_columns[$cols_row][$cols];
                            $html .= '</th>';
                        }
                        $html .= '</tr>';
                    }

                    $table_row_target[] = '';
                    $table_row_target[] = '6%';
                    $table_row_actual[] = '';
                    $table_row_actual[] = '<b style="color:green">5.37%</b>';//need calc

                    //build table body
                    $count_rows = count($table_row_labels);
                    for ($cols_row = 0; $cols_row < $count_rows; $cols_row++) {
                        $html .= '<tr>';
                        for ($cols = 0; $cols < $table_columns_count; $cols++) {
                            $html .= '<td>';
                            if ($cols == 0) {
                                $html .= '<b>' . $table_row_labels[$cols_row] . '</b>';
                            } else {
                                if ($cols_row == 0) {
                                    $html .= '<b>' . $table_row_target[$cols] . '</b>';
                                } else {
                                    $html .= '<b>' . $table_row_actual[$cols] . '</b>';
                                }
                            }
                            $html .= '</td>';
                        }
                        $html .= '</tr>';
                    }


                    $html .= '</table>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';

                    break;
                case '3':
                    $table_label = '';

                    //performance rating
                    $table_columns['performance'][] = [
                        'Rating',
                        'TEA',
                        '1',
                        '2',
                        '3',
                        '4',
                        'Average',
                    ];

                    $table_row_labels['performance'][] = 'General Staff';
                    $table_row_labels['performance'][] = 'Professional Staff';

                    //compa ratio rating
                    $table_columns['compa_ratio'][] = [
                        'Compa-ratio band',
                        '0-85%',
                        '85%-95%',
                        '95%-105%',
                        '105%-115%',
                        '115%+',
                        'Average',
                    ];

                    $table_row_labels['compa_ratio'][] = 'Strategy Influencers';
                    $table_row_labels['compa_ratio'][] = 'Strategy Drivers';
                    $table_row_labels['compa_ratio'][] = 'Executive';
                    $table_row_labels['compa_ratio'][] = 'Investment Professionals';
                    $table_row_labels['compa_ratio'][] = 'Distribution';

                    //incentive rating
                    $table_columns['incentive'][] = ['', 'Allocated Pool'];

                    $table_row_labels['incentive'][] = 'General Staff';
                    $table_row_labels['incentive'][] = 'Professional Staff';
                    $table_row_labels['incentive'][] = 'Strategy Influencers';
                    $table_row_labels['incentive'][] = 'Strategy Drivers';
                    $table_row_labels['incentive'][] = 'Executive';
                    $table_row_labels['incentive'][] = 'Investment Professionals';
                    $table_row_labels['incentive'][] = 'Distribution';
                    $table_row_labels['incentive'][] = '-';
                    $table_row_labels['incentive'][] = 'Total Shared Services';
                    $table_row_labels['incentive'][] = 'Allocated';
                    $table_row_labels['incentive'][] = 'Remainder';
                    $table_row_labels['incentive'][] = '-';
                    $table_row_labels['incentive'][] = 'Total Incentive Pool';
                    $table_row_labels['incentive'][] = 'Allocated';
                    $table_row_labels['incentive'][] = 'Remainder';


                    $html .= '<div class="col-md-6">';
                    //$html .= '<div class="card">';
                    //$html .= '<div class="card-body">';
                    $html .= '<table class=" table-striped table-bordered calculation-table">';

                    //build table header
                    $table_columns_count = 1;

                    //get different tables for different tabs
                    $identifier = 'performance';
                    switch ($category) {
                        case 'all_increase':
                            $identifier = 'performance';
                            break;
                        case 'all_incentive':
                            $identifier = 'incentive';
                            break;
                        case 'general_staff':
                            $identifier = 'performance';
                            break;
                        case 'professional_staff':
                            $identifier = 'performance';
                            break;
                        case 'strategy':
                            $identifier = 'compa_ratio';
                            break;
                        case 'strategy_drivers':
                            $identifier = 'compa_ratio';
                            break;
                        case 'executive':
                            $identifier = 'compa_ratio';
                            break;
                        case 'investment':
                            $identifier = 'compa_ratio';
                            break;
                        case 'distribution':
                            $identifier = 'compa_ratio';
                            break;
                    }

                    $count_col_row = count($table_columns[$identifier]);
                    for ($cols_row = 0; $cols_row < $count_col_row; $cols_row++) {
                        $html .= '<tr>';
                        $cnt_cols = count($table_columns[$identifier][$cols_row]);
                        $table_columns_count = $cnt_cols;
                        for ($cols = 0; $cols < $cnt_cols; $cols++) {
                            $html .= '<th>';
                            $html .= $table_columns[$identifier][$cols_row][$cols];
                            $html .= '</th>';
                        }
                        $html .= '</tr>';
                    }

                    //build table body
                    $count_rows = count($table_row_labels[$identifier]);
                    for ($cols_row = 0; $cols_row < $count_rows; $cols_row++) {
                        $html .= '<tr>';
                        for ($cols = 0; $cols < $table_columns_count; $cols++) {
                            $html .= '<td>';
                            if ($cols == 0) {
                                $html .= '<b>' . $table_row_labels[$identifier][$cols_row] . '</b>';
                            }
                            $html .= '</td>';
                        }
                        $html .= '</tr>';
                    }


                    $html .= '</table>';
                    //$html .= '</div>';
                    //$html .= '</div>';
                    $html .= '</div>';

                    break;
            }
        }


        return $html;
    }

    private function getCategoryTabs()
    {
        $categories = array();

        //get instance
        $user_instance = PermissionController::getUserInstance();
        if ($user_instance['message'] == 'Success') {
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }

        if (isset($instance_id) && $instance_id != NULL) {
            switch ($instance_id) {
                case '1':
                    $categories[0]['name'] = 'Salary';
                    $categories[0]['tab_name'] = 'salary';
                    /*$categories[1]['name'] = 'General Staff Salary';
                    $categories[1]['tab_name'] = 'general_salary';*/
                    $categories[2]['name'] = 'General Staff Incentive';
                    $categories[2]['tab_name'] = 'general_incentive';
                    $categories[3]['name'] = 'SM & Risk Incentive';
                    $categories[3]['tab_name'] = 'sm_risk_incentive';
                    $categories[4]['name'] = 'Specialist';
                    $categories[4]['tab_name'] = 'specialist_incentive';
                    $categories[5]['name'] = 'Other Incentives';
                    $categories[5]['tab_name'] = 'other_incentive';
                    break;
                case '2':
                    $categories[0]['name'] = 'Salary';
                    $categories[0]['tab_name'] = 'salary';
                    $categories[1]['name'] = 'General Incentive';
                    $categories[1]['tab_name'] = 'general_incentive';
                    break;
                case '3':
                    $categories[0]['name'] = 'All Staff - Increases';
                    $categories[0]['tab_name'] = 'all_increase';
                    $categories[1]['name'] = 'All Staff - Incentive';
                    $categories[1]['tab_name'] = 'all_incentive';
                    $categories[2]['name'] = 'General Staff';
                    $categories[2]['tab_name'] = 'general_staff';
                    $categories[3]['name'] = 'Professional Staff';
                    $categories[3]['tab_name'] = 'professional_staff';
                    $categories[4]['name'] = 'Strategy Influencers';
                    $categories[4]['tab_name'] = 'strategy';
                    $categories[5]['name'] = 'Strategy Drivers';
                    $categories[5]['tab_name'] = 'strategy_drivers';
                    $categories[6]['name'] = 'Executive';
                    $categories[6]['tab_name'] = 'executive';
                    $categories[7]['name'] = 'Investment Professionals';
                    $categories[7]['tab_name'] = 'investment';
                    $categories[8]['name'] = 'Distribution';
                    $categories[8]['tab_name'] = 'distribution';
                    break;
            }
        }


        return $categories;
    }

    private function getCountryTabs($cat_tab)
    {
        $categories = array();
        /*$countries = Country::select('name')->where('name','Kenya')->get();
        //$countries[] = 'Kenya';
        $i = 0;
        foreach($countries as $country){
            $categories[$i]['name'] = $country->name;
            $categories[$i]['tab_name'] = str_replace(" ","",strtolower($country->name)).'_'.$cat_tab;
            $i++;
        }*/

        $categories[0]['name'] = 'Kenya';
        $categories[0]['tab_name'] = str_replace(" ", "", strtolower('Kenya')) . '_' . $cat_tab;

        return $categories;
    }


    public static function GroupMiniDashBoardCalc($filtered = 0)
    {
        $manager_pn         = Auth::user()->personnelnumber;
        $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $records            = DB::table('vw_employees_export');
        $records            = $records->whereIn('status_id',  [1,7,19]);
        $records            = $records->where('instance_indicator_id',  $active_instance);
        if (isset($filtered) && $filtered === 1){
            $records            = Filters::applyFilters($records, Auth::user());
        }

        //filter by manager/roles
        $records            = ManagerTypeUser::filterDatasetManagerType($records, $user_roles);
        $records            =  $records->where('instance_indicator_id', 1);
        $records = $records->select(DB::raw(self::$GroupDashboarduery));

        return $records->get();

    }

    public static function africaMiniDashboardCalc($filtered = 0){
        $manager_pn         = Auth::user()->personnelnumber;
        $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $records            = DB::table('vw_employees_export');
        $records            = $records->whereIn('status_id',  [1,7,19]);
        $records            = $records->where('instance_indicator_id',  $active_instance);

//        if (isset($filtered) && $filtered === 1){
            $records        = Filters::applyFilters($records, Auth::user());
//        }

        $records            = ManagerTypeUser::filterDatasetManagerType($records, $user_roles);

        if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
            $records = $records->select(DB::raw(self::$africaDashboardQuery));
        }
        else{
            $records = $records->select(DB::raw(self::$AfricaCalcForAdmin));
        }
        $records = $records->groupBy('orglevel7');

        return $records->get();
    }



    public static function STANLIBMiniDashBoardCalc($filtered = 0)
    {
        $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $records            = DB::table('vw_employees_export');
        $records            = $records->whereIn('status_id',  [1,7,19]);
        $records            = $records->where('instance_indicator_id',  $active_instance);
        if (isset($filtered) && $filtered === 1){
            $records            = Filters::applyFilters($records, Auth::user());
        }

        //filter by manager/roles
        $records            = ManagerTypeUser::filterDatasetManagerType($records, $user_roles);
        //$records            =  $records->where('instance_indicator_id', 3);
        $records            = $records->select(DB::raw(self::$STANLIBMiniDashboardQuery));

        return $records->get();

    }
    public static function STANLIBIncentiveCalc($filtered = 0)
    {
        $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $records            = DB::table('vw_employees_export');
        $records            = $records->whereIn('status_id',  [1,7,19]);
        $records            = $records->where('instance_indicator_id',  $active_instance);
        if (isset($filtered) && $filtered == 1){

        }
        $records            = Filters::applyFilters($records, Auth::user());
        //filter by manager/roles
        $records            = ManagerTypeUser::filterDatasetManagerType($records, $user_roles);
        //$records            =  $records->where('instance_indicator_id', 3);
        $records            = $records->select(DB::raw(self::$STANLIBQuery))->groupBy( 'franchise_two_level_id');

        return $records->get();

    }
    public static function STANLIBFranchiseMappings($filtered = 0)
    {
        $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $records            = DB::table('vw_employees_export');
        $records            = $records->whereIn('status_id',  [1,7,19]);
        $records            = $records->where('instance_indicator_id',  $active_instance);
        if (isset($filtered) && $filtered === 1){
            $records            = Filters::applyFilters($records, Auth::user());
        }

        $records            = ManagerTypeUser::filterDatasetManagerType($records, $user_roles);
        $records            = $records->select(DB::raw('incentive_scheme_id, franchise_one_level_id, franchise_two_level_id, franchise_three_level_id, 
                                                             franchise_one_level, franchise_two_level, franchise_three_level '));

        return $records->get();

    }

    public static $GroupDashboarduery = '
                   sum( CASE WHEN (scheme_id != 13 AND scheme_id != 20 AND scheme_id != 43 AND scheme_id != 23) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar END) as total_proposed_incentive_zar_other,
                   
                   sum( CASE WHEN scheme_id = 23 OR scheme_id = 20 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN allocation END) as allocation,
                   sum( CASE WHEN scheme_id = 23 OR scheme_id = 20 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)THEN calc_on_target_achievement END) as calc_on_target_achievement,
                   sum( CASE WHEN scheme_id = 23 OR scheme_id = 20 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)THEN calc_proposedincentive_zar END) as pis_sm_risk,
                   
                   
                   sum( CASE WHEN (scheme_id = 20 OR scheme_id = 23) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_salary_zar END) as  current_total_salary_zar_sm,
                   
                   
                   sum( CASE WHEN scheme_id = 13 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar END) as total_salary_zar_gi,
                   
                   
                   sum( CASE WHEN scheme_id = 43 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar END) as pis_special,
                   sum( CASE WHEN scheme_id = 13 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar END) as total_proposed_incentive_zar_gi,
                   
                   
                   
                   
                   (sum( CASE WHEN scheme_id = 13 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar END) / 
                   sum( CASE WHEN (scheme_id = 13) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar END) * 100) as proposed_incentive_percentage_gi,
                   
                   
                   
                   
                   sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar END) as calc_salary_zar,
                 
                   sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar END) as calc_proposedsalary_zar,
                   
                   
                   
                   ROUND((( (sum( CASE WHEN (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                            (sum( CASE WHEN (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) ,1) as ps_percentage12,
                   
                   
                   
                   
                   ROUND((( (sum( CASE WHEN (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                            (sum( CASE WHEN (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as ps_percentage3,
                   
                   
                   
                   
                   ROUND((( (sum( CASE WHEN (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
                            (sum( CASE WHEN (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as ps_percentage45,
                   
                   
                   
                   
                   sum( CASE WHEN scheme_id = 43 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) as calc_kpi_incentive_value_sp,
                   sum( CASE WHEN scheme_id = 43 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar END) as current_total_salary_zar_sp,
                   sum( CASE WHEN (scheme_id = 43) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_salary_zar *(calc_non_financial / 100)   END)  as total_actual_pool_sp,
                   
                   
                   
                   sum( CASE WHEN (scheme_id = 23 OR scheme_id = 20) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) as calc_kpi_incentive_value_sm_risk,
                   sum( CASE WHEN (scheme_id = 23 OR scheme_id = 20) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_salary_zar *(calc_non_financial / 100)   END)  as total_actual_pool_sm_risk,
                   
                   
                   sum( CASE WHEN (scheme_id = 23 OR scheme_id = 20) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) / 
             sum( CASE WHEN (scheme_id = 23 OR scheme_id = 20) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_actual_pool   END) * 100 as total_actual_pool_percentage_sm,
             
             sum( CASE WHEN scheme_id = 43 AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_kpi_incentive_value END) / 
             sum( CASE WHEN (scheme_id = 43) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_actual_pool   END) * 100 as total_actual_pool_percentage_sp 
       ';

    public static $africaDashboardQuery = '
                    orglevel7,   
                    sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar * conversion_rate END) as current_total_salary_africa,
                sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar * conversion_rate  END)  as new_total_salary_amount_africa,
                (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar *  conversion_rate  END) - sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar *  conversion_rate END)) as proposed_increase_allocation_amount_africa,
                
                
                (((( (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar *  conversion_rate END) )  - (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar *  conversion_rate END) ) ) / 
                (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar *  conversion_rate END) ) )* 100)  )  as total_percentage_uplift_salary,
                
                sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN (calc_proposedincentive_zar *  conversion_rate) END ) as total_proposed_incentive_africa,
                
               
                ROUND((( (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar *  conversion_rate END) ) / 
                (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar *  conversion_rate END) ) )* 100  ) ,1) as proposed_incentive_percentage_africa,
                
                
                sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar *  conversion_rate  END) as calc_salary_zar_incentive_12,
                
                ROUND((((( (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar *  conversion_rate END) )  - (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar *  conversion_rate END) ) ) / (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar *  conversion_rate END) ) )* 100)  ) ,0) as totIncrease,
               
                ROUND((( (sum(  CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar *  conversion_rate END) ) / (sum(  CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar *  conversion_rate END) ) )* 100  ) ,0) as totbonus,
                
                
                
                
                sum(CASE WHEN scheme_id NOT IN (22,24) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_salary_zar *  conversion_rate  END  ) as calc_salary_zar_incentive_12_excl_smi_sale,
                
                sum(CASE WHEN scheme_id NOT IN (22,24)  AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar *  conversion_rate END ) as total_proposed_incentive_africa_excl_smi_sale,
                
                ROUND(((  (sum( CASE WHEN scheme_id NOT IN (22,24) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar * conversion_rate  END  ) ) / 
                            (sum( CASE WHEN scheme_id NOT IN (22,24) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_salary_zar * conversion_rate END) ) ) * 100  ) ,1) as totbonus_excl_smi_sale,
                            
                ROUND((( (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN  calc_proposedincentive_zar *  conversion_rate  END) ) / (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar *  conversion_rate END) ) )* 100  ) ,1) as proposed_incentive_percentage_africa_excl_smi_sale
                
    ';

    public static $AfricaCalcForAdmin = ' orglevel7,
                sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar END) as current_total_salary_africa_all,
                sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar END) as new_total_salary_amount_africa_all,   
                sum( calc_proposedincentive_zar) as total_proposed_incentive_africa_all,
                
                ROUND((( (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar END) ) / (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar END ) ) )* 100  ) ,1) as proposed_incentive_percentage_africa_all,
                sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN  calc_salary_zar END) as calc_salary_zar_incentive_12_all,    
                         
                sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar * conversion_rate END) as current_total_salary_africa,
                sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar * conversion_rate  END)  as new_total_salary_amount_africa,
                (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar * conversion_rate  END) - sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar * conversion_rate END)) as proposed_increase_allocation_amount_africa,
                
                (((( (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar * conversion_rate END) )  - (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar * conversion_rate END) ) ) / (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar * conversion_rate END) ) )* 100)  )  as total_percentage_uplift_salary,
                
                sum(CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_proposedincentive_zar * conversion_rate END) as total_proposed_incentive_africa,
                
                ROUND((( (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN  calc_proposedincentive_zar * conversion_rate END ) ) / (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar * conversion_rate END) ) )* 100  ) ,1) as proposed_incentive_percentage_africa,
                sum(CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN calc_salary_zar * conversion_rate END) as calc_salary_zar_incentive_12,
                
                ROUND((((( (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_proposedsalary_zar * conversion_rate END) )  - (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar * conversion_rate END) ) ) / (sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar * conversion_rate END) ) )* 100)  ) ,0) as totIncrease,
           
                ROUND((( (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN  calc_proposedincentive_zar * conversion_rate END) ) / (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN  calc_salary_zar * conversion_rate END) ) )* 100  ) ,0) as totbonus,
                
                
                 
                sum(CASE WHEN scheme_id NOT IN (22,24) AND (do_not_process_bonus IS NULL OR do_not_process_bonus != 1) THEN  calc_proposedincentive_zar END) as total_proposed_incentive_africa_all_excl_smi_sale,
                ROUND((( (sum(CASE WHEN scheme_id NOT IN (22,24) AND  (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN  calc_proposedincentive_zar END ) ) / (sum(CASE WHEN scheme_id NOT IN (22,24) THEN  calc_salary_zar  END) ) )* 100  ) ,1) as proposed_incentive_percentage_africa_all_excl_smi_sale,
                sum(CASE WHEN scheme_id NOT IN (22,24) AND  (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN  calc_salary_zar END) as calc_salary_zar_incentive_12_all_excl_smi_sale,
                
                
                
                sum(CASE WHEN scheme_id NOT IN (22,24)  AND  (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_salary_zar *  conversion_rate  END  ) as calc_salary_zar_incentive_12_excl_smi_sale,
                sum(CASE WHEN scheme_id NOT IN (22,24) AND  (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar *  conversion_rate END ) as total_proposed_incentive_africa_excl_smi_sale,
                ROUND(((  (sum( CASE WHEN scheme_id NOT IN (22,24)  AND  (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar * conversion_rate  END  ) ) / 
                            (sum( CASE WHEN scheme_id NOT IN (22,24) AND  (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_salary_zar * conversion_rate END) ) ) * 100  ) ,1) as totbonus_excl_smi_sale,
                            
                ROUND((( (sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_proposedincentive_zar *  conversion_rate END) ) / (sum(  CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN calc_salary_zar *  conversion_rate END) ) )* 100  ) ,1) as proposed_incentive_percentage_africa_excl_smi_sale
                
                ';

    public static $STANLIBQuery  = '
        franchise_two_level_id,
        sum( CASE WHEN do_not_process_salary IS NULL OR do_not_process_salary != 1 THEN calc_salary_zar *  conversion_rate  END) as calc_salary_zar,
        sum( CASE WHEN (do_not_process_bonus IS NULL OR do_not_process_bonus != 1)  THEN  calc_proposedincentive_zar *  conversion_rate   END) as proposedincentive
                    

    ';

    public static  $STANLIBMiniDashboardQuery= '
    ROUND((( (sum( CASE WHEN (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
            (sum( CASE WHEN (band_id = 1 OR band_id = 2) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) ,1) as ps_percentage12,
        
        
        
        
        ROUND((( (sum( CASE WHEN (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
            (sum( CASE WHEN (band_id = 3) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as ps_percentage3,
        
        
        
        
        ROUND((( (sum( CASE WHEN (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)   THEN (calc_proposedsalary_zar - calc_salary_zar) END) ) / 
            (sum( CASE WHEN (band_id = 4 OR band_id = 5) AND (do_not_process_salary IS NULL OR do_not_process_salary != 1)  THEN calc_salary_zar END) ) )* 100  ) , 1) as ps_percentage45
    ';
}

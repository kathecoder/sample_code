<?php

/* 
 * @copyright Copyright (c) 2020 ConciseFlow.
 */
namespace App\Http\Controllers;

use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;
use App\CFLibrary\DashboardViewerPanel;

 use App\Exports\ExportPerformanceRating;
use App\Models\Band;
use App\Models\CurrentRating;
use App\Models\Cycle;
use App\Models\EarningsDistribution;
use App\Models\Filters;
use App\Models\PerformanceRating;
use App\Models\IssueLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use stdClass;

class DashboardController extends Controller
{

   public function __construct()
   {
       
   }
   
   public function create(Request $request)
   {
             
        return true;
   }

   public function getCardPanel(Request $request)
   {
       
        $card_template = [];
        $dashboards = [];
        $data = [];
        $mode = '';
        $id = '';
        $scripts = '';
        $categories_tabs = $this->getCategoryTabs();
        foreach($categories_tabs as $cat_tab){
            $html = '';
            
            $dashboards[$cat_tab['tab_name']] = $html; 
        }
        $cnt_tabs = count($categories_tabs);
       
        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/DataTabs.twig',[
            'custom_settings' => [],
            'tabs'=>$categories_tabs,
            'table_test' => $dashboards,
            'cnt_tabs'=>$cnt_tabs,
            'scripts'=>$scripts
        ]);
        $dataDashboards = $template_panel->show($template);
        $card_template = $dataDashboards;
       
       return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }
    
    private function getCategoryTabs()
    {
        $categories = array();
        
        //$categories = ['All Allocation','Salary','General Staff Incentive','SM & Risk Incentive','Specialist','Other Incentives'];
        $categories[0]['name'] = 'Performance Rating';
        $categories[0]['tab_name'] = strtolower(preg_replace('/[^azAZ09_\.]/','', 'performance_rating')); 
        $categories[1]['name'] = 'Business Review';
        $categories[1]['tab_name'] = strtolower(preg_replace('/[^azAZ09_\.]/','', 'business_review')); 
        
        return $categories;
    }
    public function getTotalPerformanceRatings(Request $request){

        try{
           $company_totals      = self::getCompanyTotalPerformanceRatings('orglevel1');
           $orglevel5_totals    = self::getCompanyTotalPerformanceRatings('orglevel5');
           $orglevel6_totals    = self::getBusinessUnitTotalPerformanceRatings();
                //  logger($company_totals);

            $data           =   [];
            $data[]         =   $company_totals[0];
            $table          =   '<table class="table table-responsive table-hover table-bordered" style="display:table; outline-style: solid; outline-width: 1px;" >';
            $table_headers  =   "<thead><tr>";
            $table_body     =   "<tbody>";
            $rows           =   "<tr>";
        
                foreach ($company_totals as $key => $Array_value) {
                    $indexCounterMain   =   0;
                    foreach ($Array_value as $company_total => $company_total_value) {
                        switch ($indexCounterMain) {
                            case (0):
                                $table_headers  .=  "<th class=' performance_dash_header' >".ucwords(str_replace('_'," ",$company_total))."</th>";
                                break;
                            case (1):
                                $table_headers  .=  "<th class='text-center performance_dash_header' >".ucwords(str_replace('_'," ",$company_total))."</th>";
                                break;
                            case (2):
                                $table_headers  .=  "<th class='text-center' style='color:#ffffff;background-color:#74BBC9!important'>".ucwords(str_replace('_'," ",$company_total))."</th>";
                                break;
                            case (3):
                                $table_headers  .=  "<th class='text-center' style='color:#ffffff;background-color:#74BBC9;!important'>".ucwords(str_replace('_'," ",$company_total))."</th>";
                                break;
                            case (4):
                                $table_headers  .=  "<th class='text-center' style='color:#ffffff;background-color:#74BBC9!important;'>".ucwords(str_replace('_'," ",$company_total))."</th>";
                                break;
                            case (5 ):
                                $table_headers  .=  "<th class='text-center' style='color:#ffffff;background-color:#74BBC9!important'>".ucwords(str_replace('_'," ",$company_total))."</th>";
                                break;
                            case (6):
                                $table_headers  .=  "<th class='text-center performance_dash_header' '>".ucwords(str_replace('_'," ",$company_total))."</th>";
                                break;
                            case (7):
                                $table_headers  .=  "<th class='text-center' style='color:#ffffff;background-color:#61A3BF!important'>".ucwords(str_replace('_'," ",$company_total))."</th>";
                                break;
                            
                            default:
                            $table_headers  .=  "<th>".ucwords(str_replace('_'," ",$company_total))."</th>";
                                break;
                        }
                        if(!is_numeric($company_total_value)){
                            $rows   .=  "<td  style='color: #00164E; font-weight: bold;'>".$company_total_value."</td>";
                         }
                        else{
                             if($indexCounterMain==1){
                                $rows   .=  "<td class='text-center'  style='color: #00164E; font-weight: bold;'>".$company_total_value."</td>";
                             }else{
                                $rows   .=  "<td class='text-center'  style='color: #00164E; font-weight: bold;'>".round($company_total_value,1)."%</td>";
                             }
                         }
                         $indexCounterMain++;
                    }
                }
                $table_headers.="</tr></thead>";
                $rows.="</tr>";
               
              
         

            foreach ($orglevel5_totals as $key => $value){
                // logger("open row for main org");
                $rows       .="<tr>";
                $data[]     = $value;
                $indexCounterSecondLevel    =   0; 
                foreach ($value as $orgLevel5_keys => $orgLevel5_values) {
                    if(!is_numeric($orgLevel5_values)){
                       $rows   .=  "<td  style='color: #00164E; fontweight: bold;'>".$orgLevel5_values."</td>";
                    }else{
                        if($indexCounterSecondLevel==1){
                            $rows   .=  "<td class='text-center'  style='color: #00164E; font-weight: bold;'>".$orgLevel5_values."</td>";
                        }else{
                            $rows   .=  "<td class='text-center'  style='color: #00164E; font-weight: bold;'>".round($orgLevel5_values,1)."%</td>";
                        }
                    }
                    $indexCounterSecondLevel++;
                    // logger("row data for main org");
                 }
                 $rows.="</tr>";
                 
                //  logger("close row for main org");
                //  logger($orglevel6_totals);
                 
                foreach ($orglevel6_totals as $orglevel6_totals_keys => $orglevel6_totals_item){
                    $i  =   0;
                    if ($orglevel6_totals_item->orglevel5 == $value->area){
                         $rows  .=  "<tr>";
                        foreach ($orglevel6_totals_item as $orglevel6_key => $orglevel6_value) {
                            if($i <> 1) {
                                if(is_numeric($orglevel6_value) && $i<>2){
                                    $rows   .=  "<td class='text-center'>".round($orglevel6_value,1)."%</td>";
                                }else{
                                    $rows   .=  "<td class='text-center'>".$orglevel6_value."</td>";
                                }

                            }
                           
                            $i++;
                        }
                        $data[] = $orglevel6_totals_item;
                        $rows.="</tr>"; 
                    }
                    
                }
               
                // logger("close row for nested");
            }
           
            $table.=$table_headers;
            $table.=$table_body;
            $table.=$rows;
           
            $table.="</tbody></table>";





            return array('error'=>false,'response'=>$table,'message'=>"Successfully loaded data");

        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }

    public static function getCompanyTotalPerformanceRatings($orglevel){

        try{
            $cycle_id           = Cycle::getLiveCycleID(); //todo: update cycle id
            $user               = Auth::user();
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $collection         = null;
            $manager_pn         = $user->personnelnumber;

            $collection = DB::table('vw_employees_export')
                ->where('instance_indicator_id', $active_instance_id);
            $collection = Filters::applyFilters($collection, Auth::user());

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $collection->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)

                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                        ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

                });
            }

            $collection->select(
                $orglevel .' as area', 
                DB::raw('count(*) as head_count'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 1 THEN 1 END)/COUNT(*))*100),1) as not_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 2 THEN 1 END)/COUNT(*))*100),1) as partially_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 3 THEN 1 END)/COUNT(*))*100),1) as fully_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 4 THEN 1 END)/COUNT(*))*100),1) as exceptionally_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 5 THEN 1 END)/COUNT(*))*100),1) as tea'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id IS NULL THEN 1 END)/COUNT(*))*100),1) as unknown'))
                ->whereNotNull($orglevel)
                ->groupBy($orglevel);

            return $collection->get();
            // Logger($collection);


        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }

    public static function getBusinessUnitTotalPerformanceRatings(){

        try{
            $cycle_id           = Cycle::getLiveCycleID(); //todo: update cycle id
            $user               = Auth::user();
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $collection         = null;
            $manager_pn         = $user->personnelnumber;

            $collection = DB::table('vw_employees_export')
                ->where('instance_indicator_id',     $active_instance_id);
            $collection = Filters::applyFilters($collection, Auth::user());
            $collection = $collection->where('instance_indicator_id','=',$active_instance_id);

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $collection->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)

                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                        ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

                });
            }

            $collection->select('orglevel6 as area', 'orglevel5 as orglevel5', DB::raw('count(*) as head_count'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 1 THEN 1 END)/COUNT(*))*100),1) as not_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 2 THEN 1 END)/COUNT(*))*100),1) as partially_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 3 THEN 1 END)/COUNT(*))*100),1) as fully_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 4 THEN 1 END)/COUNT(*))*100),1) as exceptionally_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id = 5 THEN 1 END)/COUNT(*))*100),1) as tea'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating_id IS NULL THEN 1 END)/COUNT(*))*100),1) as unknown'))
                ->groupBy('orglevel5')
                ->groupBy('orglevel6');

            return $collection->get();
        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }

    public function businessReviewOverAllPerformance(Request $request){

    }

    public function performanceRatingDistribution(){
        try{
            $cycle_id           = Cycle::getLiveCycleID(); //todo: update cycle id
            $user               = Auth::user();
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $collection         = null;
            $manager_pn         = $user->personnelnumber;
            $collection = DB::table('vw_employees_export')
                //->where('cycle_id',                 $cycle_id)
                ->where('instance_indicator_id',     $active_instance_id)
            ;
            $collection = Filters::applyFilters($collection, Auth::user());

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $collection->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)

                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                        ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

                });
            }

            $collection->select(
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = 1 THEN 1 END)/COUNT(*))*100),1) as not_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = 2 THEN 1 END)/COUNT(*))*100),1) as partially_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = 3 THEN 1 END)/COUNT(*))*100),1) as fully_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = 4 THEN 1 END)/COUNT(*))*100),1) as exceptionally_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = 5 THEN 1 END)/COUNT(*))*100),1) as tea'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating IS NULL THEN 1 END)/COUNT(*))*100),1) as unknown'))
                ->whereNotNull('orglevel1')
                ->groupBy('orglevel1');
            return array('error'=>false,'response'=>$collection->get()[0],'message'=>'Success');


        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }

    public function raceRatingDistribution(){
      $arr_races =  self::getCompanyTotalPerformanceRatings('race');
    
        $data = [];
        foreach ($arr_races as $key => $race) {
            $data[$race->area] = [(float)$race->not_achieved, (float)$race->partially_achieved, (float)$race->fully_achieved, (float)$race->exceptionally_achieved, (float)$race->tea ];
        }
        return array('error'=>false,'response'=>$data,'message'=>'Success');

    }

    public function bandRatingDistribution(){
        $arr_bands =  self::getCompanyTotalPerformanceRatings('jobgrade');
        $data = [];

        $t1 = 0;
        $t2 = 0;
        $t3 = 0;
        $t4 = 0;
        $t5 = 0;

        foreach ($arr_bands as $arr_band => $values) {

            $t1 += (float)$values->not_achieved;
            $t2 += (float)$values->partially_achieved;
            $t3 += (float)$values->fully_achieved;
            $t4 += (float)$values->exceptionally_achieved;
            $t5 += (float)$values->tea;
        }

        $b1 = [0,0,0,0,0];
        $b2 = [0,0,0,0,0];
        $b3 = [0,0,0,0,0];
        $b4 = [0,0,0,0,0];
        $b5 = [0,0,0,0,0];
        foreach ($arr_bands as $arr_band => $values) {
            $percentatge_tot1 =  $t1 == 0 ? 0 : ((float)$values->not_achieved / $t1) * 100;
            $percentatge_tot2 =  $t2 == 0 ? 0 : ((float)$values->partially_achieved / $t2) * 100;
            $percentatge_tot3 =  $t3 == 0 ? 0 : ((float)$values->fully_achieved / $t3) * 100;
            $percentatge_tot4 =  $t4 == 0 ? 0 : ((float)$values->exceptionally_achieved / $t4) * 100;
            $percentatge_tot5 =  $t5 == 0 ? 0 : ((float)$values->tea / $t5) * 100;
            switch ($values->area) {
                case "B1":
                    $data[$values->area] = [ROUND($percentatge_tot1, 2), ROUND($percentatge_tot2, 2),
                        ROUND($percentatge_tot3, 2), ROUND($percentatge_tot4, 2),
                        ROUND($percentatge_tot5, 2)];
                    break;
                case "B2":
                    $data[$values->area] = [ROUND($percentatge_tot1, 2), ROUND($percentatge_tot2, 2),
                        ROUND($percentatge_tot3, 2), ROUND($percentatge_tot4, 2),
                        ROUND($percentatge_tot5, 2)];
                    break;
                case "B3":
                    $data[$values->area] = [ROUND($percentatge_tot1, 2), ROUND($percentatge_tot2, 2),
                        ROUND($percentatge_tot3, 2), ROUND($percentatge_tot4, 2),
                        ROUND($percentatge_tot5, 2)];
                    break;
                case "B4":
                    $data[$values->area] = [ROUND($percentatge_tot1, 2), ROUND($percentatge_tot2, 2),
                        ROUND($percentatge_tot3, 2), ROUND($percentatge_tot4, 2),
                        ROUND($percentatge_tot5, 2)];
                    break;
                case "B5":
                    $data[$values->area] = [ROUND($percentatge_tot1, 2), ROUND($percentatge_tot2, 2),
                        ROUND($percentatge_tot3, 2), ROUND($percentatge_tot4, 2),
                        ROUND($percentatge_tot5, 2)];
                    break;
                default:
                    break;
            }
        }

        /*foreach ($arr_bands as $key => $race) {
            $data[$race>area] = [(float)$race>not_achieved, (float)$race>partially_achieved, (float)$race>fully_achieved, (float)$race>exceptionally_achieved, (float)$race>tea ];
        }*/
        return array('error'=>false,'response'=>$data,'message'=>'Success');

    }

    public function genderRatingDistribution(){
        try{
            $cycle_id           = Cycle::getLiveCycleID(); //todo: update cycle id
            $user               = Auth::user();
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $collection         = null;
            $manager_pn         = $user->personnelnumber;

            $collection = DB::table('vw_employees_export')
                //->where('cycle_id',                 $cycle_id)
                ->where('instance_indicator_id',     $active_instance_id);
                $collection = Filters::applyFilters($collection, Auth::user());

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){

                
                $collection->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)

                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                        ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

                });
            }

            $collection->select('gender', DB::raw('count(*) as total'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = 1 THEN 1 END)/COUNT(*))*100),1) as not_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = 2 THEN 1 END)/COUNT(*))*100),1) as partially_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = 3 THEN 1 END)/COUNT(*))*100),1) as fully_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = 4 THEN 1 END)/COUNT(*))*100),1) as exceptionally_achieved'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating = 5 THEN 1 END)/COUNT(*))*100),1) as tea'),
                DB::raw('ROUND(((COUNT(CASE WHEN current_rating IS NULL THEN 1 END)/COUNT(*))*100),1) as unknown'))
                ->whereNotNull('gender')
                ->groupBy('gender');



            $arrMale = [0,0,0,0,0,0];
            $arrFemale = [0,0,0,0,0,0];
            $arr_genders = $collection->get();

            foreach ($arr_genders as $arr_gender => $values) {
                if ($values->gender == 'Male') {
                    $arrMale = [(float)$values->not_achieved, (float)$values->partially_achieved, (float)$values->fully_achieved, (float)$values->exceptionally_achieved,(float) $values->tea, (float)$values->unknown];

                } elseif ($values->gender == 'Female') {
                    $arrFemale = [(float)$values->not_achieved, (float)$values->partially_achieved, (float)$values->fully_achieved, (float)$values->exceptionally_achieved, (float)$values->tea, (float)$values->unknown];
                }
            }


            return array('error'=>false,'response'=>['male' => $arrMale, 'female' =>$arrFemale],'message'=>'Success');


        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }
    //{ Performance  export }
    public function PerformanceRatingExport(){

        $array_to_export = [];
        $company_totals = self::getCompanyTotalPerformanceRatings('orglevel1');

        foreach ($company_totals as $dbperformance => $values){
            $array_to_export[] = [
                "Area"                  =>$values->area,
                "Headcount"             =>$values->head_count,
                "Not Achieved"          =>$values->not_achieved . ' %',
                "Partially Achieved"    =>$values->partially_achieved . ' %',
                "Fully Achieved"        =>$values->fully_achieved . ' %',
                "Exceptionally Achieved"=>$values->exceptionally_achieved . ' %',
                "TEA"                   =>$values->tea . ' %',
                "Unknown Rating"        =>$values->unknown . ' %',
            ];
        }
        $orglevel5_totals = self::getCompanyTotalPerformanceRatings('orglevel5');
        $orglevel6_totals = self::getBusinessUnitTotalPerformanceRatings();

        //dd($orglevel6_totals);
        foreach ($orglevel5_totals as $dbcluster => $value) {
            $array_to_export[] = [
                "Area"                  =>$value->area,
                "Headcount"             =>$value->head_count,
                "Not Achieved"          =>$value->not_achieved . ' %',
                "Partially Achieved"    =>$value->partially_achieved . ' %',
                "Fully Achieved"        =>$value->fully_achieved . ' %',
                "Exceptionally Achieved"=>$value->exceptionally_achieved . ' %',
                "TEA"                   =>$value->tea . ' %',
                "Unknown Rating"        =>$value->unknown . ' %',
            ];

            foreach ($orglevel6_totals as $dbbusinessunit => $values)
            {
                if ($values->orglevel5 == $value->area) {

                    $array_to_export[] = [
                        "Area"                  =>$values->area,
                        "Headcount"             =>$values->head_count,
                        "Not Achieved"          =>$values->not_achieved . ' %',
                        "Partially Achieved"    =>$values->partially_achieved . ' %',
                        "Fully Achieved"        =>$values->fully_achieved . ' %',
                        "Exceptionally Achieved"=>$values->exceptionally_achieved . ' %',
                        "TEA"                   =>$values->tea . ' %',
                        "Unknown Rating"        =>$values->unknown . ' %',
                    ];
                }
            }
        }
        $filename = 'performance_rating.xlsx';
        $export = new ExportPerformanceRating($array_to_export);
        return Excel::download($export, $filename);
    }
    //{ Issue Log }
    public function getIssueLogDashBoard(Request $request)
    {
        try {

            $not_started = IssueLog::getFilteredIssueLogs('',0,10, false,  12);
            //print_r('Not Started');print_r($not_started);
            $on_progress = IssueLog::getFilteredIssueLogs('',0,10, false,  13);
            $closed = IssueLog::getFilteredIssueLogs('',0,10, false,  14);

            /*Log::warning("Total Dashboard: ");
            logger(count($not_started));
            logger(count($on_progress));
            logger(count($closed));*/

            $total = count($not_started) + count($on_progress) + count($closed);
            if($total===0){
                $total = 1;
            }

            $array = array();
            $array[0]=array("label"=>'Not Started',"custome_value"=>round(count($not_started),2), "value"=>(round((count($not_started)/$total)*100,2)));
            $array[1]=array("label"=>'On Progress',"custome_value"=>round(count($on_progress),2), "value"=>(round((count($on_progress)/$total)*100,2)));
            $array[2]=array("label"=>'Closed',"custome_value"=>round(count($closed),2), "value"=>(round((count($closed)/$total)*100,2)));

            $data = array(
                "chart"=> array(
                    "caption"=> "",
                    "subcaption"=> "",
                    "xaxisname"=> "",
                    "yaxisname"=> "",
                    "numbersuffix"=> "%",
                    "theme"=> "gammel",
                    "paletteColors" =>"#C80F3F,#DC8E18,#ADD581",
                    "bgColor"=> "#ffffff",
                ),
                'data'=> $array
            );
            return array ('error'=>false,'response'=>$data,'message'=>'Dashboard loaded');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => "DashBoard Could not be loaded");
        }
    }
    public function getDemographicIncreases(Request $request){

        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $userType           = PermissionController::getUserInstance()['response']['instance'];
        $res                = PerformanceRating::getAllDemographicsIncreaseRecords();

        return ['data'=>$res];

    }
    public function getDemographicIncentives(Request $request){

        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $userType = PermissionController::getUserInstance()['response']['instance'];
        
        $res        = PerformanceRating::getAllDemographicsIncentivesRecords();

        return ['data'=>$res];

    }
    public function getSalaryincrease(Request $request){

        $active_instance = PermissionController::getUserInstance()['response']['instance'];
        $userType = PermissionController::getUserInstance()['response']['instance'];
        
        $res        = PerformanceRating::getSalaryincreaseRecords();

        return ['data'=>$res];

    }

    public function getReviewPerformance(Request $request){
        
        $active_instance        =   PermissionController::getUserInstance()['response']['instance'];
        $userType               =   PermissionController::getUserInstance()['response']['instance'];

        ///////////////////////////Business Review Performance ***UPLIFT*** PER BAND///////////////////////////
        $dbpopulateSalUplift    = PerformanceRating::getPopulateSalUplift();

        $uplift_b1b2    =   [0,0,0,0,0];
        $uplift_b3      =   [0,0,0,0,0];
        $uplift_b4b5    =   [0,0,0,0,0];

        foreach( $dbpopulateSalUplift as $key => $value ){

            $uplift_b1b2    =   [
                                    ROUND($value->uplift_not_achievedb1b2, 1),
                                    ROUND($value->uplift_partially_achievedb1b2, 1),
                                    ROUND($value->uplift_fully_achievedb1b2, 1),
                                    ROUND($value->uplift_exceptionally_achievedb1b2, 1),
                                    ROUND($value->uplift_too_early_assessb1b2, 1)
                                ];

            $uplift_b3      =   [
                                    ROUND($value->uplift_not_achievedb3, 1),
                                    ROUND($value->uplift_partially_achievedb3, 1),
                                    ROUND($value->uplift_fully_achievedb3, 1),
                                    ROUND($value->uplift_exceptionally_achievedb3, 1),
                                    ROUND($value->uplift_too_early_assessb3, 1)
                                ];

            $uplift_b4b5    =   [   
                                    ROUND($value->uplift_not_achievedb4b5, 1),
                                    ROUND($value->uplift_partially_achievedb4b5, 1),
                                    ROUND($value->uplift_fully_achievedb4b5, 1),
                                    ROUND($value->uplift_exceptionally_achievedb4b5, 1),
                                    ROUND($value->uplift_too_early_assessb4b5, 1)
                                ];
        }

        ///////////////////////////Business Review Performance ***PER BAND*** ///////////////////////////
        $dbperformanceb1b2      =   PerformanceRating::getPerformanceb1b2();
        $dbperformanceb4b5      =   PerformanceRating::getPerformanceb4b5();
        $dbperformanceGeneral   =   PerformanceRating::getPerformanceGeneral();
        $dbperformanceSMI       =   PerformanceRating::getPerformanceSMI();

        $arr_performanceb1b2    =   [0, 0, 0, 0, 0];
        $arr_performanceb4b5    =   [0, 0, 0, 0, 0];
        $arr_performanceGeneral =   [0, 0, 0, 0, 0];
        $arr_performanceSMI     =   [0, 0, 0, 0, 0];

        foreach ( $dbperformanceb1b2 as $dbperformance => $values ) {
            $arr_performanceb1b2    =   [
                                            (float)$values->tot1,
                                            (float)$values->tot2,
                                            (float)$values->tot3,
                                            (float)$values->tot4,
                                            (float)$values->tot5
                                        ];
        }
        foreach ( $dbperformanceb4b5 as $dbperformance => $values ) {
            $arr_performanceb4b5    =   [
                                            (float)$values->tot1,
                                            (float)$values->tot2,
                                            (float)$values->tot3,
                                            (float)$values->tot4,
                                            (float)$values->tot5
                                        ];
        }
        foreach ( $dbperformanceGeneral as $dbperformance => $values ) {
            $arr_performanceGeneral =   [
                                            (float)$values->tot1,
                                            (float)$values->tot2,
                                            (float)$values->tot3,
                                            (float)$values->tot4,
                                            (float)$values->tot5
                                        ];
        }
        foreach ( $dbperformanceSMI as $dbperformance => $values ) {
            $arr_performanceSMI =   [
                                        (float)$values->tot1,
                                        (float)$values->tot2,
                                        (float)$values->tot3,
                                        (float)$values->tot4,
                                        (float)$values->tot5
                                    ];
        }

        ///////////////////////////////////////Business Review Performance 1  Current total package///////////////////////
        $arr_package    =   PerformanceRating::getArr_package();

        $arr1_package   =   [0,0,0];
        $arr2_package   =   [0,0,0];
        $arr3_package   =   [0,0,0];
        $arr4_package   =   [0,0,0];
        $arr5_package   =   [0,0,0];

  
        foreach ( $arr_package as $values ){
            switch ( $values->jobgrade ){
                case "B1":
                    $arr1_package   =   [
                                            $values->band_id,
                                            $values->currentTotPackage,
                                            $values->proposedTotPackage
                                        ];
                    break;
                case "B2":
                    $arr2_package   =   [
                                            $values->band_id,
                                            $values->currentTotPackage,
                                            $values->proposedTotPackage
                                        ];
                    break;
                case "B3":
                    $arr3_package   =   [
                                            $values->band_id,
                                            $values->currentTotPackage,
                                            $values->proposedTotPackage
                                        ];
                    break;
                case "B4":
                    $arr4_package   =   [
                                            $values->band_id,
                                            $values->currentTotPackage,
                                            $values->proposedTotPackage
                                        ];
                    break;
                case "B5":
                    $arr5_package   =   [
                                            $values->band_id,
                                            $values->currentTotPackage,
                                            $values->proposedTotPackage
                                        ];
                    break;
                default:
                    break;
            }
        }


        $arr_score      =   PerformanceRating::getScores(); 
        $TotPackage     =   PerformanceRating::getTotalPackage(); /**General Staff Bonus Performance Review 1  */
                


        $TotPackageb1b2 =   [0,0,0];
        $TotPackageb3   =   [0,0,0];
        $TotPackageb4b5 =   [0,0,0];

        foreach ( $TotPackage as $values ){
            if( !is_null($values->proposedTotPackageb1b2) && !is_null($values->currentTotPackageb1b2) ){
                $TotPackageb1b2 = [$values->proposedTotPackageb1b2,$values->currentTotPackageb1b2];
            }else{ $TotPackageb1b2; }
            
            if( !is_null($values->proposedTotPackageb3) && !is_null($values->currentTotPackageb3) ){
                $TotPackageb3   = [$values->proposedTotPackageb3,$values->currentTotPackageb3];
            }else{ $TotPackageb3; }
            if( !is_null($values->proposedTotPackageb4b5) && !is_null($values->currentTotPackageb4b5) ){
                $TotPackageb4b5 = [(int)$values->proposedTotPackageb4b5,(int)$values->currentTotPackageb4b5];
            }else{ $TotPackageb4b5; }
               
        }

        ///////////////////////////Business Review Performance Bonus /////////////////////////////
        $arr_bonus      =   PerformanceRating::getBonus();

        $arr1_bonus =   [0,0,0]; 
        $arr2_bonus =   [0,0,0];
        $arr3_bonus =   [0,0,0];
        $arr4_bonus =   [0,0,0];
        $arr5_bonus =   [0,0,0];
       

        foreach ( $arr_bonus as $values ){
            switch ($values->band_id){
                case 1:
                    $arr1_bonus = [
                        $values->band_id, 
                        $values->currentTotPackage, 
                        $values->proposedTotPackage
                    ];
                    break;
                case 2:
                    $arr2_bonus =[
                        $values->band_id, 
                        $values->currentTotPackage, 
                        $values->proposedTotPackage
                    ];
                    break;
                case 3:
                    $arr3_bonus =[
                        $values->band_id, 
                        $values->currentTotPackage, 
                        $values->proposedTotPackage
                    ];
                    break;
                case 4:
                    $arr4_bonus =[
                        $values->band_id, 
                        $values->currentTotPackage, 
                        $values->proposedTotPackage
                    ];
                    break;
                case 5:
                    $arr5_bonus =[
                        $values->band_id, 
                        $values->currentTotPackage, 
                        $values->proposedTotPackage
                    ];
                    break;
                default:
                    break;
            }
        }

        ///////////////////////////Business Review Performance ******Distribution****** /////////////////////////////

        $dbperformances         =   PerformanceRating::getAlldbPerformanceReview(); 
        $arr_performanceChart   = [0, 0, 0, 0, 0];
        $normalDistribution     = [0, 10, 70, 20, 0];
        $rows = '';


        $packageByBands         = PerformanceRating::getPackageByBands();
        $packageByBand = null;

        foreach ($packageByBands as $packageByBand_ ){
            if ($packageByBand_->instance_indicator_id = $active_instance){
                $packageByBand = $packageByBand_;
            }
        }


        foreach ( $dbperformances as $dbperformance => $values ) {
            $arr_performanceChart = [
                                        (float)$values->tot1, 
                                        (float)$values->tot2, 
                                        (float)$values->tot3, 
                                        (float)$values->tot4,
                                        (float)$values->tot5
                                    ];
        }

            return [
                    'packageByBand'         =>$packageByBand,
                    'arr1_package'          =>$arr1_package,
                    'arr2_package'          =>$arr2_package,
                    'arr3_package'          =>$arr3_package,
                    'arr4_package'          =>$arr4_package,
                    'arr5_package'          =>$arr5_package,
                    'uplift_b1b2'           =>$uplift_b1b2, 
                    'uplift_b3'             =>$uplift_b3,
                    'uplift_b4b5'           =>$uplift_b4b5,
                    'arr_performanceChart'  =>$arr_performanceChart,
                    'normalDistribution'    =>$normalDistribution,
                    'arr1_bonus'            =>$arr1_bonus,
                    'arr2_bonus'            =>$arr2_bonus,
                    'arr3_bonus'            =>$arr3_bonus,
                    'arr4_bonus'            =>$arr4_bonus,
                    'arr5_bonus'            =>$arr5_bonus,
                    'TotPackageb1b2'        =>$TotPackageb1b2,
                    'TotPackageb3'          =>$TotPackageb3,
                    'TotPackageb4b5'        =>$TotPackageb4b5,
                    'arr_performanceb1b2'   =>$arr_performanceb1b2,
                    'arr_performanceb4b5'   =>$arr_performanceb4b5,
                    'arr_performanceGeneral'=>$arr_performanceGeneral,
                    'arr_performanceSMI'    =>$arr_performanceSMI
            ];
    }
    public function getSalaryIncreaseAnalysis(Request $request){

        $active_instance    = PermissionController::getUserInstance()['response']['instance'];
        $userType           = PermissionController::getUserInstance()['response']['instance'];
        
        $dbperformances     = PerformanceRating::getAlldbPerformanceReview();
        $avgClusters        = PerformanceRating::getAllAvgClusters();

        $avgBand            = PerformanceRating::getAllAvgBands();
        $avgBandSalVal      = PerformanceRating::getSalValueAvg();

        $avgBProposedVal    = PerformanceRating::getProposedSalValueAvg();
        $dbgetProposeSpent  = PerformanceRating::getProposeSpent();
        $dbsalsummary       = PerformanceRating::getPopulateTable();
        $contIncrease       = PerformanceRating::headCount();
        $countLessY         = PerformanceRating::StatDate();


       
        $arr_clusterName =[];
        $arr_clusterAvgSal=[];
        $arr_bandName =[];
        $arr_bandAvgSal=[0,0,0,0,0];

        $arr_salAvg =[0,0,0,0,0];
        $arr_salName =[];

        $arr_proposedAvg =[0,0,0,0,0];
        $arr_proposedName =[];

        $arr_current_rating =[];
        $arr_spentavg =[];
        $arr_spenttot =[];

        $arr_sal_values =[];
        $contIncr = 0;
        foreach ($contIncrease as $value){
            $contIncr = $value->headcount;
        }


        $tot1Avg1=0;
        $tot1Avg2=0;
        $tot1Avg3=0;
        $tot1Avg4=0;
        $tot1Avg5=0;

        foreach ($dbgetProposeSpent as $key => $value){
            switch ($value->current_rating_id){
                case '1':
                    $tot1Avg1 = $value->bandsSalAvg;
                    break;
                case '2':
                    $tot1Avg2 = $value->bandsSalAvg;
                    break;
                case '3':
                    $tot1Avg3 = $value->bandsSalAvg;
                    break;
                case '4':
                    $tot1Avg4 = $value->bandsSalAvg;
                    break;
                case '5':
                    $tot1Avg5 = $value->bandsSalAvg;
                    break;
                default:
                    break;
            }
          
        }
        foreach ($dbgetProposeSpent as $key => $value){
            $arr_spenttot[] = $value->bandsSalAvg;
        }

        $totalSpent=(array_sum($arr_spenttot));

        if ($totalSpent==0){
            $totalSpent = 1;
        }

        $arr_spentavg = [ROUND(($tot1Avg1/$totalSpent)*100,2),ROUND(($tot1Avg2/$totalSpent)*100,2),
        ROUND(($tot1Avg3/$totalSpent)*100,2),ROUND(($tot1Avg4/$totalSpent)*100,2),
        ROUND(($tot1Avg5/$totalSpent)*100,2)];

            $bandtot1 = 0;$bandtot2 = 0;$bandtot3 = 0;$bandtot4 = 0;$bandtot5 = 0;
             foreach ($avgBandSalVal as $key => $value){
            switch ($value->band_id) {
            case "1":
                $bandtot1 = $value->bandsSalAvg;
                break;
            case "2":
                $bandtot2 = $value->bandsSalAvg;
                break;
            case "3":
                $bandtot3 = $value->bandsSalAvg;
                break;
            case "4":
                $bandtot4 = $value->bandsSalAvg;
                break;
            case "5":
                $bandtot5 = $value->bandsSalAvg;
                break;
            default:
                break;
        }
                    $arr_salAvg = [$bandtot1,$bandtot2,$bandtot3,$bandtot4,$bandtot5];
             }
             $avgbandtot1=0;$avgbandtot2=0;$avgbandtot3=0;$avgbandtot4=0;$avgbandtot5=0;
             foreach ($avgBProposedVal as $key => $value){
                 switch ($value->band_id) {
                     case "1":
                         $avgbandtot1 = $value->bandsSalAvg;
                         break;
                     case "2":
                         $avgbandtot2 = $value->bandsSalAvg;
                         break;
                     case "3":
                         $avgbandtot3 = $value->bandsSalAvg;
                         break;
                     case "4":
                         $avgbandtot4 = $value->bandsSalAvg;
                         break;
                     case "5":
                         $avgbandtot5 = $value->bandsSalAvg;
                         break;
                     default:
                         break;
                 }
                 $arr_proposedAvg = [$avgbandtot1,$avgbandtot2,$avgbandtot3,$avgbandtot4,$avgbandtot5];
 
            /*     if ($value->jobgrade != "0"){
                     $arr_proposedAvg[]=$value->bandsSalAvg;
                     $arr_proposedName[]=$value->jobgrade;
                 }*/
             }
             $tot1 = 0;
             $tot2 = 0;
             $tot3 = 0;
             $tot4 = 0;
             $tot5 = 0;
             foreach ($avgBand as $key => $value){
                 switch ($value->current_rating_id){
                     case '1':
                         $tot1 = $value->tot1;
                         break;
                     case '2':
                         $tot2 = $value->tot1;
                         break;
                     case '3':
                         $tot3 = $value->tot1;
                         break;
                     case '4':
                         $tot4 = $value->tot1;
                         break;
                     case '5':
                         $tot5 = $value->tot1;
                         break;
                     default:
                         break;
                 }
             }
             $arr_bandAvgSal = [$tot1,$tot2,$tot3,$tot4,$tot5];
 
             foreach ($avgClusters as $key => $value){
 
                     $arr_clusterName[] = $value->orglevel5;
                     $arr_clusterAvgSal[] = $value->clusterAvg;
 
             }
             $arr_performanceChart = [0, 0, 0, 0, 0];
             $rows = '';


             foreach ($dbperformances as $dbperformance => $values) {
                $arr_performanceChart = [
                    (float)$values->tot1, 
                    (float)$values->tot2,
                    (float)$values->tot3,
                    (float)$values->tot4,
                    (float)$values->tot5
                    // (float)$values->tot6
                ];
            }
            $chartData = [];
            foreach($arr_clusterName as $key => $value ){
                $chartData[$key]['label'] = $value;
                foreach($arr_clusterAvgSal as $def => $def_values){
                    $chartData[$def]['value'] = round($def_values,0);

                }
              
              }
              $PerfomanceChart=[];
              $PerfomanceLabels=['Not Achieved','Partially Achieved','Fully Achieved','Exceptionally Achieved','Too early to assess '];
            
            // logger($arr_bandAvgSal);
            
              foreach($PerfomanceLabels as $key => $value ){
                $PerfomanceChart[$key]['label'] = $value;
                foreach($arr_bandAvgSal as $def => $def_values){
                    $PerfomanceChart[$def]['value'] =  round($def_values,1);

                }
              
              }
                $LineChart=[];
                foreach ($arr_performanceChart as $key => $value) {
                    $LineChart[$key]['value'] =  round($value,1);
                    foreach ($PerfomanceLabels as $key2 => $valueV2) {
                        $LineChart[$key2]['label'] = $valueV2;
                    }
                }
                $bandLabels = ['B1','B2','B3','B4','B5'];

                $AveragePercentagebandChart=[];
                foreach ($arr_salAvg as $key => $value) {
                    $AveragePercentagebandChart[$key]['value'] =  round($value,1);
                    foreach ($bandLabels as $key2 => $valueV2) {
                        $AveragePercentagebandChart[$key2]['label'] = $valueV2;
                    }
                }
                $AverageIncreasebandChart=[];

                foreach ($arr_proposedAvg as $key => $value) {
                    $AverageIncreasebandChart[$key]['value'] =  round($value,0);
                    foreach ($bandLabels as $key2 => $valueV2) {
                        $AverageIncreasebandChart[$key2]['label'] = $valueV2;
                    }
                }
                




        return [
            'PerfomanceChart'=>$PerfomanceChart,
            'LineChart'=>$LineChart,
            'chartData'=>$chartData,
            'AveragePercentagebandChart'=>$AveragePercentagebandChart,
            'arr_performanceChart'=>$arr_performanceChart,
            'AverageIncreasebandChart'=>$AverageIncreasebandChart,

            'arr_clusterName'=>$arr_clusterName,
            'arr_clusterAvgSal'=>$arr_clusterAvgSal,

            'dbsalsummary'=>$dbsalsummary,

            'arr_bandAvgSal'=>$arr_bandAvgSal,
            'arr_bandName'=>$arr_bandName,

            'arr_salAvg'=>$arr_salAvg,

            'arr_proposedAvg'=>$arr_proposedAvg,
            'arr_proposedName'=>$arr_proposedName,

            'arr_spentavg'=>$arr_spentavg,

            
            'arr_current_rating'=>$arr_current_rating,
            
            'contIncr'=>$contIncr,
        ];


    }
    public function getIncentiveAnalysis(Request $request){
        $dbIncentives     = PerformanceRating::getIncentiveAnalData();
        $dbperfperscheme     = PerformanceRating::getdbPerformScheme();
        $avgclusters     = PerformanceRating::getdbAvgCluster();

        $arr_genderamt = [0,0,0,0,0];
        $arr_genderperc = [0,0,0,0,0];
        $arr_raceperc = [0,0,0,0,0];
        $arr_raceamt = [0,0,0,0,0];

        foreach ($dbIncentives as $value){
            $arr_genderamt = [(float)$value->b1perc , (float)$value->b2perc, (float)$value->b3perc,
                (float)$value->b4perc , (float)$value->b5perc];
    
            //proposed incentive percentage per band
            $arr_genderperc= [(float)$value->b1tot , (float)$value->b2tot, (float)$value->b3tot,
                (float)$value->b4tot , (float)$value->b5tot];
        }


        $arr_overallperf = [0,0,0,0,0];
        //
    
        foreach ($dbperfperscheme as $value){
            $arr_overallperf = [(float)$value->overalltot1 , (float)$value->overalltot2 ,(float)$value->overalltot3
                ,(float)$value->overalltot4 ,(float)$value->overalltot5];
        }
    
        //
        foreach ($dbperfperscheme as $value){
            $arr_raceperc = [(float)$value->tot1,(float)$value->tot2,(float)$value->tot3,
                (float)$value->tot4,(float)$value->tot5];
            
        }


        $arr_clusterName=[];
        $arr_clusterAvgSal = [];
        $BandLabel = ['B1','B2','B3','B4','B5'];
        $RankingLabel = ['Not achieved', 'Partially achieved', 'Fully achieved', 'Exceptionally achieved', 'Too early to assess'];
       
        foreach ($avgclusters as $key => $value){
            $arr_clusterName[] = $value->orglevel5;
            $arr_clusterAvgSal[] = $value->clusterAvg;
        }
        $clusterChart=[];
        foreach ($arr_clusterAvgSal as $key => $value) {
            $clusterChart[$key]['value'] = $value;
            foreach ($arr_clusterName as $key2 => $valueV2) {
                $clusterChart[$key2]['label'] = $valueV2;
            }
        }
        $AverageIncentiveBand=[];
        foreach ($arr_genderamt as $key => $value) {
            $AverageIncentiveBand[$key]['value'] = $value;
            foreach ($BandLabel as $key2 => $valueV2) {
                $AverageIncentiveBand[$key2]['label'] = $valueV2;
            }
        }

        $AverageIncentiveBandIncrease=[];
        foreach ($arr_genderperc as $key => $value) {
            $AverageIncentiveBandIncrease[$key]['value'] = $value;
            foreach ($BandLabel as $key2 => $valueV2) {
                $AverageIncentiveBandIncrease[$key2]['label'] = $valueV2;
            }
        }

        $AchieversChart=[];
        foreach ($arr_raceperc as $key => $value) {
            $AchieversChart[$key]['value'] = $value;
            foreach ($RankingLabel as $key2 => $valueV2) {
                $AchieversChart[$key2]['label'] = $valueV2;
            }
        }

        return [
            'clusterChart'=>$clusterChart,
            'AchieversChart'=>$AchieversChart,
            'AverageIncentiveBand'=>$AverageIncentiveBand,
            'AverageIncentiveBandIncrease'=>$AverageIncentiveBandIncrease,
             'arr_overallperf'=>$arr_overallperf,
            'arr_clusterAvgSal'=>$arr_clusterAvgSal,
             'arr_clusterName'=>$arr_clusterName,
            'arr_genderamt'=>$arr_genderamt,
            'arr_raceamt'=>$arr_raceamt,
            'arr_genderperc'=>$arr_genderperc,
            'arr_raceperc'=>$arr_raceperc,
            'dbIncentives'=>$dbIncentives,


        ];
    }
    public function getTopFiftyIncentive(Request $request){
        $dbsalaryanalysis     = PerformanceRating::getdbSalaryAnalysisTopFifty();
        $dbinsentiveanalysis     = PerformanceRating::getdbSalaryAnalysisTopFiftyIncentive();

            $arr_insentive = [];
            $arr_insentivetop20 = [];
            $arr_insentivetop30 = [];
            $arr_insentivetop40 = [];
            $arr_insentivetop50 = [];
            $arr_overallincentive = [];
            $arr_overallincentiveowner = [];
            $category = "";

            $top_filter =$request->selected ;
 
            $counter = 0;
            // $top_filter =1;

            if ($top_filter == 1){
                $category = "Incentive";
                 foreach($dbinsentiveanalysis  as $key => $value){
                     $arr_overallincentive[] = ROUND($value->proposedincentive_value,0);

                     $arr_overallincentiveowner[] = $value->lastname . ' ' . $value->knownas;
                     if($counter < 10){
                         $arr_insentive[] =  $value->proposedincentive_value;
                     }
                     else if($counter < 20){
                         $arr_insentivetop20[] =  $value->proposedincentive_value;
                     }
                     else if($counter < 30){
                         $arr_insentivetop30[] =  $value->proposedincentive_value;
                     }
                     else if($counter < 40){
                         $arr_insentivetop40[] =  $value->proposedincentive_value;
                     }
                     else if($counter < 50){
                         $arr_insentivetop50[] =  $value->proposedincentive_value;
                     }
                     $counter++;
                 }
             } else if ($top_filter == 2){
                 $category = "Proposed Salary";
                 foreach($dbsalaryanalysis  as $key => $value){
                     $arr_overallincentive[] = ROUND($value->proposedsalary_value,0);

                     $arr_overallincentiveowner[] = $value->lastname . ' ' . $value->knownas;
                     if($counter < 10){
                         $arr_insentive[] = $value->proposedsalary_value;
                     }
                     else if($counter < 20){
                         $arr_insentivetop20[] = $value->proposedsalary_value;
                     }
                     else if($counter < 30){
                         $arr_insentivetop30[] = $value->proposedsalary_value;
                     }
                     else if($counter < 40){
                         $arr_insentivetop40[] = $value->proposedsalary_value;
                     }
                     else if($counter < 50){
                         $arr_insentivetop50[] = $value->proposedsalary_value;
                     }
                     $counter++;
                 }
             } else if ($top_filter == 3){

             } else if ($top_filter == 4){

             }

             $rows = '';
             if ($arr_insentive != []) {
                 $rows .= '<h5>Top 10</h5>';
                $rows .= ' <table class="tablestriped tablehover" style={{width:"100%"}}>
                <thead className="">
                <tr>
                </tr>
                </thead>
                <tbody>';
                 $rows .= '<tr>';
                 $rows .= '<td >Highest '.$category . '</td>';
                 $rows .= '<td className="textcenter">ZAR ' . number_format(MAX($arr_insentive), 0, '.', ' ') . '</td>';
                 $rows .= '</td><tr>';
                 $rows .= '<td> Lowest '.$category . '</td>';
                 $rows .= '<td className="textcenter">ZAR ' . number_format(MIN($arr_insentive) , 0, '.', ' ') . '</td>';
                 $rows .= '</td><tr>';
                 $rows .= '<td>Average '.$category . '</td>';
                 $rows .= '<td className="textcenter">ZAR ' . number_format((array_sum($arr_insentive) / count($arr_insentive)), 0, '.', ' ') . '</td>';
                 $rows .= '</tr></tbody></table>';
             }
     //
                 $rows2 = '';
             if ($arr_insentivetop20 != []) {
                $rows2 .= '<h5>Next 20</h5>';
                $rows2 .= ' <table class="tablestriped tablehover" style={{width:"100%"}}>
                <thead className="">
                <tr>
                </tr>
                </thead>
                <tbody>';
                 $rows2 .= '<tr>';
                 $rows2 .= '<td>Highest '.$category . '</td>';
                 $rows2 .= '<td className="textcenter">ZAR ' . number_format(MAX($arr_insentivetop20), 0, '.', ' ') . '</td>';
                 $rows2 .= '</td><tr>';
                 $rows2 .= '<td> Lowest '.$category . '</td>';
                 $rows2 .= '<td className="textcenter">ZAR ' . number_format(MIN($arr_insentivetop20), 0, '.', ' ') . '</td>';
                 $rows2 .= '</td><tr>';
                 $rows2 .= '<td>Average '.$category . '</td>';
                 $rows2 .= '<td className="textcenter">ZAR ' . number_format((array_sum($arr_insentivetop20) / count($arr_insentivetop20)), 0, '.', ' ') . '</td>';
                 $rows2 .= '</tr></tbody></table>';
             }
                 //
                 $rows3 = '';
             if ($arr_insentivetop30 != []) {
                $rows3 .= '<h5>Next 30</h5>';
                $rows3 .= ' <table class="tablestriped tablehover" style={{width:"100%"}}>
                <thead className="">
                <tr>
                </tr>
                </thead>
                <tbody>';                 $rows3 .= '<tr>';
                 $rows3 .= '<td>Highest '.$category . '</td>';
                 $rows3 .= '<td className="textcenter">ZAR ' . number_format(MAX($arr_insentivetop30), 0, '.', ' ') . '</td>';
                 $rows3 .= '</td><tr>';
                 $rows3 .= '<td> Lowest '.$category . '</td>';
                 $rows3 .= '<td className="textcenter">ZAR ' . number_format(MIN($arr_insentivetop30), 0, '.', ' ') . '</td>';
                 $rows3 .= '</td><tr>';
                 $rows3 .= '<td>Average '.$category . '</td>';
                 $rows3 .= '<td className="textcenter">ZAR ' . number_format((array_sum($arr_insentivetop30) / count($arr_insentivetop30)), 0, '.', ' ') . '</td>';
                 $rows3 .= '</tr></tbody></table>';
             }
         //
                 $rows4 = '';
                 if ($arr_insentivetop40 != []) {
                    $rows4 .= '<h5>Next 40</h5>';
                    $rows4 .= ' <table class=" tablestriped tablehover" style={{width:"100%"}}>
                    <thead className="">
                    <tr>
                    </tr>
                    </thead>
                    <tbody>';                      $rows4 .= '<tr>';
                     $rows4 .= '<td>Highest '.$category . '</td>';
                     $rows4 .= '<td className="textcenter">ZAR ' . number_format(MAX($arr_insentivetop40), 0, '.', ' ') . '</td>';
                     $rows4 .= '</td><tr>';
                     $rows4 .= '<td> Lowest '.$category . '</td>';
                     $rows4 .= '<td className="textcenter">ZAR ' . number_format(MIN($arr_insentivetop40), 0, '.', ' ') . '</td>';
                     $rows4 .= '</td><tr>';
                     $rows4 .= '<td>Average '.$category . '</td>';
                     $rows4 .= '<td className="textcenter">ZAR ' . number_format((array_sum($arr_insentivetop40) / count($arr_insentivetop40)), 0, '.', ' ') . '</td>';
                     $rows4 .= '</tr></tbody></table>';
                    }
                 //
                 $rows5 = '';
             if ($arr_insentivetop50 != []) {
                $rows5 .= '<h5>Next 50</h5>';
                $rows5 .= ' <table class="tablestriped tablehover" style={{width:"100%"}}>
                <thead className="">
                <tr>
                </tr>
                </thead>
                <tbody>';                  $rows5 .= '<tr>';
                 $rows5 .= '<td>Highest ' .$category . '</td>';
                 $rows5 .= '<td className="textcenter">ZAR ' . number_format(MAX($arr_insentivetop50), 0, '.', ' ') . '</td>';
                 $rows5 .= '</td><tr>';
                 $rows5 .= '<td> Lowest '.$category . '</td>';
                 $rows5 .= '<td className="textcenter">ZAR ' . number_format(MIN($arr_insentivetop50), 0, '.', ' ') . '</td>';
                 $rows5 .= '</td><tr>';
                 $rows5 .= '<td>Average '.$category . '</td>';
                 $rows5 .= '<td className="textcenter">ZAR ' . number_format((array_sum($arr_insentivetop50) / count($arr_insentivetop50)), 0, '.', ' ') . '</td>';
                 $rows5 .= '</tr></tbody></table>';
                }
             $chartData=[];
             foreach ($arr_overallincentive as $key => $value) {
                $chartData[$key]['value'] = $value;
                foreach ($arr_overallincentiveowner as $key2 => $valueV2) {
                    $chartData[$key2]['label'] = $valueV2;
                }
            }

             return[
                 'data'=>[
                    'chartData'=>$chartData,
                   
                    'rows'=>$rows,
                    'rows2'=>$rows2,
                    'rows3'=>$rows3,
                    'rows4'=>$rows4,
                    'rows5'=>$rows5,
                    'top_filter'=>$top_filter,  
                 ]
                ];


    }
    public function getTopTwenty(Request $request){
        $dbTopTwenty    = PerformanceRating::getTopTwentyEarners();

        return ["data"=>$dbTopTwenty];
    }
   
    public function getAfricaBusinessReview(Request $request){
            $getAfricaBusinessReviewSeniorSalaries      = PerformanceRating::getAfricaBusinessReviewSeniorSalaries();
            $getAfricaBusinessReviewSeniorBonus         = PerformanceRating::getAfricaBusinessReviewSeniorBonus();
            $getAfricaBusinessReviewGeneralSalaries     = PerformanceRating::getAfricaBusinessReviewGeneralSalaries();
            $countries =  DB::table('country_pools')->get();

            $SeniorBonusData=[];
            foreach($getAfricaBusinessReviewSeniorBonus as $key => $value){
                foreach($countries as $countryKey => $countryValue){
                    if( $value->orglevel7 == $countryValue->name ){
                        $SeniorBonusData[]=[
                            'orglevel7'=>$value->orglevel7,
                            'total'=>$value->total,
                            'bonus'=>$value->bonus,
                            'headcount'=>$value->headcount,
                            'actual'=>$value->actual,
                            'bonus_pool_percentage'=>ROUND($countryValue->bonus_pool_percentage,1)
                        ];

                    }
                }
            }
            $SeniorManagementBonusData=[];
            foreach($getAfricaBusinessReviewGeneralSalaries as $key => $value){
                foreach($countries as $countryKey => $countryValue){
                    if( $value->orglevel7 == $countryValue->name ){
                        $SeniorManagementBonusData[]=[
                            'orglevel7'=>$value->orglevel7,
                            'total'=>$value->total,
                            'bonus'=>$value->bonus,//Actual Bonus Allocated	
                            'headcount'=>$value->headcount,
                            'actual'=>$value->actual,//Actual % allocated for FY2020
                            'bonus_pool_percentage'=>ROUND($countryValue->bonus_pool_percentage,1)//Budgeted Increase Allocation
                        ];

                    }
                }
            }
            $SeniorManSalaryData=[];
            foreach($getAfricaBusinessReviewSeniorSalaries as $key => $value){
                foreach($countries as $countryKey => $countryValue){
                    if( $value->orglevel7 == $countryValue->name ){
                        $SeniorManSalaryData[]=[
                            'orglevel7'=>$value->orglevel7,
                            'total'=>$value->total,
                            // 'bonus'=>$value->bonus,
                            'uplift'=>$value->uplift,
                            'headcount'=>$value->headcount,
                            'actual'=>$value->actual,
                            'bonus_pool_percentage'=>ROUND($countryValue->bonus_pool_percentage,1)
                        ];

                    }
                }
            }

                 return [

                    'senior_'=>  $getAfricaBusinessReviewSeniorSalaries,

                'countries'=>$countries,
                'seniorSal'=> $SeniorManSalaryData,

                'seniorBonus'=>$SeniorBonusData,

                'SeniorManagementBonus'=>$SeniorManagementBonusData
            ];

    }

    public function getBusinessReviewPerformanceTwoData(Request $request){
        
 

     try{
       // $active_instance                =   PermissionController::getUserInstance()['response']['instance'];
        // $userType                       =   PermissionController::getUserInstance()['response']['instance'];

            // $BandPercentagesDistribution    =   PerformanceRating:: getBusinessReviewtwoModelData_BandPercentage('band_id');
        // $BandCounts                     =   PerformanceRating:: getBusinessReviewtwoModelData_BandCount('band_id');
      
        // logger("MEHOD: " . __METHOD__ . " LINE:". __LINE__);


            //*******BAND Distribution chart and table data */
       // logger("MEHOD: " . __METHOD__ . " LINE:". __LINE__);

       

       


         

       


        //logger("MEHOD: " . __METHOD__ . " LINE:". __LINE__);






        $getAvgIncreaseBandVsRatingb1    =   PerformanceRating:: getAvgIncreaseBandVsRating(1);
        $getAvgIncreaseBandVsRatingb2    =   PerformanceRating:: getAvgIncreaseBandVsRating(2);
        $getAvgIncreaseBandVsRatingb3    =   PerformanceRating:: getAvgIncreaseBandVsRating(3);
        $getAvgIncreaseBandVsRatingb4    =   PerformanceRating:: getAvgIncreaseBandVsRating(4);
        $getAvgIncreaseBandVsRatingb5    =   PerformanceRating:: getAvgIncreaseBandVsRating(5);
        $b1Ratings=[
                'rating1_avgpercentage'=>0,
                'rating2_avgpercentage'=>0,
                'rating3_avgpercentage'=>0,
                'rating4_avgpercentage'=>0,
                'rating5_avgpercentage'=>0,
            ];
        $b2Ratings=[
                'rating1_avgpercentage'=>0,
                'rating2_avgpercentage'=>0,
                'rating3_avgpercentage'=>0,
                'rating5_avgpercentage'=>0,
            ];
        $b3Ratings=[
                'rating1_avgpercentage'=>0,
                'rating2_avgpercentage'=>0,
                'rating3_avgpercentage'=>0,
                'rating5_avgpercentage'=>0,
            ];

    

        $GeneralStaffBonusPerBands      =   PerformanceRating:: getGeneralStaffBandsBonus();
        //logger("MEHOD: " . __METHOD__ . " LINE:". __LINE__);



        $Band_AverageIncreases_headcounts           =   PerformanceRating:: getBusinessReviewtwoModelData_BandAverageIncrease('band_id');
        //logger("MEHOD: " . __METHOD__ . " LINE:". __LINE__);

        $getGeneralStaffTableData                   =   PerformanceRating:: getGeneralStaffTableData();
        $getSeniorManagementandSpecialistTableData  =   PerformanceRating:: getSeniorManagementandSpecialistTableData();

     


        $BandAvgIncrease_b1_ = [0,0,0,0];
        $BandAvgIncrease_b2_ = [0,0,0,0];
        $BandAvgIncrease_b3_ = [0,0,0,0];
        $BandAvgIncrease_b4_ = [0,0,0,0];
        $BandAvgIncrease_b5_ = [0,0,0,0];
        foreach($Band_AverageIncreases_headcounts as $keuy => $values){
            switch ($values->band_id) {
                case '1':
                    $BandAvgIncrease_b1_=[
                      

                        $values->percentage_average_NA,
                        $values->percentage_average_PA,
                        $values->percentage_average_FA,
                        $values->percentage_average_EA,
                        $values->percentage_average_TEA,
                    ];
                    break;
                case '2':
                    $BandAvgIncrease_b2_=[
                       

                        $values->percentage_average_NA,
                        $values->percentage_average_PA,
                        $values->percentage_average_FA,
                        $values->percentage_average_EA,
                        $values->percentage_average_TEA,
                    ];
                    break;
                case '3':
                    $BandAvgIncrease_b3_=[
                     

                        $values->percentage_average_NA,
                        $values->percentage_average_PA,
                        $values->percentage_average_FA,
                        $values->percentage_average_EA,
                        $values->percentage_average_TEA,
                    ];
                    break;
                case '4':
                    $BandAvgIncrease_b4_=[
                     

                        $values->percentage_average_NA,
                        $values->percentage_average_PA,
                        $values->percentage_average_FA,
                        $values->percentage_average_EA,
                        $values->percentage_average_TEA,
                    ];
                    break;
                case '5':
                    $BandAvgIncrease_b5_=[
                      
                        $values->percentage_average_NA,
                        $values->percentage_average_PA,
                        $values->percentage_average_FA,
                        $values->percentage_average_EA,
                        $values->percentage_average_TEA,
                    ];
                    break;
                
                default:
                    break;
            }

        }





        $kat = [
          

            // "overallPerformanceDistribution"=>$overallPerformanceDistribution,
           



                
                'BandAvgIncrease_b1_'                   =>$BandAvgIncrease_b1_ ,
                'BandAvgIncrease_b2_'                   =>$BandAvgIncrease_b2_ ,
                'BandAvgIncrease_b3_'                   =>$BandAvgIncrease_b3_ ,
                'BandAvgIncrease_b4_'                   =>$BandAvgIncrease_b4_ ,
                'BandAvgIncrease_b5_'                   =>$BandAvgIncrease_b5_ ,
                 


                // 'Band_AverageIncreases_headcounts'      =>$Band_AverageIncreases_headcounts,
               
        ];



        return $kat;
     }
     catch (\Exception $e) {
        $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
        Log::critical($msg);
        return array('error' => true, 'response' => [], 'message' => 'Could not get Audits');
    }
        
    }

    public function getAverageBandIncrease_businessReview(Request $request){

        try{
            //$AverageBandIncrease_businessReview     =   PerformanceRating::getAverageBandIncrease_businessReview();
            $AverageBandIncrease_B1B2   =   PerformanceRating::getAverageBandIncrease_businessReview2([1,2])[0];
            $AverageBandIncrease_B3     =   PerformanceRating::getAverageBandIncrease_businessReview2([3])[0];
            $AverageBandIncrease_B4B5   =   PerformanceRating::getAverageBandIncrease_businessReview2([4,5])[0];
            $grandTotal                 =   PerformanceRating::getAverageBandIncrease_businessReviewTotalAverage()[0];

            return [
                "AverageBandIncrease_businessReview" => array(
                    'AverageBandIncrease_B1B2'  => $AverageBandIncrease_B1B2,
                    'AverageBandIncrease_B3'    => $AverageBandIncrease_B3,
                    'AverageBandIncrease_B4B5'  => $AverageBandIncrease_B4B5,
                    'grandTotal'                => $grandTotal,
                ),
            ];

         } catch (\Exception $e) {
                $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
                Log::critical($msg);
                return array('error' => true, 'response' => [], 'message' => 'Controller error please debug!!');
            }



    }

    public function getoverallPerformanceDistribution(Request $request){
        
        
       //*******Overall Distribution chart and table data */
        try{
                   $overallPerformanceDistribution = PerformanceRating::getoverallPerformanceDistribution_BusinessReview();

                //    logger("METHOD :: ".__METHOD__." LINE:: ".__LINE__);

        $ratingArray['partially_achieved_count']        =0;
        $ratingArray_p['partially_achieved_percentage'] =0;
        $ratingArray['fully_achieved_count']            =0;
        $ratingArray['exceptionally_achieved_count']    =0;
        $ratingArray['tstr_count']                      =0;
        // $ratingArray['total_count']                     =[];
        //logger($overallPerformanceDistribution);

        foreach($overallPerformanceDistribution as $head_key=>$head_value){
            switch($head_value->current_rating_id){
                case 2:
                $ratingArray['partially_achieved_count'] = $head_value->partially_achieved_count;
                case 3:
                    $ratingArray['fully_achieved_count'] = $head_value->fully_achieved_count;
                case 4:
                    $ratingArray['exceptionally_achieved_count'] = $head_value->exceptionally_achieved_count;
                case 5:
                    $ratingArray['tstr_count'] = $head_value->tstr;
                default:
                    break;
            }
        

            
        }

        $ratingArray_p =  [
            'partially_achieved_percentage'       =>$ratingArray['partially_achieved_count'] == 0 ? 0 :(float)number_format((($ratingArray['partially_achieved_count']/array_sum( $ratingArray ))*100),2 ),
            'fully_achieved_percentage'           =>$ratingArray['fully_achieved_count'] == 0 ? 0: (float)number_format((($ratingArray['fully_achieved_count']/array_sum( $ratingArray ))*100),2 ),
            'exceptionally_achieved_percentage'   =>$ratingArray['exceptionally_achieved_count'] == 0 ? 0 : (float)number_format((($ratingArray['exceptionally_achieved_count']/array_sum( $ratingArray ))*100),2 ),
            'tstr_percentage'                     =>$ratingArray['tstr_count']== 0 ? 0:(float)number_format((($ratingArray['tstr_count']/array_sum( $ratingArray ))*100),2 )    
        ];
        $ratingArray['total_count'] = array_sum( $ratingArray );
        $overallPerformanceDistribution_Array= [$ratingArray_p,$ratingArray];
        return ["overallPerformanceDistribution"=>$overallPerformanceDistribution_Array] ;

            }catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Controller error please debug!!');
        }

   
    }

    public function getBandPercentagesDistribution(Request $request){

        $BandPercentagesDistribution        =   PerformanceRating:: getBusinessReviewtwoModelData_BandPercentage();
        $BandPercentagesDistributionB4B5    =   PerformanceRating:: getBusinessReviewtwoModelData_BandPercentageB3B4();


        $BandvsRatingDistribution_b1= ['band_exceptionally_achieved'=>0,
                                        'band_fully_achieved'=>0,
                                        'band_id'=>1,
                                        'band_not_achieved'=>0,
                                        'band_partially_achieved'=>0,
                                        'band_tea'=>0,
                                        'band_unknown'=>0,
                                        'total_band_head_count'=>0
                                    ];
        $BandvSratingDistribution_b2= ['band_exceptionally_achieved'=>0,
                                'band_fully_achieved'=>0,
                                'band_id'=>2,
                                'band_not_achieved'=>0,
                                'band_partially_achieved'=>0,
                                'band_tea'=>0,
                                'band_unknown'=>0,
                                'total_band_head_count'=>0
                            ];
        $BandvSratingDistribution_b3= ['band_exceptionally_achieved'=>0,
                                'band_fully_achieved'=>0,
                                'band_id'=>3,
                                'band_not_achieved'=>0,
                                'band_partially_achieved'=>0,
                                'band_tea'=>0,
                                'band_unknown'=>0,
                                'total_band_head_count'=>0
                            ];
        $BandvSratingDistribution_b4= ['band_exceptionally_achieved'=>0,
                                'band_fully_achieved'=>0,
                                'band_id'=>4,
                                'band_not_achieved'=>0,
                                'band_partially_achieved'=>0,
                                'band_tea'=>0,
                                'band_unknown'=>0,
                                'total_band_head_count'=>0
                            ];
        $BandvSratingDistribution_b5= ['band_exceptionally_achieved'=>0,
                                'band_fully_achieved'=>0,
                                'band_id'=>5,
                                'band_not_achieved'=>0,
                                'band_partially_achieved'=>0,
                                'band_tea'=>0,
                                'band_unknown'=>0,
                                'total_band_head_count'=>0
                            ];

        $BandvSratingDistribution_b3b4= ['band_exceptionally_achieved'=>0,
                                'band_fully_achieved'=>0,
                                'band_id'=>34,
                                'band_not_achieved'=>0,
                                'band_partially_achieved'=>0,
                                'band_tea'=>0,
                                'band_unknown'=>0,
                                'total_band_head_count'=>0
                            ];

        //*******bands head count average increase % Mendate */
        $BandvsRatingDistribution_b1_ = [0,0,0,0];
        $BandvSratingDistribution_b2_ = [0,0,0,0];
        $BandvSratingDistribution_b3_ = [0,0,0,0];
        $BandvSratingDistribution_b4_ = [0,0,0,0];
        $BandvSratingDistribution_b5_ = [0,0,0,0];


        if (count($BandPercentagesDistributionB4B5) > 0) {

            $BandvSratingDistribution_b3b4= [
                'band_exceptionally_achieved'=>$BandPercentagesDistributionB4B5[0]->band_exceptionally_achieved,
                'band_fully_achieved'       =>$BandPercentagesDistributionB4B5[0]->band_fully_achieved,
                'band_id'=>34,
                'band_not_achieved'         =>$BandPercentagesDistributionB4B5[0]->band_not_achieved,
                'band_partially_achieved'   =>$BandPercentagesDistributionB4B5[0]->band_partially_achieved,
                'band_tea'                  =>$BandPercentagesDistributionB4B5[0]->band_tea,
                'band_unknown'              =>$BandPercentagesDistributionB4B5[0]->band_partially_achieved,
                'total_band_head_count'     =>$BandPercentagesDistributionB4B5[0]->total_band_head_count,
            ];
        }



        foreach($BandPercentagesDistribution as $key=>$value ){

            if ($value->band_id == 1){
                $BandvsRatingDistribution_b1 = [
                    'band_exceptionally_achieved'=>$value->band_exceptionally_achieved,
                    'band_fully_achieved'=>$value->band_fully_achieved,
                    'band_id'=>$value->band_id,
                    'band_not_achieved'=>$value->band_not_achieved,
                    'band_partially_achieved'=>$value->band_partially_achieved,
                    'band_tea'=>$value->band_tea,
                    'band_unknown'=>$value->band_unknown,
                    'total_band_head_count'=>$value->total_band_head_count,
                ];
            }
            elseif ($value->band_id == 2){
                $BandvSratingDistribution_b2= ['band_exceptionally_achieved'=>$value->band_exceptionally_achieved,
                    'band_fully_achieved'=>$value->band_fully_achieved,
                    'band_id'=>$value->band_id,
                    'band_not_achieved'=>$value->band_not_achieved,
                    'band_partially_achieved'=>$value->band_partially_achieved,
                    'band_tea'=>$value->band_tea,
                    'band_unknown'=>$value->band_unknown,
                    'total_band_head_count'=>$value->total_band_head_count,
                ];
            }
            elseif ($value->band_id == 3){
                $BandvSratingDistribution_b3= ['band_exceptionally_achieved'=>$value->band_exceptionally_achieved,
                    'band_fully_achieved'=>$value->band_fully_achieved,
                    'band_id'=>$value->band_id,
                    'band_not_achieved'=>$value->band_not_achieved,
                    'band_partially_achieved'=>$value->band_partially_achieved,
                    'band_tea'=>$value->band_tea,
                    'band_unknown'=>$value->band_unknown,
                    'total_band_head_count'=>$value->total_band_head_count,
                ];
            }
            elseif ($value->band_id == 4){
                $BandvSratingDistribution_b4= ['band_exceptionally_achieved'=>$value->band_exceptionally_achieved,
                    'band_fully_achieved'=>$value->band_fully_achieved,
                    'band_id'=>$value->band_id,
                    'band_not_achieved'=>$value->band_not_achieved,
                    'band_partially_achieved'=>$value->band_partially_achieved,
                    'band_tea'=>$value->band_tea,
                    'band_unknown'=>$value->band_unknown,
                    'total_band_head_count'=>$value->total_band_head_count,
                ];
            }
            elseif ($value->band_id == 5){
                $BandvSratingDistribution_b5= ['band_exceptionally_achieved'=>$value->band_exceptionally_achieved,
                    'band_fully_achieved'=>$value->band_fully_achieved,
                    'band_id'=>$value->band_id,
                    'band_not_achieved'=>$value->band_not_achieved,
                    'band_partially_achieved'=>$value->band_partially_achieved,
                    'band_tea'=>$value->band_tea,
                    'band_unknown'=>$value->band_unknown,
                    'total_band_head_count'=>$value->total_band_head_count,
                ];
            }

        }

        foreach ($BandPercentagesDistribution as $BandPercentages_key => $BandPercentages_value) {
            switch ($BandPercentages_value->band_id) {
                case '1':
                    $BandvsRatingDistribution_b1_=[
                        $BandPercentages_value->band_exceptionally_achieved,
                        $BandPercentages_value->band_fully_achieved,
                        $BandPercentages_value->band_partially_achieved,
                        $BandPercentages_value->band_tea
                    ];
                 case '2':
                    $BandvSratingDistribution_b2_=[
                        $BandPercentages_value->band_exceptionally_achieved,
                        $BandPercentages_value->band_fully_achieved,
                        $BandPercentages_value->band_partially_achieved,
                        $BandPercentages_value->band_tea
                    ];
                 case '3':
                    $BandvSratingDistribution_b3_=[
                        $BandPercentages_value->band_exceptionally_achieved,
                        $BandPercentages_value->band_fully_achieved,
                        $BandPercentages_value->band_partially_achieved,
                        $BandPercentages_value->band_tea
                    ];
                 case '4':
                    $BandvSratingDistribution_b4_=[
                        $BandPercentages_value->band_exceptionally_achieved,
                        $BandPercentages_value->band_fully_achieved,
                        $BandPercentages_value->band_partially_achieved,
                        $BandPercentages_value->band_tea
                    ];
                case '5':
                    $BandvSratingDistribution_b5_=[
                        $BandPercentages_value->band_exceptionally_achieved,
                        $BandPercentages_value->band_fully_achieved,
                        $BandPercentages_value->band_partially_achieved,
                        $BandPercentages_value->band_tea
                    ];
                 default:
                    break;
            }
        }

        return[
            'BandPercentagesDistribution'          =>$BandPercentagesDistribution,
            'BandvSratingDistribution_b1'          =>$BandvsRatingDistribution_b1,
            'BandvSratingDistribution_b2'          =>$BandvSratingDistribution_b2 ,
            'BandvSratingDistribution_b3'          =>$BandvSratingDistribution_b3,
            'BandvSratingDistribution_b4'          =>$BandvSratingDistribution_b4 ,
            'BandvSratingDistribution_b5'          =>$BandvSratingDistribution_b5 ,
            'BandvSratingDistribution_b3b4'        =>$BandvSratingDistribution_b3b4,
        ];


    }

    public function getsm_sp_kpipool(Request $request){
        $sm_sp = [];
        $SeniorManBonus_review    = PerformanceRating::SeniorManBonus_review();
        $SPBonus_review           = PerformanceRating::SPBonus_review();
        //$prevYearData             = PerformanceRating::calcSMSpecialist();


        $old_data = [
            'head_count_sp' => 0,
            'percentage_sp' => 0,
            'head_count_sm' => 0,
            'percentage_sm' => 0,
        ];
        $h_total_head_count = 0;

        $sm_sp = [
            'h_total_head_count'                => $h_total_head_count,
            'total_head_count_sp'               => count($SPBonus_review) > 0?  round($SPBonus_review[0]->total_head_count,0) : 0,
            'bonus_sp'                          => count($SPBonus_review) > 0?  round($SPBonus_review[0]->bonus_sp,0) : 0,
            'total_actual_pool_percentage_sp'   => count($SPBonus_review) > 0?  round(($SPBonus_review[0]->total_actual_pool_percentage_sp),1) : 0,
            'total_head_count_sm'               => count($SeniorManBonus_review) > 0?  round($SeniorManBonus_review[0]->total_head_count,0) : 0,
            'bonus_sm'                          => count($SeniorManBonus_review) > 0?  round($SeniorManBonus_review[0]->bonus_sm,1) : 0,
            'total_actual_pool_percentage_sm'   => count($SeniorManBonus_review) > 0?  round($SeniorManBonus_review[0]->total_actual_pool_percentage_sm,1) : 0
        ];
        //$sm_sp = array_merge($sm_sp, $old_data);

        return ['sm_sp_kpipool'=> $sm_sp];
    }

    public function getgeneralStaffBandsvsBonus(Request $request){

        /***************General staff Scheme Bonus by Band */
        //$active_instance            = PermissionController::getUserInstance()['response']['instance'];
        //$generalStaffBandsvsBonus   =   PerformanceRating:: generalStaffBandsvsBonus();

        $generalStaffBandsvsBonusB1B2_   =   PerformanceRating:: generalStaffBandsvsBonus2([1,2]);
        $generalStaffBandsvsBonusB3_     =   PerformanceRating:: generalStaffBandsvsBonus2([3]);
        $generalStaffBandsvsBonusB4B4_   =   PerformanceRating:: generalStaffBandsvsBonus2([4,5]);
        $generalStaffBandsvsBonusGT_     =   PerformanceRating:: generalStaffBandsvsBonus2GranTotal();

       // dd($generalStaffBandsvsBonusGT_);

        /**
         *
         * bonus_percentage
        bonus_value
        total_head_count
         */
        $generalStaffBandsvsBonusB1B2   =  new stdClass();
        $generalStaffBandsvsBonusB3     =  new stdClass();
        $generalStaffBandsvsBonusB4B4   =  new stdClass();
        $generalStaffBandsvsBonusGT     =  new stdClass(); // grand totals


        if (count($generalStaffBandsvsBonusB1B2_) > 0 ){
            $generalStaffBandsvsBonusB1B2 = $generalStaffBandsvsBonusB1B2_[0];
            $generalStaffBandsvsBonusB1B2->bonus_value = number_format($generalStaffBandsvsBonusB1B2->bonus_value, 0, '.', ' ');
        }
        else{
            $generalStaffBandsvsBonusB1B2->bonus_percentage = 0.0;
            $generalStaffBandsvsBonusB1B2->bonus_value      = 0.0;
            $generalStaffBandsvsBonusB1B2->total_head_count = 0.0;
        }

        if (count($generalStaffBandsvsBonusB3_) > 0 ){
            $generalStaffBandsvsBonusB3 = $generalStaffBandsvsBonusB3_[0];
            $generalStaffBandsvsBonusB3->bonus_value = number_format($generalStaffBandsvsBonusB3->bonus_value, 0, '.', ' ');
        }
        else{
            $generalStaffBandsvsBonusB3->bonus_percentage = 0.0;
            $generalStaffBandsvsBonusB3->bonus_value = 0.0;
            $generalStaffBandsvsBonusB3->total_head_count = 0.0;
        }

        if (count($generalStaffBandsvsBonusB4B4_) > 0 ){
            $generalStaffBandsvsBonusB4B4 = $generalStaffBandsvsBonusB4B4_[0];
            $generalStaffBandsvsBonusB4B4->bonus_value = number_format($generalStaffBandsvsBonusB4B4->bonus_value, 0, '.', ' ');
        }
        else{
            $generalStaffBandsvsBonusB4B4->bonus_percentage = 0.0;
            $generalStaffBandsvsBonusB4B4->bonus_value = 0.0;
            $generalStaffBandsvsBonusB4B4->total_head_count = 0.0;
        }

        if (count($generalStaffBandsvsBonusGT_) > 0 ){
            $generalStaffBandsvsBonusGT = $generalStaffBandsvsBonusGT_[0];
            $generalStaffBandsvsBonusGT->bonus_value = number_format($generalStaffBandsvsBonusGT->bonus_value, 0, '.', ' ');
        }
        else{
            $generalStaffBandsvsBonusGT->bonus_percentage = 0.0;
            $generalStaffBandsvsBonusGT->bonus_value = 0.0;
            $generalStaffBandsvsBonusGT->total_head_count = 0.0;
        }

        $generalStaffBandsvsBonus2 =  [
            "generalStaffBandsvsBonusB4B5"  => $generalStaffBandsvsBonusB4B4,
            "generalStaffBandsvsBonusB3"    => $generalStaffBandsvsBonusB3,
            "generalStaffBandsvsBonusB1B2"  => $generalStaffBandsvsBonusB1B2,
            "generalStaffBandsvsBonusGT"    => $generalStaffBandsvsBonusGT
        ];

        return[
            "generalStaffBandsvsBonus2"=>$generalStaffBandsvsBonus2];

    }

    public function getAveragePercentSalaryBandVsPerformance(Request $request){
        $getAveragePercentSalaryBandVsPerformanceB5B4   =   PerformanceRating:: getAveragePercentSalaryBandVsPerformanceB5B4();
        $getAveragePercentSalaryBandVsPerformanceB2     =   PerformanceRating:: getAveragePercentSalaryBandVsPerformanceB2();
        $getAveragePercentSalaryBandVsPerformanceB3     =   PerformanceRating:: getAveragePercentSalaryBandVsPerformanceB3();
        //dd($getAveragePercentSalaryBandVsPerformanceB2);
 
        $AveragePercentSalaryBandVsPerformanceB2_PA = 0;
        $AveragePercentSalaryBandVsPerformanceB2_FA = 0;
        $AveragePercentSalaryBandVsPerformanceB2_EA = 0;
        $AveragePercentSalaryBandVsPerformanceB2_TEA = 0;
 
        $AveragePercentSalaryBandVsPerformanceB3_PA = 0;
        $AveragePercentSalaryBandVsPerformanceB3_FA = 0;
        $AveragePercentSalaryBandVsPerformanceB3_EA = 0;
        $AveragePercentSalaryBandVsPerformanceB3_TEA = 0;
 
        $AveragePercentSalaryBandVsPerformanceB4B5_PA = 0;
        $AveragePercentSalaryBandVsPerformanceB4B5_FA = 0;
        $AveragePercentSalaryBandVsPerformanceB4B5_EA = 0;
        $AveragePercentSalaryBandVsPerformanceB4B5_TEA = 0;



        foreach($getAveragePercentSalaryBandVsPerformanceB2 as $key=>$value ){
            switch ($value->current_rating_id) {
                case 2:
                    $AveragePercentSalaryBandVsPerformanceB2_PA = $value->averageBandSalary;
                    break;
                case 3:
                    $AveragePercentSalaryBandVsPerformanceB2_FA = $value->averageBandSalary;
                    break;
                case 4:
                    $AveragePercentSalaryBandVsPerformanceB2_EA = $value->averageBandSalary;
                    break;
                case 5:
                    $AveragePercentSalaryBandVsPerformanceB2_TEA = $value->averageBandSalary;
                    break;
                default:
                    break;
            }
        }

        foreach($getAveragePercentSalaryBandVsPerformanceB5B4 as $key=>$value ){
            switch ($value->current_rating_id) {
                case 2:
                    $AveragePercentSalaryBandVsPerformanceB4B5_PA = $value->averageBandSalary;
                    break;
                case 3:
                    $AveragePercentSalaryBandVsPerformanceB4B5_FA = $value->averageBandSalary;
                    break;
                case 4:
                    $AveragePercentSalaryBandVsPerformanceB4B5_EA = $value->averageBandSalary;
                    break;
                case 5:
                    $AveragePercentSalaryBandVsPerformanceB4B5_TEA = $value->averageBandSalary;
                    break;
                default:
                    break;
            }
        }
        foreach($getAveragePercentSalaryBandVsPerformanceB3 as $key=>$value ){
            switch ($value->current_rating_id) {
                case 2:
                    $AveragePercentSalaryBandVsPerformanceB3_PA = $value->averageBandSalary;
                    break;
                case 3:
                    $AveragePercentSalaryBandVsPerformanceB3_FA = $value->averageBandSalary;
                    break;
                case 4:
                    $AveragePercentSalaryBandVsPerformanceB3_EA = $value->averageBandSalary;
                    break;
                case 5:
                    $AveragePercentSalaryBandVsPerformanceB3_TEA = $value->averageBandSalary;
                    break;
                default:
                    break;
            }
        }

        $AveragePercentSalaryBandVsPerformanceB2_Array = [
            $AveragePercentSalaryBandVsPerformanceB2_PA,
            $AveragePercentSalaryBandVsPerformanceB2_FA,
            $AveragePercentSalaryBandVsPerformanceB2_EA,
            $AveragePercentSalaryBandVsPerformanceB2_TEA,
        ];
        $AveragePercentSalaryBandVsPerformanceB3_Array = [
            $AveragePercentSalaryBandVsPerformanceB3_PA,
            $AveragePercentSalaryBandVsPerformanceB3_FA,
            $AveragePercentSalaryBandVsPerformanceB3_EA,
            $AveragePercentSalaryBandVsPerformanceB3_TEA,
        ];
        $AveragePercentSalaryBandVsPerformanceB4B5_Array = [
            $AveragePercentSalaryBandVsPerformanceB4B5_PA,
            $AveragePercentSalaryBandVsPerformanceB4B5_FA,
            $AveragePercentSalaryBandVsPerformanceB4B5_EA,
            $AveragePercentSalaryBandVsPerformanceB4B5_TEA,
        ];



        return [

            "getAveragePercentSalaryBandVsPerformanceB2"=>$AveragePercentSalaryBandVsPerformanceB2_Array,
            "getAveragePercentSalaryBandVsPerformanceB3"=>$AveragePercentSalaryBandVsPerformanceB3_Array,
            "getAveragePercentSalaryBandVsPerformanceB5B4"=>$AveragePercentSalaryBandVsPerformanceB4B5_Array,
        ];

    }

    public function getAverageIncentiveBandVsPerformance(Request $request){

            //*******Average incentive % by band and current rating */
       

            $getAverageIncentiveBandVsPerformanceB5B4       =   PerformanceRating:: getAverageSalaryBandVsPerformanceB5B4();
            $getAverageIncentiveBandVsPerformanceB2         =   PerformanceRating:: getAverageSalaryBandVsPerformanceB2();
            $getAverageIncentiveBandVsPerformanceB3         =   PerformanceRating:: getAverageSalaryBandVsPerformanceB3();
      





            $AveragePercentBonusBandVsPerformanceB2_PA = 0;
            $AveragePercentBonusBandVsPerformanceB2_FA = 0;
            $AveragePercentBonusBandVsPerformanceB2_EA = 0;
            $AveragePercentBonusBandVsPerformanceB2_TEA = 0;
     
            $AveragePercentBonusBandVsPerformanceB3_PA = 0;
            $AveragePercentBonusBandVsPerformanceB3_FA = 0;
            $AveragePercentBonusBandVsPerformanceB3_EA = 0;
            $AveragePercentBonusBandVsPerformanceB3_TEA = 0;
     
            $AveragePercentBonusBandVsPerformanceB4B5_PA = 0;
            $AveragePercentBonusBandVsPerformanceB4B5_FA = 0;
            $AveragePercentBonusBandVsPerformanceB4B5_EA = 0;
            $AveragePercentBonusBandVsPerformanceB4B5_TEA = 0;
    
    
    
            foreach($getAverageIncentiveBandVsPerformanceB2 as $key=>$value ){
                switch ($value->current_rating_id) {
                    case 2:
                        $AveragePercentBonusBandVsPerformanceB2_PA = $value->averageBandSalary;
                        break;
                    case 3:
                        $AveragePercentBonusBandVsPerformanceB2_FA = $value->averageBandSalary;
                        break;
                    case 4:
                        $AveragePercentBonusBandVsPerformanceB2_EA = $value->averageBandSalary;
                        break;
                    case 5:
                        $AveragePercentBonusBandVsPerformanceB2_TEA = $value->averageBandSalary;
                        break;
                    default:
                        break;
                }
            }
    
            foreach($getAverageIncentiveBandVsPerformanceB5B4 as $key=>$value ){
                switch ($value->current_rating_id) {
                    case 2:
                        $AveragePercentBonusBandVsPerformanceB4B5_PA = $value->averageBandSalary;
                        break;
                    case 3:
                        $AveragePercentBonusBandVsPerformanceB4B5_FA = $value->averageBandSalary;
                        break;
                    case 4:
                        $AveragePercentBonusBandVsPerformanceB4B5_EA = $value->averageBandSalary;
                        break;
                    case 5:
                        $AveragePercentBonusBandVsPerformanceB4B5_TEA = $value->averageBandSalary;
                        break;
                    default:
                        break;
                }
            }
            foreach($getAverageIncentiveBandVsPerformanceB3 as $key=>$value ){
                switch ($value->current_rating_id) {
                    case 2:
                        $AveragePercentBonusBandVsPerformanceB3_PA = $value->averageBandSalary;
                        break;
                    case 3:
                        $AveragePercentBonusBandVsPerformanceB3_FA = $value->averageBandSalary;
                        break;
                    case 4:
                        $AveragePercentBonusBandVsPerformanceB3_EA = $value->averageBandSalary;
                        break;
                    case 5:
                        $AveragePercentBonusBandVsPerformanceB3_TEA = $value->averageBandSalary;
                        break;
                    default:
                        break;
                }
            }
    
            $AveragePercentBonusBandVsPerformanceB2_Array = [
                $AveragePercentBonusBandVsPerformanceB2_PA,
                $AveragePercentBonusBandVsPerformanceB2_FA,
                $AveragePercentBonusBandVsPerformanceB2_EA,
                $AveragePercentBonusBandVsPerformanceB2_TEA,
            ];
            $AveragePercentBonusBandVsPerformanceB3_Array = [
                $AveragePercentBonusBandVsPerformanceB3_PA,
                $AveragePercentBonusBandVsPerformanceB3_FA,
                $AveragePercentBonusBandVsPerformanceB3_EA,
                $AveragePercentBonusBandVsPerformanceB3_TEA,
            ];
            $AveragePercentBonusBandVsPerformanceB4B5_Array = [
                $AveragePercentBonusBandVsPerformanceB4B5_PA,
                $AveragePercentBonusBandVsPerformanceB4B5_FA,
                $AveragePercentBonusBandVsPerformanceB4B5_EA,
                $AveragePercentBonusBandVsPerformanceB4B5_TEA,
            ];

            return [
                'getAverageIncentiveBandVsPerformanceB5B4'=>$AveragePercentBonusBandVsPerformanceB4B5_Array,
                'getAverageIncentiveBandVsPerformanceB2'=>$AveragePercentBonusBandVsPerformanceB2_Array,
                'getAverageIncentiveBandVsPerformanceB3'=>$AveragePercentBonusBandVsPerformanceB3_Array
            ];






    }

    /**
     * Earnings Distribution: All Staff Incentives
     * @param Request $request
     * @return string[]
     */
    public function getAllStaffIncentive(Request $request){
        $calc_orglevel5                 = EarningsDistribution::getAllStaffIncentiveByBA();
        $current_ratings                 = CurrentRating::all()->pluck('name', 'id')->toArray();
        /*$calc_rr                = EarningsDistribution::getAllStaffIncentiveByBABandCurrentRating();
        $business_area_bands    = EarningsDistribution::getAllStaffIncentiveByBABand();*/


        $business_area_B1B2s            = EarningsDistribution::getAllStaffIncentiveByBABands( [1, 2]);
        $business_area_rating_B1B2      = EarningsDistribution::getAllStaffIncentiveByBABandsRatings([1, 2]);

        $business_area_B3               = EarningsDistribution::getAllStaffIncentiveByBABands([3]);
        $business_area_rating_B3        = EarningsDistribution::getAllStaffIncentiveByBABandsRatings([3]);

        $business_area_B4B5s            = EarningsDistribution::getAllStaffIncentiveByBABands([4,5]);
        $business_area_rating_B4B5      = EarningsDistribution::getAllStaffIncentiveByBABandsRatings([4, 5]);


        $table          =   '<table class="table table-responsive table-hover table-bordered" style="display:table; outline-style: solid; outline-width: 1px;" >';
        $table_headers  =   "<thead>
                                <tr>
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Row Labels</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Headcount FY2020</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Sum of Current Annual Salary (ZAR)</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Sum of New Annual Salary (ZAR)</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Sum of Total Bonus Value </td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Average of Proposed Salary Increase %</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Average increase</td>                             
                                </tr>
                             </thead>
                             ";
        $table_body     =   "<tbody>";
        $rows           =   "";

        foreach ($calc_orglevel5 as $item){
            $rows           .=   " <tr>
                                    <td style='color: #00164E; font-weight: bold;'>$item->orglevel5</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'>$item->total_head_count</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($item->total_salary_value_zar, 0, '.', ' ') . "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($item->total_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($item->total_proposedincentive_zar, 0, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($item->total_avg_proposedsalary_percentage, 1, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($item->total_avg_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                </tr>";

            ///////////////////////////////////////////////////////   B1 & B2  ////////////////////////////////////////////////////////////////////////////
            /***  Second Level  B1 & B2 ***/

            if ($business_area_B1B2s){
                foreach ($business_area_B1B2s as $business_area_B1B2){

                    if ($business_area_B1B2->orglevel5 == $item->orglevel5){
                        $business_area_B1B2_ = $business_area_B1B2;
                        $rows           .=   " <tr>
                                    <td style='color: #00164E; font-weight: bold;'>&emsp;&emsp;B1 & B2</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'>$business_area_B1B2_->total_head_count</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B1B2_->total_salary_value_zar, 0, '.', ' ') . "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B1B2_->total_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B1B2_->total_proposedincentive_zar, 0, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B1B2_->total_avg_proposedsalary_percentage, 1, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B1B2_->total_avg_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                </tr>";
                    }

                }
                /***  Third Level  B1 & B2 ***/

                foreach ($business_area_rating_B1B2 as $rating_B1B2){
                    if ($rating_B1B2->orglevel5 == $item->orglevel5){
                        $current_rating_id =  $current_ratings[$rating_B1B2->current_rating_id];
                        $rows           .=   " <tr>
                                            <td style='color: #00164E; '>&emsp;&emsp;&emsp;&emsp;$current_rating_id</td>                             
                                            <td class='text-center'  style='color: #00164E;'>$rating_B1B2->total_head_count</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B1B2->total_salary_value_zar, 0, '.', ' ') . "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B1B2->total_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B1B2->total_proposedincentive_zar, 0, '.', ' '). "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B1B2->total_avg_proposedsalary_percentage, 1, '.', ' '). "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B1B2->total_avg_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                        </tr>";
                    }
                }
            }



            //////////////////////////////////////////////////////////  B3   ///////////////////////////////////////////////////////////////////////////
            /***  Second Level  B3 ***/

            if ($business_area_B3){
                foreach ($business_area_B3 as $business_area_B3z){
                    if ($business_area_B3z->orglevel5 == $item->orglevel5){
                        $business_area_B3z_ = $business_area_B3z;
                        $rows           .=   " <tr>
                                    <td style='color: #00164E; font-weight: bold;'>&emsp;&emsp;B3</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'>$business_area_B3z_->total_head_count</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B3z_->total_salary_value_zar, 0, '.', ' ') . "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B3z_->total_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B3z_->total_proposedincentive_zar, 0, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B3z_->total_avg_proposedsalary_percentage, 1, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B3z_->total_avg_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                </tr>";
                    }
                }
                /***  Third Level  B3 ***/
                foreach ($business_area_rating_B3 as $rating_B3){
                    if ($rating_B3->orglevel5 == $item->orglevel5){
                        $current_rating_id =  $current_ratings[$rating_B3->current_rating_id];
                        $rows           .=   " <tr>
                                            <td style='color: #00164E;'>&emsp;&emsp;&emsp;&emsp;$current_rating_id</td>                             
                                            <td class='text-center'  style='color: #00164E;'>$rating_B3->total_head_count</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B3->total_salary_value_zar, 0, '.', ' ') . "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B3->total_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B3->total_proposedincentive_zar, 0, '.', ' '). "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B3->total_avg_proposedsalary_percentage, 1, '.', ' '). "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B3->total_avg_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                        </tr>";
                    }
                }
            }


            /////////////////////////////////////////////////////// B4 & B5 //////////////////////////////////////////////////////////////////////////////

            /***  2nd Level  B4 & B5 ***/

            if ($business_area_B4B5s){
                foreach ($business_area_B4B5s as $business_area_B4B5){
                    if ($business_area_B4B5->orglevel5 == $item->orglevel5){
                        $business_area_B4B5_ = $business_area_B4B5;
                        $rows           .=   " <tr>
                                    <td style='color: #00164E; font-weight: bold;'>&emsp;&emsp;B4 & B5</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'>$business_area_B4B5_->total_head_count</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B4B5_->total_salary_value_zar, 0, '.', ' ') . "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B4B5_->total_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B4B5_->total_proposedincentive_zar, 0, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B4B5_->total_avg_proposedsalary_percentage, 1, '.', ' '). "</td>                             
                                    <td class='text-center'  style='color: #00164E; font-weight: bold;'> ". number_format($business_area_B4B5_->total_avg_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                </tr>";
                    }
                }

                /***  Third Level  B4 & B5 ***/
                foreach ($business_area_rating_B4B5 as $rating_B4B5){
                    if ($rating_B4B5->orglevel5 == $item->orglevel5){
                        $current_rating_id =  $current_ratings[$rating_B4B5->current_rating_id];
                        $rows           .=   " <tr>
                                            <td style='color: #00164E;'>&emsp;&emsp;&emsp;&emsp;$current_rating_id</td>                             
                                            <td class='text-center'  style='color: #00164E;'>$rating_B4B5->total_head_count</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B4B5->total_salary_value_zar, 0, '.', ' ') . "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B4B5->total_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B4B5->total_proposedincentive_zar, 0, '.', ' '). "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B4B5->total_avg_proposedsalary_percentage, 1, '.', ' '). "</td>                             
                                            <td class='text-center'  style='color: #00164E;'> ". number_format($rating_B4B5->total_avg_proposedsalary_zar, 0, '.', ' '). "</td>                             
                                        </tr>";
                    }
                }
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }



        $table.=$table_headers;
        $table.=$table_body;
        $table.=$rows;

        $table.="</tbody></table>";
        return ['getAllStaffIncentive' => $table];
    }

    /**
     * Earnings Distribution: SM, RISK & Specialist Incentives
     * @param Request $request
     * @return string[]
     */
    public function getIncentiveByBusinessArea(Request $request){
        $incentive_orglevel5    = EarningsDistribution::getIncentiveByBusinessArea();
        $grandTotals            = EarningsDistribution::getIncentiveByBusinessAreaGrandTotal();



        $table          =   '<table class="table table-responsive table-hover table-bordered" style="display:table; outline-style: solid; outline-width: 1px;" >';
        $table_headers  =   "<thead>
                                <tr>
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Business Area</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Headcount FY2020</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Sum Non-Financial (Discretionary/KPI) Allocation value</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Sum of Financial Value</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Sum of Total Bonus Value FY2019</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'> % of Discretionary pool allocated </td>                             
                                </tr>
                             </thead>
                             ";
        $table_body     =   "<tbody>";
        $rows           =   "";


        foreach($incentive_orglevel5 as $item){
            $calc_kpi_incentive_value   = $item->calc_kpi_incentive_value !== null ? $item->calc_kpi_incentive_value : 0;
            $financial_incentive_value  = $item->financial_incentive_value !== null ? $item->financial_incentive_value : 0;
            $total_incentive_value      = $calc_kpi_incentive_value + $financial_incentive_value;

            $rows .= "<tr>
                        <td style='color: #00164E; font-weight: bold;'>" . $item->orglevel5 ." </td>
                        <td class='text-center'  style='color: #00164E;'>" . $item->total_band_head_count ." </td>
                        <td class='text-center'  style='color: #00164E;'>" . number_format($item->calc_kpi_incentive_value, 0, '.', ' ') ." </td>
                        <td class='text-center'  style='color: #00164E;'>" . number_format($item->financial_incentive_value, 0, '.', ' ')."</td>
                        <td class='text-center'  style='color: #00164E;'>" . number_format($total_incentive_value, 0, '.', ' ')."</td>
                        <td class='text-center'  style='color: #00164E;'>" . $item->allocated_percentage ."</td>
                     </tr>";

        }

        if (count($grandTotals) > 0){
            $grandTotal                 = $grandTotals[0];
            $calc_kpi_incentive_value   = $grandTotal->calc_kpi_incentive_value !== null ? $grandTotal->calc_kpi_incentive_value : 0;
            $financial_incentive_value  = $grandTotal->financial_incentive_value !== null ? $grandTotal->financial_incentive_value : 0;
            $total_incentive_value      = $calc_kpi_incentive_value + $financial_incentive_value;

            $rows .= "<tr>
                        <td class=' performance_dash_header text-center' style='font-weight: bold;'> Grand Total </td>
                        <td class=' performance_dash_header text-center' style='font-weight: bold;'>". $grandTotal->total_band_head_count ." </td>
                        <td class=' performance_dash_header text-center' style='font-weight: bold;'>" . number_format($grandTotal->calc_kpi_incentive_value, 0, '.', ' ') ." </td>
                        <td class=' performance_dash_header text-center' style='font-weight: bold;'>" . number_format($grandTotal->financial_incentive_value, 0, '.', ' ')."</td>
                        <td class=' performance_dash_header text-center' style='font-weight: bold;'>" . number_format($total_incentive_value, 0, '.', ' ')."</td>
                        <td class=' performance_dash_header text-center' style='font-weight: bold;'>" .$grandTotal->allocated_percentage ."</td>
                     </tr>";
        }

        $table.=$table_headers;
        $table.=$table_body;
        $table.=$rows;

        $table.="</tbody></table>";

        return ['getIncentiveByBusinessArea' => $table];

    }

    /**
     * Earnings Distribution: Incentives - Band & Rating Tab
     * @param Request $request
     * @return string[]
     */
    public function getIncentiveByBandRating (Request $request){
        $incentive_band_rating_totals   = EarningsDistribution::getIncentiveByBandRatingTotal();
        $incentive_band_rating          = EarningsDistribution::getIncentiveByBandRating();
        $grandTotals                    = EarningsDistribution::getIncentiveByBandRatingGrndTotal();
        $bands                          = Band::all()->pluck('name', 'id')->toArray();
        $current_ratings                 = CurrentRating::all()->pluck('name', 'id')->toArray();

        $table          =   '<table class="table table-responsive table-hover table-bordered" style="display:table; outline-style: solid; outline-width: 1px;" >';
        $table_headers  =   "<thead>
                                <tr>
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Salary band & performance rating</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Headcount FY2020</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Sum of New Annual Salary (ZAR)</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Sum of Revised Pro-rated Incentive Value</td>                             
                                    <td class=' performance_dash_header' style='font-weight: bold;'>Average Bonus as % of salary bill</td>                             
                                </tr>
                             </thead>
                             ";
        $table_body     =   "<tbody>";
        $rows           =   "";

        foreach ($incentive_band_rating_totals as $top_record){
            $band = $bands[$top_record->band_id];
            $total_proposedsalary_zar = number_format($top_record->total_proposedsalary_zar, 0, '.', ' ');
            $total_revised_pro_rated_incentive_value = number_format($top_record->total_revised_pro_rated_incentive_value, 0, '.', ' ');
            $rows .= "<tr>
                        <td style='color: #00164E; font-weight: bold;'>" . $bands[$top_record->band_id]." </td>
                        <td class='text-center'  style='color: #00164E; font-weight: bold;'>". $top_record->total_band_head_count ." </td>
                        <td class='text-center'  style='color: #00164E; font-weight: bold;'>" . number_format($top_record->total_proposedsalary_zar, 0, '.', ' ') ." </td>
                        <td class='text-center'  style='color: #00164E; font-weight: bold;'>" . number_format($top_record->total_revised_pro_rated_incentive_value, 0, '.', ' ')."</td>
                        <td class='text-center'  style='color: #00164E; font-weight: bold;'>" .$top_record->total_avg_proposedsalary_percentage ."</td>
                     </tr>";

            foreach ($incentive_band_rating as $item){
                if ($top_record->band_id == $item->band_id){

                    $rows .= "<tr>
                        <td class='text-center'  style='color: #00164E;'>     &emsp;&emsp;" . $current_ratings[$item->current_rating_id]." </td>
                        <td class='text-center'  style='color: #00164E;'>     ". $item->total_band_head_count ." </td>
                        <td class='text-center'  style='color: #00164E;'>     " . number_format($item->total_proposedsalary_zar, 0, '.', ' ') ." </td>
                        <td class='text-center'  style='color: #00164E;'>     " . number_format($item->total_revised_pro_rated_incentive_value, 0, '.', ' ')."</td>
                        <td class='text-center'  style='color: #00164E;'>     " .$item->total_avg_proposedsalary_percentage ."</td>
                     </tr>";
                }
            }
        }

        if (count($grandTotals) > 0){
            $rows .= "<tr>
                        <td class=' performance_dash_header' style='font-weight: bold;'> Grand Total </td>
                        <td class='text-center performance_dash_header' style='font-weight: bold;'>". $grandTotals[0]->total_band_head_count ." </td>
                        <td class='text-center performance_dash_header' style='font-weight: bold;'>" . number_format($grandTotals[0]->total_proposedsalary_zar, 0, '.', ' ') ." </td>
                        <td class='text-center performance_dash_header' style='font-weight: bold;'>" . number_format($grandTotals[0]->total_revised_pro_rated_incentive_value, 0, '.', ' ')."</td>
                        <td class='text-center performance_dash_header' style='font-weight: bold;'>" .$grandTotals[0]->total_avg_proposedsalary_percentage ."</td>
                     </tr>";
        }

        $table.=$table_headers;
        $table.=$table_body;
        $table.=$rows;

        $table.="</tbody></table>";

        return ['getIncentiveByBandRating' => $table];
    }


    public function getExportAllStaffIncentives(Request $request){
        try{

            $export         = EarningsDistribution::buildExportAllStaffIncentives();
            $file_name      = "/public/spreadsheet/Exports/EarningsDistribution/AllStaffIncentives" . '_export_'.date('YmdHis').'.xlsx';
            Excel::store($export, $file_name);
            $response       = array('export' => $export, 'file_path' => base64_encode($file_name));
            return array('error' => false, 'response' => $response, 'message' => "Successfully stored export file");
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public function getExportSMRiskSpecialistIncentive(Request $request){
        try{

            $export         = EarningsDistribution::buildExportSMRiskSpecialistIncentive();
            $file_name      = "/public/spreadsheet/Exports/EarningsDistribution/SMRiskSpecialistIncentive" . '_export_'.date('YmdHis').'.xlsx';
            Excel::store($export, $file_name);
            $response       = array('export' => $export, 'file_path' => base64_encode($file_name));
            return array('error' => false, 'response' => $response, 'message' => "Successfully stored export file");
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public function getExportBandRatings(Request $request){
        try{

            $export         = EarningsDistribution::buildExportBandRatings();
            $file_name      = "/public/spreadsheet/Exports/EarningsDistribution/EarningsDistributionBandRatings" . '_export_'.date('YmdHis').'.xlsx';
            Excel::store($export, $file_name);
            $response       = array('export' => $export, 'file_path' => base64_encode($file_name));
            return array('error' => false, 'response' => $response, 'message' => "Successfully stored export file");
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public function getExportTop20Earners(Request $request){
        try{

            $export         = EarningsDistribution::buildExportTop20Earners();
            $file_name      = "/public/spreadsheet/Exports/EarningsDistribution/Top20Earners" . '_export_'.date('YmdHis').'.xlsx';
            Excel::store($export, $file_name);
            $response       = array('export' => $export, 'file_path' => base64_encode($file_name));
            return array('error' => false, 'response' => $response, 'message' => "Successfully stored export file");
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

}
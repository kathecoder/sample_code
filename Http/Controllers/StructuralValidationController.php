<?php

namespace App\Http\Controllers;

use App\Models\AppExport;
use App\Models\Employee;
use App\Models\EmployeeStructuralValidation;
use App\Models\FranchiseMapping;
use App\Models\ManagerType;
use App\CFLibrary\FilterHandler;
use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use App\Models\EmployeeAssessor;
use App\Models\Audit;
use App\Models\Phase;
use App\User;
use App\Models\Cycle;

class StructuralValidationController extends Controller
{
    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }

    public function create(Request $request)
    {

        return true;
    }

    public function getCardPanel(Request $request){

       ini_set('max_execution_time', 21100);
       ini_set('memory_limit', '-1');
        $data = [];
        $mode = 'read';
        $id = '';

        $card_template = $this->getPhaseTabsPanels($request);

        if($mode == "new"){
            $id = 0;
        }

        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }

    public function getCardSinglePanel(Request $request){

        ini_set('max_execution_time', 21100);
        ini_set('memory_limit', '-1');
        $data = [];
        $mode = 'read';
        $id = '';

        $res = $this->getPhasePanel($request);

        if($mode == "new"){
            $id = 0;
        }
        if (isset($res['export'])){
            return $res['export'];
        }
        else{
            list($content_header,$card_template) = $res;
        }
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'content_header' => $content_header,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }

    public function getHorizontalPhases(Request $request){

        ini_set('max_execution_time', 21100);
        ini_set('memory_limit', '-1');
        $user_roles = PermissionController::getRolesMapping();

        return array
        (
            'error' => false,
            'response' => $user_roles['response']['type'],
        );

    }

    public function getPhasePanel(Request $request){

        ini_set('max_execution_time', 21100);
        ini_set('memory_limit', '-1');



        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        $form_submit_data = array();
        $content_header = '';
        $export = '';
        $phase_id = Phase::getLivePhaseID();
        $user       = User::find(Auth::user()->id);
        $user_personnel_number = $user->personnelnumber;
        if(isset($request['data'])){
            $data = $request['data'];
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];

                if ($item_name == 'mode' && $dat == 'export'){
                    $export = $dat;
                }

                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = isset($dat_split[1]) ? $dat_split[1] : null;
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }else{
                        $id = '';
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }

        
        if($mode == 'update'){
            if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                        //print_r($request['data']);
                        $item_field = '';
                        $dat_split = explode('_',$key);
                        $count_split = count($dat_split);
                        for($j=0;$j<($count_split - 1);$j++){
                            $item_field .= $dat_split[$j].'_';
                        }
                        $item_field = substr($item_field,0,(strlen($item_field) - 1));
                        $item_value = $dat;
                        $form_submit_data[$item_field] = $item_value;
                        if($item_field == 'id'){
                            $selections['field_name'] = $item_field;
                            $selections['field_value'] = $item_value;
                        }
                        $i++;
                }
            }
            //print_r($form_submit_data);
            $cycle_id = Cycle::getLiveCycleID();
            foreach($form_submit_data as $key=>$submit_el){
                $split_tag = explode("_",$submit_el);
                $cnt_split = count($split_tag) - 1;
                $personnel_number = $split_tag[$cnt_split];
                for($i=0;$i<$cnt_split;$i++){
                    $value = $split_tag[$i];                   
                }
                //print_r($key);
                if (strpos($key, 'calc_') !== false || strpos($key, 'selectorselect') !== false || strpos($key, 'id_') !== false || strpos($key, 'mode') !== false || strpos($key, '_name') !== false) {
                    
                }else{
                    $employee_assessor = EmployeeAssessor::where('cycle_id', $cycle_id)->where('personnelnumber', $personnel_number)->first();
                    $prev_value = $employee_assessor->$key;
                    $new_value = $value;
                    $employee_assessor->$key = $new_value;
                    $employee_assessor->save();
                    $audit = new Audit;
                    $audit_entry = $audit::captureAudit($personnel_number, $key, $user_personnel_number, $prev_value, $new_value, $phase_id);
                }
                
            }
            
        }

        /*SET PERMISSION PER INSTANCE,PHASE(IF ANY),USER(IF ANY) etc.*/
        $instance_id = NULL;
        $instance_name = '';
        $user_pers_no = PermissionController::getUserPersonnelNumber();
        $user_instance = PermissionController::getUserInstance();
        if($user_instance['message'] == 'Success'){
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }
        $where[]['instance_indicator_id'] = $instance_id;
        //print_r($user_roles);exit;
        $user_roles = PermissionController::getRolesMapping();
        if(isset($request['tab']) && !empty($request['tab']) && $request['tab'] === 'start_tab' ){
             $current_tab = '';
             //get first table tab according to perms
             if(isset($user_roles['response']['type']['is_validator']) && $user_roles['response']['type']['is_validator'] === 1){
                 $current_tab = 'is_validator';
                 $content_header = 'Validation Manager';
             }else if(isset($user_roles['response']['type']['is_allocator']) && $user_roles['response']['type']['is_allocator'] === 1){
                 $current_tab = 'is_allocator';
                 $content_header = 'Allocation Manager';
             }else if(isset($user_roles['response']['type']['is_area_mgt']) && $user_roles['response']['type']['is_area_mgt'] === 1){
                 $current_tab = 'is_area_mgt';
                 $content_header = 'Business Area Manager';
             }else if(isset($user_roles['response']['type']['is_hcbp']) && $user_roles['response']['type']['is_hcbp'] === 1){
                 $current_tab = 'is_hcbp';
                 $content_header = 'Human Capital Business Partner (HCBP)';
             }else if(isset($user_roles['response']['type']['is_viewer']) && $user_roles['response']['type']['is_viewer'] === 1){
                 $current_tab = 'is_viewer';
                 $content_header = 'Reward Administrators and/or Viewer';
             }else{
                 $current_tab = 'signoff';
                 $content_header = 'Signoff & Feedback';
             }
        }  else {
            $current_tab = $request['tab'];
            if(isset($request['tab_name']) && !empty($request['tab_name']) && $request['tab_name'] !== '' ){
                $content_header = $request['tab_name'];
            }
        }

        


        if($current_tab != 'signoff'){
            if((isset($user_roles['response']['type']['is_admin']) && $user_roles['response']['type']['is_admin'] === 1)
                    || (isset($user_roles['response']['type']['is_super']) && $user_roles['response']['type']['is_super'] === 1)){

            }else{

                switch($current_tab){
                    case 'is_validator' :
                        $where[]['validation_manager_1'] = $user_pers_no;

                        break;
                    case 'is_allocator' :
                        $where[]['allocation_manager_1'] = $user_pers_no;
                        break;
                    case 'is_area_mgt' :
                        $where[]['business_manager_1'] = $user_pers_no;
                        break;
                    case 'is_hcbp' :
                        $where[]['hrbp_1'] = $user_pers_no;
                         break;
                    case 'is_viewer' :
                        $where[]['viewer_1'] = $user_pers_no;
                        break;
                    default :
                }
            }


                $panel = new TableViewerPanel('vw_employee_structure_validation', 'CRUSIX', $mode, $selections, $form_submit_data, 'vw_employee_structure_validation', 'horizontal',$where,$current_tab);
                $table = $panel->show();
                $table_content = $table['content'];
           }else{
               $table_content = 'Signoff';

           }

        if ($export == "export"){
            $id = 0;
            $where = Session::get('where_clause');
            $res = AppExport::storeExport('employees', $where);
            $response = array();
            if ($res['error'] === false){

                $export_res = $res['response'];
                $data['file_path'] = $export_res['file_path'];
                $data['mode'] = $mode;
                $response['export'] =
                 array
                (
                    'error'     => false,
                    'response'  => true,
                    'message'   => 'Success',
                    'template'  => '',
                    'data'      => $data,
                    'mode'      => $export,
                    'id'        => $id,
                );
                return $response;
            }
            else{
                $response['export'] =  $res;
                return $response;
            }

        }
        else{
            Session::put('where_clause', $where);
        }
        return [$content_header,$table_content];
    }

    public function getPhaseTabsPanels(Request $request){

        ini_set('max_execution_time', 21100);
        ini_set('memory_limit', '-1');
        /* This will handle the data you sent from react - each page on react will be differnet
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        $form_submit_data = array();

        if(isset($request['data'])){
            $data = $request['data'];
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];

                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }else{
                        $id = '';
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }


        /*SET PERMISSION PER INSTANCE,PHASE(IF ANY),USER(IF ANY) etc.*/
        $instance_id = NULL;
        $instance_name = '';
        $user_pers_no = PermissionController::getUserPersonnelNumber();
        $user_instance = PermissionController::getUserInstance();
        if($user_instance['message'] == 'Success'){
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }

        $user_roles = PermissionController::getRolesMapping();
        $where[]['instance_indicator_id'] = $instance_id;
        //print_r($user_roles);exit;


        $categories_tabs = $this->getRoleCategoryTabs($user_roles);
        $cnt_tabs = count($categories_tabs);
        $scripts = '';
        foreach($categories_tabs as $cat_tab){

           if($cat_tab['tab_name'] != 'signoff'){

                switch($cat_tab['tab_name']){
                    case 'is_validator' :
                        $where[]['validation_manager_1'] = $user_pers_no;
                        break;
                    case 'is_allocator' :
                        $where[]['allocation_manager_1'] = $user_pers_no;
                        break;
                    case 'is_area_mgt' :
                        $where[]['business_manager_1'] = $user_pers_no;
                        break;
                    case 'is_hcbp' :
                        $where[]['hrbp_1'] = $user_pers_no;
                        break;
                    case 'is_viewer' :
                        $where[]['viewer_1'] = $user_pers_no;
                        break;
                    default :


                }

                $panel = new TableViewerPanel('vw_employee_structure_validation', 'CRUSIX', $mode, $selections, $form_submit_data, 'vw_employee_structure_validation', 'horizontal',$where,$cat_tab['tab_name']);
                $table = $panel->show();
                $table_content = $table['content'];
                //$html = substr($table_content,0,strpos($table_content,'<script>') - 1);
                //$scripts .= substr($table_content,strpos($table_content,'<script>'),strpos($table_content,'</script>') - 1);
                $table_test[$cat_tab['tab_name']] = $table_content;
           }else{
               $table_test[$cat_tab['tab_name']] = 'Signoff';

           }

        }

        $template = new TemplateRenderer();
        $template_panel = new TemplatePanel('cf_library/DataTabs.twig',[
            'custom_settings' => [],
            'tabs'=>$categories_tabs,
            'table_test' => $table_test,
            'cnt_tabs'=>$cnt_tabs,
            'scripts'=>$scripts
        ]);
        $dataTable = $template_panel->show($template);
        return $dataTable;
    }


    /**
    * @return array
    */
    private function getRoleCategoryTabs($user_roles)
    {
        ini_set('max_execution_time', 21100);
        ini_set('memory_limit', '-1');
        $categories = array();
        $i=0;
        foreach($user_roles['response']['type'] as $key=>$value){

            //get the role name
            if($value == 1 && $key != 'is_admin' && $key != 'is_super' && $key != 'is_contingent' && $key != 'is_moderator'){
                $role_names = ManagerType::select('name')->where('code',$key)->first();

                $categories[$i]['name'] = $role_names->name;
                $categories[$i]['tab_name'] = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', $key));
                $i++;
            }

        }

        if(!empty($categories)){
            $categories[$i]['name'] = 'Signoff & Feedback';
            $categories[$i]['tab_name'] = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/','', 'signoff'));
        }

        return $categories;
    }

    public function exportDataSV(Request $request){

            $mode = 'export';
            $id = 0;
            $where = Session::get('where_clause');
            $res = AppExport::storeExport('employees', $where);

            $response = array();
            if ($res['error'] === false){

                $export_res = $res['response'];
                $data['file_path'] = $export_res['file_path'];
                $data['mode'] = $mode;
                $response['export'] =
                    array
                    (
                        'error'     => false,
                        'response'  => true,
                        'message'   => 'Success',
                        'template'  => '',
                        'data'      => $data,
                        'mode'      => $mode,
                        'id'        => $id,
                    );

                return $response;
            }
            else{
                $response['export'] =  $res;
                return $response;
            }




    }

    public function getStructuralData(Request $request)
    {
        try {

            $active_instance = PermissionController::getUserInstance()['response']['instance'];
            if($request->state['take']===null){
                Log::warning("The value is null: ".$request->state['take']);
            }
            // $res    = EmployeeStructuralValidation::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->state['sort_col'], $request->state['is_ascending'],$request['status_id'], '');
            // $total    = EmployeeStructuralValidation::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request->state['sort_col'], $request->state['is_ascending'],$request['status_id'], '');

            if($request->state['sort_col'] == ""){
               $sort_col = "personnelnumber";
           }else{
            $sort_col = $request->state['sort_col'];
           }
           $res    = EmployeeStructuralValidation::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true,$request['status_id'], $sort_col ,$request->state['is_ascending'],'');

            $total  = EmployeeStructuralValidation::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false,$request['status_id'],$sort_col ,$request->state['is_ascending'],'');
             $array = [];
            if($active_instance === 1){
                foreach ($res as $data){
                    $array []= array(
                        'id'                => $data->personnelnumber,
                        'personnelnumber'   => $data->personnelnumber,
                        'knownas'           => $data->knownas,
                        'lastname'          => $data->lastname,
                        'calc_prorata'      => $data->calc_prorata,
                        'jobgrade'          => $data->jobgrade,
                        'milestone'         => $data->milestone,
                        'jobtitle'          => $data->jobtitle,
                        'current_rating'    => $data->current_rating,
                        'h_1_performance1'  => $data->h_1_performance1,
                        'incentive_scheme'  => $data->incentive_scheme,

                        //'incentive_cap'                     => $data->incentive_cap,
                        //'internal_jobscale_code'            => $data->internal_jobscale_code,
                        //'calc_internal_jobscale_name'       => $data->calc_internal_jobscale_name,
                        //'calc_internal_jobscale_indicator'  => $data->calc_internal_jobscale_indicator,

                        'calc_allocationmanager1name'       =>$data->calc_allocationmanager1name !== null ? $data->calc_allocationmanager1name : "" ,
                        'calc_allocationmanager2name'       =>$data->calc_allocationmanager2name !== null ? $data->calc_allocationmanager2name : "" ,

                        'calc_phaseclustermanager1name'     =>$data->calc_phaseclustermanager1name !== null ? $data->calc_phaseclustermanager1name : "" ,
                        'calc_phaseclustermanager2name'     =>$data->calc_phaseclustermanager2name !== null ? $data->calc_phaseclustermanager2name : "" ,

                        'calc_businessmanager1name'         =>$data->calc_businessmanager1name !== null ? $data->calc_businessmanager1name : "" ,
                        'calc_businessmanager2name'         =>$data->calc_businessmanager2name !== null ? $data->calc_businessmanager2name : "" ,

                        'calc_hrbp1name'                    =>$data->calc_hrbp1name !== null ? $data->calc_hrbp1name . "   " : "" ,
                        'calc_hrbp2name'                    =>$data->calc_hrbp2name !== null ? $data->calc_hrbp2name . "   ": "" ,
                        'calc_hrbp3name'                    =>$data->calc_hrbp3name !== null ? $data->calc_hrbp3name . "   ": "" ,
                        'calc_hrbp4name'                    =>$data->calc_hrbp4name !== null ? $data->calc_hrbp4name . "   " : "" ,
                        'calc_hrbp5name'                    =>$data->calc_hrbp5name !== null ? $data->calc_hrbp5name . "   ": "" ,

                        'linemanager'          => $data->linemanager !== null ? $data->linemanager : "" ,
                        'orglevel5'          => $data->orglevel5,
                        'orglevel6'          => $data->orglevel6,
                        'orglevel7'          => $data->orglevel7,
                        'orglevel8'          => $data->orglevel8,

                        'assign_manager'          => true,
                        'dummy'             => 'adsdsads',

                    );
                }
            }
            elseif ($active_instance === 2){
                foreach ($res as $data){
                    $array []= array(
                        'id'                => $data->personnelnumber,
                        'personnelnumber'   => $data->personnelnumber,
                        'knownas'           => $data->knownas,
                        'lastname'          => $data->lastname,
                        'calc_prorata'      => $data->calc_prorata,
                        //'jobgrade'          => $data->jobgrade,
                        //'milestone'         => $data->milestone,
                        'jobtitle'          => $data->jobtitle,
                        'current_rating'    => $data->current_rating,
                        'h_1_performance1'  => $data->h_1_performance1,
                        'incentive_scheme'  => $data->incentive_scheme,

                        //'incentive_cap'                     => $data->incentive_cap,
                        //'internal_jobscale_code'            => $data->internal_jobscale_code,
                        //'calc_internal_jobscale_name'       => $data->calc_internal_jobscale_name,
                        //'calc_internal_jobscale_indicator'  => $data->calc_internal_jobscale_indicator,

                        'calc_allocationmanager1name'       =>$data->calc_allocationmanager1name !== null ? $data->calc_allocationmanager1name : "" ,
                        'calc_allocationmanager2name'       =>$data->calc_allocationmanager2name !== null ? $data->calc_allocationmanager2name : "" ,

                        'calc_phaseclustermanager1name'     =>$data->calc_phaseclustermanager1name !== null ? $data->calc_phaseclustermanager1name : "" ,
                        'calc_phaseclustermanager2name'     =>$data->calc_phaseclustermanager2name !== null ? $data->calc_phaseclustermanager2name : "" ,

                        'calc_businessmanager1name'         =>$data->calc_businessmanager1name !== null ? $data->calc_businessmanager1name : "" ,
                        'calc_businessmanager2name'         =>$data->calc_businessmanager2name !== null ? $data->calc_businessmanager2name : "" ,

                        'calc_hrbp1name'                    =>$data->calc_hrbp1name !== null ? $data->calc_hrbp1name . "   " : "" ,
                        'calc_hrbp2name'                    =>$data->calc_hrbp2name !== null ? $data->calc_hrbp2name . "   ": "" ,
                        'calc_hrbp3name'                    =>$data->calc_hrbp3name !== null ? $data->calc_hrbp3name . "   ": "" ,
                        'calc_hrbp4name'                    =>$data->calc_hrbp4name !== null ? $data->calc_hrbp4name . "   " : "" ,
                        'calc_hrbp5name'                    =>$data->calc_hrbp5name !== null ? $data->calc_hrbp5name . "   ": "" ,

                        'linemanager'          => $data->linemanager !== null ? $data->linemanager : "" ,
                        'orglevel5'          => $data->orglevel5,
                        'orglevel6'          => $data->orglevel6,
                        'orglevel7'          => $data->orglevel7,
                        'orglevel8'          => $data->orglevel8,

                        'assign_manager'          => true,
                        'dummy'             => 'adsdsads',

                    );
                }
            }
            elseif ($active_instance === 3){
                foreach ($res as $data){
                    $array []= array(
                        'id'                => $data->personnelnumber,
                        'personnelnumber'   => $data->personnelnumber,
                        'knownas'           => $data->knownas,
                        'lastname'          => $data->lastname,
                        'calc_prorata'      => $data->calc_prorata,
                        'jobgrade'          => $data->jobgrade,
                        'milestone'         => $data->milestone,
                        'jobtitle'          => $data->jobtitle,
                        'current_rating'    => $data->current_rating,
                        'h_1_performance1'  => $data->h_1_performance1,
                        'incentive_scheme'  => $data->incentive_scheme,

                        'incentive_cap'                     => $data->incentive_cap,
                        'internal_jobscale_code'            => $data->internal_jobscale_code,
                        'calc_internal_jobscale_name'       => $data->calc_internal_jobscale_name,
                        'calc_internal_jobscale_indicator'  => $data->calc_internal_jobscale_indicator,

                        'calc_allocationmanager1name'       =>$data->calc_allocationmanager1name !== null ? $data->calc_allocationmanager1name : "" ,
                        'calc_allocationmanager2name'       =>$data->calc_allocationmanager2name !== null ? $data->calc_allocationmanager2name : "" ,

                        'calc_phaseclustermanager1name'     =>$data->calc_phaseclustermanager1name !== null ? $data->calc_phaseclustermanager1name : "" ,
                        'calc_phaseclustermanager2name'     =>$data->calc_phaseclustermanager2name !== null ? $data->calc_phaseclustermanager2name : "" ,

                        'calc_businessmanager1name'         =>$data->calc_businessmanager1name !== null ? $data->calc_businessmanager1name : "" ,
                        'calc_businessmanager2name'         =>$data->calc_businessmanager2name !== null ? $data->calc_businessmanager2name : "" ,

                        'calc_hrbp1name'                    =>$data->calc_hrbp1name !== null ? $data->calc_hrbp1name . "   " : "" ,
                        'calc_hrbp2name'                    =>$data->calc_hrbp2name !== null ? $data->calc_hrbp2name . "   ": "" ,
                        'calc_hrbp3name'                    =>$data->calc_hrbp3name !== null ? $data->calc_hrbp3name . "   ": "" ,
                        'calc_hrbp4name'                    =>$data->calc_hrbp4name !== null ? $data->calc_hrbp4name . "   " : "" ,
                        'calc_hrbp5name'                    =>$data->calc_hrbp5name !== null ? $data->calc_hrbp5name . "   ": "" ,

                        'linemanager'          => $data->linemanager !== null ? $data->linemanager : "" ,
                        'orglevel5'          => $data->orglevel5,
                        'orglevel6'          => $data->orglevel6,
                        'orglevel7'          => $data->orglevel7,
                        'orglevel8'          => $data->orglevel8,

                        'assign_manager'          => true,
                        'dummy'             => 'adsdsads',

                    );
                }
            }

            Log::warning('Check this out');
            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>$this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>count($total),
                    'currentCycle'=>Cycle::getLiveCycleID()
                ),
                'message'=>'Records Loaded');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load records');
        }
    }

    public function getAssessorData(Request $request)
    {
        try {

            if($request->state['take']===null){
                Log::warning("The value is null: ".$request->state['take']);
            }
            $res    = EmployeeStructuralValidation::getAssessorRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, null, '');
            $total  = EmployeeStructuralValidation::getAssessorRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, null, '');

            $array = [];

            foreach ($res as $data){
                $array []= array(
                    'id'                => $data->personnelnumber,
                    'personnelnumber'   => $data->personnelnumber,
                    'knownas'           => $data->knownas,
                    'lastname'          => $data->lastname,
                    'orglevel5'          => $data->orglevel5,
                    'orglevel6'          => $data->orglevel6,
                    'orglevel7'          => $data->orglevel7,
                    'orglevel8'          => $data->orglevel8,

                );
            }
            Log::warning('Check this out');
            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>$this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>count($total),
                    'currentCycle'=>Cycle::getLiveCycleID()
                ),
                'message'=>'AssessorDataLoaded');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get issue logs');
        }
    }

    public function assignAssessor(Request $request){
        try{
            $manager_id     = $request->manager_id;
            $manager_type   = $request->manager_type;
            $record_id      = $request->record_id;
            $user           = Auth::user();
            $col            = '';

            // get the employee assessor
            $emp_assessor = EmployeeAssessor::where('cycle_id', Cycle::getLiveCycleID())->where('personnelnumber', $record_id)->first();
            $new_manager = Employee::where('cycle_id', Cycle::getLiveCycleID())->where("personnelnumber", $manager_id)->first();

            if ($manager_type === "calc_allocationmanager1name"){
                $col = 'allocation_manager_1';
            }
            elseif ($manager_type === "calc_allocationmanager2name"){
                $col = 'allocation_manager_2';
            }
            elseif ($manager_type === "calc_businessmanager1name"){
                $col = 'business_manager_1';
            }
            elseif ($manager_type === "calc_businessmanager2name"){
                $col = 'business_manager_2';
            }

            elseif ($manager_type === "calc_phaseclustermanager1name"){
                $col = 'business_area_manager_1';
            }

            elseif ($manager_type === "calc_phaseclustermanager2name"){
                $col = 'business_area_manager_2';
            }

            elseif ($manager_type === "calc_hrbp1name"){
                $col = 'hrbp_1';
            }

            elseif ($manager_type === "calc_hrbp2name"){
                $col = 'hrbp_2';
            }

            elseif ($manager_type === "calc_hrbp3name"){
                $col = 'hrbp_3';
            }

            elseif ($manager_type === "calc_hrbp4name"){
                $col = 'hrbp_4';
            }

            elseif ($manager_type === "calc_hrbp5name"){
                $col = 'hrbp_5';
            }

            $value_before = $emp_assessor->{$col};
            $emp_assessor->{$col} = $manager_id;
            $emp_assessor->save();
            Audit::captureAudit($record_id, $col, $user->personnelnumber, $value_before, $manager_id );

            return array('error' => false, 'response' => [], 'message' => 'Manager updated successfully');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get issue logs');
        }
    }


    public function getAllOrgStructures(Request $request)
    {
        try {
            $active_instance = PermissionController::getUserInstance()['response']['instance'];

            $res    = EmployeeStructuralValidation::getOrg($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, null, '');
            $total  = EmployeeStructuralValidation::getOrg($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, null, '');

            $array = [];
            foreach ($res as $item){
                $array[] = array(
                    'id'        =>$item->id,
                    'instance'  =>$item->instance,
                    'region'    =>$item->region,
                    'country'   =>$item->country,
                    'company'            =>$item->company,
                    'business'            =>$item->business,
                    'business_unit'            =>$item->business_unit,
                    'division'            =>$item->division,
                    'department'            =>$item->department,
                );
            }

            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>$this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>count($total),
                    'currentCycle'=>Cycle::getLiveCycleID()
                ),
                'message'=>'Org structures Loaded');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not load Org Structures');
        }
    }

    public function getOrgStructureByID(Request $request)
    {
        try {
            $id     = $request['id'];
            $record = DB::table('vw_organisational_structures');
            $record =   $record->where('id', $id)->first();

            return array (
                'error'=>false,
                'response'=> ['org_structure' => $record],
                'message'=>'Loaded Org Structure');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not load org structure');
        }
    }

    public function geFranchiseMapping(Request $request)
    {
        try {
            $id     = $request['id'];
            $record = DB::table('vw_franchise_mapping');
            $record =   $record->where('id', $id)->first();

            return array (
                'error'=>false,
                'response'=> ['franchise_mapping' => $record],
                'message'=>'Loaded franchise mapping');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could no load franchise mapping');
        }
    }

    public function geFranchiseMappings(Request $request)
    {
        try {
           //$active_instance = PermissionController::getUserInstance()['response']['instance'];

            $res    = FranchiseMapping::getFranchiseMappings($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, null, '');
            $total  = FranchiseMapping::getFranchiseMappings($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, null, '');

            $array = [];
            foreach ($res as $item){
                $array[] = array(
                    'id'                    => $item->id,
                    'incentive_scheme'      => $item->incentive_scheme,
                    'franchise_one_level'   => $item->franchise_one_level,
                    'franchise_two_level'   => $item->franchise_two_level,
                    'franchise_three_level' => $item->franchise_three_level
                );
            }

            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>$this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>count($total)
                ),
                'message'=>'Org structures Loaded');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get issue logs');
        }
    }
}
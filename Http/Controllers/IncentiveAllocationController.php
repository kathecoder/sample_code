<?php

namespace App\Http\Controllers;

use App\Models\AppExport;
use App\Models\Cycle;
use App\Models\DataField;
use App\Models\EmployeeIncentive;
use App\Models\EmployeeIncentiveAllocations;
use App\Models\EmployeeStructuralValidation;
use App\Models\ManagerType;
use App\CFLibrary\FilterHandler;
use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use App\Models\ManagerTypeUser;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Symfony\Component\VarDumper\Cloner\Data;


class IncentiveAllocationController extends Controller
{
    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }

    public function getAllocationData(Request $request){

        try{
            //get headers
            $active_instance = PermissionController::getUserInstance()['response']['instance'];
            if ($active_instance == 1){
                $editable_fields = self::$group_editable_fields;
                $headers = self::$group_headers;
            }

            if ($active_instance == 2){
                $editable_fields = self::$africa_editable_fields;
                $headers = self::$africa_headers;
            }

            if ($active_instance == 3){
                $editable_fields = self::$stanlib_editable_fields;
                $headers = self::$stanlib_headers;
            }

            $imploded_strings = implode("' '", $headers);
            $data_fields = DataField::whereIn('name', $headers)
                ->orderByRaw(DB::raw("FIELD(name, '$imploded_strings')"))
                ->get()->all();

            // get rows
            $user               = Auth::user();
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $user_roles         = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $collection         = null;
            $manager_pn         = $user->personnelnumber;

            $collection = DB::table('vw_employees_export')
                //->where('cycle_id',                 $cycle_id)
                ->where('instance_indicator_id',     $active_instance_id);

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])){
                $collection->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)

                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                        ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

                });
            }

            $collection = $collection->orderByDesc('updated_at')->get();




            $data = array(
                'collection'        => $collection,
                'headers'           => $data_fields,
                'editable_fields'   => $editable_fields,
            );

            return array('error' => false, 'response' => $data, 'message' => '');




        }
        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public function getAllocationRecords(Request $request)
    {
        try {
            $active_instance    = PermissionController::getUserInstance()['response']['instance'];
            $fields             = DataField::getFields($request->table_type);

            if($request->state['sort_col'] == ""){
                $sort_col = "knownas";
            }else{
                $sort_col = $request->state['sort_col'];
            }
            $res    = EmployeeIncentiveAllocations::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true,$request['status_id'], $sort_col ,$request->state['is_ascending'],$request->table_type);
            $total  = EmployeeIncentiveAllocations::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false,$request['status_id'],$sort_col ,$request->state['is_ascending'],$request->table_type);
            $array = [];

            foreach ($res as $data){

                // build array entity based on the selected fields
                $row = array();
                foreach ($fields as $field){

                    if (in_array($field, ['salary_continuation_plan', 'do_not_process_salary', 'do_not_process_bonus']) ){
                        $row[$field] =  $data->{$field} == 1 ? "Yes" : "No";
                    }
                    elseif(  (is_numeric($data->{$field}) == 1 &&  !in_array($field, ['personnelnumber', 'id']))   ){

                        if (strpos($field, 'percent') >0 || $field == 'suggested_incentive'|| $field == 'assessment_score'){

                            $row[$field] =number_format($data->{$field}, 1, '.', ' ');

                        }
                        else{
                            $row[$field] = number_format($data->{$field}, 0, '.', ' ');
                        }
                    }
                    else{
                        $row[$field] =  $data->{$field};
                    }

                }
                if (isset($data->incentive_scheme) && in_array($data->incentive_scheme, ['Specialist Scheme', 'Senior Management Scheme', 'Risk Senior Management Scheme', 'Sales Incentive Scheme - Africa'])){
                    $row["special_scheme"] = true;
                    $row["not_special_scheme"]  = true;
                }
                else {
                    $row["special_scheme"]      = false;
                    $row["not_special_scheme"]  = true;
                }


                // add the array entity to the $array variable
                $array [] = $row;
            }

            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>$this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>count($total),
                ),
                'message'=>'Allocation Records Loaded');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load Allocation records');
        }
    }

    public function allocationCalculations(Request $request){
        try{
            $res = [];
            if ($request->category == 'salary'){

                $res = EmployeeIncentiveAllocations::salaryCalculations($request->id, $request->field_name, $request->value);
            }
            elseif ($request->category == 'incentive'){
                $res = EmployeeIncentiveAllocations::incentiveCalculations($request->id, $request->field_name, $request->value);
            }

            return $res;
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load Allocation records');
        }
    }

    public function getGroupMiniDashBoard(Request $request){

        try{

            $dataset= [];
            $pool = 0.068; //config('iiasstaticlookups.incentive_pool');
            $collection = IncentiveAllocationCalcController::GroupMiniDashBoardCalc($request->filtered);
            if (count($collection) > 0  && isset($collection[0])){
                /************ General Staff Incentive Start *************/

                $total_proposed_incentive_zar_gi = $collection[0]->total_proposed_incentive_zar_gi != null ? $collection[0]->total_proposed_incentive_zar_gi : 0;
                $dataset['total_proposed_incentive_zar_gi'] = number_format ( $total_proposed_incentive_zar_gi, 0,  '.', " " ) ;

                $proposed_incentive_percentage_gi = $collection[0]->proposed_incentive_percentage_gi != null ? $collection[0]->proposed_incentive_percentage_gi : 0;
                $dataset['proposed_incentive_percentage_gi'] = number_format ( $proposed_incentive_percentage_gi, 1,  '.', " " ) ;

                $total_salary_zar_gi = $collection[0]->total_salary_zar_gi != null ? $collection[0]->total_salary_zar_gi : 0;

                $proposed_incentive_pool = $total_salary_zar_gi * $pool;
                $dataset['proposed_incentive_pool_gi'] = number_format ( $proposed_incentive_pool, 0,  '.', " " ) ;

                $total_incentive_pool_available = $proposed_incentive_pool - $total_proposed_incentive_zar_gi  ;
                $dataset['total_incentive_pool_available_gi'] = number_format ( $total_incentive_pool_available, 0,  '.', " " ) ;

                /************ General Staff Incentive End *************/


                /************ SMI & Risk Start *************/

                //$total_actual_pool              = $collection[0]->calc_on_target_achievement != null ? $collection[0]->calc_on_target_achievement : 0.0; //total actual pool
                $total_actual_pool              = $collection[0]->total_actual_pool_sm_risk != null ? $collection[0]->total_actual_pool_sm_risk : 0; //total actual pool
                $total_smi_non_financial_allocation_value   = $collection[0]->allocation != null ? $collection[0]->allocation : 0; // Total SMI Non-Financial Allocation Value total_smi_non_financial_allocation_value
                $total_variance                 = $total_actual_pool - $total_smi_non_financial_allocation_value;
                $current_total_salary_zar_sm    = $collection[0]->current_total_salary_zar_sm != null ? $collection[0]->current_total_salary_zar_sm : 0;
                $percentage                     = $total_actual_pool > 0 ? $total_smi_non_financial_allocation_value / $total_actual_pool  * 100 : 0;

                $dataset['current_total_salary_zar_sm']   = number_format ( $current_total_salary_zar_sm, 0,  '.', " " ) ;
                $dataset['percentage_sm']                 = number_format ( $percentage, 1,  '.', " " ) ;
                $dataset['total_variance_sm']             = number_format ( $total_variance, 0,  '.', " " ) ;
                $dataset['total_actual_pool_sm']          = number_format ( $total_actual_pool, 0,  '.', " " ) ;
                $dataset['total_smi_non_financial_allocation_value']          = number_format ( $total_smi_non_financial_allocation_value, 0,  '.', " " ) ;


                $total_actual_pool_sm_risk                  = $collection[0]->total_actual_pool_sm_risk != null ? $collection[0]->total_actual_pool_sm_risk : 0;
                $dataset['total_actual_pool_sm_risk']       = number_format ( $total_actual_pool_sm_risk, 0,  '.', " " ) ;


                $calc_kpi_incentive_value_sm_risk               = $collection[0]->calc_kpi_incentive_value_sm_risk != null ? $collection[0]->calc_kpi_incentive_value_sm_risk : 0;
                $dataset['total_variance_sm_risk']              = number_format ( $total_actual_pool_sm_risk - $calc_kpi_incentive_value_sm_risk, 0,  '.', " " ) ;
                $dataset['total_percentage_variance_sm_risk']   = $total_actual_pool_sm_risk > 0 ? number_format ( ($total_actual_pool_sm_risk - $calc_kpi_incentive_value_sm_risk) / $total_actual_pool_sm_risk * 100, 1,  '.', " " ) : 0;
                $dataset['kpi_percent_sm_risk']                 = $total_actual_pool_sm_risk != 0 ? number_format ( $calc_kpi_incentive_value_sm_risk / $total_actual_pool_sm_risk * 100, 1,  '.', " " ) : 0;
                $dataset['kpi_percent_sm_risk']                 = $total_actual_pool_sm_risk != 0 ? number_format ( $collection[0]->total_actual_pool_percentage_sm, 1,  '.', " " ) : 0;





                /************ SMI & Risk End *************/

                /************ Specialist Start *************/
                $calc_kpi_incentive_value_sp       = $collection[0]->calc_kpi_incentive_value_sp != null ? $collection[0]->calc_kpi_incentive_value_sp : 0;
                $current_total_salary_zar_sp    = $collection[0]->current_total_salary_zar_sp != null ? $collection[0]->current_total_salary_zar_sp :0;
                $total_actual_pool_sp           = $collection[0]->total_actual_pool_sp != null ? $collection[0]->total_actual_pool_sp : 0;
                $dataset['kpi']       = number_format ( $calc_kpi_incentive_value_sp, 0,  '.', " " ) ;
                $dataset['current_total_salary_zar_sp']   = number_format ( $current_total_salary_zar_sp, 0,  '.', " " ) ;


                $dataset['total_actual_pool_sp']          = number_format ( $total_actual_pool_sp, 0,  '.', " " ) ;
                $dataset['total_variance_sp']             = number_format ( $total_actual_pool_sp - $calc_kpi_incentive_value_sp, 0,  '.', " " ) ;
                $dataset['total_percentage_variance_sp']  = $total_actual_pool_sp > 0 ? number_format ( ($total_actual_pool_sp - $calc_kpi_incentive_value_sp) / $total_actual_pool_sp * 100, 1,  '.', " " ) : 0.00;
                $dataset['kpi_percent_sp']                = $total_actual_pool_sp != 0 ? number_format ( $calc_kpi_incentive_value_sp / $total_actual_pool_sp * 100, 1,  '.', " " ) : 0.00;
                $dataset['kpi_percent_sp']                = $total_actual_pool_sp != 0 ? number_format ( $collection[0]->total_actual_pool_percentage_sp, 1,  '.', " " ) : 0.00;

                /************ Specialist End *************/


                /************ Other Start *************/
                $total_proposed_incentive_zar = $collection[0]->total_proposed_incentive_zar_other != null ? $collection[0]->total_proposed_incentive_zar_other : 0;
                $dataset['total_proposed_incentive_zar_other'] = number_format ( $total_proposed_incentive_zar, 0,  '.', " " ) ;
                /************ Other End *************/


                /************ General Staff Salary Start *************/

                $current_total_salary_zar   = $collection[0]->calc_salary_zar;
                $sum_proposedsalary_zar     = $collection[0]->calc_proposedsalary_zar;

                $dataset['current_total_salary_zar']                 = number_format ( $current_total_salary_zar, 0,  '.', " " ) ;
                $dataset['new_total_salary_amount_zar']              = number_format ( $sum_proposedsalary_zar, 0,  '.', " " ) ;
                $proposed_increase_allocation_amount_zar                    = $sum_proposedsalary_zar - $current_total_salary_zar;
                $dataset['proposed_increase_allocation_amount_zar']  = number_format ( $proposed_increase_allocation_amount_zar, 0,  '.', " " ) ;


                $total_percentage_uplift_salary = $current_total_salary_zar == 0 ? 0.00 : ($proposed_increase_allocation_amount_zar / $current_total_salary_zar) * 100;
                $dataset['total_percentage_uplift_salary'] = round($total_percentage_uplift_salary, 1);

                /************ General Staff Salary Start *************/

                /************ Bands Start *************/
                $b1b2   = $collection[0]->ps_percentage12 != null ? $collection[0]->ps_percentage12 : 0;
                $b3     = $collection[0]->ps_percentage3  != null ? $collection[0]->ps_percentage3  : 0;
                $b4b5   = $collection[0]->ps_percentage45 != null ? $collection[0]->ps_percentage45 : 0;

                $dataset['b1b2'] = round($b1b2, 1);
                $dataset['b3'] = round($b3, 1);
                $dataset['b4b5'] = round($b4b5, 1);

                /************ Bands End *************/
            }
            else{

                $dataset['total_proposed_incentive_zar_gi']     = 0.00;
                $dataset['proposed_incentive_percentage_gi']    = 0.00;
                $dataset['proposed_incentive_pool_gi']          = 0.00;
                $dataset['total_incentive_pool_available_gi']   = 0.00;

                $dataset['current_total_salary_zar_sm']   = 0.00;
                $dataset['percentage_sm']                 = 0.00;
                $dataset['total_variance_sm']             = 0.00;
                $dataset['total_actual_pool_sm']          = 0.00;
                $dataset['total_smi_non_financial_allocation_value']    = 0.00;

                $dataset['kpi']                             = 0.00;
                $dataset['current_total_salary_zar_sp']     = 0.00;

                $dataset['total_proposed_incentive_zar_other'] = 0.00;

                $dataset['current_total_salary_zar']                 = number_format ( 0.00, 2,  '.', " " ) ;
                $dataset['new_total_salary_amount_zar']              = number_format ( 0.00,2,  '.', " " ) ;
                $dataset['proposed_increase_allocation_amount_zar']  = number_format ( 0.00, 2,  '.', " " ) ;
                $dataset['total_percentage_uplift_salary']           = round(0.00, 1);


            }



            return array('error' => false, 'response' => $dataset, 'message' => 'Group Dashboard Dataset Loaded Successfully');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load Group Dashboard Dataset');
        }

    }

    public function getAfricaMiniDashBoard(Request $request){
        try{
            $user_roles             = UserRolesController::getUserRoleData()['response']['UserRoles'];
            $totalCountrySalaryZAR  = IncentiveAllocationCalcController::africaMiniDashBoardCalc(/*$request->filtered*/);
            $new_total_salary_amount_africa_all         = 0;
            $current_total_salary_africa_all            = 0;
            $total_proposed_incentive_africa_all        = 0;
            $proposed_incentive_percentage_africa_all   = 0;
            $calc_salary_zar_incentive_12_all           = 0; // general staff sccheme africa
            $total_pool_africa_all                      = 0;
            $af_count = 0;
            $salary_countries_array = [];
            $salary_actual_colors = '';
            $incentive_actual_colors = '';
            $target_salary = '';
            $target_incentive = '';

            foreach ($totalCountrySalaryZAR as $item){
                $key = str_replace(' ', '_', $item->orglevel7);
                $pool = 0;
                $country = DB::table('country_pools')->where('name', $item->orglevel7)->first();
                if ($country){
                    $pool                           = $country->bonus_pool_percentage / 100;
                    $item->currency_code            = $country->code;
                    $item->bonus_pool_percentage    = round($country->bonus_pool_percentage, 1);
                    $item->salary_pool_percentage   = round($country->salary_pool_percentage, 1);


                    $target_salary                  = $country->salary_pool_percentage;
                    $target_incentive               = $country->bonus_pool_percentage;

                }

                $actual_salary                                      = $item->total_percentage_uplift_salary;
                $item->total_percentage_uplift_salary               = number_format($item->total_percentage_uplift_salary , 1, '.', ' ');
                $item->new_total_salary_amount_africa               = number_format($item->new_total_salary_amount_africa , 0, '.', ' ');
                $item->proposed_increase_allocation_amount_africa   = number_format($item->proposed_increase_allocation_amount_africa , 0, '.', ' ');
                $item->current_total_salary_africa                  = number_format($item->current_total_salary_africa , 0, '.', ' ');

                /*$actual_incentive                                   = $item->total_proposed_incentive_africa / $item->calc_salary_zar_incentive_12 * 100;
                $item->proposed_incentive_pool_africa               = number_format($item->calc_salary_zar_incentive_12  * $pool, 0, '.', ' ');
                $item->total_incentive_pool_available_africa        = number_format(($item->calc_salary_zar_incentive_12  * $pool) - $item->total_proposed_incentive_africa, 0, '.', ' ');
                $item->total_proposed_incentive_africa              = number_format($item->total_proposed_incentive_africa , 0, '.', ' ');
                $item->proposed_incentive_percentage_africa         = number_format($actual_incentive , 1, '.', ' ');*/

                $calc_salary_zar_incentive_12_excl_smi_sale         = $item->calc_salary_zar_incentive_12_excl_smi_sale;
                $actual_incentive                                   = $calc_salary_zar_incentive_12_excl_smi_sale > 0 ? $item->total_proposed_incentive_africa_excl_smi_sale / $calc_salary_zar_incentive_12_excl_smi_sale * 100 : 0.0;
                $item->proposed_incentive_pool_africa               = number_format($item->calc_salary_zar_incentive_12_excl_smi_sale  * $pool, 0, '.', ' ');
                $item->total_incentive_pool_available_africa        = number_format(($item->calc_salary_zar_incentive_12_excl_smi_sale  * $pool) - $item->total_proposed_incentive_africa_excl_smi_sale, 0, '.', ' ');
                $item->total_proposed_incentive_africa              = number_format($item->total_proposed_incentive_africa_excl_smi_sale , 0, '.', ' ');
                $item->proposed_incentive_percentage_africa         = number_format($actual_incentive , 1, '.', ' ');

                //-------------------------------- put in a function ----------------------

                if($actual_salary < 0 ){
                    $salary_actual_colors = 'target_black';
                }
                elseif ($actual_salary >=  ($target_salary + 2)){
                    $salary_actual_colors = 'target_red';
                }
                elseif ($actual_salary >= ($target_salary - 0.9) && $actual_salary <= ($target_salary + 0.9)){
                    $salary_actual_colors = 'target_green';
                }
                elseif ($actual_salary <=  ($target_salary - 1) ){
                    $salary_actual_colors = 'target_blue';
                }

                if($actual_incentive < 0 ){
                    $incentive_actual_colors = 'target_black';
                }
                elseif ($actual_incentive >=  ($target_incentive + 2)){
                    $incentive_actual_colors = 'target_red';
                }
                elseif ($actual_incentive >= ($target_incentive - 0.9) && $actual_incentive <= ($target_incentive + 0.9)){
                    $incentive_actual_colors = 'target_green';
                }
                elseif ($actual_incentive <=  ($target_incentive - 1) ){
                    $incentive_actual_colors = 'target_blue';
                }


                $item->incentive_actual_colors  = $incentive_actual_colors;
                $item->salary_actual_colors     = $salary_actual_colors;
                $salary_countries_array[$key]   = $item;

                //-------------------------------- put in a function ----------------------

                if ( in_array($user_roles['accounttype'], [1, 2, 3])){
                    $af_count += 1;
                    $current_total_salary_africa_all            = $current_total_salary_africa_all + $item->current_total_salary_africa_all;
                    $new_total_salary_amount_africa_all         = $new_total_salary_amount_africa_all + $item->new_total_salary_amount_africa_all;

                    //incentive not filtered
                    $total_proposed_incentive_africa_all        = $total_proposed_incentive_africa_all + $item->total_proposed_incentive_africa_all_excl_smi_sale;
                    $proposed_incentive_percentage_africa_all   = $proposed_incentive_percentage_africa_all + $item->proposed_incentive_percentage_africa_all_excl_smi_sale;
                    $calc_salary_zar_incentive_12_all           = $calc_salary_zar_incentive_12_all + $item->calc_salary_zar_incentive_12_all_excl_smi_sale;

                    // calculate pool for the whole of africa - in zar
                    $total_pool_africa_all                      = $total_pool_africa_all + ($item->calc_salary_zar_incentive_12_all_excl_smi_sale * $pool);
                    // calculate total incentive percentage  - in zar (total incentive value / total current salary value) * 100
                }

            }

            if ( in_array($user_roles['accounttype'], [1, 2, 3])){
                $pool = config('iiasstaticlookups.incentive_pool');
                $all_zar = new \stdClass();
                $all_zar->orglevel7                                     =  'ZAR';
                $all_zar->currency_code                                 =  'ZAR';
                $all_zar->current_total_salary_africa                   =  number_format($current_total_salary_africa_all, 0, '.', ' ');
                $all_zar->proposed_increase_allocation_amount_africa    =  number_format($new_total_salary_amount_africa_all - $current_total_salary_africa_all, 0, '.', ' ');
                $all_zar->new_total_salary_amount_africa                =  number_format($new_total_salary_amount_africa_all, 0, '.', ' ');
                $all_zar->total_percentage_uplift_salary                =  $current_total_salary_africa_all == 0 ? 0.0 : round( $current_total_salary_africa_all != 0 ? ((($new_total_salary_amount_africa_all - $current_total_salary_africa_all) / $current_total_salary_africa_all) * 100) : 0, 1);

                $all_zar->total_proposed_incentive_africa               =  number_format($total_proposed_incentive_africa_all, 0, '.', ' ');
                $all_zar->proposed_incentive_percentage_africa          =  $calc_salary_zar_incentive_12_all == 0 ? 0.0 :number_format($total_proposed_incentive_africa_all /  $calc_salary_zar_incentive_12_all * 100, 0, '.', ' ');
                $all_zar->proposed_incentive_pool_africa                =  number_format($total_pool_africa_all, 0, '.', ' ');
                $all_zar->total_incentive_pool_available_africa         =  number_format($total_pool_africa_all - $total_proposed_incentive_africa_all, 0, '.', ' ');
                $salary_countries_array['ZAR'] = $all_zar;
            }
//            logger($salary_countries_array);
            return array('error' => false, 'response' => $salary_countries_array, 'message' => 'Group Dashboard Dataset Loaded Successfully');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load Group Dashboard Dataset');
        }
    }


    public function getSTANLIBMiniDashboard(Request $request){
        try {

            $dataset = array();
            $collection = IncentiveAllocationCalcController::STANLIBMiniDashBoardCalc($request->filtered);

           // dd($collection);

            if (count($collection) > 0  && isset($collection[0])){

                /************ Bands Start *************/
                $b1b2   = $collection[0]->ps_percentage12 != null ? $collection[0]->ps_percentage12 : 0;
                $b3     = $collection[0]->ps_percentage3  != null ? $collection[0]->ps_percentage3  : 0;
                $b4b5   = $collection[0]->ps_percentage45 != null ? $collection[0]->ps_percentage45 : 0;

                $dataset['b1b2']    = round($b1b2, 1);
                $dataset['b3']      = round($b3, 1);
                $dataset['b4b5']    = round($b4b5, 1);

                $dataset['b1b2_color']    = self::colorCoding( round($b1b2, 1), 3);
                $dataset['b3_color']      = self::colorCoding( round($b3, 1), 3);
                $dataset['b4b5_color']    = self::colorCoding( round($b4b5, 1), 3);

                /************ Bands End *************/
            }
            else{
                $dataset['b1b2'] = 0.0;
                $dataset['b3']   = 0.0;
                $dataset['b4b5'] = 0.0;
            }

            return array('error' => false, 'response' => $dataset, 'message' => 'STANLIB Dashboard Dataset Loaded Successfully');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load STANLIB Dashboard Dataset');
        }
    }

    public function getSTANLIBIncentive(Request $request){
        try {
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $user               = Auth::user();
            $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 : 4;

            if ($active_instance_id === 3 && ManagerTypeUser::isHCBP($user->personnelnumber, $active_instance_id ) === false){
                $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 :7;
            }

            $dataset                = array();
            $collection             = IncentiveAllocationCalcController::STANLIBIncentiveCalc($request->filtered);
            $franchise_two_levels   = DB::table('franchise_two_levels')->pluck('bonus_pool', 'id');
            $temp = array();
            $franchise_two_level_ids = [];

            foreach ($collection as $item) {
                $franchise_two_level_ids[] = $item->franchise_two_level_id;
            }

            $franchise_two_level_ids = array_unique($franchise_two_level_ids);
            $franchises = DB::table('vw_franchise_mapping')->whereIn('franchise_two_level_id', $franchise_two_level_ids)->get();

            foreach ($collection as $item) {
                foreach ($franchises as $franchise){
                    if ($franchise->franchise_two_level_id == $item->franchise_two_level_id ){
                        $item->franchise_one_level_id = $franchise->franchise_one_level_id;
                        $temp[$item->franchise_two_level_id] = $item;
                    }
                }

            }


            foreach ($franchises as $franchise){
                $total_proposedincentive = 0;
                $total_pool              = 0;
                foreach ($temp  as $key => $item){
                    if ($franchise->franchise_one_level_id === $item->franchise_one_level_id && $item->franchise_two_level_id !== null){

                        $pool                       = $franchise_two_levels[$item->franchise_two_level_id] >= 0 ? $franchise_two_levels[$item->franchise_two_level_id] /100 * $item->calc_salary_zar : 0;

                        $total_proposedincentive    = $total_proposedincentive + $item->proposedincentive;
                        $total_pool                 = $total_pool + $pool;

                        if ($franchise->franchise_two_level_id === $item->franchise_two_level_id ){
                            // get the pool, calc total pool, calc remaining
                            $pool = $franchise_two_levels[$item->franchise_two_level_id] >= 0 ? $franchise_two_levels[$item->franchise_two_level_id] /100 * $item->calc_salary_zar : 0;
                            $item->pool         = number_format($pool, '0', '.', ' ');
                            $remaining          = $pool - $item->proposedincentive;
                            $item->remaining    = number_format($remaining, '0', '.', ' ');
                            $item->colour       = $remaining < 0 ? "Incentive-Red" : "";

                            $item->pool = number_format($pool, '0', '.', ' ');
                            $item->proposedincentive_formatted = number_format($item->proposedincentive, '0', '.', ' ');
                            $dataset[$franchise->franchise_one_level][$franchise->franchise_two_level] = $item;

                        }
                    }
                }

                $dataset[$franchise->franchise_one_level]['total_proposedincentive']    = number_format($total_proposedincentive, '0', '.', ' ');
                $dataset[$franchise->franchise_one_level]['total_pool']                 = number_format($total_pool, '0', '.', ' ');
                $dataset[$franchise->franchise_one_level]['total_remaining']            = number_format($total_pool - $total_proposedincentive, '0', '.', ' ');

            }

            return array('error' => false, 'response' => $dataset, 'manager_type' => $manager_type, 'message' => 'STANLIB Dashboard Dataset Loaded Successfully');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load STANLIB Dashboard Dataset');
        }
    }

    public function getFormFields(Request $request){
        try{
            $res = DataField::getFormFields($request);
            return array('error' => false, 'response' => $res, 'message' => 'Success');
            return $res;
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load Allocation records');
        }
    }

    public function personnelAdminCalculations(Request $request){
        try{
            //logger($request);
            $res = [];
            if ($request->category == 'salary'){

                $res = EmployeeIncentiveAllocations::salaryCalculationsPA($request->personnelnumber, $request->field_name, $request->value);
            }
            elseif ($request->category == 'incentive'){
                $res = EmployeeIncentiveAllocations::incentiveCalculationsPA($request->personnelnumber, $request->field_name, $request->value);
            }

            return $res;
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load Allocation records');
        }
    }

    public function getScorecardInfo(Request $request){


    }


    public static function colorCoding($actual, $target){
        $actual_colors = 'target_black';
        if($actual < 0 ){
            $actual_colors = 'target_black';
        }
        elseif ($actual >=  ($target + 2)){
            $actual_colors = 'target_red';
        }
        elseif ($actual >= ($target - 0.9) && $actual <= ($target + 0.9)){
            $actual_colors = 'target_green';
        }
        elseif ($actual <=  ($target - 1) ){
            $actual_colors = 'target_blue';
        }

        return $actual_colors ;
    }




    public static $stanlib_editable_fields = [
        'do_not_process_salary',
        'proposedsalary_percentage',
        'proposedsalary_value',
        'proposedincentive_percentage',
        'proposedincentive_value',
        'assessment_score',
        'allocation',
    ];
    public static $group_headers = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'calc_prorata',
        'maternity',
        'jobgrade',
        'milestone',
        'current_rating',
        'salary_value',
        'calc_salary_zar',
        'calc_suggested_salary',
        'proposedsalary_percentage',
        'proposedsalary_value',
        'calc_proposedsalary_zar',
        'incentive_scheme',
        'calc_suggested_incentive',
        'proposedincentive_percentage',
        'proposedincentive_value',
        'h_1_incentive_percentage',
        'h_1_incentive_value',
        'calc_proposedincentive_zar',
        'financial_incentive_value',
        'allocation',
        'calc_on_target_achievement',
        'assessment_score',
        'calc_kpi_incentive_value',
        'calc_financial',
        'calc_non_financial',
        'jobtitle',
        'linemanager',
        'do_not_process_salary',
        'h_1_performance1',
        'h_2_performance1',
        'costcentre',
        'orglevel3',
        'orglevel4',
        'orglevel5',
        'orglevel6',
        'orglevel7',
        'orglevel8',
        'calc_years_of_service_val',
        'gender',
        'race',
        'h_1_salary_value',
        'h_1_proposedsalary_percentage',
        'h_1_proposedsalary_value',
        'salary_continuation_plan',
        'calc_allocationmanager1name',
        'calc_allocationmanager2name',
        'calc_businessmanager1name',
        'calc_phaseclustermanager1name',
        'calc_hrbp1name',
        'calc_hrbp2name',
        'calc_hrbp3name',
        'calc_hrbp4name',
        'calc_hrbp5name',
        'comments'
    );
    public static $stanlib_headers = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'calc_prorata',
        'maternity',
        'current_rating',
        'h_1_performance1',
        'h_2_performance1',
        'jobgrade',
        'milestone',
        'jobtitle',
        'salary_value',
        'proposedsalary_currency',
        'calc_salary_zar',
        'calc_suggested_salary',
        'proposedsalary_value',
        'proposedsalary_percentage',
        'incentive_scheme',
        'incentive_cap',
        'calc_suggested_incentive',
        'proposedincentive_percentage',
        'proposedincentive_value',
        'internal_jobscale_code',
        'calc_internal_jobscale_name',
        'calc_internal_jobscale_indicator',
        'calc_intbench_salary_median',
        'calc_intbench_bonus_median',
        'calc_compa_ratio_salary_pre',
        'calc_compa_ratio_salary_post',
        'calc_compa_ratio_incentive',
        'calc_years_of_service_val',
        'gender',
        'race',
        'linemanager',
        'costcentre',
        'orglevel3',
        'orglevel4',
        'orglevel5',
        'orglevel6',
        'orglevel7',
        'orglevel8',
        'h_1_salary_value',
        'h_1_proposedsalary_percentage',
        'h_1_proposedsalary_value',
        'h_1_incentive_percentage',
        'h_1_incentive_value',
        'salary_continuation_plan',
        'do_not_process_salary',
        'comments',
        'calc_allocationmanager1name',
        'calc_allocationmanager2name',
        'calc_businessmanager1name',
        'calc_phaseclustermanager1name',
        'calc_view1name',
        'calc_view2name',
        'calc_hrbp1name',
        'calc_hrbp2name',
        'calc_hrbp3name',
        'calc_hrbp4name',
        'calc_hrbp5name'
    );
    public static $africa_headers = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'calc_prorata',
        'current_rating',
        'h_1_performance1',
        'salary_value',
        'proposedsalary_percentage',
        'proposedsalary_value',
        'incentive_scheme',
        'proposedincentive_percentage',
        'proposedincentive_value',
        'h_1_incentive_percentage',
        'h_1_incentive_value',
        'h_1_proposedsalary_percentage',
        'h_1_proposedsalary_value',
        'jobtitle',
        'gender',
        'linemanager',
        'orglevel3',
        'orglevel5',
        'orglevel6',
        'orglevel7',
        'orglevel8',
        'h_1_salary_value',
        'do_not_process_salary',
        'maternity',
        'calc_allocationmanager1name',
        'calc_allocationmanager2name',
        'calc_businessmanager1name',
        'calc_phaseclustermanager1name',
        'calc_view1name',
        'calc_view2name',
        'calc_hrbp1name',
        'calc_hrbp2name'
    );

    public static $salary_headers = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'calc_prorata',
        'maternity',
        'jobgrade',
        'milestone',
        'current_rating',
        'salary_value',
        'calc_salary_zar',
        'calc_suggested_salary',
        'calc_proposedsalary_zar',
        'proposedsalary_percentage',
        'proposedsalary_value',
        'incentive_scheme',
        'h_1_incentive_percentage',
        'h_1_incentive_value',
        'actuary_indicator',
        'h_1_salary_value',
        'h_1_proposedsalary_percentage',
        'h_1_proposedsalary_value'
    );
    public static $generalIncentive_headers = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'calc_prorata',
        'maternity',
        'current_rating',
        'salary_value',
        'calc_suggested_incentive',
        'proposedincentive_percentage',
        'proposedincentive_value',
        'h_1_incentive_percentage',
        'h_1_incentive_value'
    );
    public static $riskIncentive_headers = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'calc_prorata',
        'maternity',
        'current_rating',
        'salary_value',
        'incentive_scheme',
        'proposedincentive_percentage',
        'proposedincentive_value',
        'h_1_incentive_percentage',
        'h_1_incentive_value',
        'financial_incentive_value',
        'allocation',
        'calc_on_target_achievement',
        'calc_financial',
        'calc_non_financial'
    );
    public static $specialistIncentive_headers = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'calc_prorata',
        'maternity',
        'current_rating',
        'salary_value',
        'proposedincentive_percentage',
        'proposedincentive_value',
        'h_1_incentive_percentage',
        'h_1_incentive_value',
        'financial_incentive_value',
        'assessment_score',
        'calc_kpi_incentive_value',
        'calc_financial',
        'calc_non_financial'
    );
    public static $otherIncentive_headers = array(
        'personnelnumber',
        'lastname',
        'knownas',
        'calc_prorata',
        'maternity',
        'jobgrade',
        'milestone',
        'current_rating',
        'salary_value',
        'calc_salary_zar',
        'proposedsalary_value',
        'calc_proposedsalary_zar',
        'incentive_scheme',
        'proposedincentive_percentage',
        'calc_proposedincentive_zar',
        'proposedincentive_value',
        'h_1_incentive_percentage',
        'h_1_incentive_value'
    );

    public static $group_editable_fields = [
        'do_not_process_salary',
        'proposedsalary_percentage',
        'proposedsalary_value',
        'proposedincentive_percentage',
        'proposedincentive_value',
        'assessment_score',
        'allocation',
        'comments',
    ];
    public static $africa_editable_fields = [
        'do_not_process_salary',
        'proposedsalary_percentage',
        'proposedsalary_value',
        'proposedincentive_percentage',
        'proposedincentive_value',
        'assessment_score',
        'allocation',
    ];

}

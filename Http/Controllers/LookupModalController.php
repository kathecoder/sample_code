<?php

namespace App\Http\Controllers;

use App\CFLibrary\FilterHandler;
use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LookupModalController extends Controller
{ 
    
   public function __construct()
   {

   }
   
   public function getCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        $table_name = '';
        if(isset($request['data'])){
            $data = $request['data'];
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];
                
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }else{
                        $id = '';
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        $form_submit_data = array();
        if($mode == 'save'){
            if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                        //print_r($request['data']);
                        $item_field = '';
                        $dat_split = explode('_',$key);
                        $count_split = count($dat_split);
                        for($j=0;$j<($count_split - 1);$j++){
                            $item_field .= $dat_split[$j].'_';
                        }
                        $item_field = substr($item_field,0,(strlen($item_field) - 1));
                        $item_value = $dat;
                        $form_submit_data[$item_field] = $item_value;
                        if($item_field == 'id'){
                            $selections['field_name'] = $item_field;
                            $selections['field_value'] = $item_value;
                        }
                        $i++;
                }
            }
        }
        
        if($mode == "new"){
            $id = 0;
        }
        
        if(isset($table_name) && $table_name != ''){
            $panel = new TableViewerPanel($table_name, '', $mode, $selections, $form_submit_data, $table_name);
            $table = $panel->show();
            $card_template = $table['content'];
        }else{
            $card_template = '';
        }
                
        Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }
    
    public static function getTablePanel($mode,$selections,$data,$table_id_name,$field_name,$table_name){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $id = '';
        $form_submit_data = array();
        $where = array();
        $lookup_table = '';
        
        $table_name = self::getLookupTableName($field_name);
                
        if(isset($table_name) && $table_name != ''){
            $panel = new TableViewerPanel($table_name, '', $mode, $selections, $form_submit_data, $table_name,'vertical',$where,$table_id_name,true);
            $table = $panel->show();
            $lookup_table = $table['content'];
        }
        
        return $lookup_table;
    }
    
     public static function getLookupTableName($field_name){
         $table_name = '';
         
         if(isset($field_name) && $field_name != '' && $field_name != NULL){
            
            if(strpos($field_name, 'org_structure') !== false){
                $table_name = 'vw_org_structure';
            }
            if(strpos($field_name, 'currency') !== false){
                $table_name = 'currencies';
            }
            if(strpos($field_name, 'scheme') !== false){
                $table_name = 'incentive_schemes';
            }
            if(strpos($field_name, 'participation') !== false){
                $table_name = 'participation_categories';
            }
            if(strpos($field_name, 'benchmark') !== false){
                $table_name = 'benchmarks';
            }
        }
         
         return $table_name;
     }
     
    public static function getLookupTableManager(){
        $html = '';
        
        
        return $html;
    }

}   

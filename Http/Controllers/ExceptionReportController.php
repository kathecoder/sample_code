<?php

namespace App\Http\Controllers;

use App\CFLibrary\TableViewerPanel;
use App\Models\AppExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\ExceptionReport;
use App\Models\UserCycle;
use App\Models\Cycle;

class ExceptionReportController extends Controller
{

    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }
   

    public function create(Request $request)
    {

        return true;
    }

    public function getCardPanel(Request $request){

        $table_post = $request['table'];
        $ids = UserRolesController::getEmployeeIDsPerManager($table_post);

        Session::put('custom_filtered_ids', $ids);

        logger("returned ids ", $ids);

        /* This will handle the data you sent from react - each page on react will be differnet
         * It does not form part of framework and will be created specifically for project
         * You will thus have complete control of the elements you pass to framework, just ensure that you have these
         * variables set when using the datatable to connect with form of framework
         */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();

        if(isset($request['data'])){
            $data = $request['data'];

            $i = 0;
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
            $id = $request['id'];
            $selections['field_name'] = 'id';
            $selections['field_value'] = $id;
            $mode = $request['mode'];
        }

        $form_submit_data = array();
        if($mode == 'save'){
            if(isset($request['data'])){
                $data = $request['data'];
                $i = 0;
                foreach($data as $key=>$dat){
                    //print_r($request['data']);
                    $item_field = '';
                    $dat_split = explode('_',$key);
                    $count_split = count($dat_split);
                    for($j=0;$j<($count_split - 1);$j++){
                        $item_field .= $dat_split[$j].'_';
                    }
                    $item_field = substr($item_field,0,(strlen($item_field) - 1));
                    $item_value = $dat;
                    $form_submit_data[$item_field] = $item_value;
                    if($item_field == 'id'){
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                    }
                    $i++;
                }
            }
        }

        /*SET PERMISSION PER INSTANCE,PHASE(IF ANY),USER(IF ANY) etc.*/
        $instance_id = NULL;
        $instance_name = '';

        $user_instance = PermissionController::getUserInstance();
        if($user_instance['message'] == 'Success'){
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }

        // $user_roles = PermissionController::getRolesMapping();
        $where[]['instance_indicator_id'] = $instance_id;
        $where[]['id'] = $ids;
        //print_r($where);exit;
        $view_type = 'vertical';
        if($mode == 'edit_hor'){
            $mode = 'edit';
            $view_type = 'horizontal';
        }

        if($mode == "new"){
            $id = 0;
        }
        elseif ($mode == "export"){
            $id = 0;
            $res = AppExport::storeExport('employees','employees', $where);

            if ($res['error'] === false){

                $export_res = $res['response'];
                $data['file_path'] = $export_res['file_path'];
                $data['mode'] = $mode;
                return array
                (
                    'error'     => false,
                    'response'  => true,
                    'message'   => 'Success',
                    'template'  => '',
                    'data'      => $data,
                    'mode'      => $mode,
                    'id'        => $id,
                );
            }
            else{
                return $res;
            }

        }

        $panel = new TableViewerPanel($table_post, 'X', $mode, $selections, $form_submit_data, 'employees',$view_type, $where);

        $table          = $panel->show();
        $card_template  = $table['content'];
        Session::forget('custom_filtered_ids');
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }


    public function getExceptionReport(Request $request){
        $res    = ExceptionReport::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'],true,$request['link']);
        $total    = ExceptionReport::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'],false,$request['link']);
          $array = [];
         foreach ($res as $data){
            if($data->proposedsalary_percentage == null){
                $data->proposedsalary_percentage = 0;
            }
            if($data->proposedincentive_percentage == null){
                $data->proposedincentive_percentage = 0;
            }

            if($data->scheme_id == 20 || $data->scheme_id ==23 || $data->scheme_id ==43){
                $data->proposedincentive_percentage ='';
 
            }else{
                $data->proposedincentive_percentage =number_format($data->proposedincentive_percentage,1) .' %';
                $data->salary_value =number_format($data->salary_value,2); 
            }
            $array[]=array(
                'id'=>$data->id,
                'personnelnumber'=>$data->personnelnumber,
                'lastname'=>$data->lastname,
                'knownas'=>$data->knownas,
                'country'=>$data->orglevel3,
                'company'=>$data->orglevel4,
                'business_area'=>$data->orglevel5,
                'business_unit'=>$data->orglevel6,
                'division'=>$data->orglevel7,
                'department'=>$data->orglevel8,
                'current_rating'=>$data->current_rating,
                'salary_value'=> $data->salary_value,
                'proposedsalary_percentage'=> number_format($data->proposedsalary_percentage,1) .' %',// $data->proposedsalary_percentage,
                'proposedincentive_percentage'=>$data->proposedincentive_percentage,
             );
        }

        return [
            'error'=>false,
            'avg_proposedsalary_percentage'=>ExceptionReport::getProposedSalaryAverage()[0]->avg_proposedsalary_percentage,
            'response'=>[
                'data'=>$array,
                'pageNate'=>$this->pageNationController->index(count($total),$request->state['take']),
                'totalTableData'=>count($total),
            ],
            'message'=>'Exception data Loaded'
        ];
    }

}

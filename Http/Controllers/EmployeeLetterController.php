<?php

namespace App\Http\Controllers;

use App\Models\EmployeeLetter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EmployeeLetterController extends Controller
{

    public static function runScript(){
        $response = EmployeeLetter::script();
    }


    public function downloadArchiveLetters($file_path){
        ini_set('memory_limit',-1);
        try{
            return response()->download(storage_path('app' . '/public/ArchiveLetters/TestDumps/'. $file_path . '.zip'));
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use App\User;

class SAMLController extends Controller
{
    public function login()
    {
        return \Auth::guest() ? redirect('saml2/login') : \Redirect::intended('/');
    }

    public function logout()
    {
        //recover sessionIndex and nameId from session
        $sessionIndex = session()->get('sessionIndex');
        $nameId = session()->get('nameId');

        //get the logout route from saml2 config
        $returnTo = config('saml2_settings.logoutRoute');

        //pass parameters into the url
        return redirect()->route('saml_logout', [
            'returnTo'=>$returnTo,
            'nameId'=>$nameId,
            'sessionIndex'=>$sessionIndex
        ]);
    }

    public function loggedin()
    {
        return redirect('/');
    }

    public function samlACS(Request $request) {
        try{
            //logger($request);
            $xmlPayload = base64_decode($request->SAMLResponse);
            $user_arr = [];
            //logger($xmlPayload);
            // Load the XML document
            $doc = new \DOMDocument();
            $doc->loadXML($xmlPayload);
            //print_r($doc);
            //logger(__LINE__);
            // Traverse User elements
            foreach ($doc->getElementsByTagName('Attribute') as $attribute) {
                $user_arr[$attribute->getAttribute('Name')] = $attribute->nodeValue;
            }

            //logger(__LINE__);
            if (count($user_arr) > 0)
            Log::info($user_arr);
            //logger(__LINE__);

            $user = null;
            if (isset($user_arr['emailaddress']))
                $user = User::where('email', $user_arr['emailaddress'])->where('active', 1)->first();
            elseif (isset($user_arr['email']))
                $user = User::where('email', $user_arr['email'])->where('active', 1)->first();


            if($user){
                if($user->session_id != '')
                {
                    $last_session = Session::getHandler()->read($user->session_id);
                    if ($last_session)
                    {
                        Session::getHandler()->destroy($user->session_id);
                    }
                }
                //logger(__LINE__);
                Auth::login($user, true);
                $user->date_last_login = now();
                $user->save();
                //logger(__LINE__);
                return redirect('/');
            }
            else{

                Log::debug("reporting from: Authentication failed - in " .__CLASS__);
                return redirect()->route('signin');
            }

        }
        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return redirect()->route('signin')/*->with('error', 'Something went wrong please contact administrator.')*/;
        }
    }
}
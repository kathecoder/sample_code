<?php

namespace App\Http\Controllers;

use App\Mail\OTPMailable;
use App\Models\AppAssist;
use App\Models\WhiteListedEmails;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\Self_;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        Session::forget('isns-session-employee-id');
        $email              = $request->get ( 'username' );
        $whitelisted  = WhiteListedEmails::where('email',$email)->first();

        if ($whitelisted !== null){
            return $this->whiteListLogin($request);
        }

        if (Auth::attempt ( array (
            'email'     => $request->get ( 'username' ),
            'password'  => $request->get ( 'password' )
        ) ))
        {
            if (auth::user()->active !== 1){
                return array
                (
                    'error'             => true,
                    'response'          => false,
                    'is_whitelisted'    => false,
                    'message'           => 'User account is not active'
                );
            }

            session ( [
                'user_id' => auth::user()->id
            ] );

            self::storeUserOTP($request->get ( 'username' ), $request->get ( 'password' ));

            Log::info("Authenticated");
            $data = UserRolesController::getUserRoleData();
            Auth::logout();
            return array
            (
                'error'             => false,
                'response'          => true,
                'is_whitelisted'    => false,
                'data'              => $data,
                'message'           => 'Authenticated'
            );
        }
        else {
            return array
            (
                'error'             => false,
                'response'          => false,
                'is_whitelisted'    => false,
                'message'           => 'Email and Password do not match'
            );
        }

    }

    public function logout(Request $request)
    {
        Session::flush ();
        Auth::logout ();

        return redirect()->back();
        return array ('error'=>false,'response'=>true,'message'=>'Logged out');

    }

    public function validateOTP(Request $request)
    {
        logger('check if otp has expired');
        //check if otp has expired
        if(Session::has('otp_time')){
            $check  = self::checkOTPExpiryTime();
            if($check){
                return array
                ('error'=>true, 'response'=>false,  'data'=>[], 'message'=>'Your OTP has expired. Please login again.' );
            }
        }

        try {
            $email      = Session::has('email') ? decrypt(Session::get('email')) : null;
            $password   = Session::has('pass') ? decrypt(Session::get('pass')) : null;
            $user       = User::where('otp', $request->input('otp'))->where('email', '=', $email)->first();

            if ($user === null){

                if(!$this->countOTP()){
                    return array
                    ('error'=>true, 'response'=>false, 'data'=>[], 'message'=>'Too many OTP attempts. Please login again.');

                }
                return array
                ('error'=>true, 'response'=>false, 'data'=>[], 'message'=>'We do not have this OTP in our records!!');
            }

            if (auth()->guard('web')->attempt(['email' => $user->email, 'password' => $password]))
            {
                $new_sessid = Session::getId();

                if($user->session_id != '')
                {
                    $last_session = Session::getHandler()->read($user->session_id);
                    if ($last_session)
                    {
                        Session::getHandler()->destroy($user->session_id);
                    }
                }

                DB::table('users')->where('id', $user->id)->update(['session_id' => $new_sessid, 'date_last_login'=>Carbon::now()]);

                $user = auth()->guard('web')->user();
                $user->otp = null;
                $user->date_last_login = now();
                $user->save();

                Log::info("OTP Validated");
//                $employees = $user->getEmployees();


                return array
                (
                    'error'             => false,
                    'response'          => true,
                    'employees'         => [],
                    'is_whitelisted'    => false,
                    'names'             => explode(" ",$user->name),
                    'data'              => UserRolesController::getUserRoleData(),
                    'message'           => 'OTP Validated Successfully.'
                );

            }

        }
        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public static function storeUserOTP($email, $pass){

        $user = User::where('email', $email)->first();
        $otp = AppAssist::generateRandomString(10);
        $user->otp = $otp;
        $user->save();
        $email = $user->email;
            session ( [
                'otp_time'=> date("Y-m-d h:i:sa"),
                'otp_count'=> 0,
            ] );

        $sendTo = strtolower($email);

        /*if ($sendTo == 'heerachund829@test.com')
            $sendTo = 'sam.heerachund@liberty.co.za';

        else  if ($sendTo == 'dewee827@test.com')
            $sendTo = 'essaivani.pillay@liberty.co.za';
            //$sendTo = 'maxwell.mphioe@conciseflow.co.za';

        else  if ($sendTo == 'green3987@test.com')
            $sendTo = 'tsholofelo.monaisa@liberty.co.za';

        else  if ($sendTo == 'kruger5178@test.com')
            $sendTo = 'mia.theron@conciseflow.co.za';

        else  if ($sendTo == 'Ramsamoojh313@test.com')
            $sendTo = 'mia.theron@conciseflow.co.za';*/

        if(env('APP_ENV')==='local'){
            $sendTo = env('DEV_EMAIL');
        }else if(env('APP_ENV')==='uat'){
            $sendTo = env('UAT_EMAIL');
        }else{//Prodution
            //$sendTo = strtolower($email);
            $sendTo = env('UAT_EMAIL');
        }

        Log::info("Email has been sent to ".$sendTo);
        //$sendTo = "mia.theron@conciseflow.co.za";
            Mail::to($sendTo)->send(new OTPMailable($user));

        Session::put('pass', encrypt($pass));
        Session::put('email', encrypt($email));

    }

    public static function checkOTPExpiryTime(){
        $otp_time = Session::get('otp_time');
        $otp_time = date('Y-m-d h:i:sa', strtotime('+360 seconds', strtotime($otp_time)));
        $otp_time = strtotime($otp_time);
        $current_time = date('Y-m-d h:i:sa');
        $current_time = strtotime($current_time);

        return $otp_time < $current_time;
    }

    public function countOTP(){
        $continue = true;
        if(Session::has('otp_count')){
            $otp_count = Session::get('otp_count');
            $otp_count++;

            if($otp_count >= 3){
                return $continue = false;
            }
            Session::put('otp_count', $otp_count);
        }
        return $continue;
    }

    public function whiteListLogin(Request $request){
        $email = $request->get ( 'username' );
        $pass =  $request->get ( 'password' );

        if(Auth::validate([ 'email'=>$email, 'password'=>$pass])){
            $user = User::where('email', $email)->first();
            /* for whitelisted emails*/
            if (auth()->guard('web')->attempt(['email' => $user->email, 'password' => $pass]))
            {
                $new_sessid = Session::getId();
                if($user->session_id != '')
                {
                    $last_session = Session::getHandler()->read($user->session_id);
                    if ($last_session)
                    {
                        Session::getHandler()->destroy($user->session_id);
                    }
                }

                DB::table('users')->where('id', $user->id)->update(['session_id' => $new_sessid]);

                $user = auth()->guard('web')->user();

                if ($user->active == 1){
                    $user->otp = null;
                    $user->date_last_login = now();
                    $user->update();

                    return array
                    (
                        'error'             => false,
                        'response'          => true,
                        'is_whitelisted'    => true,
                        'employees'         => [],
                        'message'           => "Authenticated Successfully.",
                        'names'             => explode(" ",$user->name),
                        'data'              => UserRolesController::getUserRoleData(),
                    );
                    //return redirect('/');
                }
                else{
                    // Log her out
                    Auth::logout();

                    return array
                    (
                        'error'             => false,
                        'response'          => false,
                        'is_whitelisted'    => false,
                        'message'           => 'Email and Password do not match'
                    );
                }

            }
        }
        else {
            return array
            (
                'error'             => false,
                'response'          => false,
                'is_whitelisted'    => false,
                'message'           => 'Email and Password do not match'
            );
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Cycle;
use App\Models\Employee;
use App\Models\Role;
use App\Models\User;
use App\Models\UserCycle;
use App\Models\AppExport;
use Illuminate\Http\Request;
use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    
    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }
    
    public function getCurrentUserNames(){
        try {
            $res = User::find(Auth::user()->id);
            if($res->id > 0){
                return array('error'=>false,'response'=>$res->name,'message'=>'Success');

            }else{
                $msg = 'No Names';
                Log::critical($msg);
                return array('error'=>false,'response'=>[],'message'=>$msg);
            }
        }catch (\Exception $e){
            $msg = 'Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }
    
    public function getUsersCardPanel(Request $request)
    {

        /* This will handle the data you sent from react - each page on react will be differnet
               * It does not form part of framework and will be created specifically for project
               * You will thus have complete control of the elements you pass to framework, just ensure that you have these
               * variables set when using the datatable to connect with form of framework
               */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        if(isset($request['data'])){
            $data = $request['data'];
            
            $i = 0;
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];               
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }
                }                 
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        $form_submit_data = array();
        if($mode == 'save'){
            if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                    //print_r($request['data']);
                    $item_field = '';
                    $dat_split = explode('_',$key);
                    $count_split = count($dat_split);
                    for($j=0;$j<($count_split - 1);$j++){
                        $item_field .= $dat_split[$j].'_';
                    }
                    $item_field = substr($item_field,0,(strlen($item_field) - 1));
                    $item_value = $dat;
                    $form_submit_data[$item_field] = $item_value;
                    if($item_field == 'id'){
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                    }
                    $i++;
                }
            }
            //print_r($form_submit_data);
        }
        
        $view_type = 'vertical';
        $where = [];
        // logger(FilterController::getFilteredUsers());
        $panel = new TableViewerPanel('users', 'CRX', $mode, $selections, $form_submit_data, 'users',$view_type, $where);
        $table = $panel->show();
        $card_template = $table['content'];
        if($mode == "new"){
            $id = 0;
        }

        elseif ($mode == "export"){
            $id = 0;
            $res = AppExport::storeExport('users', null);

            if ($res['error'] === false){

                $export_res = $res['response'];
                $data['file_path'] = $export_res['file_path'];
                $data['mode'] = $mode;
                return array
                (
                    'error'     => false,
                    'response'  => true,
                    'message'   => 'Success',
                    'template'  => '',
                    'data'      => $data,
                    'mode'      => $mode,
                    'id'        => $id,
                );
            }
            else{
                return $res;
            }

        }
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );

    }
    
    public function getAllUsers(Request $request)
    {
        try {
            $res = User::allUsers($request->state['searchInput'], $request->state['skip'], $request->state['take'], true);
            $total = User::allUsers($request->state['searchInput'], $request->state['skip'], $request->state['take'], false);

            $array = array();
            foreach ($res as $data){
                $array[]=array(
                    'id'=>$data->id,
                    'name'=>$data->name,
                    'email'=>$data->email,
                    'admin'=>$data->is_admin === 1 ? true : false,
                    'active'=>$data->active === 1 ? true : false,
                );
            }
            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>PageNationController::getPages(count($total),$request->state['take']),
                    'totalTableData'=>count($total)
                ),
                'message'=>'Users Loaded');
        }catch (\Exception $e){
            $msg = 'Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>"Could not load users");
        }
    }

    public function crudUser(Request $request)
    {
        try {
            if ($request->id > 0){
                $user_current   =  User::find($request->id);
                $user1          = User::where('email', $request->state['email'])->first();
                $user2          = User::where('personnelnumber', $request->state['personnelnumber'])->first();
                $active_instance = PermissionController::getUserInstance()['response'];

                if (($user1 !== null &&  $user_current !== null) && ($user1->id !== $user_current->id)){

                    if ($user1->email !== $user_current->email){
                        return array('error'=>true,'response'=>[],'message'=>"Email address already exists.");
                    }
                }

                if (($user2 !== null &&  $user_current !== null) && ($user2->id !== $user_current->id) &&  ($user2->id !== $user_current->id)){
                    return array('error'=>true,'response'=>[],'message'=>"Personnel Number already exists");
                }

                $user =  User::find($request->id);
                $user->name     = $request->state['name'];
                $user->email    = $request->state['email'];
                if (isset($request->state['password']) && !empty($request->state['password']) )
                    $user->password    =  Hash::make($request->state['password']) ;
                $user->accounttype = $request->state['admin'] === null ? 4 : $request->state['admin'];
                $user->instance_indicator_id = $active_instance['instance'];
                $user->admin    = $request->state['admin'] === null ? false : true;
                $user->active   = $request->state['active'] === null ? false : true;;
                $user->save();

                // ------------- update employee email
                $employee = Employee::where('personnelnumber', $user_current->personnelnumber)->where('emailaddress', $user_current->email)->first();

                if ($employee){
                    $employee->emailaddress = $user->email;
                    $employee->save();
                }

            }
            else {
                $active_instance = PermissionController::getUserInstance()['response'];

                $user = User::where('email', $request->state['email'])->first();
                if ($user){
                    return array('error'=>true,'response'=>[],'message'=>"Email address already exists.");
                }
                $user = User::where('personnelnumber', $request->state['personnelnumber'])->first();
                if ($user){
                    return array('error'=>true,'response'=>[],'message'=>"Personnel Number already exists");
                }

                if ($user === null){
                    $user = new User();
                    $user->name     = $request->state['name'];
                    $user->email    = $request->state['email'];
                    $user->admin    = $request->state['admin'];
                    $user->active   = $request->state['active'];
                    $user->accounttype = $request->state['admin'] === null ? 4 : $request->state['admin'];
                    $user->instance_indicator_id = $active_instance['instance'];
                    $user->personnelnumber   = $request->state['personnelnumber'];
                    $user->password = Hash::make($request->state['password']);
                    $user->save();
                }

            }

            return array (
                'error'=>false,
                'response'=>[],
                'message'=>'User details saved successfully.');
        }catch (\Exception $e){
            $msg = 'Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>"Could not Store users");
        }
    }

    public function getUser(Request $request)
    {
        try {
            $res            =   User::find($request->id);

            return array (
                'error'=>false,
                               'response'=>array(
                    'name'=>$res->name,
                    'email'=>$res->email,
                    'admin'=>$res->is_admin,
                    'active'=>$res->active,
                    'personnelnumber'=>$res->personnelnumber,
                    'getUser'=>$res->getUser,
                ),
                'message'=>'Succcess'
            );

        }catch (\Exception $e){
            $msg = 'Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>"Could not get user data");
        }
    }
}

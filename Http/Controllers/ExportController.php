<?php

namespace App\Http\Controllers;

use App\CFLibrary\FilterHandler;
use App\Models\AppExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    /** Todo: update the function to work Export Management Page (To be added later)
     * The function should receive post request from the "Export Management Page - or whatever we decide to call it"
     * The "Export Management Page" should post  $table_name and $exclude_fields, amongst other fields
     * @param $table_name
     * @return array|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadExport($file_path){
        ini_set('memory_limit',-1);
        try{
            return response()->download(storage_path('app' . base64_decode($file_path)));
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public function getExport(Request $request){
        ini_set('memory_limit','-1');
        $where['my_page'] = isset($request['page']) ? $request['page']: 'personnel_admin';
        $where['status_id'] = $request->status_id;
        $res = AppExport:: storeExport('employees',$where,'employees');

        if ($res['error'] === false){

            $export_res = $res['response'];
            $data['file_path'] = $export_res['file_path'];
            $data['mode'] = '';
            return array
            (
                'error'     => false,
                'response'  => true,
                'message'   => 'Success',
                'template'  => '',
                'data'      => $data,
                'mode'      => '',
                'id'        => '',
            );
        }

    }

}

<?php

namespace App\Http\Controllers;

use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use App\Models\AppExport;
use App\Models\BusinessArea;
use App\Models\BusinessUnit;
use App\Models\Cycle;
use App\Models\Division;
use App\Models\Files;
use App\Models\Filters;
use App\Models\IssueExtraDetails;
use App\Models\IssueExtraDetailsFromUser;
use App\Models\IssueLog;
use App\Models\issueLogsCategory;
use App\Models\issueLogsSubCategory;
use App\Models\ParticipationCategory;
use App\Models\Scheme;
use App\Models\TargetBusinessUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class IssueLogController extends Controller
{
    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }
   
   public function create(Request $request)
   {
             
        return true;
   }

   public function getCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        if(isset($request['data'])){
            $data = $request['data'];
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];
                
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }else{
                        $id = '';
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }

       if($mode == "new"){
           $id = 0;
       }
       elseif ($mode == "export"){
           $id = 0;
           $res = AppExport::storeExport('issue_logs',null);

           if ($res['error'] === false){

               $export_res = $res['response'];
               $data['file_path'] = $export_res['file_path'];
               $data['mode'] = $mode;
               return array
               (
                   'error'     => false,
                   'response'  => true,
                   'message'   => 'Success',
                   'template'  => '',
                   'data'      => $data,
                   'mode'      => $mode,
                   'id'        => $id,
               );
           }
           else{
               return $res;
           }

       }
        $panel = new TableViewerPanel('issue_logs', 'CRUX', $mode, $selections);
        $table = $panel->show();
        $card_template = $table['content'];
        // Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }



    public function createUpdateIssueLog(Request $request)
    {
        try {
            $res = IssueLog::createUpdateIssueLogWithExtraDetails($request->state);
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not create or update issue');
        }
    }
    public function resolveIssueLog(Request $request){
        try {
            $res = IssueLog::resolveIssueLog($request->id, $request->comment, $request->status);
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            // Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not update');
        }
    }
    public function getIssueLog(Request $request){
        try {
            $res = IssueLog::find($request->id);
            $file = url('/').'/storage/'.Files::getFile($res->id, 'issue_logs');
            if(Files::getFile($res->id, 'issue_logs')===false){
                $file = false;
            }
            return array (
                'error'=>false,
                'response'=>$res,
                'disable_form'=>true,
                'can_resolve'=>true,
                'file_path'=>$file,
                'message'=>'Issue Log Loaded');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get issue logs');
        }
    }
    public function getIssueLogs(Request $request)
    {


        try {
            //logger($request);
            $status = false;
            if($request->status>0){
                $status = $request -> status;
            }
            $clause = '';
            if ($request['exclude_close'] === true){
                $clause = $clause." AND issue_logs.status_id!=11";
            }

            if(Auth::user()->admin===0){
                $clause = $clause." AND users.id=".Auth::user()->id;
            }

            // Log::alert(Auth::user()->admin);
            if($request->state['take']===null){
                Log::warning("The value is null: ".$request->state['take']);
            }
            $res = IssueLog::getFilteredIssueLogs($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $status, $clause,$request['startDate'],$request['endDate'],$request['selected']);
            
            $total = IssueLog::getFilteredIssueLogs($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $status, $clause,$request['startDate'],$request['endDate'],$request['selected']);
            $totales = IssueLog::getIssueLogsDash();


            $array = [];


            $count_notStarted = 0 ;
            $count_closed = 0 ;
            $count_progress = 0 ;
            $count_all = 0;

            foreach ($totales as $data) {
                
                if($data->status_name == "Not Started"){
                    $count_notStarted += 1;
                }
                if($data->status_name == "Closed"){
                    $count_closed += 1;
                }
                if($data->status_name == "In Progress"){
                    $count_progress += 1;
                }

                    $count_all += 1 ;
                
            }

            foreach ($res as $data){
                $array []= array(
                    'id'=>$data->id,
                    'name'=>$data->name,
                    'status_name'=>$data->status_name,
                    'is_file'=>Files::getFile($data->id, 'issue_logs')!==false ? true : false,
                    'file_path'=>url('/').'/storage/'.Files::getFile($data->id, 'issue_logs'),
                    'issue_sub_categories'=>$data->issue_sub_categories,
                    'date_opened'=>$data->date_opened,
                    'date_closed'=>$data->date_closed,
                    'subject'=>$data->subject,
                );



            }

            $Chartarray = array();

            if($count_notStarted == 0){
                $Chartarray[0]=array("label"=>'Not Started',"custome_value"=>0, "value"=>0);
            }else{
                $Chartarray[0]=array("label"=>'Not Started',"custome_value"=>$count_notStarted, "value"=>(round(($count_notStarted/$count_all)*100,1)));
            }
            if($count_progress == 0){
                $Chartarray[1]=array("label"=>'In Progress',"custome_value"=>0, "value"=>0);

            }else{
                $Chartarray[1]=array("label"=>'In Progress',"custome_value"=>$count_progress, "value"=>(round(($count_progress/$count_all)*100,1)));

            }
            if($count_closed == 0){
                $Chartarray[2]=array("label"=>'Closed',"custome_value"=>0, "value"=>0);

            }else{
                $Chartarray[2]=array("label"=>'Closed',"custome_value"=>$count_closed, "value"=>(round(($count_closed/$count_all)*100,1)));

            }






            // $Chartarray[1]=array("label"=>'On Progress',"custome_value"=>$count_progress, "value"=>(round(($count_progress/$count_all)*100,1)));


            $Chartdata = array(
                "chart"=> array(
                    "caption"=> "",
                    "subcaption"=> "",
                    "xaxisname"=> "",
                    "yaxisname"=> "",
                    "numbersuffix"=> "%",
                    "theme"=> "gammel",
                    "paletteColors" =>"#C80F3F,#DC8E18,#ADD581",
                    "bgColor"=> "#ffffff",
                ),
                'data'=> $Chartarray
            );
            // $array[1]=array("label"=>'On Progress',"custome_value"=>round(count($on_progress),2), "value"=>(round((count($on_progress)/$total)*100,2)));
            // $array[2]=array("label"=>'Closed',"custome_value"=>round(count($closed),2), "value"=>(round((count($closed)/$total)*100,2)));



            // Log::warning('Check this out');
            return array (
                'error'=>false,
                "chart"=> $Chartdata,
                'response'=>array(
                    'data'          =>$array,
                    'pageNate'      =>$this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>count($total),
                    'currentCycle'  =>Cycle::getLiveCycleID()
                ),
                'message'=>'Issue Log Loaded');






        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get issue logs');
        }
    }
    public function getAllIssueCategories(Request $request)
    {
        try {

            $res = issueLogsCategory::getAllCategories();
            return array ('error'=>false,'response'=>$res,'message'=>'Issue Log Categories Loaded');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get issue log categories');
        }
    }
    public function getIssueSubCategories(Request $request)
    {
        try {

            $res = issueLogsSubCategory::getIssueSubCategories($request->issue_sub_category_id);
            return array ('error'=>false,'response'=>$res,'message'=>'Issue Log Sub-Categories Loaded');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get issue log Sub-categories');
        }
    }
    public function getBusinessArea(Request $request)
    {
        try {
            //logger(__METHOD__);
            $res = BusinessArea::getALlBusinessArea('',0,100, false);
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get Business Area');
        }
    }
    public function getBusinessUnits(Request $request)
    {
        try {
            $res = BusinessUnit::getALlBusinessUnits('',0,100, false);
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get Business Units');
        }
    }
    public function getDivisions(Request $request)
    {
        try {
            $res = Division::getAllDivisions('',0,100, false);
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get Business Units');
        }
    }
    public function getALlSchemes(Request $request)
    {
        try {
            $res = Scheme::getALlSchemes('',0,100, false);
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get Schemes');
        }
    }
    public function getALlParticipationCategories(Request $request)
    {
        try {
            $res = ParticipationCategory::getAllParticipationCategory('',0,100, false, $request->scheme);
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get Schemes');
        }
    }
    public function getAllTargetBusiness(Request $request)
    {
        try {
            $res = TargetBusinessUnit::getTargetBusinessUnits('',0,100, false);
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get Schemes');
        }
    }
    public function getIssueLogExtraDetail(Request $request){
        try {
            $res = IssueExtraDetailsFromUser::getIssueLogExtraDetail($request->id, Filters::convertJsonIntoArray(IssueExtraDetails::where('issue_sub_category_id', $request->issue_sub)->get()));
            return array ('error'=>false,'response'=>$res,'message'=>'Success');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Could not get Schemes');
        }
    }

}

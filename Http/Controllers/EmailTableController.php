<?php

namespace App\Http\Controllers;

use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;
use App\Models\InstanceIndicator;
use App\User;
use App\Models\Cycle;
use App\Models\CyclePhases;
use App\Models\Email;
use App\Models\Employee;
use App\Models\EmployeeAssessor;
use App\Models\Phase;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Log;

class EmailTableController extends Controller
{
    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }
   
   public function create(Request $request)
   {
             
        return true;
   }

   public function getCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        if(isset($request['data'])){
            $data = $request['data'];
            
            $i = 0;
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];               
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }
                }                 
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        $form_submit_data = array();
        if($mode == 'save'){
            if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                        //print_r($request['data']);
                        $item_field = '';
                        $dat_split = explode('_',$key);
                        $count_split = count($dat_split);
                        for($j=0;$j<($count_split - 1);$j++){
                            $item_field .= $dat_split[$j].'_';
                        }
                        $item_field = substr($item_field,0,(strlen($item_field) - 1));
                        $item_value = $dat;
                        $form_submit_data[$item_field] = $item_value;
                        if($item_field == 'id'){
                            $selections['field_name'] = $item_field;
                            $selections['field_value'] = $item_value;
                        }
                        $i++;
                }
            }
            //print_r($form_submit_data);
        }
        
        /*SET PERMISSION PER INSTANCE,PHASE(IF ANY),USER(IF ANY) etc.*/
        $instance_id = NULL;
        $instance_name = '';
        
        $user_instance = PermissionController::getUserInstance();
        if($user_instance['message'] == 'Success'){
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }
        
        // $user_roles = PermissionController::getRolesMapping();
        $where[]['instance_id'] = $instance_id;
        //print_r($mode);exit;
        
        $panel = new TableViewerPanel('emails', 'CRU', $mode, $selections, $form_submit_data, 'emails','vertical', $where);
        $table = $panel->show();
        $card_template = $table['content'];
        
        if($mode == "new"){
            $id = 0;
        }
        
        Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }
    
    public function sendTemplateNotifications(Request $request){

        try {
            $email_id           = $request['record_id'];
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $cycle_id           = Cycle::getLiveCycleID();
            $phase_id           = Phase::getLivePhaseID();
            $user               = User::find(Auth::user()->id);
            

            // get email type
            $email = Email::find($email_id);
            
            if ($email){
                $sendTo = $user->emailaddress;
                $name   = $user->name;
                $tags   = array('%NAME%' => $name);
                MailController::sendNotification($email, $sendTo, $tags);
            }
            return array('error' => true, 'response' => [], 'message' => 'Emails Sent Successfully');
        }
        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => false, 'response' => [], 'message' => $msg);
        }
    }


    public function getEmails(Request $request){
        try{

            $res    = Email::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true, $request['status_id'], '');
            $total  = Email::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false, $request['status_id'], '');

            $array = [];
            foreach ($res as $item){
                $array[] = array(
                    'id'        =>$item->id,
                    'instance'  =>$item->instance,
                    'name'    =>$item->name,
                    'subject'               =>$item->subject,
                    'created_at'            =>$item->created_at,
                    'updated_at'            =>$item->updated_at,

                );
            }

            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>$this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>count($total),
                    'currentCycle'=>Cycle::getLiveCycleID()
                ),
                'message'=>'Emails Loaded Successfully');
        }
        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => false, 'response' => [], 'message' => $msg);
        }

    }

    public function loadEmail(Request $request){
        $email = Email::find($request['id']);
        $instance = InstanceIndicator::find($email->instance_id);
        logger($email);
        return array('error' => false, 'instance' => $instance, 'html_template' => $email->html_template,  'email' => $email, 'message' => "Loaded");

    }

    public function updateEmail(Request $request){

        $object = Email::find($request['id']);
        $object->subject = $request['subject'];
        $object->html_template = $request['html_template'];
        $object->save();
  /*      logger($request['html_template']);
       logger($object);*/
        return array('error' => false, 'response' => [], 'message' => "Save Success");
    }

}

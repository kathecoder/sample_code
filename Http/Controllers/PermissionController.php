<?php

namespace App\Http\Controllers;

use App\Models\DataField;
use App\Models\InstanceIndicator;
use App\CFLibrary\Model\UniPermission;
use App\Models\Permission;
use App\User;
use App\Models\Instance;
use App\Models\ManagerType;
use App\Models\ManagerTypeUser;
use App\Models\CyclePhases;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use App\Models\Cycle;
use App\Models\Phase;

class PermissionController extends Controller
{
    public static function getUserInstance(){
        ini_set('memory_limit','1600M');
        try {
            $check = Session::get('active_instance');

            if ($check === null){
                $user       = User::find(Auth::user()->id);
                $instance   = InstanceIndicator::find($user->instance_indicator_id);
                $res        = array(
                                'instance'=>$user->instance_indicator_id,
                                'instance_name'=>$instance->name,
                                );
                Session::put('active_instance', $res);
            }
            else{
                $res = Session::get('active_instance');
            }

            return array('error'=>false,'response'=>$res,'message'=>'Success');

        }catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }
    
    public static function getRolesMapping(){
        try {
            $roles = array();
            $role_map = array();
            
            $user = User::find(Auth::user()->id);
            
            $roles_types = ManagerType::all();
            foreach ($roles_types as $roles_types) {
                $roles[$roles_types->id]['code'] =  $roles_types->code;
            }
            
            $role_mappings = ManagerTypeUser::where('user_id', Auth::user()->id)->get();
            if(!empty($role_mappings)){
                foreach ($role_mappings as $map) {
                    $role_map['id'] =  $map->id;
                    $role_map['type_id'][$roles[$map->manager_type_id]['code']] =  $map->manager_type_id;
                    $role_map['user_id'] =  $map->user_id;
                    $role_map['type'][$roles[$map->manager_type_id]['code']] = 1;
                }   
            }else{
                $role_map['id'] =  1;
                $role_map['type_id']['is_admin'] =  2;
                $role_map['user_id'] =  Auth::user()->id;
                $role_map['type']['is_admin'] = 1;
            }

            return array('error'=>false,'response'=>$role_map,'message'=>'Success');

        }catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }
    
    public static function getUserPersonnelNumber(){

            $user = User::find(Auth::user()->id);
            return $user->personnelnumber;
    }
    
    static function getFormPermissions($table, $table_name)
    {
       $form_permission = array();
       
       $user = User::find(Auth::user()->id);
       
       $check = Session::get('active_instance');

            if ($check === null){
                $user       = User::find(Auth::user()->id);
                $instance   = InstanceIndicator::find($user->instance_indicator_id);
                $res        = array(
                                'instance'=>$user->instance_indicator_id,
                                'instance_name'=>$instance->name,
                                );
                Session::put('active_instance', $res);
            }
            else{
                $res = Session::get('active_instance');
            }
       
       $user_type = $user->accounttype;
       
       $user_instance = $res['instance'];
       
        //active phase
        $cycle_id        = Cycle::getLiveCycleID();
        $live_phase_id   = 0;

        $live_phase = CyclePhases::where('live', 1)->where('cycle_id', $cycle_id)->where('instance_id', $user_instance)->first();
        if ($live_phase){
            $live_phase_id = $live_phase->id;
            if($live_phase_id == 12){
                $live_phase_id = 2;
            }
            if($live_phase_id == 7){
                $live_phase_id = 2;
            }
            
            if($live_phase_id == 13){
                $live_phase_id = 3;
            }
            if($live_phase_id == 8){
                $live_phase_id = 3;
            }
        }else{
            $live_phase_id = 2;   
        }
        
        
        
        $roles = array();
        $role_map = array();
            
        $roles_types = ManagerType::all();
        foreach ($roles_types as $roles_types) {
            $roles[$roles_types->id]['code'] =  $roles_types->code;
        }
            
        $role_mappings = ManagerTypeUser::where('user_id', Auth::user()->id)->get();

        foreach ($role_mappings as $map) {
            $role_map['id'] =  $map->id;
            $role_map['type_id'][$roles[$map->manager_type_id]['code']] =  $map->manager_type_id;
            $role_map['user_id'] =  $map->user_id;
            $role_map['type'][$roles[$map->manager_type_id]['code']] = 1;
        }
            
        //print_r($role_map);   
        $user_role_type = $role_map['type'];
        $role_id = 8;
        if($user_role_type['is_hcbp'] == 1){
            $role_id = 7;
        }else if($user_role_type['is_validator'] == 1){
            $role_id = 3;
        }else{
            $role_id = 8;
        }
       
       //print_r('user type : '.$user_type);
       //print_r('instance_id : '.$user_instance);
       //print_r('phase_id : '.$live_phase_id);
       //print_r('roles_id : '.$role_id);
       if($user_type == 4){
           $permissions = UniPermission::where('uni_table_id', $table)
                ->where('user_type', $user_type)
                ->where('instance_id', $user_instance)
                ->where('phase_id', $live_phase_id)
                ->where('roles_id', $role_id)
                ->get();
       }else{
           $permissions = UniPermission::where('uni_table_id', $table)
                ->where('instance_id', $user_instance)
                ->where('phase_id', $live_phase_id)
                ->where('roles_id', 1)
                ->get();
       }
       
       
       
       foreach($permissions as $perms)
       {
           $form_permission[$perms->field_name]['role_id'] = $role_id;
           $form_permission[$perms->field_name]['instance'] = $user_instance;
           $form_permission[$perms->field_name]['field_name'] = $perms->field_name;
           $form_permission[$perms->field_name]['field_id'] = $perms->field_id;
           $form_permission[$perms->field_name]['uni_permissiontype_id'] = $perms->uni_permissiontype_id;
           $form_permission[$perms->field_name]['phase_id'] = $perms->phase_id;
           if($user_instance == 3 && $perms->field_name == 'incentive_scheme_id'){
               $form_permission[$perms->field_name]['new_field_label'] = 'Bonus Pool';
           }elseif($user_instance == 3 && $perms->field_name == 'incentive_cap'){
               $form_permission[$perms->field_name]['new_field_label'] = 'This Year Franchise';
           }else{ 
                $form_permission[$perms->field_name]['new_field_label'] = $perms->new_field_label;
           }
       }
       
       //print_r($form_permission);//exit;
       return $form_permission;
    }

    public function getColumnDefinition(Request $request){
        try{
            $res = DataField::columnDefinitionBuilder($request->table_type);
            //logger($res);
            return array('error'=>false,'response'=>$res ,'message'=>'Success');
        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }
    
    public function fff(){
        $user               = Auth::user();
        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 : 4;

        if (ManagerTypeUser::isHCBP($user->personnelnumber, $active_instance_id ) == false){
            $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 :7;
        }

        return $manager_type;

    }
}

<?php

namespace App\Http\Controllers;

use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use App\Models\AppAssist;
use App\Models\AssessorNotification;
use App\Models\Cycle;
use App\Models\CyclePhases;
use App\Models\Email;
use App\Models\Employee;
use App\Models\EmployeeAssessor;
use App\Models\Phase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class GoLiveController extends Controller
{
    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }

   
   public function create(Request $request)
   {
             
        return true;
   }

   public function getCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        if(isset($request['data'])){
            $data = $request['data'];
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];
                
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }else{
                        $id = '';
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        
        if($mode == 'save'){

            
            $instance = PermissionController::getUserInstance();
            $instance_id = 1;
            $cycle_id = 1;
            
            $main_t_model_phase = "App\\Models\\CyclePhases";
            //$phase_table = $main_t_model_phase;
            $phase_table = $main_t_model_phase::select('id')
                    ->where('instance_id', $instance_id)      
                    ->where('cycle_id', $cycle_id) 
                    ->get();
            foreach($phase_table as $phase_tbl_id){
                 $phase_table_sec = $main_t_model_phase::find($phase_tbl_id->id);
                 $phase_table_sec->open = 0;
                $phase_table_sec->live = 0;
                $phase_table_sec->save();        

            }
            
            $phase_id = 3;
            $main_t_model_phase_update = "App\\Models\\CyclePhases";
            $phase_table_update = $main_t_model_phase_update::find($phase_id);
            $phase_table_update->open = 1;
            $phase_table_update->live = 1;
            $phase_table_update->save();
            
        }
        
        $panel = new TableViewerPanel('assessor_phase_notifications', '', $mode, $selections);
        $table = $panel->show();
        $card_template = $table['content'];
        if($mode == "new"){
            $id = 0;
        }
        
        //Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }
    
    public function getFormCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'edit';
        $id = '';
        $selections = array();
        $selections['id'] = 2;
        $view_type = 'vertical';
        $where = [];
        
        
        if(isset($request['data'])){
            $data = $request['data'];
            
            $i = 0;
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];               
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }
                }                 
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        /*SET PERMISSION PER INSTANCE,PHASE(IF ANY),USER(IF ANY) etc.*/
        $instance_id = NULL;
        $instance_name = '';
        
        $user_instance = PermissionController::getUserInstance();
        if($user_instance['message'] == 'Success'){
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }
        
        // $user_roles = PermissionController::getRolesMapping();
        $where[]['instance_indicator_id'] = $instance_id;
        $where[]['cycle_id'] = 1;
        //print_r($where);exit;
        
        $form_submit_data = array();
        if($mode == 'save'){

            
            $instance = PermissionController::getUserInstance();
            $instance_id = $instance['instance'];
            $cycle_id = 1;
            
            $main_t_model_phase = "App\\Models\\CyclePhases";
            $phase_table = $main_t_model_phase::find($phase_id);
            $phase_table::where('instance_id', $instance_id)      
                    ->where('cycle_id', $cycle_id) 
                    ->chunkById(200, function ($flights) {
                $phase_table->each->update(['live' => 0]);
                $phase_table->each->update(['open' => 0]);
            });
            
            $phase_id = 3;
            $main_t_model_phase = "App\\Models\\CyclePhases";
            $phase_table = $main_t_model_phase::find($phase_id)
                    ->where('instance_id', $instance_id)      
                    ->where('cycle_id', $cycle_id) ;
            $phase_table->open = 1;
            $phase_table->live = 1;
            $phase_table->save();
            
        }
        
        $panel = new TableViewerPanel('cycle_phases', 'U', $mode, $selections, $form_submit_data, 'cycle_phases',$view_type, $where);
        $table = $panel->show();
        $card_template = $table['content'];
        if($mode == "new"){
            $id = 0;
        }
        
        
        //Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }

    public function getGoLiveData(Request $request){

        try {
            $phases     = Phase::getPhasesPerCurrentInstance();
            $live_phase = Phase::getLivePhase();
            $res        = AssessorNotification::getData($request->state['searchInput'],$request->state['skip'],$request->state['take'], true);
            $dataTotal  = AssessorNotification::getData($request->state['searchInput'],$request->state['skip'],$request->state['take'], false);
            $array      = [];

            foreach ($res as $item){
                $array[] = array(
                    'id'                => $item->personnelnumber,
                    'name'              => $item->knownas . ' ' . $item->lastname,
                    'personnelnumber'   => $item->personnelnumber,
                    'emailaddress'      => $item->emailaddress,
                    'instance'          => $item->instance,
                    'cycle_name'        => $item->cycle_name,
                    'phase'             => $item->phase,
                    'notified_on'       => $item->notified_on,
                    'notified'          => $item->notified ? "Yes":"No",
                    'resent'            => $item->resent ? "Yes":"No",
                );
            }

            $data = array(
                'phases'        => $phases,
                'live_phase'    => $live_phase,
                'apiData'=>array(
                    'pageNate'=>$this->pageNationController->index(count($dataTotal),$request->state['take']),
                    'totalTableData'=>count($dataTotal),
                    'data'=>$array,
                ),
            );
            return array('error' => false, 'response' => $data, 'message' => 'Phases GoLive Load Success');
        }

        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public function goLiveSave(Request $request){

        try {
            $phase_id           = $request['phase_id'];
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $cycle_id           = Cycle::getLiveCycleID();


            $cycle_phases = CyclePhases::where('cycle_id', $cycle_id)->where('instance_id', $active_instance_id)->get();
            foreach ($cycle_phases as $cycle_phase){
                $cycle_phase->live = 0;
                $cycle_phase->open = 0;
                $cycle_phase->save();
            }

            $cycle_phase = CyclePhases::where('phase_id', $phase_id)->where('cycle_id', $cycle_id)->where('instance_id', $active_instance_id)->first();

            if ($cycle_phase){
                $cycle_phase->live = 1;
                $cycle_phase->open = 1;
                $cycle_phase->save();
            }

            $data = array(

            );
            return array('error' => false, 'response' => $data, 'message' => 'Phases GoLive Success');
        }

        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public function goLiveWithNotifications(Request $request){

        try {
            $phase_id           = $request['phase_id'];
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $cycle_id           = Cycle::getLiveCycleID();
            $live_phase         = Phase::getLivePhase();

            $cycle_phases = CyclePhases::where('cycle_id', $cycle_id)->where('instance_id', $active_instance_id)->get();
            foreach ($cycle_phases as $cycle_phase){
                $cycle_phase->live = 0;
                $cycle_phase->open = 0;
                $cycle_phase->save();
            }

            $cycle_phase = CyclePhases::where('phase_id', $phase_id)->where('cycle_id', $cycle_id)->where('instance_id', $active_instance_id)->first();

            if ($cycle_phase){
                $cycle_phase->live = 1;
                $cycle_phase->open = 1;
                $cycle_phase->save();
            }

            // get phase managers - if SV get HCBPS else get all managers
            $exclude_fields = ['id', 'personnelnumber', 'cycle_id', 'created_at', 'updated_at'];
            $cols = in_array($phase_id, [2,7,12]) ? ['hrbp_1', 'hrbp_2', 'hrbp_3', 'hrbp_4', 'hrbp_5', 'hrbp_6' ] : AppAssist::getWriteFields('employee_assessors', $exclude_fields);

            //get all cycle employee assessors pns
            $employee_pns = Employee::where('cycle_id',$cycle_id)->where('instance_indicator_id',$active_instance_id)->pluck('personnelnumber')->toArray();
            $emp_assessors  = EmployeeAssessor::whereIn('personnelnumber', $employee_pns)->where('cycle_id', $cycle_id)->get();
            $assessor_pn    = [];
            foreach ($emp_assessors as $record){
                foreach ($cols as $col){
                    if ($record->{$col} === null) continue;

                    $assessor_pn[] = $record->{$col};
                }
            }

            $assessor_pn = array_unique($assessor_pn);
            //get managers as/from employees
            $assessors = Employee::where('cycle_id', $cycle_id)->whereIn('personnelnumber', $assessor_pn)->get();


            // get email type
            $email_name = '';
            if (in_array($phase_id, [2,7,12]) ){ // structural validation
                $email_name = 'Structural and Employee Validation Phase Email';
            }
            elseif (in_array($phase_id, [3,8,13]) ) { // allocation
                $email_name = 'Increase and Incentive Allocation Phase Email';
            }
            elseif (in_array($phase_id, [5,10,15]) ) { // Letters
                $email_name = 'Letter Validation and Distribution Email';
            }
            $email = Email::where('instance_id', $active_instance_id)->where('name', $email_name)->first();

            //get phase notifications
            $notifications = AssessorNotification::where('cycle_id', $cycle_id)->where('instance_indicator_id', $active_instance_id)->where('phase_id', $phase_id)->get();

            // send email - check if sent
            if ($email){
                foreach ($assessors as $assessor){
                    $sendTo = $assessor->emailaddress;
                    $name   = $assessor->knownas . ' ' . $assessor->lastname ;
                    $tags   = array('%NAME%' => $name);

                    $notification = AssessorNotification::where('personnelnumber', $assessor->personnelnumber)->where('cycle_id', $cycle_id)->where('instance_indicator_id', $active_instance_id)->where('phase_id', $phase_id)->first();
                    if ($notification === null){
                        MailController::sendNotification($email, $sendTo, $tags);

                        $notification = new AssessorNotification();
                        $notification->personnelnumber          = $assessor->personnelnumber;
                        $notification->cycle_id                 = $cycle_id;
                        $notification->user_personnelnumber     = Auth::user()->personnelnumber;
                        $notification->instance_indicator_id    = $active_instance_id;
                        $notification->phase_id                 = $phase_id;
                        $notification->email_id                 = $email->id;
                        $notification->resent                   = false;
                        $notification->notified                 = true;
                        $notification->save();
                    }
                }
            }

            $data = array();
            return array('error' => false, 'response' => $data, 'message' => 'Phases GoLive Emails Sent Successfully');
        }

        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

    public function resentNotification(Request $request){

        try {
            $assessor_pn        = $request['personnelnumber'];
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $cycle_id           = Cycle::getLiveCycleID();
            $phase_id           = Phase::getLivePhaseID();

            //get managers as/from employees
            $assessor = Employee::where('cycle_id', $cycle_id)->where('personnelnumber', $assessor_pn)->first();

            // get email type
            $email_name = '';
            if (in_array($phase_id, [2,7,12]) ){ // structural validation
                $email_name = 'Structural and Employee Validation Phase Email';
            }
            elseif (in_array($phase_id, [3,8,13]) ) { // allocation
                $email_name = 'Increase and Incentive Allocation Phase Email';
            }
            elseif (in_array($phase_id, [5,10,15]) ) { // Letters
                $email_name = 'Letter Validation and Distribution Email';
            }
            $email = Email::where('instance_id', $active_instance_id)->where('name', $email_name)->first();

            // send email - check if sent
            if ($email){
                    $sendTo = $assessor->emailaddress;
                    $name   = $assessor->knownas . ' ' . $assessor->lastname ;
                    $tags   = array('%NAME%' => $name);

                    $notification = AssessorNotification::where('personnelnumber', $assessor->personnelnumber)->where('cycle_id', $cycle_id)->where('instance_indicator_id', $active_instance_id)->where('phase_id', $phase_id)->where('notified', true)->where('resent', false)->first();
                    if ($notification !== null){
                        MailController::sendNotification($email, $sendTo, $tags);

                        $notification->personnelnumber          = $assessor->personnelnumber;
                        $notification->cycle_id                 = $cycle_id;
                        $notification->user_personnelnumber     = $cycle_id;
                        $notification->instance_indicator_id    = $active_instance_id;
                        $notification->phase_id                 = $phase_id;
                        $notification->email_id                 = $email->id;
                        $notification->resent                   = true;
                        $notification->notified                 = true;
                        $notification->save();
                    }
            }

            $data = array();
            return array('error' => false, 'response' => $data, 'message' => 'Phases GoLive Emails Sent Successfully');
        }

        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }

}

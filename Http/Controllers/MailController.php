<?php

namespace App\Http\Controllers;

use App\Mail\ImportNotificationMail;
use App\Mail\LetterDownloadNotification;
use App\Mail\LivePhaseNotificationMail;
use App\Mail\NewEmployeeMail;
use App\Mail\NewIssuelogNotificationMail;
use App\Mail\SystemMail;
use App\Mail\UnitAllocationMail;
use App\Mail\UnitAllocationMappingMail;
use App\Models\Email;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public static $notification_type = ['LivePhaseNotificationMail', 'ImportNotificationMail','NewEmployeeNotification', 'NewIssueLogNotification', 'LetterDownloadNotification'];

    /**
     * @param Email $email
     * @param $sendTo - email address
     * @param array $tags - associative array ('tag_name' => 'value')
     * @return array
     */
    public static function sendNotification(Email $email, $sendTo, $tags = array())
    {       $sendTo = strtolower($sendTo);
         if(env('APP_ENV')==='local'){
            $sendTo = env('DEV_EMAIL');
        }else if(env('APP_ENV')==='uat')
        {
          $sendTo = env('UAT_EMAIL');
        }
        else if(env('APP_ENV')==='production')
        {
            $sendTo = $sendTo;
        }
        
        try {

           Mail::to($sendTo)->send(new SystemMail($email, $tags));

            return array('error' => false, 'response' => [], 'message' => 'Email sent successfully');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }

    }
}

<?php

namespace App\Http\Controllers;

use App\Models\ProcessOverview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProcessOverviewController extends Controller
{
    public function getPageContent(Request $request){

        try{
            $active_instance = PermissionController::getUserInstance();

            $po = ProcessOverview::where('instance_indicator_id', $active_instance['response']['instance'])->first();

            if ($po){
                $data = array(
                    'po'        => $po,
                    'UserRoles' => UserRolesController::getUserRoleData()['response']['UserRoles'],
                );
                return array('error'=>false,'response'=>$data,'message'=>'Successfully updated Process Overview Content');
            }
            else{
                return array('error'=>true,'response'=>'','message'=>'Failed to update Process Overview Content. Please Contact Administrator.');
            }

        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }

    public function storeProcessOverview(Request $request){

        try{
            $active_instance = PermissionController::getUserInstance();
            $id = $request['id'];
            if ($id > 0)
                $po = ProcessOverview::where('id', $id)->first();
            else
                $po = null;

            if ($po === null){
                $po = new ProcessOverview();
            }
            $po->page_content           = $request['page_content'];
            $po->instance_indicator_id  = $active_instance['response']['instance'];
            $po->save();

            //$po = ProcessOverview::where('instance_indicator_id', $active_instance['response']['instance'])->where('id', $active_instance['response']['instance'])->first();
            if ($po)
                return array('error'=>false,'response'=>$po,'message'=>'Successfully updated Process Overview Content');
            else
                return array('error'=>true,'response'=>'','message'=>'Failed to update Process Overview Content. Please Contact Administrator.');


        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }
}

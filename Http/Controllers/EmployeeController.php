<?php

namespace App\Http\Controllers;

use App\CFLibrary\FilterHandler;
use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use App\Exports\ValidationExport;
use App\Imports\EmployeeImport;
use App\Imports\ValidateEmployeeImport;
use App\Models\AppAssist;
use App\Models\AppExport;
use App\Models\Audit;
use App\Models\Benchmark;
use App\Models\Currency;
use App\Models\Email;
use App\Models\Employee;
use App\Models\EmployeeAssessor;
use App\Models\EmployeeBenchmarks;
use App\Models\EmployeeIncentive;
use App\Models\EmployeePerformance;
use App\Models\EmployeeSalary;
use App\Models\EmployeeStructuralValidation;
use App\Models\FileImport;
use App\Models\Filters;
use App\Models\IncentiveScheme;
use App\Models\OccupationalLevel;
use App\Models\ParticipationCategory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Cycle;
use App\Models\CyclePhases;
use App\Models\Phase;

class EmployeeController extends Controller
{
    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }
   
    public function create(Request $request)
   {
             
        return true;
   }

    public function getCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        
        //get status where
        $employee_table_view = 'vw_employees_combine';
        $action_permissions = 'CRUSX';
        if(isset($request['tab']) && !empty($request['tab']) && $request['tab'] == 'employee' ){
             //$where[]['status_id'] = 1;
             $employee_table_view = 'vw_employees_combine';
             $action_permissions = 'CRUSX';
        }
        if(isset($request['tab']) && !empty($request['tab']) && $request['tab'] == 'employee_assesor' ){
             //$where[]['status_id'] = 2;    
            $employee_table_view = 'vw_employees_assessor';
            $action_permissions = 'RUS';
        }
        if(isset($request['tab']) && !empty($request['tab']) && $request['tab'] == 'employee_inactive' ){
             //$where[]['status_id'] = 3;
            $employee_table_view = 'vw_employees_inactive';
            $action_permissions = 'RUS';
        }
        
        if(isset($request['tab']) && !empty($request['tab']) && $request['tab'] == 'employees_retirement' ){
             //$where[]['status_id'] = 9;
             $employee_table_view = 'vw_employees_retirement';
             $action_permissions = 'RUS';
        }
        if(isset($request['tab']) && !empty($request['tab']) && $request['tab'] == 'employees_sabbatical' ){
             //$where[]['status_id'] = 8;    
            $employee_table_view = 'vw_employees_sabbatical';
            $action_permissions = 'RUS';
        }
        if(isset($request['tab']) && !empty($request['tab']) && $request['tab'] == 'employees_active_ipp' ){
             //$where[]['status_id'] = 7;
            $employee_table_view = 'vw_employees_active_ipp';
            $action_permissions = 'RUS';
        }
        
        if(isset($request['tab']) && !empty($request['tab']) && $request['tab'] == 'employees_duplicate' ){
             //$where[]['status_id'] = 6;
             $employee_table_view = 'vw_employees_duplicate';
             $action_permissions = 'RUS';
        }
        if(isset($request['tab']) && !empty($request['tab']) && $request['tab'] == 'employees_resign' ){
             //$where[]['status_id'] = 5;    
            $employee_table_view = 'vw_employees_resign';
            $action_permissions = 'RUSX';
        }
        if(isset($request['tab']) && !empty($request['tab']) && $request['tab'] == 'employees_error' ){
             //$where[]['status_id'] = 4;
            $employee_table_view = 'vw_employees_error';
            $action_permissions = 'RUS';
        }
        
        if(isset($request['data'])){
            $data = $request['data'];
            
            $i = 0;
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];               
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }
                }                 
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        $form_submit_data = array();
        if($mode == 'save'){
            if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                        //print_r($request['data']);
                        $item_field = '';
                        $dat_split = explode('_',$key);
                        $count_split = count($dat_split);
                        for($j=0;$j<($count_split - 1);$j++){
                            $item_field .= $dat_split[$j].'_';
                        }
                        $item_field = substr($item_field,0,(strlen($item_field) - 1));
                        $item_value = $dat;
                        $form_submit_data[$item_field] = $item_value;
                        if($item_field == 'id'){
                            $selections['field_name'] = $item_field;
                            $selections['field_value'] = $item_value;
                        }
                        $i++;
                }
            }
            //print_r($form_submit_data);
        }
        
        
        /*SET PERMISSION PER INSTANCE,PHASE(IF ANY),USER(IF ANY) etc.*/
        $instance_id = NULL;
        $instance_name = '';
        
        $user_instance = PermissionController::getUserInstance();
        if($user_instance['message'] == 'Success'){
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }
        
        // $user_roles = PermissionController::getRolesMapping();
        $where[]['instance_indicator_id'] = $instance_id;
        //print_r($where);exit;
        
        
        
        $view_type = 'vertical';
        if($mode == 'edit_hor'){
            $mode = 'edit';
            $view_type = 'horizontal';
        }
        
        if($mode == "new"){
            $id = 0;
        }
        elseif ($mode == "export"){
            $id = 0;
            $where['my_page'] = 'personnel_admin';
            $res = AppExport::storeExport('employees',$where,'employees');

            if ($res['error'] === false){

                $export_res = $res['response'];
                $data['file_path'] = $export_res['file_path'];
                $data['mode'] = $mode;
                return array
                (
                    'error'     => false,
                    'response'  => true,
                    'message'   => 'Success',
                    'template'  => '',
                    'data'      => $data,
                    'mode'      => $mode,
                    'id'        => $id,
                );
            }
            else{
                return $res;
            }

        }
        
        $panel = new TableViewerPanel($employee_table_view, $action_permissions, $mode, $selections, $form_submit_data, 'employees',$view_type, $where);

        $table = $panel->show();
        $card_template = $table['content'];
        
        $user = User::find(Auth::user()->id);
        $user_accounttype = $user->accounttype;

        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
            'accounttype' => $user_accounttype,
        );
    }

    public function uploadEmployeeData(Request $request){
       try{
           ini_set('max_execution_time', 21100);
           ini_set('memory_limit', '-1');
           $import_file     = $request->file('import_file');
           $import_comment  = $request->input('comment');
           $upload_mode     = $request->input('upload_mode');
           $import_path     = Storage::putFileAs('public/spreadsheet/Imports', $request->file('import_file'), date('YmdHis').'.xlsx');
           if ($upload_mode == 3)
               Employee::removeExistingData();
           /*else if ($upload_mode === 2)
               Employee::removeExistingData();*/

           //logger("Upload mode is $upload_mode - New records only");

           $tables      = 'employees';
           $vl          = new ValidateEmployeeImport($upload_mode);
           $vl->setTable($tables);
           Excel::import($vl, $import_file, 'public');
           $rows_failed    = $vl->rows_failed;
           $file_name      = false;
           $import_error   = false;

           if(count($rows_failed)>0){
                $file_name   = '/public/spreadsheet/Exports/Validation/import_validation_report' . '_'.date('YmdHis').'.xlsx';
                Excel::store(new ValidationExport(array($rows_failed)), $file_name);
                $import_error = true;
           }

           $import = new FileImport();
           $import->user_id     = Auth::id();
           $import->cycle_id    = Cycle::getLiveCycleID();
           $import->path_source = $import_path;
           $import->comment     = $import_comment;


           if ($import_error === true){

               //update import record
               $import->validation_report   = $file_name !== false ? $file_name : false;
               $import->status              = $import_error === true ? 'Error':'Success';
               $import->save();
               $url    = URL::route('validation_report',['file'=>base64_encode($file_name)]);
               return array('error' => true, 'response' => ['file_path'=>$url], 'message' => 'Import Validation Failed. Please download the import validation report below for more details.');
           }
           else{
               $import->validation_report = false;
               $import->status      = 'Success';
               $import->save();

               $employee_import = new EmployeeImport();
               Excel::import($employee_import, $import_file);
               $imported_personnel_numbers = $employee_import->import_personnel_numbers;

               foreach($imported_personnel_numbers as $imported_personnel_number){
                   $user_count  = 0;
                   $cycle_id    = Cycle::getLiveCycleID(); //TODO: update cycle id
                   //get employee
                   $employee    = Employee::where('personnelnumber', $imported_personnel_number)->where('cycle_id', $cycle_id)->first();
                   //get employee_assessor
                   if ($employee !== null){
                       $employee_assessor = EmployeeAssessor::where('personnelnumber', $employee->personnelnumber)->first();
                       $user = null;
                       $assessor_fields = AppAssist::getWriteFields('employee_assessors', null);
                        if ($employee_assessor){
                            foreach($assessor_fields as $assessor_field){

                                if (is_numeric(strpos($assessor_field, 'hrbp'))){
                                    $manager_type_id = 7; //--------> role
                                    $pn = $employee_assessor->{$assessor_field};
                                    if ($pn !== null){

                                        $assessor = Employee::where('personnelnumber',$pn)->where('cycle_id', $cycle_id)->first();

                                        $res = User::CreateUserByEmployee($assessor);
                                        if ($res['error'] === false){
                                            $user = $res['response']['user'];
                                            if ($res['response']['is_new'] === true) $user_count = $user_count + 1;
                                        }

                                        $manager_type_user = User::CreateManagerTypeUser($manager_type_id, $user->id);

                                        // create manager mapping


                                    }
                                }

                                if (is_numeric(strpos($assessor_field, 'validation_manager'))){
                                    $manager_type_id = 3; //--------> role
                                    $pn = $employee_assessor->{$assessor_field};
                                    if ($pn !== null){

                                        $assessor = Employee::where('personnelnumber',$pn)->where('cycle_id', $cycle_id)->first();

                                        $res = User::CreateUserByEmployee($assessor);
                                        if ($res['error'] === false){
                                            $user = $res['response']['user'];
                                            if ($res['response']['is_new'] === true) $user_count = $user_count + 1;
                                        }

                                        $manager_type_user = User::CreateManagerTypeUser($manager_type_id, $user->id);

                                        // create manager mapping


                                    }
                                }

                                if (is_numeric(strpos($assessor_field, 'allocation_manager'))){
                                    $manager_type_id = 4; //--------> role
                                    $pn = $employee_assessor->{$assessor_field};
                                    if ($pn !== null){

                                        $assessor = Employee::where('personnelnumber',$pn)->where('cycle_id', $cycle_id)->first();

                                        $res = User::CreateUserByEmployee($assessor);
                                        if ($res['error'] === false){
                                            $user = $res['response']['user'];
                                            if ($res['response']['is_new'] === true) $user_count = $user_count + 1;
                                        }

                                        $manager_type_user = User::CreateManagerTypeUser($manager_type_id, $user->id);

                                        // create manager mapping


                                    }
                                }

                                if (is_numeric(strpos($assessor_field, 'business_area_manager'))){
                                    $manager_type_id = 6; //--------> role
                                    $pn = $employee_assessor->{$assessor_field};
                                    if ($pn !== null){

                                        $assessor = Employee::where('personnelnumber',$pn)->where('cycle_id', $cycle_id)->first();

                                        $res = User::CreateUserByEmployee($assessor);
                                        if ($res['error'] === false){
                                            $user = $res['response']['user'];
                                            if ($res['response']['is_new'] === true) $user_count = $user_count + 1;
                                        }

                                        $manager_type_user = User::CreateManagerTypeUser($manager_type_id, $user->id);

                                        // create manager mapping


                                    }
                                }

                                if (is_numeric(strpos($assessor_field, 'viewer'))){
                                    $manager_type_id = 8; //--------> role
                                    $pn = $employee_assessor->{$assessor_field};
                                    if ($pn !== null){

                                        $assessor = Employee::where('personnelnumber',$pn)->where('cycle_id', $cycle_id)->first();

                                        $res = User::CreateUserByEmployee($assessor);
                                        if ($res['error'] === false){
                                            $user = $res['response']['user'];
                                            if ($res['response']['is_new'] === true) $user_count = $user_count + 1;
                                        }

                                        $manager_type_user = User::CreateManagerTypeUser($manager_type_id, $user->id);

                                        // create manager mapping


                                    }
                                }

                                if (is_numeric(strpos($assessor_field, 'business_manager'))){
                                    $manager_type_id = 5; //--------> role
                                    $pn = $employee_assessor->{$assessor_field};
                                    if ($pn !== null){

                                        $assessor = Employee::where('personnelnumber',$pn)->where('cycle_id', $cycle_id)->first();

                                        $res = User::CreateUserByEmployee($assessor);
                                        if ($res['error'] === false){
                                            $user = $res['response']['user'];
                                            if ($res['response']['is_new'] === true) $user_count = $user_count + 1;
                                        }

                                        $manager_type_user = User::CreateManagerTypeUser($manager_type_id, $user->id);


                                    }
                                }

                            }
                        }
                        else{
                            logger("Employee assessor for this record does not exist " . $employee->personnelnumber);
                        }
                   }

               }
               return array('error' => false, 'response' => [], 'message' => 'Employee Import ran successfully.');
           }

       }
       catch (\Exception $e) {
           $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
           Log::critical($msg);
           return array('error' => true, 'response' => [], 'message' => $msg);
       }

    }

    public function getEmployeeData(Request $request){

       try{
           $currencies = Currency::where('cycle_id', Cycle::getLiveCycleID())->get();
           $arr_curr = [];
           foreach ($currencies as $currency){
               $arr_curr[$currency->id] = $currency->name . " - " . $currency->code;
           }
           $schemes = IncentiveScheme::all()->pluck('name','id')->toArray();
           $participation_categories = ParticipationCategory::all()->pluck('name','id')->toArray();
           $occupational_levels     = OccupationalLevel::all()->pluck('name','id')->toArray();
           $record_id = $request['id'];
           $employee = DB::table('vw_personnel_admin')
                            ->where('personnelnumber', $record_id)
                            ->first();

           $benchmark = EmployeeBenchmarks::where('personnelnumber', $employee->personnelnumber)->first();

           return array(
               'error' => false,
               'response' => [
                   'data'           => $employee,
                   'currencies'     => $arr_curr,
                   'schemes'        => $schemes,
                   'benchmark'      => $benchmark,
                   'occupational_levels'        => $occupational_levels,
                   'participation_categories'   => $participation_categories,
                            ],
                            'message' => '');
       }
       catch (\Exception $e) {
           $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
           Log::critical($msg);
           return array('error' => true, 'response' => [], 'message' => $msg);
       }
    }

    public function getEmployeeByID(Request $request){

       try{

           $record_id = $request['id'];
           $employee = DB::table('vw_personnel_admin')
                            ->where('id', $record_id)
                            ->first();

           //$benchmark = EmployeeBenchmarks::where('personnelnumber', $employee->personnelnumber)->first();

           return array(
               'error' => false,
               'response' => [
                   'data'           => $employee,
                            ],
                            'message' => '');
       }
       catch (\Exception $e) {
           $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
           Log::critical($msg);
           return array('error' => true, 'response' => [], 'message' => $msg);
       }
    }

    public function getEmployeeFullName(Request $request){
       try{
           $personnelnumber = $request['manager_pn'];
           $employee = Employee::where('cycle_id', Cycle::getLiveCycleID())
                            ->where('personnelnumber', $personnelnumber)
                            ->first();
           $full_name = $employee->knownas . " " . $employee->lastname;
           logger($full_name);
           return array('error' => false, 'response' => ['data' => $employee], 'full_name' => $full_name, 'message' => '');
       }
       catch (\Exception $e) {
           $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
           Log::critical($msg);
           return array('error' => true, 'response' => [], 'message' => $msg);
       }
    }

    public function getEmployeeSaveUpdate(Request $request){

       try{

           $employeeFields = $request['employeeFields'];
           $employeeFields['incentive_scheme_id'] = $employeeFields['scheme_id'];
           $user            = Auth::user();
           $record_id       = $request['id'];

           //validation
           //$required_fields = ['particiption_category_id']


           //$employeeFields  = $request['employeeFields'];
           $cycle_id        =  Cycle::getLiveCycleID();

           $arr_benchmark['benchmark_id']= $request['state']['benchmark_id'];
           $arr_benchmark['internal_jobscale_code']= $request['state']['internal_jobscale_code'];
           $arr_benchmark['calc_internal_jobscale_name']=  $request['state']['calc_internal_jobscale_name'];
           $arr_benchmark['calc_internal_jobscale_grade']=  $request['state']['calc_internal_jobscale_grade'];
           $arr_benchmark['calc_internal_jobscale_indicator']=  $request['state']['calc_internal_jobscale_indicator'];

           if (isset($request['state']['benchmark_id'])){
               $benchmark_id = $request['state']['benchmark_id'];
               $benchmark          = Benchmark::find($benchmark_id);
               if ($benchmark){
                   $arr_benchmark['calc_intbench_bonus_median']  = $benchmark->mm;
                   $arr_benchmark['calc_intbench_salary_median'] = $benchmark->p2;;
               }
           }

           $employee = Employee::where('personnelnumber', $record_id)->where('cycle_id', Cycle::getLiveCycleID())->first();

           if ($employee){

               $columns    = DB::select("describe employees");
               foreach ($columns as $column) {
                    if (!in_array($column->Field, ['id', 'personnelnumber', 'cycle_id', 'updated_at', 'created_at'])){
                        $value_before = '';
                        $value_after = '';
                        if (isset($employeeFields[$column->Field])){
                            $value_before                = $employee->{$column->Field};
                            $value_after                 = $employeeFields[$column->Field];
                            $employee->{$column->Field}  = $employeeFields[$column->Field];
                        }
                       if ($value_before != $value_after){
                             Audit::captureAudit(
                                $employee->personnelnumber,
                                $column->Field,
                                $user->personnelnumber,
                                $value_before,
                                $value_after
                            );
                            // logger("VALUE BEFORE: $value_before%");
                            // logger("VALUE  AFTER: $value_after%");
                            // logger("FIELD  NAME: $column->Field");
                            // logger("#############################");
                        }

                    }
               }
               $employee->save();

               //Employee::dynamicRecordUpdate($employee, 'employees', $employeeFields, ['id', 'personnelnumber', 'cycle_id', 'updated_at', 'created_at']);


               $object = EmployeeAssessor::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
               if ($object){
                   $columns    = DB::select("describe employee_assessors");
                   foreach ($columns as $column) {
                       if (!in_array($column->Field, ['id', 'personnelnumber', 'cycle_id', 'updated_at', 'created_at'])){
                           $value_before = '';
                           $value_after = '';
                           if (isset($employeeFields[$column->Field])){
                               $value_before                = $object->{$column->Field};
                               $value_after                 = $employeeFields[$column->Field];
                               $object->{$column->Field}  = $employeeFields[$column->Field];
                           }
                           if($value_before !== $value_after){
                               Audit::captureAudit(
                                   $employee->personnelnumber,
                                   $column->Field,
                                   $user->personnelnumber,
                                   $value_before,
                                   $value_after
                               );
                            //    logger("VALUE BEFORE: $value_before");
                            //    logger("VALUE  AFTER: $value_after");
                            //    logger("FIELD  NAME: $column->Field");
                            //    logger("#############################");
                           }

                       }
                   }

                   $object->save();
               }

               $object = EmployeeBenchmarks::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
               if ($object){


                   $columns    = DB::select("describe employee_benchmarks");
                    // logger($columns);
                   foreach ($columns as $column) {
                       if (!in_array($column->Field, ['id', 'personnelnumber', 'cycle_id', 'updated_at', 'created_at'])){
                           $value_before = '';
                           $value_after = '';
                           if (isset($arr_benchmark[$column->Field])){
                               $value_before                = $object->{$column->Field};
                               $value_after                 = $arr_benchmark[$column->Field];
                               $object->{$column->Field}    = $arr_benchmark[$column->Field];
                           }
                           if($value_before !== $value_after){
                               Audit::captureAudit(
                                   $employee->personnelnumber,
                                   $column->Field,
                                   $user->personnelnumber,
                                   $value_before,
                                   $value_after
                               );
                            //    logger("VALUE BEFORE: $value_before");
                            //    logger("VALUE  AFTER: $value_after");
                            //    logger("FIELD  NAME: $column->Field");
                            //    logger("#############################");
                           }

                       }
                   }

                   $object->save();
               }

               $object = EmployeePerformance::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
               if ($object){

                   $columns    = DB::select("describe employee_performances");
                   foreach ($columns as $column) {
                       if (!in_array($column->Field, ['id', 'personnelnumber', 'cycle_id', 'updated_at', 'created_at'])){
                           $value_before = '';
                           $value_after = '';
                           if (isset($employeeFields[$column->Field])){
                               $value_before                = $object->{$column->Field};
                               $value_after                 = $employeeFields[$column->Field];
                               $object->{$column->Field}  = $employeeFields[$column->Field];
                           }
                           if($value_before !== $value_after){
                               Audit::captureAudit(
                                   $employee->personnelnumber,
                                   $column->Field,
                                   $user->personnelnumber,
                                   $value_before,
                                   $value_after
                               );
                            //    logger("VALUE BEFORE: $value_before");
                            //    logger("VALUE  AFTER: $value_after");
                            //    logger("FIELD  NAME: $column->Field");
                            //    logger("#############################");
                           }

                       }
                   }

                   $object->save();
               }

               $object = EmployeeIncentive::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
               if ($object){

                   $columns    = DB::select("describe employee_incentives");
                   //logger($columns);
                   foreach ($columns as $column) {
                       if (!in_array($column->Field, ['id', 'personnelnumber', 'cycle_id', 'updated_at', 'created_at'])){
                           $value_before = '';
                           $value_after = '';
                           if (isset($employeeFields[$column->Field])){
                               $value_before                = $object->{$column->Field};
                               $value_after                 = $employeeFields[$column->Field];
                               $object->{$column->Field}  = $employeeFields[$column->Field];
                           }
                           if($value_before !== $value_after){
                               Audit::captureAudit(
                                   $employee->personnelnumber,
                                   $column->Field,
                                   $user->personnelnumber,
                                   $value_before,
                                   $value_after
                               );

                               /*logger("#############################");
                                logger("VALUE BEFORE: $value_before");
                                logger("VALUE  AFTER: $value_after");
                                logger("FIELD  NAME: $column->Field");
                                logger("#############################");*/
                           }

                       }
                   }

                   $object->save();
               }

               $object = EmployeeSalary::where('cycle_id', $cycle_id)->where('personnelnumber', $employee->personnelnumber)->first();
               if ($object){

                   $columns    = DB::select("describe employee_salaries");
                   foreach ($columns as $column) {
                       if (!in_array($column->Field, ['id', 'personnelnumber', 'cycle_id', 'updated_at', 'created_at'])){
                           $value_before = '';
                           $value_after = '';
                           if (isset($employeeFields[$column->Field])){
                               $value_before                = $object->{$column->Field};
                               $value_after                 = $employeeFields[$column->Field];
                               $object->{$column->Field}  = $employeeFields[$column->Field];
                           }
                           if($value_before !== $value_after){
                               Audit::captureAudit(
                                   $employee->personnelnumber,
                                   $column->Field,
                                   $user->personnelnumber,
                                   $value_before,
                                   $value_after
                               );
                            //    logger("VALUE BEFORE: $value_before");
                            //    logger("VALUE  AFTER: $value_after");
                            //    logger("FIELD  NAME: $column->Field");
                            //    logger("#############################");
                           }

                       }
                   }

                   $object->save();
               }

               $emp_user = User::where('personnelnumber', $employee->personnelnumber)->first();
               if ($emp_user){

                   $emp_user->save();
               }

               $employee->refreshCalculations();
           }

           return array('error' => false, 'response' => ['data' => $employee], 'message' => 'Employee data saved successfuly');
       }
       catch (\Exception $e) {
           $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
           Log::critical($msg);
           return array('error' => true, 'response' => [], 'message' => $msg);
       }
    }

    public function loadLookups(Request $request){

        try{
            $currencies = Currency::where('cycle_id', Cycle::getLiveCycleID())->get();
            $arr_curr = [];
            foreach ($currencies as $currency){
                $arr_curr[$currency->id] = $currency->name . " - " . $currency->code;
            }
            $schemes = IncentiveScheme::all()->pluck('name','id')->toArray();
            $record_id = $request['id'];
            return array('error' => false, 'response' => ['currencies' => $arr_curr, 'schemes' => $schemes], 'message' => '');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public function postEmployeeCreate(Request $request){

        try{
            $authUser = User::find(Auth::user()->id);
            $AffectedPerson = $request['employeeFields']['knownas'] .' '. $request['employeeFields']['lastname'];

            //  logger($authUser->name);

            $user            = Auth::user();
            $employeeFields  = $request['employeeFields'];
            $cycle_id        =  Cycle::getLiveCycleID();

            $employeeFields['cycle_id'] = $cycle_id;
            $employee = new Employee();

            if ($employee){

                $columns    = DB::select("describe employees");
                foreach ($columns as $column) {
                    if (!in_array($column->Field, ['id'])){
                        $value_before = '';
                        $value_after = '';
                        if (isset($employeeFields[$column->Field])){
                            $value_before                = $employee->{$column->Field};
                            $value_after                 = $employeeFields[$column->Field];
                            $employee->{$column->Field}  = $employeeFields[$column->Field];
                        }
                        if($value_before !== $value_after){
                            Audit::captureAudit(
                                $employee->personnelnumber,
                                $column->Field,
                                $user->personnelnumber,
                                $value_before,
                                $value_after
                            );
                            // logger("VALUE BEFORE: $value_before");
                            // logger("VALUE  AFTER: $value_after");
                            // logger("FIELD  NAME: $column->Field");
                            // logger("#############################");
                        }

                    }
                }
                $employee->save();

                //Employee::dynamicRecordUpdate($employee, 'employees', $employeeFields, ['id', 'personnelnumber', 'cycle_id', 'updated_at', 'created_at']);


                $object = new EmployeeAssessor();
                if ($object){
                    $columns    = DB::select("describe employee_assessors");
                    foreach ($columns as $column) {
                        if (!in_array($column->Field, ['id'])){
                            $value_before = '';
                            $value_after = '';
                            if (isset($employeeFields[$column->Field])){
                                $value_before                = $object->{$column->Field};
                                $value_after                 = $employeeFields[$column->Field];
                                $object->{$column->Field}  = $employeeFields[$column->Field];
                            }
                            if($value_before !== $value_after){
                                Audit::captureAudit(
                                    $employee->personnelnumber,
                                    $column->Field,
                                    $user->personnelnumber,
                                    $value_before,
                                    $value_after
                                );
                                // logger("VALUE BEFORE: $value_before");
                                // logger("VALUE  AFTER: $value_after");
                                // logger("FIELD  NAME: $column->Field");
                                // logger("#############################");
                            }

                        }
                    }

                    $object->save();
                }

                $object = new EmployeeBenchmarks();
                if ($object){

                    $columns    = DB::select("describe employee_benchmarks");
                    foreach ($columns as $column) {
                        if (!in_array($column->Field, ['id'])){
                            $value_before = '';
                            $value_after = '';
                            if (isset($employeeFields[$column->Field])){
                                $value_before                = $object->{$column->Field};
                                $value_after                 = $employeeFields[$column->Field];
                                $object->{$column->Field}  = $employeeFields[$column->Field];
                            }
                            if($value_before !== $value_after){
                                Audit::captureAudit(
                                    $employee->personnelnumber,
                                    $column->Field,
                                    $user->personnelnumber,
                                    $value_before,
                                    $value_after
                                );
                                // logger("VALUE BEFORE: $value_before");
                                // logger("VALUE  AFTER: $value_after");
                                // logger("FIELD  NAME: $column->Field");
                                // logger("#############################");
                            }

                        }
                    }

                    $object->save();
                }

                $object = new EmployeePerformance();
                if ($object){

                    $columns    = DB::select("describe employee_performances");
                    foreach ($columns as $column) {
                        if (!in_array($column->Field, ['id'])){
                            $value_before = '';
                            $value_after = '';
                            if (isset($employeeFields[$column->Field])){
                                $value_before                = $object->{$column->Field};
                                $value_after                 = $employeeFields[$column->Field];
                                $object->{$column->Field}  = $employeeFields[$column->Field];
                            }
                            if($value_before !== $value_after){
                                Audit::captureAudit(
                                    $employee->personnelnumber,
                                    $column->Field,
                                    $user->personnelnumber,
                                    $value_before,
                                    $value_after
                                );
                                // logger("VALUE BEFORE: $value_before");
                                // logger("VALUE  AFTER: $value_after");
                                // logger("FIELD  NAME: $column->Field");
                                // logger("#############################");
                            }

                        }
                    }

                    $object->save();
                }

                $object = new EmployeeIncentive ();
                if ($object){

                    $columns    = DB::select("describe employee_incentives");
                    foreach ($columns as $column) {
                        if (!in_array($column->Field, ['id'])){
                            $value_before = '';
                            $value_after = '';
                            if (isset($employeeFields[$column->Field])){
                                $value_before                = $object->{$column->Field};
                                $value_after                 = $employeeFields[$column->Field];
                                $object->{$column->Field}  = $employeeFields[$column->Field];
                            }
                            if($value_before !== $value_after){
                                Audit::captureAudit(
                                    $employee->personnelnumber,
                                    $column->Field,
                                    $user->personnelnumber,
                                    $value_before,
                                    $value_after
                                );
                                // logger("VALUE BEFORE: $value_before");
                                // logger("VALUE  AFTER: $value_after");
                                // logger("FIELD  NAME: $column->Field");
                                // logger("#############################");
                            }

                        }
                    }

                    $object->save();
                }

                $object = new EmployeeSalary();
                if ($object){

                    $columns    = DB::select("describe employee_salaries");
                    foreach ($columns as $column) {
                        if (!in_array($column->Field, ['id'])){
                            $value_before = '';
                            $value_after = '';
                            if (isset($employeeFields[$column->Field])){
                                $value_before                = $object->{$column->Field};
                                $value_after                 = $employeeFields[$column->Field];
                                $object->{$column->Field}  = $employeeFields[$column->Field];
                            }
                            if($value_before !== $value_after){
                                Audit::captureAudit(
                                    $employee->personnelnumber,
                                    $column->Field,
                                    $user->personnelnumber,
                                    $value_before,
                                    $value_after
                                );
                                // logger("VALUE BEFORE: $value_before");
                                // logger("VALUE  AFTER: $value_after");
                                // logger("FIELD  NAME: $column->Field");
                                // logger("#############################");
                            }

                        }
                    }

                    $object->save();
                }

                $employeeFields['cycle_id']     = $cycle_id;
                $employeeFields['name']         = $employee->knownas . " " . $employee->lastname;
                $employeeFields['email']        = $employee->emailaddress;
                $employeeFields['username']     = $employee->emailaddress;
                $employeeFields['password']     = Hash::make('Pa55w0rd*');
                $object = User::where('personnelnumber', $employee->personnelnumber)->first();
                if ($object){
                    foreach ($columns as $column) {
                        if (!in_array($column->Field, ['id'])){
                            $value_before = '';
                            $value_after = '';
                            if (isset($employeeFields[$column->Field])){
                                $value_before                = $object->{$column->Field};
                                $value_after                 = $employeeFields[$column->Field];
                                $object->{$column->Field}  = $employeeFields[$column->Field];
                            }
                            if($value_before !== $value_after){
                                Audit::captureAudit(
                                    $employee->personnelnumber,
                                    $column->Field,
                                    $user->personnelnumber,
                                    $value_before,
                                    $value_after
                                );
                                // logger("VALUE BEFORE: $value_before");
                                // logger("VALUE  AFTER: $value_after");
                                // logger("FIELD  NAME: $column->Field");
                                // logger("#############################");
                            }

                        }
                    }
                    $object->save();
                }
            }


            foreach (User::where('admin', 1)->get() as $item){
                // Log::notice('Issue Log Email sent to this admin: '.$item->name);

                $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
                $email = Email::where('instance_id', $active_instance_id)->where('name', 'New Employee')->first();
                $sendTo = $item->email;
                $tags = array(
                    '%NAME%'            => $item->name,
                    '%HRBP%'            => $authUser->name,
                    '%AFFECTED_PERSON%' => $AffectedPerson,
                );
                MailController::sendNotification( $email, $sendTo, $tags);
            }

            return array('error' => false, 'response' => ['data' => $employee], 'message' => '');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public function getEmployeeRecords(Request $request)
    {
        try {

            $active_instance = PermissionController::getUserInstance()['response']['instance'];
            if($request->state['take']===null){
                Log::warning("The value is null: ".$request->state['take']);
            }
            if($request->state['sort_col'] == ""){
                $sort_col = "knownas";
            }else{
                $sort_col = $request->state['sort_col'];
            }
            $res    = Employee::getEmployeeRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true,$request['status_id'], $sort_col ,$request->state['is_ascending'],'');
            $total  = Employee::getEmployeeRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false,$request['status_id'],$sort_col ,$request->state['is_ascending'],'');
            $array = [];
            if($active_instance === 1){
                foreach ($res as $data){
                    $array []= array(
                        'id'                => $data->personnelnumber,
                        'personnelnumber'   => $data->personnelnumber,
                        'knownas'           => $data->knownas,
                        'lastname'          => $data->lastname,
                        'calc_prorata'      => $data->calc_prorata,
                        'jobgrade'          => $data->jobgrade,
                        'milestone'         => $data->milestone,
                        'jobtitle'          => $data->jobtitle,
                        'current_rating'    => $data->current_rating,
                        'h_1_performance1'  => $data->h_1_performance1,
                        'incentive_scheme'  => $data->incentive_scheme,

                        //'incentive_cap'                     => $data->incentive_cap,
                        //'internal_jobscale_code'            => $data->internal_jobscale_code,
                        //'calc_internal_jobscale_name'       => $data->calc_internal_jobscale_name,
                        //'calc_internal_jobscale_indicator'  => $data->calc_internal_jobscale_indicator,

                        'calc_allocationmanager1name'       =>$data->calc_allocationmanager1name !== null ? $data->calc_allocationmanager1name : "" ,
                        'calc_allocationmanager2name'       =>$data->calc_allocationmanager2name !== null ? $data->calc_allocationmanager2name : "" ,

                        'calc_phaseclustermanager1name'     =>$data->calc_phaseclustermanager1name !== null ? $data->calc_phaseclustermanager1name : "" ,
                        'calc_phaseclustermanager2name'     =>$data->calc_phaseclustermanager2name !== null ? $data->calc_phaseclustermanager2name : "" ,

                        'calc_businessmanager1name'         =>$data->calc_businessmanager1name !== null ? $data->calc_businessmanager1name : "" ,
                        'calc_businessmanager2name'         =>$data->calc_businessmanager2name !== null ? $data->calc_businessmanager2name : "" ,

                        'calc_hrbp1name'                    =>$data->calc_hrbp1name !== null ? $data->calc_hrbp1name . "   " : "" ,
                        'calc_hrbp2name'                    =>$data->calc_hrbp2name !== null ? $data->calc_hrbp2name . "   ": "" ,
                        'calc_hrbp3name'                    =>$data->calc_hrbp3name !== null ? $data->calc_hrbp3name . "   ": "" ,
                        'calc_hrbp4name'                    =>$data->calc_hrbp4name !== null ? $data->calc_hrbp4name . "   " : "" ,
                        'calc_hrbp5name'                    =>$data->calc_hrbp5name !== null ? $data->calc_hrbp5name . "   ": "" ,

                        'linemanager'        => $data->linemanager !== null ? $data->linemanager : "" ,
                        'orglevel5'          => $data->orglevel5,
                        'orglevel6'          => $data->orglevel6,
                        'orglevel7'          => $data->orglevel7,
                        'orglevel8'          => $data->orglevel8,

                        'assign_manager'          => true,
                        'dummy'             => 'adsdsads',

                    );
                }
            }
            elseif ($active_instance === 2){
                foreach ($res as $data){
                    $array []= array(
                        'id'                => $data->personnelnumber,
                        'personnelnumber'   => $data->personnelnumber,
                        'knownas'           => $data->knownas,
                        'lastname'          => $data->lastname,
                        'calc_prorata'      => $data->calc_prorata,
                        //'jobgrade'          => $data->jobgrade,
                        //'milestone'         => $data->milestone,
                        'jobtitle'          => $data->jobtitle,
                        'current_rating'    => $data->current_rating,
                        'h_1_performance1'  => $data->h_1_performance1,
                        'incentive_scheme'  => $data->incentive_scheme,

                        //'incentive_cap'                     => $data->incentive_cap,
                        //'internal_jobscale_code'            => $data->internal_jobscale_code,
                        //'calc_internal_jobscale_name'       => $data->calc_internal_jobscale_name,
                        //'calc_internal_jobscale_indicator'  => $data->calc_internal_jobscale_indicator,

                        'calc_allocationmanager1name'       =>$data->calc_allocationmanager1name !== null ? $data->calc_allocationmanager1name : "" ,
                        'calc_allocationmanager2name'       =>$data->calc_allocationmanager2name !== null ? $data->calc_allocationmanager2name : "" ,

                        'calc_phaseclustermanager1name'     =>$data->calc_phaseclustermanager1name !== null ? $data->calc_phaseclustermanager1name : "" ,
                        'calc_phaseclustermanager2name'     =>$data->calc_phaseclustermanager2name !== null ? $data->calc_phaseclustermanager2name : "" ,

                        'calc_businessmanager1name'         =>$data->calc_businessmanager1name !== null ? $data->calc_businessmanager1name : "" ,
                        'calc_businessmanager2name'         =>$data->calc_businessmanager2name !== null ? $data->calc_businessmanager2name : "" ,

                        'calc_hrbp1name'                    =>$data->calc_hrbp1name !== null ? $data->calc_hrbp1name . "   " : "" ,
                        'calc_hrbp2name'                    =>$data->calc_hrbp2name !== null ? $data->calc_hrbp2name . "   ": "" ,
                        'calc_hrbp3name'                    =>$data->calc_hrbp3name !== null ? $data->calc_hrbp3name . "   ": "" ,
                        'calc_hrbp4name'                    =>$data->calc_hrbp4name !== null ? $data->calc_hrbp4name . "   " : "" ,
                        'calc_hrbp5name'                    =>$data->calc_hrbp5name !== null ? $data->calc_hrbp5name . "   ": "" ,

                        'linemanager'          => $data->linemanager !== null ? $data->linemanager : "" ,
                        'orglevel5'          => $data->orglevel5,
                        'orglevel6'          => $data->orglevel6,
                        'orglevel7'          => $data->orglevel7,
                        'orglevel8'          => $data->orglevel8,

                        'assign_manager'          => true,
                        'dummy'             => 'adsdsads',

                    );
                }
            }
            elseif ($active_instance === 3){
                foreach ($res as $data){
                    $array []= array(
                        'id'                => $data->personnelnumber,
                        'personnelnumber'   => $data->personnelnumber,
                        'knownas'           => $data->knownas,
                        'lastname'          => $data->lastname,
                        'calc_prorata'      => $data->calc_prorata,
                        'jobgrade'          => $data->jobgrade,
                        'milestone'         => $data->milestone,
                        'jobtitle'          => $data->jobtitle,
                        'current_rating'    => $data->current_rating,
                        'h_1_performance1'  => $data->h_1_performance1,
                        'incentive_scheme'  => $data->incentive_scheme,

                        'incentive_cap'                     => $data->incentive_cap,
                        'internal_jobscale_code'            => $data->internal_jobscale_code,
                        'calc_internal_jobscale_name'       => $data->calc_internal_jobscale_name,
                        'calc_internal_jobscale_indicator'  => $data->calc_internal_jobscale_indicator,

                        'calc_allocationmanager1name'       =>$data->calc_allocationmanager1name !== null ? $data->calc_allocationmanager1name : "" ,
                        'calc_allocationmanager2name'       =>$data->calc_allocationmanager2name !== null ? $data->calc_allocationmanager2name : "" ,

                        'calc_phaseclustermanager1name'     =>$data->calc_phaseclustermanager1name !== null ? $data->calc_phaseclustermanager1name : "" ,
                        'calc_phaseclustermanager2name'     =>$data->calc_phaseclustermanager2name !== null ? $data->calc_phaseclustermanager2name : "" ,

                        'calc_businessmanager1name'         =>$data->calc_businessmanager1name !== null ? $data->calc_businessmanager1name : "" ,
                        'calc_businessmanager2name'         =>$data->calc_businessmanager2name !== null ? $data->calc_businessmanager2name : "" ,

                        'calc_hrbp1name'                    =>$data->calc_hrbp1name !== null ? $data->calc_hrbp1name . "   " : "" ,
                        'calc_hrbp2name'                    =>$data->calc_hrbp2name !== null ? $data->calc_hrbp2name . "   ": "" ,
                        'calc_hrbp3name'                    =>$data->calc_hrbp3name !== null ? $data->calc_hrbp3name . "   ": "" ,
                        'calc_hrbp4name'                    =>$data->calc_hrbp4name !== null ? $data->calc_hrbp4name . "   " : "" ,
                        'calc_hrbp5name'                    =>$data->calc_hrbp5name !== null ? $data->calc_hrbp5name . "   ": "" ,

                        'linemanager'          => $data->linemanager !== null ? $data->linemanager : "" ,
                        'orglevel5'          => $data->orglevel5,
                        'orglevel6'          => $data->orglevel6,
                        'orglevel7'          => $data->orglevel7,
                        'orglevel8'          => $data->orglevel8,

                        'assign_manager'          => true,
                        'dummy'             => 'adsdsads',

                    );
                }
            }

            /*// we need to identify the active status_id state based on the filters to facilitate toggling of active tab(s)
            $status_filters = Filters::where('reference_table', 'statuses')->where('user_id', Auth::user()->id)->pluck('reference_table_id')->toArray();
            if (count($status_filters) === 0){
                $status_filters = [1,19];
            }*/
            $status_filters = Filters::getStatusFilters();
            return array (
                'error'     =>false,
                'status_id' =>  $status_filters,
                'response'  =>array(
                    'data'          =>  $array,
                    'pageNate'      =>  $this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>  count($total)
                ),
                'message'=>'Records Loaded');
        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load records');
        }
    }

    public function getBenchmarks(Request $request)
    {
        try {
           // logger($request);
                                    //getRecords($searchInput,$skip,$take, $is_paginate, $status = null,$sort_by,$is_ascending, $clause = ""){
            $res    = Benchmark::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], true);
            $total  = Benchmark::getRecords($request->state['searchInput'], $request->state['skip'], $request->state['take'], false );
            $array = [];

            foreach ($res as $data){
                $array []= array(
                    'id'                                    => $data->id,
                    'internal_jobscale_code'               => $data->job_code,
                    'calc_internal_jobscale_name'           => $data->title,
                    'calc_internal_jobscale_grade'          => $data->grade,
                    'calc_internal_jobscale_indicator'      => $data->indicator,
                );
            }
            return array (
                'error'=>false,
                'response'=>array(
                    'data'=>$array,
                    'pageNate'=>$this->pageNationController->index(count($total),$request->state['take']),
                    'totalTableData'=>count($total)
                ),
                'message'=>'Benchmark Records Loaded');
        } catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => 'Failed to load records');
        }
    }

    public function getBenchmark(Request $request){
        $record_id = $request['record_id'];

        $record = Benchmark::find($record_id);

        return array('error' => false, 'response' => [
            'id'                                    => $record->id,
            'internal_jobscale_code'                => $record->job_code,
            'calc_internal_jobscale_name'           => $record->title,
            'calc_internal_jobscale_grade'          => $record->grade,
            'calc_internal_jobscale_indicator'      => $record->indicator,
        ], 'message' => 'Record Loaded successfully');
    }

    public function getCurrencyConversionRate(Request $request){
        $record_id  = $request['id'];
        $record     = Currency::find($record_id);
        return array('error' => false, 'response' => [
            'conversion_rate'   => $record->conversion_rate,
            'id'                => $record->id,
        ], 'message' => 'Currency Loaded successfully');
    }

    public function backendCalculations(Request $request){
        try{
            $pn                     = $request['id'];
            $field                  = $request['key'];
            $value                  = $request['value'];
            $employee               = Employee::where('personnelnumber', $pn)->first();
            $employee_incentive     = EmployeeIncentive::where('personnelnumber', $pn)->first();
            $employee_salary        = EmployeeSalary::where('personnelnumber', $pn)->first();
            $currency               = Currency::find($employee_salary->salary_currency_id);

            if ($currency){
                if($field == "ref_contribution"){
                    $ref_contribution           = $value;
                    $ref_contribution_percent   = $employee_incentive->proposedincentive_value > 0 ? $ref_contribution / $employee_incentive->proposedincentive_value * 100 : 0;
                    $ref_contribution_zar       = $ref_contribution  / $currency->conversion_rate;

                    return array(
                        'error'     => false,
                        'message'   => 'REF Calculations completed successfully',
                        'response'  => [
                            'ref_contribution'          => $ref_contribution, //round($ref_contribution,0),
                            'ref_contribution_percent'  => $ref_contribution_percent, //round($ref_contribution_percent, 1),
                            'ref_contribution_zar'      => $ref_contribution_zar, //round($ref_contribution_zar, 0),
                        ],
                    );
                }

                if($field == "ref_contribution_percent"){
                    $ref_contribution_percent   = $value;
                    $ref_contribution           = $employee_incentive->proposedincentive_value * $ref_contribution_percent / 100;
                    $ref_contribution_zar       = $currency->conversion_rate > 0 ? $ref_contribution  / $currency->conversion_rate : 0;

                    return array(
                        'error'     => false,
                        'message'   => 'REF Calculations completed successfully',
                        'response'  => [
                            'ref_contribution'          => $ref_contribution, //round($ref_contribution,0),
                            'ref_contribution_percent'  => $ref_contribution_percent, //round($ref_contribution_percent, 1),
                            'ref_contribution_zar'      => $ref_contribution_zar, //round($ref_contribution_zar, 0),
                        ],
                    );
                }

                if($field == "special_deferred_restricted_shares"){
                    $special_deferred_restricted_shares         = $value;
                    $special_deferred_restricted_shares_zar     = $special_deferred_restricted_shares  / $currency->conversion_rate;

                    return array(
                        'error'     => false,
                        'message'   => 'SDRS Calculations completed successfully',
                        'response'  => [
                            'special_deferred_restricted_shares'        => round($special_deferred_restricted_shares,0),
                            'special_deferred_restricted_shares_zar'    => round($special_deferred_restricted_shares_zar, 0),
                        ],
                    );
                }

                if($field == "deferred_restricted_shares"){
                    $deferred_restricted_shares         = $value;
                    $deferred_restricted_shares_zar     = $deferred_restricted_shares  / $currency->conversion_rate;

                    return array(
                        'error'     => false,
                        'message'   => 'DRS Calculations completed successfully',
                        'response'  => [
                            'deferred_restricted_shares'        => round($deferred_restricted_shares,0),
                            'deferred_restricted_shares_zar'    => round($deferred_restricted_shares_zar, 0),
                        ],
                    );
                }
                if($field == "performance_reward_plan"){
                    $performance_reward_plan         = $value;
                    $performance_reward_plan_zar     = $performance_reward_plan  / $currency->conversion_rate;

                    return array(
                        'error'     => false,
                        'message'   => 'DRS Calculations completed successfully',
                        'response'  => [
                            'performance_reward_plan'        => round($performance_reward_plan,0),
                            'performance_reward_plan_zar'    => round($performance_reward_plan_zar, 0),
                        ],
                    );
                }
                if($field == "incentive_top_up"){
                    $incentive_top_up         = $value;
                    $incentive_top_up_zar     = $incentive_top_up  / $currency->conversion_rate;

                    return array(
                        'error'     => false,
                        'message'   => 'Incentive Top Up Calculations completed successfully',
                        'response'  => [
                            'incentive_top_up'        => round($incentive_top_up,0),
                            'incentive_top_up_zar'    => round($incentive_top_up_zar, 0),
                        ],
                    );
                }
                if($field == "stanlib_suggest_incentive_cash_value"){
                    $stanlib_suggest_incentive_cash_value         = $value;
                    //$incentive_top_up_zar     = $incentive_top_up  / $currency->conversion_rate;

                    return array(
                        'error'     => false,
                        'message'   => 'stanlib_suggest_incentive_cash_value Calculations completed successfully',
                        'response'  => [
                            'incentive_top_up'        => round($stanlib_suggest_incentive_cash_value,0),
                            //'incentive_top_up_zar'    => round($incentive_top_up_zar, 0),
                        ],
                    );
                }
                if($field == "stanlib_suggest_incentive_shares_value"){
                    $stanlib_suggest_incentive_shares_value         = $value;
                    //$incentive_top_up_zar     = $incentive_top_up  / $currency->conversion_rate;

                    return array(
                        'error'     => false,
                        'message'   => 'stanlib_suggest_incentive_shares_value Calculations completed successfully',
                        'response'  => [
                            'stanlib_suggest_incentive_shares_value'        => round($stanlib_suggest_incentive_shares_value,0),
                            //'incentive_top_up_zar'    => round($incentive_top_up_zar, 0),
                        ],
                    );
                }
                if($field == "stanlib_suggest_incentive_units_value"){
                    $stanlib_suggest_incentive_units_value         = $value;
                    //$incentive_top_up_zar     = $incentive_top_up  / $currency->conversion_rate;

                    return array(
                        'error'     => false,
                        'message'   => 'stanlib_suggest_incentive_units_value Calculations completed successfully',
                        'response'  => [
                            'stanlib_suggest_incentive_units_value'        => round($stanlib_suggest_incentive_units_value,0),
                            //'incentive_top_up_zar'    => round($incentive_top_up_zar, 0),
                        ],
                    );
                }
            }
            else{
                return array('error' => true, 'response' => [], 'message' => 'Currency conversion failed.');
            }

        }
        catch (\Exception $e) {
            $msg = 'File: '.$e->getFile().' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }


}

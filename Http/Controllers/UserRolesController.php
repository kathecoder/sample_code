<?php

namespace App\Http\Controllers;

use App\Models\Cycle;
use App\Models\Employee;
use App\Models\Instance;
use App\Models\InstanceIndicator;
use App\Models\ManagerTypeUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class UserRolesController extends Controller
{
    public function getUserRole(){
        try {
            $user  = User::find(Auth::user()->id);
            PermissionController::getUserInstance();
            /*$res = array(
                'is_participant'=>0, 'is_hr'=>0, 'active'=>$user->active, 'super'=>$user->super, 'admin'=>$user->admin, 'name' => explode(" ",$user->name),
            );*/
            $res = array(
                'is_admin'=>$user->admin,
                'is_super'=>$user->super,
                'accounttype'=>$user->accounttype,
                'name' => explode(" ",$user->name),
            );

            return array('error'=>false,'response'=>array('UserRoles'=>$res, 'instance'=>$user->instance_indicator_id,'userDetails'=>array('name'=>'User User')),'message'=>'Success');

        }catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }
    public static function getUserRoleData($user = null){
        try {

            $user = $user === null ?  User::find(Auth::user()->id) :  User::find($user->id) ;


            /// added after Allocation was closed
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 : 4;

            if(!Session::has('manager_type') && !in_array($user->accounttype, [1,2,3])){

                if (ManagerTypeUser::isHCBP($user->personnelnumber, $active_instance_id ) == false){
                    $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 :7;
                    Session::put('manager_type', $manager_type);
                }
                else{
                    $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 :4;
                    Session::put('manager_type', $manager_type);
                }

            }
            elseif(Session::has('manager_type')){
               // logger("MANAGER TYPE: FROM SESSION ". Session::get('manager_type'));
                $manager_type       = Session::get('manager_type');
            }

            elseif(!in_array($user->accounttype, [1,2,3])){

                $manager_type       = in_array($user->accounttype, [1,2,3]) ? 1 : 4;
            }

            /// added after Allocation was closed



            $res = array(
                'is_admin'      => $user->admin,
                'is_super'      => $user->super,
                'accounttype'   => $manager_type, //$user->accounttype,
                'name' => explode(" ",$user->name),
            );

            return array('error'=>false,'response'=>array('UserRoles'=>$res,'instance'=>$user->instance_indicator_id,'userDetails'=>array('name'=>'User User')),'message'=>'Success');

        }catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }
    public function changeInstance(Request $request)
    {
        try {
            $user = User::find(Auth::user()->id);

            $active_instance_id = $request['id'];
            $instance           = InstanceIndicator::find($active_instance_id);
            $active_instance    = array(
                                    'instance'      => $active_instance_id,
                                    'instance_name' => $instance->name,
                                    );

            Session::put('active_instance', $active_instance);

            $res = array(
                'UserRoles'     => UserRolesController::getUserRoleData()['response']['UserRoles'],
                'instance'      => $request->id,
                'userDetails'   => array('name'=>'User User')
            );

            return array(
                'error'         => false,
                'response'      => $res,
                'message'       =>'Success'
            );

        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }

    public  function userInstanceAccess() {
        try{
            $cycle_id   = Cycle::getLiveCycleID(); //todo: update cycle id
            $user       = Auth::user();
            $user_roles = self::getUserRoleData()['response']['UserRoles'];
            $active_instance = PermissionController::getUserInstance()['response'];

            if ( in_array($user_roles['accounttype'], [1, 2, 3])) {
                $instances = DB::table('instance_indicators')->whereIn('id', [1, 2, 3])->get()->toArray();
                return array(
                    'error'             => false,
                    'response'          => $instances,
                    'active_instance'   => $active_instance,
                    'message'           =>'Success'
                );
            }

            else{
                $collection = DB::table('employees')
                    ->leftJoin('employee_assessors',   'employees.personnelnumber', '=', 'employee_assessors.personnelnumber')
                    ->where('employees.cycle_id',           $cycle_id)
                    ->where('employee_assessors.cycle_id',  $cycle_id);
                $manager_pn = $user->personnelnumber;

                $collection->where(function ($q) use($manager_pn) {
                    $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                        ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)

                        ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                        ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                        ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                        ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                        ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                        ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                        ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                        ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                        ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                        ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

                });
                $res = $collection->pluck('instance_indicator_id')->toArray();

                $instances = DB::table('instance_indicators')->whereIn('id',$res)->get()->toArray();
                return array(
                    'error'             => false,
                    'response'          => $instances,
                    'active_instance'   => $active_instance,
                    'message'           =>'Success'
                );
            }
        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }

    }

    public static function getEmployeeIDsPerManager($table_name = null){

        try{
            $cycle_id           = Cycle::getLiveCycleID(); //todo: update cycle id
            $user               = Auth::user();
            $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
            $user_roles         = self::getUserRoleData()['response']['UserRoles'];
            $collection         = null;
            $manager_pn         = $user->personnelnumber;

            if ( !in_array($user_roles['accounttype'], [1, 2, 3])) {

                if ($table_name !== null){
                    $collection = DB::table($table_name)
                        ->where('instance_indicator_id',     $active_instance_id);

                    $collection->where(function ($q) use($manager_pn) {
                        $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                            ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)

                            ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                            ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                            ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                            ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                            ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                            ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                            ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                            ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                            ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                            ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

                    });

                    return $collection->pluck('id')->toArray();
                }
                else{
                    $collection = DB::table('employees as e')
                        ->leftJoin('employee_assessors as a',   'e.personnelnumber', '=', 'a.personnelnumber')
                        ->where('e.cycle_id',               $cycle_id)
                        ->where('e.instance_indicator_id',     $active_instance_id);

                    $collection->where(function ($q) use($manager_pn) {
                        $q->where('hrbp_1', $manager_pn)->orWhere('hrbp_2', $manager_pn)->orWhere('hrbp_3', $manager_pn)
                            ->orWhere('hrbp_4', $manager_pn)->orWhere('hrbp_5', $manager_pn)->orWhere('hrbp_6', $manager_pn)

                            ->orWhere('validation_manager_1', $manager_pn)->orWhere('validation_manager_2', $manager_pn)->orWhere('validation_manager_3', $manager_pn)
                            ->orWhere('validation_manager_4', $manager_pn)->orWhere('validation_manager_5', $manager_pn)->orWhere('validation_manager_6', $manager_pn)

                            ->orWhere('allocation_manager_1', $manager_pn)->orWhere('allocation_manager_2', $manager_pn)->orWhere('allocation_manager_3', $manager_pn)
                            ->orwhere('allocation_manager_4', $manager_pn)->orWhere('allocation_manager_5', $manager_pn)->orWhere('allocation_manager_6', $manager_pn)

                            ->orWhere('business_manager_1', $manager_pn)->orWhere('business_manager_2', $manager_pn)->orWhere('business_manager_3', $manager_pn)
                            ->orwhere('business_manager_4', $manager_pn)->orWhere('business_manager_5', $manager_pn)->orWhere('business_manager_6', $manager_pn)

                            ->orWhere('business_area_manager_1', $manager_pn)->orWhere('business_area_manager_2', $manager_pn)->orWhere('business_area_manager_3', $manager_pn)
                            ->orwhere('business_area_manager_4', $manager_pn)->orWhere('business_area_manager_5', $manager_pn)->orWhere('business_area_manager_6', $manager_pn)

                            ->orwhere('viewer_1', $manager_pn)->orWhere('viewer_2', $manager_pn)->orWhere('viewer_3', $manager_pn)
                            ->orwhere('viewer_4', $manager_pn)->orWhere('viewer_5', $manager_pn)->orWhere('viewer_6', $manager_pn);

                    });

                    return $collection->pluck('e.id')->toArray();
                }
            }
            else{
                if ($table_name !== null){
                    $collection = DB::table($table_name)
                        ->where('instance_indicator_id',     $active_instance_id);

                    return $collection->pluck('id')->toArray();
                }
            }
        }
        catch (\Exception $e){
            $msg = ' Line no: '.$e->getLine().' => Failed Because '.$e->getMessage();
            Log::critical($msg);
            return array('error'=>true,'response'=>[],'message'=>$msg);
        }
    }




}

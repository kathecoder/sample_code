<?php

namespace App\Http\Controllers;

use App\CFLibrary\FilterHandler;
use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CycleTableController extends Controller
{

   public function __construct()
   {
       
   }
   
   public function create(Request $request)
   {
             
        return true;
   }

   public function getCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        if(isset($request['data'])){
            $data = $request['data'];
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];
                
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }else{
                        $id = '';
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        $form_submit_data = array();
        if($mode == 'save'){
            if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                        //print_r($request['data']);
                        $item_field = '';
                        $dat_split = explode('_',$key);
                        $count_split = count($dat_split);
                        for($j=0;$j<($count_split - 1);$j++){
                            $item_field .= $dat_split[$j].'_';
                        }
                        $item_field = substr($item_field,0,(strlen($item_field) - 1));
                        $item_value = $dat;
                        $form_submit_data[$item_field] = $item_value;
                        if($item_field == 'id'){
                            $selections['field_name'] = $item_field;
                            $selections['field_value'] = $item_value;
                        }
                        $i++;
                }
            }
        }
        
        $panel = new TableViewerPanel('cycles', 'CRUX', $mode, $selections, $form_submit_data, 'cycles');
        $table = $panel->show();
        $card_template = $table['content'];
        
        if($mode == "new"){
            $id = 0;
        }
        
        Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }

}

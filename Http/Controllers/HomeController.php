<?php

namespace App\Http\Controllers;

use App\CFLibrary\Model\InformationSchema;
use App\CFLibrary\TableViewerPanel;
use App\Exports\CustomExport;
use App\Exports\ValidationExport;
use App\Imports\EmployeeImport;
use App\Imports\ValidateEmployeeImport;
use App\Models\AppAssist;
use App\Models\AppExport;
use App\Models\Audit;
use App\Models\Band;
use App\Models\Cycle;
use App\Models\Employee;
use App\Models\EmployeeAssessor;
use App\Models\EmployeeHrRepresentative;
use App\Models\EmployeeTargetBusinessUnit;
use App\Models\FileImport;
use App\Models\Files;
use App\Models\Gender;
use App\Models\InputValidator;
use App\Models\Milestone;
use App\Models\OrgStructure;
use App\Models\PerformanceRating;
use App\Models\Race;
use App\Models\Status;
use App\Models\TargetMapping;
use App\User;
use App\Models\UserCycle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Pluralizer;
use Maatwebsite\Excel\Facades\Excel;
use PDF;


class HomeController extends Controller
{




    public function ssoLogin(){
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        $user_role_data      = null;
        $employees           = null;
        $names               = null;
        try{
            $user = Auth::user();
            if ($user){

                if($user->session_id != '')
                {
                    $last_session = Session::getHandler()->read($user->session_id);
                    if ($last_session)
                    {
                        Session::getHandler()->destroy($user->session_id);
                    }
                }

                return array
                (
                    'error'             => false,
                    'response'          => true,
                    'is_whitelisted'    => true,
                    'employees'         => [],
                    'message'           => "Authenticated Successfully.",
                    'names'             => explode(" ",$user->name),
                    'data'              => UserRolesController::getUserRoleData(),
                );


                //Log::debug("Line: " . __LINE__ . " Reporting live from HomeController User LOGGING IN: SUCCESS ");
                //$userRolesController = new UserRolesController(new UserController());
                //$user_role_data      = $userRolesController->getUserRole();
                //$employees           = $user->getEmployees();
                $names               = explode(" ",$user->name);
                $user_role_data      = null;//RolesController::getUserRoleData();

                $data = compact('user_role_data');
                return view('portal', $data);

            }
            else{

                //Auth::logout();

                return array
                (
                    'error'             => false,
                    'response'          => false,
                    'is_whitelisted'    => false,
                    'employees'         => [],
                    'data'              => null,
                    'message'           => 'Authentication Failed'
                );


                //Log::debug("Line: " . __LINE__ . " Reporting live from HomeController User LOGGING IN: FAILED ");
                $data = compact('employees', 'user_role_data', 'names');
                return view('portal', $data);
            }

        }
        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }

    public function portal(){
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        $user_role_data      = null;
        $employees           = null;
        $names               = null;
        try{

            $user = Auth::user();
            if ($user){
                //Log::debug("Line: " . __LINE__ . " Reporting live from HomeController User LOGGING IN: SUCCESS ");
                //$userRolesController = new UserRolesController(new UserController());
                //$user_role_data      = $userRolesController->getUserRole();
                //$employees           = $user->getEmployees();
                $names               = explode(" ",$user->name);
                $user_role_data      = UserRolesController::getUserRoleData();

                $data = compact('user_role_data');
                return view('portal', $data);

            }
            else{
                //Log::debug("Line: " . __LINE__ . " Reporting live from HomeController User LOGGING IN: FAILED ");
                $data = compact('employees', 'user_role_data', 'names');
                return view('portal', $data);
            }

        }
        catch (\Exception $e) {
            $msg = ' Line no: ' . $e->getLine() . ' => Failed Because ' . $e->getMessage();
            Log::critical($msg);
            return array('error' => true, 'response' => [], 'message' => $msg);
        }
    }
   public function signin(){
//       Log::info("Login Route");
       return view('portal');
   }
   public function testUpload(){

        return Redirect::route('sys_export', ['table_name'=>'employees', 'where_clause'=>'sds']);



       exit;
       $panel = new TableViewerPanel('vw_employees_combine', 'R', 'start', []);

       //$table = $panel->show();
       //$card_template = $table['content'];

       //dd($panel);

       $exclude_fields  = ['id','cycle_id', 'calc_change_structure', 'benchmark_id'];
       $table_name      = 'employees';
       $read_fields     = AppAssist::getUniFieldWrites($table_name, $exclude_fields, true);  //dd($read_fields);
       $uni_tables      = DB::table('uni_table')->select('*')->pluck('name', 'id')->toArray();
       $columns         = DB::select("describe $table_name");

       /*$information_schema = InformationSchema::select(
           'CHARACTER_MAXIMUM_LENGTH as CharLength',
           'NUMERIC_PRECISION as Base',
           'NUMERIC_SCALE as Decimals',
           'COLUMN_NAME as Field',
           'DATA_TYPE as Type',
           'IS_NULLABLE as Nullable')
           ->where('table_name', 'employee_incentives')
           ->get();
            dd($information_schema);*/


       $main_table_fields = [];
       foreach($columns as $column){
           $main_table_fields[] = $column->Field;
       }



       //build headers
       $headers  = [];
       $headings = [];
       $fields   = [];

       foreach ($read_fields as $key => $read_field){
           $headers[$key] = $read_field->label;
           $headings[] = $read_field->label;
           $fields[] = $key;
       }

       //get the data
        #1. build select - done
        #2. build from
        #3. build Joins
        #4. build where clause

       #1. build select
       $select = 'SELECT ';

       foreach ($read_fields as $key => $read_field){
           $reference_table_name    = $uni_tables[$read_field->uni_ref_table_id];
           $field_name              = $read_field->field_name;
           $ref_rule_arr            = explode('.', $read_field->reference_rule);
           if ($read_field->reference_rule !== NULL && $read_field->reference_rule !== "NULL" && !empty($read_field->reference_rule) ){
               //if ($reference_table_name !== $table_name){

                   if (count($ref_rule_arr) >= 3){
                       //currencies.id.code
                       $select .= "$ref_rule_arr[0].$ref_rule_arr[2] as $field_name, ";
                       //$select .= "$reference_table_name.$field_name, ";
                   }
                   elseif (count($ref_rule_arr) >= 2){
                       $select .= "$ref_rule_arr[0].$ref_rule_arr[1], ";
                       //$select .= "$reference_table_name.$field_name, ";
                   }
                   else{
                       $select .= "$reference_table_name.$field_name, ";
                   }
               //}
               /*else{
                   //$select .= "$ref_rule_arr[0].$ref_rule_arr[2] as $field_name, ";
                   $select .= "$read_field->reference_rule, ";
               }*/

           }
           elseif ($reference_table_name !== $table_name){
                   //$select .= "$ref_rule_arr[0].$ref_rule_arr[2] as $field_name, ";
                   $select .= "$reference_table_name.$field_name, ";
           }
           else{
               $select .= "$table_name.$key, ";
           }
       }
        //Build joins
       $join_tables = " ";
       foreach ($read_fields as $key => $read_field){
           $reference_table_name    = $uni_tables[$read_field->uni_ref_table_id];
           $field_name              = $read_field->field_name;
           $ref_rule_arr            = explode('.', $read_field->reference_rule);

           if ($read_field->reference_rule !== NULL && $read_field->reference_rule !== "None" && $read_field->reference_rule !== "NULL" && !empty($read_field->reference_rule) ){
               if (count($ref_rule_arr) >= 3){

                   if ($reference_table_name !== $table_name){
                       $join_tables .= "INNER JOIN $ref_rule_arr[0] ON $ref_rule_arr[0].$ref_rule_arr[1]  =  $reference_table_name.$field_name ";
                   }
                   else{
                       $join_tables .= "INNER JOIN $ref_rule_arr[0] ON $ref_rule_arr[0].$ref_rule_arr[1]  =  $table_name.$field_name ";
                   }

               }
               elseif (count($ref_rule_arr) >= 2){

                   if ($reference_table_name !== $table_name){
                       $join_tables .= "INNER JOIN $ref_rule_arr[0] ON $ref_rule_arr[0].$ref_rule_arr[1]  =  $reference_table_name.$field_name ";
                   }
                   else{
                       $join_tables .= "INNER JOIN $ref_rule_arr[0] ON $ref_rule_arr[0].$ref_rule_arr[1]  =  $table_name.$field_name ";
                   }
               }

           }



       }



       $select =  rtrim($select, ", ");

        //dd($read_fields);

       $data = [];
       $personnels = DB::table('vw_employees_export');
       //$personnels->where('cycle_id',  $cycle_id);
       $personnels = $personnels->select($fields)->get();

       foreach ($personnels as $personnel){
           $data [] = (array) $personnel;
       }


       $file_name = date('YmdHis').'_personnel_export.xlsx';
       $export = new CustomExport($data, $headings);

       return Excel::download($export, $file_name);


       //captureAudit(string $employee_personnel_number, string $field_name, string $user_personnel_number, $value_before, $value_after, int $phase_id = null,  int $cycle_id = null ){
       $audit = Audit::captureAudit('isiuss', 'Salaries', 'userpn894984', '788', '999009');


   }

}

<?php

namespace App\Http\Controllers;

use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use App\Models\AppExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
// use Auth;
use App\User;
use App\Models\InstanceIndicator;
use Illuminate\Support\Facades\Session;
use App\Models\Cycle;
use App\Models\CyclePhases;
use App\Models\Phase;
use App\Http\Controllers\PermissionController;
use App\CFLibrary\FilterHandler;

use App\Models\CyclePhaseSignoffs;
use App\Models\Signoff;

class SignoffController extends Controller
{

    protected $pageNationController;
    public function __construct(PageNationController $pageNationController)
    {
        $this->pageNationController = $pageNationController;
    }
   
   public function create(Request $request)
   {
             
        return true;
   }

   public function getCardPanel(Request $request){

            /* This will handle the data you sent from react - each page on react will be differnet  
                * It does not form part of framework and will be created specifically for project
                * You will thus have complete control of the elements you pass to framework, just ensure that you have these
                * variables set when using the datatable to connect with form of framework
                */
        $data = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        if(isset($request['data'])){
            $data = $request['data'];
            
            $i = 0;
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];               
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }
                }                 
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        $form_submit_data = array();
        if($mode == 'save'){
            if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                        //print_r($request['data']);
                        $item_field = '';
                        $dat_split = explode('_',$key);
                        $count_split = count($dat_split);
                        for($j=0;$j<($count_split - 1);$j++){
                            $item_field .= $dat_split[$j].'_';
                        }
                        $item_field = substr($item_field,0,(strlen($item_field) - 1));
                        $item_value = $dat;
                        $form_submit_data[$item_field] = $item_value;
                        if($item_field == 'id'){
                            $selections['field_name'] = $item_field;
                            $selections['field_value'] = $item_value;
                        }
                        $i++;
                }
            }
            //print_r($form_submit_data);
        }
        if($mode != 'read'){
     // print_r('selections');  
      //print_r($selections);
     // print_r('mode');  
     // print_r($mode);
        }
       $where = [];
       //print_r(FilterHandler::getFilteredData('vw_signoff', Auth::user()->id));
       $view_type = 'vertical';
        $panel = new TableViewerPanel('vw_signoff', 'RU', $mode, $selections, $form_submit_data, 'vw_signoff',$view_type, $where);
        $table = $panel->show();
        $card_template = $table['content'];
        
        if($mode == "new"){
           $id = 0;
        }

        //Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }
    
    public function getFormCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $mode = 'new';
        $id = '';
        $selections = array();
        $selections['id'] = '*';
        $message = '';
        $form_submit_data = array();
        //print_r($request['data']);
        
        if(isset($request['data'])){
            $data = $request['data'];
            
            $i = 0;
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];               
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }
                }                 
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        
        
        if($mode == 'save'){
            
            $instance_id      = PermissionController::getUserInstance()['response']['instance'];
            $cycle_id         = Cycle::getLiveCycleID();
            $phase_id         = Phase::getLivePhaseID();
            $user             = User::find(Auth::user()->id);
            $comments         = '';
            
            //if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                        //print_r($dat);
                        $item_field = '';
                        $dat_split = explode('_',$key);
                        $count_split = count($dat_split);
                        for($j=0;$j<($count_split - 1);$j++){
                            $item_field .= $dat_split[$j].'_';
                        }
                        $item_field = substr($item_field,0,(strlen($item_field) - 1));
                        $item_value = $dat;
                        $form_submit_data[$item_field] = $item_value;
                        if($item_field == 'id'){
                            $selections['field_name'] = $item_field;
                            $selections['field_value'] = $item_value;
                        }
                        $i++;
                }
            //}
            //print_r($form_submit_data);
            if(!empty($form_submit_data)){
                
                foreach($form_submit_data as $key=>$dat){
                    $item_field = $key;
                    $item_value = $dat;
                    $form_submit_data[$item_field] = $item_value;
                    //print_r($item_field);
                    if($item_field == 'comment'){
                        $comments = $item_value;
                    }                    
                }  
            $main_t_model = "App\\Models\\CyclePhaseSignoffs";
            //print_r('comments : '.$comments);    
            $cycle_phase = new CyclePhaseSignoffs;
            $cycle_phase->user_id = Auth::user()->id;
            $cycle_phase->phase_id = $phase_id;
            $cycle_phase->cycle_id = $cycle_id;
            $cycle_phase->comment = $comments;
            $cycle_phase->accept = 1;
            $cycle_phase->save();
            $id = $cycle_phase->id;  
            }    
            
            if(!is_numeric($id)){
                $id = '*';
            }
            
            $data = [];
            $mode = 'new';
            
            $selections = array();
            $selections['id'] = $id;
            $message = 'Signoff has been saved successfully.';
            $panel = new TableViewerPanel('cycle_phase_signoffs', 'CU', $mode, $selections, $form_submit_data, 'cycle_phase_signoffs');
            $table = $panel->show();
            $card_template = $table['content'];
            
        }else{
            $message = 'Success';
            $panel = new TableViewerPanel('cycle_phase_signoffs', 'CU', $mode, $selections, $form_submit_data, 'cycle_phase_signoffs');
            $table = $panel->show();
            $card_template = $table['content'];
            if($mode == "new"){
                $id = 0;
            }
        }
        
        
        
        //Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => $message,
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }
    
    public function getSignoffForm(Request $request){

        //print_r($request['data']);
        
        //Log::info("Portal Route");
        $mode = 'read';
        $id = 0;
        $data = [];
        $card_template = '';
        $message = '';
        
        if(isset($request['data'])){
            
            $instance_id      = PermissionController::getUserInstance()['response']['instance'];
            $cycle_id         = Cycle::getLiveCycleID();
            $phase_id         = Phase::getLivePhaseID();
            $user             = User::find(Auth::user()->id);
            $comments         = '';
            
            //if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                        //print_r($dat);
                        $item_field = '';
                        $dat_split = explode('_',$key);
                        $count_split = count($dat_split);
                        for($j=0;$j<($count_split - 1);$j++){
                            $item_field .= $dat_split[$j].'_';
                        }
                        $item_field = substr($item_field,0,(strlen($item_field) - 1));
                        $item_value = $dat;
                        $form_submit_data[$item_field] = $item_value;
                        if($item_field == 'id'){
                            $selections['field_name'] = $item_field;
                            $selections['field_value'] = $item_value;
                        }
                        $i++;
                }
            //}
            //print_r($form_submit_data);
            if(!empty($form_submit_data)){
                
                foreach($form_submit_data as $key=>$dat){
                    $item_field = $key;
                    $item_value = $dat;
                    $form_submit_data[$item_field] = $item_value;
                    //print_r($item_field);
                    if($item_field == 'comment'){
                        $comments = $item_value;
                    }                    
                }  
            $main_t_model = "App\\Models\\CyclePhaseSignoffs";
            //print_r('comments : '.$comments);    
            $cycle_phase = new CyclePhaseSignoffs;
            $cycle_phase->user_id = Auth::user()->id;
            $cycle_phase->phase_id = $phase_id;
            $cycle_phase->cycle_id = $cycle_id;
            $cycle_phase->comment = $comments;
            $cycle_phase->accept = 1;
            $cycle_phase->save();
            $id = $cycle_phase->id;  
            }    
            
            if(!is_numeric($id)){
                $id = '*';
            }
            
            $data = [];
            $mode = 'new';

            $message = 'Signoff has been saved successfully.';
            
        }else{
            $message = 'Error saving signoff, please contact the system administrator';
            
        }
        
        
        return array
        (
            'error' => false,
            'response' => true,
            'message' => $message,
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
        
    }


    public function getSignOffRecords(Request $request){
         $active_instance = PermissionController::getUserInstance()['response']['instance'];
         $userType = PermissionController::getUserInstance()['response']['instance'];

         $startDate  = isset($request['startDate']) ? $request['startDate'] : null;
         $endDate    = isset($request['endDate']) ? $request['endDate'] : null;
      


 
                $res    = Signoff::getRecords( $request->state['searchInput'],$request->state['skip'],$request->state['take'],true,true,$startDate,$endDate );

                $total    = Signoff::getRecords( $request->state['searchInput'],$request->state['skip'],$request->state['take'],false,true,$startDate,$endDate );
 
                
                $array =[];
                    foreach ($res as $data){
                        $array []= array(
                            'id'            => $data->id,
                            'comment'       => $data->comment,
                            'status'        => $data->status,
                            'user_name'     => $data->user_name,
                            'business_area' => $data->business_area,
                            'business_unit' => $data->business_unit,
                            'signoff_date'  => $data->created_at,
                        );
                    }
        return [
            "data"=>[
                'error'=>false,
                     'response'=>   [
                        'data'=>$array,
                        'totalTableData'=>count($total),
                        'pageNate'=>$this->pageNationController->index(count($total),$request->state['take']),

                     ],
                      
                 
                
                ]
        ];
 

    }












}

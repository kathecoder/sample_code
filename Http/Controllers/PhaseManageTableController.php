<?php

namespace App\Http\Controllers;

use App\CFLibrary\TableViewerPanel;
use App\CFLibrary\TemplateRenderer;
use App\CFLibrary\TemplatePanel;

use App\Models\Cycle;
use App\Models\CyclePhases;
use App\Models\Phase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PhaseManageTableController extends Controller
{

   public function __construct()
   {
       
   }
   
   public function create(Request $request)
   {
             
        return true;
   }

   public function getCardPanel(Request $request){

       /* This will handle the data you sent from react - each page on react will be differnet  
        * It does not form part of framework and will be created specifically for project
        * You will thus have complete control of the elements you pass to framework, just ensure that you have these
        * variables set when using the datatable to connect with form of framework
        */
        $data = [];
        $where = [];
        $mode = 'read';
        $id = '';
        $selections = array();
        if(isset($request['data'])){
            $data = $request['data'];
            foreach($data as $key=>$dat){
                $key_split = explode('_',$key);
                $item_name = $key_split[0];
                $item_cnt = $key_split[1];
                
                if($item_name == 'mode'){
                    $mode = $dat;
                }else{
                    if($item_name == 'selectorselect'){
                        $dat_split = explode('_',$dat);
                        $item_field = $dat_split[0];
                        $item_value = $dat_split[1];
                        $selections['field_name'] = $item_field;
                        $selections['field_value'] = $item_value;
                        $id = $item_value;
                    }else{
                        $id = '';
                    }
                }
            }
        }else if (isset($request['id']) && isset($request['mode'])){
             $id = $request['id'];
             $selections['field_name'] = 'id';
             $selections['field_value'] = $id;
             $mode = $request['mode'];
        }
        
        $form_submit_data = array();
        if($mode == 'save'){
            if(isset($request['data'])){
                $data = $request['data'];            
                $i = 0;
                foreach($data as $key=>$dat){
                        //print_r($request['data']);
                        $item_field = '';
                        $dat_split = explode('_',$key);
                        $count_split = count($dat_split);
                        for($j=0;$j<($count_split - 1);$j++){
                            $item_field .= $dat_split[$j].'_';
                        }
                        $item_field = substr($item_field,0,(strlen($item_field) - 1));
                        $item_value = $dat;
                        $form_submit_data[$item_field] = $item_value;
                        if($item_field == 'id'){
                            $selections['field_name'] = $item_field;
                            $selections['field_value'] = $item_value;
                        }
                        $i++;
                }
            }
        }
        
        /*SET PERMISSION PER INSTANCE,PHASE(IF ANY),USER(IF ANY) etc.*/
        $instance_id = NULL;
        $instance_name = '';
        
        $user_instance = PermissionController::getUserInstance();
        if($user_instance['message'] == 'Success'){
            $instance_id = $user_instance['response']['instance'];
            $instance_name = $user_instance['response']['instance_name'];
        }
        
        // $user_roles = PermissionController::getRolesMapping();
        $where[]['sub_owner_id'] = $instance_id;
        $where[]['is_core'] = 1;
        //print_r($where);exit;
        
        $panel = new TableViewerPanel('phases', 'CRU', $mode, $selections, $form_submit_data, 'phases','vertical', $where);
        $table = $panel->show();
        $card_template = $table['content'];
        
        if($mode == "new"){
            $id = 0;
        }
        
        //Log::info("Portal Route");
        return array
        (
            'error' => false,
            'response' => true,
            'message' => 'Success',
            'template' => $card_template,
            'data'=> $data,
            'mode'=>$mode,
            'id' =>$id,
        );
    }


    public function getTimelinePhases(Request $request){

        $active_instance = PermissionController::getUserInstance();
        $cycle_id        = Cycle::getLiveCycleID();
        $live_phase_id   = 0;

        $live_phase = CyclePhases::where('live', 1)->where('cycle_id', $cycle_id)->where('instance_id', $active_instance['response']['instance'])->first();
        if ($live_phase)
            $live_phase_id = $live_phase->id;

        $phases     = Phase::where('sub_owner_id', $active_instance['response']['instance'])->get();
        $data = [];
        foreach($phases as $phase){
            $data[] = array(
                'name'          =>  $phase->name,
                'description'   =>  $phase->description !== null ? $phase->description : "No description.",
                'live'          =>  $phase->id === $live_phase_id ? 1:0,
                'id'            =>  $phase->id
            );
        }

         return array
        (
            'error'     => false,
            'response'  => $data,
            'message'   => 'Success',
        );

    }
    
    public function getActiveTimelinePhases(Request $request){

        $active_instance_id = PermissionController::getUserInstance()['response']['instance'];
        $cycle_id        = Cycle::getLiveCycleID();
        $live_phase_id   = 0;

        $live_phase = CyclePhases::where('live', 1)->where('cycle_id', $cycle_id)->where('instance_id', $active_instance_id)->first();
        if ($live_phase){
            $live_phase_id = $live_phase->id;

            $data = $live_phase_id;
        
        }else{
          $data = 2;   
        }
        
        //print_r('Phase : '.$data);
        
         return array
        (
            'error'     => false,
            'response'  => ['phase_id' => $live_phase_id, 'instance' => $active_instance_id],
            'message'   => 'Success',
        );

    }



}
